﻿''' <summary> A stop watch with timeout preset. </summary>
''' <license> (c) 2016 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="05/06/2009" by="David" revision="1.1.3413.x"> Created. </history>
<DebuggerDisplay("Elapsed = {Elapsed}")>
Public Class TimeoutStopwatch
    Inherits Diagnostics.Stopwatch

    ''' <summary> Constructs a new timeout stop watch with the specified
    ''' <paramref name="duration">duration</paramref> </summary>
    ''' <param name="duration"> Specifies the stop watch timeout duration. </param>
    Public Sub New(ByVal duration As TimeSpan)
        MyBase.New()
        Me._Duration = duration
    End Sub

    ''' <summary> Constructs a new timeout stop watch with the specified
    ''' <paramref name="duration">duration</paramref> </summary>
    ''' <param name="duration"> Specifies the stop watch duration. </param>
    ''' <returns>Returns true if timeout is done.</returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix",
        Justification:="Must be the same name as in the base class")>
    Public Overloads Shared Function StartNew(ByVal duration As TimeSpan) As TimeoutStopwatch
        Dim osw As New TimeoutStopwatch(duration)
        osw.Start()
        Return osw
    End Function

    ''' <summary> Gets or sets the duration of the timeout stop watch. </summary>
    ''' <value> <c>Duration</c>is a TimeSpan property. </value>
    Public Property Duration() As TimeSpan

    ''' <summary> Returns true if timeout. </summary>
    ''' <returns> Returns true if timeout. </returns>
    Public Function IsTimeout() As Boolean
        Return Me.Elapsed > Me.Duration
    End Function

    ''' <summary> Waits time timeout. </summary>
    Public Sub Wait()
        If Not Me.IsRunning Then Me.Start()
        Dim interval As TimeSpan = Me.Duration - Me.Elapsed
        If interval > TimeSpan.Zero Then Threading.Tasks.Task.Delay(Me.Duration - Me.Elapsed)
#If False Then
        Do Until Me.IsTimeout
            System.Windows.Fo rms.Application.DoEvents()
        Loop
#End If
    End Sub

End Class

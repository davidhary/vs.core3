﻿Imports System.Timers
''' <summary> A yield counter. </summary>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="10/8/2016" by="David" revision=""> Created. </history>
<DebuggerDisplay("Total = {TotalCount}")>
Public Class YieldTimer
    Inherits YieldCounter

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me._ResetKnownState()
    End Sub

    ''' <summary> Creates a new YieldTimer. </summary>
    ''' <remarks> Helps implement CA2000. </remarks>
    ''' <returns> A YieldTimer. </returns>
    Public Shared Function Create() As YieldTimer
        Dim result As YieldTimer = Nothing
        Try
            result = New YieldTimer
        Catch
            result?.Dispose()
            Throw
        End Try
        Return result
    End Function

    ''' <summary> Constructor. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As YieldTimer)
        MyBase.New(value)
        If value IsNot Nothing Then
            Me.Interval = value.Interval
        End If
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                                                   release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If disposing AndAlso Not Me.IsDisposed Then
            Me._Timer?.Dispose() : Me._Timer = Nothing
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region

#Region " RESET AND CLEAR "

    ''' <summary> Resets the know state. </summary>
    Private Sub _ResetKnownState()
        Me._Timer = New Timers.Timer(1000)
        Me._Timer.Stop()
    End Sub

    ''' <summary> Resets the know state. </summary>
    Public Overrides Sub ResetKnownState()
        Me._ResetKnownState()
        MyBase.ResetKnownState()
    End Sub

#End Region

#Region " TIMER "

    Private WithEvents _Timer As Timers.Timer

    ''' <summary> Gets the interval. </summary>
    ''' <value> The interval. </value>
    Public Property Interval As TimeSpan
        Get
            Return TimeSpan.FromMilliseconds(Me._Timer.Interval)
        End Get
        Set(value As TimeSpan)
            Me._Timer.Interval = value.TotalMilliseconds
        End Set
    End Property

    ''' <summary> Timer elapsed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Elapsed event information. </param>
    Private Sub _Timer_Elapsed(sender As Object, e As ElapsedEventArgs) Handles _Timer.Elapsed
        If Threading.SynchronizationContext.Current Is Nothing AndAlso Me.CapturedSyncContext IsNot Nothing Then
            Me.ApplyCapturedSyncContext()
            If Threading.SynchronizationContext.Current IsNot Nothing Then
                Me.SafePostPropertyChanged(NameOf(YieldTimer.Elapsed))
            End If
        End If
    End Sub

    ''' <summary> Clears know start and starts the timer. </summary>

    Public Overrides Sub Start()
        If Me.CapturedSyncContext IsNot Nothing Then Me._Timer.Start()
        MyBase.Start()
    End Sub

    ''' <summary> Finishes measuring elapsed time. </summary>
    Public Overrides Sub Finish()
        Me._Timer.Stop()
        MyBase.Finish()
    End Sub

    ''' <summary> Resumes counting after a <see cref="Finish()">stop</see>. </summary>
    Public Overrides Sub [Resume]()
        Me._Timer.Start()
        MyBase.Resume()
    End Sub

#End Region

End Class

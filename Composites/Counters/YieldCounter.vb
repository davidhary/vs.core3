﻿Imports System.ComponentModel
''' <summary> A yield counter. </summary>
''' <remakrs> Time is kept in Coordinated Universal Time (UTC). </remakrs>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="10/8/2016" by="David" revision=""> Created. </history>
<DebuggerDisplay("Total = {TotalCount}")>
Public Class YieldCounter
    Inherits isr.Core.Pith.PropertyNotifyBase

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me._ResetKnownState()
        Me._ClearKnownState()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As YieldCounter)
        Me.New()
        If value IsNot Nothing Then
            Me._TotalCount = value.TotalCount
            Me._PercentYieldFormat = value.PercentYieldFormat
        End If
    End Sub

    ''' <summary> Creates a new Yield Counter. </summary>
    ''' <remarks> Helps implement CA2000. </remarks>
    ''' <returns> A  <see cref="YieldCounter"/>. </returns>
    Public Shared Function Create() As YieldCounter
        Dim result As YieldCounter = Nothing
        Try
            result = New YieldCounter
        Catch
            result?.Dispose()
            Throw
        End Try
        Return result
    End Function

#End Region

#Region " RESET AND CLEAR "

    ''' <summary> Clears to known (clear) state; Clears select values to their initial state. </summary>
    Private Sub _ClearKnownState()
        Me.ClearCounters()
    End Sub

    Public Overridable Sub ClearKnownState()
        ' if clearing and value cleared, we need to publish to make sure displays get updated.
        Me._ClearKnownState()
        Me.Publish()
    End Sub

    ''' <summary> Publishes this object. </summary>
    Public Overridable Sub Publish()
        Me.SafePostPropertyChanged(NameOf(YieldCounter.TotalCount))
    End Sub

    ''' <summary> Resets the know state. </summary>
    Private Sub _ResetKnownState()
        Me._PercentYieldFormat = YieldCounter.DefaultPercentYieldFormat
    End Sub

    ''' <summary> Resets the know state. </summary>
    Public Overridable Sub ResetKnownState()
        Me._ResetKnownState()
        Me.Publish()
    End Sub

#End Region

#Region " COUNTERS "

    Private _FailedCount As Integer
    ''' <summary> Gets or sets the number of Failures. </summary>
    ''' <value> The Failures number of count. </value>
    Public Property FailedCount As Integer
        Get
            Return Me._FailedCount
        End Get
        Protected Set(value As Integer)
            Me._FailedCount = value
        End Set
    End Property

    Private _InvalidCount As Integer
    ''' <summary> Gets or sets the number of Invalid values. </summary>
    ''' <value> The Invalid number of count. </value>
    Public Property InvalidCount As Integer
        Get
            Return Me._InvalidCount
        End Get
        Protected Set(value As Integer)
            Me._InvalidCount = value
        End Set
    End Property

    ''' <summary> Gets the number of valid values. </summary>
    ''' <value> The number of valid values. </value>
    Public ReadOnly Property ValidCount As Integer
        Get
            Return Me.TotalCount - Me.InvalidCount
        End Get
    End Property

    ''' <summary> Gets the number of goods. </summary>
    ''' <value> The number of goods. </value>
    Public ReadOnly Property GoodCount As Integer
        Get
            Return Me.ValidCount - Me.FailedCount
        End Get
    End Property

    Private _TotalCount As Integer
    ''' <summary> Gets or sets the total count. </summary>
    ''' <value> The total number of count. </value>
    Public Property TotalCount As Integer
        Get
            Return Me._TotalCount
        End Get
        Protected Set(value As Integer)
            If value <> Me.TotalCount Then
                Me._TotalCount = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Clears the counters. </summary>
    Private Sub ClearCounters()
        Me._TotalCount = 0
        Me._InvalidCount = 0
        Me._FailedCount = 0
    End Sub

    ''' <summary> Increments the total yield count. </summary>
    ''' <param name="count"> Number of. </param>
    Public Sub Increment(ByVal count As Integer)
        Me.Increment(YieldBinNumber.Good, count)
    End Sub

    ''' <summary> Increments the yield count </summary>
    ''' <param name="binNumber"> The bin number. </param>
    ''' <param name="count">     Number of. </param>
    Public Sub Increment(ByVal binNumber As YieldBinNumber, ByVal count As Integer)
        Select Case binNumber
            Case YieldBinNumber.Good
                Me.TotalCount += count
            Case YieldBinNumber.Fail
                Me.FailedCount += count
                Me.TotalCount += count
            Case YieldBinNumber.Invalid
                Me.InvalidCount += count
                Me.TotalCount += count
        End Select
    End Sub

#End Region

#Region " YIELD "

    ''' <summary> Gets the total used for calculating yield. </summary>
    ''' <value> The total used for calculating yield. </value>
    Public Overridable ReadOnly Property TotalYieldCount As Integer
        Get
            Return Me.TotalCount
        End Get
    End Property

    ''' <summary> Gets the count used for calculating yield. </summary>
    ''' <value> The count used for calculating yield. </value>
    Public Overridable ReadOnly Property YieldCount As Integer
        Get
            Return Me.GoodCount
        End Get
    End Property

    ''' <summary> Gets the percent yield. </summary>
    ''' <value> The percent yield. </value>
    Public ReadOnly Property PercentYield As Double
        Get
            If Me.TotalCount > 0 Then
                Return 100 * Me.YieldCount / Me.TotalYieldCount
            Else
                Return 0
            End If
        End Get
    End Property

    ''' <summary> The default percent yield format. </summary>
    Public Const DefaultPercentYieldFormat As String = "{0:0} %"

    ''' <summary> Gets the percent yield format. </summary>
    ''' <value> The percent yield format. </value>
    Public Overridable Property PercentYieldFormat As String

    ''' <summary> Gets the percent yield caption. </summary>
    ''' <value> The percent yield count caption. </value>
    Public ReadOnly Property PercentYieldCaption As String
        Get
            Return String.Format(Globalization.CultureInfo.CurrentCulture, Me.PercentYieldFormat, Me.PercentYield)
        End Get
    End Property

    ''' <summary> Gets the hourly yield. </summary>
    ''' <param name="elapsed"> The elapsed. </param>
    ''' <returns> An Integer. </returns>
    Public Function HourlyRate(ByVal elapsed As TimeSpan) As Double
        Return If(elapsed.TotalSeconds > 0, 3600 * Me.YieldCount / elapsed.TotalSeconds, 0)
    End Function

#End Region

End Class

''' <summary> Values that represent yield bin numbers. </summary>
Public Enum YieldBinNumber
    <Description("Good")> Good
    <Description("Fail")> Fail
    <Description("Invalid")> Invalid
End Enum
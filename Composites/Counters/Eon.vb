﻿Imports System.ComponentModel
''' <summary> Eon - a time span time keeper. </summary>
''' <remakrs> Time is kept in Coordinated Universal Time (UTC). </remakrs>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="11/19/2014" by="David" revision="2.1.5436"> Created. </history>
Public Class Eon

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    Public Sub New(ByVal minimumTime As DateTime)
        MyBase.New()
        Me._MinimumTime = minimumTime
        Me._ResetKnownState()
    End Sub

    ''' <summary> Validated eon. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="eon"> The eon. </param>
    ''' <returns> An Eon. </returns>
    Public Shared Function ValidatedEon(ByVal eon As Eon) As Eon
        If eon Is Nothing Then Throw New ArgumentNullException(NameOf(eon))
        Return eon
    End Function

    ''' <summary> Constructor. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As Eon)
        Me.New(Eon.ValidatedEon(value).MinimumTime)
        If value IsNot Nothing Then
            Me._DateFormatUtc = value.DateFormatUtc
            Me._DateFormatLocal = value.DateFormatLocal
            Me._TimeFormatUtc = value.TimeFormatUtc
            Me._TimeFormatLocal = value.TimeFormatLocal
            Me._DateTimeFormatUtc = value.DateTimeFormatUtc
            Me._DateTimeFormatLocal = value.DateTimeFormatLocal
            Me._ElapsedTimeFormat = value.ElapsedTimeFormat
            Me._InitializeKnownState(value.StartTime, value.EndTime)
        End If
    End Sub

#End Region

#Region " RESET AND CLEAR "

    ''' <summary> Clears to known (clear) state; Clears select values to their initial state. </summary>
    Private Sub _ClearKnownState()
        Me.StartTime = Me.MinimumTime
        Me.EndTime = New DateTime?
    End Sub

    ''' <summary> Clears to known (clear) state; Clears select values to their initial state. </summary>
    Public Sub ClearKnownState()
        Me._ClearKnownState()
    End Sub

    ''' <summary> Initializes to known (Initialize) state; Initializes select values to their initial
    ''' state. </summary>
    ''' <param name="startTime"> The start time. </param>
    ''' <param name="endTime">   The end time. </param>
    Private Sub _InitializeKnownState(ByVal startTime As DateTime, ByVal endTime As DateTime?)
        Me.StartTime = startTime
        Me.EndTime = endTime
    End Sub


    ''' <summary> Initializes to known (Initialize) state; Initializes select values to their initial
    ''' state. </summary>
    ''' <param name="startTime"> The start time. </param>
    ''' <param name="endTime">   The end time. </param>
    Public Sub InitializeKnownState(ByVal startTime As DateTime, ByVal endTime As DateTime?)
        Me._InitializeKnownState(startTime, endTime)
    End Sub

    ''' <summary>
    ''' Initializes to known (Initialize) state; Initializes select values to their initial state.
    ''' </summary>
    ''' <param name="startTime"> The start time. </param>
    Public Sub InitializeKnownState(ByVal startTime As DateTime)
        Me.InitializeKnownState(startTime, New DateTime?)
    End Sub

    ''' <summary> Initializes to known (Initialize) state; Initializes select values to their initial state. </summary>
    Public Sub InitializeKnownState()
        Me.InitializeKnownState(DateTime.UtcNow)
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    Private Sub _ResetKnownState()
        Me._ClearKnownState()
        Me._DateFormatLocal = Eon.DefaultDateFormat
        Me._DateFormatUtc = Eon.DefaultDateFormat & Eon.UniversalTimeSuffix
        Me._TimeFormatLocal = Eon.DefaultTimeFormat
        Me._TimeFormatUtc = Eon.DefaultTimeFormat & Eon.UniversalTimeSuffix
        Me._DateTimeFormatLocal = Eon.DefaultTimestampFormat
        Me._DateTimeFormatUtc = Eon.DefaultTimestampFormat & Eon.UniversalTimeSuffix
        Me._ElapsedTimeFormat = Eon.DefaultElapsedTimeFormat
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    Public Sub ResetKnownState()
        Me._ResetKnownState()
    End Sub

#End Region

#Region " TIME PROPERTIES "

    Private _StartTime As DateTime

    ''' <summary> Gets or sets the start time. </summary>
    ''' <remarks>
    ''' The eon must have a start time. Start time can be set to <see cref="MinimumTime"/> to tag
    ''' the eon as not started. Start time can be <see cref="ResetStartTime()">reset</see>, 
    ''' <see cref="PostponeStartTime(Date)">posponed</see> or <see cref="AdvanceStartTime(Date)">advanced</see>.
    ''' </remarks>
    ''' <value> The start time. </value>
    Public Property StartTime As DateTime
        Get
            Return Me._StartTime
        End Get
        Set(value As DateTime)
            Me._StartTime = value
        End Set
    End Property

    Private _EndTime As DateTime?
    ''' <summary> Gets the End time. </summary>
    ''' <value> The End time. </value>
    Public Property EndTime As DateTime?
        Get
            Return Me._EndTime
        End Get
        Set(value As DateTime?)
            Me._EndTime = value
        End Set
    End Property

#End Region

#Region " TIME FUNCTIONS "

    ''' <summary> Determines if the <see cref="Eon"/> started. </summary>
    ''' <returns> <c>true</c> if it started; otherwise <c>false</c> </returns>
    Public Function HasStarted() As Boolean
        Return Me.StartTime > Me.MinimumTime
    End Function

    ''' <summary> Query if time span has ended. </summary>
    ''' <returns> <c>true</c> if time span has ended; otherwise <c>false</c> </returns>
    Public Function HasEnded() As Boolean
        Return Me.EndTime.HasValue
    End Function

    ''' <summary> Query start time is later than end time or ended but not started. </summary>
    ''' <returns> <c>true</c> if conflict; otherwise <c>false</c> </returns>
    Public Function HasConflict() As Boolean
        Return (Me.HasEnded AndAlso Not Me.HasStarted) OrElse
                Me.HasStarted AndAlso Me.HasEnded AndAlso Me.StartTime > Me.EndTime.Value
    End Function

    ''' <summary> Query if this Eon is active. </summary>
    ''' <returns> <c>true</c> if active; otherwise <c>false</c>. </returns>
    Public Function IsActive() As Boolean
        Return Me.HasStarted AndAlso Not Me.HasEnded
    End Function

    ''' <summary> Gets the active start time. Defaults to <see cref="DateTime.MinValue"/>. Is set to SQL minimum time. </summary>
    ''' <value> The <see cref="StartTime"/> if started or the <see cref="MinimumTime"/> </value>
    Public Property MinimumTime As DateTime

    ''' <summary> Gets the eon start time. </summary>
    ''' <value> The eon start time if started or the <see cref="MinimumTime"/> </value>
    Public ReadOnly Property ActiveStartTime As DateTime
        Get
            If Me.HasStarted Then
                Return Me.StartTime
            Else
                Return Me.MinimumTime
            End If
        End Get
    End Property

    ''' <summary> Gets the active end time. </summary>
    ''' <value> The <see cref="EndTime"/> if started or the <see cref="MinimumTime"/> </value>
    Public ReadOnly Property ActiveEndTime As DateTime
        Get
            If Me.HasEnded Then
                Return Me.EndTime.Value
            ElseIf Me.HasStarted Then
                Return DateTime.UtcNow
            Else
                Return Me.MinimumTime
            End If
        End Get
    End Property

    ''' <summary> Gets the last elapsed time. </summary>
    ''' <value> The last elapsed time. </value>
    Protected ReadOnly Property LastElapsedTime As TimeSpan

    ''' <summary> Gets the elapsed time span. </summary>
    ''' <value> The elapsed time span. </value>
    Public ReadOnly Property ElapsedTimespan As TimeSpan
        Get
            Me._LastElapsedTime = Me.ActiveEndTime.Subtract(Me.ActiveStartTime)
            Return Me.LastElapsedTime
        End Get
    End Property

#End Region

#Region " TIME ACTIONS "

    ''' <summary> Starts the <see cref="Eon"/>. </summary>
    Public Sub [Start]()
        Me.InitializeKnownState()
    End Sub

    ''' <summary> Starts the <see cref="Eon"/>. </summary>
    ''' <param name="dateTime"> The Date/Time. </param>
    Public Overridable Sub Start(ByVal dateTime As DateTime)
        Me.InitializeKnownState(dateTime)
    End Sub

    ''' <summary> Ends the <see cref="Eon"/>. </summary>
    Public Overridable Sub Finish()
        Me.Finish(DateTime.UtcNow)
    End Sub

    ''' <summary> Ends the <see cref="Eon"/>. </summary>
    ''' <param name="value"> The value. </param>
    Public Overridable Sub Finish(ByVal value As DateTime)
        Me.EndTime = value
    End Sub

    ''' <summary> Opens the Eon after a <see cref="Finish()">stop</see>. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId:="Resume")>
    Public Overridable Sub [Resume]()
        Me.EndTime = New DateTime?
    End Sub

    ''' <summary> Resets the Eon start time to current time. </summary>
    Public Overridable Sub ResetStartTime()
        Me.ResetStartTime(DateTime.UtcNow)
    End Sub

    ''' <summary> Resets the Eon start time. </summary>
    ''' <param name="value"> The new start time. </param>
    Public Overridable Sub ResetStartTime(ByVal value As DateTime)
        Me.StartTime = value
    End Sub

#End Region

#Region " TIME SHIFTS "

    ''' <summary> Advance start time to an earlier time. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub AdvanceStartTime(ByVal value As DateTime)
        If Me.HasStarted Then
            If Me.StartTime > value Then
                Me.StartTime = value
            End If
        Else
            Me.Start(value)
        End If
    End Sub

    ''' <summary> Postpone start time. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub PostponeStartTime(ByVal value As DateTime)
        If Me.HasStarted Then
            If Me.StartTime < value Then
                Me.StartTime = value
            End If
        Else
            Me.Start(value)
        End If
    End Sub

    ''' <summary> Sets start time to a later start time. </summary>
    ''' <param name="startTime"> The start time. </param>
    Public Sub StartLater(ByVal startTime As DateTime)
        If Not Me.HasStarted OrElse Me.StartTime < startTime Then Me.Start(startTime)
    End Sub

    ''' <summary> Sets start time to an earlier start time. </summary>
    ''' <param name="startTime"> The start time. </param>
    Public Sub StartEarlier(ByVal startTime As DateTime)
        If Not Me.HasStarted OrElse Me.StartTime > startTime Then Me.Start(startTime)
    End Sub

    ''' <summary> Postpone end time. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub PostponeEndTime(ByVal value As DateTime?)
        If value.HasValue Then
            If Me.HasStarted Then
                If Me.HasEnded Then
                    If Me.EndTime.Value < value.Value Then
                        Me.Finish(value.Value)
                    End If
                Else
                    Me.EndTime = value
                End If
            Else
                Me.Finish(value.Value)
            End If
        End If
    End Sub

#End Region

#Region " TIMESTAMP FORMATTING "

    ''' <summary> Converts a timestamp to a caption. </summary>
    ''' <param name="format">    The ElapsedTime Date/Time. </param>
    ''' <param name="timestamp"> The timestamp Date/Time. </param>
    ''' <returns> timestamp as a String. </returns>
    Public Shared Function ToTimestampCaption(ByVal format As String, ByVal timestamp As Date) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:" & format & "}", timestamp)
    End Function

    ''' <summary> Converts timestamp to date. </summary>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="timestamp"> The timestamp Date/Time. </param>
    ''' <returns> A Date. </returns>
    Public Shared Function FromTimestampCaption(ByVal format As String, ByVal timestamp As String) As Date
        If String.IsNullOrWhiteSpace(format) Then Throw New ArgumentNullException(NameOf(format))
        If String.IsNullOrWhiteSpace(timestamp) Then Throw New ArgumentNullException(NameOf(timestamp))
        Dim value As Date = Date.MinValue
        Dim style As Globalization.DateTimeStyles = Globalization.DateTimeStyles.AssumeLocal
        If format.EndsWith(Eon.UniversalTimeSuffix, StringComparison.OrdinalIgnoreCase) Then
            format = format.Substring(0, format.Length - 1)
            style = Globalization.DateTimeStyles.AssumeUniversal
        Else
            style = Globalization.DateTimeStyles.AssumeLocal
        End If
        If Date.TryParseExact(timestamp, format, Globalization.CultureInfo.CurrentCulture, style, value) Then
        End If
        Return value
    End Function

#End Region

#Region " ELAPSED TIME FORMATTING "

    ''' <summary> Converts an Elapsed Time to a caption. </summary>
    ''' <param name="format">      The ElapsedTime Date/Time. </param>
    ''' <param name="elapsedTime"> The elapsed time. </param>
    ''' <returns> ElapsedTime as a String. </returns>
    Public Shared Function ToElapsedTimeCaption(ByVal format As String, ByVal elapsedTime As TimeSpan) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:" & format & "}", elapsedTime)
    End Function

    ''' <summary> Converts ElapsedTime to date. </summary>
    ''' <param name="ElapsedTime"> The ElapsedTime Date/Time. </param>
    ''' <returns> A Date. </returns>
    Public Shared Function FromElapsedTimeCaption(ByVal format As String, ByVal elapsedTime As String) As TimeSpan
        Dim value As TimeSpan = TimeSpan.Zero
        If TimeSpan.TryParseExact(elapsedTime, format, Globalization.CultureInfo.CurrentCulture, value) Then
        End If
        Return value
    End Function

#End Region

End Class

''' <summary> A list of eons sorted by the Eon start time. </summary>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="7/11/2017" by="David" revision=""> Created. </history>
<CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix")>
Public Class EonSortedList
    Inherits SortedList(Of DateTime, Eon)

    ''' <summary> Default constructor. </summary>
    Public Sub New(ByVal minimumTime As DateTime)
        MyBase.New()
        Me._MinimumTimeEon = New Eon(minimumTime) With {.StartTime = minimumTime}
    End Sub

    ''' <summary> Gets the minimum time eon. </summary>
    ''' <value> The minimum time eon. </value>
    Public ReadOnly Property MinimumTimeEon As Eon

    ''' <summary> Gets the default start time. </summary>
    ''' <value> The default start time. </value>
    Public Shared ReadOnly Property DefaultStartTime As DateTime

    ''' <summary> Adds item. </summary>
    ''' <param name="item"> The item to add. </param>
    Public Overloads Sub Add(ByVal item As Eon)
        If item IsNot Nothing Then
            MyBase.Add(item.StartTime, item)
        End If
    End Sub

    ''' <summary> Adds item. </summary>
    ''' <param name="startTime"> The start time. </param>
    ''' <param name="endTime">   The end time. </param>
    Public Overloads Sub Add(ByVal startTime As DateTime, endTime As DateTime?)
        MyBase.Add(startTime, New Eon(Me.MinimumTimeEon.MinimumTime) With {.StartTime = (startTime), .EndTime = (endTime)})
    End Sub

    ''' <summary> Updates this object. </summary>
    ''' <param name="startTime"> The start time. </param>
    ''' <param name="endTime">   The end time. </param>
    Public Sub Update(ByVal startTime As DateTime, endTime As DateTime?)
        If Me.ContainsKey(startTime) Then
            With Me(startTime)
                .StartTime = startTime
                .EndTime = endTime
            End With
        Else
            Me.Add(startTime, endTime)
        End If
    End Sub

    ''' <summary> Gets the first. </summary>
    ''' <value> The first. </value>
    Public ReadOnly Property First As Eon
        Get
            If Me.Values.Any Then
                Return Me.Values.First
            Else
                Return Me.MinimumTimeEon
            End If
        End Get
    End Property

    ''' <summary> Gets the last. </summary>
    ''' <value> The last. </value>
    Public ReadOnly Property Last As Eon
        Get
            If Me.Values.Any Then
                Return Me.Values.Last
            Else
                Return Me.MinimumTimeEon
            End If
        End Get
    End Property

    ''' <summary> Gets the start time. </summary>
    ''' <value> The start time. </value>
    Public ReadOnly Property StartTime As DateTime
        Get
            Return Me.First.ActiveStartTime
        End Get
    End Property

    ''' <summary> Gets the start time caption. </summary>
    ''' <returns> A String. </returns>
    Public Function StartTimeCaption() As String
        Return Me.StartTimeCaption(TimeStyle.LocalTime)
    End Function

    ''' <summary> Gets the start time caption. </summary>
    ''' <param name="timeStyle"> The time style. </param>
    ''' <returns> A String. </returns>
    Public Function StartTimeCaption(ByVal timeStyle As TimeStyle) As String
        Return Me.First.StartTimeCaption(timeStyle)
    End Function

    ''' <summary> Gets the end time. </summary>
    ''' <value> The end time. </value>
    Public ReadOnly Property EndTime As DateTime
        Get
            Return Me.Last.ActiveEndTime
        End Get
    End Property

    ''' <summary> Elapsed time span. </summary>
    ''' <returns> A TimeSpan. </returns>
    Public Function ElapsedTimeSpan() As TimeSpan
        Dim value As TimeSpan = TimeSpan.Zero
        For Each eon As Eon In Me.Values
            value.Add(eon.ElapsedTimespan)
        Next
    End Function

    ''' <summary> Elapsed time caption. </summary>
    ''' <returns> A String. </returns>
    Public Function ElapsedTimeCaption() As String
        Return Eon.ToElapsedTimeCaption(Eon.DefaultElapsedTimeFormat, Me.ElapsedTimeSpan)
    End Function

End Class

''' <summary> Dictionary of ordered list of Eons. </summary>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="8/31/2017" by="David" revision=""> Created. </history>
<CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2237:MarkISerializableTypesWithSerializable")>
Public Class OrderedEonsDictionary
    Inherits Dictionary(Of Integer, isr.Core.Composites.EonSortedList)

    Public Sub AddDictionary(ByVal key As Integer, ByVal minimumTime As DateTime)
        If Not Me.ContainsKey(key) Then
            Me._MinimumTimeEon = New Eon(minimumTime)
            Me.Add(key, New isr.Core.Composites.EonSortedList(minimumTime))
        End If
    End Sub

    ''' <summary> Returns true if the reference dictionary has any elements. </summary>
    ''' <param name="key"> The key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function Any(ByVal key As Integer) As Boolean
        Return Me.ContainsKey(key) AndAlso Me(key).Any
    End Function

    ''' <summary> Gets the minimum time eon. </summary>
    ''' <value> The minimum time eon. </value>
    Public ReadOnly Property MinimumTimeEon As Eon

    ''' <summary> Gets the start time. </summary>
    ''' <param name="key"> The key. </param>
    ''' <returns> A DateTime. </returns>
    Public Function StartTime(ByVal key As Integer) As DateTime
        If Me.Any(key) Then
            Return Me(key).StartTime
        Else
            Return Me.MinimumTimeEon.StartTime
        End If
    End Function

    ''' <summary> Gets the start time caption. </summary>
    Public Function StartTimeCaption(ByVal key As Integer) As String
        Return Me.StartTimeCaption(key, TimeStyle.LocalTime)
    End Function

    ''' <summary> Gets the start time caption. </summary>
    ''' <param name="key">       The key. </param>
    ''' <param name="timeStyle"> The time style. </param>
    ''' <returns> A String. </returns>
    Public Function StartTimeCaption(ByVal key As Integer, ByVal timeStyle As TimeStyle) As String
        If Me.Any(key) Then
            Return Me(key).StartTimeCaption(timeStyle)
        Else
            Return Me.MinimumTimeEon.StartTimeCaption(timeStyle)
        End If
    End Function

    ''' <summary> Gets the end time. </summary>
    ''' <param name="key"> The key. </param>
    ''' <returns> A DateTime. </returns>
    Public Function EndTime(ByVal key As Integer) As DateTime
        If Me.Any(key) Then
            Return Me(key).Last.ActiveEndTime
        Else
            Return Me.MinimumTimeEon.ActiveEndTime
        End If
    End Function

    ''' <summary> Elapsed time span. </summary>
    ''' <param name="key"> The key. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Function ElapsedTimeSpan(ByVal key As Integer) As TimeSpan
        Dim value As TimeSpan = TimeSpan.Zero
        If Me.Any(key) Then
            value = Me(key).ElapsedTimeSpan
        End If
        Return value
    End Function

    ''' <summary> Elapsed time caption. </summary>
    ''' <param name="key"> The key. </param>
    ''' <returns> A String. </returns>
    Public Function ElapsedTimeCaption(ByVal key As Integer) As String
        Return Eon.ToElapsedTimeCaption(Eon.DefaultElapsedTimeFormat, Me(key).ElapsedTimeSpan)
    End Function

End Class

''' <summary> Values that represent time styles. </summary>
Public Enum TimeStyle
    <Description("Local Time")> LocalTime
    <Description("Universal Time")> UniversalTime
End Enum
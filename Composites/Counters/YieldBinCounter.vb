﻿Imports System.Runtime.Serialization
Imports System.Collections.Concurrent
Imports System.Security.Permissions
Imports isr.Core.Pith.EnumExtensions
''' <summary> A yield bin counter. </summary>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="10/7/2016" by="David" revision=""> Created. </history>
<DebuggerDisplay("Total = {TotalCount}")>
Public Class YieldBinCounter
    Inherits isr.Core.Composites.YieldCounter

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    Public Sub New(ByVal binNumbers As IEnumerable(Of Integer), ByVal goodBinNumbers As IEnumerable(Of Integer),
                   ByVal failureBinNumbers As IEnumerable(Of Integer), ByVal invalidBinNumbers As IEnumerable(Of Integer))
        MyBase.New()
        Me._ResetKnownState(binNumbers, goodBinNumbers, failureBinNumbers, invalidBinNumbers)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As YieldBinCounter)
        Me.New(If(value Is Nothing, New Integer() {}, value.BinNumbers),
               If(value Is Nothing, New Integer() {}, value.BinNumbers),
               If(value Is Nothing, New Integer() {}, value.BinNumbers),
               If(value Is Nothing, New Integer() {}, value.BinNumbers))
        If value IsNot Nothing Then
            Me.IncrementRange(value)
            Me.BinNumber = value.BinNumber
        End If
    End Sub

#End Region

#Region " RESET AND CLEAR "

    ''' <summary> Clears to known (clear) state; Clears select values to their initial state. </summary>
    Public Overrides Sub ClearKnownState()
        Me._BinCountDictionary.ClearKnownState()
        MyBase.ClearKnownState()
    End Sub

    ''' <summary> Publishes this object. </summary>
    Public Overrides Sub Publish()
        Me.SafePostPropertyChanged(NameOf(YieldBinCounter.BinNumber))
    End Sub

    ''' <summary> Resets the know state. </summary>
    Private Sub _ResetKnownState(ByVal binNumbers As IEnumerable(Of Integer), ByVal goodBinNumbers As IEnumerable(Of Integer),
                                 ByVal failureBinNumbers As IEnumerable(Of Integer), ByVal InvalidBinNumbers As IEnumerable(Of Integer))
        Me._BinCountDictionary = New BinCountDictionary(binNumbers)
        Me._InitializeFailureBinNumbers(failureBinNumbers)
        Me._InitializeGoodBinNumbers(goodBinNumbers)
        Me._InitializeInvalidBinNumbers(InvalidBinNumbers)
        Me.ValidateBinNumbers()
    End Sub

    ''' <summary> Validates the bin numbers. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Private Sub ValidateBinNumbers()
        For Each binNumber As Integer In Me.BinNumbers
            If Me.GoodBinNumbers.Contains(binNumber) Then
            ElseIf Me.InvalidBinNumbers.Contains(binNumber) Then
            ElseIf Me.FailureBinNumbers.Contains(binNumber) Then
            Else
                Throw New InvalidOperationException($"Bin number validation failed because bin number {binNumber} not defined.")
            End If
        Next
    End Sub

#End Region

#Region " SPECIAL BINS "

    Private _GoodBinNumbers As List(Of Integer)
    ''' <summary> Gets the Good bin number. </summary>
    ''' <value> The Good bin number. </value>
    Public ReadOnly Property GoodBinNumbers As IEnumerable(Of Integer)
        Get
            Return Me._GoodBinNumbers
        End Get
    End Property

    Private Sub _InitializeGoodBinNumbers(ByVal goodBinNumbers As IEnumerable(Of Integer))
        Me._GoodBinNumbers = New List(Of Integer)(goodBinNumbers)
    End Sub

    ''' <summary> Initializes the Good bin numbers described by GoodBinNumbers. </summary>
    ''' <param name="GoodBinNumbers"> The Good bin number. </param>
    Public Sub InitializeGoodBinNumbers(ByVal goodBinNumbers As IEnumerable(Of Integer))
        Me._InitializeGoodBinNumbers(goodBinNumbers)
    End Sub

    Private _FailureBinNumbers As List(Of Integer)
    ''' <summary> Gets the Failure bin number. </summary>
    ''' <value> The Failure bin number. </value>
    Public ReadOnly Property FailureBinNumbers As IEnumerable(Of Integer)
        Get
            Return Me._FailureBinNumbers
        End Get
    End Property

    Private Sub _InitializeFailureBinNumbers(ByVal failureBinNumbers As IEnumerable(Of Integer))
        Me._FailureBinNumbers = New List(Of Integer)(failureBinNumbers)
    End Sub

    ''' <summary> Initializes the Failure bin numbers described by FailureBinNumbers. </summary>
    ''' <param name="FailureBinNumbers"> The Failure bin number. </param>
    Public Sub InitializeFailureBinNumbers(ByVal failureBinNumbers As IEnumerable(Of Integer))
        Me._InitializeFailureBinNumbers(failureBinNumbers)
    End Sub

    Private _InvalidBinNumbers As List(Of Integer)
    ''' <summary> Gets the invalid bin number. </summary>
    ''' <value> The invalid bin number. </value>
    Public ReadOnly Property InvalidBinNumbers As IEnumerable(Of Integer)
        Get
            Return Me._InvalidBinNumbers
        End Get
    End Property

    Private Sub _InitializeInvalidBinNumbers(ByVal invalidBinNumbers As IEnumerable(Of Integer))
        Me._InvalidBinNumbers = New List(Of Integer)(invalidBinNumbers)
    End Sub

    ''' <summary> Initializes the invalid bin numbers described by invalidBinNumbers. </summary>
    ''' <param name="invalidBinNumbers"> The invalid bin number. </param>
    Public Sub InitializeInvalidBinNumbers(ByVal invalidBinNumbers As IEnumerable(Of Integer))
        Me._InitializeInvalidBinNumbers(invalidBinNumbers)
    End Sub

#End Region

#Region " BIN COUNTER "

    Private _BinCountDictionary As BinCountDictionary

    ''' <summary> Gets a dictionary of bin counts. </summary>
    ''' <value> A Dictionary of bin counts. </value>
    Private ReadOnly Property BinCountDictionary As BinCountDictionary
        Get
            Return Me._BinCountDictionary
        End Get
    End Property

    ''' <summary> Gets the bin word. </summary>
    ''' <value> The bin word. </value>
    Public ReadOnly Property BinWord As Integer
        Get
            Return Me.BinCountDictionary.BinWord
        End Get
    End Property

    ''' <summary> Gets or sets the bin numbers. </summary>
    ''' <value> The bin numbers. </value>
    Public ReadOnly Property BinNumbers As IEnumerable(Of Integer)
        Get
            Return Me.BinCountDictionary.BinNumbers
        End Get
    End Property

    Public Sub IncrementBin(ByVal binNumber As Integer)
        Me.IncrementBin(binNumber, 1)
    End Sub

    ''' <summary> Increment bin. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="binNumber"> The bin number. </param>
    ''' <param name="count">     Number of. </param>
    Public Sub IncrementBin(ByVal binNumber As Integer, ByVal count As Integer)
        If Me.GoodBinNumbers.Contains(binNumber) Then
            ' good bin is incremented in the total count
        ElseIf Me.InvalidBinNumbers.Contains(binNumber) Then
            Me.InvalidCount += count
        ElseIf Me.FailureBinNumbers.Contains(binNumber) Then
            Me.FailedCount += count
        Else
            Throw New InvalidOperationException($"Bin number {binNumber} not defined.")
        End If
        Me.BinCountDictionary.Item(binNumber) += count
        Me.TotalCount += count
        Me.BinNumber = binNumber
    End Sub

    ''' <summary> Increment range. </summary>
    ''' <param name="counter"> The counter. </param>
    Public Sub IncrementRange(ByVal counter As YieldBinCounter)
        If counter Is Nothing Then Throw New ArgumentNullException(NameOf(counter))
        For Each binNumber As Integer In counter.BinNumbers
            Me.IncrementBin(binNumber, counter.BinCountDictionary(binNumber))
        Next
    End Sub

    ''' <summary> Decrement bin. </summary>
    ''' <param name="binNumber"> The bin number. </param>
    Public Sub DecrementBin(ByVal binNumber As Integer)
        Me.DecrementBin(binNumber, 1)
    End Sub

    ''' <summary> Decrement bin. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="binNumber"> The bin number. </param>
    ''' <param name="count">     Number of. </param>
    Public Sub DecrementBin(ByVal binNumber As Integer, ByVal count As Integer)
        If Me.GoodBinNumbers.Contains(binNumber) Then
            ' good bin is incremented in the total count
        ElseIf Me.InvalidBinNumbers.Contains(binNumber) Then
            If Me.InvalidCount >= count Then
                Me.InvalidCount -= count
            Else
                Throw New InvalidOperationException($"Invalid count {Me.InvalidCount } lower then the decrement {count} for bin number {binNumber}")
            End If
        ElseIf Me.FailureBinNumbers.Contains(binNumber) Then
            If Me.FailedCount >= count Then
                Me.FailedCount -= count
            Else
                Throw New InvalidOperationException($"Failed count {Me.FailedCount } lower then the decrement {count} for bin number {binNumber}")
            End If
        Else
            Throw New InvalidOperationException($"Bin number {binNumber} not defined.")
        End If
        If Me.BinCountDictionary.Item(binNumber) >= count Then
            Me.BinCountDictionary.Item(binNumber) -= count
        Else
            Throw New InvalidOperationException($"Bin count {Me.BinCountDictionary.Item(binNumber)} for bin number {binNumber} lower then the decrement {count}")
        End If
        If Me.TotalCount >= count Then
            Me.TotalCount -= count
        Else
            Throw New InvalidOperationException($"Total count {Me.TotalCount} is lower then the decrement {count} for bin number {binNumber} ")
        End If
        Me.BinNumber = binNumber
    End Sub

    ''' <summary> Decrement range. </summary>
    ''' <param name="counter"> The counter. </param>
    Public Sub DecrementRange(ByVal counter As YieldBinCounter)
        If counter Is Nothing Then Throw New ArgumentNullException(NameOf(counter))
        For Each binNumber As Integer In counter.BinNumbers
            Me.DecrementBin(binNumber, counter.BinCountDictionary(binNumber))
        Next
    End Sub

    ''' <summary> Gets or sets the number of bins. </summary>
    ''' <value> The number of bins. </value>
    Public Property BinCount(ByVal binNumber As Integer) As Integer
        Get
            Return Me.BinCountDictionary.Item(binNumber)
        End Get
        Set(value As Integer)
            Me.BinCountDictionary.Item(binNumber) = value
            Me.BinNumber = binNumber
        End Set
    End Property

    Private _BinNumber As Integer

    ''' <summary> Gets or sets the bin number. </summary>
    ''' <value> The bin number. </value>
    Public Property BinNumber As Integer
        Get
            Return Me._BinNumber
        End Get
        Set(value As Integer)
            Me._BinNumber = value
            Me.SafePostPropertyChanged()
        End Set
    End Property

#End Region

End Class

''' <summary> Dictionary of bin counts. </summary>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="7/14/2017" by="David" revision=""> Created. </history>
<Serializable>
Public Class BinCountDictionary
    Inherits System.Collections.Concurrent.ConcurrentDictionary(Of Integer, Integer)

    ''' <summary> Constructor. </summary>
    ''' <param name="enumeration"> An enum constant representing the enumeration option. </param>
    Public Sub New(ByVal enumeration As System.Enum)
        Me.New(enumeration.Values)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="binNumbers"> The bin numbers. </param>
    Public Sub New(ByVal binNumbers As IEnumerable(Of Integer))
        MyBase.New()
        Me._ResetKnownState(binNumbers)
    End Sub

    ''' <summary> Gets the bin numbers. </summary>
    ''' <value> The bin numbers. </value>
    Public ReadOnly Property BinNumbers As IEnumerable(Of Integer)
        Get
            Return Me.Keys
        End Get
    End Property

    ''' <summary> Resets the known state described by binNumbers. </summary>
    ''' <param name="binNumbers"> The bin numbers. </param>
    Private Sub _ResetKnownState(ByVal binNumbers As IEnumerable(Of Integer))
        For Each i As Integer In binNumbers
            Me.TryAdd(i, 0)
        Next
    End Sub

    ''' <summary> Clears the known state. </summary>
    Public Sub ClearKnownState()
        For Each key As Integer In Me.Keys
            Me(key) = 0
        Next
    End Sub

    ''' <summary> Resets the known state described by binNumbers. </summary>
    ''' <param name="binNumbers"> The bin numbers. </param>
    Public Sub ResetKnownState(ByVal binNumbers As IEnumerable(Of Integer))
        Me.Clear()
        Me._ResetKnownState(binNumbers)
    End Sub

    ''' <summary> Gets the bin word. </summary>
    ''' <value> The bin word. </value>
    Public ReadOnly Property BinWord As Integer
        Get
            Dim result As Integer = 0
            For Each value As KeyValuePair(Of Integer, Integer) In Me
                If value.Value > 0 Then
                    result = result Or (2 >> value.Key)
                End If
            Next
            Return result
        End Get
    End Property

End Class


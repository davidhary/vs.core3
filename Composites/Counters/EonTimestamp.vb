﻿
Partial Public Class Eon

#Region " TIMESTAMP "

    ''' <summary> Gets the local time format. </summary>
    ''' <value> The local time format. </value>
    Public Property DateFormatLocal As String

    ''' <summary> Gets the UTC time format. </summary>
    ''' <value> The UTC time format. </value>
    Public Property DateFormatUtc As String

    ''' <summary> Gets the local time format. </summary>
    ''' <value> The local time format. </value>
    Public Property TimeFormatLocal As String

    ''' <summary> Gets the UTC time format. </summary>
    ''' <value> The UTC time format. </value>
    Public Property TimeFormatUtc As String

    ''' <summary> Gets the local time format. </summary>
    ''' <value> The local time format. </value>
    Public Property DateTimeFormatLocal As String

    ''' <summary> Gets the UTC time format. </summary>
    ''' <value> The UTC time format. </value>
    Public Property DateTimeFormatUtc As String

    ''' <summary> The universal time suffix. </summary>
    Public Const UniversalTimeSuffix As String = "Z"

    Public Function DateFormat(ByVal timeStyle As TimeStyle) As String
        Return If(timeStyle = TimeStyle.UniversalTime, Me.DateFormatUtc, Me.DateFormatLocal)
    End Function

    Public Function TimeFormat(ByVal timeStyle As TimeStyle) As String
        Return If(timeStyle = TimeStyle.UniversalTime, Me.TimeFormatUtc, Me.TimeFormatLocal)
    End Function

    ''' <summary> Date Time format. </summary>
    ''' <param name="timeStyle"> The time style. </param>
    ''' <returns> A String. </returns>
    Public Function DateTimeFormat(ByVal timeStyle As TimeStyle) As String
        Return If(timeStyle = TimeStyle.UniversalTime, Me.DateTimeFormatUtc, Me.DateTimeFormatLocal)
    End Function

    ''' <summary> Default date time format. </summary>
    ''' <param name="timeStyle"> The time style. </param>
    ''' <returns> A String. </returns>
    Public Shared Function DefaultDateTimeFormat(ByVal timeStyle As TimeStyle) As String
        Return $"{Eon.DefaultTimestampFormat}{If(timeStyle = TimeStyle.UniversalTime, Eon.UniversalTimeSuffix, "")}"
    End Function

    ''' <summary> The default date and time format. </summary>
    Public Shared Property DefaultTimestampFormat As String = "yyyy/MM/dd HH:mm:ss"

    ''' <summary> The default time format. </summary>
    Public Shared Property DefaultDateFormat As String = "yyyy/MM/dd"

    ''' <summary> The default time format. </summary>
    Public Shared Property DefaultTimeFormat As String = "HH:mm:ss"

    Public Function StartTimeCaption(ByVal timeStyle As TimeStyle) As String
        Return Me.ToTimeCaption(If(timeStyle = TimeStyle.UniversalTime, Me.ActiveStartTime, Me.ActiveStartTime.ToLocalTime), timeStyle)
    End Function

    ''' <summary> Gets the start time caption. </summary>
    ''' <param name="timeStyle"> The time style. </param>
    ''' <returns> A String. </returns>
    Public Function StartDateTimeCaption(ByVal timeStyle As TimeStyle) As String
        Return Me.ToDateTimeCaption(If(timeStyle = TimeStyle.UniversalTime, Me.ActiveStartTime, Me.ActiveStartTime.ToLocalTime), timeStyle)
    End Function

    ''' <summary> Gets the End time caption. </summary>
    ''' <param name="timeStyle"> The time style. </param>
    ''' <returns> A String. </returns>
    Public Function EndDateTimeCaption(ByVal timeStyle As TimeStyle) As String
        Return Me.ToDateTimeCaption(If(timeStyle = TimeStyle.UniversalTime, Me.ActiveEndTime, Me.ActiveEndTime.ToLocalTime), timeStyle)
    End Function

    Public Function ToDateCaption(ByVal timestamp As Date, ByVal timeStyle As TimeStyle) As String
        Return Eon.ToTimestampCaption(Me.DateFormat(timeStyle), timestamp)
    End Function

    Public Function ToTimeCaption(ByVal timestamp As Date, ByVal timeStyle As TimeStyle) As String
        Return Eon.ToTimestampCaption(Me.TimeFormat(timeStyle), timestamp)
    End Function

    ''' <summary> Converts a timestamp to a caption. </summary>
    ''' <param name="timestamp"> The timestamp in the UTC time format if <paramref name="timeStyle"/>. </param>
    ''' <param name="timeStyle"> The time style. </param>
    ''' <returns> timestamp as a String. </returns>
    Public Function ToDateTimeCaption(ByVal timestamp As Date, ByVal timeStyle As TimeStyle) As String
        Return Eon.ToTimestampCaption(Me.DateTimeFormat(timeStyle), timestamp)
    End Function

    ''' <summary> Converts timestamp to date. </summary>
    ''' <param name="timestamp"> The timestamp Date/Time. </param>
    ''' <returns> A Date. </returns>
    Public Function FromDateTimeCaption(ByVal timestamp As String) As Date
        If String.IsNullOrWhiteSpace(timestamp) Then Throw New ArgumentNullException(NameOf(timestamp))
        Return Eon.FromTimestampCaption(If(timestamp.EndsWith(Eon.UniversalTimeSuffix, StringComparison.OrdinalIgnoreCase),
                                                   Me.DateTimeFormatUtc, Me.DateTimeFormatLocal), timestamp)
    End Function

    ''' <summary> Converts a timestamp to a caption. </summary>
    ''' <param name="timestamp"> The timestamp in the UTC time format if <paramref name="timeStyle"/>. </param>
    ''' <returns> timestamp as a String. </returns>
    Public Shared Function ToDefaultDateTimeCaption(ByVal timestamp As Date, ByVal timeStyle As TimeStyle) As String
        Return Eon.ToTimestampCaption(Eon.DefaultDateTimeFormat(timeStyle), timestamp)
    End Function

    ''' <summary> Converts timestamp to date. </summary>
    ''' <param name="timestamp"> The timestamp Date/Time. </param>
    ''' <returns> A Date. </returns>
    Public Shared Function FromDefaultDateTimeCaption(ByVal timestamp As String) As Date
        If String.IsNullOrWhiteSpace(timestamp) Then Throw New ArgumentNullException(NameOf(timestamp))
        Return Eon.FromTimestampCaption(Eon.DefaultTimestampFormat, timestamp)
    End Function

#End Region


End Class

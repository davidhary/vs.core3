﻿''' <summary> A yield bin counter. </summary>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="10/7/2016" by="David" revision=""> Created. </history>
<DebuggerDisplay("Total = {TotalCount}")>
Public Class YieldLimitBinCounter
    Inherits isr.Core.Composites.YieldCounter

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me._ResetKnownState()
        Me._ClearKnownState()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As YieldLimitBinCounter)
        MyBase.New(value)
        If value IsNot Nothing Then
            Me.GoodBinNumber = value.GoodBinNumber
            Me.HighBinNumber = value.HighBinNumber
            Me.LowBinNumber = value.LowBinNumber
            Me.LowerLimit = value.LowerLimit
            Me.UpperLimit = value.UpperLimit
            Me.BinNumber = value.BinNumber
            For Each kvp As KeyValuePair(Of Integer, Integer) In value.BinCountDictionary
                Me.BinCount(kvp.Key) = kvp.Value
            Next
        End If
    End Sub

#End Region

#Region " RESET AND CLEAR "

    ''' <summary> Clears to known (clear) state; Clears select values to their initial state. </summary>
    Private Sub _ClearKnownState()
        Me._BinCountDictionary = New Dictionary(Of Integer, Integer)
        For i As Integer = 0 To 30
            Me._BinCountDictionary.Add(i, 0)
        Next
    End Sub

    ''' <summary> Clears to known (clear) state; Clears select values to their initial state. </summary>
    Public Overrides Sub ClearKnownState()
        Me._ClearKnownState()
        MyBase.ClearKnownState()
    End Sub

    Public Overrides Sub Publish()
        Me.SafePostPropertyChanged(NameOf(YieldLimitBinCounter.BinNumber))
    End Sub

    ''' <summary> Resets the know state. </summary>
    Private Sub _ResetKnownState()
        Me._GoodBinNumber = 0
        Me._HighBinNumber = 1
        Me._LowBinNumber = 2
        Me._LowerLimit = -1
        Me._UpperLimit = 1
    End Sub

    ''' <summary> Resets the know state. </summary>
    Public Overrides Sub ResetKnownState()
        Me._ResetKnownState()
        MyBase.ResetKnownState()
    End Sub

#End Region

#Region " BIN COUNTER "

    Private _BinCountDictionary As Dictionary(Of Integer, Integer)

    ''' <summary> Gets a dictionary of bin counts. </summary>
    ''' <value> A Dictionary of bin counts. </value>
    Private ReadOnly Property BinCountDictionary As Dictionary(Of Integer, Integer)
        Get
            Return Me._BinCountDictionary
        End Get
    End Property

    ''' <summary> Gets or sets the number of bins. </summary>
    ''' <value> The number of bins. </value>
    Public Property BinCount(ByVal binNumber As Integer) As Integer
        Get
            Return Me.BinCountDictionary.Item(binNumber)
        End Get
        Set(value As Integer)
            Me.BinCountDictionary.Item(binNumber) = value
            Me.BinNumber = binNumber
        End Set
    End Property

    Private _BinNumber As Integer
    Public Property BinNumber As Integer
        Get
            Return Me._BinNumber
        End Get
        Set(value As Integer)
            Me._BinNumber = value
            Me.SafePostPropertyChanged()
        End Set
    End Property

    ''' <summary> Gets or sets the good bin number. </summary>
    ''' <value> The good bin number. </value>
    Public Property GoodBinNumber As Integer

    ''' <summary> Adds a value. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub AddValue(ByVal value As Double?)
        If value.HasValue Then
            Me.AddValue(value)
        Else
            Me.InvalidCount += 1
            Me.TotalCount += 1
        End If
    End Sub

#End Region

#Region " LIMIT BINS "

    ''' <summary> Gets or sets the high bin number. </summary>
    ''' <value> The high bin number. </value>
    Public Property HighBinNumber As Integer

    ''' <summary> Gets or sets the low bin number. </summary>
    ''' <value> The low bin number. </value>
    Public Property LowBinNumber As Integer

    ''' <summary> Gets the lower limit. </summary>
    ''' <value> The lower limit. </value>
    Public Property LowerLimit As Double

    ''' <summary> Gets the upper limit. </summary>
    ''' <value> The upper limit. </value>
    Public Property UpperLimit As Double

    ''' <summary> Adds a value. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub AddValue(ByVal value As Double)
        Dim bin As Integer
        If value > Me.UpperLimit Then
            bin = Me.HighBinNumber
        ElseIf value < Me.LowerLimit Then
            bin = Me.LowBinNumber
        Else
            bin = Me.GoodBinNumber
        End If
        Me.BinCountDictionary.Item(bin) += 1
        Me.TotalCount += 1
    End Sub

#End Region

End Class

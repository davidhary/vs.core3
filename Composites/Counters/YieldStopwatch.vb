﻿Imports System.Timers
''' <summary> A yield counter using a stopwatch and optional timer. </summary>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="10/8/2016" by="David" revision=""> Created. </history>
<DebuggerDisplay("Total = {TotalCount}")>
Public Class YieldStopwatch
    Inherits YieldCounter

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me._ResetKnownState()
        Me._ClearKnownState()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As YieldStopwatch)
        MyBase.New(value)
        If value IsNot Nothing Then
            Me._DateFormatUtc = value.DateFormatUtc
            Me._DateFormatLocal = value.DateFormatLocal
            Me._TimeFormatUtc = value.TimeFormatUtc
            Me._TimeFormatLocal = value.TimeFormatLocal
            Me._DateTimeFormatUtc = value.DateTimeFormatUtc
            Me._DateTimeFormatLocal = value.DateTimeFormatLocal
            Me._ElapsedTimeFormat = value.ElapsedTimeFormat
            Me._InitializeKnownState(value.StartTime)
            Me.Interval = value.Interval
            Me._Stopwatch = New OffsetStopwatch(value.Stopwatch)
            If value.Stopwatch.IsRunning Then
            Else
                Me.Stopwatch.Reset()
            End If
        End If
    End Sub

    ''' <summary> Creates a new Yield Stopwatch. </summary>
    ''' <remarks> Helps implement CA2000. </remarks>
    ''' <returns> A  <see cref="YieldStopwatch"/>. </returns>
    Public Overloads Shared Function Create() As YieldStopwatch
        Dim result As YieldStopwatch = Nothing
        Try
            result = New YieldStopwatch
        Catch
            result?.Dispose()
            Throw
        End Try
        Return result
    End Function

#End Region

#Region " PUBLISH "

    ''' <summary> Publishes this object. </summary>
    Public Overrides Sub Publish()
        MyBase.Publish()
        Me.SafePostPropertyChanged(NameOf(YieldStopwatch.StartTime))
        Me.SafePostPropertyChanged(NameOf(YieldStopwatch.EndTime))
    End Sub

#End Region

#Region " RESET AND CLEAR "

    ''' <summary> Clears to known (clear) state; Clears select values to their initial state. </summary>
    Private Sub _ClearKnownState()
        Me._StartTime = DateTime.MinValue
        Me._Stopwatch = New OffsetStopwatch(TimeSpan.Zero)
    End Sub

    ''' <summary> Clears to known (clear) state; Clears select values to their initial state. </summary>
    Public Overrides Sub ClearKnownState()
        MyBase.ClearKnownState()
        ' if clearing and value cleared, we need to publish to make sure displays get updated.
        Me._ClearKnownState()
        Me.Publish()
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    Private Sub _ResetKnownState()
        Me._ClearKnownState()
        Me._Timer = New Timers.Timer()
        Me._Timer.Stop()
        Me._DateFormatLocal = YieldStopwatch.DefaultDateFormat
        Me._DateFormatUtc = YieldStopwatch.DefaultDateFormat & YieldStopwatch.UniversalTimeSuffix
        Me._TimeFormatLocal = YieldStopwatch.DefaultTimeFormat
        Me._TimeFormatUtc = YieldStopwatch.DefaultTimeFormat & YieldStopwatch.UniversalTimeSuffix
        Me._DateTimeFormatLocal = YieldStopwatch.DefaultTimestampFormat
        Me._DateTimeFormatUtc = YieldStopwatch.DefaultTimestampFormat & YieldStopwatch.UniversalTimeSuffix
        Me._ElapsedTimeFormat = YieldStopwatch.DefaultElapsedTimeFormat
    End Sub

    ''' <summary> Resets the know state. </summary>
    Public Overrides Sub ResetKnownState()
        MyBase.ResetKnownState()
        Me._ResetKnownState()
        Me.Publish()
    End Sub

    ''' <summary> Initializes to known (Initialize) state; Initializes select values to their initial
    ''' state. </summary>
    ''' <param name="startTime"> The start time. </param>
    Private Sub _InitializeKnownState(ByVal startTime As DateTime)
        Me._ClearKnownState()
        Me._StartTime = startTime
    End Sub


    ''' <summary> Initializes to known (Initialize) state; Initializes select values to their initial
    ''' state. </summary>
    ''' <param name="startTime"> The start time. </param>
    Public Sub InitializeKnownState(ByVal startTime As DateTime)
        Me._InitializeKnownState(startTime)
    End Sub

    ''' <summary> Initializes to known (Initialize) state; Initializes select values to their initial state. </summary>
    Public Sub InitializeKnownState()
        Me.InitializeKnownState(DateTime.UtcNow)
    End Sub

#End Region

#Region " TIME ELEMENTS "

    Public Property Stopwatch As OffsetStopwatch

    ''' <summary> Gets the is started. </summary>
    ''' <value> The is started. </value>
    Public ReadOnly Property IsStarted As Boolean
        Get
            Return Me.StartTime > DateTime.MinValue
        End Get
    End Property

    ''' <summary> Gets or sets the start time. </summary>
    ''' <value> The start time. </value>
    Public ReadOnly Property StartTime As DateTime

    ''' <summary> Gets the end time. </summary>
    ''' <value> The end time. </value>
    Public ReadOnly Property EndTime As DateTime
        Get
            Return Me.StartTime.Add(Me.Stopwatch.Elapsed)
        End Get
    End Property

#End Region

#Region " TIME ACTIONS "

    ''' <summary> Resets and starts. </summary>
    Public Sub Start()
        Me.InitializeKnownState()
        Me.Stopwatch.Start()
        If Me.CapturedSyncContext IsNot Nothing AndAlso Me.Interval > TimeSpan.Zero Then Me._Timer.Start()
        Me.SafePostPropertyChanged(NameOf(YieldStopwatch.StartTime))
    End Sub

    ''' <summary> Finishes measuring elapsed time. </summary>
    Public Sub Finish()
        Me._Timer.Stop()
        Me.Stopwatch.Stop()
        Me.SafePostPropertyChanged(NameOf(YieldStopwatch.EndTime))
    End Sub

    ''' <summary> Resumes counting after a <see cref="Finish()">stop</see>. </summary>
    Public Sub [Resume]()
        If Me.CapturedSyncContext IsNot Nothing AndAlso Me.Interval > TimeSpan.Zero Then Me._Timer.Start()
        If Not Me.Stopwatch.IsRunning Then Me.Stopwatch.Start()
    End Sub

#End Region

#Region " RATE "

    ''' <summary> Hourly rate. </summary>
    ''' <returns> A Double. </returns>
    Public Overloads Function HourlyRate() As Double
        Return Me.HourlyRate(Me.Stopwatch.Elapsed)
    End Function

#End Region

#Region " TIMER "

    Private WithEvents _Timer As Timers.Timer

    ''' <summary> Gets the interval. </summary>
    ''' <value> The interval. </value>
    Public Property Interval As TimeSpan
        Get
            Return TimeSpan.FromMilliseconds(Me._Timer.Interval)
        End Get
        Set(value As TimeSpan)
            Me._Timer.Interval = value.TotalMilliseconds
        End Set
    End Property

    ''' <summary> Timer elapsed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Elapsed event information. </param>
    Private Sub _Timer_Elapsed(sender As Object, e As ElapsedEventArgs) Handles _Timer.Elapsed
        If Threading.SynchronizationContext.Current Is Nothing AndAlso Me.CapturedSyncContext IsNot Nothing Then
            Me.ApplyCapturedSyncContext()
            If Threading.SynchronizationContext.Current IsNot Nothing Then
                Me.SafePostPropertyChanged(NameOf(YieldStopwatch.ElapsedTimeCaption))
            End If
        End If
    End Sub

#End Region

End Class


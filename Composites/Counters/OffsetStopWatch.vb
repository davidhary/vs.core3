﻿''' <summary> A stop watch with Offset preset. </summary>
''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="05/06/2009" by="David" revision="1.1.3413.x"> Created. </history>
<DebuggerDisplay("Elapsed = {Elapsed}")>
Public Class OffsetStopwatch
    Inherits Diagnostics.Stopwatch

    ''' <summary> Constructs a new Offset stop watch with the specified offset (initial elapsed time)
    ''' <paramref name="duration">duration</paramref> </summary>
    ''' <param name="duration"> Specifies the stop watch Offset duration. </param>
    Public Sub New(ByVal duration As TimeSpan)
        MyBase.New()
        Me._InitialElapsedTimespan = duration
    End Sub

    ''' <summary> Constructs a new timeout stop watch using the specified stopwatch elapsed time as offset. </summary>
    ''' <param name="stopwatch"> The stopwatch. </param>
    Public Sub New(ByVal stopwatch As Stopwatch)
        Me.New(If(stopwatch Is Nothing, TimeSpan.Zero, stopwatch.Elapsed))
    End Sub

    ''' <summary> Constructs a new Offset stop watch with the specified
    ''' <paramref name="duration">duration</paramref> </summary>
    ''' <param name="duration"> Specifies the stop watch duration. </param>
    ''' <returns>Returns true if Offset is done.</returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix",
        Justification:="Must be the same name as in the base class")>
    Public Overloads Shared Function StartNew(ByVal duration As TimeSpan) As OffsetStopwatch
        Dim osw As New OffsetStopwatch(duration)
        osw.Start()
        Return osw
    End Function

    ''' <summary> Gets or sets the initial elapsed time. </summary>
    ''' <value> <c>Duration</c>is a TimeSpan property. </value>
    Public Property InitialElapsedTimespan() As TimeSpan

    ''' <summary> Gets the elapsed timespan including the initial offset time. </summary>
    ''' <returns> A TimeSpan. </returns>
    Public Shadows Function Elapsed() As TimeSpan
        Return Me.NetElapsed.Add(Me.InitialElapsedTimespan)
    End Function

    ''' <summary> Net elapsed time (without offset). </summary>
    ''' <returns> A TimeSpan. </returns>
    Public Function NetElapsed() As TimeSpan
        Return MyBase.Elapsed
    End Function

End Class

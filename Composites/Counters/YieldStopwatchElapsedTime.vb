﻿Partial Public Class YieldStopwatch

#Region " ELAPSED TIME "

    ''' <summary> Gets or sets the default elapsed time format. </summary>
    ''' <value> The default elapsed time format. </value>
    Public Shared Property DefaultElapsedTimeFormat As String = "d\.hh\:mm\:ss\.fff"

    ''' <summary> Gets or sets the ElapsedTime format. </summary>
    ''' <value> The ElapsedTime format. </value>
    Public Property ElapsedTimeFormat As String

    ''' <summary> Gets the start time caption. </summary>
    ''' <value> The start time caption. </value>
    Public ReadOnly Property ElapsedTimeCaption As String
        Get
            Return Me.ToElapsedTimeCaption(Me.Stopwatch.Elapsed)
        End Get
    End Property

    ''' <summary> Converts a ElapsedTime to a caption. </summary>
    ''' <param name="ElapsedTime"> The ElapsedTime Date/Time. </param>
    ''' <returns> ElapsedTime as a String. </returns>
    Public Function ToElapsedTimeCaption(ByVal elapsedTime As TimeSpan) As String
        Return Eon.ToElapsedTimeCaption(Me.ElapsedTimeFormat, elapsedTime)
    End Function

    ''' <summary> Converts ElapsedTime to date. </summary>
    ''' <param name="ElapsedTime"> The ElapsedTime Date/Time. </param>
    ''' <returns> A Date. </returns>
    Public Function FromElapsedTimeCaption(ByVal elapsedTime As String) As TimeSpan
        Return Eon.FromElapsedTimeCaption(Me.ElapsedTimeFormat, elapsedTime)
    End Function

    ''' <summary> Converts a ElapsedTime to a caption. </summary>
    ''' <param name="ElapsedTime"> The ElapsedTime Date/Time. </param>
    ''' <returns> ElapsedTime as a String. </returns>
    Public Shared Function ToDefaultElapsedTimeCaption(ByVal elapsedTime As TimeSpan) As String
        Return Eon.ToElapsedTimeCaption(Eon.DefaultElapsedTimeFormat, elapsedTime)
    End Function

    ''' <summary> Converts ElapsedTime to date. </summary>
    ''' <param name="ElapsedTime"> The ElapsedTime Date/Time. </param>
    ''' <returns> A Date. </returns>
    Public Shared Function FromDefaultElapsedTimeCaption(ByVal elapsedTime As String) As TimeSpan
        Return Eon.FromElapsedTimeCaption(Eon.DefaultElapsedTimeFormat, elapsedTime)
    End Function

#End Region

End Class

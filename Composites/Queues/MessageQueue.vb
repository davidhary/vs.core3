﻿''' <summary> Implements a queue for holding messages. </summary>
''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/24/2009" by="David" revision="1.1.3401.x"> Created. </history>
<DebuggerDisplay("Count = {Count}")>
Public Class MessageQueue
    Inherits Queue(Of String)

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me._queueLocker = New Object
    End Sub

    ''' <summary> Copy constructor. </summary>
    ''' <param name="value"> Specifies the message to enqueue. </param>
    Public Sub New(ByVal value As MessageQueue)
        Me.New()
        If value Is Nothing Then Return
        SyncLock Me._queueLocker
            MyBase.Clear()
            Do While value.Any
                MyBase.Enqueue(value.Dequeue)
            Loop
            MyBase.Reverse()
        End SyncLock
    End Sub

    ''' <summary> Clears the messages. </summary>
    Public Sub ClearMessages()
        SyncLock Me._queueLocker
            MyBase.Clear()
        End SyncLock
    End Sub

    Private _QueueLocker As Object

    ''' <summary> Dequeues a single queue element. </summary>
    ''' <returns> A String. </returns>
    Public Function DequeueAll() As String
        SyncLock Me._queueLocker
            If Not MyBase.Any Then
                Return ""
            End If
            Dim messages As New System.Text.StringBuilder
            Do While MyBase.Any
                If messages.Length > 0 Then
                    messages.AppendLine()
                End If
                messages.Append(MyBase.Dequeue)
            Loop
            Return messages.ToString
        End SyncLock
    End Function

    ''' <summary> Thread save enqueue. </summary>
    ''' <param name="value"> Specifies the message to enqueue. </param>
    ''' <returns> A String. </returns>
    Private Function _Enqueue(ByVal value As String) As String
        SyncLock Me._queueLocker
            MyBase.Enqueue(value)
        End SyncLock
        Return value
    End Function

    ''' <summary> Enqueues a message. </summary>
    ''' <param name="value"> Specifies the message to enqueue. </param>
    ''' <returns> A String. </returns>
    Public Overloads Function Enqueue(ByVal value As String) As String
        Return Me._enqueue(value)
    End Function

    ''' <summary> Enqueues a message. </summary>
    ''' <param name="format"> Specifies the message format. </param>
    ''' <param name="args">   Specifies the message arguments. </param>
    ''' <returns> A String. </returns>
    Public Overloads Function Enqueue(ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Me._enqueue(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Function

    ''' <summary> Returns all messages without clearing the queue. </summary>
    ''' <returns> A String. </returns>
    Public Overrides Function ToString() As String
        SyncLock Me._queueLocker
            If Not MyBase.Any Then
                Return ""
            End If
            Dim builder As New System.Text.StringBuilder
            If MyBase.Any Then
                Dim messages(MyBase.Count - 1) As String
                MyBase.CopyTo(messages, 0)
                For Each item As String In messages
                    If Not String.IsNullOrWhiteSpace(item) Then
                        If builder.Length > 0 Then
                            builder.AppendLine()
                        End If
                        builder.Append(item)
                    End If
                Next
                Return builder.ToString
            Else
                Return ""
            End If
        End SyncLock
    End Function

End Class

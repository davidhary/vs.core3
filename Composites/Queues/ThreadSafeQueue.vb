﻿Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics.CodeAnalysis
Imports System.Threading
Imports isr.Core.Pith

''' <summary>Represents a first-in, first-out collection of objects that is thread safe.</summary>
''' <typeparam name="T">Specifies the type of elements in the queue.</typeparam>
<SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix")>
<SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix")>
<DebuggerDisplay("Count = {Count}")>
Public Class ThreadSafeQueue(Of T)
    Implements IEnumerable(Of T), ICollection, IDisposable

#Region "Constructor"
    ''' <summary>
    ''' Initializes a new instance of the <see cref="ThreadSafeQueue"/> class that is empty and has a default initial capacity.
    ''' </summary>
    Public Sub New()
        MyBase.New()
        Me._Queue = New Queue(Of T)()
    End Sub

    ''' <summary>
    ''' Initialized a new instance of the <see cref="ThreadSafeQueue"/> class that contains elements copied from the specified collection and has sufficient capacity to accommodate the number of elements copied.
    ''' </summary>
    ''' <param name="collection">The collection whose elements are copied to the new <see cref="T:System.Collections.Generic.Queue`1" />.</param>
    ''' <exception cref="T:System.ArgumentNullException">
    ''' <paramref name="collection" /> is null.</exception>
    <SuppressMessage("ReSharper", "PossibleMultipleEnumeration")>
    Public Sub New(ByVal collection As IEnumerable(Of T))
        MyBase.New()
        Ensure.IsNotNull(NameOf(ThreadSafeQueue(Of T).collection), collection)
        Me._Queue = New Queue(Of T)(collection)
    End Sub

    ''' <summary>Initializes a new instance of the <see cref="ThreadSafeQueue" /> class that is empty and has the specified initial capacity.</summary>
    ''' <param name="capacity">The initial number of elements that the <see cref="ThreadSafeQueue" /> can contain.</param>

    ''' <exception cref="T:System.ArgumentOutOfRangeException">
    ''' <paramref name="capacity" /> is less than zero.</exception>
    Public Sub New(ByVal capacity As Integer)
        MyBase.New()
        Ensure.IsInRange(NameOf(ThreadSafeQueue(Of T)capacity), capacity >= 0)
        Me._Queue = New Queue(Of T)(capacity)
    End Sub

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If Me._ItemsLocker IsNot Nothing Then Me._ItemsLocker.Dispose() : Me._ItemsLocker = Nothing
            End If
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

#End Region

#Region " THREAD SYNC MANAGEMENT  "

    ''' <summary> Gets the items locker. </summary>
    ''' <value> The items locker. </value>
    Protected ReadOnly Property ItemsLocker As New ReaderWriterLockSlim()

    Private _SyncRoot As Object

#If False Then
    Private _SyncRoot As Object
    Protected ReadOnly Property SyncRoot() As Object Implements ICollection.SyncRoot
        Get
            If Me._syncRoot Is Nothing Then
                Interlocked.CompareExchange(Of Object)(Me._syncRoot, New Object(), Nothing)
            End If
            Return Me._syncRoot
        End Get
    End Property
#End If

    ''' <summary> Gets the synchronization root. </summary>
    ''' <value> The synchronization root. </value>
    ''' <remarks> The Sync root helps super classes synchronously access the items. 
    '''           For details see reply by Roman Zavalov in https://stackoverflow.com/questions/728896/whats-the-use-of-the-syncroot-pattern
    ''' </remarks>
    Public ReadOnly Property SyncRoot() As Object Implements ICollection.SyncRoot
        Get
            If Me._syncRoot Is Nothing Then
                Me.ItemsLocker.EnterReadLock()
                Try
                    Dim c As ICollection = TryCast(Me.Queue, ICollection)
                    If c IsNot Nothing Then
                        Me._syncRoot = c.SyncRoot
                    Else
                        Interlocked.CompareExchange(Of [Object])(Me._syncRoot, New [Object](), Nothing)
                    End If
                Finally
                    Me.ItemsLocker.ExitReadLock()
                End Try
            End If
            Return Me._syncRoot
        End Get
    End Property

#End Region

#Region " QUEUE IMPLEMENTATION "

    Private ReadOnly Property Queue As Queue(Of T)

    ''' <summary>
    ''' Gets the number of items contained in the <see cref="ThreadSafeQueue"/>
    ''' </summary>
    Public ReadOnly Property Count() As Integer Implements ICollection.Count
        Get
            Me.ItemsLocker.EnterReadLock()
            Try
                Return Me.Queue.Count
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End Get
    End Property

    ''' <summary> Gets the is synchronized. </summary>
    ''' <value> The is synchronized. </value>
    Public ReadOnly Property IsSynchronized As Boolean Implements ICollection.IsSynchronized
        Get
            Return True ' original was false?
        End Get
    End Property

    ''' <summary>
    ''' Removes all objects from the <see cref="ThreadSafeQueue" />.
    ''' </summary>
    Public Sub Clear()
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.Queue.Clear()
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary>Determines whether an element is in the <see cref="ThreadSafeQueue" />.</summary>
    ''' <returns>true if <paramref name="item" /> is found in the <see cref="ThreadSafeQueue" />; otherwise, false.</returns>
    ''' <param name="item">The object to locate in the <see cref="ThreadSafeQueue" />. The value can be null for reference types.</param>
    Public Function Contains(ByVal item As T) As Boolean
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Queue.Contains(item)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    ''' <summary>Copies the <see cref="ThreadSafeQueue" /> elements to an existing one-dimensional <see cref="T:System.Array" />, starting at the specified array index.</summary>
    ''' <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="ThreadSafeQueue" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
    ''' <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins.</param>
    ''' <exception cref="T:System.ArgumentNullException">
    ''' <paramref name="array" /> is null.</exception>
    ''' <exception cref="T:System.ArgumentOutOfRangeException">
    ''' <paramref name="arrayIndex" /> is less than zero.</exception>
    ''' <exception cref="T:System.ArgumentException">The number of elements in the source <see cref="ThreadSafeQueue" /> is greater than the available space from <paramref name="arrayIndex" /> to the end of the destination <paramref name="array" />.</exception>
    <SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="0")>
    Public Sub CopyTo(ByVal array() As T, ByVal arrayIndex As Integer)
        Ensure.IsNotNull(NameOf(array), array)
        Ensure.IsInRange(NameOf(arrayIndex), arrayIndex >= 0 AndAlso arrayIndex < array.Length)
        Ensure.IsValid(Of ArgumentException)(NameOf(arrayIndex), array.Length - arrayIndex >= Count, "Invalid offset length.")
        Me.ItemsLocker.EnterReadLock()
        Try
            Me.Queue.CopyTo(array, arrayIndex)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Sub

    'TODO: ThreadSafeQueue<T> ICollection.CopyTo(Array, Integer) - Implement
    Private Sub ICollection_CopyTo(ByVal array As Array, ByVal index As Integer) Implements ICollection.CopyTo
        Throw New NotImplementedException("Hope you didn't need this :)")
        'if (array == null)
        '    ThrowHelper.ThrowArgumentNullException(ExceptionArgument.array);
        'if (array.Rank != 1)
        '    ThrowHelper.ThrowArgumentException(ExceptionResource.Arg_RankMultiDimNotSupported);
        'if (array.GetLowerBound(0) != 0)
        '    ThrowHelper.ThrowArgumentException(ExceptionResource.Arg_NonZeroLowerBound);
        'int length1 = array.Length;
        'if (index < 0 || index > length1)
        '    ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.index, ExceptionResource.ArgumentOutOfRange_Index);
        'if (length1 - index < this._size)
        '    ThrowHelper.ThrowArgumentException(ExceptionResource.Argument_InvalidOffLen);
        'int n as integer = length1 - index < this._size ? length1 - index : this._size;
        'if (n == 0)
        '    return;
        'try
        '{
        '    int length2 = this._array.Length - this._head < n ? this._array.Length - this._head : n;
        '    Array.Copy((Array)this._array, this._head, array, index, length2);
        '    int length3 = n - length2;
        '    if (length3 <= 0)
        '        return;
        '    Array.Copy((Array)this._array, 0, array, index + this._array.Length - this._head, length3);
        '}
        'catch (ArrayTypeMismatchException ex)
        '{
        '    ThrowHelper.ThrowArgumentException(ExceptionResource.Argument_InvalidArrayType);
        '}
    End Sub

    ''' <summary>Removes and returns the object at the beginning of the <see cref="ThreadSafeQueue" />.</summary>
    ''' <returns>The object that is removed from the beginning of the <see cref="ThreadSafeQueue" />.</returns>
    ''' <exception cref="T:System.InvalidOperationException">The <see cref="ThreadSafeQueue" /> is empty.</exception>
    Public Function Dequeue() As T
        Ensure.IsValid(Of InvalidOperationException)(NameOf(Count), Count > 0, $"'{NameOf(Count)}' is less than or equal to zero.")
        Me.ItemsLocker.EnterWriteLock()
        Try
            Return Me.Queue.Dequeue()
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Function

    ''' <summary>Adds an object to the end of the <see cref="ThreadSafeQueue" />.</summary>
    ''' <param name="item">The object to add to the <see cref="ThreadSafeQueue" />. The value can be null for reference types.</param>
    Public Sub Enqueue(ByVal item As T)
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.Queue.Enqueue(item)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Enqueue range. </summary>
    ''' <param name="items"> The items. </param>
    <SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="0")>
    <SuppressMessage("ReSharper", "PossibleMultipleEnumeration")>
    Public Sub EnqueueRange(ByVal items As IEnumerable(Of T))
        Ensure.IsNotNull(NameOf(items), items)
        Me.ItemsLocker.EnterWriteLock()
        Try
            For Each item As T In items
                Me.Queue.Enqueue(item)
            Next item
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary>Returns an enumerator that iterates through the <see cref="ThreadSafeQueue" />.</summary>
    ''' <returns>An <see cref="T:System.Collections.Generic.Queue`1.Enumerator" /> for the <see cref="ThreadSafeQueue" />.</returns>
    Public Function GetEnumerator() As Queue(Of T).Enumerator
        Me.ItemsLocker.EnterReadLock()
        Try
            Return (New Queue(Of T)(_Queue)).GetEnumerator()
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    ' ReSharper disable RedundantCast
    <ExcludeFromCodeCoverage>
    Private Function IEnumerableGeneric_GetEnumerator() As IEnumerator(Of T) Implements IEnumerable(Of T).GetEnumerator
        Return CType(GetEnumerator(), IEnumerator(Of T))
    End Function

    <ExcludeFromCodeCoverage>
    Private Function IEnumerable_GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator
        Return CType(GetEnumerator(), IEnumerator)
    End Function

    ''' <summary>Returns the object at the beginning of the <see cref="ThreadSafeQueue" />.</summary>
    ''' <returns>The object at the beginning of the <see cref="ThreadSafeQueue" />.</returns>
    ''' <exception cref="T:System.InvalidOperationException">The <see cref="ThreadSafeQueue" /> is empty.</exception>
    Public Function Peek() As T
        Ensure.IsValid(Of InvalidOperationException)(NameOf(Count), Count > 0, $"'{NameOf(Count)}' is less than or equal to zero.")
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Queue.Peek()
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    ''' <summary>
    ''' Sets the capacity to the actual number of elements in the <see cref="ThreadSafeQueue" />, if that number is less than 90 percent of current capacity.
    ''' </summary>
    Public Sub TrimExcess()
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.Queue.TrimExcess()
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    <SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function TryDequeue(<System.Runtime.InteropServices.Out()> ByRef item As T) As Boolean
        item = Nothing
        Me.ItemsLocker.EnterUpgradeableReadLock()
        Try
            If Count <= 0 Then
                Return False
            End If
            Me.ItemsLocker.EnterWriteLock()
            Try
                item = Me.Queue.Dequeue()
                Return True
            Catch e1 As Exception
                Return False
            End Try
        Finally
            Me.ItemsLocker.ExitUpgradeableReadLock()
        End Try
    End Function

#End Region

End Class

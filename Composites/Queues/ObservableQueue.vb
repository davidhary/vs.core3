﻿Imports System.Collections.Specialized

''' <summary> An Observable Queue. </summary>
''' <remarks> BINDING NOTE: The data grid view does not bind to this collection even though it implements the required interfaces. </remarks>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/7/2016" by="David" revision=""> Created. </history>
<DebuggerDisplay("Count = {Count}")>
Public Class ObservableQueue(Of T)
    Inherits System.Collections.Generic.Queue(Of T)
    Implements INotifyCollectionChanged, IEnumerable(Of T)

    Public Event CollectionChanged As NotifyCollectionChangedEventHandler Implements INotifyCollectionChanged.CollectionChanged
    ' Private ReadOnly queue As New Queue(Of T)()

    ''' <summary> Adds an object onto the end of this queue. </summary>
    ''' <param name="item"> The item. </param>
    Public Overloads Sub Enqueue(ByVal item As T)
        MyBase.Enqueue(item)
        Dim evt As NotifyCollectionChangedEventHandler = Me.CollectionChangedEvent
        If evt IsNot Nothing Then evt.Invoke(Me, New NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item))
        ' RaiseEvent CollectionChanged(Me, New NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item))
    End Sub

    ''' <summary> Removes the head object from this queue. </summary>
    ''' <returns> The head object from this queue. </returns>
    Public Overloads Function Dequeue() As T
        Dim item As T = MyBase.Dequeue()
        Dim evt As NotifyCollectionChangedEventHandler = Me.CollectionChangedEvent
        If evt IsNot Nothing Then evt.Invoke(Me, New NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item))
        ' RaiseEvent CollectionChanged(Me, New NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item))
        Return item
    End Function

    ''' <summary> Returns an enumerator that iterates through the collection. </summary>
    ''' <returns> An enumerator that can be used to iterate through the collection. </returns>
    Public Overloads Function GetEnumerator() As IEnumerator(Of T) Implements IEnumerable(Of T).GetEnumerator
        Return MyBase.GetEnumerator()
    End Function

    Private Function IEnumerable_GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator
        Return GetEnumerator()
    End Function

End Class


''' <summary> An Observable Queue based the observable collection. </summary>
''' <remarks> BINDING NOTE: The data grid view does not bind to this collection even though it implements the required interfaces. </remarks>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/7/2016" by="David" revision=""> Created. </history>
<CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix")>
<CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix")>
Public Class ObservableCollectionQueue(Of T)
    Inherits ObjectModel.ObservableCollection(Of T)

    ''' <summary> Adds an object onto the end of this queue. </summary>
    ''' <param name="item"> The item. </param>
    Public Sub Enqueue(ByVal item As T)
        Me.Add(item)
    End Sub

    ''' <summary> Removes the head object from this queue. </summary>
    ''' <returns> The head object from this queue. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="item")>
    Public Function Dequeue() As T
        Dim item As T = MyBase.Item(0)
        Me.RemoveAt(0)
    End Function

    Public Function Peek() As T
        Return Me.Item(0)
    End Function

End Class

''' <summary> An Observable Queue based a generic list. </summary>
''' <remarks> BINDING NOTE: The data grid view does not bind to this collection even though it implements the required interfaces. </remarks>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/7/2016" by="David" revision=""> Created. </history>
<CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix")>
<CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix")>
Public Class ObservableListQueue(Of T)
    Inherits List(Of T)

    ''' <summary> Adds an object onto the end of this queue. </summary>
    ''' <param name="item"> The item. </param>
    Public Sub Enqueue(ByVal item As T)
        Me.Add(item)
    End Sub

    ''' <summary> Removes the head object from this queue. </summary>
    ''' <returns> The head object from this queue. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="item")>
    Public Function Dequeue() As T
        Dim item As T = MyBase.Item(0)
        Me.RemoveAt(0)
    End Function


    Public Function Peek() As T
        Return Me.Item(0)
    End Function

End Class

﻿Imports System.Threading
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports isr.Core.Pith
''' <summary> A synchronized observable collection </summary>
''' <remarks> TO_DO: Modify based on the Observable Keyed Collection. </remarks>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="5/2/2017" by="David" revision=""> Created. </history>
<Serializable, DebuggerDisplay("Count = {Count}")>
Public Class SynchronizedObservableCollection(Of T)
    Inherits isr.Core.Pith.PropertyNotifyBase
    Implements IDisposable, IList(Of T), IList, IReadOnlyList(Of T), INotifyCollectionChanged, INotifyPropertyChanged

#Region " CONSTRUCTOR "

    Public Sub New()
        _Context = SynchronizationContext.Current
    End Sub

    Public Sub New(ByVal collection As IEnumerable(Of T))
        Me.New()
        If collection Is Nothing Then
            Throw New ArgumentNullException(NameOf(collection), "'collection' cannot be null")
        End If

        For Each item As T In collection
            Me._Items.Add(item)
        Next
    End Sub

    Public Sub New(ByVal context As SynchronizationContext)
        Me._Context = context
    End Sub

    Public Sub New(ByVal collection As IEnumerable(Of T), ByVal context As SynchronizationContext)
        Me.New(context)
        If collection Is Nothing Then
            Throw New ArgumentNullException(NameOf(collection), "'collection' cannot be null")
        End If
        For Each item As T In collection
            Me._Items.Add(item)
        Next
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:Collection" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                                                   release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not MyBase.IsDisposed Then
                If Me._ItemsLocker IsNot Nothing Then Me._ItemsLocker.Dispose() : Me._ItemsLocker = Nothing
            End If
        Catch
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " NOTIFICATIONS "

    Private ReadOnly Property Context As SynchronizationContext

#Region " PROPERTY CHANGE NOTIFICATIONS "

    ''' <summary> Executes the property changed action. </summary>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal propertyName As String)
        Me.OnPropertyChanged(New PropertyChangedEventArgs(propertyName))
    End Sub

    ''' <summary> Executes the property changed action. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnPropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = ChangedEvent()
        If evt IsNot Nothing Then
            Me.Context.Send(Sub() evt(Me, e), Nothing)
        End If
    End Sub

#End Region

#Region " COLLECTION CHANGE NOTIFICATIONS "

    Public Event CollectionChanged As NotifyCollectionChangedEventHandler Implements INotifyCollectionChanged.CollectionChanged

    Private Sub OnCollectionChanged(action As NotifyCollectionChangedAction, item As Object, index As Integer)
        Me.OnCollectionChanged(New NotifyCollectionChangedEventArgs(action, item, index))
    End Sub

    Private Sub OnCollectionChanged(action As NotifyCollectionChangedAction, item As Object, index As Integer, oldIndex As Integer)
        Me.OnCollectionChanged(New NotifyCollectionChangedEventArgs(action, item, index, oldIndex))
    End Sub

    Private Sub OnCollectionChanged(action As NotifyCollectionChangedAction, oldItem As Object, newItem As Object, index As Integer)
        Me.OnCollectionChanged(New NotifyCollectionChangedEventArgs(action, newItem, oldItem, index))
    End Sub

    Private Sub OnCollectionChanged(e As NotifyCollectionChangedEventArgs)
        Dim evt As NotifyCollectionChangedEventHandler = CollectionChangedEvent
        If evt IsNot Nothing Then
            Using BlockReentrancy()
                Me.Context.Send(Sub(state) evt(Me, e), Nothing)
            End Using
        End If
    End Sub

    Private Sub OnCollectionReset()
        Me.OnCollectionChanged(New NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset))
    End Sub

#End Region
#End Region

#Region " THREAD SYNC MANAGEMENT  "

    Private ReadOnly Property ItemsLocker As New ReaderWriterLockSlim()

    Private ReadOnly Property IsSynchronized() As Boolean Implements ICollection.IsSynchronized
        Get
            Return True
        End Get
    End Property

    <NonSerialized>
    Private _SyncRoot As Object
    Private ReadOnly Property SyncRoot() As Object Implements ICollection.SyncRoot
        Get
            If Me._SyncRoot Is Nothing Then
                Me.ItemsLocker.EnterReadLock()
                Try
                    Dim c As ICollection = TryCast(Me.Items, ICollection)
                    If c IsNot Nothing Then
                        _SyncRoot = c.SyncRoot
                    Else
                        Interlocked.CompareExchange(Of [Object])(_SyncRoot, New [Object](), Nothing)
                    End If
                Finally
                    _ItemsLocker.ExitReadLock()
                End Try
            End If
            Return Me._SyncRoot
        End Get
    End Property

#Region " CHECK RE-RENTY and VALIDATION METHODS "

    Private ReadOnly Property Monitor As New ThreadSafeMonitor()

    ''' <summary> Blocks reentrancy. </summary>
    ''' <returns> An IDisposable. </returns>
    Private Function BlockReentrancy() As IDisposable
        Me.Monitor.Enter()
        Return Me.Monitor
    End Function

    ''' <summary> Check reentrancy. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Private Sub CheckReentrancy()
        If Me.Monitor.Busy AndAlso Me.CollectionChangedEvent IsNot Nothing AndAlso Me.CollectionChangedEvent.GetInvocationList().Any Then
            Throw New InvalidOperationException("Reentrancy not allowed")
        End If
    End Sub

#End Region

#End Region

#Region " ITEMS AS T "

    Private ReadOnly Property Items As IList(Of T) = New List(Of T)()


    Private ReadOnly Property IsFixedSize() As Boolean Implements IList.IsFixedSize
        Get
            Dim list As IList = TryCast(_Items, IList)
            If list IsNot Nothing Then
                Return list.IsFixedSize
            End If

            Return Me.Items.IsReadOnly
        End Get
    End Property

    Private ReadOnly Property IsReadOnly() As Boolean Implements ICollection(Of T).IsReadOnly
        Get
            Return Me.Items.IsReadOnly
        End Get
    End Property

    Private ReadOnly Property _Ilist_IsReadOnly() As Boolean Implements IList.IsReadOnly
        Get
            Return Me.Items.IsReadOnly
        End Get
    End Property

#End Region

#Region " LIST, LIST OF T and READ ONLY LIST OF T IMPLEMENTATION  "

    ''' <summary> Check index. </summary>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="index"> Zero-based index of the. </param>
    Private Sub CheckIndex(index As Integer)
        If index < 0 OrElse index >= Me.Items.Count Then
            Throw New ArgumentOutOfRangeException(NameOf(index), FormattableString.Invariant($"Must be between 0 and {Me._Items.Count}"))
        End If
    End Sub


    ''' <summary> Gets the number of elements.  </summary>
    ''' <value> The count. </value>
    Public ReadOnly Property Count() As Integer Implements IList.Count, IList(Of T).Count, IReadOnlyList(Of T).Count
        Get
            Me.ItemsLocker.EnterReadLock()
            Try
                Return Me.Items.Count
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End Get
    End Property

    ''' <summary> Gets or sets the element at the specified index. </summary>
    ''' <value> The element at the specified index. </value>
    Public Property Item(index As Integer) As T Implements IList(Of T).Item, IReadOnlyList(Of T).Item
        Get
            Me.ItemsLocker.EnterReadLock()
            Try
                Me.CheckIndex(index)
                Return Me.Items(index)
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End Get
        Set
            Dim oldValue As T
            Me.ItemsLocker.EnterWriteLock()

            Try
                Me.CheckIsReadOnly()
                Me.CheckIndex(index)
                Me.CheckReentrancy()

                oldValue = Me(index)

                Me.Items(index) = Value
            Finally
                Me.ItemsLocker.ExitWriteLock()
            End Try

            Me.OnPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Item))
            Me.OnCollectionChanged(NotifyCollectionChangedAction.Replace, oldValue, Value, index)
        End Set
    End Property

    Private Property IListItem(index As Integer) As Object Implements IList.Item
        Get
            Return Me.Item(index)
        End Get
        Set
            Try
                Me.Item(index) = CType(Value, T)
            Catch generatedExceptionName As InvalidCastException
                Throw New ArgumentException("'value' is the wrong type")
            End Try
        End Set
    End Property

    ''' <summary> Check is read only. </summary>
    ''' <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    Private Sub CheckIsReadOnly()
        If Me.Items.IsReadOnly Then
            Throw New NotSupportedException("Collection is read only")
        End If
    End Sub

    ''' <summary> Query if 'value' is compatible object. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if compatible object; otherwise <c>false</c> </returns>
    Private Shared Function IsCompatibleObject(value As Object) As Boolean
        ' Non-null values are fine.  Only accept nulls if T is a class or Nullable<U>.
        ' Note that default(T) is not equal to null for value types except when T is Nullable<U>. 
        Return (TypeOf value Is T) OrElse (value Is Nothing)
    End Function

#End Region

#Region " PUBLIC METHODS "

    Public Sub Add(item As T) Implements IList(Of T).Add
        _ItemsLocker.EnterWriteLock()

        Dim index As Integer = -1

        Try
            Me.CheckIsReadOnly()
            Me.CheckReentrancy()

            index = Me.Items.Count

            Me.Items.Insert(index, item)
        Finally
            _ItemsLocker.ExitWriteLock()
        End Try

        Me.OnPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Count))
        Me.OnPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).item))
        Me.OnCollectionChanged(NotifyCollectionChangedAction.Add, item, index)
    End Sub

    Private Function Add(value As Object) As Integer Implements IList.Add
        Me.ItemsLocker.EnterWriteLock()

        Dim index As Integer = -1
        Dim item As T

        Try
            Me.CheckIsReadOnly()
            Me.CheckReentrancy()
            index = Me.Items.Count
            item = CType(value, T)
            Me.Items.Insert(index, item)
        Catch generatedExceptionName As InvalidCastException
            Throw New ArgumentException(FormattableString.Invariant($"'{NameOf(value)}' is the wrong type"))
        Finally
            _ItemsLocker.ExitWriteLock()
        End Try

        Me.OnPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Count))
        Me.OnPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).item))
        Me.OnCollectionChanged(NotifyCollectionChangedAction.Add, item, index)
        Return index
    End Function

    Public Sub Clear() Implements IList.Clear, IList(Of T).Clear
        Me.ItemsLocker.EnterWriteLock()

        Try
            Me.CheckIsReadOnly()
            Me.CheckReentrancy()
            Me.Items.Clear()
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try

        Me.OnPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Count))
        Me.OnPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Item))
        Me.OnCollectionReset()
    End Sub

    Public Sub CopyTo(array As T(), arrayIndex As Integer) Implements IList(Of T).CopyTo
        Me.ItemsLocker.EnterReadLock()
        Try
            Me.Items.CopyTo(array, arrayIndex)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Sub

    Private Sub CopyTo(array As Array, index As Integer) Implements ICollection.CopyTo
        Me.ItemsLocker.EnterReadLock()
        Try
            If array Is Nothing Then
                Throw New ArgumentNullException(NameOf(array), FormattableString.Invariant($"'{NameOf(array)}' cannot be null"))
            End If

            If array.Rank <> 1 Then
                Throw New ArgumentException("Multidimensional arrays are not supported", "array")
            End If

            If array.GetLowerBound(0) <> 0 Then
                Throw New ArgumentException("Non-zero lower bound arrays are not supported", "array")
            End If

            If index < 0 Then
                Throw New ArgumentOutOfRangeException(NameOf(index), FormattableString.Invariant($"'{NameOf(index)}' is out of range"))
            End If

            If array.Length - index < Me.Items.Count Then
                Throw New ArgumentException("Array is too small")
            End If

            Dim tArray As T() = TryCast(array, T())
            If tArray IsNot Nothing Then
                Me.Items.CopyTo(tArray, index)
            Else
                '
                ' Catch the obvious case assignment will fail.
                ' We can found all possible problems by doing the check though.
                ' For example, if the element type of the Array is derived from T,
                ' we can't figure out if we can successfully copy the element beforehand.
                '
                Dim targetType As System.Type = array.[GetType]().GetElementType()
                Dim sourceType As System.Type = GetType(T)
                If Not (targetType.IsAssignableFrom(sourceType) OrElse sourceType.IsAssignableFrom(targetType)) Then
                    Throw New ArrayTypeMismatchException("Invalid array type")
                End If

                '
                ' We can't cast array of value type to object[], so we don't support 
                ' widening of primitive types here.
                '
                Dim objects As Object() = TryCast(array, Object())
                If objects Is Nothing Then
                    Throw New ArrayTypeMismatchException("Invalid array type")
                End If

                Dim count As Integer = _Items.Count
                Try
                    Dim i As Integer = 0
                    While i < count
                        index += 1
                        objects(index) = Me.Items(i)
                        i += 1
                    End While
                Catch generatedExceptionName As ArrayTypeMismatchException
                    Throw New ArrayTypeMismatchException("Invalid array type")
                End Try
            End If
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Sub

    Public Function Contains(item As T) As Boolean Implements IList(Of T).Contains
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Items.Contains(item)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    Private Function Contains(value As Object) As Boolean Implements IList.Contains
        If IsCompatibleObject(value) Then
            Me.ItemsLocker.EnterReadLock()
            Try
                Return Me.Items.Contains(CType(value, T))
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End If

        Return False
    End Function

    Public Function GetEnumerator() As IEnumerator(Of T) Implements IList(Of T).GetEnumerator
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Items.ToList().GetEnumerator()
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    Private Function _GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator
        Me.ItemsLocker.EnterReadLock()
        Try
            Return CType(Items.ToList(), IEnumerable).GetEnumerator()
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    Public Function IndexOf(item As T) As Integer Implements IList(Of T).IndexOf
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Items.IndexOf(item)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    Private Function IndexOf(value As Object) As Integer Implements IList.IndexOf
        If IsCompatibleObject(value) Then
            Me.ItemsLocker.EnterReadLock()
            Try
                Return Me.Items.IndexOf(CType(value, T))
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End If

        Return -1
    End Function

    Public Sub Insert(index As Integer, item As T) Implements IList(Of T).Insert
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.CheckIsReadOnly()
            If index < 0 OrElse index >= Me.Items.Count Then
                Throw New ArgumentOutOfRangeException(NameOf(index), FormattableString.Invariant($"Must be between {0} and {Me._Items.Count}"))
            End If
            Me.CheckReentrancy()
            Me.Items.Insert(index, item)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
        Me.OnPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Count))
        Me.OnPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).item))
        Me.OnCollectionChanged(NotifyCollectionChangedAction.Add, item, index)
    End Sub

    Private Sub Insert(index As Integer, value As Object) Implements IList.Insert
        Try
            Me.Insert(index, CType(value, T))
        Catch generatedExceptionName As InvalidCastException
            Throw New ArgumentException(FormattableString.Invariant($"'{NameOf(value)}' is the wrong type"))
        End Try
    End Sub

    Public Function Remove(item As T) As Boolean Implements IList(Of T).Remove
        Dim index As Integer
        Dim value As T

        Me.ItemsLocker.EnterWriteLock()

        Try
            Me.CheckIsReadOnly()
            Me.CheckReentrancy()

            index = Me.Items.IndexOf(item)

            If index < 0 Then
                Return False
            End If

            value = Me.Items(index)
            Me.Items.RemoveAt(index)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try

        Me.OnPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Count))
        Me.OnPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).item))
        Me.OnCollectionChanged(NotifyCollectionChangedAction.Remove, value, index)

        Return True
    End Function

    Private Sub Remove(value As Object) Implements IList.Remove
        If IsCompatibleObject(value) Then
            Me.Remove(CType(value, T))
        End If
    End Sub

    Public Sub RemoveAt(index As Integer) Implements IList.RemoveAt, IList(Of T).RemoveAt

        Dim value As T

        Me.ItemsLocker.EnterWriteLock()

        Try
            Me.CheckIsReadOnly()
            Me.CheckIndex(index)
            Me.CheckReentrancy()

            value = Me.Items(index)

            Me.Items.RemoveAt(index)
        Finally
            _ItemsLocker.ExitWriteLock()
        End Try

        Me.OnPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Count))
        Me.OnPropertyChanged(NameOf(SynchronizedObservableCollection(Of T).Item))
        Me.OnCollectionChanged(NotifyCollectionChangedAction.Remove, value, index)
    End Sub
#End Region

End Class


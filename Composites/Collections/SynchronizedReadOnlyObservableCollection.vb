﻿Imports System.Collections.ObjectModel
Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Threading

<DebuggerDisplay("Count = {Count}")>
Public Class SynchronizedReadOnlyObservableCollection(Of T)
    Inherits ReadOnlyCollection(Of T)
    Implements INotifyCollectionChanged, INotifyPropertyChanged

#Region "Constructor"
    Public Sub New(ByVal list As SynchronizedObservableCollection(Of T))
        MyBase.New(CType(list, IList(Of T)))
        Me._Context = SynchronizationContext.Current
        AddHandler CType(Items, INotifyCollectionChanged).CollectionChanged, AddressOf HandleCollectionChanged
        AddHandler CType(Items, INotifyPropertyChanged).PropertyChanged, AddressOf HandlePropertyChanged
    End Sub
#End Region

#Region "Public Events"
    Public Event CollectionChanged As NotifyCollectionChangedEventHandler Implements INotifyCollectionChanged.CollectionChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
#End Region

#Region "Private Methods"
    Private Sub HandleCollectionChanged(ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
        OnCollectionChanged(e)
    End Sub

    Private Sub HandlePropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        OnPropertyChanged(e)
    End Sub
#End Region

#Region "Protected Methods"

    Private ReadOnly Property Context As SynchronizationContext

    Private Sub OnCollectionChanged(ByVal e As NotifyCollectionChangedEventArgs)
        Dim evt As NotifyCollectionChangedEventHandler = CollectionChangedEvent
        If evt IsNot Nothing Then
            Me.Context.Send(Sub(state) evt(Me, e), Nothing)
        End If
    End Sub

    Private Sub OnPropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = PropertyChangedEvent
        If evt IsNot Nothing Then
            Me.Context.Send(Sub(state) evt(Me, e), Nothing)
        End If

    End Sub
#End Region

End Class

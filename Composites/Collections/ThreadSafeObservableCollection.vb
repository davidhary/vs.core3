﻿Imports System.Threading
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports isr.Core.Pith.EventHandlerExtensions
Imports isr.Core.Pith.ExceptionExtensions

''' <summary> A thread safe observable collection based upon <see cref="ObjectModel.ObservableCollection(Of T)"/>   </summary>
''' <remarks> TO_DO: Modify based on Observable Keyed Collection. </remarks>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/13/2016" by="David" revision=""> Created. </history>
<Serializable>
<DebuggerDisplay("Count = {Count}")>
Public Class ThreadSafeObservableCollection(Of T)
    Inherits ObjectModel.ObservableCollection(Of T)
    Implements IDisposable

#Region " CONSTRUCTOR "

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal collection As IEnumerable(Of T))
        MyBase.New(collection)
    End Sub

    Protected Property IsDisposed As Boolean

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:Collection" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                                                   release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If Me._ItemsLocker IsNot Nothing Then Me._ItemsLocker.Dispose() : Me._ItemsLocker = Nothing
                Me.RemovePropertyChangedEventHandler()
                Me.RemovePropertyChangedEventHandler()
            End If
        Catch
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

#End Region

#Region " COLLECTION CHANGE NOTIFICATIONS "

    Public Shadows Event CollectionChanged As NotifyCollectionChangedEventHandler

    Protected Overrides Sub OnCollectionChanged(e As NotifyCollectionChangedEventArgs)
#If True Then
        Using MyBase.BlockReentrancy()
            Me.CollectionChangedEvent.SafePost(Me, e)
        End Using
#Else
        Dim evt As NotifyCollectionChangedEventHandler = Me.CollectionChangedEvent
        If evt IsNot Nothing Then
            Using MyBase.BlockReentrancy()
                Me.CollectionChangedEvent.SafePost(Me, e)
                Me.SyncContext.Send(Sub(state) evt(Me, e), Nothing)
            End Using
        End If
#End If
    End Sub

    ''' <summary> Removes the event handler. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Sub RemoveEventHandler(ByVal value As NotifyCollectionChangedEventHandler)
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.CollectionChanged, CType(d, NotifyCollectionChangedEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub
    Protected Sub RemoveNotifyCollectionChangedEventHandler()
        Me.RemoveEventHandler(Me.CollectionChangedEvent)
    End Sub


#End Region

#Region " THREAD SYNC MANAGEMENT  "

    ''' <summary> Gets the items locker. </summary>
    ''' <value> The items locker. </value>
    Protected ReadOnly Property ItemsLocker As New ReaderWriterLockSlim()

    <NonSerialized>
    Private _SyncRoot As Object

    ''' <summary> Gets the synchronization root. </summary>
    ''' <value> The synchronization root. </value>
    ''' <remarks> The Sync root helps super classes synchronously access the items. 
    '''           For details see reply by Roman Zavalov in https://stackoverflow.com/questions/728896/whats-the-use-of-the-syncroot-pattern
    ''' </remarks>
    Public ReadOnly Property SyncRoot() As Object
        Get
            If Me._syncRoot Is Nothing Then
                Me.ItemsLocker.EnterReadLock()
                Try
                    Dim c As ICollection = TryCast(Me.Items, ICollection)
                    If c IsNot Nothing Then
                        Me._syncRoot = c.SyncRoot
                    Else
                        Interlocked.CompareExchange(Of [Object])(Me._syncRoot, New [Object](), Nothing)
                    End If
                Finally
                    Me.ItemsLocker.ExitReadLock()
                End Try
            End If
            Return Me._syncRoot
        End Get
    End Property

#End Region

#Region " CHECK METHODS  "

    ''' <summary> Gets the size of the is fixed. </summary>
    ''' <value> The size of the is fixed. </value>
    Private ReadOnly Property IsFixedSize() As Boolean
        Get
            Dim list As IList = TryCast(MyBase.Items, IList)
            If list IsNot Nothing Then
                Return list.IsFixedSize
            End If
            Return Me.Items.IsReadOnly
        End Get
    End Property

    ''' <summary> Gets the is read only. </summary>
    ''' <value> The is read only. </value>
    Private ReadOnly Property IsReadOnly() As Boolean
        Get
            Return Me.Items.IsReadOnly
        End Get
    End Property

    ''' <summary> Throws an exception if index is out of range. </summary>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="index"> Zero-based index of the. </param>
    Private Sub EnsureIndexRange(index As Integer)
        If index < 0 OrElse index >= Me.Items.Count Then
            Throw New ArgumentOutOfRangeException(NameOf(index), FormattableString.Invariant($"Must be between {0} and {Me.Items.Count}"))
        End If
    End Sub

    ''' <summary> Throws an exception if read only. </summary>
    ''' <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    Private Sub EnsureNotReadOnly()
        If Me.Items.IsReadOnly Then
            Throw New NotSupportedException("Collection is read only")
        End If
    End Sub

    ''' <summary> Query if 'value' is compatible object. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if compatible object; otherwise <c>false</c> </returns>
    Private Shared Function IsCompatibleObject(value As Object) As Boolean
        ' Non-null values are fine.  Only accept nulls if T is a class or Nullable<U>.
        ' Note that default(T) is not equal to null for value types except when T is Nullable<U>. 
        Return (TypeOf value Is T) OrElse (value Is Nothing)
    End Function

#End Region

#Region " PUBLIC OVERLOADS "

    ''' <summary> Gets the number of elements.  </summary>
    ''' <value> The count. </value>
    Public Overloads ReadOnly Property Count() As Integer
        Get
            Me.ItemsLocker.EnterReadLock()
            Try
                Return Me.Items.Count
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End Get
    End Property

    ''' <summary> Provides direct access to the base class item to permit synchronization for the super class. </summary>
    ''' <value> The base item. </value>
    Protected Property BaseItem(ByVal index As Integer) As T
        Get
            Return MyBase.Item(index)
        End Get
        Set(value As T)
            MyBase.Item(index) = value
        End Set
    End Property

    ''' <summary> Gets or sets the element at the specified index. </summary>
    ''' <value> The element at the specified index. </value>
    Public Overloads Property Item(index As Integer) As T
        Get
            Me.ItemsLocker.EnterReadLock()
            Try
                Me.EnsureIndexRange(index)
                Return MyBase.Item(index)
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End Get
        Set
            Me.ItemsLocker.EnterWriteLock()
            Try
                Me.EnsureNotReadOnly()
                Me.EnsureIndexRange(index)
                MyBase.Item(index) = Value
            Finally
                Me.ItemsLocker.ExitWriteLock()
            End Try
        End Set
    End Property

    ''' <summary>
    ''' Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />
    ''' .
    ''' </summary>
    ''' <param name="item"> The object to add to the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />
    '''                     . </param>
    Public Overloads Sub Add(ByVal item As T)
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.EnsureNotReadOnly()
            MyBase.Add(item)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary>
    ''' Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" />
    '''  to an <see cref="T:System.Array" />
    ''' , starting at a particular <see cref="T:System.Array" />
    '''  index.
    ''' </summary>
    ''' <param name="array"> The one-dimensional <see cref="T:System.Array" />
    '''                       that is the destination of the elements copied from
    '''                       <see cref="T:System.Collections.Generic.ICollection`1" />
    '''                      . The <see cref="T:System.Array" />
    '''                       must have zero-based indexing. </param>
    ''' <param name="index"> The zero-based index in <paramref name="array" />
    '''                       at which copying begins. </param>
    Public Overloads Sub CopyTo(array As T(), index As Integer)
        Me.ItemsLocker.EnterReadLock()
        Try
            MyBase.CopyTo(array, index)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Sub

    ''' <summary>
    ''' Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" />
    '''  contains a specific value.
    ''' </summary>
    ''' <param name="item"> The object to locate in the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />
    '''                     . </param>
    ''' <returns>
    ''' true if <paramref name="item" />
    '''  is found in the <see cref="T:System.Collections.Generic.ICollection`1" />
    ''' ; otherwise, false.
    ''' </returns>
    Public Overloads Function Contains(item As T) As Boolean
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Items.Contains(item)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    ''' <summary> Returns an enumerator that iterates through the collection. </summary>
    ''' <returns> An enumerator that can be used to iterate through the collection. </returns>
    Public Overloads Function GetEnumerator() As IEnumerator(Of T)
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Items.ToList().GetEnumerator()
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    ''' <summary>
    ''' Determines the index of a specific item in the
    ''' <see cref="T:System.Collections.Generic.IList`1" />
    ''' .
    ''' </summary>
    ''' <param name="item"> The object to locate in the
    '''                     <see cref="T:System.Collections.Generic.IList`1" />
    '''                     . </param>
    ''' <returns>
    ''' The index of <paramref name="item" />
    '''  if found in the list; otherwise, -1.
    ''' </returns>
    Public Overloads Function IndexOf(item As T) As Integer
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Items.IndexOf(item)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

#End Region

#Region " PROTECTED OVERRIDES "

    ''' <summary> Removes all items from the collection. </summary>
    Protected Overrides Sub ClearItems()
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.EnsureNotReadOnly()
            MyBase.ClearItems()
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Inserts an item into the collection at the specified index. </summary>
    ''' <param name="index"> The zero-based index at which <paramref name="item" />
    '''                       should be inserted. </param>
    ''' <param name="item">  The object to insert. </param>
    Protected Overrides Sub InsertItem(index As Integer, item As T)
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.EnsureNotReadOnly()
            Me.EnsureIndexRange(index)
            MyBase.InsertItem(index, item)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    Protected Sub BaseRemoveItem(ByVal index As Integer)
        MyBase.RemoveItem(index)
    End Sub

    ''' <summary> Removes the item at the specified index of the collection. </summary>
    ''' <param name="index"> The zero-based index of the element to remove. </param>
    Protected Overrides Sub RemoveItem(ByVal index As Integer)
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.EnsureNotReadOnly()
            Me.EnsureIndexRange(index)
            MyBase.RemoveItem(index)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Replaces the element at the specified index. </summary>
    ''' <param name="index"> The zero-based index of the element to replace. </param>
    ''' <param name="item">  The new value for the element at the specified index. </param>
    Protected Overrides Sub SetItem(index As Integer, item As T)
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.EnsureNotReadOnly()
            Me.EnsureIndexRange(index)
            MyBase.SetItem(index, item)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

#End Region

End Class


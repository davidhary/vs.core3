﻿Imports System.Threading
Imports System.ComponentModel
Imports isr.Core.Pith.ExceptionExtensions
Imports isr.Core.Pith.EventHandlerExtensions

<CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2240:ImplementISerializableCorrectly")>
Partial Public Class ThreadSafeObservableCollection(Of T)

#Region " SYNC CONTEXT "

    ''' <summary> Caches the synchronization context for threading functions. </summary>
    ''' <value> The captured synchronization context. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property CapturedSyncContext As Threading.SynchronizationContext

    ''' <summary> Applies the captured or a new synchronization context. </summary>
    Public Sub ApplyCapturedSyncContext()
        If SynchronizationContext.Current Is Nothing Then
            If Me.CapturedSyncContext Is Nothing Then Me._CapturedSyncContext = New SynchronizationContext
            Threading.SynchronizationContext.SetSynchronizationContext(Me.CapturedSyncContext)
        End If
    End Sub

    ''' <summary> Captures and applies synchronization context. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when the captured sync context is null. </exception>
    ''' <param name="syncContext"> Context for the synchronization. </param>

    Public Sub CaptureSyncContext(ByVal syncContext As Threading.SynchronizationContext)
        If syncContext Is Nothing Then Throw New ArgumentNullException(NameOf(syncContext))

        Me._CapturedSyncContext = syncContext
        Me.ApplyCapturedSyncContext()
    End Sub

#End Region

#Region " PROPERTY CHANGED EVENT IMPLEMENTATION "

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Shadows Event PropertyChanged As PropertyChangedEventHandler

    ''' <summary> Removes the event handler. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Sub RemoveEventHandler(ByVal value As PropertyChangedEventHandler)
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.PropertyChanged, CType(d, PropertyChangedEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub
    Protected Sub RemovePropertyChangedEventHandler()
        Me.RemoveEventHandler(PropertyChangedEvent)
    End Sub

#Region " PROPERTTY CHANGE NOTIFICATIONS "

    ''' <summary> Safe post property changed. </summary>
    ''' <param name="e"> Property Changed event information. </param>
    Protected Overridable Sub SafePostPropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.ApplyCapturedSyncContext()
        Me.PropertyChangedEvent.SafePost(Me, e)
    End Sub

    ''' <summary> Safe post property changed. </summary>
    ''' <param name="name"> (Optional) caller member. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafePostPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafePostPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

    ''' <summary> Safe send property changed. </summary>
    ''' <param name="e"> Property Changed event information. </param>
    Protected Overridable Sub SafeSendPropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.ApplyCapturedSyncContext()
        Me.PropertyChangedEvent.SafeSend(Me, e)
    End Sub

    ''' <summary> Safe send property changed. </summary>
    ''' <param name="name"> (Optional) caller member. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeSendPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafePostPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#End Region

End Class


﻿''' <summary> A Thread safe Observable Queue. </summary>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/12/2016" by="David" revision=""> Created. </history>
<CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix")>
<CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix")>
<DebuggerDisplay("Count = {Count}")>
Public Class ThreadSafeObservableQueue(Of T)
    Inherits ThreadSafeObservableCollection(Of T)

    ''' <summary> Adds an object onto the end of this queue. </summary>
    ''' <param name="item"> The item. </param>
    Public Sub Enqueue(ByVal item As T)
        Me.Add(item)
    End Sub

    ''' <summary> Removes the head object from this queue. </summary>
    ''' <returns> The head object from this queue. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="item")>
    Public Function Dequeue() As T
        Me.ItemsLocker.EnterUpgradeableReadLock()
        Try
            Dim item As T = Me.BaseItem(0)
            Me.ItemsLocker.EnterWriteLock()
            Me.BaseRemoveItem(0)
            Return item
        Finally
            Me.ItemsLocker.ExitUpgradeableReadLock()
        End Try
    End Function

    ''' <summary> Returns the top-of-stack object without removing it. </summary>
    ''' <returns> The current top-of-stack object. </returns>
    Public Function Peek() As T
        Return Me.Item(0)
    End Function


End Class

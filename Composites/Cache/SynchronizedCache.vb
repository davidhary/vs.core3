﻿Imports System.Threading
Imports System.Threading.Tasks
''' <summary> A synchronized cache. </summary>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="4/6/2017" by="David" revision=""> 
''' https://msdn.microsoft.com/en-us/library/system.threading.readerwriterlockslim(v=vs.110).aspx?cs-save-lang=1%26cs-lang=vb#code-snippet-2. 
''' </history>
Public Class SynchronizedCache(Of TKey, TValue)
    Implements IDisposable

#Region " CONSTRUCTOR "

    Public Sub New()
        MyBase.New()
        cacheLock = New ReaderWriterLockSlim()
        innerCache = New Dictionary(Of TKey, TValue)
    End Sub

#Region "IDisposable Support"

    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If Me.cacheLock IsNot Nothing Then Me.cacheLock.Dispose() : Me.cacheLock = Nothing
            End If
            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
    ' Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        ' TODO: uncomment the following line if Finalize() is overridden above.
        GC.SuppressFinalize(Me)
    End Sub

#End Region
#End Region

    Private cacheLock As ReaderWriterLockSlim
    Private innerCache As Dictionary(Of TKey, TValue)

    ''' <summary> Gets the number of.  </summary>
    ''' <value> The count. </value>
    Public ReadOnly Property Count As Integer
        Get
            Return innerCache.Count
        End Get
    End Property

    ''' <summary> Reads a value using the given key. </summary>
    ''' <param name="key"> The key to read. </param>
    ''' <returns> A TValue. </returns>
    Public Function Read(ByVal key As TKey) As TValue
        cacheLock.EnterReadLock()
        Try
            Return innerCache(key)
        Finally
            cacheLock.ExitReadLock()
        End Try
    End Function

    ''' <summary> Adds a key-value pair. </summary>
    ''' <param name="key">   The key to read. </param>
    ''' <param name="value"> The value. </param>
    Public Sub Add(ByVal key As TKey, ByVal value As TValue)
        cacheLock.EnterWriteLock()
        Try
            innerCache.Add(key, value)
        Finally
            cacheLock.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Adds a with timeout. </summary>
    ''' <param name="key">     The key to read. </param>
    ''' <param name="value">   The value. </param>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function AddWithTimeout(ByVal key As TKey, ByVal value As TValue, ByVal timeout As Integer) As Boolean
        If cacheLock.TryEnterWriteLock(timeout) Then
            Try
                innerCache.Add(key, value)
            Finally
                cacheLock.ExitWriteLock()
            End Try
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary> Adds an or update to 'value'. </summary>
    ''' <param name="key">   The key to read. </param>
    ''' <param name="value"> The value. </param>
    ''' <returns> The AddOrUpdateStatus. </returns>
    Public Function AddOrUpdate(ByVal key As TKey, ByVal value As TValue) As CacheChangeStatus
        cacheLock.EnterUpgradeableReadLock()
        Try
            Dim result As TValue = Nothing
            If innerCache.TryGetValue(key, result) Then
                If result.Equals(value) Then
                    Return CacheChangeStatus.Unchanged
                Else
                    cacheLock.EnterWriteLock()
                    Try
                        innerCache.Item(key) = value
                    Finally
                        cacheLock.ExitWriteLock()
                    End Try
                    Return CacheChangeStatus.Updated
                End If
            Else
                cacheLock.EnterWriteLock()
                Try
                    innerCache.Add(key, value)
                Finally
                    cacheLock.ExitWriteLock()
                End Try
                Return CacheChangeStatus.Added
            End If
        Finally
            cacheLock.ExitUpgradeableReadLock()
        End Try
    End Function

    ''' <summary> Deletes the given key. </summary>
    ''' <param name="key"> The key to read. </param>
    Public Sub Delete(ByVal key As TKey)
        cacheLock.EnterWriteLock()
        Try
            innerCache.Remove(key)
        Finally
            cacheLock.ExitWriteLock()
        End Try
    End Sub

End Class

''' <summary> Values that represent cache change status. </summary>
Public Enum CacheChangeStatus
    Added
    Updated
    Unchanged
End Enum


Public Module SynchronizedCacheExample
    Public Sub Main()
        Dim sc As New SynchronizedCache(Of Integer, String)
        Dim tasks As New List(Of Task)
        Dim itemsWritten As Integer

        ' Execute a writer.
        tasks.Add(Task.Run(Sub()
                               Dim vegetables() As String = {"broccoli", "cauliflower",
                                                            "carrot", "sorrel", "baby turnip",
                                                            "beet", "Brussels sprout",
                                                            "cabbage", "plantain",
                                                            "spinach", "grape leaves",
                                                            "lime leaves", "corn",
                                                            "radish", "cucumber",
                                                            "raddichio", "Lima beans"}
                               For ctr As Integer = 1 To vegetables.Length
                                   sc.Add(ctr, vegetables(ctr - 1))
                               Next
                               itemsWritten = vegetables.Length
                               Console.WriteLine("Task {0} wrote {1} items{2}",
                                               Task.CurrentId, itemsWritten, vbCrLf)
                           End Sub))
        ' Execute two readers, one to read from first to last and the second from last to first.
        For ctr As Integer = 0 To 1
            Dim flag As Integer = ctr
            tasks.Add(Task.Run(Sub()
                                   Dim start, last, stp As Integer
                                   Dim items As Integer
                                   Do
                                       Dim output As String = String.Empty
                                       items = sc.Count
                                       If flag = 0 Then
                                           start = 1 : stp = 1 : last = items
                                       Else
                                           start = items : stp = -1 : last = 1
                                       End If
                                       For index As Integer = start To last Step stp
                                           output += String.Format("[{0}] ", sc.Read(index))
                                       Next
                                       Console.WriteLine("Task {0} read {1} items: {2}{3}",
                                                           Task.CurrentId, items, output,
                                                           vbCrLf)
                                   Loop While items < itemsWritten Or itemsWritten = 0
                               End Sub))
        Next
        ' Execute a red/update task.
        tasks.Add(Task.Run(Sub()
                               For ctr As Integer = 1 To sc.Count
                                   Dim value As String = sc.Read(ctr)
                                   If value = "cucumber" Then
                                       If sc.AddOrUpdate(ctr, "green bean") <> CacheChangeStatus.Unchanged Then
                                           Console.WriteLine("Changed 'cucumber' to 'green bean'")
                                       End If
                                   End If
                               Next
                           End Sub))

        ' Wait for all three tasks to complete.
        Task.WaitAll(tasks.ToArray())

        ' Display the final contents of the cache.
        Console.WriteLine()
        Console.WriteLine("Values in synchronized cache: ")
        For ctr As Integer = 1 To sc.Count
            Console.WriteLine("   {0}: {1}", ctr, sc.Read(ctr))
        Next
    End Sub

End Module

#Region " EXAMPLE "
#If False Then


' The example displays output like the following:
'    Task 1 read 0 items:
'
'    Task 3 wrote 17 items
'
'    Task 1 read 17 items: [broccoli] [cauliflower] [carrot] [sorrel] [baby turnip] [
'    beet] [brussel sprout] [cabbage] [plantain] [spinach] [grape leaves] [lime leave
'    s] [corn] [radish] [cucumber] [raddichio] [lima beans]
'
'    Task 2 read 0 items:
'
'    Task 2 read 17 items: [lima beans] [raddichio] [cucumber] [radish] [corn] [lime
'    leaves] [grape leaves] [spinach] [plantain] [cabbage] [brussel sprout] [beet] [b
'    aby turnip] [sorrel] [carrot] [cauliflower] [broccoli]
'
'    Changed 'cucumber' to 'green bean'
'
'    Values in synchronized cache:
'       1: broccoli
'       2: cauliflower
'       3: carrot
'       4: sorrel
'       5: baby turnip
'       6: beet
'       7: brussel sprout
'       8: cabbage
'       9: plantain
'       10: spinach
'       11: grape leaves
'       12: lime leaves
'       13: corn
'       14: radish
'       15: green bean
'       16: raddichio
'       17: lima beans

#End If
#End Region
#If False Then

#End If
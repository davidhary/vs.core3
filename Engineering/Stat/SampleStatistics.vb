﻿Imports System.Collections.Concurrent
''' <summary> Sample statistics. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="11/19/2014" by="David" revision=""> Created. </history>
Public Class SampleStatistics
    Implements ICloneable

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me._ResetKnownState()
    End Sub

    ''' <summary> The cloning constructor. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As SampleStatistics)
        Me.New()
        If value IsNot Nothing Then
            Me._Mean = value.Mean
            Me._Sigma = value.Sigma
            Me._Sum = value.Sum
            Me._SumSquareDeviations = value.SumSquareDeviations
            Me._Maximum = value.Maximum
            Me._Minimum = value.Minimum
            Me._Queue = New ConcurrentQueue(Of Double)(value.Queue)
        End If
    End Sub

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Overridable Function Clone() As Object Implements System.ICloneable.Clone
        Return New SampleStatistics(Me)
    End Function

#End Region

#Region " RESET AND CLEAR "

    ''' <summary> Clears values to their known (initial) state. </summary>
    Private Sub _ClearKnownState()
        Me._Mean = 0
        Me._Sigma = 0
        Me._Sum = 0
        Me._SumSquareDeviations = 0
        Me._Maximum = Double.MinValue
        Me._Minimum = Double.MaxValue
        Me._Queue = New ConcurrentQueue(Of Double)
        Me._IsDirty = False
    End Sub

    ''' <summary> Clears values to their known (initial) state. </summary>
    Public Overridable Sub ClearKnownState()
        Me._ClearKnownState()
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    Private Sub _ResetKnownState()
        Me._ClearKnownState()
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    Public Overridable Sub ResetKnownState()
        Me._ResetKnownState()
    End Sub

#End Region

#Region " STATISTICS "

    Private _IsDirty As Boolean

    ''' <summary> Gets a value indicating whether this object is dirty. </summary>
    ''' <value> <c>true</c> if this object is dirty; otherwise <c>false</c> </value>
    Public ReadOnly Property IsDirty As Boolean
        Get
            Return Me._IsDirty
        End Get
    End Property

    Private _Maximum As Double
    ''' <summary> Gets the maximum. </summary>
    ''' <value> The maximum value. </value>
    Public ReadOnly Property Maximum As Double
        Get
            Return Me._Maximum
        End Get
    End Property

    Private _Minimum As Double
    ''' <summary> Gets the minimum. </summary>
    ''' <value> The minimum value. </value>
    Public ReadOnly Property Minimum As Double
        Get
            Return Me._Minimum
        End Get
    End Property

    Private _Mean As Double
    ''' <summary> Gets or sets the mean. </summary>
    ''' <value> The mean value. </value>
    Public ReadOnly Property Mean As Double
        Get
            If Me.IsDirty Then Me.Evaluate()
            Return Me._Mean
        End Get
    End Property

    Private _Sigma As Double
    ''' <summary> Gets or sets the sigma. </summary>
    ''' <value> The sigma. </value>
    Public ReadOnly Property Sigma As Double
        Get
            If Me.IsDirty Then Me.Evaluate()
            Return Me._Sigma
        End Get
    End Property

    Private _Sum As Double
    ''' <summary> Gets or sets the number of. </summary>
    ''' <value> The sum. </value>
    Public ReadOnly Property Sum As Double
        Get
            If Me.IsDirty Then Me.Evaluate()
            Return Me._Sum
        End Get
    End Property

    Private _SumSquareDeviations As Double
    ''' <summary> Gets or sets the sum square deviations. </summary>
    ''' <value> The total number of square deviations. </value>
    Public ReadOnly Property SumSquareDeviations As Double
        Get
            If Me.IsDirty Then Me.Evaluate()
            Return Me._SumSquareDeviations
        End Get
    End Property

    ''' <summary> Returns true if the sample includes values. </summary>
    ''' <value> any. </value>
    Public ReadOnly Property Any As Boolean
        Get
            Return Me._Queue.Any
        End Get
    End Property

    ''' <summary> Gets the number of values. </summary>
    ''' <value> The count. </value>
    Public ReadOnly Property Count As Integer
        Get
            Return Me._Queue.Count
        End Get
    End Property

    ''' <summary> Gets the internal values. </summary>
    ''' <value> The internal values. </value>
    Protected ReadOnly Property Queue As ConcurrentQueue(Of Double)

    ''' <summary> Gets the values. </summary>
    Public ReadOnly Property Values As IEnumerable(Of Double)
        Get
            Return Me.Queue
        End Get
    End Property

    ''' <summary> Evaluate mean. </summary>
    ''' <returns> A Double. </returns>
    Public Function EvaluateMean() As Double
        Me._Sum = 0
        Me._Mean = 0
        If Me._Queue IsNot Nothing AndAlso Me._Queue.Any Then
            For Each v As Double In Me._Queue
                Me._Sum += v
            Next
            Me._Mean = Me._Sum / Me.Count
        End If
        Return Me._Mean
    End Function

    ''' <summary> Evaluate sigma. </summary>
    Public Sub EvaluateSigma(ByVal average As Double)
        Me._Sigma = 0
        Me._SumSquareDeviations = 0
        If Me._Queue IsNot Nothing AndAlso Me._Queue.Count > 1 Then
            For Each v As Double In Me._Queue
                Me._SumSquareDeviations += (v - average) * (v - average)
            Next
            If Me.Count > 1 Then
                Me._Sigma = Math.Sqrt(Me._SumSquareDeviations / (Me.Count - 1))
            End If
        End If
    End Sub

    ''' <summary> Evaluates correlation coefficient. </summary>
    ''' <remarks> Assumes that the function values already exist. </remarks>
    ''' <returns> The correlation coefficient or coefficient of multiple determination. </returns>
    Public Function EvaluateCorrelationCoefficient(ByVal values As IEnumerable(Of Double)) As Double
        Dim valuesSample As New SampleStatistics()
        valuesSample.AddValues(values)
        valuesSample.Evaluate()
        Dim favg As Double = valuesSample.Mean
        Dim fssq As Double = valuesSample.SumSquareDeviations

        If Me.IsDirty Then Me.Evaluate()
        Dim oavg As Double = Me.Mean
        Dim ossq As Double = Me.SumSquareDeviations
        Dim ofssq As Double = 0
        For i As Integer = 0 To Me.Count - 1
            ofssq += (Me.Values(i) - oavg) * (valuesSample.Values(i) - favg)
        Next
        Return ofssq / (Math.Sqrt(fssq) * Math.Sqrt(ossq))
    End Function

    ''' <summary> Evaluates this object. </summary>
    Public Sub Evaluate()
        Me.EvaluateMean()
        Me.EvaluateSigma(Me._Mean)
        Me._IsDirty = False
    End Sub

    ''' <summary> Updates the range for the value. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub UpdateRange(ByVal value As Double)
        If value > Me.Maximum Then
            Me._Maximum = value
        End If
        If value < Me.Minimum Then
            Me._Minimum = value
        End If
    End Sub

    ''' <summary> Updates the range for all values. </summary>
    Public Sub UpdateRange()
        Me._Maximum = Double.MinValue
        Me._Minimum = Double.MaxValue
        For Each v As Double In Me.Queue
            Me.UpdateRange(v)
        Next
    End Sub

    ''' <summary> Adds a value. </summary>
    ''' <param name="value"> The value. </param>
    Public Overridable Sub AddValue(ByVal value As Double)
        Me._IsDirty = True
        Me.Queue.Enqueue(value)
        Me.UpdateRange(value)
    End Sub

    ''' <summary> Adds the values. </summary>
    ''' <param name="values"> The values. </param>
    Public Sub AddValues(ByVal values As Double())
        If values IsNot Nothing Then
            For Each v As Double In values
                Me.AddValue(v)
            Next
        End If
    End Sub

    ''' <summary> Adds the values. </summary>
    ''' <param name="values"> The values. </param>
    Public Sub AddValues(ByVal values As IEnumerable(Of Double))
        If values IsNot Nothing Then
            For Each v As Double In values
                Me.AddValue(v)
            Next
        End If
    End Sub

#End Region

End Class

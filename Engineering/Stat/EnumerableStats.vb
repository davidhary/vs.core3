﻿
Namespace EnumerableStats

    ''' <summary> A methods. </summary>
    ''' <license>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    Public Module Methods

        ''' <summary> Computes the histogram. </summary>
        ''' <remarks>
        ''' The first bin at X=<paramref name="lowerBound"/> counts all the values below the lower limit;
        ''' <para>
        ''' The second bin at X=<paramref name="lowerBound"/> + half the bin width counts the values higher and equal to the lower limit but lower than the bin
        ''' width;</para><para>
        ''' The next to last bin at X=<paramref name="upperBound"/> - half the bin width counts the values at the last bin;</para><para>
        ''' The last bin at <paramref name="upperBound"/> counts the number of values equal or higher than the high limit. </para>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">     Source for the. </param>
        ''' <param name="lowerBound"> The lower bound. </param>
        ''' <param name="upperBound"> The upper bound. </param>
        ''' <param name="binCount">   Number of bins. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process histogram direct in this collection.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function Histogram(ByVal source As IEnumerable(Of Double), ByVal lowerBound As Double, ByVal upperBound As Double,
                                  ByVal binCount As Integer) As IEnumerable(Of System.Windows.Point)
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Dim binWidth As Double = (upperBound - lowerBound) / binCount
            Dim inverseWidth As Double = 1 / binWidth
            Dim l As New List(Of Integer)
            For i As Integer = 0 To binCount + 1
                l.Add(0)
            Next
            For Each x As Double In source
                Dim binValue As Double = inverseWidth * (x - lowerBound)
                Dim binNumber As Integer = If(binValue < 0, 0, If(binValue >= binCount, binCount + 1, 1 + CInt(Fix(binValue))))
                l.Item(binNumber) += 1
            Next
            ' number of groups between values
            Dim histF As New List(Of System.Windows.Point)
            For i As Integer = 0 To l.Count - 1
                Dim x As Double = 1
                If i = 0 Then
                    x = lowerBound
                ElseIf i = l.Count - 1 Then
                    x = upperBound
                Else
                    ' x = lowerBound + 0.5 * binWidth + binWidth * (i - 1)
                    x = lowerBound + binWidth * (i - 0.5)
                End If
                histF.Add(New System.Windows.Point(x, l(i)))
            Next
            Return histF.ToArray
        End Function

    End Module

End Namespace

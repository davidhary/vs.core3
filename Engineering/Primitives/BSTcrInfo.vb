﻿''' <summary> British Standard (BS EN 60062) Temperature coefficient acceptance intervals. </summary>
''' <remarks> This class is sealed to ensure that the hash value of its elements is not used
'''           by two instances with different hash value set. </remarks>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="5/19/2014" by="David" revision=""> Created. </history>
Public NotInheritable Class BSTcrInfo
    Inherits AcceptanceInterval

    ''' <summary> Constructor. </summary>
    ''' <param name="code"> The temperature coefficient code. </param>
    Public Sub New(ByVal code As String)
        MyBase.New()
        If String.IsNullOrWhiteSpace(code) Then
            Me.Parsed = False
        Else
            Dim info As BSTcrInfo = BSTcrInfo.Parse(code)
            Me.Parsed = info IsNot Nothing
            If Me.Parsed Then
                Me.Code = code
                Me.RelativeInterval = New TcrInterval(info.RelativeInterval)
                Me.Caption = info.Caption
                Me.CompoundCaption = AcceptanceInterval.BuildCompoundCaptionFormat(code, Caption)
            End If
        End If
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="code">  The temperature coefficient code. </param>
    ''' <param name="lowerCoefficient"> The lower Coefficient. </param>
    ''' <param name="upperCoefficient"> The upper Coefficient. </param>
    ''' <param name="caption">        The caption. </param>
    Public Sub New(ByVal code As String, ByVal lowerCoefficient As Double, ByVal upperCoefficient As Double, ByVal caption As String)
        MyBase.New(code, New TcrInterval(lowerCoefficient, upperCoefficient), caption)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <param name="value"> The value. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="0")>
    Public Sub New(ByVal value As AcceptanceInterval)
        Me.New(value.Code, value.RelativeInterval, value.Caption)
    End Sub

    ''' <summary> Constructor for symmetric range and standard caption. </summary>
    ''' <param name="code">        The tolerance code. </param>
    ''' <param name="coefficient"> The coefficient. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="1")>
    Public Sub New(ByVal code As String, ByVal coefficient As Interval)
        MyBase.New(code, coefficient, BSTcrInfo.BuildCaption(coefficient.HighEndPoint))
    End Sub

    ''' <summary> Constructor for symmetric range. </summary>
    ''' <param name="code">  The temperature coefficient code. </param>
    ''' <param name="caption">        The caption. </param>
    Public Sub New(ByVal code As String, ByVal coefficient As Interval, ByVal caption As String)
        MyBase.New(code, coefficient, caption)
    End Sub

    ''' <summary> Constructor for symmetric range and standard caption. </summary>
    ''' <param name="code">        The tolerance code. </param>
    ''' <param name="coefficient"> The coefficient. </param>
    Public Sub New(ByVal code As String, ByVal coefficient As Double)
        Me.New(code, coefficient, BSTcrInfo.BuildCaption(coefficient))
    End Sub

    ''' <summary> Constructor for symmetric range. </summary>
    ''' <param name="code">  The temperature coefficient code. </param>
    ''' <param name="caption">        The caption. </param>
    Public Sub New(ByVal code As String, ByVal coefficient As Double, ByVal caption As String)
        Me.New(code, New ToleranceInterval(coefficient), caption)
    End Sub

    ''' <summary> The clone Constructor. </summary>
    ''' <param name="value"> The value. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="0")>
    Public Sub New(ByVal value As BSTcrInfo)
        Me.New(value.Code, value.RelativeInterval, value.Caption)
    End Sub

    ''' <summary> Gets the empty value. </summary>
    ''' <value> The empty. </value>
    Public Shared ReadOnly Property Empty As BSTcrInfo
        Get
            Return New BSTcrInfo(BSTcrInfo.EmptyCode, 0)
        End Get
    End Property

    ''' <summary> Gets the units caption. </summary>
    ''' <value> The units format. </value>
    Public Shared Property UnitsCaption As String = "ppm/°K"

    ''' <summary> Gets the caption format. </summary>
    ''' <value> The caption format. </value>
    Public Shared Property CaptionFormat As String = "±{0} " & UnitsCaption

    ''' <summary> Builds a caption. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String value using the 
    ''' <see cref="BSTcrInfo.CaptionFormat">caption format</see>. </returns>
    Public Shared Function BuildCaption(ByVal value As Double) As String
        Dim scaleFactor As Double = 1
        If BSTcrInfo.CaptionFormat.Contains("ppm") Then
            scaleFactor = 1000000.0
        End If
        Return BSTcrInfo.BuildCaption(value, scaleFactor, BSTcrInfo.CaptionFormat)
    End Function

    ''' <summary> Builds a caption. </summary>
    ''' <param name="value">       The value. </param>
    ''' <param name="scaleFactor"> The scale factor. </param>
    ''' <param name="format">      Describes the format to use. </param>
    ''' <returns> A String value using the
    ''' <see cref="BSTcrInfo.CaptionFormat">caption format</see>. </returns>
    Public Shared Function BuildCaption(ByVal value As Double, ByVal scaleFactor As Double, ByVal format As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, format, scaleFactor * value)
    End Function

    ''' <summary> The unknown Coefficient value. </summary>
    Public Shared Property UnknownValue As BSTcrInfo = New BSTcrInfo("?", 0.001)

    ''' <summary> Gets the unknown code. </summary>
    ''' <value> The unknown code. </value>
    Public Overloads Shared Property UnknownCode As String
        Get
            Return UnknownValue.Code
        End Get
        Set(value As String)
            BSTcrInfo.UnknownValue = New BSTcrInfo(value, BSTcrInfo.UnknownValue.RelativeInterval)
        End Set
    End Property

    ''' <summary> The unknown Coefficient value. </summary>
    Public Shared Property UserValue As BSTcrInfo = New BSTcrInfo("@", 0.001)

    ''' <summary> Gets the user code. </summary>
    ''' <value> The user code. </value>
    Public Overloads Shared Property UserCode As String
        Get
            Return UserValue.Code
        End Get
        Set(value As String)
            BSTcrInfo.UserValue = New BSTcrInfo(value, BSTcrInfo.UserValue.RelativeInterval)
        End Set
    End Property

    Private Shared _Dictionary As AcceptanceIntervalCollection
    ''' <summary> Gets the <see cref="AcceptanceIntervalCollection">dictionary</see>. </summary>
    ''' <returns> A Dictionary(Of String, CodedIntervalBase). </returns>
    Public Shared Function Dictionary() As AcceptanceIntervalCollection
        If BSTcrInfo._Dictionary Is Nothing OrElse BSTcrInfo._Dictionary.Count = 0 Then
            BSTcrInfo.BuildDictionary()
        End If
        Return BSTcrInfo._Dictionary
    End Function

    ''' <summary> Builds coded interval base dictionary. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The values. </param>
    Public Shared Sub BuildDictionary(ByVal values As AcceptanceIntervalCollection)
        BSTcrInfo._Dictionary = New AcceptanceIntervalCollection
        BSTcrInfo._Dictionary.Populate(values)
    End Sub

    ''' <summary> Builds Coefficient information dictionary. </summary>
    Private Shared Sub BuildDictionary()
        Dim dix As New AcceptanceIntervalCollection From {
            New BSTcrInfo("K", 0.000001),
            New BSTcrInfo("M", 0.000005),
            New BSTcrInfo("N", 0.00001),
            New BSTcrInfo("Z", 0.00002),  'TO_DO _ not right.
            New BSTcrInfo("Q", 0.000025),
            New BSTcrInfo("P", 0.000015),
            New BSTcrInfo("R", 0.00005),
            New BSTcrInfo("S", 0.0001),
            New BSTcrInfo("U", 0.00025),
            New BSTcrInfo("1", 0.0001),
            New BSTcrInfo("5", 0.0001),
            New BSTcrInfo("2", 0.00005),
            New BSTcrInfo("6", 0.00005),
            New BSTcrInfo("3", 0.000025),
            New BSTcrInfo("7", 0.000025),
            New BSTcrInfo("4", 0.00025),
            BSTcrInfo.UnknownValue,
            BSTcrInfo.UserValue
        }
        BSTcrInfo.BuildDictionary(dix)
        dix = Nothing
    End Sub

    ''' <summary> Parses. </summary>
    ''' <param name="code"> The code. </param>
    ''' <returns> A Temperature Coefficient Info. </returns>
    Public Shared Function Parse(ByVal code As String) As BSTcrInfo
        With BSTcrInfo.Dictionary
            If .Contains(code) Then
                Return New BSTcrInfo(.Item(code))
            Else
                Return Nothing
            End If
        End With
    End Function

    ''' <summary> Tries to parse a coded value. </summary>
    ''' <param name="code">    The code. </param>
    ''' <param name="value">   [in,out] The Margin Factor. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if Margin Factor code can be parsed. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#")>
    Public Shared Function TryParse(ByVal code As String, ByRef value As BSTcrInfo, ByRef details As String) As Boolean
        Dim affirmative As Boolean = False
        With BSTcrInfo.Dictionary
            If .Contains(code) Then
                value = New BSTcrInfo(.Item(code))
                affirmative = True
            Else
                details = String.Format("MarginFactor code '{0}' is unknown", code)
            End If
        End With
        Return affirmative
    End Function

    ''' <summary> Gets the sentinel indicating if the entered value is Empty defined. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsEmptyCode As Boolean
        Get
            Return String.Equals(Me.Code, BSTcrInfo.EmptyCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating if the entered code is unknown. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsUnknownCode As Boolean
        Get
            Return String.Equals(Me.Code, BSTcrInfo.UnknownCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating if the entered value is user defined. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsUserCode As Boolean
        Get
            Return String.Equals(Me.Code, BSTcrInfo.UserCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

End Class

﻿''' <summary> Defines the Temperature Coefficient of Resistance (TCR) acceptance interval. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="9/22/2014" by="David" revision=""> Created. </history>
Public Class TcrInfo
    Inherits AcceptanceInterval

    ''' <summary> Constructor. </summary>
    ''' <param name="code"> The temperature coefficient code. </param>
    Public Sub New(ByVal code As String)
        MyBase.New()
        If String.IsNullOrWhiteSpace(code) Then
            Me.Parsed = False
        Else
            Dim info As TcrInfo = TcrInfo.Parse(code)
            Me.Parsed = info IsNot Nothing
            If Me.Parsed Then
                Me.Code = code
                Me.RelativeInterval = New TcrInterval(info.RelativeInterval)
                Me.Caption = info.Caption
                Me.CompoundCaption = AcceptanceInterval.BuildCompoundCaptionFormat(code, Caption)
            End If
        End If
    End Sub

    ''' <summary> Constructor for symmetric interval. </summary>
    ''' <param name="code">        The temperature coefficient code. </param>
    ''' <param name="coefficient"> The coefficient interval. </param>
    ''' <param name="caption">     The caption. </param>
    Public Sub New(ByVal code As String, ByVal coefficient As Interval, ByVal caption As String)
        MyBase.New(code, coefficient, caption)
    End Sub


    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <param name="value"> The acceptance interval. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="0")>
    Protected Sub New(ByVal value As AcceptanceInterval)
        Me.New(value.Code, value.RelativeInterval.HighEndPoint, value.Caption)
    End Sub

    ''' <summary> Constructor for symmetric interval and standard caption. </summary>
    ''' <param name="code">        The tolerance code. </param>
    ''' <param name="coefficient"> The coefficient  interval. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="1")>
    Public Sub New(ByVal code As String, ByVal coefficient As Interval)
        Me.New(code, coefficient, TcrInfo.BuildCaption(coefficient.HighEndPoint))
    End Sub

    ''' <summary> Constructor for symmetric interval and standard caption. </summary>
    ''' <param name="code">        The tolerance code. </param>
    ''' <param name="coefficient"> The coefficient. </param>
    Public Sub New(ByVal code As String, ByVal coefficient As Double)
        Me.New(code, coefficient, TcrInfo.BuildCaption(coefficient))
    End Sub

    ''' <summary> Constructor for symmetric interval. </summary>
    ''' <param name="code">  The temperature coefficient code. </param>
    ''' <param name="caption">        The caption. </param>
    Public Sub New(ByVal code As String, ByVal coefficient As Double, ByVal caption As String)
        Me.New(code, New TcrInterval(coefficient), caption)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="lowerCoefficient"> The lower Coefficient. </param>
    ''' <param name="upperCoefficient"> The upper Coefficient. </param>
    Public Sub New(ByVal lowerCoefficient As Double, ByVal upperCoefficient As Double)
        Me.New(TcrInfo.UserCode, New TcrInterval(lowerCoefficient, upperCoefficient), TcrInfo.BuildCaption(lowerCoefficient, upperCoefficient, TcrInfo.CaptionFormat))
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="code">  The temperature coefficient code. </param>
    ''' <param name="lowerCoefficient"> The lower Coefficient. </param>
    ''' <param name="upperCoefficient"> The upper Coefficient. </param>
    ''' <param name="caption">        The caption. </param>
    Public Sub New(ByVal code As String, ByVal lowerCoefficient As Double, ByVal upperCoefficient As Double, ByVal caption As String)
        Me.New(code, New TcrInterval(lowerCoefficient, upperCoefficient), caption)
    End Sub

    ''' <summary> The cloning constructor. </summary>
    ''' <param name="value"> The value. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="0")>
    Public Sub New(ByVal value As TcrInfo)
        Me.New(value.Code, value.RelativeInterval.LowEndPoint, value.RelativeInterval.HighEndPoint, value.Caption)
    End Sub

    ''' <summary> Gets the empty. </summary>
    ''' <value> The empty. </value>
    Public Shared ReadOnly Property Empty As TcrInfo
        Get
            Return New TcrInfo(TcrInfo.EmptyCode, 0)
        End Get
    End Property

    ''' <summary> Gets the units caption. </summary>
    ''' <value> The units format. </value>
    Public Shared Property UnitsCaption As String = "ppm/°C"

    ''' <summary> Gets the caption format. </summary>
    ''' <value> The caption format. </value>
    Public Shared Property CaptionFormat As String = "±{0} " & TcrInfo.UnitsCaption

    ''' <summary> Builds a caption. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String value using the 
    ''' <see cref="TcrInfo.CaptionFormat">caption format</see>. </returns>
    Public Shared Function BuildCaption(ByVal value As Double) As String
        Dim scaleFactor As Double = 1
        If TcrInfo.CaptionFormat.Contains("ppm") Then
            scaleFactor = 1000000.0
        End If
        Return TcrInfo.BuildCaption(value, scaleFactor, TcrInfo.CaptionFormat)
    End Function

    ''' <summary> Builds a caption. </summary>
    ''' <param name="value">       The value. </param>
    ''' <param name="scaleFactor"> The scale factor. </param>
    ''' <param name="format">      Describes the format to use. </param>
    ''' <returns> A String value using the
    ''' <see cref="TcrInfo.CaptionFormat">caption format</see>. </returns>
    Public Shared Function BuildCaption(ByVal value As Double, ByVal scaleFactor As Double, ByVal format As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, format, scaleFactor * value)
    End Function

    ''' <summary> The unknown Coefficient value. </summary>
    Public Shared Property UnknownValue As TcrInfo = New TcrInfo("??", 0.00025)

    ''' <summary> Gets the unknown code. </summary>
    ''' <value> The unknown code. </value>
    Public Overloads Shared Property UnknownCode As String
        Get
            Return UnknownValue.Code
        End Get
        Set(value As String)
            TcrInfo.UnknownValue = New TcrInfo(value, TcrInfo.UnknownValue.RelativeInterval)
        End Set
    End Property

    ''' <summary> The user coefficient value. </summary>
    Public Shared Property UserValue As TcrInfo = New TcrInfo("@@", 0.001)

    Public Overloads Shared Property UserCode As String
        Get
            Return UserValue.Code
        End Get
        Set(value As String)
            TcrInfo.UserValue = New TcrInfo(value, TcrInfo.UserValue.RelativeInterval)
        End Set
    End Property

    Private Shared _Dictionary As AcceptanceIntervalCollection
    ''' <summary> Gets the <see cref="AcceptanceIntervalCollection">dictionary</see>. </summary>
    ''' <returns> A Dictionary(Of String, CodedIntervalBase). </returns>
    Public Shared Function Dictionary() As AcceptanceIntervalCollection
        If TcrInfo._Dictionary Is Nothing OrElse TcrInfo._Dictionary.Count = 0 Then
            TcrInfo.BuildDictionary()
        End If
        Return TcrInfo._Dictionary
    End Function

    ''' <summary> Builds coded interval base dictionary. </summary>
    Public Shared Sub BuildDictionary(ByVal values As AcceptanceIntervalCollection)
        TcrInfo._Dictionary = New AcceptanceIntervalCollection
        TcrInfo._Dictionary.Populate(values)
    End Sub

    ''' <summary> Builds Coefficient information dictionary. </summary>
    Private Shared Sub BuildDictionary()
        Dim dix As New AcceptanceIntervalCollection From {
            New TcrInfo("00", -0.000125, -0.000075, "-100±25 " & TcrInfo.UnitsCaption), ' Commercial
            New TcrInfo("01", 0.0001),  ' Commercial
            New TcrInfo("02", 0.00005), ' Commercial
            New TcrInfo("03", 0.000025), ' Commercial
            New TcrInfo("10", 0.00002), ' Commercial
            New TcrInfo("11", 0.000015), ' Commercial
            New TcrInfo("12", 0.00001), ' Commercial
            New TcrInfo("13", 0.000005), ' Screened
            New TcrInfo("04", 0.0003),  ' Screened
            New TcrInfo("05", 0.0001),  ' Screened
            New TcrInfo("06", 0.00005), ' Screened
            New TcrInfo("07", 0.00025), ' Screened
            New TcrInfo("14", 0.00002), ' Screened
            New TcrInfo("15", 0.000015), ' Screened
            New TcrInfo("16", 0.00001), ' Screened
            New TcrInfo("17", 0.000005), ' Screened
            New TcrInfo("99", 0.00025), ' Commercial
            New TcrInfo("08", 0.00025), ' Screened
            TcrInfo.UnknownValue
        }
        TcrInfo.BuildDictionary(dix)
        dix = Nothing
    End Sub

    ''' <summary> Tries to parse a coded value. </summary>
    ''' <param name="code">    The code. </param>
    ''' <param name="value">   [in,out] The Margin Factor. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if Margin Factor code can be parsed. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#")>
    Public Shared Function TryParse(ByVal code As String, ByRef value As TcrInfo, ByRef details As String) As Boolean
        Dim affirmative As Boolean = False
        With TcrInfo.Dictionary
            If .Contains(code) Then
                value = New TcrInfo(.Item(code))
                affirmative = True
            Else
                details = String.Format("MarginFactor code '{0}' is unknown", code)
            End If
        End With
        Return affirmative
    End Function

    ''' <summary> Parses. </summary>
    ''' <param name="code"> The code. </param>
    ''' <returns> A TcrInfo. </returns>
    Public Shared Function Parse(ByVal code As String) As TcrInfo
        With TcrInfo.Dictionary
            If .Contains(code) Then
                Return New TcrInfo(.Item(code))
            Else
                Return Nothing
            End If
        End With
    End Function

    ''' <summary> Gets the sentinel indicating if the entered value is Empty defined. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsEmptyCode As Boolean
        Get
            Return String.Equals(Me.Code, TcrInfo.EmptyCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating if the entered code is unknown. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsUnknownCode As Boolean
        Get
            Return String.Equals(Me.Code, TcrInfo.UnknownCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating if the entered value is user defined. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsUserCode As Boolean
        Get
            Return String.Equals(Me.Code, TcrInfo.UserCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

End Class


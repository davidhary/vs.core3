﻿''' <summary> A sealed class defining the acceptance Interval. </summary>
''' <remarks> Defines a class that encodes intervals based on offsets from a nominal value. </remarks>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="9/20/2014" by="David" revision="2.1.5376"> Created. </history>
Public MustInherit Class AcceptanceInterval

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Constructor for symmetric margin factors. </summary>
    ''' <param name="code">         The code. </param>
    ''' <param name="marginFactor"> The margin factor. </param>
    ''' <param name="caption">      The caption. </param>
    Protected Sub New(ByVal code As String, ByVal marginFactor As Interval, ByVal caption As String)
        MyBase.New()
        Me.Code = code
        If marginFactor Is Nothing Then
            Me.RelativeInterval = isr.Core.Engineering.Interval.Empty
        Else
            Me.RelativeInterval = isr.Core.Engineering.Interval.CreateInstance(marginFactor)
        End If
        Me.Caption = caption
        Me.CompoundCaption = AcceptanceInterval.BuildCompoundCaptionFormat(code, caption)
        Me.Parsed = True
    End Sub

    ''' <summary> The clone Constructor. </summary>
    ''' <param name="value"> The value. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="0")>
    Protected Sub New(ByVal value As AcceptanceInterval)
        Me.New(value.Code, value.RelativeInterval, value.Caption)
    End Sub

    ''' <summary> Gets or sets the acceptance interval code. </summary>
    ''' <value> The code. </value>
    Public Property Code As String

    ''' <summary> Gets or sets the sentinel indicating that the interval was parsed using the interval code. </summary>
    ''' <value> The sentinel indicating that the interval was parsed. </value>
    Public Property Parsed As Boolean

    Private _RelativeInterval As Interval

    ''' <summary> Gets or sets the relative interval. This is the interval, which actual values depend
    ''' on the nominal value. The relative interval can be compared to the relative deviation from
    ''' the nominal value. For example, +/- 5%. </summary>
    ''' <value> The margin factor. </value>
    Public Property RelativeInterval As Interval
        Get
            Return Me._RelativeInterval
        End Get
        Set(value As Interval)
            Me._RelativeInterval = value
            ' update the acceptance interval.
            Me.NominalValue = Me.NominalValue
        End Set
    End Property

    Private _NominalValue As Double
    ''' <summary> Gets or sets the nominal value. </summary>
    ''' <remarks> This also updates the interval based on the relative interval. </remarks>
    ''' <value> The nominal value. </value>
    Public Property NominalValue As Double
        Get
            Return Me._NominalValue
        End Get
        Set(value As Double)
            Me._NominalValue = value
            Me.Interval = New NominalInterval(value * (1 + Me.RelativeInterval.LowEndPoint), value * (1 + Me.RelativeInterval.HighEndPoint))
        End Set
    End Property

    ''' <summary> Gets or sets the acceptance interval with its end points. The interval defines the
    ''' actual lower and upper levels. </summary>
    ''' <value> The acceptance interval. </value>
    Public Property Interval As Interval

    ''' <summary> Query if this object is empty; that is the limits range is zero. </summary>
    ''' <returns> <c>true</c> if empty; otherwise <c>false</c> </returns>
    Public Function IsEmpty() As Boolean
        Return Me.Interval.IsEmpty
    End Function

    ''' <summary> Gets the unknown code. </summary>
    ''' <value> The unknown code. </value>
    Public Shared Property EmptyCode As String = ""

    ''' <summary> Gets the sentinel indicating if the entered code is empty. </summary>
    ''' <value> <c>True</c> if empty code. </value>
    Public MustOverride ReadOnly Property IsEmptyCode As Boolean

    ''' <summary> Gets the unknown code. </summary>
    ''' <value> The unknown code. </value>
    Public Shared Property UnknownCode As String = "?"

    ''' <summary> Gets the sentinel indicating if the entered code is unknown. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public MustOverride ReadOnly Property IsUnknownCode As Boolean

    ''' <summary> Gets the user code. </summary>
    ''' <value> The user code. </value>
    Public Shared Property UserCode As String = "@"

    ''' <summary> Gets the sentinel indicating if the entered value is user defined. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public MustOverride ReadOnly Property IsUserCode As Boolean

    ''' <summary> Checks if the acceptance interval contains the value. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if the acceptance interval contains the value; false otherwise. </returns>
    Public Function Contains(ByVal value As Double) As Boolean
        Return Me.Interval.Contains(value)
    End Function

    ''' <summary> Gets or sets the caption. </summary>
    ''' <value> The caption. </value>
    Public Property Caption As String

    ''' <summary> Gets or sets the compound caption. </summary>
    ''' <value> The compound caption. </value>
    Public Property CompoundCaption As String

    ''' <summary> Gets or sets the compound caption format. </summary>
    ''' <value> The compound caption format. </value>
    Public Shared Property CompoundCaptionFormat As String = "{0}: {1}"

    ''' <summary> Builds compound caption format. </summary>
    ''' <param name="code">    The code. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildCompoundCaptionFormat(ByVal code As String, ByVal caption As String) As String
        If String.Equals(code, AcceptanceInterval.EmptyCode) Then
            Return AcceptanceInterval.EmptyCode
        Else
            Return String.Format(AcceptanceInterval.CompoundCaptionFormat, code, caption)
        End If
    End Function

End Class

''' <summary> Dictionary of acceptance intervals. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="9/22/2014" by="David" revision=""> Created. </history>
Public Class AcceptanceIntervalCollection
    Inherits Collections.ObjectModel.KeyedCollection(Of String, AcceptanceInterval)

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.new()
    End Sub

    ''' <summary> Gets key for item. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The item. </param>
    ''' <returns> The key for item. </returns>
    Protected Overrides Function GetKeyForItem(item As AcceptanceInterval) As String
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
        Return item.Code
    End Function

    ''' <summary> Populates the given values. </summary>
    ''' <param name="values"> The values. </param>
    Public Overloads Sub Populate(ByVal values As AcceptanceIntervalCollection)
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Me.Clear()
        For Each value As AcceptanceInterval In values
            Me.Add(value)
        Next
    End Sub

    Private _CompoundCaptions As CompoundCaptionCollection

    ''' <summary> Gets the compound captions. </summary>
    ''' <value> The compound captions. </value>
    Public ReadOnly Property CompoundCaptions As CompoundCaptionCollection
        Get
            If Me._compoundCaptions Is Nothing OrElse Me._compoundCaptions.Count = 0 Then
                Me._compoundCaptions = New CompoundCaptionCollection
                Me._compoundCaptions.Populate(Me)
            End If
            Return Me._compoundCaptions
        End Get
    End Property

End Class

''' <summary> Collection of compound captions. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="12/20/2014" by="David" revision=""> Created. </history>
Public Class CompoundCaptionCollection
    Inherits Collections.ObjectModel.Collection(Of KeyValuePair(Of String, String))

    ''' <summary> Adds code. </summary>
    ''' <param name="code">            The code. </param>
    ''' <param name="compoundCaption"> The compound caption. </param>
    Public Overloads Sub Add(ByVal code As String, ByVal compoundCaption As String)
        Me.Add(New KeyValuePair(Of String, String)(code, compoundCaption))
    End Sub

    ''' <summary> Adds code. </summary>
    ''' <param name="acceptanceInterval"> The acceptance interval to add. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="0")>
    Public Overloads Sub Add(ByVal acceptanceInterval As AcceptanceInterval)
        Me.Add(acceptanceInterval.Code, acceptanceInterval.CompoundCaption)
    End Sub

    ''' <summary> Populates the given values. </summary>
    ''' <param name="values"> The values. </param>
    Public Overloads Sub Populate(ByVal values As AcceptanceIntervalCollection)
        Me.Clear()
        If values IsNot Nothing Then
            For Each value As AcceptanceInterval In values
                Me.Add(value)
            Next
        End If
    End Sub

End Class
﻿Imports System.ComponentModel
''' <summary> A resistance implementation of Ohms law. </summary>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="6/28/2018" by="David" revision=""> Created. </history>
Public Class Resistance

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="resistance"> The resistance or <see cref="Double.NaN"/> if not
    '''                           <see cref="HasValue"/>. </param>
    Public Sub New(ByVal resistance As Resistance)
        Me.New(Resistance.Validated(resistance).Voltage, resistance.Current)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="voltage"> The voltage. </param>
    ''' <param name="current"> The current. </param>
    Public Sub New(ByVal voltage As Double, ByVal current As Double)
        Me.New
        Me.Current = current
        Me.Voltage = voltage
    End Sub

    ''' <summary> Validated the given resistance. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resistance"> The resistance or <see cref="Double.NaN"/> if not
    '''                           <see cref="HasValue"/>. </param>
    ''' <returns> A Resistance. </returns>
    Public Shared Function Validated(ByVal resistance As Resistance) As Resistance
        If resistance Is Nothing Then Throw New ArgumentNullException(NameOf(resistance))
        Return resistance
    End Function

    ''' <summary> Gets the sentinel indicating if the measure has a non zero current value. </summary>
    ''' <value> The sentinel indicating if the measure has a non zero current value. </value>
    Public ReadOnly Property HasValue As Boolean
        Get
            Return Me.Current <> 0 AndAlso Math.Sign(Me.Voltage) = Math.Sign(Me.Current)
        End Get
    End Property

    ''' <summary> Gets the resistance. </summary>
    ''' <value> The resistance or <see cref="Double.NaN"/> if not <see cref="HasValue"/>. </value>
    Public ReadOnly Property Resistance As Double
        Get
            If Me.HasValue Then
                Return Me.Voltage / Me.Current
            Else
                Return Double.NaN
            End If
        End Get
    End Property

    ''' <summary> Gets or sets the voltage. </summary>
    ''' <value> The voltage. </value>
    Public Property Voltage As Double

    ''' <summary> Gets or sets the current. </summary>
    ''' <value> The current. </value>
    Public Property Current As Double

    ''' <summary> Query if 'value' is hit compliance. </summary>
    ''' <param name="value">           True to value. </param>
    ''' <param name="limit">           The source limit. </param>
    ''' <param name="complianceLimit"> The compliance limit. </param>
    ''' <returns> <c>true</c> if hit compliance; otherwise <c>false</c> </returns>
    Public Shared Function IsHitCompliance(ByVal value As Double, ByVal limit As Double, ByVal complianceLimit As Double) As Boolean
        value = Math.Abs(value)
        If complianceLimit <= 0 Then
            Throw New ArgumentException($"N{NameOf(complianceLimit)}={complianceLimit} must be positive")
        ElseIf limit = 0 Then

            Return Math.Abs(value) < complianceLimit
        Else
            Return Math.Abs(value / limit - 1) < complianceLimit
        End If
    End Function

    ''' <summary> Query if <see cref="Voltage"/> 'value' is hit compliance. </summary>
    ''' <param name="limit">           The source limit. </param>
    ''' <param name="complianceLimit"> The compliance limit. </param>
    ''' <returns> <c>true</c> if hit compliance; otherwise <c>false</c> </returns>
    Public Function IsHitVoltageCompliance(ByVal limit As Double, ByVal complianceLimit As Double) As Boolean
        Return Engineering.Resistance.IsHitCompliance(Me.Voltage, limit, complianceLimit)
    End Function

    ''' <summary> Query if 'limit' is hit current compliance. </summary>
    ''' <param name="limit">           The source limit. </param>
    ''' <param name="complianceLimit"> The compliance limit. </param>
    ''' <returns> <c>true</c> if hit current compliance; otherwise <c>false</c> </returns>
    Public Function IsHitCurrentCompliance(ByVal limit As Double, ByVal complianceLimit As Double) As Boolean
        Return Engineering.Resistance.IsHitCompliance(Me.Current, limit, complianceLimit)
    End Function

End Class

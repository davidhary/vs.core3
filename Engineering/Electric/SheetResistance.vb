﻿Imports System.ComponentModel
''' <summary> A sheet resistance. </summary>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="6/28/2018" by="David" revision=""> Created. </history>
Public Class SheetResistance
    Inherits Resistance

    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="sheetResistance"> The sheet resistance or <see cref="Double.NaN"/> if no value. </param>
    Public Sub New(ByVal sheetResistance As SheetResistance)
        MyBase.New(sheetResistance)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="voltage"> The voltage. </param>
    ''' <param name="current"> The current. </param>
    Public Sub New(ByVal voltage As Double, ByVal current As Double)
        MyBase.New(voltage, current)
    End Sub

    ''' <summary> Gets the sheet resistance. </summary>
    ''' <value> The sheet resistance or <see cref="Double.NaN"/> if no value. </value>
    Public ReadOnly Property SheetResistance As Double
        Get
            If Me.HasValue Then
                Return Me.Resistance * Math.PI / Math.Log(2)
            Else
                Return Double.NaN
            End If
        End Get
    End Property

End Class

''' <summary> Collection of sheet resistances. </summary>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="4/21/2018" by="David" revision=""> Created. </history>
Public Class SheetResistanceCollection
    Inherits ObjectModel.Collection(Of Core.Engineering.SheetResistance)

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="sheetResistances"> The sheet resistances. </param>
    Public Sub New(sheetResistances As SheetResistanceCollection)
        Me.New
        If sheetResistances IsNot Nothing Then
            For Each sr As SheetResistance In sheetResistances
                Me.Add(New SheetResistance(sr))
            Next
        End If
    End Sub

    ''' <summary> Gets the number of sheet resistances. </summary>
    ''' <value> The number of sheet resistances. </value>
    Public ReadOnly Property SheetResistanceCount As Integer

    ''' <summary> Gets the sheet resistance. </summary>
    ''' <value> The sheet resistance or <see cref="Double.NaN"/> if <see cref="SheetResistanceCount"/> is zero. </value>
    Public ReadOnly Property SheetResistance As Double?
        Get
            Dim sum As Double = 0
            Dim count As Integer = 0
            For Each sr As Core.Engineering.SheetResistance In Me
                If sr.HasValue Then
                    sum += sr.SheetResistance
                    count += 1
                End If
            Next
            Me._SheetResistanceCount = count
            If count > 0 Then
                Return sum / Me.Count
            Else
                Return New Double?
            End If
        End Get
    End Property

    ''' <summary> Gets the average measured Voltage. </summary>
    ''' <value> The average measured Voltage or <see cref="Double.NaN"/> if <see cref="SheetResistanceCount"/> is zero. </value>
    Public ReadOnly Property AverageAbsoluteVoltage As Double?
        Get
            Dim sum As Double = 0
            Dim count As Integer = 0
            For Each sr As Core.Engineering.SheetResistance In Me
                If sr.HasValue Then
                    sum += Math.Abs(sr.Voltage)
                    count += 1
                End If
            Next
            If count > 0 Then
                Return sum / Me.Count
            Else
                Return New Double?
            End If
        End Get
    End Property

    ''' <summary> Gets the average source current. </summary>
    ''' <value> The average source current or <see cref="Double.NaN"/> if <see cref="SheetResistanceCount"/> is zero. </value>
    Public ReadOnly Property AbsoluteCurrent As Double?
        Get
            Dim sum As Double = 0
            Dim count As Integer = 0
            For Each sr As Core.Engineering.SheetResistance In Me
                If sr.HasValue Then
                    sum += Math.Abs(sr.Current)
                    count += 1
                End If
            Next
            If count > 0 Then
                Return sum / Me.Count
            Else
                Return New Double?
            End If
        End Get
    End Property

End Class



﻿Imports System.ComponentModel
''' <summary> A Wheatstone bridge with balancing components. </summary>
''' <remarks>
''' Without loss of generality, the balance bridge layout is defined by assigning series and
''' parallel (shunt) elements to each of the compensated edges of a 'Naked' Wheatstone bridge. 
''' </remarks>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="9/15/2017" by="David">             > Created. </history>
Public Class BalanceBridge
    Inherits Wheatstone

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <param name="nakedBridge"> The naked bridge. </param>
    ''' <param name="balanceValues">      The values. </param>
    ''' <param name="balanceLayout">      The layout. </param>
    Public Sub New(ByVal nakedBridge As Wheatstone, ByVal balanceValues As IEnumerable(Of Double), ByVal balanceLayout As BalanceLayout)
        MyBase.New(BalanceBridge.ToWheatstone(Wheatstone.ValidatedBridge(nakedBridge),
                                              BalanceBridge.ValidatedBalanceValues(balanceValues), balanceLayout))
        Me._NakedBridge = Wheatstone.ValidatedBridge(nakedBridge)
        Me._BridgeBalance = New BridgeBalance(balanceValues, balanceLayout)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="nakedBridge"> The naked bridge. </param>
    ''' <param name="layout">      The layout. </param>
    Public Sub New(ByVal nakedBridge As Wheatstone, ByVal layout As BalanceLayout)
        Me.New(nakedBridge, New Double() {0, 0, 0, 0}, layout)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="nakedBridge">   The naked bridge. </param>
    ''' <param name="bridgeBalance"> The bridge balance. </param>
    Public Sub New(ByVal nakedBridge As Wheatstone, ByVal bridgeBalance As BridgeBalance)
        MyBase.New(BalanceBridge.ToWheatstone(Wheatstone.ValidatedBridge(nakedBridge), BridgeBalance.ValidatedBridgeBalance(bridgeBalance)))
        Me._NakedBridge = Wheatstone.ValidatedBridge(nakedBridge)
        Me._BridgeBalance = bridgeBalance
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="bridge"> The bridge. </param>
    Public Sub New(ByVal bridge As BalanceBridge)
        Me.New(New Wheatstone(BalanceBridge.ValidatedBalanceBridge(bridge).NakedBridge),
                              BridgeBalance.ValidatedBridgeBalance(bridge.BridgeBalance))
    End Sub

    ''' <summary> Makes a deep copy of this object. </summary>
    ''' <returns> A copy of this object. </returns>
    Public Function Clone() As BalanceBridge
        Return New BalanceBridge(Me)
    End Function

    ''' <summary> Validated balance bridge. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="bridge"> The bridge. </param>
    ''' <returns> A BalanceBridge. </returns>
    Public Shared Function ValidatedBalanceBridge(ByVal bridge As BalanceBridge) As BalanceBridge
        If bridge Is Nothing Then Throw New ArgumentNullException(NameOf(bridge))
        If bridge.BridgeBalance Is Nothing Then Throw New ArgumentNullException(NameOf(bridge))
        Return bridge
    End Function

    ''' <summary> Enumerates validated balance values in this collection. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process validated balance values in this
    ''' collection.
    ''' </returns>
    Public Shared Function ValidatedBalanceValues(ByVal values As IEnumerable(Of Double)) As IEnumerable(Of Double)
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If values.Count <> 4 Then Throw New InvalidOperationException($"{NameOf(values)} must have 4 elements")
        Return values
    End Function

#End Region

#Region " EQUALS "

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso Me.GetType() Is obj.GetType() AndAlso Me.Equals(CType(obj, BalanceBridge))
    End Function

    ''' <summary> Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits. </summary>
    ''' <remarks> The bridges are the same if the have the same naked bridge and balance. </remarks>
    ''' <param name="other"> Specifies the other <see cref="BalanceBridge">Balance Bridge</see>
    ''' to compare for equality with this instance. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As BalanceBridge) As Boolean
        If other Is Nothing Then
            Return False
        ElseIf Me.NakedBridge Is Nothing OrElse other.NakedBridge Is Nothing Then
            Return False
        ElseIf Me.BridgeBalance Is Nothing OrElse other.BridgeBalance Is Nothing Then
            Return False
        Else
            Return Me.NakedBridge.Equals(other.NakedBridge) AndAlso Me.BridgeBalance.Equals(other.BridgeBalance)
        End If
    End Function

    ''' <summary> Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits. </summary>
    ''' <remarks> The bridges are the same if the have the same naked bridge and balance. </remarks>
    ''' <param name="other">     Specifies the other <see cref="BalanceBridge">Balance Bridge</see>
    ''' to compare for equality with this instance. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
    ''' values are compared based on their end points. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As BalanceBridge, ByVal tolerance As Double) As Boolean
        If other Is Nothing Then
            Return False
        ElseIf Me.NakedBridge Is Nothing OrElse other.NakedBridge Is Nothing Then
            Return False
        ElseIf Me.BridgeBalance Is Nothing OrElse other.BridgeBalance Is Nothing Then
            Return False
        Else
            Return Me.BridgeBalance.Equals(other.BridgeBalance, tolerance) AndAlso
                   Me.NakedBridge.Equals(other.NakedBridge, tolerance) AndAlso Wheatstone.Equals(Me, other, tolerance)
        End If
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Function Equals(ByVal left As BalanceBridge, ByVal right As BalanceBridge) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Function

    ''' <summary>
    ''' Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits.
    ''' </summary>
    ''' <remarks> The bridges are the same if the have the same naked bridge and balance. </remarks>
    ''' <param name="left">      Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right">     Specifies the right hand side argument of the binary operation. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
    '''                          values are compared based on their end points. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Shared Function Equals(ByVal left As BalanceBridge, ByVal right As BalanceBridge, ByVal tolerance As Double) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse (left IsNot Nothing) AndAlso left.Equals(right, tolerance)
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Operator =(ByVal left As BalanceBridge, ByVal right As BalanceBridge) As Boolean
        Return BalanceBridge.Equals(left, right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Operator <>(ByVal left As BalanceBridge, ByVal right As BalanceBridge) As Boolean
        Return Not BalanceBridge.Equals(left, right)
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <returns> An <see cref="System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.NakedBridge.GetHashCode Xor Me.BridgeBalance.GetHashCode
    End Function

#End Region

#Region " CONVERTERS "

    ''' <summary> Converts this object to a Wheatstone bridge. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="nakedBridge"> The naked bridge. </param>
    ''' <param name="balanceValues">      The balance values. </param>
    ''' <param name="balanceLayout">      The balance layout. </param>
    ''' <returns> The given data converted to a Wheatstone. </returns>
    Public Shared Function ToWheatstone(ByVal nakedBridge As Wheatstone, ByVal balanceValues As IEnumerable(Of Double),
                                        ByVal balanceLayout As BalanceLayout) As Wheatstone
        If nakedBridge Is Nothing Then Throw New ArgumentNullException(NameOf(nakedBridge))
        If balanceValues Is Nothing Then Throw New ArgumentNullException(NameOf(balanceValues))
        Select Case balanceLayout
            Case BalanceLayout.TopShuntBottomSeries
                Return New Wheatstone(Resistor.ShuntConductor(nakedBridge.TopRight, balanceValues(TopShuntBottomSeriesIndex.TopRightShunt)),
                                      balanceValues(TopShuntBottomSeriesIndex.BottomRightSeries) + nakedBridge.BottomRight,
                                      balanceValues(TopShuntBottomSeriesIndex.BottomLeftSeries) + nakedBridge.BottomLeft,
                                      Resistor.ShuntConductor(nakedBridge.TopLeft, balanceValues(TopShuntBottomSeriesIndex.TopLeftShunt)))
            Case BalanceLayout.RightShuntRightSeries
                Return New Wheatstone(Resistor.ShuntConductor(nakedBridge.TopRight,
                                                              balanceValues(RightShuntRightSeriesIndex.TopRightShunt)) +
                                                                    balanceValues(RightShuntRightSeriesIndex.TopRightSeries),
                                      Resistor.ShuntConductor(nakedBridge.BottomRight,
                                                              balanceValues(RightShuntRightSeriesIndex.BottomRightShunt)) +
                                                                    balanceValues(RightShuntRightSeriesIndex.BottomRightSeries),
                                      nakedBridge.BottomLeft,
                                      nakedBridge.TopLeft)
            Case Else
                Throw New InvalidOperationException($"Unhandled layout {balanceLayout}")
        End Select
    End Function

    ''' <summary> Converts this object to a Wheatstone bridge. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="nakedBridge">   The naked bridge. </param>
    ''' <param name="bridgeBalance"> The bridge balance. </param>
    ''' <returns> The given data converted to a Wheatstone. </returns>
    Public Shared Function ToWheatstone(ByVal nakedBridge As Wheatstone, ByVal bridgeBalance As BridgeBalance) As Wheatstone
        If nakedBridge Is Nothing Then Throw New ArgumentNullException(NameOf(nakedBridge))
        If bridgeBalance Is Nothing Then Throw New ArgumentNullException(NameOf(bridgeBalance))
        Return BalanceBridge.ToWheatstone(nakedBridge, bridgeBalance.Values, bridgeBalance.Layout)
    End Function

#End Region

#Region " COMPONENETS "

    Private _BridgeBalance As BridgeBalance

    ''' <summary> Gets or sets the bridge balance. </summary>
    ''' <value> The bridge balance. </value>
    Public Property BridgeBalance As BridgeBalance
        Get
            Return Me._BridgeBalance
        End Get
        Set(ByVal value As BridgeBalance)
            If value IsNot Nothing AndAlso Not value.Equals(Me.BridgeBalance) Then
                Me._BridgeBalance = value
                MyBase.Initialize(BalanceBridge.ToWheatstone(Me.NakedBridge, Me.BridgeBalance))
            End If
        End Set
    End Property

    Private _NakedBridge As Wheatstone

    ''' <summary> Gets or sets the naked bridge. </summary>
    ''' <value> The naked bridge. </value>
    Public Property NakedBridge As Wheatstone
        Get
            Return Me._NakedBridge
        End Get
        Set(value As Wheatstone)
            If value IsNot Nothing AndAlso Not value.Equals(Me.NakedBridge) Then
                Me._NakedBridge = value
                MyBase.Initialize(BalanceBridge.ToWheatstone(Me.NakedBridge, Me.BridgeBalance))
            End If
        End Set
    End Property

    ''' <summary> Query if the bridge is valid. </summary>
    ''' <returns> <c>true</c> if valid; otherwise <c>false</c> </returns>
    Public Overrides Function IsValid() As Boolean
        Return MyBase.IsValid AndAlso Me.BridgeBalance.IsValid
    End Function

#End Region

#Region " COMPENSATORS "

    ''' <summary> Computes shunt only compensation for the bridge. </summary>
    ''' <param name="bridge">        The bridge. </param>
    ''' <param name="balanceLayout"> The balance layout. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the shunt compensations in this
    ''' collection.
    ''' </returns>
    Public Shared Function ShuntCompensation(ByVal bridge As Wheatstone, ByVal balanceLayout As BalanceLayout) As IEnumerable(Of Double)
        If bridge Is Nothing Then Throw New ArgumentNullException(NameOf(bridge))
        Dim values As Double() = New Double() {0, 0, 0, 0}
        If balanceLayout = BalanceLayout.TopShuntBottomSeries Then
            If bridge.ProductImbalance > 0 Then
                values(TopShuntBottomSeriesIndex.TopRightShunt) = bridge.BalanceDeviation / bridge.TopRight
                values(TopShuntBottomSeriesIndex.TopLeftShunt) = Conductor.OpenConductance
            ElseIf bridge.ProductImbalance < 0 Then
                values(TopShuntBottomSeriesIndex.TopLeftShunt) = -bridge.BalanceDeviation / (bridge.TopLeft * bridge.Balance)
                values(TopShuntBottomSeriesIndex.TopRightShunt) = Conductor.OpenConductance
            Else
                ' open shunts
                values(TopShuntBottomSeriesIndex.TopRightShunt) = Conductor.OpenConductance
                values(TopShuntBottomSeriesIndex.TopLeftShunt) = Conductor.OpenConductance
            End If
            ' short resistors
            values(TopShuntBottomSeriesIndex.BottomLeftSeries) = Resistor.ShortResistance
            values(TopShuntBottomSeriesIndex.BottomRightSeries) = Resistor.ShortResistance
        ElseIf balanceLayout = BalanceLayout.RightShuntRightSeries Then
            If bridge.ProductImbalance > 0 Then
                values(RightShuntRightSeriesIndex.TopRightShunt) = bridge.BalanceDeviation / bridge.TopRight
                values(RightShuntRightSeriesIndex.BottomRightShunt) = Conductor.OpenConductance
            ElseIf bridge.ProductImbalance < 0 Then
                values(RightShuntRightSeriesIndex.BottomRightShunt) = -bridge.BalanceDeviation / (bridge.TopLeft * bridge.Balance)
                values(RightShuntRightSeriesIndex.TopRightShunt) = Conductor.OpenConductance
            Else
                values(RightShuntRightSeriesIndex.TopRightSeries) = Conductor.OpenConductance
                values(RightShuntRightSeriesIndex.BottomRightSeries) = Resistor.ShortResistance
            End If
        End If
        Return values
    End Function

    ''' <summary> Evaluates the shunt bridge balance. </summary>
    ''' <param name="bridge">        The bridge. </param>
    ''' <param name="balanceLayout"> The layout. </param>
    ''' <returns> A BalanceBridge. </returns>
    Public Shared Function ShuntBridgeBalance(ByVal bridge As Wheatstone, ByVal balanceLayout As BalanceLayout) As BridgeBalance
        Return New BridgeBalance(BalanceBridge.ShuntCompensation(bridge, balanceLayout), balanceLayout)
    End Function

    ''' <summary> Evaluates the shunt bridge balance. </summary>
    ''' <returns> A BalanceBridge. </returns>
    Public Function ShuntBridgeBalance() As BridgeBalance
        Return BalanceBridge.ShuntBridgeBalance(Me.NakedBridge, Me.BridgeBalance.Layout)
    End Function

    ''' <summary> Applies the shunt compensation. </summary>
    Public Sub ApplyShuntCompensation()
        Me.BridgeBalance = Me.ShuntBridgeBalance
    End Sub

    ''' <summary> Derives the series only compensation for the bridge. </summary>
    ''' <param name="bridge">        The bridge. </param>
    ''' <param name="balanceLayout"> The balance layout. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process series compensation in this
    ''' collection.
    ''' </returns>
    Public Shared Function SeriesCompensation(ByVal bridge As Wheatstone, ByVal balanceLayout As BalanceLayout) As IEnumerable(Of Double)
        If bridge Is Nothing Then Throw New ArgumentNullException(NameOf(bridge))
        Dim values As Double() = New Double() {0, 0, 0, 0}
        If balanceLayout = BalanceLayout.TopShuntBottomSeries Then
            If bridge.BalanceDeviation > 0 Then
                values(TopShuntBottomSeriesIndex.BottomRightSeries) = bridge.BottomRight * bridge.BalanceDeviation
                values(TopShuntBottomSeriesIndex.BottomLeftSeries) = Resistor.ShortResistance
            ElseIf bridge.BalanceDeviation < 0 Then
                values(TopShuntBottomSeriesIndex.BottomLeftSeries) = -bridge.BottomLeft * bridge.BalanceDeviation / bridge.Balance
                values(TopShuntBottomSeriesIndex.BottomRightSeries) = Resistor.ShortResistance
            Else
                ' short resistors
                values(TopShuntBottomSeriesIndex.BottomRightSeries) = Resistor.ShortResistance
                values(TopShuntBottomSeriesIndex.BottomRightSeries) = Resistor.ShortResistance
            End If
            ' open shunts
            values(TopShuntBottomSeriesIndex.TopRightShunt) = Conductor.OpenConductance
            values(TopShuntBottomSeriesIndex.TopLeftShunt) = Conductor.OpenConductance
        ElseIf balanceLayout = BalanceLayout.RightShuntRightSeries Then
            If bridge.BalanceDeviation > 0 Then
                values(RightShuntRightSeriesIndex.BottomRightSeries) = bridge.BottomRight * bridge.BalanceDeviation
                values(RightShuntRightSeriesIndex.TopRightSeries) = Resistor.ShortResistance
            ElseIf bridge.BalanceDeviation < 0 Then
                values(RightShuntRightSeriesIndex.TopRightSeries) = -bridge.BottomLeft * bridge.BalanceDeviation / bridge.Balance
                values(RightShuntRightSeriesIndex.BottomRightSeries) = Resistor.ShortResistance
            Else
                ' short resistors
                values(RightShuntRightSeriesIndex.TopRightSeries) = Resistor.ShortResistance
                values(RightShuntRightSeriesIndex.BottomRightSeries) = Resistor.ShortResistance
            End If
            values(RightShuntRightSeriesIndex.TopRightShunt) = Conductor.OpenConductance
            values(RightShuntRightSeriesIndex.BottomRightShunt) = Conductor.OpenConductance
        End If
        Return values
    End Function

    ''' <summary> Series bridge balance. </summary>
    ''' <param name="bridge">        The bridge. </param>
    ''' <param name="balanceLayout"> The layout. </param>
    ''' <returns> A BridgeBalance. </returns>
    Public Shared Function SeriesBridgeBalance(ByVal bridge As Wheatstone, ByVal balanceLayout As BalanceLayout) As BridgeBalance
        Return New BridgeBalance(BalanceBridge.SeriesCompensation(bridge, balanceLayout), balanceLayout)
    End Function

    ''' <summary> Series bridge balance. </summary>
    ''' <returns> A BridgeBalance. </returns>
    Public Function SeriesBridgeBalance() As BridgeBalance
        Return BalanceBridge.SeriesBridgeBalance(Me.NakedBridge, Me.BridgeBalance.Layout)
    End Function

    ''' <summary> Applies the series compensation. </summary>
    Public Sub ApplySeriesCompensation()
        Me.BridgeBalance = Me.SeriesBridgeBalance()
    End Sub

#End Region

End Class

''' <summary> A bridge balance. </summary>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="11/25/2017" by="David" revision=""> Created. </history>
Public Class BridgeBalance
    Implements IComparable(Of BridgeBalance)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <param name="values">  The balance values. </param>
    ''' <param name="indexes"> The balance Indexes. </param>
    ''' <param name="layout">  The balance layout. </param>
    Public Sub New(ByVal values As IEnumerable(Of Double), ByVal indexes As IEnumerable(Of Integer), ByVal layout As BalanceLayout)
        MyBase.New()
        Me._Metadata = New BalanceMetadataCollection(Me.Layout)
        Me.ValuesList = New List(Of Double)(BridgeBalance.ValidatedValues(values))
        Me._Layout = layout
        Me.IndexesList = New List(Of Integer)(indexes)
        Me.InitializeMinimaMaxima()
        Me._AbsoluteBalanceDeviation = Double.MaxValue
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="indexes"> The balance Indexes. </param>
    ''' <param name="layout">  The balance layout. </param>
    Public Sub New(ByVal indexes As IEnumerable(Of Integer), ByVal layout As BalanceLayout)
        Me.New(New Double() {0, 0, 0, 0}, indexes, layout)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="values">      The balance values. </param>
    ''' <param name="layout">      The balance layout. </param>
    Public Sub New(ByVal values As IEnumerable(Of Double), ByVal layout As BalanceLayout)
        Me.New(values, BalanceMetadataCollection.DefaultIndexes(values, layout), layout)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="layout">      The balance layout. </param>
    Public Sub New(ByVal layout As BalanceLayout)
        Me.New(New Double() {0, 0, 0, 0}, layout)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="bridgeBalance"> The bridge balance. </param>
    Public Sub New(ByVal bridgeBalance As BridgeBalance)
        Me.New(BridgeBalance.ValidatedValues(BridgeBalance.ValidatedBridgeBalance(bridgeBalance).Values), bridgeBalance.Indexes, bridgeBalance.Layout)
        Me._InitializeMinimaMaxima(bridgeBalance.Minima, bridgeBalance.Maxima)
        Me._AbsoluteBalanceDeviation = bridgeBalance.AbsoluteBalanceDeviation
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="bridgeBalance"> The bridge balance. </param>
    ''' <param name="values">        The balance values. </param>
    Public Sub New(ByVal bridgeBalance As BridgeBalance, ByVal values As IEnumerable(Of Double), ByVal absoluteBalanceDeviation As Double)
        Me.New(BridgeBalance.ValidatedBridgeBalance(bridgeBalance))
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If Me.IndexesList Is Nothing Then Throw New InvalidOperationException("Balance indexes must be specified if inserting new values")
        If 4 > values.Count AndAlso values.Count = Me.IndexesList.Count Then
            ' if values are a subset of the set of values then update using the indexes.
            Me.InsertValues(bridgeBalance.Indexes, values)
        Else
            BridgeBalance.ValidatedValues(values)
            Me.ValuesList = New List(Of Double)(values)
        End If
        Me._InitializeMinimaMaxima(bridgeBalance.Minima, bridgeBalance.Maxima)
        Me._AbsoluteBalanceDeviation = absoluteBalanceDeviation
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="bridgeBalance"> The bridge balance. </param>
    ''' <param name="values">        The balance values. </param>
    Public Sub New(ByVal bridgeBalance As BridgeBalance, ByVal values As IEnumerable(Of Double))
        Me.New(BridgeBalance.ValidatedBridgeBalance(bridgeBalance), values, Double.MaxValue)
    End Sub

    ''' <summary> Makes a deep copy of this object. </summary>
    ''' <returns> A copy of this object. </returns>
    Public Function Clone() As BridgeBalance
        Return New BridgeBalance(Me)
    End Function

    ''' <summary> Enumerates validated balance values in this collection. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="values"> The balance values. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process validated balance values in this
    ''' collection.
    ''' </returns>
    Public Shared Function ValidatedValues(ByVal values As IEnumerable(Of Double)) As IEnumerable(Of Double)
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If values.Count <> 4 Then Throw New InvalidOperationException($"{NameOf(values)} must have 4 elements")
        Return values
    End Function

    ''' <summary> Validated bridge balance. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="bridgeBalance"> The bridge balance. </param>
    ''' <returns> A Bridge Balance. </returns>
    Public Shared Function ValidatedBridgeBalance(ByVal bridgeBalance As BridgeBalance) As BridgeBalance
        If bridgeBalance Is Nothing Then Throw New ArgumentNullException(NameOf(bridgeBalance))
        BridgeBalance.ValidatedValues(bridgeBalance.Values)
        Return bridgeBalance
    End Function

#End Region

#Region " I COMPARABLE "

    ''' <summary> Compares two <see cref="BridgeBalance"/> objects to determine their relative ordering. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> Negative if 'left' is less than 'right', 0 if they are equal, or positive if it is
    ''' greater. </returns>
    Public Shared Function Compare(ByVal left As BridgeBalance, ByVal right As BridgeBalance) As Integer
        If left Is Nothing Then Throw New ArgumentNullException(NameOf(left))
        If right Is Nothing Then Throw New ArgumentNullException(NameOf(right))
        Return left.CompareTo(right)
    End Function

    ''' <summary> Compares this <see cref="BridgeBalance"/> to another to determine their relative ordering based on the
    '''           Wheatstone <see cref="AbsoluteBalanceDeviation">Absolute Balance Deviation</see>. </summary>
    ''' <param name="other"> Another instance to compare. </param>
    ''' <returns> Negative if this object is less than the other, 0 if they are equal, or positive if
    ''' this is greater. </returns>
    Public Function CompareTo(ByVal other As BridgeBalance) As Integer Implements IComparable(Of BridgeBalance).CompareTo
        If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
        Return Me.AbsoluteBalanceDeviation.CompareTo(other.AbsoluteBalanceDeviation)
    End Function

    ''' <summary> Cast that converts the given BridgeBalance to a > </summary>
    ''' <param name="left">  The left-hand-side of the '-' operator (A BalanceCandidate class) </param>
    ''' <param name="right"> The right-hand-side of the '-' operator (A BalanceCandidate class) </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator >(ByVal left As BridgeBalance, ByVal right As BridgeBalance) As Boolean
        Return Compare(left, right) > 0
    End Operator

    ''' <summary> Cast that converts the given BalanceCandidate to a &lt; </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="left">  The left-hand-side of the '-' operator (A BalanceCandidate class) </param>
    ''' <param name="right"> The right-hand-side of the '-' operator (A BalanceCandidate class) </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <(ByVal left As BridgeBalance, ByVal right As BridgeBalance) As Boolean
        Return Compare(left, right) < 0
    End Operator

#End Region

#Region " EQUALS "

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, BridgeBalance))
    End Function

    ''' <summary> Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits. </summary>
    ''' <remarks> The balances are the same if the have the same values and layout. </remarks>
    ''' <param name="other"> Specifies the other <see cref="BridgeBalance">Bridge Balance</see>
    ''' to compare for equality with this instance. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As BridgeBalance) As Boolean
        If other Is Nothing Then
            Return False
        ElseIf Me.Values Is Nothing OrElse other.Values Is Nothing Then
            Return False
        Else
            Dim affirmative As Boolean = True
            For i As Integer = 0 To Me.Values.Count - 1
                affirmative = affirmative AndAlso Me.Values(i).Equals(other.Values(i))
                If Not affirmative Then Exit For
            Next
            Return (Me.Layout = other.Layout) AndAlso affirmative
        End If
    End Function

    ''' <summary> Compares two bridge balances. </summary>
    ''' <remarks> The balances are the same if the have the same values and layout. </remarks>
    ''' <param name="other">     Specifies the other <see cref="BridgeBalance">Bridge balance</see>
    ''' to compare for equality with this instance. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
    ''' values are compared based on their end points. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As BridgeBalance, ByVal tolerance As Double) As Boolean
        If other Is Nothing Then
            Return False
        ElseIf Me.Values Is Nothing OrElse other.Values Is Nothing Then
            Return False
        ElseIf Me.Values.Count <> other.Values.Count Then
            Return False
        ElseIf Me.Equals(other) Then
            Return True
        Else
            Dim affirmative As Boolean = True
            For i As Integer = 0 To Me.Values.Count - 1
                affirmative = affirmative AndAlso Math.Abs(Me.Values(i) - other.Values(i)) <= 0.5 * tolerance * (Me.Values(i) + other.Values(i))
                If Not affirmative Then Exit For
            Next
            Return Me.Layout = other.Layout AndAlso affirmative
        End If
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Function Equals(ByVal left As BridgeBalance, ByVal right As BridgeBalance) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Function

    ''' <summary>
    ''' Compares two bridges. The bridges are compared using their LowerLimits and UpperLimits.
    ''' </summary>
    ''' <remarks> The balances are the same if the have the same values and layout. </remarks>
    ''' <param name="left">      Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right">     Specifies the right hand side argument of the binary operation. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two values. The
    '''                          values are compared based on their end points. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Shared Function Equals(ByVal left As BridgeBalance, ByVal right As BridgeBalance, ByVal tolerance As Double) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse (left IsNot Nothing) AndAlso left.Equals(right, tolerance)
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Operator =(ByVal left As BridgeBalance, ByVal right As BridgeBalance) As Boolean
        Return BridgeBalance.Equals(left, right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Operator <>(ByVal left As BridgeBalance, ByVal right As BridgeBalance) As Boolean
        Return Not BridgeBalance.Equals(left, right)
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <returns> An <see cref="System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.Values.GetHashCode Xor Me.Layout.GetHashCode
    End Function

#End Region

#Region " ELEMENTS "

    ''' <summary> Gets or sets the balance layout. </summary>
    ''' <value> The balance layout. </value>
    Public ReadOnly Property Layout As BalanceLayout

    ''' <summary> Gets the identity. </summary>
    ''' <value> The identity. </value>
    Public ReadOnly Property Identity As String
        Get
            Return BalanceMetadataCollection.ToDelimitedString(Me.Metadata.SelectValues(Me.Indexes), "+")
        End Get
    End Property

    ''' <summary> Gets the balance elements meta data. </summary>
    ''' <value> The balance elements meta data. </value>
    Public ReadOnly Property Metadata() As BalanceMetadataCollection

    Private ReadOnly Property ValuesList As List(Of Double)

    ''' <summary> Gets the balance values. </summary>
    ''' <value> The balance values. </value>
    Public ReadOnly Property Values As IEnumerable(Of Double)
        Get
            Return ValuesList
        End Get
    End Property

    ''' <summary> Insert new values using the specified indexes. </summary>
    ''' <param name="indexes"> The balance Indexes. </param>
    ''' <param name="values">  The balance values. </param>
    Private Sub InsertValues(ByVal indexes As IEnumerable(Of Integer), ByVal values As IEnumerable(Of Double))
        For i As Integer = 0 To indexes.Count - 1
            Me.ValuesList(indexes(i)) = values(i)
        Next
    End Sub

    Private ReadOnly Property IndexesList As List(Of Integer)

    ''' <summary> Gets the indexes of the balance element values and meta data.. </summary>
    ''' <value> The indexes of the balance element values and meta data.. </value>
    Public Property Indexes As IEnumerable(Of Integer)
        Get
            Return Me.IndexesList
        End Get
        Set(value As IEnumerable(Of Integer))
            Me._IndexesList = New List(Of Integer)(value)
        End Set
    End Property

    ''' <summary> Shunt index. </summary>
    ''' <returns> An Integer. </returns>
    Public Function ShuntIndex() As Integer
        Return Me.Indexes(Me.DefaultShuntIndex)
    End Function

    ''' <summary> Gets or sets the default shunt index. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The default shunt index. </value>
    Public ReadOnly Property DefaultShuntIndex As Integer = 0

    ''' <summary> Looks for the shunt index. </summary>
    ''' <returns> An Integer. </returns>
    Public Function FindShuntIndex() As Integer
        Return Me.FindShuntIndex(Me.Values, Me.Indexes)
    End Function

    ''' <summary> Looks for the shunt index. </summary>
    ''' <param name="values">  The balance values. </param>
    ''' <param name="indexes"> The balance Indexes. </param>
    ''' <returns> An Integer. </returns>
    Public Function FindShuntIndex(ByVal values As IEnumerable(Of Double), ByVal indexes As IEnumerable(Of Integer)) As Integer
        ' bug fixed, default selected as series index instead of shunt index. 
        Dim result As Integer = indexes(Me.DefaultShuntIndex)
        For Each index As Integer In Me.Metadata.ShuntIndexes
            If values(index) <> 0 Then result = index
        Next
        Return result
    End Function

    ''' <summary> Gets the zero-based index of the series resistor. </summary>
    ''' <returns> An Integer. </returns>
    Public Function SeriesIndex() As Integer
        Return Me.Indexes(Me.DefaultSeriesIndex)
    End Function

    ''' <summary> Gets or sets the default series index. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The default series index. </value>
    Public ReadOnly Property DefaultSeriesIndex As Integer = 1

    Public Function FindSeriesIndex() As Integer
        Return Me.FindSeriesIndex(Me.Values, Me.Indexes)
    End Function


    ''' <summary> Gets the zero-based index of the series resistor. </summary>
    Public Function FindSeriesIndex(ByVal values As IEnumerable(Of Double), ByVal indexes As IEnumerable(Of Integer)) As Integer
        Dim result As Integer = indexes(Me.DefaultSeriesIndex)
        For Each index As Integer In Me.Metadata.SeriesIndexes
            If values(index) <> 0 Then result = index
        Next
        Return result
    End Function

    ''' <summary> Enumerates balance indexes used in the bridge balance. </summary>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process balance indexes in this collection.
    ''' </returns>
    Public Function BalanceIndexes() As IEnumerable(Of Integer)
        Return New List(Of Integer)(New Integer() {Me.ShuntIndex, Me.SeriesIndex})
    End Function

    ''' <summary> Enumerates balance values the bridge balance. </summary>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process balance values in this collection.
    ''' </returns>
    Public Function BalanceValues() As IEnumerable(Of Double)
        Return BridgeBalance.SelectValues(Me.Values, Me.BalanceIndexes)
    End Function

    ''' <summary> Converts a value to an enum. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a T. </returns>
    Public Shared Function ToEnum(Of T)(ByVal value As Integer) As T
        Return CType(System.Enum.Parse(GetType(T), value.ToString, True), T)
    End Function

    ''' <summary> Converts an index to an balance index. </summary>
    ''' <param name="index"> Zero-based index of the balance values. </param>
    ''' <returns> Index as a T. </returns>
    Public Function ToIndex(Of T)(ByVal index As Integer) As T
        Return BridgeBalance.ToEnum(Of T)(Me.Indexes(index))
    End Function

    ''' <summary> Enumerates select values in this collection. </summary>
    ''' <param name="values">  The balance values. </param>
    ''' <param name="indexes"> The balance Indexes. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process select values in this collection.
    ''' </returns>
    Public Shared Function SelectValues(ByVal values As IEnumerable(Of Double), ByVal indexes As IEnumerable(Of Integer)) As IEnumerable(Of Double)
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If indexes Is Nothing Then Throw New ArgumentNullException(NameOf(indexes))
        Dim l As New List(Of Double)
        For Each index As Integer In indexes
            l.Add(values(index))
        Next
        Return l
    End Function

    ''' <summary> Query if the bridge balance is valid. </summary>
    ''' <returns> <c>true</c> if valid; otherwise <c>false</c> </returns>
    Public Function IsValid() As Boolean
        Dim affirmative As Boolean = True
        For i As Integer = 0 To Me.Values.Count - 1
            If Me.Metadata(i).ElementType = BalanceElementType.Series Then
                affirmative = affirmative AndAlso Not Resistor.IsOpen(Me.Values(i))
            Else
                affirmative = affirmative AndAlso Not Conductor.IsShort(Me.Values(i))
            End If
            If Not affirmative Then Exit For
        Next
        Return affirmative
    End Function

    ''' <summary> Gets or sets the absolute balance deviation. </summary>
    ''' <value> The absolute balance deviation. </value>
    Public Property AbsoluteBalanceDeviation As Double

#End Region

#Region " MAXIMA/MINIMA "

    Private _Maxima As List(Of Double)

    ''' <summary> Gets or sets the maxima. </summary>
    ''' <value> The maxima. </value>
    Public Property Maxima As IEnumerable(Of Double)
        Get
            Return Me._Maxima
        End Get
        Set(value As IEnumerable(Of Double))
            Me._Maxima = New List(Of Double)(value)
        End Set
    End Property

    Private _Minima As List(Of Double)

    ''' <summary> Gets or sets the minima. </summary>
    ''' <value> The minima. </value>
    Public Property Minima As IEnumerable(Of Double)
        Get
            Return Me._Minima
        End Get
        Set(value As IEnumerable(Of Double))
            Me._Minima = New List(Of Double)(value)
        End Set
    End Property

    ''' <summary> Initializes the minima and maxima. </summary>
    ''' <param name="equivalentBridgeResistance"> The equivalent bridge resistance. </param>
    Public Sub InitializeMinimaMaxima(ByVal equivalentBridgeResistance As Double)
        Me.InitializeMinimaMaxima()
        For Each index As Integer In Me.Indexes
            If Me.Metadata(index).ElementType = BalanceElementType.Series Then
                Me._Maxima(index) = equivalentBridgeResistance
            ElseIf Me.Metadata(index).ElementType = BalanceElementType.Shunt Then
                Me._Maxima(index) = 1 / equivalentBridgeResistance
            End If
        Next
    End Sub

    ''' <summary> Initializes the minima and maxima. </summary>
    Private Sub InitializeMinimaMaxima()
        Me._InitializeMinimaMaxima(New Double() {0, 0, 0, 0}, New Double() {0, 0, 0, 0})
    End Sub

    ''' <summary> Initializes minima and maxima. </summary>
    ''' <param name="minima"> The minima. </param>
    ''' <param name="maxima"> The maxima. </param>
    Private Sub _InitializeMinimaMaxima(ByVal minima As IEnumerable(Of Double), maxima As IEnumerable(Of Double))
        Me._Minima = New List(Of Double)(minima)
        Me._Maxima = New List(Of Double)(maxima)
    End Sub

    ''' <summary> Initializes the minima maxima. </summary>
    ''' <param name="minima"> The minima. </param>
    ''' <param name="maxima"> The maxima. </param>
    Public Sub InitializeMinimaMaxima(ByVal minima As IEnumerable(Of Double), maxima As IEnumerable(Of Double))
        Me._InitializeMinimaMaxima(minima, maxima)
    End Sub

#End Region

#Region " TO COMMA SEPARATED STRING "

    ''' <summary> Returns a comma-delimited values string. </summary>
    ''' <param name="values"> The values. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <returns> A String that represents this object. </returns>
    Public Shared Function ToCommaSeparatedString(ByVal values As IEnumerable(Of Double), ByVal format As String) As String
        Dim builder As New System.Text.StringBuilder()
        For i As Integer = 0 To values.Count - 1
            builder.Append($"{values(i).ToString(format)}")
            If i < (values.Count - 1) Then builder.Append(",")
        Next
        Return builder.ToString
    End Function

    ''' <summary> Returns a comma-delimited values string. </summary>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <returns> A String that represents this object. </returns>
    Public Function ToCommaSeparatedString(ByVal format As String) As String
        Return BridgeBalance.ToCommaSeparatedString(Me.Values, format)
    End Function

    ''' <summary> Returns a comma-delimited values string. </summary>
    ''' <returns> A String that represents this object. </returns>
    Public Function ToCommaSeparatedString() As String
        Return Me.ToCommaSeparatedString("G6")
    End Function

#End Region

#Region " TO STRING "

    ''' <summary> Convert this object into a string representation. </summary>
    ''' <param name="values">   The values. </param>
    ''' <param name="metadata"> Information describing the meta. </param>
    ''' <param name="format">   Describes the format to use. </param>
    ''' <returns> A String that represents this object. </returns>
    Public Overloads Shared Function ToString(ByVal values As IEnumerable(Of Double),
                                              ByVal metadata As IEnumerable(Of BalanceElementMetadata),
                                              ByVal delimiter As String,
                                              ByVal format As String) As String
        Dim builder As New System.Text.StringBuilder
        For i As Integer = 0 To values.Count - 1
            builder.Append($"{metadata(i).Edge} {metadata(i).ElementType} = {values(i).ToString(format)}")
            If i < (values.Count - 1) Then builder.Append(delimiter)
        Next
        Return builder.ToString
    End Function

    ''' <summary> Convert this object into a string representation. </summary>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <returns> A String that represents this object. </returns>
    Public Overloads Function ToString(ByVal delimiter As String, ByVal format As String) As String
        Return BridgeBalance.ToString(Me.Values, Me.Metadata, delimiter, format)
    End Function

    ''' <summary> Convert this object into a string representation. </summary>
    ''' <param name="values"> The values. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <returns> A String that represents this object. </returns>
    Public Overloads Shared Function ToString(ByVal values As IEnumerable(Of Double), ByVal format As String) As String
        Dim builder As New System.Text.StringBuilder("[")
        builder.Append(BridgeBalance.ToCommaSeparatedString(values, format))
        builder.Append("]")
        Return builder.ToString
    End Function

    ''' <summary> Convert this object into a string representation. </summary>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <returns> A String that represents this object. </returns>
    Public Overloads Function ToString(ByVal format As String) As String
        Return BridgeBalance.ToString(Me.Values, format)
    End Function

    ''' <summary> Convert this object into a string representation. </summary>
    ''' <returns> A String that represents this object. </returns>
    Public Overrides Function ToString() As String
        Return Me.ToString("G6")
    End Function

#End Region

End Class

''' <summary> A collection of Balance Bridges. </summary>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="9/25/2017" by="David" revision=""> Created. </history>
Public Class BalanceBridgeCollection
    Inherits ObjectModel.Collection(Of BalanceBridge)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="bridges"> The bridges. </param>
    Public Sub New(ByVal bridges As BalanceBridgeCollection)
        Me.New
        If bridges Is Nothing Then Throw New ArgumentNullException(NameOf(bridges))
        For Each b As BalanceBridge In bridges
            Me.Add(New BalanceBridge(b))
        Next
    End Sub

    Public Shared Function Validate(ByVal bridges As BalanceBridgeCollection) As BalanceBridgeCollection
        If bridges Is Nothing Then Throw New ArgumentNullException(NameOf(bridges))
        Return bridges
    End Function

#End Region

#Region " BRIDGE BALANCE "

    ''' <summary>
    ''' Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    ''' <exception cref="T:System.NotSupportedException"> The
    '''                                                   <see cref="T:System.Collections.Generic.ICollection`1" />
    '''                                                   is read-only. </exception>
    ''' <param name="bridge"> The object to add to the
    '''                       <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
    Public Overloads Sub Add(ByVal bridge As BalanceBridge)
        If bridge Is Nothing Then Throw New ArgumentNullException(NameOf(bridge))
        MyBase.Add(bridge)
        If Me._BridgeBalance Is Nothing Then Me._BridgeBalance = New BridgeBalance(bridge.BridgeBalance)
    End Sub

    Private _BridgeBalance As BridgeBalance

    ''' <summary> Gets or sets the bridge balance. </summary>
    ''' <value> The bridge balance. </value>
    Public Property BridgeBalance As BridgeBalance
        Get
            Return Me._BridgeBalance
        End Get
        Set(value As BridgeBalance)
            Me._BridgeBalance = value
            For Each bridge As BalanceBridge In Me
                bridge.BridgeBalance = value
            Next
        End Set
    End Property
#End Region

#Region " CALCULATIONS "

    ''' <summary> Root sum squares. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function RootSumSquares(ByVal values As IEnumerable(Of Double)) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim result As Double = 0
        For Each value As Double In values
            result += value * value
        Next
        result = Math.Sqrt(result)
        Return result
    End Function

    ''' <summary> Enumerates imbalances in this collection. </summary>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process imbalances in this collection.
    ''' </returns>
    Public Function Imbalances() As IEnumerable(Of Double)
        Dim l As New List(Of Double)
        For Each bridge As Wheatstone In Me
            l.Add(bridge.ProductImbalance)
        Next
        Return l
    End Function

    ''' <summary> Enumerates deviations in this collection. </summary>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process imbalances in this collection.
    ''' </returns>
    Public Function Deviations() As IEnumerable(Of Double)
        Dim l As New List(Of Double)
        For Each bridge As Wheatstone In Me
            l.Add(bridge.BalanceDeviation)
        Next
        Return l
    End Function

    ''' <summary> Enumerates Relative Offsets in this collection. </summary>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process imbalances in this collection.
    ''' </returns>
    Public Function RelativeOffsets() As IEnumerable(Of Double)
        Dim l As New List(Of Double)
        For Each bridge As Wheatstone In Me
            l.Add(bridge.Output)
        Next
        Return l
    End Function


#End Region

End Class

''' <summary> Values that represent balance layouts. </summary>
Public Enum BalanceLayout

    ''' <summary> An enum constant representing the top shunt bottom series option where
    '''           shunt conductances are connected across the top bridge elements and 
    '''           resistors are wired in series to the bottom bridge elements. Element order 
    '''           is Top Right Shunt, Bottom Right Series, Bottom Left Series, Top Left Shunt</summary>
    <Description("Top shunts bottom series")> TopShuntBottomSeries
    ''' <summary> An enum constant representing the right shunt right series option where 
    '''           the shunt conductances are applied to the elements of the right arm of 
    '''           the bridge and a series resistor is added to this parallel pair. . Element order
    '''           is Top Right Shunt, Top Right Series, Bottom Right Series, Bottom Right Shunt</summary>
    <Description("Right shunts right series")> RightShuntRightSeries
End Enum

''' <summary> Values that represent top shunt bottom series indexes. </summary>
Public Enum TopShuntBottomSeriesIndex
    TopRightShunt = 0
    BottomRightSeries = 1
    BottomLeftSeries = 2
    TopLeftShunt = 3
End Enum

''' <summary> Values that represent right shunt right series indexes. </summary>
Public Enum RightShuntRightSeriesIndex
    TopRightShunt = 0
    TopRightSeries = 1
    BottomRightSeries = 2
    BottomRightShunt = 3
End Enum


''' <summary> Values that represent element types. </summary>
Public Enum BalanceElementType
    ''' <summary> An enum constant representing the shunt option. </summary>
    Shunt
    ''' <summary> An enum constant representing the series option. </summary>
    Series
End Enum

''' <summary> Meta data for a balance element. </summary>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
Public Structure BalanceElementMetadata

    Public Sub New(ByVal index As Integer, ByVal edge As WheatstoneEdge, ByVal elementType As BalanceElementType)
        Me.ElementIndex = index
        Me.Edge = edge
        Me.ElementType = elementType
    End Sub
    Public Property Edge As WheatstoneEdge
    Public Property ElementType As BalanceElementType
    Public Property ElementIndex As Integer

#Region " EQUALS "

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse Me.Equals(CType(obj, BalanceElementMetadata)))
    End Function

    ''' <summary> = casting operator. </summary>
    ''' <param name="left">  The left-hand-side of the '-' operator (A BalanceElementAttribute class) </param>
    ''' <param name="right"> The right-hand-side of the '-' operator (A BalanceElementAttribute class) </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As BalanceElementMetadata, ByVal right As BalanceElementMetadata) As Boolean
        Return BalanceElementMetadata.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <param name="left">  The left-hand-side of the '-' operator (A BalanceElementAttribute class) </param>
    ''' <param name="right"> The right-hand-side of the '-' operator (A BalanceElementAttribute class) </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As BalanceElementMetadata, ByVal right As BalanceElementMetadata) As Boolean
        Return Not BalanceElementMetadata.Equals(left, right)
    End Operator

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <param name="left">  The left-hand-side of the '-' operator (A BalanceElementAttribute class) </param>
    ''' <param name="right"> The right-hand-side of the '-' operator (A BalanceElementAttribute class) </param>
    ''' <returns> <c>True</c> if <paramref name="left" /> and <paramref name="right" /> are the same type and represent the
    ''' same value; otherwise, <c>False</c>. </returns>
    Public Overloads Shared Function Equals(ByVal left As BalanceElementMetadata, ByVal right As BalanceElementMetadata) As Boolean
        Return left.Edge.Equals(right.Edge) AndAlso left.ElementType.Equals(right.ElementType) AndAlso left.ElementIndex.Equals(right.ElementIndex)
    End Function

    ''' <summary> Returns True if the value of the <paramref name="other"/> equals to the instance
    ''' value. </summary>
    ''' <remarks> Balance element meta data are the same if the have the same values. </remarks>
    ''' <param name="other"> The other <see cref="BalanceElementMetadata">BalanceElementAttribute</see> to compare for equality
    ''' with this instance. </param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    Public Overloads Function Equals(ByVal other As BalanceElementMetadata) As Boolean
        Return BalanceElementMetadata.Equals(Me, other)
    End Function

    ''' <summary> Creates a unique hash code. </summary>
    ''' <returns> An <see cref="System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.Edge.GetHashCode Xor Me.ElementType.GetHashCode Xor Me.ElementIndex.GetHashCode
    End Function

#End Region

End Structure

''' <summary> A balance meta data collection. </summary>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="11/27/2017" by="David" revision=""> Created. </history>
Public Class BalanceMetadataCollection
    Inherits Collections.ObjectModel.KeyedCollection(Of Integer, BalanceElementMetadata)

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <exception cref="InvalidEnumArgumentException"> Thrown when an Invalid Enum Argument error
    '''                                                 condition occurs. </exception>
    ''' <param name="layout"> The layout. </param>
    Public Sub New(ByVal layout As BalanceLayout)
        MyBase.New
        Me.SeriesIndexesList = New List(Of Integer)
        Me.ShuntIndexesList = New List(Of Integer)
        Select Case layout
            Case BalanceLayout.RightShuntRightSeries
                Me.Add(New BalanceElementMetadata(RightShuntRightSeriesIndex.TopRightShunt, WheatstoneEdge.TopRight, BalanceElementType.Shunt))
                Me.ShuntIndexesList.Add(Me.Count - 1)
                Me.Add(New BalanceElementMetadata(RightShuntRightSeriesIndex.TopRightSeries, WheatstoneEdge.TopRight, BalanceElementType.Series))
                Me.SeriesIndexesList.Add(Me.Count - 1)
                Me.Add(New BalanceElementMetadata(RightShuntRightSeriesIndex.BottomRightSeries, WheatstoneEdge.BottomRight, BalanceElementType.Series))
                Me.SeriesIndexesList.Add(Me.Count - 1)
                Me.Add(New BalanceElementMetadata(RightShuntRightSeriesIndex.BottomRightShunt, WheatstoneEdge.BottomRight, BalanceElementType.Shunt))
                Me.ShuntIndexesList.Add(Me.Count - 1)
            Case BalanceLayout.TopShuntBottomSeries
                Me.Add(New BalanceElementMetadata(TopShuntBottomSeriesIndex.TopRightShunt, WheatstoneEdge.TopRight, BalanceElementType.Shunt))
                Me.ShuntIndexesList.Add(Me.Count - 1)
                Me.Add(New BalanceElementMetadata(TopShuntBottomSeriesIndex.BottomRightSeries, WheatstoneEdge.BottomRight, BalanceElementType.Series))
                Me.SeriesIndexesList.Add(Me.Count - 1)
                Me.Add(New BalanceElementMetadata(TopShuntBottomSeriesIndex.BottomLeftSeries, WheatstoneEdge.BottomLeft, BalanceElementType.Series))
                Me.SeriesIndexesList.Add(Me.Count - 1)
                Me.Add(New BalanceElementMetadata(TopShuntBottomSeriesIndex.TopLeftShunt, WheatstoneEdge.TopLeft, BalanceElementType.Shunt))
                Me.ShuntIndexesList.Add(Me.Count - 1)
            Case Else
                Throw New InvalidEnumArgumentException(NameOf(layout), layout, GetType(BalanceLayout))
        End Select

    End Sub

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As BalanceElementMetadata) As Integer
        Return item.ElementIndex
    End Function

    ''' <summary> Gets the meta data. </summary>
    ''' <value> The meta data. </value>
    Public Shared ReadOnly Property Metadata(ByVal layout As BalanceLayout) As BalanceMetadataCollection
        Get
            Return New BalanceMetadataCollection(layout)
        End Get
    End Property

    ''' <summary> Returns the default indexes for the specified values. </summary>
    ''' <returns> An enumerator that allows for each to be used to process default indexes in this collection. </returns>
    Public Shared Function DefaultIndexes(ByVal values As IEnumerable(Of Double), ByVal layout As BalanceLayout) As IEnumerable(Of Integer)
        values = BalanceBridge.ValidatedBalanceValues(values)
        With BalanceMetadataCollection.Metadata(layout)
            Return New List(Of Integer)(New Integer() { .SuggestShuntIndex(values, .ShuntIndexes(0)), .SuggestSeriesIndex(values, .SeriesIndexes(0))})
        End With
    End Function

    ''' <summary> Returns the default indexes. </summary>
    ''' <param name="layout"> The layout. </param>
    ''' <returns> An enumerator that allows for each to be used to process default indexes in this collection. </returns>
    Public Shared Function DefaultIndexes(ByVal layout As BalanceLayout) As IEnumerable(Of Integer)
        With BalanceMetadataCollection.Metadata(layout)
            Return New List(Of Integer)(New Integer() { .ShuntIndexes(0), .SeriesIndexes(0)})
        End With
    End Function

#End Region

#Region " INDEXES "

    Private ReadOnly Property SeriesIndexesList As List(Of Integer)

    ''' <summary> Gets the series indexes. </summary>
    ''' <value> The series indexes. </value>
    Public ReadOnly Property SeriesIndexes As IEnumerable(Of Integer)
        Get
            Return Me.SeriesIndexesList
        End Get
    End Property

    Private ReadOnly Property ShuntIndexesList As List(Of Integer)

    ''' <summary> Gets the shunt indexes. </summary>
    ''' <value> The shunt indexes. </value>
    Public ReadOnly Property ShuntIndexes As IEnumerable(Of Integer)
        Get
            Return Me.ShuntIndexesList
        End Get
    End Property

#End Region

#Region " SELECT VALUES AND INDICES "

    ''' <summary> Enumerates select values in this collection. </summary>
    ''' <param name="values">  The balance values. </param>
    ''' <param name="indexes"> The balance Indexes. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process select values in this collection.
    ''' </returns>
    Public Shared Function SelectValues(ByVal values As IEnumerable(Of BalanceElementMetadata), ByVal indexes As IEnumerable(Of Integer)) As IEnumerable(Of BalanceElementMetadata)
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If indexes Is Nothing Then Throw New ArgumentNullException(NameOf(indexes))
        Dim l As New List(Of BalanceElementMetadata)
        For Each index As Integer In indexes
            l.Add(values(index))
        Next
        Return l
    End Function

    ''' <summary> Enumerates select values in this collection. </summary>
    ''' <param name="indexes"> The balance Indexes. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process select values in this collection.
    ''' </returns>
    Public Function SelectValues(ByVal indexes As IEnumerable(Of Integer)) As IEnumerable(Of BalanceElementMetadata)
        Return BalanceMetadataCollection.SelectValues(Me, indexes)
    End Function

    ''' <summary> Suggests a shunt index. </summary>
    ''' <param name="layout"> The layout. </param>
    ''' <param name="values"> The balance values. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function SuggestShuntIndex(ByVal layout As BalanceLayout, ByVal values As IEnumerable(Of Double)) As Integer
        With BalanceMetadataCollection.Metadata(layout)
            Return .SuggestShuntIndex(values, .ShuntIndexes(0))
        End With
    End Function

    ''' <summary> Suggests a shunt index. </summary>
    ''' <param name="layout">       The layout. </param>
    ''' <param name="values">       The balance values. </param>
    ''' <param name="defaultIndex"> The default index. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function SuggestShuntIndex(ByVal layout As BalanceLayout, ByVal values As IEnumerable(Of Double), ByVal defaultIndex As Integer) As Integer
        Return BalanceMetadataCollection.Metadata(layout).SuggestShuntIndex(values, defaultIndex)
    End Function

    ''' <summary> Suggest shunt index. </summary>
    ''' <param name="values"> The balance values. </param>
    ''' <returns> An Integer. </returns>
    Public Function SuggestShuntIndex(ByVal values As IEnumerable(Of Double)) As Integer
        Return Me.SuggestShuntIndex(values, Me.ShuntIndexes(0))
    End Function

    ''' <summary> Suggest shunt index. </summary>
    ''' <param name="values">       The balance values. </param>
    ''' <param name="defaultIndex"> The default index. </param>
    ''' <returns> An Integer. </returns>
    Public Function SuggestShuntIndex(ByVal values As IEnumerable(Of Double), ByVal defaultIndex As Integer) As Integer
        ' default to the first index
        Dim shuntIndex As Integer = defaultIndex
        For Each index As Integer In Me.ShuntIndexes
            If values(index) > 0 Then shuntIndex = index
        Next
        Return shuntIndex
    End Function

    ''' <summary> Suggests a Series index. </summary>
    ''' <param name="layout"> The layout. </param>
    ''' <param name="values"> The balance values. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function SuggestSeriesIndex(ByVal layout As BalanceLayout, ByVal values As IEnumerable(Of Double)) As Integer
        With BalanceMetadataCollection.Metadata(layout)
            Return .SuggestSeriesIndex(values, .SeriesIndexes(0))
        End With
    End Function

    ''' <summary> Suggests a Series index. </summary>
    ''' <param name="layout">       The layout. </param>
    ''' <param name="values">       The balance values. </param>
    ''' <param name="defaultIndex"> The default index. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function SuggestSeriesIndex(ByVal layout As BalanceLayout, ByVal values As IEnumerable(Of Double), ByVal defaultIndex As Integer) As Integer
        Return BalanceMetadataCollection.Metadata(layout).SuggestSeriesIndex(values, defaultIndex)
    End Function

    ''' <summary> Suggest Series index. </summary>
    ''' <param name="values"> The balance values. </param>
    ''' <returns> An Integer. </returns>
    Public Function SuggestSeriesIndex(ByVal values As IEnumerable(Of Double)) As Integer
        Return Me.SuggestSeriesIndex(values, Me.SeriesIndexes(0))
    End Function

    ''' <summary> Suggest Series index. </summary>
    ''' <param name="values">       The balance values. </param>
    ''' <param name="defaultIndex"> The default index. </param>
    ''' <returns> An Integer. </returns>
    Public Function SuggestSeriesIndex(ByVal values As IEnumerable(Of Double), ByVal defaultIndex As Integer) As Integer
        ' default to the first index
        Dim SeriesIndex As Integer = defaultIndex
        For Each index As Integer In Me.SeriesIndexes
            If values(index) > 0 Then SeriesIndex = index
        Next
        Return SeriesIndex
    End Function



#End Region

#Region " TO STRING "

    ''' <summary> Returns delimited header string. </summary>
    ''' <param name="metadata"> Information describing the meta. </param>
    ''' <returns> A String that represents this object. </returns>
    Public Shared Function ToDelimitedString(ByVal metadata As IEnumerable(Of BalanceElementMetadata), ByVal delimiter As String) As String
        Dim builder As New System.Text.StringBuilder
        For i As Integer = 0 To metadata.Count - 1
            builder.Append($"{metadata(i).Edge} {metadata(i).ElementType}")
            If i < (metadata.Count - 1) Then builder.Append(delimiter)
        Next
        Return builder.ToString
    End Function

    ''' <summary> Returns a comma-delimited header string. </summary>
    ''' <returns> A String that represents this object. </returns>
    Public Function ToCommaSeparatedString() As String
        Return BalanceMetadataCollection.ToDelimitedString(Me, ",")
    End Function

    ''' <summary> Convert this object into a string representation. </summary>
    ''' <param name="metadata"> The meta data. </param>
    ''' <returns> A String that represents this object. </returns>
    Public Overloads Shared Function ToString(ByVal metadata As IEnumerable(Of BalanceElementMetadata)) As String
        Dim builder As New System.Text.StringBuilder("[")
        builder.Append(BalanceMetadataCollection.ToDelimitedString(metadata, ","))
        builder.Append("]")
        Return builder.ToString
    End Function

    ''' <summary> Convert this object into a string representation. </summary>
    ''' <returns> A String that represents this object. </returns>
    Public Overrides Function ToString() As String
        Return BalanceMetadataCollection.ToString(Me)
    End Function

#End Region

End Class


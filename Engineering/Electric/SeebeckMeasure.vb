﻿''' <summary> A Seebeck measure. </summary>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="6/28/2018" by="David" revision=""> Created. </history>
Public Class SeebeckMeasure

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="voltage">         The voltage. </param>
    ''' <param name="lowTemperature">  The low temperature. </param>
    ''' <param name="highTemperature"> The high temperature. </param>
    Public Sub New(ByVal voltage As Double, ByVal lowTemperature As Double, ByVal highTemperature As Double)
        Me.New
        Me.Voltage = voltage
        Me.LowTemperature = lowTemperature
        Me.HighTemperature = highTemperature
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="seebeckMeasure"> The seebeck measure. </param>
    Public Sub New(ByVal seebeckMeasure As SeebeckMeasure)
        Me.New(SeebeckMeasure.Validated(seebeckMeasure).Voltage, seebeckMeasure.LowTemperature, seebeckMeasure.HighTemperature)
    End Sub

    Public Shared Function Validated(ByVal seebeckMeasure As SeebeckMeasure) As SeebeckMeasure
        If seebeckMeasure Is Nothing Then Throw New ArgumentNullException(NameOf(seebeckMeasure))
        Return seebeckMeasure
    End Function

    ''' <summary> Gets the voltage. </summary>
    ''' <value> The voltage. </value>
    Public Property Voltage As Double

    ''' <summary> Gets the high temperature. </summary>
    ''' <value> The high temperature. </value>
    Public Property HighTemperature As Double

    ''' <summary> Gets the low temperature. </summary>
    ''' <value> The low temperature. </value>
    Public Property LowTemperature As Double

    ''' <summary> Gets the has value. </summary>
    ''' <value> The has value. </value>
    Public ReadOnly Property HasValue As Boolean
        Get
            Return (Me.HighTemperature - Me.LowTemperature) > 0
        End Get
    End Property

    ''' <summary> Gets the Seebeck Coefficient. </summary>
    ''' <value> The Seebeck Coefficient or <see cref="Double.NaN"/> if not <see cref="HasValue"/>. </value>
    Public ReadOnly Property SeebeckCoefficient As Double
        Get
            If Me.HasValue Then
                Return Me.Voltage / (Me.HighTemperature - Me.LowTemperature)
            Else
                Return Double.NaN
            End If
        End Get
    End Property

End Class

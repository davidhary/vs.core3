﻿''' <summary> A cubic polynomial. </summary>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="2/6/2016" by="David"> Created. </history>
Public Class CubicPolynomial

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="value"> Information describing the polynomial. </param>
    Public Sub New(ByVal value As CubicPolynomial)
        Me.New
        If value IsNot Nothing Then
            Me.ConstantCoefficient = value.ConstantCoefficient
            Me.LinearCoefficient = value.LinearCoefficient
            Me.QuadraticCoefficient = value.QuadraticCoefficient
            Me.CubicCoefficient = value.CubicCoefficient
        End If
    End Sub

    ''' <summary> Gets or sets the constant coefficient. </summary>
    ''' <value> The constant coefficient. </value>
    Public Property ConstantCoefficient As Double

    ''' <summary> Gets or sets the linear coefficient. </summary>
    ''' <value> The linear coefficient. </value>
    Public Property LinearCoefficient As Double

    ''' <summary> Gets or sets the quadratic coefficient. </summary>
    ''' <value> The quadratic coefficient. </value>
    Public Property QuadraticCoefficient As Double

    ''' <summary> Gets or sets the cubic coefficient. </summary>
    ''' <value> The cubic coefficient. </value>
    Public Property CubicCoefficient As Double

    ''' <summary> Evaluates. </summary>
    ''' <param name="value"> Information describing the polynomial. </param>
    ''' <returns> A Double. </returns>
    Public Function Evaluate(ByVal value As Double) As Double
        Return Me.ConstantCoefficient + value * (Me.LinearCoefficient + value * (Me.QuadraticCoefficient + value * Me.CubicCoefficient))
    End Function

    ''' <summary> Square root. </summary>
    ''' <param name="value"> Information describing the polynomial. </param>
    ''' <returns> A Double. </returns>
    Private Function SquareRoot(ByVal value As Double) As Double
        Return Math.Sqrt(Me.LinearCoefficient * Me.LinearCoefficient -
                             4 * Me.QuadraticCoefficient * (Me.ConstantCoefficient - value))
    End Function

    ''' <summary> Calculates the positive quadratic root for the given value. </summary>
    ''' <param name="value"> Information describing the polynomial. </param>
    ''' <returns> A Double. </returns>
    Public Function PositiveQuadraticRoot(ByVal value As Double) As Double
        Return (-Me.LinearCoefficient + Me.SquareRoot(value)) / (2 * Me.QuadraticCoefficient)
    End Function

    ''' <summary> Calculates the positive quadratic root for the given value. </summary>
    ''' <param name="value"> Information describing the polynomial. </param>
    ''' <returns> A Double. </returns>
    Public Function NegativeQuadraticRoot(ByVal value As Double) As Double
        Return (-Me.LinearCoefficient - Me.SquareRoot(value)) / (2 * Me.QuadraticCoefficient)
    End Function

End Class

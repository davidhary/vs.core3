﻿''' <summary> A quadratic polynomial. </summary>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="2/6/2016" by="David"> Created. </history>
Public Class QuadraticPolynomial

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New
        Me.GoodnessOfFit = Double.NaN
        Me.StandardError = Double.NaN
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="constantCoefficient">  The constant coefficient. </param>
    ''' <param name="linearCoefficient">    The linear coefficient. </param>
    ''' <param name="quadraticCoefficient"> The quadratic coefficient. </param>
    Public Sub New(ByVal constantCoefficient As Double, ByVal linearCoefficient As Double, ByVal quadraticCoefficient As Double)
        Me.New
        Me.ConstantCoefficient = constantCoefficient
        Me.LinearCoefficient = linearCoefficient
        Me.QuadraticCoefficient = quadraticCoefficient
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="value"> Information describing the polynomial. </param>
    Public Sub New(ByVal value As QuadraticPolynomial)
        Me.New
        If value IsNot Nothing Then
            Me.ConstantCoefficient = value.ConstantCoefficient
            Me.LinearCoefficient = value.LinearCoefficient
            Me.QuadraticCoefficient = value.QuadraticCoefficient
        End If
    End Sub

    ''' <summary> Gets or sets the constant coefficient. </summary>
    ''' <value> The constant coefficient. </value>
    Public Property ConstantCoefficient As Double

    ''' <summary> Gets or sets the linear coefficient. </summary>
    ''' <value> The linear coefficient. </value>
    Public Property LinearCoefficient As Double

    ''' <summary> Gets or sets the quadratic coefficient. </summary>
    ''' <value> The quadratic coefficient. </value>
    Public Property QuadraticCoefficient As Double

#End Region

#Region " EVALUATION "

    ''' <summary> Evaluates the polynomial at the specified value. </summary>
    ''' <param name="value"> The value for evaluating the polynomial. </param>
    ''' <returns> A Double. </returns>
    Public Function Evaluate(ByVal value As Double) As Double
        Return Me.ConstantCoefficient + value * (Me.LinearCoefficient + value * (Me.QuadraticCoefficient))
    End Function

    ''' <summary> Evaluates the slope at the specified value. </summary>
    ''' <param name="value"> The value for evaluating the derivative. </param>
    ''' <returns> A Double. </returns>
    Public Function Slope(ByVal value As Double) As Double
        Return Me.LinearCoefficient + 2 * value * Me.QuadraticCoefficient
    End Function

#End Region

#Region " ROOTS "

    ''' <summary> Square root. </summary>
    ''' <param name="value"> Information describing the polynomial. </param>
    ''' <returns> A Double. </returns>
    Private Function SquareRoot(ByVal value As Double) As Double
        Return Math.Sqrt(Me.LinearCoefficient * Me.LinearCoefficient -
                             4 * Me.QuadraticCoefficient * (Me.ConstantCoefficient - value))
    End Function

    ''' <summary> Calculates the positive quadratic root for the given value. </summary>
    ''' <param name="value"> Information describing the polynomial. </param>
    ''' <returns> A Double. </returns>
    Public Function PositiveQuadraticRoot(ByVal value As Double) As Double
        Return (-Me.LinearCoefficient + Me.SquareRoot(value)) / (2 * Me.QuadraticCoefficient)
    End Function

    ''' <summary> Calculates the positive quadratic root for the given value. </summary>
    ''' <param name="value"> Information describing the polynomial. </param>
    ''' <returns> A Double. </returns>
    Public Function NegativeQuadraticRoot(ByVal value As Double) As Double
        Return (-Me.LinearCoefficient - Me.SquareRoot(value)) / (2 * Me.QuadraticCoefficient)
    End Function

#End Region

#Region " GOODNESS OF FIT "

    ''' <summary> Gets or sets the standard error. </summary>
    ''' <value> The standard error. </value>
    Public ReadOnly Property StandardError As Double

    ''' <summary> Gets the goodness of fit (R-Squared). </summary>
    ''' <value> The goodness of fit (R-Squared). </value>
    Public ReadOnly Property GoodnessOfFit As Double

    ''' <summary> Gets the correlation coefficient. </summary>
    ''' <value> The correlation coefficient. </value>
    Public ReadOnly Property CorrelationCoefficient As Double
        Get
            Return Math.Sqrt(Me.GoodnessOfFit)
        End Get
    End Property

    ''' <summary> Sum squared deviations. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> The total number of squared deviations. </returns>
    Public Function SumSquaredDeviations(ByVal values As IEnumerable(Of System.Windows.Point)) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim result As Double = 0
        For Each p As System.Windows.Point In values
            Dim temp As Double = p.Y - Me.Evaluate(p.X)
            result += temp * temp
        Next
        Return result
    End Function

    ''' <summary> Sum squared mean deviations. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> The total number of squared mean deviations. </returns>
    Public Shared Function SumSquaredMeanDeviations(ByVal values As IEnumerable(Of System.Windows.Point)) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim mean As Double = 0
        Dim result As Double = 0
        If values.Any Then
            For Each p As System.Windows.Point In values
                mean += p.Y
            Next
            mean /= values.Count
            For Each p As System.Windows.Point In values
                Dim temp As Double = p.Y - mean
                result += temp * temp
            Next
        End If
        Return result
    End Function

#End Region

#Region " POLY FIT "

    ''' <summary> Fits a second order polynomial to the values. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="values"> The values. </param>
    ''' <returns> A Double. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="Body")>
    Private Function _PolyFit(ByVal values As IEnumerable(Of System.Windows.Point)) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If Not values.Any Then Throw New InvalidOperationException("Input is empty.")
        If values.Count < 3 Then Throw New ArgumentOutOfRangeException(NameOf(values), "Input is too short; requires at least 3 elements.")
        Me.ConstantCoefficient = 0
        Me.LinearCoefficient = 0
        Me.QuadraticCoefficient = 0

        ' setup the normal equations. 
        Dim dimension As Integer = 2
        Dim a(dimension, dimension + 1) As Double
        Dim coefficients(dimension, dimension) As Double
        Dim constants(dimension) As Double
        Dim dp1 As Integer = dimension + 1
        For i As Integer = 0 To dimension
            For j As Integer = 0 To dimension
                Dim k As Integer = i + j
                For l As Integer = 0 To values.Count - 1
                    a(i, j) += Math.Pow(values(l).X, k)
                Next
                coefficients(i, j) = a(i, j)
            Next
            For Each p As System.Windows.Point In values
                a(i, dp1) += p.Y * Math.Pow(p.X, i)
            Next
            constants(i) = a(i, dp1)
        Next
        Dim d As Double = QuadraticPolynomial.Determinant(coefficients)
        Me._GoodnessOfFit = Double.NaN
        Me._StandardError = Double.NaN
        If Math.Abs(d) > Single.Epsilon Then
            Dim p(dp1) As Double
            p = QuadraticPolynomial.CramerRule(coefficients, constants)
            Me.ConstantCoefficient = p(0) / d
            Me.LinearCoefficient = p(1) / d
            Me.QuadraticCoefficient = p(2) / d
            Dim ssq As Double = Me.SumSquaredDeviations(values)
            Me._StandardError = Math.Sqrt(ssq / values.Count)
            Dim ssqdm As Double = QuadraticPolynomial.SumSquaredMeanDeviations(values)
            If (ssqdm - ssq) > Single.Epsilon Then
                ' otherwise, correlation coefficient could be negative
                Me._GoodnessOfFit = (ssqdm - ssq) / ssqdm
            Else
                Me._GoodnessOfFit = 0
            End If
        End If
        Return Me.GoodnessOfFit
    End Function

    ''' <summary> Polygon fit. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> A Double. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="Body")>
    Public Function PolyFit(ByVal values As IEnumerable(Of System.Windows.Point)) As Double
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If Not values.Any Then Throw New InvalidOperationException("Input is empty.")
        Me.ConstantCoefficient = 0
        Me.LinearCoefficient = 0
        Me.QuadraticCoefficient = 0
        If values.Count = 1 Then
            Me.ConstantCoefficient = values(0).Y
            Me._GoodnessOfFit = 1
            Me._StandardError = 0
        ElseIf values.Count = 2 Then
            Dim dx As Double = (values(1).X - values(0).X)
            Dim dy As Double = (values(1).Y - values(0).Y)
            If dx > Single.Epsilon Then
                Me.LinearCoefficient = dy / dx
                Me.ConstantCoefficient = values(0).Y - values(0).X * Me.LinearCoefficient
                Me._GoodnessOfFit = 1
                Me._StandardError = 0
            Else
                Me.ConstantCoefficient = 0.5 * (values(1).Y + values(0).Y)
                Me._GoodnessOfFit = 0
                Me._StandardError = Me.ConstantCoefficient
            End If
        Else
            Me._PolyFit(values)
        End If
        Return Me.GoodnessOfFit
    End Function


    ''' <summary> Builds the matrix for calculating the numerator of the Cramer Rule. </summary>
    ''' <param name="index">        Zero-based index of the solution variable. </param>
    ''' <param name="coefficients"> The coefficients. </param>
    ''' <param name="constants">    The constants. </param>
    ''' <returns> The matrix array of solution values times the determinant. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="1#")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="Return")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="Body")>
    Public Shared Function CramerSubstitution(ByVal index As Integer, ByVal coefficients(,) As Double, ByVal constants() As Double) As Double(,)
        If coefficients Is Nothing Then Throw New ArgumentNullException(NameOf(coefficients))
        If constants Is Nothing Then Throw New ArgumentNullException(NameOf(constants))
        If index < 0 Then Throw New ArgumentOutOfRangeException(NameOf(index), "Must be non-negative")
        If index >= constants.Count Then Throw New ArgumentOutOfRangeException(NameOf(index), $"Must be less than {constants.Count}")
        Dim d As Integer = constants.Count - 1
        Dim a(d, d) As Double
        For col As Integer = 0 To d
            For row As Integer = 0 To d
                If col = index Then
                    a(col, row) = constants(row)
                Else
                    a(col, row) = coefficients(col, row)
                End If
            Next
        Next
        Return a
    End Function

    ''' <summary> Use Cramer rule to calculate the solution of the linear equations. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="coefficients"> The coefficients. </param>
    ''' <param name="constants">    The constants. </param>
    ''' <returns> The array of solution values times the determinant. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="0#")>
    Public Shared Function CramerRule(ByVal coefficients(,) As Double, ByVal constants() As Double) As Double()
        If coefficients Is Nothing Then Throw New ArgumentNullException(NameOf(coefficients))
        If constants Is Nothing Then Throw New ArgumentNullException(NameOf(constants))
        Dim l As New List(Of Double)
        For Xi As Integer = 0 To constants.Count - 1
            Dim a(,) As Double = CramerSubstitution(Xi, coefficients, constants)
            l.Add(QuadraticPolynomial.Determinant(a))
        Next
        Return l.ToArray
    End Function

    ''' <summary> Determinants. </summary>
    ''' <param name="matrix"> The matrix values to process. </param>
    ''' <returns> A Double. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="0#")>
    Public Shared Function Determinant(ByVal matrix(,) As Double) As Double
        If matrix Is Nothing Then Throw New ArgumentNullException(NameOf(matrix))
        ' If Not matrix.Any Then Throw New InvalidOperationException("matrix is empty.")
        If matrix.GetLength(0) <> 3 Then Throw New ArgumentOutOfRangeException(NameOf(matrix), $"First dimension {matrix.GetLength(0)} must be 3.")
        If matrix.GetLength(1) <> 3 Then Throw New ArgumentOutOfRangeException(NameOf(matrix), $"Second dimension {matrix.GetLength(1)} must be 3.")
        Dim result As Double = 0
        result += matrix(0, 0) * matrix(1, 1) * matrix(2, 2)
        result -= matrix(0, 0) * matrix(1, 2) * matrix(2, 1)
        result -= matrix(0, 1) * matrix(1, 0) * matrix(2, 2)
        result += matrix(0, 1) * matrix(1, 2) * matrix(2, 0)
        result += matrix(0, 2) * matrix(1, 0) * matrix(2, 1)
        result -= matrix(0, 2) * matrix(1, 1) * matrix(2, 0)
        Return result
    End Function

#End Region

End Class

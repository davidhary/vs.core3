''' <summary>
''' Defines a general method to test it an item should be included in a
''' <see cref="BindingListView(Of T)"/>.
''' </summary>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
'''
''' ### <typeparam name="T"> The type of item to be filtered. </typeparam>
Public Interface IItemFilter(Of T)

    ''' <summary> Tests if the item should be included. </summary>
    ''' <param name="item"> The item to test. </param>
    ''' <returns> True if the item should be included, otherwise false. </returns>
    Function Include(ByVal item As T) As Boolean
End Interface

''' <summary>
''' A dummy filter that is used when no filter is needed. It simply includes any and all items
''' tested.
''' </summary>
''' <license>
''' (c) 2006 Andrew Davey. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/15/2018" by="David" revision="">      Created. </history>
''' <history date="12/15/2018" by="David" revision="1.2.*"> https://blogs.warwick.ac.uk/andrewdavey
'''                                                         and
'''                                                         https://sourceforge.net/projects/blw/. </history>
Public Class IncludeAllItemFilter(Of T)
    Implements IItemFilter(Of T)

    ''' <summary> Tests if the item should be included. </summary>
    ''' <param name="item"> The item to test. </param>
    ''' <returns> True if the item should be included, otherwise false. </returns>
    Public Function Include(ByVal item As T) As Boolean Implements IItemFilter(Of T).Include
        ' All items are to be included.
        ' So always return true.
        Return True
    End Function

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return My.Resources.NoFilter
    End Function

#Region " Singleton Accessor "

    ''' <summary> The instance. </summary>
    Private Shared _instance As IncludeAllItemFilter(Of T)

    ''' <summary> Gets the singleton instance of <see cref="IncludeAllItemFilter(Of T)"/>. </summary>
    ''' <value> The instance. </value>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Shared ReadOnly Property Instance() As IncludeAllItemFilter(Of T)
        Get
            If _instance Is Nothing Then
                _instance = New IncludeAllItemFilter(Of T)()
            End If
            Return _instance
        End Get
    End Property

#End Region

End Class

''' <summary>
''' A filter that uses a user-defined <see cref="Predicate(Of T)"/> to test items for inclusion
''' in <see cref="BindingListView(Of T)"/>.
''' </summary>
''' <license>
''' (c) 2006 Andrew Davey. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/15/2018" by="David" revision="">      Created. </history>
''' <history date="12/15/2018" by="David" revision="1.2.*"> https://blogs.warwick.ac.uk/andrewdavey
'''                                                         and
'''                                                         https://sourceforge.net/projects/blw/. </history>
Public Class PredicateItemFilter(Of T)
    Implements IItemFilter(Of T)

    ''' <summary>
    ''' Creates a new <see cref="PredicateItemFilter(Of T)"/> that uses the specified
    ''' <see cref="Predicate(Of T)"/> and default name.
    ''' </summary>
    ''' <param name="includeDelegate"> The <see cref="Predicate(Of T)"/> used to test items. </param>
    Public Sub New(ByVal includeDelegate As Predicate(Of T))
        Me.New(includeDelegate, Nothing)
        ' The other constructor is called to do the work.
    End Sub

    ''' <summary>
    ''' Creates a new <see cref="PredicateItemFilter(Of T)"/> that uses the specified
    ''' <see cref="Predicate(Of T)"/>.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="includeDelegate"> The <see cref="PredicateItemFilter(Of T)"/> used to test
    '''                                items. </param>
    ''' <param name="name">            The name used for the ToString() return value. </param>
    Public Sub New(ByVal includeDelegate As Predicate(Of T), ByVal name As String)
        ' We don't allow a null string. Use the default instead.
        _name = If(name, defaultName)
        If includeDelegate IsNot Nothing Then
            _includeDelegate = includeDelegate
        Else
            Throw New ArgumentNullException("includeDelegate", My.Resources.IncludeDelegateCannotBeNull)
        End If
    End Sub

    ''' <summary> The include delegate. </summary>
    Private ReadOnly _includeDelegate As Predicate(Of T)

    ''' <summary> The name. </summary>
    Private ReadOnly _name As String

    ''' <summary> The default name. </summary>
    Private ReadOnly defaultName As String = My.Resources.PredicateFilter

    ''' <summary> Tests if the item should be included. </summary>
    ''' <param name="item"> The item to test. </param>
    ''' <returns> True if the item should be included, otherwise false. </returns>
    Public Function Include(ByVal item As T) As Boolean Implements IItemFilter(Of T).Include
        Return _includeDelegate(item)
    End Function

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return _name
    End Function
End Class


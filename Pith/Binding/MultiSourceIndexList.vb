Imports System.ComponentModel
Imports System.Collections

''' <summary> Multi source index list. </summary>
''' <license>
''' (c) 2006 Andrew Davey. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/15/2018" by="David" revision="">      Created. </history>
''' <history date="12/15/2018" by="David" revision="1.2.*"> https://blogs.warwick.ac.uk/andrewdavey
'''                                                         and
'''                                                         https://sourceforge.net/projects/blw/. </history>
Friend Class MultiSourceIndexList(Of T)
    Inherits List(Of KeyValuePair(Of ListItemPair(Of T), Integer))

    ''' <summary> Adds sourceList. </summary>
    ''' <param name="sourceList"> List of sources. </param>
    ''' <param name="item">       The <see cref="ObjectView(Of T)"/> to search for. </param>
    ''' <param name="index">      Zero-based index of the. </param>
    Public Overloads Sub Add(ByVal sourceList As IList, ByVal item As ObjectView(Of T), ByVal index As Integer)
        MyBase.Add(New KeyValuePair(Of ListItemPair(Of T), Integer)(New ListItemPair(Of T)(sourceList, item), index))
    End Sub

    ''' <summary>
    ''' Searches for a given source index value, returning the list index of the value.
    ''' </summary>
    ''' <param name="sourceList">  List of sources. </param>
    ''' <param name="sourceIndex"> The source index to find. </param>
    ''' <returns> Returns the index in this list of the source index, or -1 if not found. </returns>
    Public Function IndexOfSourceIndex(ByVal sourceList As IList, ByVal sourceIndex As Integer) As Integer
        For i As Integer = 0 To Count - 1
            If Me(i).Key.List Is sourceList AndAlso Me(i).Value = sourceIndex Then
                Return i
            End If
        Next i
        Return -1
    End Function

    ''' <summary> Searches for a given item, returning the index of the value in this list. </summary>
    ''' <param name="item"> The <typeparamref name="T"/> item to search for. </param>
    ''' <returns> Returns the index in this list of the item, or -1 if not found. </returns>
    Public Function IndexOfItem(ByVal item As T) As Integer
        For i As Integer = 0 To Count - 1
            If Me(i).Key.Item.Object.Equals(item) AndAlso Me(i).Value > -1 Then
                Return i
            End If
        Next i
        Return -1
    End Function

    ''' <summary>
    ''' Searches for a given item's <see cref="ObjectView(Of T)"/> wrapper, returning the index of
    ''' the value in this list.
    ''' </summary>
    ''' <param name="item"> The <see cref="ObjectView(Of T)"/> to search for. </param>
    ''' <returns> Returns the index in this list of the item, or -1 if not found. </returns>
    Public Function IndexOfKey(ByVal item As ObjectView(Of T)) As Integer
        For i As Integer = 0 To Count - 1
            If Me(i).Key.Item.Equals(item) AndAlso Me(i).Value > -1 Then
                Return i
            End If
        Next i
        Return -1
    End Function

    ''' <summary> Checks if the list contains a given item. </summary>
    ''' <param name="item"> The <typeparamref name="T"/> item to check for. </param>
    ''' <returns> True if the item is contained in the list, otherwise false. </returns>
    Public Function ContainsItem(ByVal item As T) As Boolean
        Return (IndexOfItem(item) <> -1)
    End Function

    ''' <summary> Checks if the list contains a given <see cref="ObjectView(Of T)"/> key. </summary>
    ''' <param name="key"> The key to search for. </param>
    ''' <returns> True if the key is contained in the list, otherwise false. </returns>
    Public Function ContainsKey(ByVal key As ObjectView(Of T)) As Boolean
        Return (IndexOfKey(key) <> -1)
    End Function

    ''' <summary>
    ''' Returns an array of all the <see cref="ObjectView(Of T)"/> keys in the list.
    ''' </summary>
    ''' <value> The keys. </value>
    Public ReadOnly Property Keys() As ObjectView(Of T)()
        Get
            Return ConvertAll(Of ObjectView(Of T))(New Converter(Of KeyValuePair(Of ListItemPair(Of T), Integer), ObjectView(Of T))(Function(kvp As KeyValuePair(Of ListItemPair(Of T), Integer)) kvp.Key.Item)).ToArray()
        End Get
    End Property

    ''' <summary>
    ''' Returns an <see cref="IEnumerator(Of T)"/> to iterate over all the keys in this list.
    ''' </summary>
    ''' <returns> The <see cref="IEnumerator(Of T)"/> to use. </returns>
    Public Iterator Function GetKeyEnumerator() As IEnumerator(Of ObjectView(Of T))
        For Each kvp As KeyValuePair(Of ListItemPair(Of T), Integer) In Me
            Yield kvp.Key.Item
        Next kvp
    End Function
End Class

''' <summary> A t. </summary>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/15/2018" by="David" revision=""> Created. </history>
Friend Class ListItemPair(Of T)
    ''' <summary> The list. </summary>
    Private ReadOnly _list As IList
    ''' <summary> The item. </summary>
    Private ReadOnly _item As ObjectView(Of T)

    ''' <summary> Constructor. </summary>
    ''' <param name="list"> The list. </param>
    ''' <param name="item"> The item. </param>
    Public Sub New(ByVal list As IList, ByVal item As ObjectView(Of T))
        _list = list
        _item = item
    End Sub

    ''' <summary> Gets the list. </summary>
    ''' <value> The list. </value>
    Public ReadOnly Property List() As IList
        Get
            Return _list
        End Get
    End Property

    ''' <summary> Gets the item. </summary>
    ''' <value> The item. </value>
    Public ReadOnly Property Item() As ObjectView(Of T)
        Get
            Return _item
        End Get
    End Property
End Class

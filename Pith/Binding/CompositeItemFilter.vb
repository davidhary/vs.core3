''' <summary> Composite item filter. </summary>
''' <license>
''' (c) 2006 Andrew Davey. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/15/2018" by="David" revision="">      Created. </history>
''' <history date="12/15/2018" by="David" revision="1.2.*"> https://blogs.warwick.ac.uk/andrewdavey
'''                                                         and
'''                                                         https://sourceforge.net/projects/blw/. </history>
Public Class CompositeItemFilter(Of T)
    Implements IItemFilter(Of T)

    ''' <summary> The filters. </summary>
    Private _filters As List(Of IItemFilter(Of T))

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        _filters = New List(Of IItemFilter(Of T))()
    End Sub

    ''' <summary> Adds a filter. </summary>
    ''' <param name="filter"> Specifies the filter. </param>
    Public Sub AddFilter(ByVal filter As IItemFilter(Of T))
        _filters.Add(filter)
    End Sub

    ''' <summary> Removes the filter described by filter. </summary>
    ''' <param name="filter"> Specifies the filter. </param>
    Public Sub RemoveFilter(ByVal filter As IItemFilter(Of T))
        _filters.Remove(filter)
    End Sub

    ''' <summary> Tests if the item should be included. </summary>
    ''' <param name="item"> The item to test. </param>
    ''' <returns> True if the item should be included, otherwise false. </returns>
    Public Function Include(ByVal item As T) As Boolean Implements IItemFilter(Of T).Include
        For Each filter As IItemFilter(Of T) In _filters
            If Not filter.Include(item) Then
                Return False
            End If
        Next filter
        Return True
    End Function

End Class

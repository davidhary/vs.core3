Imports System.Text

''' <summary> Exception for signaling invalid source list errors. </summary>
''' <license>
''' (c) 2006 Andrew Davey. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/15/2018" by="David" revision="1.2.*"> https://blogs.warwick.ac.uk/andrewdavey
'''                                                         and
'''                                                         https://sourceforge.net/projects/blw/. </history>
<Serializable>
Public Class InvalidSourceListException
    Inherits Exception

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New(My.Resources.InvalidSourceList)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="message"> The message. </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    Public Sub New(ByVal message As String, ByVal innerException As Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="info">    The information. </param>
    ''' <param name="context"> The context. </param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub
End Class

Imports System.ComponentModel

''' <summary>
''' Extends <see cref="System.ComponentModel.IEditableObject"/> by providing events to raise
''' during edit state changes.
''' </summary>
''' <license>
''' (c) 2006 Andrew Davey. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/15/2018" by="David" revision="1.2.*">
''' https://blogs.warwick.ac.uk/andrewdavey and https://sourceforge.net/projects/blw/.
''' </history>
Friend Interface INotifyingEditableObject
    Inherits IEditableObject

    ''' <summary>
    ''' An edit has started on the object.
    ''' </summary>
    ''' <remarks>
    ''' This event should be raised from BeginEdit().
    ''' </remarks>
    Event EditBegun As EventHandler

    ''' <summary>
    ''' The editing of the object was canceled.
    ''' </summary>
    ''' <remarks>
    ''' This event should be raised from CancelEdit().
    ''' </remarks>
    ''' 
    Event EditCanceled As EventHandler
    ''' <summary>
    ''' The editing of the object was ended.
    ''' </summary>
    ''' <remarks>
    ''' This event should be raised from EndEdit().
    ''' </remarks>
    Event EditEnded As EventHandler

End Interface

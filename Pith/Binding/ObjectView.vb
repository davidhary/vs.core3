Imports System.ComponentModel

''' <summary>
''' Serves a wrapper for items being viewed in a <see cref="BindingListView(Of T)"/>. This class
''' implements <see cref="INotifyingEditableObject"/> so will raise the necessary events during
''' the item edit life-cycle.
''' </summary>
''' <remarks>
''' If <typeparamref name="T"/> implements <see cref="System.ComponentModel.IEditableObject"/>
''' this class will call BeginEdit/CancelEdit/EndEdit on the <typeparamref name="T"/> object as
''' well. If <typeparamref name="T"/> implements
''' <see cref="System.ComponentModel.IDataErrorInfo"/> this class will use that implementation as
''' its own.
''' </remarks>
''' <license>
''' (c) 2006 Andrew Davey. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/15/2018" by="David">                  >      Created. </history>
''' <history date="12/15/2018" by="David" revision="1.2.*"> https://blogs.warwick.ac.uk/andrewdavey
'''                                                         and
'''                                                         https://sourceforge.net/projects/blw/. </history>
<Serializable>
Public Class ObjectView(Of T)
    Implements INotifyingEditableObject, IDataErrorInfo, INotifyPropertyChanged, ICustomTypeDescriptor

    ''' <summary>
    ''' Creates a new <see cref="ObjectView(Of T)"/> wrapper for a <typeparamref name="T"/> object.
    ''' </summary>
    ''' <param name="[object]"> Gets the object being edited. </param>
    ''' <param name="parent">   The view containing this ObjectView. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")>
    Public Sub New(ByVal [object] As T, ByVal parent As AggregateBindingListView(Of T))
        Me._parent = parent
        Me.Object = [object]
        If TypeOf [object] Is INotifyPropertyChanged Then
            AddHandler CType([object], INotifyPropertyChanged).PropertyChanged, AddressOf ObjectPropertyChanged
        End If

        If GetType(ICustomTypeDescriptor).IsAssignableFrom(GetType(T)) Then
            _isCustomTypeDescriptor = True
            _customTypeDescriptor = TryCast([object], ICustomTypeDescriptor)
            Debug.Assert(_customTypeDescriptor IsNot Nothing)
        End If
        _providedViews = New Dictionary(Of String, Object)()
        Me.CreateProvidedViews()
    End Sub

    ''' <summary>
    ''' The view containing this ObjectView.
    ''' </summary>
    <NonSerialized>
    Private _parent As AggregateBindingListView(Of T)

    ''' <summary>
    ''' Flag that signals if we are currently editing the object.
    ''' </summary>
    Private _editing As Boolean

    ''' <summary>
    ''' The actual object being edited.
    ''' </summary>
    Private _object As T

    ''' <summary>
    ''' Flag set to true if type of T implements ICustomTypeDescriptor
    ''' </summary>
    Private ReadOnly _isCustomTypeDescriptor As Boolean

    ''' <summary>
    ''' Holds the Object pre-casted ICustomTypeDescriptor (if supported).
    ''' </summary>
    Private _customTypeDescriptor As ICustomTypeDescriptor

    ''' <summary>
    ''' A collection of BindingListView objects, indexed by name, for views auto-provided for any generic IList members.
    ''' </summary>
    Private _providedViews As Dictionary(Of String, Object)

    ''' <summary>
    ''' Gets the object being edited.
    ''' </summary>
    Public Property [Object]() As T
        Get
            Return _object
        End Get
        Private Set(ByVal value As T)
            If value Is Nothing Then
                Throw New ArgumentNullException(NameOf(value), My.Resources.ObjectCannotBeNull)
            End If
            _object = value
        End Set
    End Property

    ''' <summary> Gets provided view. </summary>
    ''' <param name="name"> The name. </param>
    ''' <returns> The provided view. </returns>
    Public Function GetProvidedView(ByVal name As String) As Object
        Return _providedViews(name)
    End Function

    ''' <summary> Casts an ObjectView(Of T) to a T by getting the wrapped T object. </summary>
    ''' <param name="value"> The ObjectView(Of T) to cast to a T. </param>
    ''' <returns> The object that is wrapped. </returns>
    Public Shared Narrowing Operator CType(ByVal value As ObjectView(Of T)) As T
        If value Is Nothing Then
            Return Nothing
        Else
            Return value.Object
        End If
    End Operator

    ''' <summary> Determines whether the specified object is equal to the current object. </summary>
    ''' <param name="obj"> The object to compare with the current object. </param>
    ''' <returns>
    ''' true if the specified object  is equal to the current object; otherwise, false.
    ''' </returns>
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        If obj Is Nothing Then
            Return False
        ElseIf TypeOf obj Is T Then
            Return Me.[Object].Equals(obj)
        ElseIf TypeOf obj Is ObjectView(Of T) Then
            Return Me.[Object].Equals((TryCast(obj, ObjectView(Of T))).Object)
        Else
            Return MyBase.Equals(obj)
        End If
    End Function

    ''' <summary> Serves as the default hash function. </summary>
    ''' <returns> A hash code for the current object. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.[Object].GetHashCode()
    End Function

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return [Object].ToString()
    End Function

    ''' <summary> Object property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Private Sub ObjectPropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        ' Raise our own event
        OnPropertyChanged(sender, New PropertyChangedEventArgs(e.PropertyName))
    End Sub

    ''' <summary> Should provide view of the given list property. </summary>
    ''' <param name="listProp"> The list property. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function ShouldProvideViewOf(ByVal listProp As PropertyDescriptor) As Boolean
        Return _parent.ShouldProvideView(listProp)
    End Function

    ''' <summary> Gets provided view name. </summary>
    ''' <param name="listProp"> The list property. </param>
    ''' <returns> The provided view name. </returns>
    Private Function GetProvidedViewName(ByVal listProp As PropertyDescriptor) As String
        Return _parent.GetProvidedViewName(listProp)
    End Function

    ''' <summary> Creates provided views. </summary>
    Private Sub CreateProvidedViews()
        For Each prop As PropertyDescriptor In (TryCast(Me, ICustomTypeDescriptor)).GetProperties()
            If ShouldProvideViewOf(prop) Then
                Dim view As Object = _parent.CreateProvidedView(Me, prop)
                Dim viewName As String = GetProvidedViewName(prop)
                _providedViews.Add(viewName, view)
            End If
        Next prop
    End Sub

#Region " INotifyEditableObject Members "

    ''' <summary>
    ''' Indicates an edit has just begun.
    ''' </summary>
    <NonSerialized>
    Public Event EditBegun As EventHandler Implements INotifyingEditableObject.EditBegun

    ''' <summary>
    ''' Indicates the edit was canceled.
    ''' </summary>
    <NonSerialized>
    Public Event EditCanceled As EventHandler Implements INotifyingEditableObject.EditCanceled

    ''' <summary>
    ''' Indicated the edit was ended.
    ''' </summary>
    <NonSerialized>
    Public Event EditEnded As EventHandler Implements INotifyingEditableObject.EditEnded

    ''' <summary> Executes the edit begun action. </summary>
    Protected Overridable Sub OnEditBegun()
        RaiseEvent EditBegun(Me, EventArgs.Empty)
    End Sub

    ''' <summary> Executes the edit canceled action. </summary>
    Protected Overridable Sub OnEditCanceled()
        RaiseEvent EditCanceled(Me, EventArgs.Empty)
    End Sub

    ''' <summary> Executes the edit ended action. </summary>
    Protected Overridable Sub OnEditEnded()
        RaiseEvent EditEnded(Me, EventArgs.Empty)
    End Sub

#End Region

#Region " IEditableObject Members "

    ''' <summary> Begins an edit. </summary>
    Public Sub BeginEdit() Implements System.ComponentModel.IEditableObject.BeginEdit
        ' As per documentation, this method may get called multiple times for a single edit.
        ' So we set a flag to only honor the first call.
        If Not _editing Then
            _editing = True
            ' If possible call the object's BeginEdit() method
            ' to let it do what ever it needs e.g. save state
            If TypeOf [Object] Is IEditableObject Then
                CType([Object], IEditableObject).BeginEdit()
            End If
            ' Raise the EditBegun event.                
            OnEditBegun()
        End If
    End Sub

    ''' <summary> Cancel edit. </summary>
    Public Sub CancelEdit() Implements System.ComponentModel.IEditableObject.CancelEdit
        ' We can only cancel if currently editing
        If _editing Then
            ' If possible call the object's CancelEdit() method
            ' to let it do what ever it needs e.g. rollback state
            If TypeOf [Object] Is IEditableObject Then
                CType([Object], IEditableObject).CancelEdit()
            End If
            ' Raise the EditCancelled event.
            OnEditCanceled()
            ' No longer editing now.
            _editing = False
        End If
    End Sub

    ''' <summary> Ends an edit. </summary>
    Public Sub EndEdit() Implements System.ComponentModel.IEditableObject.EndEdit
        ' We can only end if currently editing. 
        If _editing Then
            ' If possible call the object's EndEdit() method
            ' to let it do what ever it needs e.g. commit state
            If TypeOf [Object] Is IEditableObject Then
                CType([Object], IEditableObject).EndEdit()
            End If
            ' Raise the EditEnded event.
            OnEditEnded()
            ' No longer editing now.
            _editing = False
        End If
    End Sub

#End Region

#Region " IDataErrorInfo Members "

    ''' <summary> Gets the data error information error. </summary>
    ''' <remarks>
    ''' If the wrapped Object support IDataErrorInfo we forward calls to it. Otherwise, we just
    ''' return empty strings that signal "no error".
    ''' </remarks>
    ''' <value> The i data error information error. </value>
    Private ReadOnly Property IDataErrorInfo_Error() As String Implements IDataErrorInfo.Error
        Get
            If TypeOf [Object] Is IDataErrorInfo Then
                Return CType([Object], IDataErrorInfo).Error
            End If
            Return String.Empty
        End Get
    End Property

    ''' <summary> Gets the data error information item. </summary>
    ''' <value> The i data error information item. </value>
    Public ReadOnly Property IDataErrorInfoItem(ByVal columnName As String) As String Implements IDataErrorInfo.Item
        Get
            If TypeOf [Object] Is IDataErrorInfo Then
                Return CType([Object], IDataErrorInfo)(columnName)
            End If
            Return String.Empty
        End Get
    End Property

#End Region

#Region " INotifyPropertyChanged Members "

    ''' <summary> Event queue for all listeners interested in PropertyChanged events. </summary>
    <NonSerialized>
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary> Raises the property changed event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="args">   Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnPropertyChanged(ByVal sender As Object, ByVal args As PropertyChangedEventArgs)
        RaiseEvent PropertyChanged(sender, args)
    End Sub

#End Region

#Region " ICustomTypeDescriptor Members "

    Private Function ICustomTypeDescriptor_GetAttributes() As AttributeCollection Implements ICustomTypeDescriptor.GetAttributes
        If _isCustomTypeDescriptor Then
            Return _customTypeDescriptor.GetAttributes()
        Else
            Return TypeDescriptor.GetAttributes([Object])
        End If
    End Function

    Private Function ICustomTypeDescriptor_GetClassName() As String Implements ICustomTypeDescriptor.GetClassName
        If _isCustomTypeDescriptor Then
            Return _customTypeDescriptor.GetClassName()
        Else
            Return GetType(T).FullName
        End If
    End Function

    Private Function ICustomTypeDescriptor_GetComponentName() As String Implements ICustomTypeDescriptor.GetComponentName
        If _isCustomTypeDescriptor Then
            Return _customTypeDescriptor.GetComponentName()
        Else
            Return TypeDescriptor.GetFullComponentName([Object])
        End If
    End Function

    Private Function ICustomTypeDescriptor_GetConverter() As ComponentModel.TypeConverter Implements ICustomTypeDescriptor.GetConverter
        If _isCustomTypeDescriptor Then
            Return _customTypeDescriptor.GetConverter()
        Else
            Return TypeDescriptor.GetConverter([Object])
        End If
    End Function

    Private Function ICustomTypeDescriptor_GetDefaultEvent() As EventDescriptor Implements ICustomTypeDescriptor.GetDefaultEvent
        If _isCustomTypeDescriptor Then
            Return _customTypeDescriptor.GetDefaultEvent()
        Else
            Return TypeDescriptor.GetDefaultEvent([Object])
        End If
    End Function

    Private Function ICustomTypeDescriptor_GetDefaultProperty() As PropertyDescriptor Implements ICustomTypeDescriptor.GetDefaultProperty
        If _isCustomTypeDescriptor Then
            Return _customTypeDescriptor.GetDefaultProperty()
        Else
            Return TypeDescriptor.GetDefaultProperty([Object])
        End If
    End Function

    Private Function ICustomTypeDescriptor_GetEditor(ByVal editorBaseType As Type) As Object Implements ICustomTypeDescriptor.GetEditor
        If _isCustomTypeDescriptor Then
            Return _customTypeDescriptor.GetEditor(editorBaseType)
        Else
            Return Nothing
        End If
    End Function

    Private Function ICustomTypeDescriptor_GetEvents(ByVal attributes() As Attribute) As EventDescriptorCollection Implements ICustomTypeDescriptor.GetEvents
        If _isCustomTypeDescriptor Then
            Return _customTypeDescriptor.GetEvents()
        Else
            Return TypeDescriptor.GetEvents([Object], attributes)
        End If
    End Function

    Private Function ICustomTypeDescriptor_GetEvents() As EventDescriptorCollection Implements ICustomTypeDescriptor.GetEvents
        If _isCustomTypeDescriptor Then
            Return _customTypeDescriptor.GetEvents()
        Else
            Return TypeDescriptor.GetEvents([Object])
        End If
    End Function

    Private Function ICustomTypeDescriptor_GetProperties(ByVal attributes() As Attribute) As PropertyDescriptorCollection Implements ICustomTypeDescriptor.GetProperties
        If _isCustomTypeDescriptor Then
            Return _customTypeDescriptor.GetProperties()
        Else
            Return TypeDescriptor.GetProperties([Object], attributes)
        End If
    End Function

    ''' <summary> Custom type descriptor get properties. </summary>
    ''' <returns> A PropertyDescriptorCollection. </returns>
    Private Function ICustomTypeDescriptor_GetProperties() As PropertyDescriptorCollection Implements ICustomTypeDescriptor.GetProperties
        If _isCustomTypeDescriptor Then
            Return _customTypeDescriptor.GetProperties()
        Else
            Return TypeDescriptor.GetProperties([Object])
        End If
    End Function

    ''' <summary> Custom type descriptor get property owner. </summary>
    ''' <param name="pd"> The pd. </param>
    ''' <returns> An Object. </returns>
    Private Function ICustomTypeDescriptor_GetPropertyOwner(ByVal pd As PropertyDescriptor) As Object Implements ICustomTypeDescriptor.GetPropertyOwner
        If _isCustomTypeDescriptor Then
            Return _customTypeDescriptor.GetPropertyOwner(pd)
        Else
            Return [Object]
        End If
    End Function

#End Region

End Class

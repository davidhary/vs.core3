﻿Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Imports System.Windows.Forms
Namespace EnumExtensions
    ''' <summary> Includes extensions for enumerations and enumerated types. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    ''' <history date="07/07/2009" by="David" revision="1.2.3475.x"> <para>
    ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx </para><para>
    ''' Under The Creative Commons Attribution-ShareAlike 2.5 License.</para>
    ''' </history>
    Public Module Methods

#Region " VALUE DESCRIPTION PAIRS "

        ''' <summary> Displays a value description pair. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="enumConstant"> The <see cref="T:System.Enum"/> constant. </param>
        ''' <param name="comboBox">     The combo box. </param>
        <Extension()>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")>
        Public Function DisplayValueDescriptionPairs(ByVal enumConstant As System.Enum, ByVal comboBox As ComboBox) As Integer
            If enumConstant Is Nothing Then Throw New ArgumentNullException(NameOf(enumConstant))
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            With comboBox
                .DataSource = Nothing
                .Items.Clear()
                .DataSource = Methods.ValueDescriptionBindingList(enumConstant)
                .DisplayMember = "Value"
                .ValueMember = "Key"
            End With
            Return comboBox.Items.Count
        End Function

        ''' <summary> Gets a Key Value Pair description item. </summary>
        ''' <param name="enumConstant"> The <see cref="T:System.Enum"/> constant. </param>
        ''' <returns> A list of. </returns>
        <Extension()>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")>
        Public Function ValueDescriptionPair(ByVal enumConstant As System.Enum) As System.Collections.Generic.KeyValuePair(Of System.Enum, String)
            If enumConstant Is Nothing Then Throw New ArgumentNullException(NameOf(enumConstant))
            Return New System.Collections.Generic.KeyValuePair(Of System.Enum, String)(enumConstant, enumConstant.Description)
        End Function

        ''' <summary>
        ''' Enumerates the value description pairs of the <see cref="T:System.Enum"/>
        ''' </summary>
        ''' <param name="enumConstant"> The <see cref="T:System.Enum"/> constant. </param>
        ''' <returns>
        ''' An <see cref="IEnumerable"/> containing the enumerated type value (key) and description
        ''' (value) pairs.
        ''' </returns>
        <Extension()>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")>
        Public Function ValueDescriptionPairs(ByVal enumConstant As System.Enum) As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))
            If enumConstant Is Nothing Then Throw New ArgumentNullException(NameOf(enumConstant))
            Return enumConstant.GetType.ValueDescriptionPairs
        End Function

        ''' <summary> Value description binding list. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="enumConstant"> The <see cref="T:System.Enum"/> constant. </param>
        ''' <returns> A list of. </returns>
        <Extension()>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")>
        Public Function ValueDescriptionBindingList(ByVal enumConstant As System.Enum) As BindingList(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))
            If enumConstant Is Nothing Then Throw New ArgumentNullException(NameOf(enumConstant))
            Dim result As New BindingList(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))
            For Each pair As System.Collections.Generic.KeyValuePair(Of System.Enum, String) In enumConstant.GetType.ValueDescriptionPairs
                result.Add(pair)
            Next
            Return result
        End Function

        ''' <summary>
        ''' Enumerates the value description pairs of the <see cref="T:System.Enum"/>
        ''' </summary>
        ''' <param name="type"> The <see cref="T:System.Enum"/> type. </param>
        ''' <returns> An <see cref="IEnumerable"/> containing the enumerated type value (key) and description (value) pairs. </returns>
        ''' <example> The first step is to add a description attribute to your Enum.
        ''' <code>
        '''     Public Enum SimpleEnum2
        '''       [System.ComponentModel.Description("Today")] Today
        '''       [System.ComponentModel.Description("Last 7 days")] Last7
        '''       [System.ComponentModel.Description("Last 14 days")] Last14
        '''       [System.ComponentModel.Description("Last 30 days")] Last30
        '''       [System.ComponentModel.Description("All")] All
        '''     End Enum
        '''     Dim combo As ComboBox = New ComboBox()
        '''     combo.DataSource = GetType(SimpleEnum2).ValueDescriptionPairs()
        '''     combo.DisplayMember = "Value"
        '''     combo.ValueMember = "Key"
        ''' </code>
        ''' This works with any control that supports data binding, including the
        ''' <see cref="ToolStripComboBox">tool strip combo box</see>,
        ''' although you will need to cast the
        ''' <see cref="ToolStripComboBox.Control">control property</see>
        ''' to a <see cref="ComboBox">Combo Box</see> to get to the DataSource property. In
        ''' that case, you will also want to perform the same cast when you are referencing the selected
        ''' value to work with it as your Enum type.</example>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")>
        <Extension()>
        Public Function ValueDescriptionPairs(ByVal type As Type) As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))

            Dim keyValuePairs As New List(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))
            For Each value As System.Enum In System.Enum.GetValues(type)
                keyValuePairs.Add(value.ValueDescriptionPair())
            Next value
            Return keyValuePairs

        End Function

        ''' <summary>
        ''' Converts the <see cref="T:System.Enum"/> type to an <see cref="IEnumerable"/> compatible
        ''' object.
        ''' </summary>
        ''' <param name="type"> Specifies the Enum type. </param>
        ''' <param name="mask"> Specifies a mask for selecting the items to include in the list. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process value (key) description (value) pairs
        ''' in this collection.
        ''' </returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")>
        <Extension()>
        Public Function ValueDescriptionPairs(ByVal type As Type, ByVal mask As Integer) As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))

            Dim keyValuePairs As New List(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))
            For Each value As System.Enum In System.Enum.GetValues(type)
                Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
                If (enumValue And mask) <> 0 Then
                    keyValuePairs.Add(value.ValueDescriptionPair())
                End If
            Next value
            Return keyValuePairs

        End Function

        ''' <summary>
        ''' Converts the <see cref="T:System.Enum"/> type to an <see cref="IEnumerable"/> compatible
        ''' object.
        ''' </summary>
        ''' <param name="type">           Specifies the Enum type. </param>
        ''' <param name="excludedValues"> Specifies list of excluded values. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process value (key) description (value)
        ''' pairs in this collection.
        ''' </returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")>
        <Extension()>
        Public Function ValueDescriptionPairs(ByVal type As Type, ByVal excludedValues As IEnumerable(Of Integer)) As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))

            Dim keyValuePairs As New List(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))
            For Each value As System.Enum In System.Enum.GetValues(type)
                Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
                If Not excludedValues.Contains(enumValue) Then
                    keyValuePairs.Add(value.ValueDescriptionPair())
                End If
            Next value
            Return keyValuePairs

        End Function

#End Region

#Region " DESCRIPTIONS "

        ''' <summary> Gets the <see cref="DescriptionAttribute"/> of an <see cref="T:System.Enum"/> type
        ''' value. </summary>
        ''' <param name="value"> The <see cref="T:System.Enum"/> value. </param>
        ''' <returns> A string containing the text of the <see cref="DescriptionAttribute"/>. </returns>
        <Extension()>
        Public Function Description(ByVal value As System.Enum) As String
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            Dim candidate As String = value.ToString()
            Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(candidate)
            Dim attributes As DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())
            If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
                candidate = attributes(0).Description
            End If
            Return candidate
        End Function

        ''' <summary> Returns the descriptions of an enumeration. </summary>
        ''' <param name="type"> The <see cref="T:System.Enum"/> type. </param>
        ''' <returns> A list of strings. </returns>
        <Extension()>
        Public Function Descriptions(ByVal type As Type) As IEnumerable(Of String)

            Dim values As New System.Collections.Generic.List(Of String)
            For Each enumValue As System.Enum In System.Enum.GetValues(type)
                values.Add(enumValue.Description())
            Next enumValue
            Return values

        End Function

        ''' <summary> Query if 'type' has description. </summary>
        ''' <param name="type">        The <see cref="T:System.Enum"/>
        '''                            type. </param>
        ''' <param name="description"> The description. </param>
        ''' <returns> <c>true</c> if description; otherwise <c>false</c> </returns>
        <Extension()>
        Public Function HasDescription(ByVal type As Type, ByVal description As String) As Boolean

            Dim result As Boolean = False
            For Each enumValue As System.Enum In System.Enum.GetValues(type)
                If String.Equals(description, enumValue.Description()) Then
                    result = True
                    Exit For
                End If
            Next enumValue
            Return result

        End Function

        ''' <summary> Select description. </summary>
        ''' <param name="type">         The <see cref="T:System.Enum"/>
        '''                             type. </param>
        ''' <param name="defaultValue"> An enum constant representing the default value option. </param>
        ''' <param name="description">   The description. </param>
        ''' <returns> A System.Enum. </returns>
        <Extension()>
        Public Function SelectDescription(ByVal type As Type, ByVal defaultValue As System.Enum, ByVal description As String) As System.Enum

            Dim result As System.Enum = defaultValue
            For Each enumValue As System.Enum In System.Enum.GetValues(type)
                If String.Equals(description, enumValue.Description()) Then
                    result = enumValue
                    Exit For
                End If
            Next enumValue
            Return result

        End Function

        ''' <summary> Returns the descriptions of an flags enumerated list masked by the specified value. </summary>
        ''' <param name="type"> The <see cref="T:System.Enum"/> type. </param>
        ''' <param name="mask"> Allows returning only selected items. </param>
        ''' <returns> A list of strings. </returns>
        <Extension()>
        Public Function Descriptions(ByVal type As Type, ByVal mask As Integer) As IEnumerable(Of String)

            Dim values As New System.Collections.Generic.List(Of String)
            For Each value As System.Enum In System.Enum.GetValues(type)
                Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
                If (enumValue And mask) <> 0 Then
                    values.Add(value.Description())
                End If
            Next value
            Return values

        End Function

#End Region

#Region " NAMES "

        ''' <summary> Gets a Key Value Pair Name item. </summary>
        ''' <param name="enumConstant"> The <see cref="T:System.Enum"/> constant. </param>
        ''' <returns> A list of. </returns>
        <Extension()>
        Public Function ValueNamePair(ByVal enumConstant As System.Enum) As System.Collections.Generic.KeyValuePair(Of System.Enum, String)
            If enumConstant Is Nothing Then Throw New ArgumentNullException(NameOf(enumConstant))
            Return New System.Collections.Generic.KeyValuePair(Of System.Enum, String)(enumConstant, enumConstant.ToString)
        End Function

        ''' <summary> Converts the <see cref="T:System.Enum"/> type to an <see cref="IEnumerable"/> compatible
        ''' object. </summary>
        ''' <param name="type"> The <see cref="T:System.Enum"/> type. </param>
        ''' <returns> An <see cref="IEnumerable"/> containing the enumerated type value (key) and Name (value) pairs. </returns>
        ''' <example> The first step is to add a Name attribute to your Enum.
        ''' <code>
        '''     Public Enum SimpleEnum2
        '''       [System.ComponentModel.Name("Today")] Today
        '''       [System.ComponentModel.Name("Last 7 days")] Last7
        '''       [System.ComponentModel.Name("Last 14 days")] Last14
        '''       [System.ComponentModel.Name("Last 30 days")] Last30
        '''       [System.ComponentModel.Name("All")] All
        '''     End Enum
        '''     Dim combo As ComboBox = New ComboBox()
        '''     combo.DataSource = GetType(SimpleEnum2).ValueNamePairs()
        '''     combo.DisplayMember = "Value"
        '''     combo.ValueMember = "Key"
        ''' </code>
        ''' This works with any control that supports data binding, including the
        ''' <see cref="ToolStripComboBox">tool strip combo box</see>,
        ''' although you will need to cast the
        ''' <see cref="ToolStripComboBox.Control">control property</see>
        ''' to a <see cref="ComboBox">Combo Box</see> to get to the DataSource property. In
        ''' that case, you will also want to perform the same cast when you are referencing the selected
        ''' value to work with it as your Enum type.</example>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")>
        <Extension()>
        Public Function ValueNamePairs(ByVal type As Type) As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))

            Dim keyValuePairs As New List(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))
            For Each value As System.Enum In System.Enum.GetValues(type)
                keyValuePairs.Add(value.ValueNamePair())
            Next value
            Return keyValuePairs

        End Function

        ''' <summary>
        ''' Converts the <see cref="T:System.Enum"/> type to an <see cref="IEnumerable"/> compatible
        ''' object.
        ''' </summary>
        ''' <param name="type"> Specifies the Enum type. </param>
        ''' <param name="mask"> Specifies a mask for selecting the items to include in the list. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process value (key) Name (value) pairs
        ''' in this collection.
        ''' </returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")>
        <Extension()>
        Public Function ValueNamePairs(ByVal type As Type, ByVal mask As Integer) As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))

            Dim keyValuePairs As New List(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))
            For Each value As System.Enum In System.Enum.GetValues(type)
                Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
                If (enumValue And mask) <> 0 Then
                    keyValuePairs.Add(value.ValueNamePair())
                End If
            Next value
            Return keyValuePairs

        End Function

        ''' <summary>
        ''' Converts the <see cref="T:System.Enum"/> type to an <see cref="IEnumerable"/> compatible
        ''' object.
        ''' </summary>
        ''' <param name="type">           Specifies the Enum type. </param>
        ''' <param name="excludedValues"> Specifies list of excluded values. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process value (key) Name (value)
        ''' pairs in this collection.
        ''' </returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")>
        <Extension()>
        Public Function ValueNamePairs(ByVal type As Type, ByVal excludedValues As IEnumerable(Of Integer)) As IEnumerable(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))

            Dim keyValuePairs As New List(Of System.Collections.Generic.KeyValuePair(Of System.Enum, String))
            For Each value As System.Enum In System.Enum.GetValues(type)
                Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
                If Not excludedValues.Contains(enumValue) Then
                    keyValuePairs.Add(value.ValueNamePair())
                End If
            Next value
            Return keyValuePairs

        End Function

        ''' <summary>
        ''' Returns the Names of an flags enumerated list masked by the specified value.
        ''' </summary>
        ''' <param name="type"> The <see cref="T:System.Enum"/> type. </param>
        ''' <param name="mask"> Allows returning only selected items. </param>
        ''' <returns> A list of strings. </returns>
        <Extension()>
        Public Function Names(ByVal type As Type, ByVal mask As Integer) As IEnumerable(Of String)

            Dim values As New System.Collections.Generic.List(Of String)
            For Each value As System.Enum In System.Enum.GetValues(type)
                Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
                If (enumValue And mask) <> 0 Then
                    values.Add(System.Enum.GetName(type, value))
                End If
            Next value
            Return values

        End Function

#End Region

#Region " VALUES "

        ''' <summary> Enumerates values for the Enum type of the supplied Enum constant. </summary>
        ''' <param name="enumConstant"> An enum constant representing the enumeration option. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process values in this collection.
        ''' </returns>
        <Extension()>
        Public Function Values(ByVal enumConstant As System.Enum) As IEnumerable(Of Integer)
            If enumConstant Is Nothing Then Throw New ArgumentNullException(NameOf(enumConstant))
            Return enumConstant.GetType.Values
        End Function

        ''' <summary> Enumerates values in this collection. </summary>
        ''' <param name="type"> The <see cref="T:System.Enum"/> type. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process values in this collection.
        ''' </returns>
        <Extension()>
        Public Function Values(ByVal type As Type) As IEnumerable(Of Integer)
            Dim _values As New System.Collections.Generic.List(Of Integer)
            For Each value As System.Enum In System.Enum.GetValues(type)
                _values.Add(Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture))
            Next value
            Return _values
        End Function

        ''' <summary> Enumerates values for the Enumeration of the Enum constant. </summary>
        ''' <param name="enumConstant"> An enum constant representing the enumeration option. </param>
        ''' <param name="mask">        Specifies a mask for selecting the items to include in the list. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process values in this collection.
        ''' </returns>
        Public Function Values(ByVal enumConstant As System.Enum, ByVal mask As Integer) As IEnumerable(Of Integer)
            If enumConstant Is Nothing Then Throw New ArgumentNullException(NameOf(enumConstant))
            Return enumConstant.GetType.Values(mask)
        End Function

        ''' <summary> Enumerates values in this collection. </summary>
        ''' <param name="type"> The <see cref="T:System.Enum"/> type. </param>
        ''' <param name="mask"> Specifies a mask for selecting the items to include in the list. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process values in this collection.
        ''' </returns>
        <Extension()>
        Public Function Values(ByVal type As Type, ByVal mask As Integer) As IEnumerable(Of Integer)
            Dim _values As New System.Collections.Generic.List(Of Integer)
            For Each value As System.Enum In System.Enum.GetValues(type)
                Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
                If (enumValue And mask) <> 0 Then
                    _values.Add(enumValue)
                End If
            Next value
            Return _values
        End Function

#End Region

#Region " DELIMITED DESCRIPTION "

        ''' <summary> The start delimiter. </summary>
        Private Const startDelimiter As Char = "("c

        ''' <summary> The end delimiter. </summary>
        Private Const endDelimiter As Char = ")"c

        ''' <summary> The embedded value format. </summary>
        Private Const embeddedValueFormat As String = startDelimiter & "{0}" & endDelimiter

        ''' <summary> Builds a delimited value. This helps parsing enumerated values that include delimited
        ''' value strings in their descriptions. </summary>
        ''' <param name="value"> The value. </param>
        <Extension()>
        Public Function BuildDelimitedValue(ByVal value As String) As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, embeddedValueFormat, value)
        End Function

        ''' <summary> Extracts a delimited value. </summary>
        ''' <param name="value">          The value. </param>
        ''' <param name="startDelimiter"> The start delimiter. </param>
        ''' <param name="endDelimiter">   The end delimiter. </param>
        ''' <returns> The extracted between. </returns>
        <Extension()>
        Public Function ExtractBetween(ByVal value As String, ByVal startDelimiter As Char, ByVal endDelimiter As Char) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return String.Empty
            Else
                Dim startingIndex As Integer = value.IndexOf(startDelimiter)
                Dim endingIndex As Integer = value.LastIndexOf(endDelimiter)
                If startingIndex > 0 AndAlso endingIndex > startingIndex Then
                    Return value.Substring(startingIndex + 1, endingIndex - startingIndex - 1)
                Else
                    Return value
                End If
            End If
        End Function

        ''' <summary> Extracts a delimited value from value. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> The extracted between. </returns>
        <Extension()>
        Public Function ExtractBetween(ByVal value As String) As String
            Return Methods.ExtractBetween(value, startDelimiter, endDelimiter)
        End Function

        ''' <summary>
        ''' Extracts a delimited value from the description of the relevant enumerated value.
        ''' </summary>
        ''' <param name="enumConstant">   The <see cref="T:System.Enum"/> constant. </param>
        ''' <param name="startDelimiter"> The start delimiter. </param>
        ''' <param name="endDelimiter">   The end delimiter. </param>
        ''' <returns> The extracted between. </returns>
        <Extension()>
        Public Function ExtractBetween(ByVal enumConstant As System.Enum, ByVal startDelimiter As Char, ByVal endDelimiter As Char) As String
            If enumConstant Is Nothing Then
                Return String.Empty
            Else
                Return Methods.ExtractBetween(enumConstant.Description, startDelimiter, endDelimiter)
            End If
        End Function

        ''' <summary> Extracts a delimited value from the description of the relevant enumerated value. </summary>
        ''' <param name="enumConstant"> The <see cref="T:System.Enum"/> constant. </param>
        ''' <returns> The extracted between. </returns>
        <Extension()>
        Public Function ExtractBetween(ByVal enumConstant As System.Enum) As String
            If enumConstant Is Nothing Then
                Return String.Empty
            Else
                Return Methods.ExtractBetween(enumConstant, startDelimiter, endDelimiter)
            End If
        End Function

#End Region

#Region " WEBDEV_HB. UNDER LICENSE FROM "

        ''' <summary> Returns true if the specified bits are set. </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="enumConstant">  The <see cref="T:System.Enum"/> constant.</param>
        ''' <param name="value"> Specifies the bit values which to compare.</param>
        ''' <history date="06/05/2010" by="David" revision="1.2.3807.x">
        ''' Returns true if equals (IS).
        ''' </history>
        <System.Runtime.CompilerServices.Extension()>
        Public Function IsSet(Of T)(ByVal enumConstant As System.Enum, ByVal value As T) As Boolean
            Return Methods.Has(enumConstant, value)
        End Function

        ''' <summary> Returns true if the enumerated flag has the specified bits. </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="enumConstant">  The <see cref="T:System.Enum"/> constant.</param>
        ''' <param name="value"> Specifies the bit values which to compare.</param>
        ''' <history date="07/07/2009" by="David" revision="1.2.3475.x">
        ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx
        ''' Under The Creative Commons Attribution-ShareAlike 2.5 License
        ''' </history>
        ''' <history date="06/05/2010" by="David" revision="1.2.3807.x">
        ''' Returns true if equals (IS).
        ''' </history>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Has(Of T)(ByVal enumConstant As System.Enum, ByVal value As T) As Boolean
            Try
                Dim oldValue As Integer = ConvertToInteger(Of T)(enumConstant)
                Dim bitsValue As Integer = convertToInteger(Of T)(value)
                Return (oldValue = bitsValue) OrElse ((oldValue And bitsValue) = bitsValue)
            Catch
                Return False
            End Try
        End Function

        ''' <summary>  Returns true if value is only the provided type. </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="enumConstant">  The <see cref="T:System.Enum"/> constant.</param>
        ''' <param name="value"> Specifies the bit values which to compare.</param>
        ''' <history date="07/07/2009" by="David" revision="1.2.3475.x">
        ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx
        ''' Under The Creative Commons Attribution-ShareAlike 2.5 License
        ''' </history>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <System.Runtime.CompilerServices.Extension()>
        Public Function [Is](Of T)(ByVal enumConstant As System.Enum, ByVal value As T) As Boolean
            Try
                Dim oldValue As Integer = Methods.ConvertToInteger(Of T)(enumConstant)
                Dim bitsValue As Integer = Methods.ConvertToInteger(Of T)(value)
                Return oldValue = bitsValue
            Catch
                Return False
            End Try
        End Function

        ''' <summary> Set the specified bits. </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="enumConstant">  The <see cref="T:System.Enum"/> constant.</param>
        ''' <param name="value"> Specifies the bit values which to add.</param>
        <System.Runtime.CompilerServices.Extension()>
        Public Function [Set](Of T)(ByVal enumConstant As System.Enum, ByVal value As T) As T
            Return Methods.Add(enumConstant, value)
        End Function

        ''' <summary> Appends a value. </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="enumConstant">  The <see cref="T:System.Enum"/> constant.</param>
        ''' <param name="value"> Specifies the bit values which to add.</param>
        ''' <history date="07/07/2009" by="David" revision="1.2.3475.x">
        ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx
        ''' Under The Creative Commons Attribution-ShareAlike 2.5 License
        ''' </history>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Add(Of T)(ByVal enumConstant As System.Enum, ByVal value As T) As T
            Try
                Dim oldValue As Integer = Methods.ConvertToInteger(Of T)(enumConstant)
                Dim bitsValue As Integer = Methods.ConvertToInteger(Of T)(value)
                Dim newValue As Integer = oldValue Or bitsValue
                Return CType(CObj(newValue), T)
            Catch ex As Exception
                Throw New ArgumentException($"Could not append value from enumerated type '{GetType(T).Name}'.", ex)
            End Try
        End Function

        ''' <summary> Clears the specified bits. </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="enumConstant">  The <see cref="T:System.Enum"/> constant.</param>
        ''' <param name="value"> Specifies the bit values which to remove.</param>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Clear(Of T)(ByVal enumConstant As System.Enum, ByVal value As T) As T
            Return Methods.Remove(enumConstant, value)
        End Function

        ''' <summary> Completely removes the value. </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="enumConstant">  The <see cref="T:System.Enum"/> constant.</param>
        ''' <param name="value"> Specifies the bit values which to remove.</param>
        ''' <history date="07/07/2009" by="David" revision="1.2.3475.x">
        ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx
        ''' Under The Creative Commons Attribution-ShareAlike 2.5 License
        ''' </history>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Remove(Of T)(ByVal enumConstant As System.Enum, ByVal value As T) As T
            Try
                Dim oldValue As Integer = Methods.ConvertToInteger(Of T)(enumConstant)
                Dim bitsValue As Integer = Methods.ConvertToInteger(Of T)(value)
                Dim newValue As Integer = oldValue And (Not bitsValue)
                Return CType(CObj(newValue), T)
            Catch ex As Exception
                Throw New ArgumentException($"Could not remove value from enumerated type '{GetType(T).Name}'.", ex)
            End Try
        End Function

        ''' <summary> Converts a value to an integer. </summary>
        ''' <param name="value"> Specifies the bit values which to compare. </param>
        ''' <returns> The given data converted to an integer. </returns>
        Private Function ConvertToInteger(Of T)(ByVal value As T) As Integer
            Return CInt(CObj(value))
        End Function

        ''' <summary> Converts a type to an integer. </summary>
        ''' <param name="enumConstant"> The <see cref="T:System.Enum"/> constant. </param>
        ''' <returns> The given data converted to an integer. </returns>
        Private Function ConvertToInteger(Of T)(ByVal enumConstant As System.Enum) As Integer
            Return CInt(CObj(enumConstant))
        End Function

#End Region

#Region " PARSE "

        ''' <summary> Converts a value to an enum. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> Value as a T. </returns>
        <Extension()>
        Public Function ToEnum(Of T)(ByVal value As String) As T
            Return CType(System.Enum.Parse(GetType(T), value), T)
        End Function

        ''' <summary> Converts a value to an enum. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> Value as a T. </returns>
        <Extension()>
        Public Function ToEnum(Of T)(ByVal value As Integer) As T
            Return CType(System.Enum.Parse(GetType(T), value.ToString, True), T)
        End Function

#End Region

    End Module
End Namespace

﻿Imports System.Windows.Forms
Imports isr.Core.Pith.BindingExtensions
Imports isr.Core.Pith.ExceptionExtensions
''' <summary> A resource connector base. </summary>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/11/2018" by="David" revision=""> Created. </history>
<System.ComponentModel.DefaultEvent("Connect")>
Public MustInherit Class ResourceConnectorViewModel
    Inherits ViewModelTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    Protected Sub New()
        MyBase.New
        Me._New()
    End Sub

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <param name="talker"> The talker. </param>
    Protected Sub New(ByVal talker As ITraceMessageTalker)
        MyBase.New(talker)
        Me._New()
    End Sub

    Private Sub _New()
        Me._ResourceNames = New System.ComponentModel.BindingList(Of String)
        Me._ConnectedImage = My.Resources.Connected_22x22
        Me._DisconnectedImage = My.Resources.Disconnected_22x22
        Me._ClearImage = My.Resources.Clear_22x22
        Me._SearchImage = My.Resources.Find_22x22
    End Sub

#End Region

#Region " CLEARABLE "

    Private _Clearable As Boolean
    ''' <summary> Gets or sets the value indicating if the clear button is visible and can be enabled.
    ''' An item can be cleared only if it is connected. </summary>
    ''' <value> The clearable. </value>
    Public Property Clearable() As Boolean
        Get
            Return Me._Clearable
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Clearable.Equals(value) Then
                Me._Clearable = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    Private _ClearToolTip As String

    ''' <summary> Gets or sets the Clear tool tip. </summary>
    ''' <value> The Clear tool tip. </value>
    Public Property ClearToolTip() As String
        Get
            Return Me._ClearToolTip
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, Me.ClearToolTip) Then
                Me._ClearToolTip = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    Private _ClearImage As System.Drawing.Image

    ''' <summary> Gets or sets the Clear Image. </summary>
    ''' <value> The Clear tool tip. </value>
    Public Property ClearImage() As System.Drawing.Image
        Get
            Return Me._ClearImage
        End Get
        Set(ByVal value As System.Drawing.Image)
            If Not System.Drawing.Image.Equals(value, Me.ClearImage) Then
                Me._ClearImage = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " CONNECTABLE "

    Private _Connectable As Boolean
    ''' <summary> Gets or sets the value indicating if the connect button is visible and can be
    ''' enabled. An item can be connected only if it is selected. </summary>
    ''' <value> The connectable. </value>
    Public Property Connectable() As Boolean
        Get
            Return Me._Connectable
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Connectable.Equals(value) Then
                Me._Connectable = value
                Me.SafePostPropertyChanged()
                Me.SafePostPropertyChanged(NameOf(ResourceConnectorViewModel.ConnectEnabled))
            End If
        End Set
    End Property

    ''' <summary> Gets Connect Enabled state. </summary>
    ''' <value> The connect enabled. </value>
    Public ReadOnly Property ConnectEnabled() As Boolean
        Get
            Return Me.Connectable AndAlso Not String.IsNullOrWhiteSpace(Me.ValidatedResourceName)
        End Get
    End Property

    Private _ConnectToolTip As String
    ''' <summary> Gets or sets the connect tool tip. </summary>
    ''' <value> The connect tool tip. </value>
    Public Property ConnectToolTip() As String
        Get
            Return Me._ConnectToolTip
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, Me.ConnectToolTip) Then
                Me._ConnectToolTip = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " CONNECTABLE "

    Private _Searchable As Boolean
    ''' <summary> Gets or sets the condition determining if the control can be searchable. The elements
    ''' can be searched only if not connected. </summary>
    ''' <value> The searchable. </value>
    Public Property Searchable() As Boolean
        Get
            Return Me._Searchable
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Searchable.Equals(value) Then
                Me._Searchable = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    Private _SearchToolTip As String

    ''' <summary> Gets or sets the Search tool tip. </summary>
    ''' <value> The Find tool tip. </value>
    Public Property SearchToolTip() As String
        Get
            Return Me._SearchToolTip
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, Me.SearchToolTip) Then
                Me._SearchToolTip = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    Private _SearchImage As System.Drawing.Image

    ''' <summary> Gets or sets the Search Image. </summary>
    ''' <value> The Search tool tip. </value>
    Public Property SearchImage() As System.Drawing.Image
        Get
            Return Me._SearchImage
        End Get
        Set(ByVal value As System.Drawing.Image)
            If Not System.Drawing.Image.Equals(value, Me.SearchImage) Then
                Me._SearchImage = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " CONNECT FIELDS "

    ''' <summary> Gets or sets the connected image. </summary>
    ''' <value> The connected image. </value>
    Public Property ConnectedImage As System.Drawing.Image

    ''' <summary> Gets or sets the disconnected image. </summary>
    ''' <value> The disconnected image. </value>
    Public Property DisconnectedImage As System.Drawing.Image

    ''' <summary> Gets or sets the connect image. </summary>
    ''' <value> The connect image. </value>
    Public ReadOnly Property ConnectImage As System.Drawing.Image
        Get
            Return If(Me.IsConnected, Me.ConnectedImage, Me.DisconnectedImage)
        End Get
    End Property

    Private _IsConnected As Boolean
    ''' <summary> Gets or sets the connected status and enables the clear button. </summary>
    ''' <value> The is connected. </value>
    Public Property IsConnected() As Boolean
        Get
            Return Me._IsConnected
        End Get
        Set(value As Boolean)
            If value <> Me.IsConnected Then
                Me._IsConnected = value
                Me.IsDisconnected = Not value
                Me.ConnectToolTip = $"Click to {If(value, "Disconnect", "Connect")}"
                Me.SafePostPropertyChanged()
                Me.SafePostPropertyChanged(NameOf(ResourceConnectorViewModel.IsDisconnected))
                Me.SafePostPropertyChanged(NameOf(ResourceConnectorViewModel.ConnectImage))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the is disconnected. </summary>
    ''' <value> The is disconnected. </value>
    Public Property IsDisconnected() As Boolean = True

#End Region

#Region " RESOURCE NAME "

    Private _CandidateResourceName As String

    ''' <summary> Gets or sets the name of the candidate resource. </summary>
    ''' <value> The name of the candidate resource. </value>
    Public Property CandidateResourceName() As String
        Get
            Return Me._CandidateResourceName
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            value = value.Trim
            If Not String.Equals(value, Me.CandidateResourceName, StringComparison.OrdinalIgnoreCase) Then
                Me._CandidateResourceName = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the resource found tool tip. </summary>
    ''' <value> The resource found tool tip. </value>
    Public Property ResourceFoundToolTip As String = "Resource found; Connect"

    ''' <summary> Gets or sets the resource not found tool tip. </summary>
    ''' <value> The resource not found tool tip. </value>
    Public Property ResourceNotFoundToolTip As String = "Resource not found"

    Private _ValidatedResourceName As String
    ''' <summary> Returns the validated resource name. </summary>
    ''' <value> The name of the validated resource. </value>
    Public Property ValidatedResourceName() As String
        Get
            Return Me._ValidatedResourceName
        End Get
        Set(value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            value = value.Trim
            If Not String.Equals(value, Me.ValidatedResourceName, StringComparison.OrdinalIgnoreCase) Then
                Me._ValidatedResourceName = value
                Me.SafeSendPropertyChanged()
                If String.IsNullOrWhiteSpace(Me.ValidatedResourceName) Then
                    Me.ConnectToolTip = Me.ResourceFoundToolTip
                Else
                    Me.ConnectToolTip = Me.ResourceNotFoundToolTip
                End If
                Me.SafeSendPropertyChanged(NameOf(ResourceConnectorViewModel.ConnectEnabled))
            End If
        End Set
    End Property

    ''' <summary> Queries resource exists. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public MustOverride Function QueryResourceExists(ByVal resourceName As String) As Boolean

#End Region

#Region " SELECT RESOURCE  "

    ''' <summary> Attempts to validate (e.g., check existence) the resource by name. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <param name="e">            Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public MustOverride Function TryValidateResource(ByVal resourceName As String, ByVal e As isr.Core.Pith.ActionEventArgs) As Boolean

    ''' <summary> Used to notify of start of the validation of resource name. </summary>
    ''' <param name="resourceName"> The name of the resource. </param>
    Protected Overridable Sub OnValidatingResourceName(ByVal resourceName As String, ByVal e As System.ComponentModel.CancelEventArgs)
        Me.PublishInfo($"Validating {resourceName};. ")
    End Sub

    ''' <summary> Used to notify of validation of resource name. </summary>
    ''' <param name="resourceName"> The name of the resource. </param>
    ''' <param name="e">            Action event information. </param>
    Protected Overridable Sub OnResourceNameValidated(ByVal resourceName As String, ByVal e As EventArgs)
        Me.PublishInfo($"{resourceName} validated;. ")
    End Sub

    ''' <summary> Used to notify of a failure validating resource name. </summary>
    ''' <param name="resourceName"> The name of the resource. </param>
    ''' <param name="e">            Action event information. </param>
    Protected Overridable Sub OnResourceNameValidationFailed(ByVal resourceName As String, ByVal e As Core.Pith.ActionEventArgs)
        Me.PublishInfo($"{resourceName} not found;. ")
    End Sub

    ''' <summary> Attempts to validate resource name from the given data. </summary>
    ''' <param name="resourceName"> The name of the resource. </param>
    ''' <param name="e">            Cancel details event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function TryValidateResourceName(ByVal resourceName As String, ByVal e As isr.Core.Pith.ActionEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = String.Empty
        Try
            activity = Me.PublishVerbose($"Selecting resource name; checking conditions")
            If String.IsNullOrWhiteSpace(resourceName) Then
                e.RegisterFailure($"{activity} canceled because name is empty")
            Else
                Dim args As New System.ComponentModel.CancelEventArgs
                activity = Me.PublishVerbose("Notifying 'Selecting' resource name")
                Me.OnValidatingResourceName(resourceName, args)
                If args.Cancel Then
                    activity = Me.PublishVerbose("'Selecting' canceled")
                Else
                    activity = Me.PublishVerbose("enumerating resource names")
                    Me.EnumerateResources()
                    activity = Me.PublishVerbose($"trying to select resource '{resourceName}'")
                    If Me.TryValidateResource(resourceName, e) Then
                        Me.OnResourceNameValidated(resourceName, EventArgs.Empty)
                    End If
                End If
            End If
        Catch ex As Exception
            e.RegisterError($"Exception {activity};. {ex.ToFullBlownString}")
        End Try
        If e.Failed Then
            Me.Publish(e)
            Me.OnResourceNameValidationFailed(resourceName, e)
        End If
        Return Not e.Failed
    End Function

#End Region

#Region " ENUMERATE RESOURCES "

    Private _HasResources As Boolean
    ''' <summary> Gets the sentinel indication if resources were enumerated. </summary>
    ''' <value> The has resources. </value>
    Public Property HasResources As Boolean
        Get
            Return _HasResources
        End Get
        Set(ByVal value As Boolean)
            If Not Me.HasResources.Equals(value) Then
                Me._HasResources = value
                ' using sync notification is required when using unit tests; otherwise,
                ' actions invoked by the event may not occur.
                Me.SafeSendPropertyChanged()
            End If
        End Set
    End Property


#Disable Warning IDE0044 ' Add readonly modifier
    Private _ResourceNames As System.ComponentModel.BindingList(Of String)
#Enable Warning IDE0044 ' Add readonly modifier

    ''' <summary> Gets or sets the connect tool tip. </summary>
    ''' <value> The connect tool tip. </value>
    Public ReadOnly Property ResourceNames() As System.ComponentModel.BindingList(Of String)
        Get
            Return Me._ResourceNames
        End Get
    End Property

    ''' <summary> Enumerate resource names. </summary>
    ''' <param name="names"> The names. </param>
    ''' <returns> A =<see cref="System.ComponentModel.BindingList(Of System.String)">binding list</see> </returns>
    Public Function EnumerateResources(ByVal names As IEnumerable(Of String)) As System.ComponentModel.BindingList(Of String)
        ' suspend binding while building the resource names
        Me._ResourceNames.RaiseListChangedEvents = False
        Me._ResourceNames.Clear()
        If names?.Any Then
            For Each name As String In names
                Me._ResourceNames.Add(name)
            Next
        End If
        ' resume and reset binding 
        Me._ResourceNames.RaiseListChangedEvents = True
        Me._ResourceNames.ResetBindings()
        Me.SafePostPropertyChanged(NameOf(ResourceConnectorViewModel.ResourceNames))
        Me.HasResources = Me.ResourceNames.Any
        Return Me.ResourceNames
    End Function

    ''' <summary> Enumerate resource names. </summary>
    ''' <returns> A list of <see cref="System.String"/>. </returns>
    Public MustOverride Function EnumerateResources() As System.ComponentModel.BindingList(Of String)

#End Region

#Region " RESOURCE TITLE "

    Private _DefaultConnectedTitleFormat As String
    ''' <summary> Gets or sets the default connected title format. </summary>
    ''' <value> The default connected title format. </value>
    Public Property DefaultConnectedTitleFormat As String
        Get
            Return Me._DefaultConnectedTitleFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.DefaultConnectedTitleFormat, value) Then
                Me._DefaultConnectedTitleFormat = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    Private _ConnectedTitleFormat As String

    ''' <summary> Gets or sets the connected title format. </summary>
    ''' <value> The open connected title format. </value>
    Public Property ConnectedTitleFormat As String
        Get
            Return Me._ConnectedTitleFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.ConnectedTitleFormat, value) Then
                Me._ConnectedTitleFormat = value
                Me.SafeSendPropertyChanged()
                Me.Title = Me.BuildTitle
            End If
        End Set
    End Property

    Private _DefaultDisconnectedTitleFormat As String
    ''' <summary> Gets or sets the default Disconnected title format. </summary>
    ''' <value> The default Disconnected title format. </value>
    Public Property DefaultDisconnectedTitleFormat As String
        Get
            Return Me._DefaultDisconnectedTitleFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.DefaultDisconnectedTitleFormat, value) Then
                Me._DefaultDisconnectedTitleFormat = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    Private _DisconnectedTitleFormat As String

    ''' <summary> Gets or sets the Disconnected title format. </summary>
    ''' <value> The open Disconnected title format. </value>
    Public Property DisconnectedTitleFormat As String
        Get
            Return Me._DisconnectedTitleFormat
        End Get
        Set(value As String)
            If Not String.Equals(Me.DisconnectedTitleFormat, value) Then
                Me._DisconnectedTitleFormat = value
                Me.SafeSendPropertyChanged()
                Me.Title = Me.BuildTitle
            End If
        End Set
    End Property

    Private _ResourceTitle As String
    ''' <summary> Gets or sets the Resource title. </summary>
    ''' <value> The resource title. </value>
    Public Property ResourceTitle As String
        Get
            Return Me._ResourceTitle
        End Get
        Set(value As String)
            If Not String.Equals(Me.ResourceTitle, value) Then
                Me._ResourceTitle = value
                Me.SafeSendPropertyChanged()
                Me.Title = Me.BuildTitle
            End If
        End Set
    End Property

    Private _DefaultTitle As String
    ''' <summary> Gets or sets the default resource title. </summary>
    ''' <value> The default resource title. </value>
    Public Property DefaultTitle As String
        Get
            Return Me._DefaultTitle
        End Get
        Set(value As String)
            If Not String.Equals(Me.DefaultTitle, value) Then
                Me._DefaultTitle = value
                Me.SafeSendPropertyChanged()
                Me.Title = Me.BuildTitle
            End If
        End Set
    End Property

    Private _Title As String
    ''' <summary> Gets or sets the Title. </summary>
    ''' <value> The Title. </value>
    Public Property Title As String
        Get
            Return Me._Title
        End Get
        Set(value As String)
            If Not String.Equals(Me.Title, value) Then
                Me._Title = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Builds the title. </summary>
    ''' <returns> A String. </returns>
    Public Function BuildTitle() As String
        Dim result As String = String.Empty
        If Me.IsConnected Then
            If Not (String.IsNullOrWhiteSpace(Me.ConnectedTitleFormat) OrElse String.IsNullOrWhiteSpace(Me.ResourceTitle)) Then
                result = String.Format(Me.ConnectedTitleFormat, Me.ResourceTitle, Me.ValidatedResourceName)
            End If
        ElseIf Not (String.IsNullOrWhiteSpace(Me.DisconnectedTitleFormat) OrElse String.IsNullOrWhiteSpace(Me.ResourceTitle)) Then
            result = String.Format(Me.DisconnectedTitleFormat, Me.ResourceTitle)
        End If
        Return result
    End Function

#End Region

#Region " COMMENCT / DISCONNECT "

    ''' <summary> Gets or sets the connection changing. </summary>
    ''' <value> The connection changing. </value>
    Protected Property ConnectionChanging As Boolean

    ''' <summary> Toggle connection. </summary>
    Public Sub ToggleConnection()
        If Me.ConnectionChanging Then Return
        If Me.IsConnected Then
            Me.OnDisconnect(New System.ComponentModel.CancelEventArgs)
        Else
            Me.OnConnect(New System.ComponentModel.CancelEventArgs)
        End If
    End Sub

#End Region

#Region " CONNECT IMPLEMENTATION "

    ''' <summary> Removes the connect event handlers. </summary>
    Protected Sub RemoveConnectEventHandlers()
        Me.ConnectEventHandlers?.RemoveAll()
    End Sub

#Disable Warning IDE0044 ' Add readonly modifier
    ''' <summary> The connect event handlers. </summary>
    Private ConnectEventHandlers As New isr.Core.Pith.EventHandlerContextCollection(Of System.ComponentModel.CancelEventArgs)
#Enable Warning IDE0044 ' Add readonly modifier

    ''' <summary> Event queue for all listeners interested in connect events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Connect As EventHandler(Of System.ComponentModel.CancelEventArgs)
        AddHandler(value As EventHandler(Of System.ComponentModel.CancelEventArgs))
            Me.ConnectEventHandlers.Add(New EventHandlerContext(Of System.ComponentModel.CancelEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.ComponentModel.CancelEventArgs))
            Me.ConnectEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.ComponentModel.CancelEventArgs)
            Me.ConnectEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary> Raises the Connect event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnConnect(ByVal e As System.ComponentModel.CancelEventArgs)
        Me.ConnectionChanging = True
        Me.ConnectEventHandlers.Send(Me, e)
        Me.ConnectionChanging = False
        If e Is Nothing OrElse Not e.Cancel Then Me.IsConnected = True
    End Sub

#End Region

#Region " DISCONNECT IMPLEMENTATION "

    ''' <summary> Removes the disconnect event handlers. </summary>
    Protected Sub RemoveDisconnectEventHandlers()
        Me.DisconnectEventHandlers?.RemoveAll()
    End Sub

#Disable Warning IDE0044 ' Add readonly modifier
    ''' <summary> The Disconnect event handlers. </summary>
    Private DisconnectEventHandlers As New isr.Core.Pith.EventHandlerContextCollection(Of System.ComponentModel.CancelEventArgs)
#Enable Warning IDE0044 ' Add readonly modifier

    ''' <summary> Event queue for all listeners interested in Disconnect events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Disconnect As EventHandler(Of System.ComponentModel.CancelEventArgs)
        AddHandler(value As EventHandler(Of System.ComponentModel.CancelEventArgs))
            Me.DisconnectEventHandlers.Add(New EventHandlerContext(Of System.ComponentModel.CancelEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.ComponentModel.CancelEventArgs))
            Me.DisconnectEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.ComponentModel.CancelEventArgs)
            Me.DisconnectEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary> Raises the Disconnect event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnDisconnect(ByVal e As System.ComponentModel.CancelEventArgs)
        Me.ConnectionChanging = True
        Me.DisconnectEventHandlers.Send(Me, e)
        Me.ConnectionChanging = False
        If e Is Nothing OrElse Not e.Cancel Then Me.IsDisconnected = True
    End Sub

#End Region

#Region " CLEAR "

    ''' <summary> Removes the clear event handlers. </summary>
    Protected Sub RemoveClearEventHandlers()
        Me.ClearEventHandlers?.RemoveAll()
    End Sub

#Disable Warning IDE0044 ' Add readonly modifier
    ''' <summary> The Clear event handlers. </summary>
    Private ClearEventHandlers As New isr.Core.Pith.EventHandlerContextCollection(Of EventArgs)
#Enable Warning IDE0044 ' Add readonly modifier

    ''' <summary> Event queue for all listeners interested in Clear events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Clear As EventHandler(Of EventArgs)
        AddHandler(value As EventHandler(Of EventArgs))
            Me.ClearEventHandlers.Add(New EventHandlerContext(Of EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of EventArgs))
            Me.ClearEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As EventArgs)
            Me.ClearEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary> Request find names. </summary>
    Public Sub RequestClear()
        Me.ClearEventHandlers.Send(Me, EventArgs.Empty)
    End Sub

#End Region

#Region " ASYNC TASK "

    ''' <summary> Gets or sets the default resource name selection timeout. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The default resource name selection timeout. </value>
    Public Shared Property DefaultResourceNameSelectionTimeout As TimeSpan = TimeSpan.FromSeconds(5)

    ''' <summary> Gets or sets the resource name selection task. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The resource name selection task. </value>
    Public ReadOnly Property ResourceNameSelectionTask As Threading.Tasks.Task

    ''' <summary> Await resource name selection. </summary>
    ''' <param name="timeout"> The timeout. </param>
    Public Sub AwaitResourceNameSelection(ByVal timeout As TimeSpan)
        Me._ResourceNameSelectionTask?.Wait(timeout)
    End Sub

    ''' <summary> Await resource name selection. </summary>
    Public Sub AwaitResourceNameSelection()
        Me.AwaitResourceNameSelection(ResourceConnectorViewModel.DefaultResourceNameSelectionTimeout)
    End Sub

    ''' <summary> Asynchronous select resource name. </summary>
    ''' <param name="resourceName"> The name of the resource. </param>
    Public Sub AsyncSelectResourceName(ByVal resourceName As String)
        Me._ResourceNameSelectionTask = Me.SelectResourceName(resourceName)
    End Sub

    ''' <summary> Select resource names. </summary>
    ''' <param name="resourceName"> The name of the resource. </param>
    ''' <returns> A Task. </returns>
    Public Async Function SelectResourceName(ByVal resourceName As String) As Threading.Tasks.Task(Of isr.Core.Pith.ActionEventArgs)
        Dim e As New isr.Core.Pith.ActionEventArgs()
        Await Threading.Tasks.Task.Run(Sub() Me.TryValidateResourceName(resourceName, e))
        Return e
    End Function

#End Region

End Class

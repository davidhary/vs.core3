﻿Imports isr.Core.Pith.ExceptionExtensions
''' <summary> A resource selector view model base class </summary>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/11/2018" by="David" revision=""> Created. </history>
Public MustInherit Class SelectorViewModel
    Inherits ViewModelTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    Protected Sub New()
        MyBase.New
        Me._New()
    End Sub

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <param name="talker"> The talker. </param>
    Protected Sub New(ByVal talker As ITraceMessageTalker)
        MyBase.New(talker)
        Me._New()
    End Sub

    Private Sub _New()
        Me._ResourceNames = New System.ComponentModel.BindingList(Of String)
        Me._SearchImage = My.Resources.Find_22x22
        Me._IsOpen = False
        Me._Searchable = True
    End Sub

#End Region

#Region " SEARCHABLE "

    Private _Searchable As Boolean
    ''' <summary> Gets or sets the condition determining if the control can be searchable. The elements
    ''' can be searched only if not open </summary>
    ''' <value> The searchable. </value>
    Public Property Searchable() As Boolean
        Get
            Return Me._Searchable
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Searchable.Equals(value) Then
                Me._Searchable = value
                Me.SafePostPropertyChanged()
                Me.SafePostPropertyChanged(NameOf(SelectorViewModel.SearchEnabled))
            End If
        End Set
    End Property

    ''' <summary> Gets search enabled state </summary>
    ''' <value> The search enabled state </value>
    Public ReadOnly Property SearchEnabled() As Boolean
        Get
            Return Me.Searchable AndAlso Not Me.IsOpen
        End Get
    End Property

    Private _IsOpen As Boolean

    ''' <summary> Gets or sets the open status. </summary>
    ''' <value> The open status. </value>
    ''' <remarks> the search should be disabled when the resource is open </remarks>
    Public Property IsOpen() As Boolean
        Get
            Return Me._IsOpen
        End Get
        Set(value As Boolean)
            If Me.IsOpen <> value Then
                Me._IsOpen = value
                Me.SafePostPropertyChanged()
                Me.SafePostPropertyChanged(NameOf(SelectorViewModel.SearchEnabled))
            End If
        End Set
    End Property

    Private _SearchToolTip As String

    ''' <summary> Gets or sets the Search tool tip. </summary>
    ''' <value> The Search tool tip. </value>
    Public Property SearchToolTip() As String
        Get
            Return Me._SearchToolTip
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, Me.SearchToolTip) Then
                Me._SearchToolTip = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    Private _SearchImage As System.Drawing.Image

    ''' <summary> Gets or sets the Search Image. </summary>
    ''' <value> The Search tool tip. </value>
    Public Property SearchImage() As System.Drawing.Image
        Get
            Return Me._SearchImage
        End Get
        Set(ByVal value As System.Drawing.Image)
            If Not System.Drawing.Image.Equals(value, Me.SearchImage) Then
                Me._SearchImage = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " RESOURCE NAME "

    Private _CandidateResourceName As String

    ''' <summary> Gets or sets the name of the candidate resource. </summary>
    ''' <value> The name of the candidate resource. </value>
    Public Property CandidateResourceName() As String
        Get
            Return Me._CandidateResourceName
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            value = value.Trim
            If Not String.Equals(value, Me.CandidateResourceName, StringComparison.OrdinalIgnoreCase) Then
                Me._CandidateResourceName = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the resource found tool tip. </summary>
    ''' <value> The resource found tool tip. </value>
    Public Property ResourceFoundToolTip As String = "Resource found"

    ''' <summary> Gets or sets the resource not found tool tip. </summary>
    ''' <value> The resource not found tool tip. </value>
    Public Property ResourceNotFoundToolTip As String = "Resource not found"

    ''' <summary> Gets existence status of the resource </summary>
    ''' <value> The resource name exists. </value>
    Public ReadOnly Property SelectedResourceExists As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.ValidatedResourceName)
        End Get
    End Property

    Private _ValidatedResourceName As String
    ''' <summary> Returns the validated resource name. </summary>
    ''' <value> The name of the validated resource. </value>
    Public Property ValidatedResourceName() As String
        Get
            Return Me._ValidatedResourceName
        End Get
        Set(value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            value = value.Trim
            If Not String.Equals(value, Me.ValidatedResourceName, StringComparison.OrdinalIgnoreCase) Then
                Me._ValidatedResourceName = value
                Me.SafeSendPropertyChanged()
                If String.IsNullOrWhiteSpace(Me.ValidatedResourceName) Then
                    Me.SearchToolTip = Me.ResourceFoundToolTip
                Else
                    Me.SearchToolTip = Me.ResourceNotFoundToolTip
                End If
            End If
        End Set
    End Property

    ''' <summary> Queries resource exists. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public MustOverride Function QueryResourceExists(ByVal resourceName As String) As Boolean

#End Region

#Region " VALIDATE RESOURCE  "

    ''' <summary> Attempts to validate (e.g., check existence) the resource by name. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <param name="e">            Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public MustOverride Function TryValidateResource(ByVal resourceName As String, ByVal e As isr.Core.Pith.ActionEventArgs) As Boolean

    ''' <summary> Used to notify of start of the validation of resource name. </summary>
    ''' <param name="resourceName"> The name of the resource. </param>
    Protected Overridable Sub OnValidatingResourceName(ByVal resourceName As String, ByVal e As System.ComponentModel.CancelEventArgs)
        Me.PublishInfo($"Validating {resourceName};. ")
    End Sub

    ''' <summary> Used to notify of validation of resource name. </summary>
    ''' <param name="resourceName"> The name of the resource. </param>
    ''' <param name="e">            Action event information. </param>
    Protected Overridable Sub OnResourceNameValidated(ByVal resourceName As String, ByVal e As EventArgs)
        Me.PublishInfo($"{resourceName} validated;. ")
    End Sub

    ''' <summary> Used to notify of a failure validating resource name. </summary>
    ''' <param name="resourceName"> The name of the resource. </param>
    ''' <param name="e">            Action event information. </param>
    Protected Overridable Sub OnResourceNameValidationFailed(ByVal resourceName As String, ByVal e As Core.Pith.ActionEventArgs)
        Me.PublishInfo($"{resourceName} not found;. ")
    End Sub

    ''' <summary> Attempts to validate resource name from the given data. </summary>
    ''' <param name="resourceName"> The name of the resource. </param>
    ''' <param name="e">            Cancel details event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function TryValidateResourceName(ByVal resourceName As String, ByVal e As isr.Core.Pith.ActionEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = String.Empty
        Try
            activity = Me.PublishVerbose($"Selecting resource name; checking conditions")
            If String.IsNullOrWhiteSpace(resourceName) Then
                e.RegisterFailure($"{activity} canceled because name is empty")
            Else
                Dim args As New System.ComponentModel.CancelEventArgs
                activity = Me.PublishVerbose("Notifying 'Selecting' resource name")
                Me.OnValidatingResourceName(resourceName, args)
                If args.Cancel Then
                    activity = Me.PublishVerbose("'Selecting' canceled")
                Else
                    activity = Me.PublishVerbose("enumerating resource names")
                    Me.EnumerateResources()
                    activity = Me.PublishVerbose($"trying to select resource '{resourceName}'")
                    If Me.TryValidateResource(resourceName, e) Then
                        Me.OnResourceNameValidated(resourceName, EventArgs.Empty)
                    End If
                End If
            End If
        Catch ex As Exception
            e.RegisterError($"Exception {activity};. {ex.ToFullBlownString}")
        End Try
        If e.Failed Then
            Me.Publish(e)
            Me.OnResourceNameValidationFailed(resourceName, e)
        End If
        Return Not e.Failed
    End Function

#End Region

#Region " ENUMERATE RESOURCES "

    Private _HasResources As Boolean
    ''' <summary> Gets the sentinel indication if resources were enumerated. </summary>
    ''' <value> The has resources. </value>
    Public Property HasResources As Boolean
        Get
            Return _HasResources
        End Get
        Set(ByVal value As Boolean)
            If Not Me.HasResources.Equals(value) Then
                Me._HasResources = value
                ' using sync notification is required when using unit tests; otherwise,
                ' actions invoked by the event may not occur.
                Me.SafeSendPropertyChanged()
            End If
        End Set
    End Property


#Disable Warning IDE0044 ' Add readonly modifier
    Private _ResourceNames As System.ComponentModel.BindingList(Of String)
#Enable Warning IDE0044 ' Add readonly modifier

    ''' <summary> Gets the resource names </summary>
    ''' <value> The the resource names </value>
    Public ReadOnly Property ResourceNames() As System.ComponentModel.BindingList(Of String)
        Get
            Return Me._ResourceNames
        End Get
    End Property

    ''' <summary> Enumerate resource names. </summary>
    ''' <param name="names"> The names. </param>
    ''' <returns> A =<see cref="System.ComponentModel.BindingList(Of System.String)">binding list</see> </returns>
    Public Function EnumerateResources(ByVal names As IEnumerable(Of String)) As System.ComponentModel.BindingList(Of String)
        ' suspend binding while building the resource names
        Me._ResourceNames.RaiseListChangedEvents = False
        Me._ResourceNames.Clear()
        If names?.Any Then
            For Each name As String In names
                Me._ResourceNames.Add(name)
            Next
        End If
        ' resume and reset binding 
        Me._ResourceNames.RaiseListChangedEvents = True
        Me._ResourceNames.ResetBindings()
        Me.SafePostPropertyChanged(NameOf(SelectorViewModel.ResourceNames))
        Me.HasResources = Me.ResourceNames.Any
        Return Me.ResourceNames
    End Function

    ''' <summary> Enumerate resource names. </summary>
    ''' <returns> A list of <see cref="System.String"/>. </returns>
    Public MustOverride Function EnumerateResources() As System.ComponentModel.BindingList(Of String)

#End Region

#Region " ASYNC TASK "

    ''' <summary> Gets or sets the default resource name selection timeout. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The default resource name selection timeout. </value>
    Public Shared Property DefaultResourceNameSelectionTimeout As TimeSpan = TimeSpan.FromSeconds(5)

    ''' <summary> Gets or sets the resource name selection task. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The resource name selection task. </value>
    Public ReadOnly Property ResourceNameSelectionTask As Threading.Tasks.Task

    ''' <summary> Await resource name selection. </summary>
    ''' <param name="timeout"> The timeout. </param>
    Public Sub AwaitResourceNameSelection(ByVal timeout As TimeSpan)
        Me._ResourceNameSelectionTask?.Wait(timeout)
    End Sub

    ''' <summary> Await resource name selection. </summary>
    Public Sub AwaitResourceNameSelection()
        Me.AwaitResourceNameSelection(SelectorViewModel.DefaultResourceNameSelectionTimeout)
    End Sub

    ''' <summary> Asynchronous select resource name. </summary>
    ''' <param name="resourceName"> The name of the resource. </param>
    Public Sub AsyncSelectResourceName(ByVal resourceName As String)
        Me._ResourceNameSelectionTask = Me.SelectResourceName(resourceName)
    End Sub

    ''' <summary> Select resource names. </summary>
    ''' <param name="resourceName"> The name of the resource. </param>
    ''' <returns> A Task. </returns>
    Public Async Function SelectResourceName(ByVal resourceName As String) As Threading.Tasks.Task(Of isr.Core.Pith.ActionEventArgs)
        Dim e As New isr.Core.Pith.ActionEventArgs()
        Await Threading.Tasks.Task.Run(Sub() Me.TryValidateResourceName(resourceName, e))
        Return e
    End Function

#End Region

End Class


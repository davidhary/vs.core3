﻿Imports System.ComponentModel
Imports isr.Core.Pith.ExceptionExtensions
Partial Public Class ViewModelTalkerBase
    Implements INotifyPropertyChanged

#Region " CUSTOM PROPERTY CHANGED EVENT IMPLEMENTATION "

    ''' <summary> Removes the property changed event handlers. </summary>
    Protected Sub RemovePropertyChangedEventHandlers()
        For i As Integer = Me._propertyChangedHandlers.Count - 1 To 0 Step -1
            Me._propertyChangedHandlers.RemoveAt(i)
        Next
    End Sub

    Private _propertyChangedHandlers As New List(Of PropertyChangedEventContext)

    ''' <summary> Event queue for all listeners interested in Custom events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

        AddHandler(ByVal value As PropertyChangedEventHandler)
            Me._propertyChangedHandlers.Add(New PropertyChangedEventContext(value))
        End AddHandler

        RemoveHandler(ByVal value As PropertyChangedEventHandler)
            For i As Integer = Me._propertyChangedHandlers.Count - 1 To 0 Step -1
                If Me._propertyChangedHandlers(i).Handler Is value Then
                    Me._propertyChangedHandlers.RemoveAt(i)
                    Return 'remove only the last one
                End If
            Next
        End RemoveHandler

        RaiseEvent(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            For Each item As PropertyChangedEventContext In Me._propertyChangedHandlers
                item.Post(sender, e)
            Next
        End RaiseEvent

    End Event

#End Region

End Class

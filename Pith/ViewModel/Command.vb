﻿Imports System.ComponentModel
Imports System.Windows.Input

''' <summary> Implements <see cref="System.Windows.Input.ICommand"/>. </summary>
''' <remarks>
''' About <see cref="System.Windows.Input.ICommand"/> implementation <para>
''' One can add one's own functionality relatively easily with attached behaviors (pure MVVM):
''' you can subscribe to certain View events (you can do it in the View code-behind, but that
''' would be dirty MVVM) to call certain methods of ViewModel (directly, via interface or using
''' reflection). ICommand was a bare minimum what WPF needs, namely it solves problem when menu
''' items/buttons become disabled e.g. for Copy/Paste scenario. Nothing more. In fact
''' ButtonBase.Command can be a simple Click event handler, you can right away call ViewModel
''' method there </para><para>
''' Both approaches are correct, they follow MVVM principles And works without ICommand, but As
''' you can see, neither Is As elegant As ICommand</para><para>
''' For additional references see:
''' https://stackoverflow.com/questions/1685088/binding-to-commands-in-winforms  </para><para>
''' https://stackoverflow.com/questions/42113388/what-is-the-reason-for-icommand-in-mvvm
''' </para><para>
''' iCommand serves these purposes:</para><para>
''' <list type="bullet"><item>
''' It wraps a method to an Object;</item><item>
''' It determines if the command is available so the UI component (typically a button Or menu
''' item) can reflect it;</item><item>
''' It notifies the UI components of changes in the availability of the command has changed, so
''' the UI can reflect it (e.g., a relevant UI component can be enabled).</item></list>
''' In the classic cut and paste example, without <see cref="System.Windows.Input.ICommand"/>
''' implementation, both the paste command and paste availability (can paste) have to be
''' addressed at the UI level, which, aside of breaking the DRY principle, adds complexity to the
''' code at the UI level. </para><para>
''' For example, In the classic cut and paste example, without <see cref="System.Windows.Input.ICommand"/>
''' implementation, both the paste command and paste availability (can paste) have to be
''' addressed at the UI level, which, aside of breaking the DRY principle, adds complexity to the
''' code at the UI level. </para><para>
''' On the other hand:</para><para>
''' <list type="bullet"><item>
''' While reducing complexity on the UI code behind, this adds complexity to the view-model;
''' </item><item>
''' This makes debugging the UI/Model interfaces hardware as the model is addressed via an action
''' call;</item><item>
''' Command Binding and command management is not fully implemented in Windows
''' Forms.</item></list>
''' </para>
''' </remarks>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/10/2018" by="David"> > Created. </history>
Public Class Command
    Implements ICommand

    ''' <summary> Gets or sets the action. </summary>
    ''' <value> The action. </value>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private ReadOnly Property Action As Action

    ''' <summary> Constructor. </summary>
    ''' <param name="action"> The action. </param>
    Sub New(action As Action)
        Me.Action = action
    End Sub

    ''' <summary>
    ''' Defines the method that determines whether the command can execute in its current state.
    ''' </summary>
    ''' <param name="parameter"> Data used by the command.  If the command does not require data to be
    '''                          passed, this object can be set to null. </param>
    ''' <returns> true if this command can be executed; otherwise, false. </returns>
    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    ''' <summary> Event queue for all listeners interested in CanExecuteChanged events. </summary>
    Public Event CanExecuteChanged(sender As Object, e As EventArgs) Implements ICommand.CanExecuteChanged

    ''' <summary> Defines the method to be called when the command is invoked. </summary>
    ''' <param name="parameter"> Data used by the command.  If the command does not require data to be
    '''                          passed, this object can be set to null. </param>
    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        Me._Action()
    End Sub
End Class

''' <summary> A predicated command. </summary>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/10/2018" by="David" revision=""> Created. </history>
Public Class PredicatedCommand
    Implements ICommand

    ''' <summary> Gets or sets the action. </summary>
    ''' <value> The action. </value>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private ReadOnly Property Action As Action

    ''' <summary> Gets or sets the predicate. </summary>
    ''' <value> The predicate. </value>
    Private ReadOnly Property Predicate As Predicate(Of Object)

    Sub New(action As Action, predicate As Predicate(Of Object))
        Me.Action = action
        Me.Predicate = predicate
    End Sub

    ''' <summary> Gets or sets the can execute sentinel. </summary>
    ''' <value> The can execute sentinel. </value>
    Private Property CanExecuteSentinel As Boolean

    Private Function RaiseCanExecuteChanged(ByVal parameter As Object) As Boolean
        If Me.CanExecuteSentinel <> Me.Predicate(parameter) Then
            Me.CanExecuteSentinel = Not Me.CanExecuteSentinel
            Dim evt As EventHandler = Me.CanExecuteChangedEvent
            evt?.Invoke(Me, EventArgs.Empty)
        End If
        Return Me.CanExecuteSentinel
    End Function

    ''' <summary>
    ''' Defines the method that determines whether the command can execute in its current state.
    ''' </summary>
    ''' <param name="parameter"> Data used by the command.  If the command does not require data to be
    '''                          passed, this object can be set to null. </param>
    ''' <returns> true if this command can be executed; otherwise, false. </returns>
    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return Me.RaiseCanExecuteChanged(parameter)
    End Function

    ''' <summary> Event queue for all listeners interested in CanExecuteChanged events. </summary>
    Public Event CanExecuteChanged(sender As Object, e As EventArgs) Implements ICommand.CanExecuteChanged

    ''' <summary> Defines the method to be called when the command is invoked. </summary>
    ''' <param name="parameter"> Data used by the command.  If the command does not require data to be
    '''                          passed, this object can be set to null. </param>
    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        If Me.CanExecute(parameter) Then Me._Action()
    End Sub
End Class

''' <summary> Implements <see cref="System.Windows.Input.ICommand"/>. </summary>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/10/2018" by="David" revision=""> Created. </history>
Public Class GenericCommand(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})
    Implements ICommand

    ''' <summary> Gets or sets the action. </summary>
    ''' <value> The action. </value>
    Private ReadOnly Property Action As Action(Of T)

    ''' <summary> Gets or sets the predicate. </summary>
    ''' <value> The predicate. </value>
    Private ReadOnly Property Predicate As Predicate(Of T)

    Sub New(action As Action(Of T), predicate As Predicate(Of T))
        Me.Action = action
        Me.Predicate = predicate
    End Sub

    ''' <summary> Gets or sets the can execute sentinel. </summary>
    ''' <value> The can execute sentinel. </value>
    Private Property CanExecuteSentinel As Boolean

    Private Function RaiseCanExecuteChanged(ByVal parameter As T) As Boolean
        If Me.CanExecuteSentinel <> Me.Predicate(parameter) Then
            Me.CanExecuteSentinel = Not Me.CanExecuteSentinel
            Dim evt As EventHandler = Me.CanExecuteChangedEvent
            evt?.Invoke(Me, EventArgs.Empty)
        End If
        Return Me.CanExecuteSentinel
    End Function

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return Me.RaiseCanExecuteChanged(CType(parameter, T))
    End Function

    Public Event CanExecuteChanged(sender As Object, e As EventArgs) Implements ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        If Me.CanExecute(parameter) Then Me._Action(CType(parameter, T))
    End Sub

End Class


﻿Imports System.ComponentModel
Imports isr.Core.Pith.ExceptionExtensions
Partial Public Class ViewModelBase
    Implements INotifyPropertyChanged

#Region " CUSTOM PROPERTY CHANGED EVENT IMPLEMENTATION "

    ''' <summary> Removes the property changed event handlers. </summary>
    Protected Sub RemovePropertyChangedEventHandlers()
        For i As Integer = Me.PropertyChangedHandlers.Count - 1 To 0 Step -1
            Me.PropertyChangedHandlers.RemoveAt(i)
        Next
    End Sub

    Private PropertyChangedHandlers As New PropertyChangeEventContextCollection()

    ''' <summary> Event queue for all listeners interested in property changed events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

        AddHandler(ByVal value As PropertyChangedEventHandler)
            Me.PropertyChangedHandlers.Add(New PropertyChangedEventContext(value))
        End AddHandler

        RemoveHandler(ByVal value As PropertyChangedEventHandler)
            Me.PropertyChangedHandlers.RemoveValue(value)
            For i As Integer = Me.PropertyChangedHandlers.Count - 1 To 0 Step -1
                If Me.PropertyChangedHandlers(i).Handler Is value Then
                    Me.PropertyChangedHandlers.RemoveAt(i)
                    Return 'remove only the last one
                End If
            Next
        End RemoveHandler

        RaiseEvent(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            For Each item As PropertyChangedEventContext In Me.PropertyChangedHandlers
                item.Post(sender, e)
            Next
        End RaiseEvent

    End Event

#End Region

End Class

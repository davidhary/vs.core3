﻿Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Core.Pith.BindingExtensions
Partial Public Class ViewModelTalkerBase

#Region " BINDING MANAGEMENT "

    ''' <summary> Handles the binding complete event. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this <see cref="Binding"/> </param>
    ''' <param name="e">      Binding complete event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub HandleBindingCompleteEvent(ByVal sender As Binding, ByVal e As BindingCompleteEventArgs)
        Dim activity As String = String.Empty
        Dim binding As Binding = TryCast(sender, Binding)
        If binding Is Nothing OrElse e Is Nothing Then Return
        Try
            activity = "setting cancel state"
            e.Cancel = e.BindingCompleteState <> BindingCompleteState.Success
            activity = $"binding: {e.Binding.BindingMemberInfo.BindingField}:{e.BindingCompleteContext}:{e.Binding.BindableComponent}"
            If e.BindingCompleteState = BindingCompleteState.DataError Then
                activity = $"data error; {activity}"
                Me.PublishWarning($"{activity};. {e.ErrorText}")
            ElseIf e.BindingCompleteState = BindingCompleteState.Exception Then
                If Not String.IsNullOrWhiteSpace(e.ErrorText) Then
                    activity = $"{activity}; {e.ErrorText}"
                End If
                Me.PublishException(activity, e.Exception)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Handles the binding complete event. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                       <see cref="Control"/> </param>
    ''' <param name="e">      Binding complete event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub HandleBindingCompleteEvent(ByVal sender As Object, ByVal e As BindingCompleteEventArgs)
        Dim activity As String = String.Empty
        Dim binding As Binding = TryCast(sender, Binding)
        If binding Is Nothing OrElse e Is Nothing Then Return
        Try
            If e.BindingCompleteState <> BindingCompleteState.Success Then
                Me.HandleBindingCompleteEvent(binding, e)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Adds or removes binding from a <see cref="IBindableComponent">bindable componenet</see>
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="component"> The bindable component. </param>
    ''' <param name="add">       True to add; otherwise, remove binding. </param>
    ''' <param name="binding">   The binding. </param>
    Public Sub AddRemoveBinding(ByVal component As IBindableComponent, ByVal add As Boolean, ByVal binding As Binding)
        component.AddRemoveBinding(add, binding, AddressOf Me.HandleBindingCompleteEvent)
    End Sub

    ''' <summary> Adds binding to a <see cref="IBindableComponent">bindable componenet</see> </summary>
    ''' <param name="component">    The bindable component. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    ''' <param name="dataMember">   The data member. </param>
    Public Sub AddBinding(ByVal component As IBindableComponent, ByVal propertyName As String, ByVal dataMember As String)
        component.AddRemoveBinding(True, New Binding(propertyName, Me, dataMember), AddressOf HandleBindingCompleteEvent)
    End Sub

    ''' <summary>
    ''' removes binding from a <see cref="IBindableComponent">bindable componenet</see>
    ''' </summary>
    ''' <param name="component">    The bindable component. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    ''' <param name="dataMember">   The data member. </param>
    Public Sub RemoveBinding(ByVal component As IBindableComponent, ByVal propertyName As String, ByVal dataMember As String)
        component.AddRemoveBinding(False, New Binding(propertyName, Me, dataMember), AddressOf HandleBindingCompleteEvent)
    End Sub

#End Region

End Class


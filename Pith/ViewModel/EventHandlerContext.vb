﻿Imports System.ComponentModel
Imports System.Threading

''' <summary> A generic event handler context. </summary>
''' <remarks>
''' The <see cref="PropertyChangedEventContext"/> and the custom
''' <see cref="PropertyChangedEventHandler"/> make the PropertyChanged event fire on the
''' SynchronizationContext of the listening code. <para>
''' The PropertyChanged event declaration is modified to a custom event handler. </para><para>
''' As each handler may have a different context, the AddHandler block stores the handler being
''' passed in as well as the SynchronizationContext to be used later when raising the event.
''' .</para>
''' </remarks>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history Date="10/01/2009" by="Bill McCarthy"> https://visualstudiomagazine.com/Articles/2009/10/01/Threading-and-the-UI.aspx?Page=2. </history>
Public Class EventHandlerContext(Of T As Class)

    Public Sub New(ByVal handler As EventHandler(Of T))
        Me.Handler = handler
        Me.Context = SynchronizationContext.Current
    End Sub

    ''' <summary> Gets or sets the synchronization context. </summary>
    ''' <value> The context. </value>
    Private ReadOnly Property Context As SynchronizationContext

    ''' <summary> Gets or sets the handler. </summary>
    ''' <value> The handler. </value>
    Public ReadOnly Property Handler() As EventHandler(Of T)

    ''' <summary>
    ''' Asynchronously raises (Posts) the <see cref="EventHandler(Of T)"/> event. It does all the checking to see
    ''' if the SynchronizationContext is nothing or not, and invokes the delegate accordingly.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      The event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="e")>
    Public Sub Post(ByVal sender As Object, ByVal e As T)
        Static sendPostCallbackDelegate As SendOrPostCallback
        If Me.Context Is Nothing Then
            Me.Handler.Invoke(sender, e)
        Else
            If sendPostCallbackDelegate Is Nothing Then
                sendPostCallbackDelegate = New SendOrPostCallback(AddressOf Me.OnEvent)
            End If
            Me.Context.Post(sendPostCallbackDelegate, New EventInfo(sender, e))
        End If
    End Sub

    ''' <summary>
    ''' Synchronously raises (sends) the <see cref="EventHandler(Of T)"/> event. It does all the checking to see
    ''' if the SynchronizationContext is nothing or not, and invokes the delegate accordingly.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      The event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="e")>
    Public Sub Send(ByVal sender As Object, ByVal e As T)
        Static sendPostCallbackDelegate As SendOrPostCallback
        If Me.Context Is Nothing Then
            Me.Handler.Invoke(sender, e)
        Else
            If sendPostCallbackDelegate Is Nothing Then
                sendPostCallbackDelegate = New SendOrPostCallback(AddressOf Me.OnEvent)
            End If
            Me.Context.Send(sendPostCallbackDelegate, New EventInfo(sender, e))
        End If
    End Sub

    ''' <summary> Executes the event handler post action. </summary>
    ''' <param name="eventInfo"> Information describing the event. </param>
    Private Sub OnEvent(ByVal eventInfo As Object)
        Dim info As EventInfo = CType(eventInfo, EventInfo)
        Me.Handler.Invoke(info.Sender, info.Arguments)
    End Sub

    Private Structure EventInfo
        Public ReadOnly Property Sender As Object
        Public ReadOnly Property Arguments As T
        Public Sub New(ByVal sender As Object, ByVal e As T)
            Me.Sender = sender
            Me.Arguments = e
        End Sub
    End Structure

End Class

''' <summary> A generic collection of <see cref="EventHandlerContext(Of T)"/> </summary>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/11/2018" by="David" revision=""> Created. </history>
Public Class EventHandlerContextCollection(Of T As Class)
    Inherits ObjectModel.Collection(Of EventHandlerContext(Of T))

    ''' <summary> Post this message. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      A T to process. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="e")>
    Public Sub Post(sender As Object, ByVal e As T)
        For Each item As EventHandlerContext(Of T) In Me
            item.Post(sender, e)
        Next
    End Sub

    ''' <summary> Send this message. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      A T to process. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="e")>
    Public Sub Send(sender As Object, ByVal e As T)
        For Each item As EventHandlerContext(Of T) In Me
            item.Send(sender, e)
        Next
    End Sub

    ''' <summary> Removes the value described by value. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub RemoveValue(value As EventHandler(Of T))
        For i As Integer = Me.Count - 1 To 0 Step -1
            If Me.Item(i).Handler Is value Then
                Me.RemoveAt(i)
                Return 'remove only the last one
            End If
        Next
    End Sub

    ''' <summary> Removes all. </summary>
    Public Sub RemoveAll()
        Do While Me.Count > 0
            Me.RemoveAt(0)
        Loop
    End Sub

End Class
﻿Imports System.ComponentModel
Imports isr.Core.Pith.ExceptionExtensions
Partial Public Class ViewModelTalkerBase
    Implements ITalker

#Region " CONSTRUCTION "

    ''' <summary> Constructor-Safe talker setter. </summary>
    ''' <param name="talker"> The talker. </param>
    Private Sub ConstructorSafeSetter(ByVal talker As ITraceMessageTalker)
        Me._Talker = talker
        If Me._Talker IsNot Nothing Then
            AddHandler Me._Talker.DateChanged, AddressOf Me.HandleTalkerDateChange
        End If
    End Sub

#End Region

#Region " ASSIGNMENTS  "

    Private _Talker As ITraceMessageTalker
    ''' <summary> Gets the trace message talker. </summary>
    ''' <value> The trace message talker. </value>
    Public Property Talker As ITraceMessageTalker
        Get
            Return Me._Talker
        End Get
        Private Set(value As ITraceMessageTalker)
            If Me._Talker IsNot Nothing Then
                RemoveHandler Me._Talker.DateChanged, AddressOf Me.HandleTalkerDateChange
                Me.RemoveListeners()
            End If
            Me.ConstructorSafeSetter(value)
        End Set
    End Property

    Private IsAssignedTalker As Boolean
    ''' <summary> Assigns a talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Overridable Sub AssignTalker(ByVal talker As ITraceMessageTalker)
        Me.IsAssignedTalker = talker IsNot Nothing
        If Me.IsAssignedTalker Then
            Me.Talker = talker
        Else
            Me.Talker = New TraceMessageTalker
        End If
        Me.IdentifyTalkers()
    End Sub

    ''' <summary> Handles the talker date change. </summary>
    ''' <param name="log">          The log. </param>
    ''' <param name="assemblyInfo"> Information describing my assembly. </param>
    Protected Overridable Sub HandleTalkerDateChange(ByVal log As MyLog, ByVal assemblyInfo As MyAssemblyInfo)
        If log Is Nothing Then Throw New ArgumentNullException(NameOf(log))
        If assemblyInfo Is Nothing Then Throw New ArgumentNullException(NameOf(assemblyInfo))
        Me.Talker.PublishOverride(TraceEventType.Information, My.MyLibrary.TraceEventId, $"Log at;. {log.FullLogFileName}")
        Me.Talker.PublishOverride(TraceEventType.Information, My.MyLibrary.TraceEventId, $"Local time {assemblyInfo.LocalUtcCaption}")
    End Sub

    ''' <summary> Handles the talker date change. </summary>
    Protected Overridable Sub HandleTalkerDateChange()
        Me.IdentifyTalkers()
    End Sub

    ''' <summary> Identifies talkers. </summary>
    Protected Overridable Sub IdentifyTalkers()
        My.MyLibrary.Identify(Me.Talker)
    End Sub

    ''' <summary> Handles the talker date change. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub HandleTalkerDateChange(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Me.HandleTalkerDateChange()
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

#Region " LISTENERS "

    ''' <summary>
    ''' Removes the private listeners. Removes all listeners if the talker was not assigned.
    ''' </summary>
    Public Overridable Sub RemoveListeners() Implements ITalker.RemoveListeners
        Me.RemovePrivateListeners()
        If Not Me.IsAssignedTalker Then Me.Talker.RemoveListeners()
    End Sub

    Private _PrivateListeners As List(Of IMessageListener)
    ''' <summary> Gets the private listeners. </summary>
    ''' <value> The private listeners. </value>
    Public Overridable ReadOnly Property PrivateListeners As IEnumerable(Of IMessageListener) Implements ITalker.PrivateListeners
        Get
            If Me._PrivateListeners Is Nothing Then _PrivateListeners = New List(Of IMessageListener)
            Return Me._PrivateListeners
        End Get
    End Property

    ''' <summary> Adds a private listener. </summary>
    ''' <param name="listener"> The listener. </param>
    Public Overridable Sub AddPrivateListener(ByVal listener As IMessageListener) Implements ITalker.AddPrivateListener
        If Me.PrivateListeners IsNot Nothing Then
            Me._PrivateListeners.Add(listener)
        End If
        Me.AddListener(listener)
    End Sub

    ''' <summary> Adds private listeners. </summary>
    ''' <param name="listeners"> The listeners. </param>
    Public Overridable Sub AddPrivateListeners(ByVal listeners As IEnumerable(Of IMessageListener)) Implements ITalker.AddPrivateListeners
        If listeners Is Nothing Then Throw New ArgumentNullException(NameOf(listeners))
        If Me.PrivateListeners IsNot Nothing Then
            Me._PrivateListeners.AddRange(listeners)
        End If
        Me.AddListeners(listeners)
    End Sub

    ''' <summary> Adds private listeners. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="talker"> The talker. </param>
    Public Overridable Sub AddPrivateListeners(ByVal talker As ITraceMessageTalker) Implements ITalker.AddPrivateListeners
        If talker Is Nothing Then Throw New ArgumentNullException(NameOf(talker))
        Me.AddPrivateListeners(talker.Listeners)
    End Sub

    ''' <summary> Removes the private listener described by listener. </summary>
    ''' <param name="listener"> The listener. </param>
    Public Overridable Sub RemovePrivateListener(ByVal listener As IMessageListener) Implements ITalker.RemovePrivateListener
        Me.RemoveListener(listener)
        If Me.PrivateListeners IsNot Nothing Then
            Me._PrivateListeners.Remove(listener)
        End If
    End Sub

    ''' <summary> Removes the private listeners. </summary>
    Public Overridable Sub RemovePrivateListeners() Implements ITalker.RemovePrivateListeners
        For Each listener As IMessageListener In Me.PrivateListeners
            Me.RemoveListener(listener)
        Next
        Me._PrivateListeners.Clear()
    End Sub

    ''' <summary> Removes the listener described by listener. </summary>
    ''' <param name="listener"> The listener. </param>
    Public Overridable Sub RemoveListener(ByVal listener As IMessageListener) Implements ITalker.RemoveListener
        Me.Talker.RemoveListener(listener)
    End Sub

    ''' <summary> Removes the specified listeners. </summary>
    ''' <param name="listeners"> The listeners. </param>
    Public Overridable Sub RemoveListeners(ByVal listeners As IEnumerable(Of IMessageListener)) Implements ITalker.RemoveListeners
        If listeners Is Nothing Then Throw New ArgumentNullException(NameOf(listeners))
        For Each listener As IMessageListener In listeners
            Me.RemoveListener(listener)
        Next
    End Sub

    ''' <summary> Adds a listener. </summary>
    ''' <param name="listener"> The listener. </param>
    Public Overridable Sub AddListener(ByVal listener As IMessageListener) Implements ITalker.AddListener
        Me.Talker.AddListener(listener)
        Me.IdentifyTalkers()
    End Sub

    ''' <summary> Adds the specified listeners. </summary>
    ''' <param name="listeners"> The listeners. </param>
    Public Overridable Sub AddListeners(ByVal listeners As IEnumerable(Of IMessageListener)) Implements ITalker.AddListeners
        If listeners Is Nothing Then Throw New ArgumentNullException(NameOf(listeners))
        For Each listener As IMessageListener In listeners
            Me.AddListener(listener)
        Next
    End Sub

    ''' <summary> Adds the listeners. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Overridable Sub AddListeners(ByVal talker As ITraceMessageTalker) Implements ITalker.AddListeners
        If talker Is Nothing Then Throw New ArgumentNullException(NameOf(talker))
        Me.AddListeners(talker.Listeners)
    End Sub

    ''' <summary> Applies the trace level to all listeners to the specified type. </summary>
    ''' <param name="listenerType"> Type of the listener. </param>
    ''' <param name="value">        The value. </param>
    Public Overridable Sub ApplyListenerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType) Implements ITalker.ApplyListenerTraceLevel
        ' this should apply only to the listeners associated with this form
        ' Not this: Me.Talker.ApplyListenerTraceLevel(listenerType, value)
        Me.IdentifyTalkers()
    End Sub

    ''' <summary> Applies the trace level type to all talkers. </summary>
    ''' <param name="listenerType"> Type of the trace level. </param>
    ''' <param name="value">        The value. </param>
    Public Overridable Sub ApplyTalkerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType) Implements ITalker.ApplyTalkerTraceLevel
        Me.Talker.ApplyTalkerTraceLevel(listenerType, value)
    End Sub

    ''' <summary> Applies the talker trace levels described by talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Overridable Sub ApplyTalkerTraceLevels(ByVal talker As ITraceMessageTalker) Implements ITalker.ApplyTalkerTraceLevels
        Me.Talker.ApplyTalkerTraceLevels(talker)
    End Sub

    ''' <summary> Applies the talker listeners trace levels described by talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Overridable Sub ApplyListenerTraceLevels(ByVal talker As ITraceMessageTalker) Implements ITalker.ApplyListenerTraceLevels
        Me.Talker.ApplyListenerTraceLevels(talker)
    End Sub

#End Region

#Region " TALKER PUBLISH "

    ''' <summary>
    ''' Uses the <see cref="Talker"/> to publish the message or log if <see cref="Talker"/> is
    ''' nothing.
    ''' </summary>
    ''' <param name="message"> The message. </param>
    ''' <returns> A String. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overloads Function Publish(ByVal message As Core.Pith.TraceMessage) As String
        If message Is Nothing Then Return String.Empty
        Dim activity As String = ""
        Dim result As String = message.Details
        Try
            If Me.Talker Is Nothing Then
                activity = $"logging unpublished message: {message}"
                My.MyLibrary.LogUnpublishedMessage(message)
            Else
                activity = $"publishing: {message}"
                Me.Talker.Publish(message)
            End If
        Catch ex As Exception
            If Debugger.IsAttached Then
                Debug.Assert(Not Debugger.IsAttached, $"Exception {activity};. {ex.ToFullBlownString}")
            Else
                Windows.Forms.MessageBox.Show(Nothing, $"Exception {activity};. {ex.ToFullBlownString}",
                                              "Exception publishing message", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Error,
                                              Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)
            End If
        End Try
        Return result
    End Function

    ''' <summary>
    ''' Uses the <see cref="Talker"/> to publish the message or log if <see cref="Talker"/> is
    ''' nothing.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overridable Overloads Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.Pith.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overridable Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        If ex Is Nothing Then
            Return String.Empty
        Else
            Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
        End If
    End Function


    ''' <summary>
    ''' Uses the <see cref="Talker"/> to publish the message or log if <see cref="Talker"/> is
    ''' nothing.
    ''' </summary>
    ''' <param name="e"> Action event information. </param>
    ''' <returns> A String. </returns>
    Protected Overloads Function Publish(ByVal e As Core.Pith.ActionEventArgs) As String
        If e Is Nothing Then
            Return String.Empty
        Else
            Return Me.Publish(e.OutcomeEvent, e.Details)
        End If
    End Function

    ''' <summary>
    ''' Uses the <see cref="Talker"/> to publish the message or log if <see cref="Talker"/> is
    ''' nothing.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Protected Overloads Function Publish(ByVal eventType As TraceEventType, ByVal format As String, ParamArray args() As Object) As String
        Return Me.Publish(eventType, String.Format(format, args))
    End Function

    ''' <summary> Publish warning. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <returns> A String. </returns>
    Protected Function PublishWarning(ByVal activity As String) As String
        Return Me.Publish(TraceEventType.Warning, activity)
    End Function

    ''' <summary> Publish warning. </summary>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Protected Function PublishWarning(ByVal format As String, ParamArray args() As Object) As String
        Return Me.Publish(TraceEventType.Warning, format, args)
    End Function

    ''' <summary> Publish verbose. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <returns> A String. </returns>
    Protected Function PublishVerbose(ByVal activity As String) As String
        Return Me.Publish(TraceEventType.Verbose, activity)
    End Function

    ''' <summary> Publish verbose. </summary>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Protected Function PublishVerbose(ByVal format As String, ParamArray args() As Object) As String
        Return Me.Publish(TraceEventType.Verbose, format, args)
    End Function

    ''' <summary> Publish information. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <returns> A String. </returns>
    Protected Function PublishInfo(ByVal activity As String) As String
        Return Me.Publish(TraceEventType.Information, activity)
    End Function

    ''' <summary> Publish information. </summary>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Protected Function PublishInfo(ByVal format As String, ParamArray args() As Object) As String
        Return Me.Publish(TraceEventType.Information, format, args)
    End Function

#End Region

#Region " TRACE EVENTS: LOG AND SHOW "

    Private _TraceLogEvents As BindingList(Of KeyValuePair(Of TraceEventType, String))

    ''' <summary> Gets a list of types of the trace events. </summary>
    ''' <value> A list of types of the trace events. </value>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")>
    Public ReadOnly Property TraceLogEvents As BindingList(Of KeyValuePair(Of TraceEventType, String))
        Get
            If Me._TraceLogEvents Is Nothing OrElse Not Me._TraceLogEvents.Any Then
                Me._TraceLogEvents = New BindingList(Of KeyValuePair(Of TraceEventType, String)) From {
                                ViewModelTalkerBase.ToTraceEvent(TraceEventType.Critical),
                                ViewModelTalkerBase.ToTraceEvent(TraceEventType.Error),
                                ViewModelTalkerBase.ToTraceEvent(TraceEventType.Warning),
                                ViewModelTalkerBase.ToTraceEvent(TraceEventType.Information),
                                ViewModelTalkerBase.ToTraceEvent(TraceEventType.Verbose)
                }
            End If
            Return Me._TraceLogEvents
        End Get
    End Property

    ''' <summary> Converts a value to a trace event. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a KeyValuePair(Of TraceEventType, String) </returns>
    Public Shared Function ToTraceEvent(ByVal value As TraceEventType) As KeyValuePair(Of TraceEventType, String)
        Return New KeyValuePair(Of TraceEventType, String)(value, value.ToString)
    End Function

    Private _TraceLogEvent As KeyValuePair(Of TraceEventType, String)
    ''' <summary> Gets the trace log level. </summary>
    ''' <value> The trace level. </value>
    Public Property TraceLogEvent As KeyValuePair(Of TraceEventType, String)
        Get
            Return Me._TraceLogEvent
        End Get
        Set(value As KeyValuePair(Of TraceEventType, String))
            If value.Key <> Me.TraceLogEvent.Key Then
                Me._TraceLogEvent = value
                Me.TraceLogLevel = value.Key
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the trace log level. </summary>
    ''' <value> The trace level. </value>
    Public Property TraceLogLevel As TraceEventType
        Get
            Return Me.Talker.TraceLogLevel
        End Get
        Set(value As TraceEventType)
            If value <> Me.TraceLogLevel Then
                Me.ApplyTalkerTraceLevel(ListenerType.Logger, value)
                Me.SafePostPropertyChanged()
                Me.TraceLogEvent = ViewModelTalkerBase.ToTraceEvent(value)
            End If
        End Set
    End Property

    Private _TraceShowEvents As BindingList(Of KeyValuePair(Of TraceEventType, String))

    ''' <summary> Gets a list of types of the trace events. </summary>
    ''' <value> A list of types of the trace events. </value>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")>
    Public ReadOnly Property TraceShowEvents As BindingList(Of KeyValuePair(Of TraceEventType, String))
        Get
            If Me._TraceShowEvents Is Nothing OrElse Not Me._TraceShowEvents.Any Then
                Me._TraceShowEvents = New BindingList(Of KeyValuePair(Of TraceEventType, String)) From {
                                ViewModelTalkerBase.ToTraceEvent(TraceEventType.Critical),
                                ViewModelTalkerBase.ToTraceEvent(TraceEventType.Error),
                                ViewModelTalkerBase.ToTraceEvent(TraceEventType.Warning),
                                ViewModelTalkerBase.ToTraceEvent(TraceEventType.Information),
                                ViewModelTalkerBase.ToTraceEvent(TraceEventType.Verbose)
                }
            End If
            Return Me._TraceShowEvents
        End Get
    End Property

    Private _TraceShowEvent As KeyValuePair(Of TraceEventType, String)
    ''' <summary> Gets the trace Show level. </summary>
    ''' <value> The trace level. </value>
    Public Property TraceShowEvent As KeyValuePair(Of TraceEventType, String)
        Get
            Return Me._TraceShowEvent
        End Get
        Set(value As KeyValuePair(Of TraceEventType, String))
            If value.Key <> Me.TraceShowEvent.Key Then
                Me._TraceShowEvent = value
                Me.TraceShowLevel = value.Key
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the trace Show level. </summary>
    ''' <value> The trace level. </value>
    Public Property TraceShowLevel As TraceEventType
        Get
            Return Me.Talker.TraceShowLevel
        End Get
        Set(value As TraceEventType)
            If value <> Me.TraceShowLevel Then
                Me.ApplyTalkerTraceLevel(ListenerType.Display, value)
                Me.SafePostPropertyChanged()
                Me.TraceShowEvent = ViewModelTalkerBase.ToTraceEvent(value)
            End If
        End Set
    End Property


#End Region

#Region " TRACE EVENT COMBO BOX MANAGEMENT "

    Private Shared _TraceEventTypes As BindingList(Of TraceEventType)

    ''' <summary> Gets a list of types of the trace events. </summary>
    ''' <value> A list of types of the trace events. </value>
    Public Shared ReadOnly Property TraceEventTypes As BindingList(Of TraceEventType)
        Get
            If ViewModelTalkerBase._TraceEventTypes Is Nothing OrElse Not ViewModelTalkerBase._TraceEventTypes.Any Then
                ViewModelTalkerBase._TraceEventTypes = New BindingList(Of TraceEventType) From {TraceEventType.Critical, TraceEventType.Error,
                                                                                                TraceEventType.Warning, TraceEventType.Information,
                                                                                                TraceEventType.Verbose}
            End If
            Return ViewModelTalkerBase._TraceEventTypes
        End Get
    End Property

    ''' <summary> List trace event levels. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="control"> The control. </param>
    Public Shared Sub ListTraceEventLevels(ByVal control As System.Windows.Forms.ComboBox)
        If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
        Dim comboEnabled As Boolean = control.Enabled
        control.Enabled = False
        control.DataSource = Nothing
        control.ValueMember = "Key"
        control.DisplayMember = "Value"
        control.Items.Clear()
        control.Items.Add(ViewModelTalkerBase.ToTraceEvent(TraceEventType.Critical))
        control.Items.Add(ViewModelTalkerBase.ToTraceEvent(TraceEventType.Error))
        control.Items.Add(ViewModelTalkerBase.ToTraceEvent(TraceEventType.Warning))
        control.Items.Add(ViewModelTalkerBase.ToTraceEvent(TraceEventType.Information))
        control.Items.Add(ViewModelTalkerBase.ToTraceEvent(TraceEventType.Verbose))

        ' This does not seem to work. It lists the items in the combo box, but the control items are empty.
        ' control.DataSource = isr.Core.Pith.EnumExtensions.ValueDescriptionPairs(TraceEventType.Critical).ToArray
        ' control.SelectedIndex = -1
        control.Enabled = comboEnabled
        control.Invalidate()
    End Sub

    ''' <summary> Select item. </summary>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   The value. </param>
    Public Shared Sub SelectItem(ByVal control As Windows.Forms.ToolStripComboBox, ByVal value As TraceEventType)
        If control IsNot Nothing Then
            ' control.SelectedItem = New KeyValuePair(Of TraceEventType, String)(value, value.ToString)
            control.SelectedItem = ViewModelTalkerBase.ToTraceEvent(value)
        End If
    End Sub

    ''' <summary> Selected value. </summary>
    ''' <param name="control">      The control. </param>
    ''' <param name="defaultValue"> The default value. </param>
    ''' <returns> A TraceEventType. </returns>
    Public Shared Function SelectedValue(ByVal control As Windows.Forms.ToolStripComboBox, ByVal defaultValue As TraceEventType) As TraceEventType
        If control IsNot Nothing AndAlso control.SelectedItem IsNot Nothing Then
            Dim kvp As KeyValuePair(Of TraceEventType, String) = CType(control.SelectedItem, KeyValuePair(Of TraceEventType, String))
            If [Enum].IsDefined(GetType(TraceEventType), kvp.Key) Then
                defaultValue = kvp.Key
            End If
        End If
        Return defaultValue
    End Function

#End Region

End Class


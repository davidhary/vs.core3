﻿Imports isr.Core.Pith
''' <summary> Interface for trace message listener. </summary>
''' <license>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
Public Interface IMessageListener
    Inherits IEquatable(Of IMessageListener)

    ''' <summary> Writes. </summary>
    ''' <param name="message"> The message to write. </param>
    Sub Write(message As String)

    ''' <summary> Writes a line. </summary>
    ''' <param name="message"> The message to write. </param>
    Sub WriteLine(message As String)

    ''' <summary> Registers this object. </summary>
    ''' <param name="level"> The level. </param>
    Sub Register(ByVal level As TraceEventType)

    ''' <summary> Gets or sets the type of the listener. </summary>
    ''' <value> The type of the listener. </value>
    ReadOnly Property ListenerType As ListenerType

    ''' <summary> Gets the trace level. </summary>
    Property TraceLevel As TraceEventType

    ''' <summary> Applies the trace level described by Trace Level. </summary>
    ''' <param name="value"> The trace level. </param>
    Sub ApplyTraceLevel(ByVal value As TraceEventType)

    ''' <summary> Determine if we should trace. </summary>
    ''' <param name="value"> The event message. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Function ShouldTrace(ByVal value As TraceEventType) As Boolean

    ''' <summary> Gets or sets a unique identifier. </summary>
    ''' <value> The identifier of the unique. </value>
    ReadOnly Property UniqueId As Guid

    ''' <summary> Gets or sets the is disposed. </summary>
    ''' <value> The is disposed. </value>
    ReadOnly Property IsDisposed As Boolean

    ''' <summary> Gets or sets the is thread safe. </summary>
    ''' <value> The is thread safe. </value>
    ReadOnly Property IsThreadSafe As Boolean

End Interface

''' <summary> Collection of trace listeners. </summary>
''' <license>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/29/2015" by="David" revision=""> Created. </history>
Public Class MessageListenerCollection
    Inherits Collections.ObjectModel.KeyedCollection(Of Guid, IMessageListener)

    Public Sub New()
        MyBase.New
        Me._listenerDix = New Dictionary(Of ListenerType, ListenerCollection)
    End Sub

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As IMessageListener) As Guid
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
        Return item.UniqueId
    End Function

    Private Class ListenerCollection
        Inherits Collections.ObjectModel.KeyedCollection(Of Guid, IMessageListener)
        Protected Overrides Function GetKeyForItem(item As IMessageListener) As Guid
            If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
            Return item.UniqueId
        End Function
    End Class

    Private _listenerDix As Dictionary(Of ListenerType, ListenerCollection)

    ''' <summary> Adds items. </summary>
    ''' <param name="items"> The items to add. </param>
    Public Overloads Sub Add(ByVal items As IEnumerable(Of IMessageListener))
        If items?.Any Then
            For Each item As IMessageListener In items
                Me.Add(item)
            Next
        End If
    End Sub

    ''' <summary> Gets or sets the contains logger listener. </summary>
    ''' <value> The contains logger listener. </value>
    Public ReadOnly Property ContainsLoggerListener As Boolean

    ''' <summary> Query if the collection contains a listener. </summary>
    ''' <param name="listenerType"> Type of the listener. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function ContainsListener(ByVal listenerType As ListenerType) As Boolean
        Return Me._listenerDix.ContainsKey(listenerType) AndAlso Me._listenerDix.Item(listenerType).Any
    End Function

    ''' <summary>
    ''' Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    ''' <param name="item"> The object to add to the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
    Public Overloads Sub Add(ByVal item As IMessageListener)
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
        If Not Me.Contains(item) Then
            MyBase.Add(item)
            If Not Me._listenerDix.ContainsKey(item.ListenerType) Then
                Me._listenerDix.Add(item.ListenerType, New ListenerCollection)
            End If
            If Not Me._listenerDix.Item(item.ListenerType).Contains(item.UniqueId) Then
                Me._listenerDix.Item(item.ListenerType).Add(item)
            End If
            Me._ContainsLoggerListener = Me.ContainsListener(ListenerType.Logger)
        End If
    End Sub

    ''' <summary> Removes the given item. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The object to add to the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
    Public Overloads Sub Remove(ByVal item As IMessageListener)
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
        If Me.Contains(item) Then
            MyBase.Remove(item)
            If Me._listenerDix.Item(item.ListenerType).Contains(item.UniqueId) Then
                Me._listenerDix.Item(item.ListenerType).Remove(item)
            End If
            Me._ContainsLoggerListener = Me.ContainsListener(ListenerType.Logger)
        End If
    End Sub

    ''' <summary> Adds a range. </summary>
    ''' <param name="items"> The items to add. </param>
    Public Sub AddRange(items As IEnumerable(Of IMessageListener))
        If items?.Any Then
            For Each item As ITraceMessageListener In items
                Me.Add(item)
            Next
        End If
    End Sub

    ''' <summary> Applies the trace level. </summary>
    ''' <param name="listenerType"> Type of the listener. </param>
    ''' <param name="value">        The value. </param>
    Public Sub ApplyTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType)
        If Me._listenerDix.ContainsKey(listenerType) Then
            For Each listener As IMessageListener In Me._listenerDix.Item(listenerType)
                listener.ApplyTraceLevel(value)
            Next
        End If
    End Sub

End Class

''' <summary> Values that represent listener types. </summary>
Public Enum ListenerType
    ''' <summary> An enum constant representing a logger type listener. </summary>
    Logger
    ''' <summary> An enum constant representing a display type listener. </summary>
    Display
End Enum


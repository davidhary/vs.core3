Imports System.Collections.Concurrent
Imports System.ComponentModel
Imports System.Threading
Imports System.Threading.Tasks
Imports System.Windows.Forms
Imports isr.Core.Pith.ExceptionExtensions
''' <summary> Messages display text box. </summary>
''' <license> (c) 2002 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/21/2002" by="David" revision="1.0.839.x"> created. </history>
<Description("Messages Test Box")>
Public Class MessagesBox
    Inherits TextBox

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor for this class. </summary>
    Public Sub New()
        MyBase.New()

        ' set defaults
        Me.syncLocker = New Object
        Me._lines = New List(Of String)
        Me.Multiline = True
        Me.ReadOnly = True
        Me.CausesValidation = False
        Me.ScrollBars = ScrollBars.Both
        Me.Size = New System.Drawing.Size(150, 150)
        Me._ResetKnownState()

        MyBase.ContextMenuStrip = CreateContextMenuStrip()
        Me._ContentsQueue = New ConcurrentQueue(Of String)

        ' instantiate cancellation token
        Me._CancellationTokenSource = New CancellationTokenSource
        Me._CancellationToken = Me._CancellationTokenSource.Token

        Me.CaptureSyncContext(SynchronizationContext.Current)

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.ContainerPanel = Nothing
                Me.ContainerTreeNode = Nothing
                If Me._TraceSourceListener IsNot Nothing Then
                    Me._TraceSourceListener.Listener = Nothing
                    Me._TraceSourceListener.Dispose()
                    Me._TraceSourceListener = Nothing
                End If
                If Me._TraceListener IsNot Nothing Then
                    Me._TraceListener.Listener = Nothing
                    Me._TraceListener.Dispose()
                    Me._TraceListener = Nothing
                End If
                Me.DisposeWorker()
                Me.DisposeTask()
                If Me.CancellationTokenSource IsNot Nothing Then
                    Me._CancellationTokenSource.Dispose()
                    Me._CancellationTokenSource = Nothing
                End If
                Me._Lines?.Clear() : Me._Lines = Nothing
                Me._MyContextMenuStrip?.Dispose() : Me._MyContextMenuStrip = Nothing
                Me.RemoveEventHandler(Me.PropertyChangedEvent)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    Private Sub _ResetKnownState()
        Me.BackColor = Drawing.SystemColors.Info
        Me.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._applyTraceLevel(TraceEventType.Verbose)
        Me.TabCaption = "Log"
        Me.CaptionFormat = "{0} " & System.Text.Encoding.GetEncoding(437).GetString(New Byte() {240})
        Me.ResetCount = 200
        Me.PresetCount = 100
        Me.Appending = False
    End Sub

    Protected Overridable Sub ResetKnownState()
        Me._ResetKnownState()
    End Sub

#End Region

#Region " LIST MANAGER "

    Private _Appending As Boolean
    ''' <summary> Gets the appending sentinel. Items are appended to an appending list. 
    '''           Otherwise, items are added to the top of the list. </summary>
    ''' <value> The ascending sentinel; True if the list is ascending or descending. </value>
    <Category("Appearance"), Description("True to add items to the bottom of the list"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(False)>
    Public Property Appending As Boolean
        Get
            Return Me._Appending
        End Get
        Set(value As Boolean)
            If value <> Me.Appending Then
                Me._Appending = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The synchronization locker. </summary>
    Private syncLocker As Object

    ''' <summary> Holds the list of lines to display. </summary>
    Private _Lines As List(Of String)

    ''' <summary> Adds the message to the message list. A message may include one or more lines. </summary>
    ''' <param name="message"> The message to add. </param>
    Private Sub AddMessageLines(ByVal message As String)
        If Not String.IsNullOrWhiteSpace(message) Then
            Dim values As String() = message.Split(CChar(Environment.NewLine))
            If values.Count = 1 Then
                If Me.Appending Then
                    Me._Lines.Add(message)
                Else
                    Me._Lines.Insert(0, message)
                End If
            Else
                If Me.Appending Then
                    Me._Lines.AddRange(values)
                Else
                    Me._Lines.InsertRange(0, values)
                End If
            End If
        End If
    End Sub

    ''' <summary> Executes the clear action. </summary>
    Protected Overridable Sub OnClear()
        Me._lines.Clear()
        MyBase.Clear()
        Me.NewMessagesAdded = False
    End Sub

    ''' <summary> Clears all text from the text box control. </summary>
    Public Shadows Sub Clear()
        If Me.InvokeRequired Then
            Me.Invoke(New Action(AddressOf Me.Clear), New Object() {})
        Else
            If Not Me.IsDisposed AndAlso Me._lines IsNot Nothing Then
                SyncLock syncLocker
                    Me.OnClear()
                End SyncLock
            End If
        End If
    End Sub

    Private _ResetCount As Integer

    ''' <summary> Gets or sets the reset count. </summary>
    ''' <remarks> The message list gets reset to the preset count when the message count exceeds the
    ''' reset count. </remarks>
    ''' <value> <c>ResetSize</c>is an integer property. </value>
    <Category("Appearance"), Description("Number of lines at which to reset"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("100")>
    Public Property ResetCount() As Integer
        Get
            Return Me._resetCount
        End Get
        Set(ByVal value As Integer)
            If Me.ResetCount <> value Then
                Me._resetCount = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    Private _PresetCount As Integer

    ''' <summary> Gets or sets the preset count. </summary>
    ''' <remarks> The message list gets reset to the preset count when the message count exceeds the
    ''' reset count. </remarks>
    ''' <value> <c>PresetSize</c>is an integer property. </value>
    <Category("Appearance"), Description("Number of lines to reset to"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("50")>
    Public Property PresetCount() As Integer
        Get
            Return Me._presetCount
        End Get
        Set(ByVal value As Integer)
            If Me.PresetCount <> value Then
                Me._presetCount = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the sentinel indicating if the control is visible to the user. </summary>
    ''' <value> The showing. </value>
    Protected ReadOnly Property UserVisible As Boolean
        Get
            Return Me.Visible AndAlso Me.Height > 100 AndAlso (Me.GetContainerControl IsNot Nothing AndAlso
                Me.GetContainerControl.ActiveControl IsNot Nothing AndAlso Me.GetContainerControl.ActiveControl.Visible)
        End Get
    End Property

    ''' <summary> Displays this object. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _Display()
        If Me.IsDisposed Then
        ElseIf Me.InvokeRequired Then
            ' the object could be disposed when the thread still has items to report. 
            ' trying to detect the disposed object does not seem to work.
            Try
                If Not Me.IsDisposed Then Me.Invoke(New Action(AddressOf Me._Display))
            Catch
            End Try
        Else
            Try
                ' this has produced a momentary error: Destination array not long enough.
                Me.Lines = Me._Lines.ToArray
                Me.NewMessagesAdded = False
                Me.SelectionStart = 0
                Me.SelectionLength = 0
            Catch
            End Try
        End If
    End Sub

    ''' <summary> Displays the available lines and clear the <see cref="NewMessagesAdded">message
    ''' sentinel</see>. </summary>
    Public Overridable Sub Display()
        If Me.UserVisible Then
            Me._Display()
        End If
    End Sub

    ''' <summary> Raises the <see cref="E: Control.VisibleChanged" /> event. 
    '''           Updates the display. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnVisibleChanged(e As System.EventArgs)
        Me.Display()
        MyBase.OnVisibleChanged(e)
    End Sub

#End Region

#Region " ADD MESSAGE "

    ''' <summary> Prepends or appends a new value to the messages box. </summary>
    ''' <remarks>
    ''' Starts a Background Worker by calling RunWorkerAsync. The Text property of the TextBox
    ''' control is set when the Background Worker raises the RunWorkerCompleted event.
    ''' </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String. </returns>
    Public Overridable Function AddMessage(ByVal value As String) As String
        Me.GetContentsQueue()?.Enqueue(value)
        Me.FlushMessages()
        Return value
    End Function

    ''' <summary> Adds an error message to the message box. </summary>
    ''' <param name="value"> The <see cref="System.Exception">error</see> to add. </param>
    Public Sub AddMessage(ByVal value As Exception)
        If value IsNot Nothing Then Me.AddMessage(value.ToFullBlownString)
    End Sub

#End Region

#Region " CAPTION "

    Private _NewMessagesAdded As Boolean

    ''' <summary> Gets or sets the sentinel indicating that new messages were added. </summary>
    ''' <value> The new messages available. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property NewMessagesAdded As Boolean
        Get
            Return Me._NewMessagesAdded
        End Get
        Set(value As Boolean)
            If value <> Me.NewMessagesAdded Then
                Me._NewMessagesAdded = value
                Me.SafePostPropertyChanged()
                Me.UpdateCaption()
            End If
        End Set
    End Property

    Private _TabCaption As String

    ''' <summary> Gets or sets the tab caption. </summary>
    ''' <value> The tab caption. </value>
    <Category("Appearance"), Description("Default title for the parent tab"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("Log")>
    Public Property TabCaption() As String
        Get
            Return Me._TabCaption
        End Get
        Set(ByVal Value As String)
            If Me.TabCaption <> Value Then
                Me._TabCaption = Value
                Me.SafePostPropertyChanged()
                Me.UpdateCaption()
            End If
        End Set
    End Property

    Private _Captionformat As String

    ''' <summary> Gets or sets the caption format indicating that messages were added. </summary>
    ''' <value> The tab caption format. </value>
    <Category("Appearance"), Description("Formats the tab caption with number of new messages"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("{0} =")>
    Public Property CaptionFormat() As String
        Get
            Return Me._Captionformat
        End Get
        Set(ByVal Value As String)
            If String.IsNullOrEmpty(Value) Then Value = ""
            If Not Value.Equals(Me.CaptionFormat) Then
                Me._Captionformat = Value
                Me.SafePostPropertyChanged()
                Me.UpdateCaption()
            End If
        End Set
    End Property

    Private Property _Caption As String

    ''' <summary> Gets or sets the caption. </summary>
    ''' <value> The caption. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property Caption As String
        Get
            Return Me._Caption
        End Get
        Set(value As String)
            If String.IsNullOrEmpty(value) Then value = ""
            If Not value.Equals(Me.Caption) Then
                Me._Caption = value
                Me.SafePostPropertyChanged()
                Me.SafeTextSetter(Me.ContainerPanel, Me.Caption)
                Me.SafeTextSetter(Me.ContainerTreeNode, Me.Caption)
            End If
        End Set
    End Property

    ''' <summary> Updates the caption. </summary>
    Private Sub UpdateCaption()
        If True = Me.NewMessagesAdded Then
            Me.Caption = String.Format(Globalization.CultureInfo.CurrentCulture, Me.CaptionFormat, Me.TabCaption)
        Else
            Me.Caption = Me.TabCaption
        End If
    End Sub

    ''' <summary> Gets or sets the container panel. </summary>
    ''' <value> The parent panel. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ContainerPanel As Panel

    ''' <summary> Gets or sets the container tree node. </summary>
    ''' <value> The container tree node. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ContainerTreeNode As TreeNode

    ''' <summary> Updates the container panel caption. </summary>
    ''' <param name="panel">The container panel.</param>
    ''' <param name="value">The value.</param>
    Private Sub SafeTextSetter(ByVal panel As Panel, ByVal value As String)
        If panel IsNot Nothing Then
            If panel.InvokeRequired Then
                panel.Invoke(New Action(Of Panel, String)(AddressOf Me.SafeTextSetter), New Object() {panel, value})
            Else
                panel.Text = value
            End If
        End If
    End Sub

    ''' <summary> Updates the container tree node caption. </summary>
    ''' <param name="node"> The container tree node. </param>
    ''' <param name="value"> The value. </param>
    Private Sub SafeTextSetter(ByVal node As TreeNode, ByVal value As String)
        If node IsNot Nothing Then
            If node.TreeView?.InvokeRequired Then
                node.TreeView.Invoke(New Action(Of TreeNode, String)(AddressOf Me.SafeTextSetter), New Object() {node, value})
            Else
                node.Text = value
                System.Windows.Forms.Application.DoEvents()
            End If
        End If
    End Sub

#End Region

#Region " CONTEXT MENU STRIP "

    Dim _MyContextMenuStrip As ContextMenuStrip

    ''' <summary> Creates a context menu strip. </summary>
    ''' <returns> The new context menu strip. </returns>
    Private Function CreateContextMenuStrip() As ContextMenuStrip

        ' Create a new ContextMenuStrip control.
        Me._MyContextMenuStrip = New ContextMenuStrip()

        ' Attach an event handler for the 
        ' ContextMenuStrip control's Opening event.
        AddHandler Me._MyContextMenuStrip.Opening, AddressOf Me.contectMenuOpeningHandler

        Return Me._MyContextMenuStrip

    End Function

    ''' <summary> Adds menu items. </summary>
    ''' <remarks> This event handler is invoked when the <see cref="ContextMenuStrip"/> control's
    ''' Opening event is raised. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub ContectMenuOpeningHandler(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)

        Me._MyContextMenuStrip = TryCast(sender, ContextMenuStrip)

        ' Clear the ContextMenuStrip control's Items collection.
        Me._MyContextMenuStrip.Items.Clear()

        ' Populate the ContextMenuStrip control with its default items.
        ' myContextMenuStrip.Items.Add("-")
        Me._MyContextMenuStrip.Items.Add(New ToolStripMenuItem("Clear &All", Nothing, AddressOf Me.clearAllHandler, "Clear"))
        Me._MyContextMenuStrip.Items.Add(New ToolStripMenuItem("Flush &Queue", Nothing, AddressOf Me.flushQueuesHandler, "Flush"))
        If Me.MyLog IsNot Nothing Then
            Me._MyContextMenuStrip.Items.Add(New ToolStripMenuItem("&Open Log File", Nothing, AddressOf Me.RequestOpeningLogFile, "Open Log File"))
        End If
        If Me.MyLog IsNot Nothing Then
            Me._MyContextMenuStrip.Items.Add(New ToolStripMenuItem("Open Log &Folder", Nothing, AddressOf Me.RequestOpeningLogFolder, "Open Log Folder"))
        End If

        ' Set Cancel to false. 
        ' It is optimized to true based on empty entry.
        e.Cancel = False

    End Sub

    ''' <summary> Applies the high point Output. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ClearAllHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Clear()
    End Sub

    ''' <summary> Flush messages and content. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FlushQueuesHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Me.FlushContent()
        Me.FlushMessages()
    End Sub

#End Region

#Region " LOG FILE HANDLING "

    ''' <summary> Request opening log File. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RequestOpeningLogFile(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MyLog?.OpenLogFile()
    End Sub

    ''' <summary> Request opening log folder. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RequestOpeningLogFolder(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MyLog?.OpenFolderLocation()
    End Sub

    ''' <summary> Gets or sets my log. </summary>
    ''' <value> my log. </value>
    Private ReadOnly Property MyLog As MyLog

    ''' <summary> Assign log. </summary>
    ''' <param name="value"> The <see cref="System.Exception">
    '''                      error</see>
    '''                      to add. </param>
    Public Sub AssignLog(ByVal value As MyLog)
        Me._MyLog = value
    End Sub

#End Region

#Region " ASYNC QUEUED MESSAGES "

    ''' <summary> The cancellation token source. </summary>
    Private ReadOnly Property CancellationTokenSource As CancellationTokenSource

    ''' <summary> The cancellation token. </summary>
    Private ReadOnly Property CancellationToken As CancellationToken

    Private Function IsCancellationRequested() As Boolean
        Return Me.CancellationToken.IsCancellationRequested
    End Function

    Protected ReadOnly Property ContentsQueue As ConcurrentQueue(Of String)

    ''' <summary> Gets contents queue. </summary>
    ''' <returns> The contents queue. </returns>
    Private Function GetContentsQueue() As ConcurrentQueue(Of String)
        Return Me._ContentsQueue
    End Function

    ''' <summary> Gets the content. </summary>
    ''' <returns> The content. </returns>
    Private Function GetContent() As String
        Dim result As String = Nothing
        Me.ContentsQueue.TryDequeue(result)
        Return result
    End Function


    ''' <summary> This event handler sets the Text property of the TextBox control. It is called on the
    ''' thread that created the TextBox control, so the call is thread-safe. </summary>
    Private Sub UpdateDisplay()
        Me.ApplyCapturedSyncContext()
        SyncLock syncLocker
            Do While Me.GetContentsQueue()?.Any AndAlso Not Me.IsCancellationRequested
                Me.AddMessageLines(Me.GetContent)
            Loop
            If Me._Lines.Count > Me.ResetCount Then
                If Me.Appending Then
                    Me._Lines.RemoveRange(0, Me._Lines.Count - Me.PresetCount)
                Else
                    Me._Lines.RemoveRange(Me.PresetCount, Me._Lines.Count - Me.PresetCount)
                End If
            End If
            Me.NewMessagesAdded = Not Me.UserVisible
        End SyncLock
        If Not Me.IsCancellationRequested Then Me.Display()
        Me.Invalidate()
        Application.DoEvents()
    End Sub

    ''' <summary> Uses the message worker to flush any queued messages. </summary>
    ''' <remarks> If the calling thread is different from the thread that created the TextBox control,
    ''' this method creates a SetTextCallback and calls itself asynchronously using the Invoke
    ''' method. </remarks>
    Public Sub FlushMessages()
        ' Me.FlushMessagesWorker()
        Me.FlushMessagesTask()
    End Sub

#End Region

#Region " ASYNC QUEUED MESSAGES: TASK "

    ''' <summary> Gets the task. </summary>
    ''' <value> The task. </value>
    Public ReadOnly Property MyTask As Task

    ''' <summary> Query if this object is busy. </summary>
    ''' <returns> <c>true</c> if busy; otherwise <c>false</c> </returns>
    Private Function IsBusy() As Boolean
        Return Not Me.IsDisposed AndAlso Me.MyTask IsNot Nothing AndAlso Me.MyTask.Status = TaskStatus.Running
    End Function

    ''' <summary> Uses the message worker to flush any queued messages. </summary>
    ''' <remarks> If the calling thread is different from the thread that created the TextBox control,
    ''' this method creates a SetTextCallback and calls itself asynchronously using the Invoke
    ''' method. </remarks>
    Private Sub FlushMessagesTask()
        If Not Me.IsBusy Then
            Me._MyTask = Me.StartAsyncTask()
        End If
    End Sub

    ''' <summary> Start Asynchronous task. </summary>
    ''' <returns> A Task. </returns>
    Private Async Function StartAsyncTask() As Task
        Await Task.Run(AddressOf Me.UpdateDisplay)
    End Function

    ''' <summary> Dispose task. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub DisposeTask()
        Try
            If Me.CancellationTokenSource IsNot Nothing Then Me.CancellationTokenSource.Cancel()
            If Me.MyTask IsNot Nothing Then
                Me.MyTask.Dispose()
                Me._MyTask = Nothing
            End If
        Catch
        End Try
    End Sub

#End Region

#Region " WORKER "

    ''' <summary> The background worker is used for setting the messages text box
    ''' in a thread safe way. </summary>
    Private WithEvents Worker As BackgroundWorker

    ''' <summary> Dispose worker. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub DisposeWorker()
        Try
            If Me.CancellationTokenSource IsNot Nothing Then Me.CancellationTokenSource.Cancel()
            If Me.Worker IsNot Nothing Then
                Me.Worker.CancelAsync()
                If Not (Me.Worker.IsBusy OrElse Me.Worker.CancellationPending) Then
                    Me.Worker.Dispose()
                End If
            End If
        Catch
        End Try
    End Sub

    ''' <summary> This event handler sets the Text property of the TextBox control. It is called on the
    ''' thread that created the TextBox control, so the call is thread-safe. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Run worker completed event information. </param>
    Private Sub UpdateDisplay(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles Worker.RunWorkerCompleted
        If Not (Me.IsDisposed OrElse e.Cancelled OrElse e.Error IsNot Nothing) Then
            Me.UpdateDisplay()
        End If
    End Sub

#End Region

#Region " MESSAGE TRACE LISTENER "

    Private _TraceSourceListener As isr.Core.Pith.MessageTraceSourceListener

    ''' <summary> Listens the given source. </summary>
    ''' <param name="source"> Source for the. </param>
    Public Sub Listen(ByVal source As TraceSource)
        If source IsNot Nothing AndAlso Me._TraceSourceListener Is Nothing Then
            ' Me._TraceSourceListener = New isr.Core.Pith.MessageTraceSourceListener With {.Listener = Me}
            Me._TraceSourceListener = MessageTraceSourceListener.Create
            Me._TraceSourceListener.Listener = Me
            source.Listeners.Add(Me._TraceSourceListener)
        End If
    End Sub

    Private _TraceListener As isr.Core.Pith.MessageTraceListener

    ''' <summary> Listens the given source. </summary>
    Public Sub Listen()
        If Me._TraceListener Is Nothing Then
            Me._TraceListener = MessageTraceListener.Create()
            Me._TraceListener.Listener = Me
            ' Me._TraceListener = New MessageTraceListener With {.Listener = Me}
            Trace.Listeners.Add(Me._TraceListener)
        End If
    End Sub

#End Region

End Class


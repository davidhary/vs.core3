﻿''' <summary> Interface for trace listener. </summary>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
Public Interface ITraceListener

    ''' <summary> Writes. </summary>
    ''' <param name="message"> The message to write. </param>
    Sub Write(message As String)

    ''' <summary> Writes a line. </summary>
    ''' <param name="message"> The message to write. </param>
    Sub WriteLine(message As String)

    ''' <summary> Registers this object. </summary>
    ''' <param name="level"> The level. </param>
    Sub Register(ByVal level As TraceEventType)

    ''' <summary> Gets or sets the trace level. </summary>
    ''' <value> The trace level. </value>
    Property TraceLevel As TraceEventType

    ''' <summary> Gets or sets the is thread safe. </summary>
    ''' <value> The is thread safe. </value>
    ReadOnly Property IsThreadSafe As Boolean

End Interface

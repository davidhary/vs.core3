﻿Imports System.ComponentModel
Imports System.Drawing
Imports System.Threading
Imports System.Windows.Forms
Imports isr.Core.Pith.EventHandlerExtensions
Imports isr.Core.Pith.ExceptionExtensions

''' <summary> A property invoke control base. </summary>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/15/2018" by="David" revision=""> Created. </history>
Public Class PropertyInvokeControlBase
    Inherits UserControl

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> A private constructor for this class making it not publicly creatable. This ensure
    ''' using the class as a singleton. </summary>
    Protected Sub New()
        MyBase.New()
        Me.InitializeComponent()
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveEventHandlers()
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#Region " Windows Form Designer generated code "

    'Required by the Windows Form Designer
    Private components As IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        '
        'MyUserControlBase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = AutoScaleMode.Inherit
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "PropertyNotifyControlBase"
        Me.Size = New System.Drawing.Size(175, 173)
        Me.ResumeLayout(False)

    End Sub

#End Region

#End Region

#Region " SYNC CONTEXT "

    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
        ' for use with running concurrent tasks within the control
        Me.CaptureSyncContext(SynchronizationContext.Current)
    End Sub

#End Region

#Region " TOOL TIP "

    ''' <summary> Sets the Tool tip for all form controls that inherit a <see cref="Control">control base.</see> </summary>
    ''' <param name="parent">  Reference to the parent form or control. </param>
    ''' <param name="toolTip"> The parent form or control tool tip. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")>
    Public Shared Sub ToolTipSetter(ByVal parent As System.Windows.Forms.Control, ByVal toolTip As ToolTip)
        If parent Is Nothing Then Return
        If toolTip Is Nothing Then Return
        If TypeOf parent Is PropertyNotifyControlBase Then
            CType(parent, PropertyNotifyControlBase).ToolTipSetter(toolTip)
        ElseIf parent.HasChildren Then
            For Each control As System.Windows.Forms.Control In parent.Controls
                ToolTipSetter(control, toolTip)
            Next
        End If
    End Sub

    ''' <summary> Sets a tool tip for all controls on the user control. Uses the message already set
    ''' for this control. </summary>
    ''' <remarks> This is required because setting a tool tip from the parent form does not show the
    ''' tool tip if hovering above children controls hosted by the user control. </remarks>
    ''' <param name="toolTip"> The tool tip control from the parent form. </param>
    Public Sub ToolTipSetter(ByVal toolTip As ToolTip)
        If toolTip IsNot Nothing Then
            ApplyToolTipToChildControls(Me, toolTip, toolTip.GetToolTip(Me))
        End If
    End Sub

    ''' <summary> Sets a tool tip for all controls on the user control. </summary>
    ''' <remarks> This is required because setting a tool tip from the parent form does not show the
    ''' tool tip if hovering above children controls hosted by the user control. </remarks>
    ''' <param name="toolTip"> The tool tip control from the parent form. </param>
    ''' <param name="message"> The tool tip message to apply to all the children controls and their
    ''' children. </param>
    Public Sub ToolTipSetter(ByVal toolTip As ToolTip, ByVal message As String)
        ApplyToolTipToChildControls(Me, toolTip, message)
    End Sub

    ''' <summary> Applies the tool tip to all control hosted by the parent as well as all the children
    ''' with these control. </summary>
    ''' <param name="parent">  The parent control. </param>
    ''' <param name="toolTip"> The tool tip control from the parent form. </param>
    ''' <param name="message"> The tool tip message to apply to all the children controls and their
    ''' children. </param>
    Private Sub ApplyToolTipToChildControls(ByVal parent As Control, ByVal toolTip As ToolTip, ByVal message As String)
        For Each control As Control In parent.Controls
            toolTip.SetToolTip(control, message)
            If parent.HasChildren Then
                ApplyToolTipToChildControls(control, toolTip, message)
            End If
        Next
    End Sub

#End Region

End Class


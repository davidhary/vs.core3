﻿Imports System.Threading
Imports System.ComponentModel
Partial Public Class PropertyNotifyControlBase

#Region " SYNC CONTEXT "

    ''' <summary> Caches the synchronization context for threading functions. </summary>
    ''' <value> The captured synchronization context. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property CapturedSyncContext As Threading.SynchronizationContext

    ''' <summary> Applies the captured or a new synchronization context. </summary>
    Public Sub ApplyCapturedSyncContext()
        If SynchronizationContext.Current Is Nothing Then
            If Me.CapturedSyncContext Is Nothing Then Me._CapturedSyncContext = New SynchronizationContext
            Threading.SynchronizationContext.SetSynchronizationContext(Me.CapturedSyncContext)
        End If
    End Sub

    ''' <summary>
    ''' Captures and applies the given synchronization context, the current sync context or a new
    ''' sync contexts if the first two are null.
    ''' </summary>
    ''' <param name="syncContext"> Context for the synchronization. </param>
    Public Overridable Sub CaptureSyncContext(ByVal syncContext As Threading.SynchronizationContext)
        If syncContext IsNot Nothing Then Me._CapturedSyncContext = syncContext
        Me.ApplyCapturedSyncContext()
    End Sub

#End Region

End Class


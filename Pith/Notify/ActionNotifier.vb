﻿Imports System.Threading
Imports System.Threading.Tasks
Imports isr.Core.Pith.EventHandlerExtensions
''' <summary> Action notifier. </summary>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="4/26/2017" by="David" revision=""> Created. </history>
Public Class ActionNotifier(Of T)

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.NotifyAsyncActions = New List(Of Func(Of T, Task))()
    End Sub

#End Region

#Region " SYNC CONTEXT "

    ''' <summary> Caches the synchronization context for threading functions. </summary>
    ''' <value> The captured synchronization context. </value>
    Public ReadOnly Property CapturedSyncContext As Threading.SynchronizationContext

    ''' <summary> Applies the captured or a new synchronization context. </summary>
    Public Sub ApplyCapturedSyncContext()
        If SynchronizationContext.Current Is Nothing Then
            If Me.CapturedSyncContext Is Nothing Then Me._CapturedSyncContext = New SynchronizationContext
            Threading.SynchronizationContext.SetSynchronizationContext(Me.CapturedSyncContext)
        End If
    End Sub

    Private ReadOnly Property CurrentSyncContext As SynchronizationContext
        Get
            If SynchronizationContext.Current Is Nothing Then
                If Me.CapturedSyncContext Is Nothing Then Me._CapturedSyncContext = New SynchronizationContext
                Threading.SynchronizationContext.SetSynchronizationContext(Me.CapturedSyncContext)
            End If
            Return SynchronizationContext.Current
        End Get
    End Property

    ''' <summary>
    ''' Captures and applies the given synchronization context, the current sync context or a new
    ''' sync contexts if the first two are null.
    ''' </summary>
    ''' <param name="syncContext"> Context for the synchronization. </param>
    Public Sub CaptureSyncContext(ByVal syncContext As Threading.SynchronizationContext)
        If syncContext IsNot Nothing Then Me._CapturedSyncContext = syncContext
        Me.ApplyCapturedSyncContext()
    End Sub

#End Region

#Region " SYNC NOTIFIERS "

    ''' <summary> Event queue for all listeners interested in Notified events. </summary>
    Private Event Notified As Action(Of T)

    ''' <summary> Safe post. </summary>
    ''' <param name="argument"> The action argument. </param>
    Public Sub SafePost(ByVal argument As T)
        Dim evt As Action(Of T) = Me.NotifiedEvent
        Me.CurrentSyncContext.Post(Sub() evt?(argument), Nothing)
    End Sub

    ''' <summary> Safe send. </summary>
    ''' <param name="argument"> The action argument. </param>
    Public Sub SafeSend(ByVal argument As T)
        Dim evt As Action(Of T) = Me.NotifiedEvent
        Me.CurrentSyncContext.Send(Sub() evt?(argument), Nothing)
    End Sub

    ''' <summary>
    ''' Executes the given operation on a different thread, and waits for the result.
    ''' </summary>
    ''' <param name="argument"> The action argument. </param>
    Public Sub Invoke(ByVal argument As T)
        Dim evt As Action(Of T) = Me.NotifiedEvent
        evt?.Invoke(argument)
    End Sub

    ''' <summary> Registers this object. </summary>
    ''' <param name="action"> The action. </param>
    Public Sub Register(ByVal action As Action(Of T))
        AddHandler Me.Notified, action
    End Sub

#End Region

#Region " ASYNC TASK NOTIFIERS "

    ''' <summary> Gets or sets the notify asynchronous actions. </summary>
    ''' <value> The notify asynchronous actions. </value>
    Private ReadOnly Property NotifyAsyncActions As New List(Of Func(Of T, Task))()

    ''' <summary>
    ''' Executes the asynchronous on a different thread, and waits for the result.
    ''' </summary>
    ''' <param name="argument"> The action argument. </param>
    ''' <returns> A Task. </returns>
    Public Async Function InvokeAsync(ByVal argument As T) As Task
        Me.Invoke(argument)
        For Each callback As Func(Of T, Task) In Me.NotifyAsyncActions
            Await callback(argument)
        Next callback
    End Function

    ''' <summary> Registers this object. </summary>
    ''' <param name="action"> The action. </param>
    Public Sub Register(ByVal action As Func(Of T, Task))
        Me.NotifyAsyncActions.Add(action)
    End Sub

#End Region

End Class


﻿Imports System.ComponentModel
Imports System.Drawing
Imports System.Threading
Imports System.Windows.Forms
Imports isr.Core.Pith.EventHandlerExtensions

Partial Public Class PropertyNotifyControlBase
    Inherits UserControl
    Implements INotifyPropertyChanged, INotifyPropertyChanging

#Region " SYNC CONTEXT "

    ''' <summary> Gets the sentinel indicating that multiple synchronization contexts are expected. </summary>
    ''' <remarks> When having multiple user interfaces or a thread running within the user interface,
    ''' the current synchronization context may not reflect the contexts of the current UI causing a
    ''' cross thread exceptions. In this case the more complex
    ''' <see cref="SafeInvokePropertyChanged">thread safe methods</see> must be used. </remarks>
    ''' <value> <c>True</c> if more than one synchronization contexts should be expected. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property MultipleSyncContextsExpected As Boolean

#End Region

#Region " PROPERTY CHANGED "

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub InvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokePropertyChanged(ByVal obj As Object)
        Me.InvokePropertyChanged(CType(obj, PropertyChangedEventArgs))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeInvokePropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeInvokePropertyChanged(New PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.ApplyCapturedSyncContext()
        Me.PropertyChangedEvent.SafeInvoke(Me, e)
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeBeginInvokePropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeBeginInvokePropertyChanged(New PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeBeginInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.ApplyCapturedSyncContext()
        Me.PropertyChangedEvent.SafeBeginInvoke(Me, e)
    End Sub

#End Region

#Region " NOTIFICATIONS "

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    Protected Overridable Sub AsyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If e IsNot Nothing Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeBeginInvokePropertyChanged(e)
            Else
                SynchronizationContext.Current.Post(New SendOrPostCallback(AddressOf InvokePropertyChanged), e)
            End If
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub AsyncNotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.AsyncNotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

    ''' <summary>
    ''' Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes or
    ''' Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property Value.
    ''' </summary>
    ''' <param name="e"> Property Changed event information. </param>
    Protected Overridable Sub SyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If e IsNot Nothing Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeInvokePropertyChanged(e)
            Else
                SynchronizationContext.Current.Send(New SendOrPostCallback(AddressOf InvokePropertyChanged), e)
            End If
        End If
    End Sub

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SyncNotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SyncNotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#End Region

#Region " PROPERTY CHANGING "

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property Value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub InvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        Dim evt As PropertyChangingEventHandler = Me.PropertyChangingEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property Value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokePropertyChanging(ByVal obj As Object)
        Me.InvokePropertyChanging(CType(obj, PropertyChangingEventArgs))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeInvokePropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeInvokePropertyChanging(New PropertyChangingEventArgs(name))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property Value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeInvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        Me.ApplyCapturedSyncContext()
        Me.PropertyChangingEvent.SafeInvoke(Me, e)
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanging">Event</see> in a property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeBeginInvokePropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeBeginInvokePropertyChanging(New PropertyChangingEventArgs(name))
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanging">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeBeginInvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        Me.ApplyCapturedSyncContext()
        Me.PropertyChangingEvent.SafeBeginInvoke(Me, e)
    End Sub

#End Region

#Region " NOTIFICATIONS "

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanging">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SyncNotifyPropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeInvokePropertyChanging(New PropertyChangingEventArgs(name))
            Else
                SynchronizationContext.Current.Send(New SendOrPostCallback(AddressOf InvokePropertyChanging),
                                                    New PropertyChangingEventArgs(name))
            End If
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub AsyncNotifyPropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeBeginInvokePropertyChanging(New PropertyChangingEventArgs(name))
            Else
                SynchronizationContext.Current.Post(New SendOrPostCallback(AddressOf InvokePropertyChanging), New PropertyChangingEventArgs(name))
            End If
        End If
    End Sub

#End Region

#End Region

End Class


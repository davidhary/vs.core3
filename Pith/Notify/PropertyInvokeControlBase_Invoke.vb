﻿Imports System.Threading
Imports System.ComponentModel
Imports isr.Core.Pith.EventHandlerExtensions
Imports isr.Core.Pith.ExceptionExtensions

Partial Public Class PropertyInvokeControlBase
    Implements IPropertyNotify

#Region " CLEANUP "

    Protected Overridable Sub RemoveEventHandlers()
        Me.RemoveEventHandler(Me.PropertyChangedEvent)
        Me.RemoveEventHandler(Me.PropertyChangingEvent)
    End Sub

#End Region

#Region " PROPERTY CHANGED EVENT IMPLEMENTATION "

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary> Removes the event handler. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As PropertyChangedEventHandler)
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.PropertyChanged, CType(d, PropertyChangedEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overridable Sub InvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary>
    ''' Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">
    ''' sync enabled entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">
    ''' event</see>
    ''' in Property Value. Must be called with the
    ''' <see cref="SynchronizationContext">
    ''' sync context</see>
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Overridable Sub InvokePropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        evt?.Invoke(Me, New PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeInvokePropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeInvokePropertyChanged(New PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overridable Sub SafeInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.PropertyChangedEvent.SafeInvoke(Me, e)
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeBeginInvokePropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeBeginInvokePropertyChanged(New PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overridable Sub SafeBeginInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.PropertyChangedEvent.SafeBeginInvoke(Me, e)
    End Sub

#End Region

#Region " ASYNC NOTIFY "

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    Protected Overridable Sub SafePostPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If e IsNot Nothing Then
            Me.SafeBeginInvokePropertyChanged(e)
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafePostPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafePostPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#Region " SYNC NOTIFY "

    ''' <summary>
    ''' Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes or
    ''' Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property Value.
    ''' </summary>
    ''' <param name="e"> Property Changed event information. </param>
    Protected Overridable Sub SyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If e IsNot Nothing Then
            Me.SafeInvokePropertyChanged(e)
        End If
    End Sub

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SyncNotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SyncNotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#End Region

#Region " PROPERTY CHANGING EVENT IMPLEMENTATION "

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanging(ByVal sender As Object, ByVal e As PropertyChangingEventArgs) Implements INotifyPropertyChanging.PropertyChanging

    ''' <summary> Removes the event handler. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As PropertyChangingEventHandler)
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.PropertyChanging, CType(d, PropertyChangingEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property Value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overridable Sub InvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        Me.PropertyChangingEvent.UnsafeInvoke(Me, e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeInvokePropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeInvokePropertyChanging(New PropertyChangingEventArgs(name))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property Value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overridable Sub SafeInvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        Me.PropertyChangingEvent.SafeInvoke(Me, e)
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanging">Event</see> in a property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeBeginInvokePropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeBeginInvokePropertyChanging(New PropertyChangingEventArgs(name))
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanging">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overridable Sub SafeBeginInvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        Me.PropertyChangingEvent.SafeBeginInvoke(Me, e)
    End Sub

#End Region

#Region " SYNC NOTIFY "

    ''' <summary>
    ''' Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes or
    ''' Dynamically Invokes) a change <see cref="PropertyChanging">event</see> in Property Value.
    ''' </summary>
    ''' <param name="e"> Property Changing event information. </param>
    Protected Overridable Sub SyncNotifyPropertyChanging(ByVal e As PropertyChangingEventArgs)
        If e IsNot Nothing Then
            Me.SafeInvokePropertyChanging(e)
        End If
    End Sub

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanging">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Overridable Sub SyncNotifyPropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SyncNotifyPropertyChanging(New PropertyChangingEventArgs(name))
        End If
    End Sub

#End Region

#Region " ASYNC NOTIFY "

    ''' <summary>
    ''' Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins Invoke
    ''' or Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property Value.
    ''' </summary>
    ''' <param name="e"> Property Changing event information. </param>
    Protected Overridable Sub AsyncNotifyPropertyChanging(ByVal e As PropertyChangingEventArgs)
        If e IsNot Nothing Then
            Me.SafeBeginInvokePropertyChanging(e)
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub AsyncNotifyPropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.AsyncNotifyPropertyChanging(New PropertyChangingEventArgs(name))
        End If
    End Sub

#End Region

#End Region

End Class


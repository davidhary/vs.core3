﻿Imports System.ComponentModel
Imports isr.Core.Pith.EventHandlerExtensions
''' <summary>
''' Defines the contract that must be implemented by property changed notifiers.
''' </summary>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para>
''' </license>
''' <history date="9/6/2016" by="David" revision="">         Created. </history>
''' <history date="09/28/12" by="David" revision="1.2.4654"> Documented. </history>
Public MustInherit Class NotifyPropertyChangedBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="PropertyNotifyBase" /> class. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary>
    ''' Gets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Private _IsDisposed As Boolean

    ''' <summary> Query if this object is disposed. </summary>
    ''' <returns> <c>true</c> if disposed; otherwise <c>false</c> </returns>
    Public Function IsDisposed() As Boolean
        Return Me._IsDisposed
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveEventHandlers()
            End If
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " DIRTY IMPLEMENTATION "

    Private _PropertyValueChanged As Boolean

    ''' <summary> Query if this object is property value changed. </summary>
    ''' <returns> <c>true</c> if property value changed; otherwise <c>false</c> </returns>
    Public Function IsPropertyValueChanged() As Boolean
        Return Me._PropertyValueChanged
    End Function

    ''' <summary> Clears the property value changed. </summary>
    Protected Sub ClearPropertyValueChanged()
        Me._PropertyValueChanged = False
    End Sub

    ''' <summary> Sets property value changed. </summary>
    Public Sub SetPropertyValueChanged()
        Me._PropertyValueChanged = True
    End Sub

#End Region


End Class


﻿Imports System.ComponentModel
Imports isr.Core.Pith.EventHandlerExtensions
''' <summary>
''' Defines the contract that must be implemented by property change notifiers.
''' </summary>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para>
''' </license>
''' <history date="9/6/2016" by="David" revision="">         Created. </history>
''' <history date="09/28/12" by="David" revision="1.2.4654"> Documented. </history>
Public MustInherit Class PropertyNotifyBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="PropertyNotifyBase" /> class. </summary>
    Protected Sub New()
        MyBase.New()
        Me._ApplyCapturedSyncContext()
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary>
    ''' Gets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Public Property IsDisposed() As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveEventHandlers()
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " DIRTY IMPLEMENTATION "

    Private _PropertyValueChanged As Boolean
    ''' <summary> Gets or sets the sentinel telling if a property value changed. </summary>
    ''' <value> The property value changed sentinel. </value>
    Public Property PropertyValueChanged As Boolean
        Get
            Return Me._PropertyValueChanged
        End Get
        Set(value As Boolean)
            If value <> Me.PropertyValueChanged Then
                Me._PropertyValueChanged = value
                Me.ApplyCapturedSyncContext()
                Me.PropertyChangedEvent.SafePost(Me, New PropertyChangedEventArgs(NameOf(PropertyValueChanged)))
            End If
        End Set
    End Property

#End Region


End Class


﻿Imports System.ComponentModel
Imports isr.Core.Pith.EventHandlerExtensions
Imports isr.Core.Pith.ExceptionExtensions
Partial Public Class PropertyNotifyBase
    Implements IPropertyNotify

#Region " CLEANUP "

    ''' <summary> Removes the event handlers. </summary>
    Protected Overridable Sub RemoveEventHandlers()
        Me.RemoveEventHandler(Me.PropertyChangedEvent)
        Me.RemoveEventHandler(Me.PropertyChangingEvent)
    End Sub

#End Region

#Region " PROPERTY CHANGED EVENT IMPLEMENTATION "

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary> Removes the event handler. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As PropertyChangedEventHandler)
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.PropertyChanged, CType(d, PropertyChangedEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

#Region " DISPATCH "

    ''' <summary> Gets or sets the dispatcher. </summary>
    ''' <remarks> It seems as though the dispatcher solves the cross thread issues; For now 20181201. </remarks>
    ''' <value> The dispatcher. </value>
    Public Property Dispatcher As Windows.Threading.Dispatcher = Windows.Threading.Dispatcher.CurrentDispatcher

    ''' <summary> Begins dispatch property changed. </summary>
    ''' <param name="e"> Property changed event information. </param>
    Protected Overridable Sub BeginDispatchPropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dispatcher.Invoke(New Action(Of PropertyChangedEventArgs)(AddressOf Me.SafePostPropertyChanged), New Object() {e})
    End Sub

    ''' <summary> Begins dispatch property changed. </summary>
    ''' <param name="name"> (Optional) caller member. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub BeginDispatchPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.BeginDispatchPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

    ''' <summary> Dispatch property changed. </summary>
    ''' <param name="e"> Property changed event information. </param>
    Protected Overridable Sub DispatchPropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dispatcher.Invoke(New Action(Of PropertyChangedEventArgs)(AddressOf Me.SafeSendPropertyChanged), New Object() {e})
    End Sub

    ''' <summary> Dispatch property changed. </summary>
    ''' <param name="name"> (Optional) caller member. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub DispatchPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.DispatchPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#Region " SEND POST "

    Public Function ChangedEvent() As PropertyChangedEventHandler
        Return Me.PropertyChangedEvent
    End Function

    ''' <summary>
    ''' Asynchronously notifies property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="e"> Property Changed event information. </param>
    Protected Overridable Sub SafePostPropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.PropertyValueChanged = True
        Me.ApplyCapturedSyncContext()
        Me.PropertyChangedEvent.SafePost(Me, e)
    End Sub

    ''' <summary>
    ''' Asynchronously notifies property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="name"> (Optional) caller member. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafePostPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafePostPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

    ''' <summary>
    ''' Synchronously notifies property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="e"> Property Changed event information. </param>
    Protected Overridable Sub SafeSendPropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.PropertyValueChanged = True
        Me.ApplyCapturedSyncContext()
        Me.PropertyChangedEvent.SafeSend(Me, e)
    End Sub

    ''' <summary>
    ''' Synchronously notifies property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="name"> (Optional) caller member. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeSendPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafeSendPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#End Region

#Region " PROPERTY CHANGING EVENT IMPLEMENTATION "

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanging(ByVal sender As Object, ByVal e As PropertyChangingEventArgs) Implements INotifyPropertyChanging.PropertyChanging

    ''' <summary> Removes the event handler. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As PropertyChangingEventHandler)
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.PropertyChanging, CType(d, PropertyChangingEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

#Region " SEND POST "

    ''' <summary>
    ''' Asynchronously notifies property changing on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="e"> Property Changing event information. </param>
    Protected Overridable Sub SafePostPropertyChanging(ByVal e As PropertyChangingEventArgs)
        Me.ApplyCapturedSyncContext()
        Me.PropertyChangingEvent.SafePost(Me, e)
    End Sub

    ''' <summary>
    ''' Asynchronously notifies property changing on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="name"> (Optional) caller member. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafePostPropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafePostPropertyChanging(New PropertyChangingEventArgs(name))
        End If
    End Sub

    ''' <summary>
    ''' Synchronously notifies property changing on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="e"> Property Changing event information. </param>
    Protected Overridable Sub SafeSendPropertyChanging(ByVal e As PropertyChangingEventArgs)
        Me.ApplyCapturedSyncContext()
        Me.PropertyChangingEvent.SafeSend(Me, e)
    End Sub

    ''' <summary>
    ''' Synchronously notifies property changing on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="name"> (Optional) caller member. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeSendPropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafeSendPropertyChanging(New PropertyChangingEventArgs(name))
        End If
    End Sub

#End Region

#End Region

End Class


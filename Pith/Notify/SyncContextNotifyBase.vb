﻿Imports System.Threading
Imports System.ComponentModel
''' <summary>
''' Defines the contract that must be implemented by sync context notifiers.
''' </summary>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para>
''' </license>
''' <history date="5/5/2017" by="David" revision=""> Created from property notify base. </history>
Public MustInherit Class SyncContextNotifyBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="PropertyNotifyBase" /> class. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " SYNC CONTEXT "

    ''' <summary> Caches the synchronization context for threading functions. </summary>
    ''' <value> The captured synchronization context. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property CapturedSyncContext As Threading.SynchronizationContext

    ''' <summary> Applies the captured or a new synchronization context. </summary>
    Public Sub ApplyCapturedSyncContext()
        If SynchronizationContext.Current Is Nothing Then
            If Me.CapturedSyncContext Is Nothing Then Me._CapturedSyncContext = New SynchronizationContext
            Threading.SynchronizationContext.SetSynchronizationContext(Me.CapturedSyncContext)
        End If
    End Sub

    ''' <summary>
    ''' Captures and applies the given synchronization context, the current sync context or a new
    ''' sync contexts if the first two are null.
    ''' </summary>
    ''' <param name="syncContext"> Context for the synchronization. </param>
    Public Overridable Sub CaptureSyncContext(ByVal syncContext As Threading.SynchronizationContext)
        If syncContext IsNot Nothing Then Me._CapturedSyncContext = syncContext
        Me.ApplyCapturedSyncContext()
    End Sub

#End Region

End Class


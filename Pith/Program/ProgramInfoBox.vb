Imports System.ComponentModel
Imports System.Drawing
Imports System.Threading
Imports System.Windows.Forms
Imports isr.Core.Pith.EventHandlerExtensions
Imports isr.Core.Pith.ExceptionExtensions
''' <summary> Program Info text box. </summary>
''' <license> (c) 2002 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/21/2002" by="David" revision="1.0.839.x"> created. </history>
<Description("Program Info Box")>
Public Class ProgramInfoBox
    Inherits RichTextBox
    Implements INotifyPropertyChanged

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> A constructor for this panel. </summary>
    Public Sub New()
        MyBase.New()
        MyBase.ContextMenuStrip = CreateContextMenuStrip()
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me._ContentsFont IsNot Nothing Then Me._ContentsFont.Dispose() : Me._ContentsFont = Nothing
                If Me._HeaderFont IsNot Nothing Then Me._HeaderFont.Dispose() : Me._HeaderFont = Nothing
                Me.RemoveEventHandler(Me.PropertyChangedEvent)
                Me.RemoveEventHandler(Me.DisplayingCopyrightsEvent)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DISPLAY "

    Private _Title As String
    <System.ComponentModel.Description("The group box title of the control."), System.ComponentModel.Category("Appearance"),
    System.ComponentModel.DefaultValue("ABOUT")>
    Public Property Title As String
        Get
            Return _Title
        End Get
        Set(ByVal value As String)
            Me._Title = value
            Me.SafePostPropertyChanged()
        End Set
    End Property

    Private _HeaderFont As Font
    ''' <summary> Gets or sets the header font. </summary>
    ''' <value> The header font. </value>
    <System.ComponentModel.Description("The font of the header lines."), System.ComponentModel.Category("Appearance"),
    System.ComponentModel.DefaultValue(GetType(Font), "Me.Font")>
    Public Property HeaderFont As Font
        Get
            If Me._HeaderFont Is Nothing Then
                Me._HeaderFont = New Font(Me.Font, FontStyle.Regular)
            End If
            Return Me._HeaderFont
        End Get
        Set(ByVal value As Font)
            Me._HeaderFont = value
        End Set
    End Property

    Private _ContentsFont As Font

    ''' <summary> Gets or sets the Contents font. </summary>
    ''' <value> The Contents font. </value>
    <System.ComponentModel.Description("The font of the contents lines."), System.ComponentModel.Category("Appearance")>
    Public Property ContentsFont As Font
        Get
            If Me._ContentsFont Is Nothing Then
                Me._ContentsFont = New Font(Me.Font, FontStyle.Bold)
            End If
            Return Me._ContentsFont
        End Get
        Set(ByVal value As Font)
            Me._ContentsFont = value
        End Set
    End Property

    ''' <summary> Appends a header line. </summary>
    ''' <param name="text"> The text. </param>
    Public Sub AppendHeaderLine(ByVal text As String)
        Me.AppendLine(text, Me.HeaderFont)
    End Sub

    ''' <summary> Appends a content line. </summary>
    ''' <param name="text"> The text. </param>
    ''' <param name="font"> The font. </param>
    Public Sub AppendLine(ByVal text As String, ByVal font As Font)
        Me.SelectionStart = Me.TextLength
        Me.SelectionFont = font
        Me.AppendText(text)
        Me.AppendText(Environment.NewLine)
    End Sub

    ''' <summary> Appends a content line. </summary>
    ''' <param name="text"> The text. </param>
    Public Sub AppendContentLine(ByVal text As String)
        Me.AppendLine(text, Me.ContentsFont)
    End Sub

    ''' <summary> Updates the display. </summary>
    Public Sub Display()

        Me.Clear()

        Me.AppendHeaderLine("Product Name:")
        Me.AppendContentLine(My.Application.Info.ProductName)
        Me.AppendContentLine("")

        Me.AppendHeaderLine("Program Title:")
        Me.AppendContentLine(My.Application.Info.Title)
        Me.AppendContentLine("")

        Me.AppendHeaderLine("Program Name:")
        Me.AppendContentLine(My.Application.Info.AssemblyName)
        Me.AppendContentLine("")

        Me.AppendHeaderLine("Program Version:")
        Me.AppendContentLine(My.Application.Info.Version.ToString)
        Me.AppendContentLine("")

        Me.AppendHeaderLine("Program Folder:")
        Me.AppendContentLine(My.Application.Info.DirectoryPath)
        Me.AppendContentLine("")

        Me.AppendHeaderLine("Program Data Folder:")
        Me.AppendContentLine(My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData)
        Me.AppendContentLine("")

        Me.AppendHeaderLine("User Program Data Folder:")
        Me.AppendContentLine(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData)
        Me.AppendContentLine("")

        Dim configuration As System.Configuration.Configuration = Global.System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None)
        Me.AppendHeaderLine("All Users Configuration File:")
        Me.AppendContentLine(configuration.FilePath())
        If configuration.Locations.Count > 0 Then
            Me.AppendHeaderLine("Configuration Locations:")
            For Each s As System.Configuration.ConfigurationLocation In configuration.Locations
                Me.AppendContentLine(s.Path)
            Next
        End If
        Me.AppendContentLine("")

        configuration = Global.System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.PerUserRoaming)
        Me.AppendHeaderLine("User Roaming Configuration File:")
        Me.AppendContentLine(configuration.FilePath())
        If configuration.Locations.Count > 0 Then
            Me.AppendHeaderLine("Configuration Locations:")
            For Each s As System.Configuration.ConfigurationLocation In configuration.Locations
                Me.AppendContentLine(s.Path)
            Next
        End If
        Me.AppendContentLine("")

        configuration = Global.System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.PerUserRoamingAndLocal)
        Me.AppendHeaderLine("User Local Configuration File:")
        Me.AppendContentLine(configuration.FilePath())
        If configuration.Locations.Count > 0 Then
            Me.AppendHeaderLine("Configuration Locations:")
            For Each s As System.Configuration.ConfigurationLocation In configuration.Locations
                Me.AppendContentLine(s.Path)
            Next
        End If
        Me.AppendContentLine("")

        ' allows the calling application to add data.
        Me.OnDisplayingCopyrights(System.EventArgs.Empty)

        Me.AppendHeaderLine("Product Copyrights:")
        Me.AppendContentLine(My.Application.Info.Copyright)
        Me.AppendContentLine("")

    End Sub

#End Region

#Region " EVENTS "

    ''' <summary> Raised before displaying the copyrights. </summary>
    '''   <param name="e">The reading event arguments.</param>
    Public Event DisplayingCopyrights As EventHandler(Of EventArgs)

    ''' <summary> Raises the displaying Copyrights event. </summary>
    ''' <param name="e"> The reading event arguments. </param>
    Friend Sub OnDisplayingCopyrights(ByVal e As EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.DisplayingCopyrightsEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Removes the event Handler; called within the dispose event. </summary>
    ''' <param name="value"> The handler. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As EventHandler(Of EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.DisplayingCopyrights, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

#End Region

#Region " CONTEXT MENU STRIP "

    Dim _MyContextMenuStrip As ContextMenuStrip

    ''' <summary> Creates a context menu strip. </summary>
    ''' <returns> The new context menu strip. </returns>
    Private Function CreateContextMenuStrip() As ContextMenuStrip

        ' Create a new ContextMenuStrip control.
        Me._MyContextMenuStrip = New ContextMenuStrip()

        ' Attach an event handler for the 
        ' ContextMenuStrip control's Opening event.
        AddHandler _MyContextMenuStrip.Opening, AddressOf Me.ContextMenuOpeningHandler

        Return Me._MyContextMenuStrip

    End Function

    ''' <summary> Adds menu items. </summary>
    ''' <remarks> This event handler is invoked when the <see cref="ContextMenuStrip"/> control's
    ''' Opening event is raised. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub ContextMenuOpeningHandler(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)

        Me._MyContextMenuStrip = TryCast(sender, ContextMenuStrip)

        ' Clear the ContextMenuStrip control's Items collection.
        Me._MyContextMenuStrip.Items.Clear()

        ' Populate the ContextMenuStrip control with its default items.
        ' myContextMenuStrip.Items.Add("-")
        Me._MyContextMenuStrip.Items.Add(New ToolStripMenuItem("Clear &All", Nothing, AddressOf Me.clearAllHandler, "Clear"))
        Me._MyContextMenuStrip.Items.Add(New ToolStripMenuItem("&Refresh", Nothing, AddressOf Me.RefreshHandler, "Refresh"))

        ' Set Cancel to false. 
        ' It is optimized to true based on empty entry.
        e.Cancel = False

    End Sub

    ''' <summary> Applies the high point Output. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ClearAllHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Clear()
    End Sub

    ''' <summary> Refreshes content. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RefreshHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Display()
    End Sub

#End Region

#Region " PROPERTTY CHANGED "

    ''' <summary> Gets the sentinel indicating that multiple synchronization contexts are expected. </summary>
    ''' <remarks> When having multiple user interfaces or a thread running within the user interface,
    ''' the current synchronization context may not reflect the contexts of the current UI causing a
    ''' cross thread exceptions. In this case the more complex
    ''' <see cref="SafeInvokePropertyChanged">thread safe methods</see> must be used. </remarks>
    ''' <value> <c>True</c> if more than one synchronization contexts should be expected. </value>
    Public Property MultipleSyncContextsExpected As Boolean

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary> Removes event handler. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As PropertyChangedEventHandler)
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.PropertyChanged, CType(d, PropertyChangedEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub InvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokePropertyChanged(ByVal obj As Object)
        Me.InvokePropertyChanged(CType(obj, PropertyChangedEventArgs))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeInvokePropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeInvokePropertyChanged(New PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.PropertyChangedEvent.SafeInvoke(Me, e)
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeBeginInvokePropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeBeginInvokePropertyChanged(New PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeBeginInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.PropertyChangedEvent.SafeBeginInvoke(Me, e)
    End Sub

#End Region

#Region " NOTIFICATIONS "

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    Protected Overridable Sub SafePostPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If e IsNot Nothing Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeBeginInvokePropertyChanged(e)
            Else
                SynchronizationContext.Current.Post(New SendOrPostCallback(AddressOf InvokePropertyChanged), e)
            End If
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafePostPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafePostPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

    ''' <summary>
    ''' Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes or
    ''' Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property Value.
    ''' </summary>
    ''' <param name="e"> Property Changed event information. </param>
    Protected Overridable Sub SyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If e IsNot Nothing Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeInvokePropertyChanged(e)
            Else
                SynchronizationContext.Current.Send(New SendOrPostCallback(AddressOf InvokePropertyChanged), e)
            End If
        End If
    End Sub

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SyncNotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SyncNotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#End Region

End Class

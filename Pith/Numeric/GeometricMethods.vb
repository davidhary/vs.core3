﻿Imports System.Runtime.CompilerServices
Namespace NumericExtensions

    ''' <summary> Includes extensions for <see cref="Random">random number generation</see>. </summary>
    ''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/14/2013" by="David" revision="2.0.5066.x"> Created. </history>
    Public Module Methods

#Region " HYPOTENEUSE "

        ''' <summary> Computes the length of a right triangle's hypotenuse. </summary>
        ''' <remarks> <para>The length is computed accurately, even in cases where x<sup>2</sup> or
        ''' y<sup>2</sup> would overflow.</para> </remarks>
        ''' <param name="value">      The length of one side. </param>
        ''' <param name="orthogonal"> The length of orthogonal side. </param>
        ''' <returns> The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>). </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal value As Single, ByVal orthogonal As Single) As Double
            Return Hypotenuse(CDbl(value), CDbl(orthogonal))
        End Function

        ''' <summary> Computes the length of a right triangle's hypotenuse. </summary>
        ''' <remarks> <para>The length is computed accurately, even in cases where x<sup>2</sup> or
        ''' y<sup>2</sup> would overflow.</para> </remarks>
        ''' <param name="value">      The length of one side. </param>
        ''' <param name="orthogonal"> The length of orthogonal side. </param>
        ''' <returns> The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>). </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal value As Double, ByVal orthogonal As Double) As Double
            If (value = 0.0) AndAlso (orthogonal = 0.0) Then
                Return (0.0)
            Else
                Dim ax As Double = Math.Abs(value)
                Dim ay As Double = Math.Abs(orthogonal)
                If ax > ay Then
                    Dim r As Double = orthogonal / value
                    Return (ax * Math.Sqrt(1.0 + r * r))
                Else
                    Dim r As Double = value / orthogonal
                    Return (ay * Math.Sqrt(1.0 + r * r))
                End If
            End If
        End Function

        ''' <summary> Compute the distance between the array and the zero array. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="polygon"> The sides of the polygon. </param>
        ''' <returns> The distance between the two arrays. </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal polygon As IEnumerable(Of Double)) As Double
            If polygon Is Nothing Then Throw New ArgumentNullException(NameOf(polygon))
            Dim d As Double = 0
            For Each p As Double In polygon
                d += Math.Pow(p, 2D)
            Next
            d = Math.Sqrt(d)
            Return d
        End Function

        ''' <summary> Compute the distance between two polygons. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="polygon"> The polygon. </param>
        ''' <param name="other">   The other polygon. </param>
        ''' <returns> The distance between the two arrays. </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal polygon As IEnumerable(Of Double), ByVal other As IEnumerable(Of Double)) As Double
            If polygon Is Nothing Then Throw New ArgumentNullException(NameOf(polygon))
            If other Is Nothing Then Throw New ArgumentNullException(NameOf(other))
            Dim d As Double = 0
            For j As Integer = 0 To polygon.Count - 1
                d += Math.Pow((polygon(j) - other(j)), 2D)
            Next j
            d = Math.Sqrt(d)
            Return d
        End Function

        ''' <summary> Compute the distance between the point and the origin. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="point"> The point. </param>
        ''' <returns> The distance between a point and the origin. </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal point As PointF) As Double
            If point Is Nothing Then Throw New ArgumentNullException(NameOf(point))
            Return Hypotenuse(point.X, point.Y)
        End Function

        ''' <summary> Compute the distance between the point and the origin. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="point"> The point. </param>
        ''' <returns> The distance between a point and the origin. </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal point As PointR) As Double
            If point Is Nothing Then Throw New ArgumentNullException(NameOf(point))
            Return Hypotenuse(point.X, point.Y)
        End Function

        ''' <summary> Compute the distance between the point and the origin. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="point"> The point. </param>
        ''' <returns> The distance between a point and the origin. </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal point As System.Windows.Point) As Double
            Return Hypotenuse(point.X, point.Y)
        End Function

        ''' <summary> Computes the distance between two points. </summary>
        ''' <remarks>
        ''' <para>The length is computed accurately, even in cases where x<sup>2</sup> or y<sup>2</sup>
        ''' would overflow.</para>
        ''' </remarks>
        ''' <param name="left">  The left. </param>
        ''' <param name="right"> The right. </param>
        ''' <returns>
        ''' The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>).
        ''' </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal left As System.Windows.Point, ByVal right As System.Windows.Point) As Double
            Return Hypotenuse(right.X - left.X, right.Y - left.Y)
        End Function

#End Region


    End Module

End Namespace

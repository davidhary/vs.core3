﻿Imports System.Runtime.CompilerServices
Namespace NumericExtensions

    ''' <summary> Hexadecimal extension methods. </summary>
    ''' <license>
    ''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    Public Module HexMethods

#Region " BYTE "

        ''' <summary> Returns an HEX string. </summary>
        ''' <param name="value">       Extended value. </param>
        ''' <param name="nibbleCount"> Number of nibbles. </param>
        ''' <returns> An HEX string. </returns>
        <Extension()>
        Public Function ToHex(ByVal value As Byte, ByVal nibbleCount As Byte) As String
            Return String.Format(String.Format("0x{{0:X{0}}}", nibbleCount), value)
        End Function

        ''' <summary> Returns an HEX string caption with preceding "0x". </summary>
        ''' <param name="value">       Extended value. </param>
        ''' <param name="nibbleCount"> Number of nibbles. </param>
        ''' <returns> An HEX string caption with preceding "0x". </returns>
        <Extension()>
        Public Function ToHexCaption(ByVal value As Byte, ByVal nibbleCount As Byte) As String
            Return String.Format(String.Format("0x{{0:X{0}}}", nibbleCount), value)
        End Function

#End Region

#Region " INTEGER "

        ''' <summary> Returns an HEX string. </summary>
        ''' <param name="value">       Extended value. </param>
        ''' <param name="nibbleCount"> Number of nibbles. </param>
        <Extension()>
        Public Function ToHex(ByVal value As Integer, ByVal nibbleCount As Integer) As String
            Return String.Format(String.Format("0x{{0:X{0}}}", nibbleCount), value)
        End Function

        ''' <summary> Returns an HEX string caption with preceding "0x". </summary>
        ''' <param name="value">       Extended value. </param>
        ''' <param name="nibbleCount"> Number of nibbles. </param>
        <Extension()>
        Public Function ToHexCaption(ByVal value As Integer, ByVal nibbleCount As Integer) As String
            Return String.Format(String.Format("0x{{0:X{0}}}", nibbleCount), value)
        End Function

#End Region

#Region " LONG "

        ''' <summary> Returns an HEX string. </summary>
        ''' <param name="value">       Extended value. </param>
        ''' <param name="nibbleCount"> Number of nibbles. </param>
        <Extension()>
        Public Function ToHex(ByVal value As Long, ByVal nibbleCount As Long) As String
            Return String.Format(String.Format("0x{{0:X{0}}}", nibbleCount), value)
        End Function

        ''' <summary> Returns an HEX string caption with preceding "0x". </summary>
        ''' <param name="value">       Extended value. </param>
        ''' <param name="nibbleCount"> Number of nibbles. </param>
        <Extension()>
        Public Function ToHexCaption(ByVal value As Long, ByVal nibbleCount As Long) As String
            Return String.Format(String.Format("0x{{0:X{0}}}", nibbleCount), value)
        End Function

#End Region

#Region " SHORT "

        ''' <summary> Returns an HEX string. </summary>
        ''' <param name="value">       Extended value. </param>
        ''' <param name="nibbleCount"> Number of nibbles. </param>
        <Extension()>
        Public Function ToHex(ByVal value As Short, ByVal nibbleCount As Short) As String
            Return String.Format(String.Format("0x{{0:X{0}}}", nibbleCount), value)
        End Function

        ''' <summary> Returns an HEX string caption with preceding "0x". </summary>
        ''' <param name="value">       Extended value. </param>
        ''' <param name="nibbleCount"> Number of nibbles. </param>
        ''' <returns> The caption. </returns>
        <Extension()>
        Public Function ToHexCaption(ByVal value As Short, ByVal nibbleCount As Short) As String
            Return String.Format(String.Format("0x{{0:X{0}}}", nibbleCount), value)
        End Function

#End Region


    End Module
End Namespace

﻿Imports System.Runtime.CompilerServices
Namespace NumericExtensions

    ''' <summary> Bit extension methods. </summary>
    ''' <license>
    ''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    Public Module BitMethods

        ''' <summary> Queries if 'status' bit is on. </summary>
        ''' <param name="status"> The status. </param>
        ''' <param name="bit">    The bit. </param>
        ''' <returns> <c>true</c> if bit; otherwise <c>false</c> </returns>
        <Extension>
        Public Function IsBit(ByVal status As Long, ByVal bit As Integer) As Boolean
            Return (1L And (status >> bit)) = 1L
        End Function

        ''' <summary> Toggles the bits specified in the <paramref name="bit"/> based on the
        ''' <paramref name="switch"/>. </summary>
        ''' <param name="bit">  The value. </param>
        ''' <param name="switch"> True to set; otherwise, clear. </param>
        <Extension>
        Public Function Toggle(ByVal status As Long, ByVal bit As Integer, ByVal switch As Boolean) As Long
            If switch Then
                status = status Or (1L << bit)
            Else
                status = status And (Not (1L << bit))
            End If
            Return status
        End Function

    End Module

End Namespace


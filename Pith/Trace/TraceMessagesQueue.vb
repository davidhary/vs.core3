﻿Imports System.Collections.Concurrent
Imports isr.Core.Pith.EventHandlerExtensions
Imports isr.Core.Pith.StackTraceExtensions
''' <summary> Queue of trace messages. </summary>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/23/2018" by="David" revision=""> Created. </history>
<CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix")>
Public Class TraceMessagesQueue

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor for this class. </summary>
    Public Sub New()
        MyBase.New()
        Me._ContentsQueue = New ConcurrentQueue(Of TraceMessage)
    End Sub

#End Region

#Region " QUEUE "

    Protected ReadOnly Property ContentsQueue As ConcurrentQueue(Of TraceMessage)

    ''' <summary> Gets contents queue. </summary>
    ''' <returns> The contents queue. </returns>
    Private Function GetContentsQueue() As ConcurrentQueue(Of TraceMessage)
        Return Me._ContentsQueue
    End Function

    ''' <summary> Gets the content. </summary>
    ''' <returns> The content. </returns>
    Public Function TryDequeue() As TraceMessage
        Dim result As TraceMessage = Nothing
        Me.GetContentsQueue.TryDequeue(result)
        Return result
    End Function

    ''' <summary> Query if the contents queue is empty. </summary>
    ''' <returns> <c>true</c> the contents queue is empty; otherwise <c>false</c> </returns>
    Public Function IsEmpty() As Boolean
        Return Me.GetContentsQueue.IsEmpty
    End Function

    ''' <summary> Fetches the content. </summary>
    ''' <returns> The content. </returns>
    Public Function FetchContent() As String
        Return Me.DequeueContent
    End Function

    ''' <summary> Fetches the content. </summary>
    ''' <returns> The content. </returns>
    Public Function DequeueContent() As String
        Dim messageNumber As Integer = 0
        Dim builder As New System.Text.StringBuilder
        Dim value As TraceMessage = Nothing
        Do
            value = Me.TryDequeue()
            If value IsNot Nothing Then
                messageNumber += 1
                builder.AppendFormat("{0}:>", messageNumber)
                builder.AppendLine(value.ToString)
            End If
        Loop Until Me.GetContentsQueue.IsEmpty
        Return builder.ToString.TrimEnd(Environment.NewLine.ToCharArray)
    End Function

#End Region

#Region " ENQUEUE "

    ''' <summary> Event queue for all listeners interested in <see cref="MessageEnqueued"/> events. </summary>
    Public Event MessageEnqueued As EventHandler(Of System.EventArgs)

    ''' <summary> Posts the message enqueued event. </summary>
    Private Sub PostMessageEnqueued()
        Dim evt As EventHandler(Of System.EventArgs) = Me.MessageEnqueuedEvent
        evt?.SafePost(Me, System.EventArgs.Empty)
    End Sub

    ''' <summary> Adds a message. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String. </returns>
    Protected Function Enqueue(ByVal value As String) As String
        If String.IsNullOrWhiteSpace(value) Then value = ""
        Me.Enqueue(New TraceMessage(TraceEventType.Information, My.MyLibrary.TraceEventId, value))
        Return value
    End Function

    ''' <summary> Adds a message. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub Enqueue(ByVal value As TraceMessage)
        If value Is Nothing OrElse String.IsNullOrWhiteSpace(value.Details) Then
            Debug.Assert(Not Debugger.IsAttached, "Empty trace message", New StackTrace(True).UserCallStack(0, 0))
        Else
            Dim lastCount As Long = CType(Me.GetContentsQueue?.LongCount, Long?).GetValueOrDefault(0)
            Me.GetContentsQueue()?.Enqueue(value)
            If CType(Me.GetContentsQueue?.LongCount, Long?).GetValueOrDefault(0) > lastCount Then
                Me.PostMessageEnqueued()
            End If
        End If
    End Sub

#End Region

End Class

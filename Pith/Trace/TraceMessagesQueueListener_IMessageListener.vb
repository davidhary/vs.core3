﻿Partial Public Class TraceMessagesQueueListener
    Implements IMessageListener

#Region " I MESSAGE LISTENER "

    ''' <summary> Gets the is disposed. </summary>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <value> The is disposed. </value>
    Public ReadOnly Property IsDisposed As Boolean Implements IMessageListener.IsDisposed

    ''' <summary> Writes. </summary>
    ''' <param name="message"> The message to add. </param>
    Public Sub Write(message As String) Implements IMessageListener.Write
        Me.Enqueue(message)
    End Sub

    ''' <summary> Writes a line. </summary>
    ''' <param name="message"> The message to add. </param>
    Public Sub WriteLine(message As String) Implements IMessageListener.WriteLine
        Me.Enqueue(message)
    End Sub

    ''' <summary> Registers this event. </summary>
    ''' <param name="level"> The level. </param>
    Public Overridable Sub Register(level As TraceEventType) Implements IMessageListener.Register
    End Sub

    ''' <summary> Gets the trace level. </summary>
    ''' <value> The trace level. </value>
    Public Overridable Property TraceLevel As TraceEventType Implements IMessageListener.TraceLevel

    ''' <summary> Checks if the log should trace the event type. </summary>
    ''' <param name="value"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns> <c>True</c> If the log should trace; <c>False</c> otherwise. </returns>
    Public Function ShouldTrace(ByVal value As TraceEventType) As Boolean Implements IMessageListener.ShouldTrace
        Return value <= Me.TraceLevel
    End Function

    ''' <summary> Gets the sentinel indicating this as thread safe. </summary>
    ''' <value> True if thread safe. </value>
    Public Overridable ReadOnly Property IsThreadSafe As Boolean Implements IMessageListener.IsThreadSafe
        Get
            Return True
        End Get
    End Property

    ''' <summary> Gets or sets a unique identifier. </summary>
    ''' <value> The identifier of the unique. </value>
    Protected ReadOnly Property UniqueId As Guid = Guid.NewGuid Implements IMessageListener.UniqueId

    ''' <summary> Tests if this ITraceMessageListener is considered equal to another. </summary>
    ''' <param name="other"> The i trace message listener to compare to this object. </param>
    ''' <returns>
    ''' <c>true</c> if the objects are considered equal, <c>false</c> if they are not.
    ''' </returns>
    Public Overloads Function Equals(other As IMessageListener) As Boolean Implements IEquatable(Of IMessageListener).Equals
        If other Is Nothing Then
            Return False
        Else
            Return Guid.Equals(other.UniqueId, Me.UniqueId)
        End If
    End Function

    Private Sub _applyTraceLevel(ByVal value As TraceEventType)
        Me._TraceLevel = value
    End Sub

    ''' <summary> Applies the trace level. </summary>
    ''' <param name="value"> The <see cref="TraceEventType">trace level</see> value. </param>
    Public Overloads Sub ApplyTraceLevel(ByVal value As TraceEventType) Implements IMessageListener.ApplyTraceLevel
        Me._applyTraceLevel(value)
    End Sub

    ''' <summary> Gets the type of the listener. </summary>
    ''' <value> The type of the listener. </value>
    Public ReadOnly Property ListenerType As ListenerType Implements IMessageListener.ListenerType
        Get
            Return ListenerType.Display
        End Get
    End Property

#End Region

End Class

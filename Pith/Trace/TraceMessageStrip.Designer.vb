﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TraceMessageStrip

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TraceMessageStrip))
        Me._ToolStrip = New System.Windows.Forms.ToolStrip()
        Me._DetailsComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._ImageLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ClearHistoryButton = New System.Windows.Forms.ToolStripButton()
        Me._ClearButton = New System.Windows.Forms.ToolStripButton()
        Me._MessageLevelSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._DropLastMessageMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ClearHistoryMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ToolStrip
        '
        Me._ToolStrip.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ImageLabel, Me._DetailsComboBox, Me._ClearHistoryButton, Me._ClearButton, Me._MessageLevelSplitButton})
        Me._ToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._ToolStrip.Name = "_ToolStrip"
        Me._ToolStrip.Size = New System.Drawing.Size(799, 29)
        Me._ToolStrip.TabIndex = 3
        Me._ToolStrip.Text = "Trace Message Strip"
        '
        '_DetailsComboBox
        '
        Me._DetailsComboBox.Name = "_DetailsComboBox"
        Me._DetailsComboBox.Size = New System.Drawing.Size(121, 29)
        Me._DetailsComboBox.ToolTipText = "Message details"
        '
        '_ImageLabel
        '
        Me._ImageLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ImageLabel.Image = Global.isr.Core.Pith.My.Resources.Resources.VerboseLevel
        Me._ImageLabel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._ImageLabel.Name = "_ImageLabel"
        Me._ImageLabel.Size = New System.Drawing.Size(22, 26)
        Me._ImageLabel.ToolTipText = "Alert level"
        '
        '_ClearHistoryButton
        '
        Me._ClearHistoryButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._ClearHistoryButton.AutoToolTip = False
        Me._ClearHistoryButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ClearHistoryButton.Image = CType(resources.GetObject("_ClearHistoryButton.Image"), System.Drawing.Image)
        Me._ClearHistoryButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._ClearHistoryButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ClearHistoryButton.Name = "_ClearHistoryButton"
        Me._ClearHistoryButton.Size = New System.Drawing.Size(26, 26)
        Me._ClearHistoryButton.ToolTipText = "Clear history"
        '
        '_ClearButton
        '
        Me._ClearButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._ClearButton.AutoToolTip = False
        Me._ClearButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ClearButton.Image = CType(resources.GetObject("_ClearButton.Image"), System.Drawing.Image)
        Me._ClearButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._ClearButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ClearButton.Name = "_ClearButton"
        Me._ClearButton.Size = New System.Drawing.Size(26, 26)
        Me._ClearButton.Text = "Clear"
        Me._ClearButton.ToolTipText = "Clear last message"
        '
        '_MessageLevelSplitButton
        '
        Me._MessageLevelSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._MessageLevelSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._DropLastMessageMenuItem, Me._ClearHistoryMenuItem})
        Me._MessageLevelSplitButton.Image = Global.isr.Core.Pith.My.Resources.Resources.Empty
        Me._MessageLevelSplitButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._MessageLevelSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._MessageLevelSplitButton.Name = "_MessageLevelSplitButton"
        Me._MessageLevelSplitButton.Size = New System.Drawing.Size(38, 26)
        Me._MessageLevelSplitButton.Text = "ToolStripSplitButton1"
        '
        '_DropLastMessageMenuItem
        '
        Me._DropLastMessageMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._DropLastMessageMenuItem.Image = Global.isr.Core.Pith.My.Resources.Resources.Clear
        Me._DropLastMessageMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._DropLastMessageMenuItem.Name = "_DropLastMessageMenuItem"
        Me._DropLastMessageMenuItem.Size = New System.Drawing.Size(158, 28)
        Me._DropLastMessageMenuItem.ToolTipText = "Drops last message"
        '
        '_ClearHistoryMenuItem
        '
        Me._ClearHistoryMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ClearHistoryMenuItem.Image = Global.isr.Core.Pith.My.Resources.Resources.ClearHistory
        Me._ClearHistoryMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._ClearHistoryMenuItem.Name = "_ClearHistoryMenuItem"
        Me._ClearHistoryMenuItem.Size = New System.Drawing.Size(158, 28)
        Me._ClearHistoryMenuItem.ToolTipText = "Clear History"
        '
        'TraceMessageStrip
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._ToolStrip)
        Me.Name = "TraceMessageStrip"
        Me.Size = New System.Drawing.Size(799, 153)
        Me._ToolStrip.ResumeLayout(False)
        Me._ToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _ToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _ClearButton As Windows.Forms.ToolStripButton
    Private WithEvents _ImageLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _DetailsComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _ClearHistoryButton As Windows.Forms.ToolStripButton
    Private WithEvents _MessageLevelSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _DropLastMessageMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ClearHistoryMenuItem As Windows.Forms.ToolStripMenuItem
End Class

﻿Imports System.Drawing
Imports System.Windows.Forms
''' <summary> A trace message tool strip. </summary>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/21/2017" by="David" revision=""> Created. </history>
Public Class TraceMessageToolStrip
    Inherits Windows.Forms.ToolStrip

#Region " CONSTRUCTION "

    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Property InitializingComponent As Boolean
    Public Sub New()
        MyBase.New
        Me.InitializingComponent = True
        Me.InitializeComponent()
        Me.InitializingComponent = False
        Me.Renderer = New BackgroundRenderer
        Me._Stack = New Stack(Of TraceMessage)
        Me.ApplyTraceEvent(TraceEventType.Start)
    End Sub

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    Private WithEvents _DetailsComboBox As isr.Core.Pith.ToolStripSpringComboBox
    Private WithEvents _MessageLevelSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _DropLastMessageMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ClearHistoryMenuItem As Windows.Forms.ToolStripMenuItem


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._MessageLevelSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._DropLastMessageMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ClearHistoryMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._DetailsComboBox = New isr.Core.Pith.ToolStripSpringComboBox
        Me.SuspendLayout()
        '
        '_DetailsComboBox
        '
        Me._DetailsComboBox.Name = "_DetailsComboBox"
        Me._DetailsComboBox.DropDownStyle = ComboBoxStyle.DropDownList
        Me._DetailsComboBox.Size = New System.Drawing.Size(121, 29)
        Me._DetailsComboBox.ToolTipText = "Message details"
        Me._DetailsComboBox.AutoSize = True
        '
        '_MessageLevelSplitButton
        '
        Me._MessageLevelSplitButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._MessageLevelSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._MessageLevelSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._DropLastMessageMenuItem, Me._ClearHistoryMenuItem})
        Me._MessageLevelSplitButton.Image = Global.isr.Core.Pith.My.Resources.Resources.Empty
        Me._MessageLevelSplitButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._MessageLevelSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._MessageLevelSplitButton.Name = "_MessageLevelSplitButton"
        Me._MessageLevelSplitButton.Size = New System.Drawing.Size(38, 26)
        Me._MessageLevelSplitButton.AutoSize = False
        Me._MessageLevelSplitButton.ToolTipText = "Message level; Clear menu"
        '
        '_DropLastMessageMenuItem
        '
        Me._DropLastMessageMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._DropLastMessageMenuItem.Image = Global.isr.Core.Pith.My.Resources.Resources.Clear
        Me._DropLastMessageMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._DropLastMessageMenuItem.Name = "_DropLastMessageMenuItem"
        Me._DropLastMessageMenuItem.Size = New System.Drawing.Size(158, 28)
        Me._DropLastMessageMenuItem.ToolTipText = "Drops last message"
        '
        '_ClearHistoryMenuItem
        '
        Me._ClearHistoryMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ClearHistoryMenuItem.Image = Global.isr.Core.Pith.My.Resources.Resources.ClearHistory
        Me._ClearHistoryMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._ClearHistoryMenuItem.Name = "_ClearHistoryMenuItem"
        Me._ClearHistoryMenuItem.Size = New System.Drawing.Size(158, 28)
        Me._ClearHistoryMenuItem.ToolTipText = "Clear History"
        '
        'TraceMessageToolStrip
        '
        Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.Stretch = True
        Me.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._DetailsComboBox, Me._MessageLevelSplitButton})
        Me.Size = New System.Drawing.Size(799, 29)
        Me.Text = "Trace Message Strip"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

#Region " BACKGROUND RENDERER "

    ''' <summary> A background renderer. </summary>
    ''' <license>
    ''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="5/5/2017" by="David" revision=""> Created. </history>
    Private Class BackgroundRenderer
        Inherits ToolStripProfessionalRenderer
        Protected Overrides Sub OnRenderLabelBackground(ByVal e As ToolStripItemRenderEventArgs)
            If e IsNot Nothing AndAlso (e.Item.BackColor <> System.Drawing.SystemColors.ControlDark) Then
                Using brush As New SolidBrush(e.Item.BackColor)
                    e.Graphics.FillRectangle(brush, e.Item.ContentRectangle)
                End Using
            End If
        End Sub
    End Class

#End Region

#Region " STACK "

    Private _Stack As Stack(Of TraceMessage)

    ''' <summary> Returns the top-of-stack object without removing it. </summary>
    ''' <returns> The current top-of-stack object. </returns>
    Public Function Peek() As TraceMessage
        If Me._Stack.Any Then
            Return Me._Stack.Peek()
        Else
            Return TraceMessage.Empty
        End If
    End Function

    ''' <summary> Removes and returns the top-of-stack object. </summary>
    Public Sub Pop()
        If Me._Stack.Any Then
            Me._Stack.Pop()
            Me.OnStackChanged(System.EventArgs.Empty)
        End If
    End Sub

    ''' <summary> Pushes an object onto this stack. </summary>
    ''' <param name="message"> The message to push. </param>
    Public Sub Push(ByVal message As TraceMessage)
        Me._Stack.Push(message)
        Me.OnStackChanged(System.EventArgs.Empty)
    End Sub

    ''' <summary> Pushes an object onto this stack. </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="message">   The message to push. </param>
    Public Sub Push(ByVal eventType As TraceEventType, ByVal message As String)
        Me.Push(New TraceMessage(eventType, 0, message))
    End Sub

#End Region

#Region " DISPLAY "

    Private _LastEventType As TraceEventType

    Private Sub ApplyTraceEvent(value As TraceEventType)
        Me._LastEventType = value
        Select Case value
            Case Is <= TraceEventType.Error
                Me._MessageLevelSplitButton.Image = My.Resources.ErrorLevel
            Case Is <= TraceEventType.Warning
                Me._MessageLevelSplitButton.Image = My.Resources.WarningLevel
            Case Is <= TraceEventType.Information
                Me._MessageLevelSplitButton.Image = My.Resources.InformationLevel
            Case Is <= TraceEventType.Verbose
                Me._MessageLevelSplitButton.Image = My.Resources.VerboseLevel
            Case Else
                Me._MessageLevelSplitButton.Image = My.Resources.Empty
        End Select
        Me._MessageLevelSplitButton.Invalidate()
        Windows.Forms.Application.DoEvents()
    End Sub


    ''' <summary> Gets or sets the type of the last event. </summary>
    ''' <value> The type of the last event. </value>
    <System.ComponentModel.Browsable(False),
        System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)>
    Private Property LastEventType As TraceEventType
        Get
            Return Me._LastEventType
        End Get
        Set(value As TraceEventType)
            If value <> Me.LastEventType Then '
                Me.ApplyTraceEvent(value)
            End If
        End Set
    End Property

    ''' <summary> Displays the given message. </summary>
    ''' <param name="message"> The message to push. </param>
    Private Sub Display(ByVal message As TraceMessage)
        Me._DetailsComboBox.Text = message.Details
        Me._DetailsComboBox.Invalidate()
        Me.LastEventType = message.EventType
    End Sub

    Public Sub ApplyBackColor(ByVal color As Color)
        Me._DetailsComboBox.BackColor = color
    End Sub

#End Region

#Region " CLEAR "

    ''' <summary> Handles the stack changed event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnStackChanged(ByVal e As EventArgs)
        If e Is Nothing Then Return
        If Me._Stack.Any Then
            Me._DetailsComboBox.ComboBox.DataSource = Me._Stack.ToArray
            Me.Display(Me._Stack.Peek)
        Else
            Me.LastEventType = TraceEventType.Start
            Me._DetailsComboBox.ComboBox.DataSource = Nothing
            Me._DetailsComboBox.ComboBox.Items.Clear()
            Me._DetailsComboBox.Text = ""
        End If
        Me._DetailsComboBox.Invalidate()
    End Sub

    ''' <summary> Drops the last message. </summary>
    Public Sub DropLastMessage()
        Me.Pop()
    End Sub

    ''' <summary> Clears the history. </summary>
    Public Sub ClearHistory()
        Me._Stack.Clear()
        Me.OnStackChanged(System.EventArgs.Empty)
    End Sub

    Private Sub _ClearHistoryMenuItem_Click(sender As Object, ByVal e As EventArgs) Handles _ClearHistoryMenuItem.Click
        Me.ClearHistory()
    End Sub

    Private Sub _DropLastMessageMenuItem_Click(sender As Object, e As EventArgs) Handles _DropLastMessageMenuItem.Click
        Me.DropLastMessage()
    End Sub

#End Region

End Class

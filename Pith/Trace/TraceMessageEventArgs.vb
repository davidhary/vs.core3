﻿Imports System.Windows.Forms

''' <summary>
''' Defines an event arguments class for <see cref="TraceMessage">trace messages</see>.
''' </summary>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class TraceMessageEventArgs

    Inherits System.EventArgs

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="TraceMessageEventArgs" /> class. </summary>
    ''' <param name="traceMessage"> A message describing the trace. </param>
    Public Sub New(ByVal traceMessage As TraceMessage)
        MyBase.New()
        Me.TraceMessage = traceMessage
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TraceMessageEventArgs" /> class.
    ''' </summary>
    ''' <param name="details">The details.</param>
    Public Sub New(ByVal id As Integer, ByVal details As String)
        Me.New(Diagnostics.TraceEventType.Information, id, details)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TraceMessageEventArgs" /> class.
    ''' </summary>
    ''' <param name="severity">The severity.</param>
    ''' <param name="details">The details.</param>
    Public Sub New(ByVal severity As Diagnostics.TraceEventType, ByVal id As Integer, ByVal details As String)
        Me.New(New TraceMessage(severity, id, details))
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TraceMessageEventArgs" /> class.
    ''' </summary>
    ''' <param name="severity">The severity.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The arguments for the format statement.</param>
    Public Sub New(ByVal severity As Diagnostics.TraceEventType, ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(New TraceMessage(severity, id, format, args))
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets a message describing the trace. </summary>
    ''' <value> A message describing the trace. </value>
    Public Property TraceMessage As TraceMessage

    ''' <summary>
    ''' Gets the message severity.
    ''' </summary>
    Public ReadOnly Property Severity As Diagnostics.TraceEventType
        Get
            Return Me.TraceMessage.EventType
        End Get
    End Property

    ''' <summary>
    ''' Gets the message details
    ''' </summary>
    Public ReadOnly Property Details As String
        Get
            Return Me.TraceMessage.Details
        End Get
    End Property

#End Region

#Region " DISPLAY "

    ''' <summary>
    ''' Displays the message.
    ''' </summary>
    ''' <param name="e">The <see cref="TraceMessageEventArgs" /> instance containing the event data.</param>
    Public Shared Sub DisplayMessage(e As TraceMessageEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        TraceMessage.DisplayMessage(e.TraceMessage)
    End Sub

#End Region

End Class

﻿Imports System.Drawing.Drawing2D
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Drawing
''' <summary> A trace message box. </summary>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/20/2017" by="David" revision=""> Created. </history>
Public Class TraceMessageStrip
    Inherits Pith.MyUserControlBase

#Region " CONSTRUCTION "

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        Me._stack = New Stack(Of TraceMessage)

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Controls.DropDownTextBox and optionally
    ''' releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
                If Me._stack IsNot Nothing Then Me._stack.Clear()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " STACK "

    Private _Stack As Stack(Of TraceMessage)

    Public Sub Push(ByVal message As TraceMessage)
        Me._stack.Push(message)
        Me.Display(message)
    End Sub

#End Region

#Region " DISPLAY "

    Private _LastEventType As TraceEventType

    ''' <summary> Gets or sets the type of the last event. </summary>
    ''' <value> The type of the last event. </value>
    <System.ComponentModel.Browsable(False),
        System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)>
    Private Property LastEventType As TraceEventType
        Get
            Return Me._LastEventType
        End Get
        Set(value As TraceEventType)
            If value <> Me.LastEventType Then '
                Me._LastEventType = value
                Select Case value
                    Case Is <= TraceEventType.Error
                        Me._ImageLabel.Image = My.Resources.ErrorLevel
                    Case Is <= TraceEventType.Warning
                        Me._ImageLabel.Image = My.Resources.WarningLevel
                    Case Is <= TraceEventType.Information
                        Me._ImageLabel.Image = My.Resources.InformationLevel
                    Case Is <= TraceEventType.Verbose
                        Me._ImageLabel.Image = My.Resources.VerboseLevel
                End Select
                Me._ImageLabel.Invalidate()
            End If
        End Set
    End Property

    Private Sub Display(ByVal message As TraceMessage)
        Me._DetailsComboBox.Text = message.Details
        Me._DetailsComboBox.Invalidate()
        Me.LastEventType = message.EventType
    End Sub

#End Region

#Region " CLEAR "

    Public Sub ClearLastMessage()
        Me._stack.Pop()
        If Me._stack.Any Then
            Me.Display(Me._stack.Peek)
        Else
            Me._ImageLabel.Image = Nothing
            Me._DetailsComboBox.Text = ""
            Me._DetailsComboBox.Invalidate()
        End If
        Me._ImageLabel.Invalidate()
    End Sub

    Public Sub ClearAllMessages()
        Me._stack.Clear()
        Me._ImageLabel.Image = Nothing
        Me._DetailsComboBox.Text = ""
        Me._DetailsComboBox.Invalidate()
    End Sub


    Private Sub _ClearAllButton_Click(sender As Object, e As EventArgs)
        Me.ClearLastMessage()
    End Sub

    Private Sub _ClearMessageButton_Click(sender As Object, e As EventArgs)
        Me.ClearLastMessage()
    End Sub

#End Region

End Class

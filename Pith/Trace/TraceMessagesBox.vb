Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Core.Pith
''' <summary> A message logging text box. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/04/2013" by="David" revision="1.2.4955"> created based on the legacy
''' messages box. </history>
<System.ComponentModel.Description("Trace Messages Text Box")>
Public Class TraceMessagesBox
    Inherits MessagesBox

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor for this class. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializeComponent()
        Me._TraceMessageFormat = TraceMessage.DefaultTraceMessageFormat
        Me._AlertLevel = TraceEventType.Warning
        Me._AlertAnnunciatorEvent = TraceEventType.Verbose
        Me._AlertSoundEvent = TraceEventType.Verbose
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            ' Invoke the base class dispose method        
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DISPLAY AND MESSAGE SENTINELS "

    Private _AlertLevel As TraceEventType

    ''' <summary> Gets or sets the alert level. Message <see cref="TraceEventType">levels</see> equal
    ''' or lower than this are tagged as alerts and set the <see cref="AlertAnnunciatorEvent">alert
    ''' sentinel</see>. </summary>
    ''' <value> The alert level. </value>
    <Category("Appearance"), Description("Level for notifying of alerts"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("TraceEventType.Warning")>
    Public Property AlertLevel As TraceEventType
        Get
            Return Me._AlertLevel
        End Get
        Set(value As TraceEventType)
            If value <> Me.AlertLevel Then
                Me._AlertLevel = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the sentinel indicating that alert messages need to be announced. </summary>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property AlertsAnnounced As Boolean
        Get
            Return Me.AlertAnnunciatorEvent <= Me.AlertLevel
        End Get
    End Property

    Private _AlertAnnunciatorEvent As TraceEventType
    ''' <summary> Gets or set the trace event type which toggles the alert annunciator. </summary>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property AlertAnnunciatorEvent As TraceEventType
        Get
            Return Me._AlertAnnunciatorEvent
        End Get
        Set(value As TraceEventType)
            If value <> Me.AlertAnnunciatorEvent Then
                Me._AlertAnnunciatorEvent = value
                Me.SafePostPropertyChanged()
                Me.UpdateAlertToggleControl(Me.AlertsToggleControl, value)
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the sentinel indicating that alert messages need to be voiced. </summary>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property AlertsVoiced As Boolean
        Get
            Return Me.AlertAnnunciatorEvent <= Me.AlertLevel
        End Get
    End Property

    Private _AlertSoundEvent As TraceEventType
    ''' <summary> Gets the highest alert event. </summary>
    ''' <value> The highest alert event. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property AlertSoundEvent As TraceEventType
        Get
            Return Me._AlertSoundEvent
        End Get
        Set(value As TraceEventType)
            If value <> Me.AlertSoundEvent Then
                Me._AlertSoundEvent = value
                Me.SafePostPropertyChanged()
                Me.PlayAlertIf(Me.AlertsVoiced, value)
            End If
        End Set
    End Property

    Protected Sub UpdateAlerts(ByVal level As TraceEventType)
        If level <= Me.AlertLevel Then
            ' Updates the cached alert level if the level represents a higher alert level.
            If level < Me.AlertAnnunciatorEvent Then Me.AlertAnnunciatorEvent = level
            If level < Me.AlertSoundEvent Then Me.AlertSoundEvent = level
        End If
        ' Me._AlertsAdded = Not Me.UserVisible AndAlso (Me.AlertsAdded OrElse (level <= Me.AlertLevel))
        ' 2016 moved to highest alert level: If Not Me.UserVisible AndAlso Me.AlertsAdded Then Me.PlayAlert(level)
    End Sub

    ''' <summary> Displays the available lines and clears the alerts and message sentinels. </summary>
    Public Overrides Sub Display()
        ' turn off the alert annunciator when displaying the messages onto a visible screen.
        ' the sound alert level is kept unchanged. 
        If Me.UserVisible Then Me.AlertAnnunciatorEvent = TraceEventType.Verbose
        MyBase.Display()
    End Sub

    ''' <summary> Executes the clear action. </summary>
    Protected Overrides Sub OnClear()
        Me.AlertAnnunciatorEvent = TraceEventType.Verbose
        Me.AlertSoundEvent = TraceEventType.Verbose
        MyBase.OnClear()
    End Sub

    ''' <summary> Gets or sets the alerts toggle control. </summary>
    ''' <value> The alerts toggle control. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property AlertsToggleControl As Control

    ''' <summary> Safe visible setter. </summary>
    ''' <param name="control"> The control. </param>
    Public Sub UpdateAlertToggleControl(ByVal control As Control, ByVal alertEvent As TraceEventType)
        If control IsNot Nothing Then
            If control.InvokeRequired Then
                control.Invoke(New Action(Of Control, TraceEventType)(AddressOf Me.UpdateAlertToggleControl), New Object() {control, alertEvent})
            Else
                With control
                    If alertEvent <= Me.AlertLevel Then
                        .Visible = True
                        .Text = alertEvent.ToString
                        Select Case alertEvent
                            Case <= TraceEventType.Error
                                .BackColor = Drawing.Color.Red
                            Case <= TraceEventType.Warning
                                .BackColor = Drawing.Color.Orange
                            Case Else
                                .BackColor = Drawing.Color.LightBlue
                        End Select
                    Else
                        .Visible = False
                    End If
                    .Invalidate()
                End With
            End If
        End If
    End Sub

    ''' <summary> Gets or sets the alert sound enabled. </summary>
    ''' <value> The alert sound enabled. </value>
    <Category("Appearance"), Description("Enables playing alert sounds"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(False)>
    Public Property AlertSoundEnabled As Boolean

    ''' <summary> Play alert if. </summary>
    ''' <param name="condition"> true to condition. </param>
    ''' <param name="level">     The level. </param>
    Public Sub PlayAlertIf(ByVal condition As Boolean, ByVal level As TraceEventType)
        If condition AndAlso Me.AlertSoundEnabled Then TraceMessagesBox.PlayAlert(level)
    End Sub

    ''' <summary> Play alert. </summary>
    ''' <param name="level"> The level. </param>
    Public Shared Sub PlayAlert(ByVal level As TraceEventType)
        If level = TraceEventType.Critical Then
            My.Computer.Audio.PlaySystemSound(Media.SystemSounds.Exclamation)
        ElseIf level = TraceEventType.Error Then
            My.Computer.Audio.PlaySystemSound(Media.SystemSounds.Exclamation)
        ElseIf level = TraceEventType.Warning Then
            My.Computer.Audio.PlaySystemSound(Media.SystemSounds.Asterisk)
        End If
    End Sub

#End Region

#Region " ADD MESSAGE "

    ''' <summary> Prepends or appends a new value to the messages box. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String. </returns>
    Public Overrides Function AddMessage(ByVal value As String) As String
        If String.IsNullOrWhiteSpace(value) Then
            Debug.Assert(Not Debugger.IsAttached, "Empty trace message @",
                         New StackTrace(True).ToString.Split(Environment.NewLine.ToCharArray())(0))
            value = ""
        Else
            MyBase.AddMessage(value)
        End If
        Return value
    End Function

    ''' <summary> Adds a message to the display. </summary>
    ''' <param name="value">  The value. </param>
    Public Overloads Sub AddMessage(ByVal value As TraceMessage)
        If value Is Nothing OrElse String.IsNullOrWhiteSpace(value.Details) Then
            Debug.Assert(Not Debugger.IsAttached, "Empty trace message @",
                         New StackTrace(True).ToString.Split(Environment.NewLine.ToCharArray())(0))
        Else
            ' sustains the alerts flag until cleared.
            Me.UpdateAlerts(value.EventType)
            Me.AddMessage(value.ToString(Me.TraceMessageFormat))
            Me.PublishSynopsis(value)
        End If
    End Sub

    ''' <summary> Gets or sets the default format for displaying the message. </summary>
    ''' <remarks> The format must include 4 elements to display the first two characters of
    ''' the trace event type, the id, timestamp and message details. For example,<code>
    ''' "{0}, {1:X}, {2:HH:mm:ss.fff}, {3}"</code>. </remarks>
    ''' <value> The trace message format. </value>
    <Category("Appearance"), Description("Formats the test message"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(TraceMessage.DefaultTraceMessageFormat)>
    Public Property TraceMessageFormat As String

#End Region

#Region " STATUS PUBLISHER "

    Private _StatusPrompt As String
    ''' <summary> Gets or sets the status prompt. </summary>
    ''' <value> The status bar label. </value>
    Public ReadOnly Property StatusPrompt As String
        Get
            Return Me._statusPrompt
        End Get
    End Property

    ''' <summary> Gets the length of the maximum synopsis. </summary>
    ''' <value> The length of the maximum synopsis. </value>
    <Category("Data"), Description("Maximum synopsis message length"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(0)>
    Public Property MaxSynopsisLength As Integer

    ''' <summary> Publishes status. </summary>
    Public Sub PublishSynopsis(ByVal traceMessage As TraceMessage)
        If traceMessage Is Nothing Then Return
        Dim value As String = TraceMessage.ExtractSynopsis(traceMessage.Details,
                                                           TraceMessage.DefaultSynopsisDelimiter, Me.MaxSynopsisLength)
        If Not String.IsNullOrWhiteSpace(value) Then
            Me.PublishStatus(value)
            System.Windows.Forms.Application.DoEvents()
        End If
    End Sub

    ''' <summary> Publishes the trace message status. </summary>
    Public Sub PublishStatus(ByVal value As String)
        If String.IsNullOrWhiteSpace(value) Then value = ""
        If Not String.Equals(value, Me.StatusPrompt, StringComparison.CurrentCulture) Then
            Me._StatusPrompt = value
            Me.SafePostPropertyChanged(NameOf(TraceMessagesBox.StatusPrompt))
        End If
    End Sub

#End Region

#Region " I MESSAGE LISTENER "

    ''' <summary> Registers this trace level and updates alerts. </summary>
    ''' <param name="level"> The level. </param>
    Public Overrides Sub Register(level As TraceEventType) Implements IMessageListener.Register
        MyBase.Register(level)
        Me.UpdateAlerts(level)
    End Sub

#End Region

End Class


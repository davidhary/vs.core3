Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Namespace EventHandlerExtensions

    Partial Public Module Methods

#Region " SAFE INVOKES "

        ''' <summary> Synchronously notifies an <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
        ''' <param name="handler"> The event handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <remarks>
        ''' <para>Test Results with 10 ms sleep on the target:</para>
        ''' <para>Safe Invoke:         invoke: .16-.24 ms; Regain Control: 4.8 - 5.0 ms. </para>
        ''' <para>Safe Begin Invoke:   invoke: .16-.24 ms; Regain Control:  .24-  .29 ms. </para>
        ''' <para>Dynamic Invoke:      Invoke:     1.6 ms; Regain Control: 4.8 - 6.2 ms. </para>
        ''' <para>Invoke:              Invoke:     1.6 ms; Regain Control: 4.3 - 6.7 ms. </para>
        ''' <para>Target Begin Invoke: Invoke:     1.6 ms; Regain Control:        .24 ms. </para>
        ''' <para>Conclusion: Using safe invoke has little if any negative effects. </para>
        '''          </remarks>
        <Extension>
        Public Sub SafeInvoke(ByVal handler As EventHandler(Of System.EventArgs), ByVal sender As Object)
            handler.SafeInvoke(sender, System.EventArgs.Empty)
        End Sub

        ''' <summary> Synchronously notifies an <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
        ''' <param name="handler"> The event handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       The arguments for the event. </param>
        ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
        <Extension>
        Public Sub SafeInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
            Dim evt As EventHandler(Of TEventArgs) = handler
            For Each d As [Delegate] In evt.SafeInvocationList
                If d.Target IsNot Nothing Then
                    ' synchronously executes the delegate on the target thread.
                    ' https://blogs.msdn.microsoft.com/jaredpar/2008/01/07/isynchronizeinvoke-now/
                    Dim syncInvokeTarget As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If syncInvokeTarget Is Nothing Then
                        ' if the target does not implement the I Synchronize Invoke interface, e.g., it is not a control, 
                        ' then do a dynamic invoke. this is a bit slow and does not support asynchronous call.
                        d.DynamicInvoke(New Object() {sender, e})
                    Else
                        ' synchronously executes the delegate on the target thread.
                        syncInvokeTarget.Invoke(d, New Object() {sender, e})
                    End If
                End If
            Next
        End Sub

        ''' <summary>
        ''' Synchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        <Extension>
        Public Sub SafeInvoke(ByVal handler As PropertyChangedEventHandler, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            Dim evt As PropertyChangedEventHandler = handler
            For Each d As [Delegate] In evt.SafeInvocationList
                If d.Target IsNot Nothing Then
                    ' synchronously executes the delegate on the target thread.
                    ' https://blogs.msdn.microsoft.com/jaredpar/2008/01/07/isynchronizeinvoke-now/
                    Dim syncInvokeTarget As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If syncInvokeTarget Is Nothing Then
                        ' if the target does not implement the I Synchronize Invoke interface, e.g., it is not a control, 
                        ' then do a dynamic invoke. this is a bit slow and does not support asynchronous call.
                        d.DynamicInvoke(New Object() {sender, e})
                    Else
                        ' synchronously executes the delegate on the target thread.
                        syncInvokeTarget.Invoke(d, New Object() {sender, e})
                    End If
                End If
            Next
        End Sub

        ''' <summary> Synchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changing event information. </param>
        <Extension>
        Public Sub SafeInvoke(ByVal handler As PropertyChangingEventHandler, ByVal sender As Object, ByVal e As PropertyChangingEventArgs)
            Dim evt As PropertyChangingEventHandler = handler
            For Each d As [Delegate] In evt.SafeInvocationList
                If d.Target IsNot Nothing Then
                    ' synchronously executes the delegate on the target thread.
                    ' https://blogs.msdn.microsoft.com/jaredpar/2008/01/07/isynchronizeinvoke-now/
                    Dim syncInvokeTarget As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If syncInvokeTarget Is Nothing Then
                        ' if the target does not implement the I Synchronize Invoke interface, e.g., it is not a control, 
                        ' then do a dynamic invoke. this is a bit slow and does not support asynchronous call.
                        d.DynamicInvoke(New Object() {sender, e})
                    Else
                        ' synchronously executes the delegate on the target thread.
                        syncInvokeTarget.Invoke(d, New Object() {sender, e})
                    End If
                End If
            Next
        End Sub

#End Region

#Region " SAFE BEGIN INVOKE "

        ''' <summary> Asynchronously notifies an <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
        ''' <param name="handler"> The event handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        <Extension>
        Public Sub SafeBeginInvoke(ByVal handler As EventHandler(Of System.EventArgs), ByVal sender As Object)
            handler.SafeBeginInvoke(sender, System.EventArgs.Empty)
        End Sub

        ''' <summary> Asynchronously notifies an <see cref="EventHandler(Of TEventArgs)">Event</see>. </summary>
        ''' <param name="handler"> The event handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       The arguments for the event. </param>
        ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
        <Extension>
        Public Sub SafeBeginInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
            Dim evt As EventHandler(Of TEventArgs) = handler
            For Each d As [Delegate] In evt.SafeInvocationList
                If d.Target IsNot Nothing Then
                    ' asynchronously executes the delegate on the target thread.
                    ' https://blogs.msdn.microsoft.com/jaredpar/2008/01/07/isynchronizeinvoke-now/
                    Dim syncInvokeTarget As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If syncInvokeTarget Is Nothing Then
                        ' if the target does not implement the I Synchronize Invoke interface, e.g., it is not a control, 
                        ' then do a dynamic invoke. this is a bit slow and does not support asynchronous call.
                        d.DynamicInvoke(New Object() {sender, e})
                    Else
                        ' asynchronously executes the delegate on the target thread.
                        syncInvokeTarget.BeginInvoke(d, New Object() {sender, e})
                    End If

                End If
            Next
        End Sub

        ''' <summary> Synchronously notifies an <see cref="EventHandler(Of TEventArgs)">Event</see>. </summary>
        ''' <param name="handler"> The event handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       The arguments for the event. </param>
        ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
        <Extension>
        Public Sub SafeBeginEndInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
            Dim evt As EventHandler(Of TEventArgs) = handler
            For Each d As [Delegate] In evt.SafeInvocationList
                If d.Target IsNot Nothing Then
                    ' asynchronously executes the delegate on the target thread.
                    Dim syncInvokeTarget As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If syncInvokeTarget Is Nothing Then
                        ' if the target does not implement the I Synchronize Invoke interface, e.g., it is not a control, 
                        ' then do a dynamic invoke. this is a bit slow and does not support asynchronous call.
                        d.DynamicInvoke(New Object() {sender, e})
                    Else
                        ' asynchronously executes the delegate on the target thread.
                        Dim result As IAsyncResult = syncInvokeTarget.BeginInvoke(d, New Object() {sender, e})
                        ' waits until the process ends.
                        If result IsNot Nothing Then syncInvokeTarget.EndInvoke(result)
                    End If
                End If
            Next
        End Sub

        ''' <summary>
        ''' Executes the given operation on a different thread, asynchronously. Safe for cross threading.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        <Extension>
        Public Sub SafeBeginInvoke(ByVal handler As PropertyChangedEventHandler, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            Dim evt As PropertyChangedEventHandler = handler
            For Each d As [Delegate] In evt.SafeInvocationList
                If d.Target IsNot Nothing Then
                    ' asynchronously executes the delegate on the target thread.
                    Dim syncInvokeTarget As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If syncInvokeTarget Is Nothing Then
                        ' if the target does not implement the I Synchronize Invoke interface, e.g., it is not a control, 
                        ' then do a dynamic invoke. this is a bit slow and does not support asynchronous call.
                        d.DynamicInvoke(New Object() {sender, e})
                    Else
                        ' asynchronously executes the delegate on the target thread.
                        syncInvokeTarget.BeginInvoke(d, New Object() {sender, e})
                    End If
                End If
            Next
        End Sub

        ''' <summary>
        ''' Executes the given operation on a different thread, asynchronously. Safe for cross threading.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changing event information. </param>
        <Extension>
        Public Sub SafeBeginInvoke(ByVal handler As PropertyChangingEventHandler, ByVal sender As Object, ByVal e As PropertyChangingEventArgs)
            Dim evt As PropertyChangingEventHandler = handler
            For Each d As [Delegate] In evt.SafeInvocationList
                If d.Target IsNot Nothing Then
                    ' asynchronously executes the delegate on the target thread.
                    Dim syncInvokeTarget As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If syncInvokeTarget Is Nothing Then
                        ' if the target does not implement the I Synchronize Invoke interface, e.g., it is not a control, 
                        ' then do a dynamic invoke. this is a bit slow and does not support asynchronous call.
                        d.DynamicInvoke(New Object() {sender, e})
                    Else
                        ' asynchronously executes the delegate on the target thread.
                        syncInvokeTarget.BeginInvoke(d, New Object() {sender, e})
                    End If
                End If
            Next
        End Sub

#End Region

    End Module

End Namespace


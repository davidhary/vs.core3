﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Namespace ErrorProviderExtensions

    ''' <summary> Includes extensions for <see cref="ErrorProvider">Error Provider</see>. </summary>
    ''' <license> (c) 2015 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="05/26/2015" by="David" revision="1.0.5624.x"> Created. </history>
    Public Module Methods

#Region " CLEAR "

        ''' <summary> Clears this object to its blank/initial state. </summary>
        ''' <param name="sender"> The event sender. </param>
        <Extension()>
        Public Sub Clear(ByVal provider As ErrorProvider, ByVal sender As Object)
            Dim control As Control = TryCast(sender, Control)
            If control IsNot Nothing Then
                provider?.Clear(control)
            Else
                Dim toolStripItem As ToolStripItem = TryCast(sender, ToolStripItem)
                If toolStripItem IsNot Nothing Then
                    provider?.Clear(toolStripItem)
                End If
            End If
        End Sub

        ''' <summary> Clears this object to its blank/initial state. </summary>
        ''' <param name="sender"> The event sender. </param>
        <Extension()>
        Public Sub Clear(ByVal provider As ErrorProvider, ByVal sender As Control)
            If sender IsNot Nothing Then
                provider?.SetError(sender, "")
                If TypeOf sender.Container Is ToolStripItem OrElse
                TypeOf sender.Container Is ToolStripMenuItem Then
                    provider?.Clear(sender.Container)
                Else
                    provider?.SetError(sender, "")
                End If
            End If
        End Sub

        ''' <summary> Clears this object to its blank/initial state. </summary>
        ''' <param name="sender"> The event sender. </param>
        <Extension()>
        Public Sub Clear(ByVal provider As ErrorProvider, ByVal sender As ToolStripItem)
            If sender IsNot Nothing Then
                provider?.SetError(sender.Owner, "")
            End If
        End Sub

#End Region

#Region " ANNUNCIATE - OBJECT "

        ''' <summary> Annunciates error. </summary>
        ''' <param name="sender">  The event sender. </param>
        ''' <param name="details"> The details. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function Annunciate(ByVal provider As ErrorProvider, ByVal sender As Object, ByVal details As String) As String
            Dim control As Control = TryCast(sender, Control)
            If control IsNot Nothing Then
                provider?.Annunciate(control, details)
            Else
                Dim toolStripMenuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
                If toolStripMenuItem IsNot Nothing Then
                    provider?.Annunciate(toolStripMenuItem, details)
                Else
                    Dim toolStripItem As ToolStripItem = TryCast(sender, ToolStripItem)
                    If toolStripItem IsNot Nothing Then
                        provider?.Annunciate(toolStripItem, details)
                    End If
                End If
            End If
            Return details
        End Function

        ''' <summary> Annunciates error. </summary>
        ''' <param name="sender"> The event sender. </param>
        ''' <param name="format"> Describes the format to use. </param>
        ''' <param name="args">   A variable-length parameters list containing arguments. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function Annunciate(ByVal provider As ErrorProvider, ByVal sender As Object, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return provider?.Annunciate(sender, String.Format(format, args))
        End Function

#End Region

#Region " ANNUNCIATE - CONTROL "

        ''' <summary> Annunciates error. </summary>
        ''' <param name="sender">  The event sender. </param>
        ''' <param name="details"> The details. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function Annunciate(ByVal provider As ErrorProvider, ByVal sender As Control, ByVal details As String) As String
            If provider IsNot Nothing AndAlso sender IsNot Nothing Then
                If TypeOf sender.Container Is ToolStripItem OrElse
                TypeOf sender.Container Is ToolStripMenuItem Then
                    provider.Annunciate(sender.Container, details)
                Else
                    provider.SetError(sender, details)
                End If
            End If
            Return details
        End Function

        ''' <summary> Annunciates error. </summary>
        ''' <param name="sender"> The event sender. </param>
        ''' <param name="format"> Describes the format to use. </param>
        ''' <param name="args">   A variable-length parameters list containing arguments. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function Annunciate(ByVal provider As ErrorProvider, ByVal sender As Control,
                                   ByVal format As String, ByVal ParamArray args() As Object) As String
            Return provider?.Annunciate(sender, String.Format(format, args))
        End Function

#End Region

#Region " ANNUNCIATE -- TOOL STRIP "

        ''' <summary> Annunciates error. </summary>
        ''' <param name="sender">  The sender. </param>
        ''' <param name="details"> The details. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function Annunciate(ByVal provider As ErrorProvider, ByVal sender As ToolStripItem, ByVal details As String) As String
            If provider IsNot Nothing AndAlso sender IsNot Nothing AndAlso sender.Owner IsNot Nothing Then
                provider.SetIconAlignment(sender.Owner, ErrorIconAlignment.BottomLeft)
                provider.SetIconPadding(sender.Owner, -sender.Bounds.X)
                provider.SetError(sender.Owner, details)
            End If
            Return details
        End Function

        ''' <summary> Annunciates error. </summary>
        ''' <param name="sender"> The sender. </param>
        ''' <param name="format"> Describes the format to use. </param>
        ''' <param name="args">   A variable-length parameters list containing arguments. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function Annunciate(ByVal provider As ErrorProvider, ByVal sender As ToolStripItem, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return provider?.Annunciate(sender, String.Format(format, args))
        End Function

#End Region

    End Module

End Namespace


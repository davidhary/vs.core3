Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Imports System.Collections.Specialized

Namespace EventHandlerExtensions

    Partial Public Module Methods

#Region " SYNC CONTEXT "

        ''' <summary> Returns the current synchronization context. </summary>
        ''' <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is null. </exception>
        ''' <returns> A Threading.SynchronizationContext. </returns>
        Private Function CurrentSyncContext() As Threading.SynchronizationContext
            If Threading.SynchronizationContext.Current Is Nothing Then
                Threading.SynchronizationContext.SetSynchronizationContext(New Threading.SynchronizationContext)
            End If
            If Threading.SynchronizationContext.Current Is Nothing Then
                Throw New InvalidOperationException("Current Synchronization Context not set;. Must be set before starting the thread.")
            End If
            Return Threading.SynchronizationContext.Current
        End Function

#End Region

#Region " SAFE SEND "

        ''' <summary> Synchronously notifies an <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
        ''' <param name="handler"> The event handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <remarks>
        ''' <para>Test Results posting events to a class and a panel with 10 ms sleep on both targets:</para>
        ''' <para>Safe Post (Async):      Post:  .15- .24 ms; Regain Control:   .41 -  .58 ms. loop over d </para>
        ''' <para>Safe Post (Async):      Post: 7.5 - 7.7 ms; Regain Control:   .34 -  .58 ms. Single Sync context call. </para>
        ''' <para>Safe Send (Sync):       Send: 7.7 - 8.5 ms; Regain Control: 10.7 - 11.3 ms. </para>
        ''' <para>Unsafe Invoke (Sync): Invoke: 6.7 - 7.5 ms; Regain Control: 10.2 - 11.2 ms. </para>
        ''' <para>Conclusion: Using safe invoke has little if any negative effects. </para>
        '''          </remarks>
        <Extension>
        Public Sub SafeSend(ByVal handler As EventHandler(Of System.EventArgs), ByVal sender As Object)
            handler.SafeSend(sender, System.EventArgs.Empty)
        End Sub

        ''' <summary> Synchronously notifies an <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
        ''' <param name="handler"> The event handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       The arguments for the event. </param>
        ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
        <Extension>
        Public Sub SafeSend(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
            Dim evt As EventHandler(Of TEventArgs) = handler
            If evt IsNot Nothing Then Methods.CurrentSyncContext.Send(Sub() evt(sender, e), Nothing)
#If False Then
            If evt IsNot Nothing Then
                ' same speed as the single call.
                For Each d As [Delegate] In evt.GetInvocationList
                    Methods.CurrentSyncContext.Send(Sub(ee) d.DynamicInvoke(New Object() {sender, e}), e)
                Next
            End If
#End If
        End Sub

        ''' <summary>
        ''' Synchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        <Extension>
        Public Sub SafeSend(ByVal handler As PropertyChangedEventHandler, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            Dim evt As PropertyChangedEventHandler = handler
            If evt IsNot Nothing Then Methods.CurrentSyncContext.Send(Sub() evt(sender, e), Nothing)
            If String.Equals(e.PropertyName, "Resistors", StringComparison.OrdinalIgnoreCase) Then Stop

#If False Then
            If evt IsNot Nothing Then
                ' same speed as the single call.
                For Each d As [Delegate] In evt.GetInvocationList
                    Methods.CurrentSyncContext.Send(Sub(ee) d.DynamicInvoke(New Object() {sender, e}), e)
                Next
            End If
#End If
        End Sub

        ''' <summary> Synchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>. </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changing event information. </param>
        <Extension>
        Public Sub SafeSend(ByVal handler As PropertyChangingEventHandler, ByVal sender As Object, ByVal e As PropertyChangingEventArgs)
            Dim evt As PropertyChangingEventHandler = handler
            If evt IsNot Nothing Then Methods.CurrentSyncContext.Send(Sub() evt(sender, e), Nothing)
#If False Then
            If evt IsNot Nothing Then
                ' same speed as the single call.
                For Each d As [Delegate] In evt.GetInvocationList
                    Methods.CurrentSyncContext.Send(Sub(ee) d.DynamicInvoke(New Object() {sender, e}), e)
                Next
            End If
#End If
        End Sub

        ''' <summary>
        ''' Synchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        <Extension>
        Public Sub SafeSend(ByVal handler As NotifyCollectionChangedEventHandler, ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
            Dim evt As NotifyCollectionChangedEventHandler = handler
            If evt IsNot Nothing Then Methods.CurrentSyncContext.Send(Sub() evt(sender, e), Nothing)
#If False Then
            If evt IsNot Nothing Then
                ' same speed as the single call.
                For Each d As [Delegate] In evt.GetInvocationList
                    Methods.CurrentSyncContext.Send(Sub(ee) d.DynamicInvoke(New Object() {sender, e}), e)
                Next
            End If
#End If
        End Sub

#End Region

#Region " SAFE POST "

        ''' <summary> Asynchronously notifies an <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
        ''' <param name="handler"> The event handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        <Extension>
        Public Sub SafePost(ByVal handler As EventHandler(Of System.EventArgs), ByVal sender As Object)
            handler.SafePost(sender, System.EventArgs.Empty)
        End Sub

        ''' <summary> Asynchronously notifies an <see cref="EventHandler(Of TEventArgs)">Event</see>. </summary>
        ''' <param name="handler"> The event handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       The arguments for the event. </param>
        ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
        <Extension>
        Public Sub SafePost(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
            Dim evt As EventHandler(Of TEventArgs) = handler
            If evt IsNot Nothing Then
                For Each d As [Delegate] In evt.GetInvocationList
                    Methods.CurrentSyncContext.Post(Sub(ee) d.DynamicInvoke(New Object() {sender, e}), e)
                Next
            End If
#If False Then
            ' much slower than the loop throw the invocation list.
            If evt IsNot Nothing Then Methods.CurrentSyncContext.Post(Sub() evt(sender, e), Nothing)
#End If
        End Sub

        ''' <summary>
        ''' Executes the given operation on a different thread, asynchronously. Safe for cross threading.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        <Extension>
        Public Sub CrossThreadSafePost(ByVal handler As PropertyChangedEventHandler, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            Dim evt As PropertyChangedEventHandler = handler
            If evt IsNot Nothing Then
                For Each d As [Delegate] In evt.GetInvocationList
                    Dim ctrl As Windows.Forms.Control = TryCast(d.Target, Windows.Forms.Control)
                    If ctrl IsNot Nothing AndAlso ctrl.InvokeRequired Then
                        ' still gets a cross thread exception.
                        ctrl.BeginInvoke(d, sender, e)
                    Else
                        Methods.CurrentSyncContext.Post(Sub(ee) d.DynamicInvoke(New Object() {sender, e}), e)
                    End If
                Next
            End If
        End Sub

        <Extension>
        Public Sub SafePost(ByVal handler As PropertyChangedEventHandler, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            Dim evt As PropertyChangedEventHandler = handler
            If evt IsNot Nothing Then
                For Each d As [Delegate] In evt.GetInvocationList
                    Methods.CurrentSyncContext.Post(Sub(ee) d.DynamicInvoke(New Object() {sender, e}), e)
                Next
            End If
#If False Then
            ' much slower than the loop throw the invocation list.
            If evt IsNot Nothing Then Methods.CurrentSyncContext.Post(Sub() evt(sender, e), Nothing)
#End If
        End Sub

        ''' <summary>
        ''' Executes the given operation on a different thread, asynchronously. Safe for cross threading.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changing event information. </param>
        <Extension>
        Public Sub SafePost(ByVal handler As PropertyChangingEventHandler, ByVal sender As Object, ByVal e As PropertyChangingEventArgs)
            Dim evt As PropertyChangingEventHandler = handler
            If evt IsNot Nothing Then
                For Each d As [Delegate] In evt.GetInvocationList
                    Methods.CurrentSyncContext.Post(Sub(ee) d.DynamicInvoke(New Object() {sender, e}), e)
                Next
            End If
#If False Then
            ' much slower than the loop throw the invocation list.
            If evt IsNot Nothing Then Methods.CurrentSyncContext.Post(Sub() evt(sender, e), Nothing)
#End If
        End Sub

        ''' <summary>
        ''' Executes the given operation on a different thread, asynchronously. Safe for cross threading.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        <Extension>
        Public Sub SafePost(ByVal handler As NotifyCollectionChangedEventHandler, ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
            Dim evt As NotifyCollectionChangedEventHandler = handler
            If evt IsNot Nothing Then
                For Each d As [Delegate] In evt.GetInvocationList
                    Methods.CurrentSyncContext.Post(Sub(ee) d.DynamicInvoke(New Object() {sender, e}), e)
                Next
            End If
#If False Then
            ' much slower than the loop throw the invocation list.
            If evt IsNot Nothing Then Methods.CurrentSyncContext.Post(Sub() evt(sender, e), Nothing)
#End If
        End Sub


#End Region

    End Module

End Namespace

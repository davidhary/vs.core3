﻿Imports System.Runtime.CompilerServices
Namespace NullableExtensions

    ''' <summary> Includes extensions for <see cref="Nullable">nullable</see>. </summary>
    ''' <license> (c) 2018 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    Public Module Methods

        Public Const NullValue As String = "NULL"

        ''' <summary> Convert this object into a string representation that can be saved in the database. </summary>
        ''' <param name="value">  The value. </param>
        ''' <returns> A String that represents this object. </returns>
        <Extension()>
        Public Function ToDataString(ByVal value As Boolean?) As String
            Return $"{If(value.HasValue, If(value.Value, "1", "0"), NullValue)}"
        End Function

        ''' <summary> Convert this object into a string representation that can be saved in the database. </summary>
        ''' <param name="value">  The value. </param>
        ''' <returns> A String that represents this object. </returns>
        <Extension()>
        Public Function ToDataString(ByVal value As Byte?) As String
            Return $"{If(value.HasValue, value.Value.ToString, NullValue)}"
        End Function

        ''' <summary> Convert this object into a string representation that can be saved in the database. </summary>
        ''' <param name="value">  The value. </param>
        ''' <returns> A String that represents this object. </returns>
        <Extension()>
        Public Function ToDataString(ByVal value As Integer?) As String
            Return $"{If(value.HasValue, value.Value.ToString, NullValue)}"
        End Function

        ''' <summary> Convert this object into a string representation that can be saved in the database. </summary>
        ''' <param name="value">  The value. </param>
        ''' <returns> A String that represents this object. </returns>
        <Extension()>
        Public Function ToDataString(ByVal value As Long?) As String
            Return $"{If(value.HasValue, value.Value.ToString, NullValue)}"
        End Function

        ''' <summary> Convert this object into a string representation that can be saved in the database. </summary>
        ''' <param name="value">  The value. </param>
        ''' <returns> A String that represents this object. </returns>
        <Extension()>
        Public Function ToDataString(ByVal value As Double?) As String
            Return $"{If(value.HasValue, value.Value.ToString, NullValue)}"
        End Function

        ''' <summary> Convert this object into a string representation that can be saved in the database. </summary>
        ''' <param name="value">  The value. </param>
        ''' <param name="format"> Describes the format to use. </param>
        ''' <returns> A String that represents this object. </returns>
        <Extension()>
        Public Function ToDataString(ByVal value As Double?, ByVal format As String) As String
            Return Methods.ToDataString(value, format, Methods.NullValue)
        End Function

        ''' <summary>
        ''' Convert this object into a string representation that can be saved in the database.
        ''' </summary>
        ''' <param name="value">     The value. </param>
        ''' <param name="format">    Describes the format to use. </param>
        ''' <param name="nullValue"> The null value. </param>
        ''' <returns> A String that represents this object. </returns>
        <Extension()>
        Public Function ToDataString(ByVal value As Double?, ByVal format As String, ByVal nullValue As String) As String
            Return $"{If(value.HasValue, $"{value.Value.ToString(format)}", nullValue)}"
        End Function

    End Module

End Namespace

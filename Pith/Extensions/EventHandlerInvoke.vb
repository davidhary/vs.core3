Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Namespace EventHandlerExtensions

    Partial Public Module Methods

#Region " UNSAFE INVOKES "

        ''' <summary>
        ''' Executes the given operation on a different thread, and waits for the result.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       T event information. </param>
        <Extension>
        Public Sub UnsafeInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
            Dim evt As EventHandler(Of TEventArgs) = handler
            evt?.Invoke(sender, e)
        End Sub

        ''' <summary>
        ''' Executes the given operation on a different thread, and waits for the result.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Event information. </param>
        <Extension>
        Public Sub UnsafeInvoke(ByVal handler As EventHandler(Of EventArgs), ByVal sender As Object, ByVal e As EventArgs)
            Dim evt As EventHandler(Of EventArgs) = handler
            evt?.Invoke(sender, e)
        End Sub

        ''' <summary>
        ''' Executes the given operation on a different thread, and waits for the result.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changed event information. </param>
        <Extension>
        Public Sub UnsafeInvoke(ByVal handler As PropertyChangedEventHandler, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            Dim evt As PropertyChangedEventHandler = handler
            evt?.Invoke(sender, e)
        End Sub

        ''' <summary>
        ''' Executes the given operation on a different thread, and waits for the result.
        ''' </summary>
        ''' <param name="handler"> The handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       Property Changing event information. </param>
        <Extension>
        Public Sub UnsafeInvoke(ByVal handler As PropertyChangingEventHandler, ByVal sender As Object, ByVal e As PropertyChangingEventArgs)
            Dim evt As PropertyChangingEventHandler = handler
            evt?.Invoke(sender, e)
        End Sub

#End Region

    End Module

End Namespace


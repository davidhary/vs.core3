﻿Imports System.Windows.Input
Namespace ICommandExtensions

    ''' <summary>
    ''' Extensions to the <see cref="System.Windows.Input.ICommand">interface</see>.
    ''' </summary>
    ''' <remarks>
    ''' https://stackoverflow.com/questions/10126968/call-command-from-code-behind.
    ''' </remarks>
    ''' <license>
    ''' (c) 2017 Stefan Vasiljevic. All rights reserved.<para>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    Public Module Methods

        ''' <summary> Casts the <see cref="Windows.Forms.Control.Tag">tag</see>> to an <see cref="ICommand"/> and executes the command if enabled (can execute). </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="control"> The control. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function CheckBeginExecuteTag(control As Windows.Forms.Control) As Boolean
            Return Methods.CheckBeginExecute(control?.Tag)
        End Function

        ''' <summary>
        ''' Casts the <see cref="Object"/> to an <see cref="ICommand"/> and executes the command if
        ''' enabled (can execute).
        ''' </summary>
        ''' <param name="command"> The command. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function CheckBeginExecute(command As Object) As Boolean
            Dim iCommand As ICommand = TryCast(command, ICommand)
            If iCommand Is Nothing Then
                Return False
            Else
                Return Methods.CheckBeginExecuteCommand(iCommand)
            End If
        End Function

        ''' <summary> Executes the command if enabled (can execute). </summary>
        ''' <param name="command"> The command. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function CheckBeginExecute(command As ICommand) As Boolean
            Return Methods.CheckBeginExecuteCommand(command)
        End Function

        ''' <summary> Executes the command if enabled (can execute). </summary>
        ''' <param name="command"> The command. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Private Function CheckBeginExecuteCommand(command As ICommand) As Boolean
            If command Is Nothing Then Throw New ArgumentNullException(NameOf(command))
            Dim canExecute As Boolean = False
            SyncLock command
                canExecute = command.CanExecute(Nothing)
                If canExecute Then
                    command.Execute(Nothing)
                End If
            End SyncLock
            Return canExecute
        End Function

    End Module


End Namespace

﻿Imports System.Runtime.CompilerServices
Namespace BackgroundWorkerExtensions

    ''' <summary> Includes extensions for <see cref="ArrayList">array lists</see>. </summary>
    ''' <license> (c) 2014 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/07/2014" by="David" revision="2.1.5425.x"> Created </history>
    Public Module Methods

        ''' <summary> Reports the progress. </summary>
        ''' <param name="worker">       The worker. </param>
        ''' <param name="userState">    State of the user. </param>
        ''' <param name="currentCount"> Number of currents. </param>
        ''' <param name="totalCount">   Number of totals. </param>
        <Extension()>
        Public Sub ReportProgress(worker As System.ComponentModel.BackgroundWorker, ByVal userState As Object,
                                  ByVal currentCount As Integer, ByVal totalCount As Integer)
            ReportProgress(worker, userState, currentCount, totalCount, TimeSpan.FromSeconds(1))
        End Sub

        ''' <summary> Reports the progress. </summary>
        ''' <param name="worker">       The worker. </param>
        ''' <param name="userState">    State of the user. </param>
        ''' <param name="currentCount"> Number of currents. </param>
        ''' <param name="totalCount">   Number of totals. </param>
        ''' <param name="period">       The refractory period - progress is reported every so often. </param>
        <Extension()>
        Public Sub ReportProgress(worker As System.ComponentModel.BackgroundWorker, ByVal userState As Object,
                                  ByVal currentCount As Integer, ByVal totalCount As Integer, ByVal period As TimeSpan)
            Static progressLevel As Double = 0
            Static nextTime As Date = DateTime.UtcNow + period
            If worker Is Nothing Then Return
            ' update every second
            If currentCount = 0 Then
                progressLevel = 0
                worker.ReportProgress(0, userState)
            ElseIf currentCount >= totalCount Then
                worker.ReportProgress(100, userState)
            Else
                Dim progress As Double = 100 * currentCount / totalCount
                If progress > progressLevel AndAlso DateTime.UtcNow > nextTime Then
                    worker.ReportProgress(CInt(progress), userState)
                    progressLevel += 1
                    nextTime = DateTime.UtcNow.Add(period)
                End If
            End If
        End Sub

    End Module

End Namespace

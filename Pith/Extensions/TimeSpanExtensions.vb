﻿Imports System.Runtime.CompilerServices
Namespace TimeSpanExtensions

    ''' <summary> Includes extensions for <see cref="TimeSpan"/> calculations. </summary>
    ''' <license>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    Public Module Methods

        ''' <summary> Converts seconds to time span with tick timespan accuracy. </summary>
        ''' <param name="timespan"> The timespan. </param>
        ''' <param name="seconds">  The number of seconds. </param>
        ''' <returns> A TimeSpan. </returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="timespan")>
        <Extension()>
        Public Function FromSecondsPrecise(ByVal timespan As TimeSpan, ByVal seconds As Double) As TimeSpan
            Return TimeSpan.FromTicks(CLng(TimeSpan.TicksPerSecond * seconds))
        End Function

        ''' <summary> Converts a timespan to an exact milliseconds. </summary>
        ''' <param name="timespan"> The timespan. </param>
        ''' <returns> Timespan as a Double. </returns>
        <Extension()>
        Public Function ToExactMilliseconds(ByVal timespan As TimeSpan) As Double
            Return timespan.Ticks / TimeSpan.TicksPerMillisecond
        End Function

    End Module

End Namespace


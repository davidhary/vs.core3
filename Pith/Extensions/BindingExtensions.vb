﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Namespace BindingExtensions

    ''' <summary>
    ''' Contains extension methods for <see cref="BindingsCollection">data binding</see>.
    ''' </summary>
    ''' <license>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    Public Module Methods

#Region " BINDING COLLECTIONS: REMOVE "

        ''' <summary> Clears all bindings. Verifies that binding indeed were cleared. </summary>
        ''' <remarks> The base <see cref="ControlBindingsCollection.Clear">clear</see> may not clear fast
        ''' enough for the binding to be cleared when a new binding is added. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value"> The value. </param>
        <Extension()>
        Public Sub ClearAll(ByVal value As ControlBindingsCollection)
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            Do While value.Count > 0
                Try
                    value.RemoveAt(0)
                    Application.DoEvents()
                Catch ex As ArgumentOutOfRangeException
                End Try
            Loop
        End Sub

        ''' <summary> Removes the specified binding based on the binding property name. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">        The value. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        <Extension()>
        Public Function RemoveAndVerify(ByVal value As ControlBindingsCollection, ByVal propertyName As String) As Binding
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If String.IsNullOrWhiteSpace(propertyName) Then Throw New ArgumentNullException(NameOf(propertyName))
            Dim result As Binding = Nothing
            Do While Methods.Exists(value, propertyName)
                For Each b As Binding In value
                    If String.Equals(b.PropertyName, propertyName, StringComparison.OrdinalIgnoreCase) Then
                        Try
                            result = b
                            value.Remove(b)
                            Application.DoEvents()
                        Catch ex As ArgumentOutOfRangeException
                            ' remove could cause an exception due to the way the binding is checked and released.
                            ' as it happens, the check for exists passes but the binding is no longer there when trying to
                            ' remove it.
                        End Try
                        Exit For
                    End If
                Next
            Loop
            Return result
        End Function

        ''' <summary> Removes the specified binding based on the binding property name. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   The value. </param>
        ''' <param name="binding"> The binding. </param>
        <Extension()>
        Public Function RemoveAndVerify(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As Binding
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            Return Methods.RemoveAndVerify(value, binding.PropertyName)
        End Function

#End Region

#Region " BINDING COLLECTIONS: FIND "

        ''' <summary> Checks if the binding already exists. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">        The value. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        ''' <returns> <c>True</c> if binding exists, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Exists(ByVal value As ControlBindingsCollection, ByVal propertyName As String) As Boolean
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If String.IsNullOrWhiteSpace(propertyName) Then Throw New ArgumentNullException(NameOf(propertyName))
            For Each b As Binding In value
                If String.Equals(b.PropertyName, propertyName, StringComparison.OrdinalIgnoreCase) Then
                    Return True
                End If
            Next
            Return False
        End Function

        ''' <summary> Checks if the binding already exists. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   The value. </param>
        ''' <param name="binding"> The binding. </param>
        ''' <returns> <c>True</c> if binding exists, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Exists(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As Boolean
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            Return Methods.Exists(value, binding.PropertyName)
        End Function

#End Region

#Region " BINDING COLLECTIONS: REPLACE "

        ''' <summary> Replaces the binding for the bound property of the control. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   The value. </param>
        ''' <param name="binding"> The binding. </param>
        ''' <returns>  Binding. </returns>
        <Extension()>
        Public Function Replace(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As Binding
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            Methods.RemoveAndVerify(value, binding)
            value.Add(binding)
            Return binding
        End Function

#End Region

#Region " BINDING COLLECTIONS: SELECT "

        ''' <summary> Selects an existing binding if there. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="bindings"> The binding collection. </param>
        ''' <param name="binding"> The binding. </param>
        ''' <returns>  Binding or nothing if not found </returns>
        <Extension()>
        Public Function Find(ByVal bindings As ControlBindingsCollection, ByVal binding As Binding) As Binding
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            Return Methods.Find(bindings, binding.PropertyName)
        End Function

        ''' <summary> Selects an existing binding if there. </summary>
        ''' <param name="bindings">     The binding collection. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        ''' <returns> A Binding. </returns>
        <Extension()>
        Public Function Find(ByVal bindings As BindingsCollection, ByVal propertyName As String) As Binding
            Dim result As Binding = Nothing
            If bindings IsNot Nothing Then
                For Each b As Binding In bindings
                    If String.Equals(b.PropertyName, propertyName, StringComparison.OrdinalIgnoreCase) Then
                        result = b
                        Exit For
                    End If
                Next
            End If
            Return result
        End Function

        ''' <summary> Select binding by property name. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="control">      The control. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        ''' <returns> A Binding. </returns>
        <Extension()>
        Public Function SelectBinding(ByVal control As Control, ByVal propertyName As String) As Binding
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            Return control.DataBindings.Find(propertyName)
        End Function

#End Region

#Region " BINDING COLLECTIONS: ADD "

        ''' <summary> Adds a binding to the control. Disables the control while adding the binding to allow
        ''' the disabling of control events while the binding is added. Note that the control event of
        ''' value change occurs before the bound count increments so the bound count cannot be used to
        ''' determine the control bindable status. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   The value. </param>
        ''' <param name="binding"> . </param>
        ''' <returns> The binding. </returns>
        <Extension()>
        Public Function SilentAdd(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As Binding
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            Dim isEnabled As Boolean = value.Control.Enabled
            value.Control.Enabled = False
            value.Add(binding)
            value.Control.Enabled = isEnabled
            Return binding
        End Function

#If False Then
        ''' <summary> Adds binding to a <see cref="control"/> </summary>
        ''' <param name="control">         The control. </param>
        ''' <param name="propertyName">    Name of the property. </param>
        ''' <param name="dataSource">      The data source. </param>
        ''' <param name="dataMember">      The data member. </param>
        ''' <param name="bindingComplete"> The binding complete. </param>
        ''' <returns> A Binding. </returns>
        <Extension()>
        Public Function AddBinding(ByVal control As Control, ByVal propertyName As String, ByVal dataSource As Object, ByVal dataMember As String,
                                ByVal bindingComplete As BindingCompleteEventHandler) As Binding
            Return Methods.AddRemoveBinding(control, True, New Binding(propertyName, dataSource, dataMember), bindingComplete)
        End Function

        ''' <summary> Removes binding from a <see cref="control"/> </summary>
        ''' <param name="control">         The control. </param>
        ''' <param name="propertyName">    Name of the property. </param>
        ''' <param name="dataSource">      The data source. </param>
        ''' <param name="dataMember">      The data member. </param>
        ''' <param name="bindingComplete"> The binding complete. </param>
        ''' <returns> A Binding. </returns>
        <Extension()>
        Public Function RemoveBinding(ByVal control As Control, ByVal propertyName As String, ByVal dataSource As Object, ByVal dataMember As String,
                                      ByVal bindingComplete As BindingCompleteEventHandler) As Binding
            Return Methods.AddRemoveBinding(control, False, New Binding(propertyName, dataSource, dataMember), bindingComplete)
        End Function

        ''' <summary> Adds or removes binding from a <see cref="control"/> </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="control">         The control. </param>
        ''' <param name="add">             True to add. </param>
        ''' <param name="binding">         The binding. </param>
        ''' <param name="bindingComplete"> The binding complete. </param>
        ''' <returns> A Binding. </returns>
        <Extension()>
        Public Function AddRemoveBinding(ByVal control As Control, ByVal add As Boolean, ByVal binding As Binding,
                                         ByVal bindingComplete As BindingCompleteEventHandler) As Binding
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            With control
                If add Then
                    ' required to prevent cross thread exceptions. Invoke is required on the property change event.
                    binding.ControlUpdateMode = ControlUpdateMode.OnPropertyChanged
                    binding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged
                    .DataBindings.Add(binding)
                    AddHandler binding.BindingComplete, bindingComplete
                Else
                    ' .DataBindings.Remove(binding)
                    Dim b As Binding = control.DataBindings.Find(binding.PropertyName)
                    If b IsNot Nothing Then .DataBindings.Remove(b)
                    RemoveHandler binding.BindingComplete, bindingComplete
                End If
            End With
            Return binding
        End Function
#End If

        ''' <summary> Adds binding to a <see cref="IBindableComponent">bindable componenet</see> </summary>
        ''' <param name="component ">      The bindable component </param>
        ''' <param name="propertyName">    Name of the property </param>
        ''' <param name="dataSource">      The data source </param>
        ''' <param name="dataMember">      The data member </param>
        ''' <param name="bindingComplete"> The binding complete. </param>
        ''' <returns> A Binding. </returns>
        <Extension()>
        Public Function AddBinding(ByVal component As IBindableComponent, ByVal propertyName As String, ByVal dataSource As Object, ByVal dataMember As String,
                                   ByVal bindingComplete As BindingCompleteEventHandler) As Binding
            Return Methods.AddRemoveBinding(component, True, New Binding(propertyName, dataSource, dataMember), bindingComplete)
        End Function

        ''' <summary> Removes binding from a <see cref="IBindableComponent">bindable componenet</see> </summary>
        ''' <param name="component ">      The bindable component </param>
        ''' <param name="propertyName">    Name of the property. </param>
        ''' <param name="dataSource">      The data source. </param>
        ''' <param name="dataMember">      The data member. </param>
        ''' <param name="bindingComplete"> The binding complete. </param>
        ''' <returns> A Binding. </returns>
        <Extension()>
        Public Function RemoveBinding(ByVal component As IBindableComponent, ByVal propertyName As String, ByVal dataSource As Object, ByVal dataMember As String,
                                      ByVal bindingComplete As BindingCompleteEventHandler) As Binding
            Return Methods.AddRemoveBinding(component, False, New Binding(propertyName, dataSource, dataMember), bindingComplete)
        End Function

        ''' <summary>
        ''' Adds or removes binding from a <see cref="IBindableComponent">bindable componenet</see>
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="component">       The bindable component. </param>
        ''' <param name="add">             True to add; otherwise, remove. </param>
        ''' <param name="binding">         The binding. </param>
        ''' <param name="bindingComplete"> The binding complete handler. </param>
        ''' <returns> A Binding. </returns>
        <Extension()>
        Public Function AddRemoveBinding(ByVal component As IBindableComponent, ByVal add As Boolean, ByVal binding As Binding,
                                         ByVal bindingComplete As BindingCompleteEventHandler) As Binding
            If component Is Nothing Then Throw New ArgumentNullException(NameOf(component))
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            With component
                If add Then
                    ' required to prevent cross thread exceptions. Invoke is required on the property change event.
                    binding.ControlUpdateMode = ControlUpdateMode.OnPropertyChanged
                    binding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged
                    .DataBindings.Add(binding)
                    AddHandler binding.BindingComplete, bindingComplete
                Else
                    ' .DataBindings.Remove(binding)
                    Dim b As Binding = component.DataBindings.Find(binding.PropertyName)
                    If b IsNot Nothing Then .DataBindings.Remove(b)
                    RemoveHandler binding.BindingComplete, bindingComplete
                End If
            End With
            Return binding
        End Function

#End Region

#Region " CONTROL WRITE BOUND VALUE "

        ''' <summary> Writes a bound value to the data source. </summary>
        ''' <remarks>
        ''' The combo box does not implement a property change for the selected item property. When
        ''' called from the Selected Value Changed event of the control, this method effects a change of
        ''' selected item when the selected value changes by writing the selected item to the data source
        ''' identified in the binding of the selected item data source property, if such item exists.
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="control"> The control. </param>
        <Extension()>
        Public Sub WriteBoundValue(ByVal control As Control, ByVal propertyName As String)
            If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
            control.SelectBinding(propertyName)?.WriteValue()
        End Sub

        ''' <summary> Writes a bound selected item value to the data source. </summary>
        ''' <param name="control"> The control. </param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters",
                                      Justification:="This requires a control having a Selected Item property")>
        <Extension()>
        Public Sub WriteBoundSelectedItem(ByVal control As ComboBox)
            Methods.WriteBoundValue(control, NameOf(ComboBox.SelectedItem))
        End Sub

#End Region

    End Module

End Namespace


﻿Imports System.Runtime.CompilerServices
Namespace StopwatchExtensions

    ''' <summary> Includes extensions for <see cref="Stopwatch">Stop Watch</see>. </summary>
    ''' <license> (c) 2015 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="03/19/2015" by="David" revision="2.0.5556.x"> Created. </history>
    Public Module Methods

        ''' <summary> Waits wait the stop watch is running for the elapsed time to expire. </summary>
        ''' <param name="stopwatch"> The stop watch. </param>
        ''' <param name="value">     The value. </param>
        ''' <returns> The elapsed <see cref="System.TimeSpan"/> </returns>
        <Extension()>
        Public Function LetElapse(ByVal stopwatch As Stopwatch, ByVal value As TimeSpan) As TimeSpan
            If stopwatch IsNot Nothing AndAlso value > TimeSpan.Zero AndAlso stopwatch.IsRunning Then
                Do
                    System.Windows.Forms.Application.DoEvents()
                Loop Until stopwatch.Elapsed > value
                Return stopwatch.Elapsed
            Else
                Return TimeSpan.Zero
            End If
        End Function

        ''' <summary> Adds a new time span to elapsed time and wait to completion. </summary>
        ''' <param name="stopwatch"> The stop watch. </param>
        ''' <param name="value">     The value to add to the elapsed time. </param>
        ''' <returns> The elapsed <see cref="System.TimeSpan"/> </returns>
        <Extension()>
        Public Function Wait(ByVal stopwatch As Stopwatch, ByVal value As TimeSpan) As TimeSpan
            If stopwatch IsNot Nothing AndAlso value > TimeSpan.Zero Then
                If Not stopwatch.IsRunning Then stopwatch.Restart()
                value = value.Add(stopwatch.Elapsed)
                Do
                    System.Windows.Forms.Application.DoEvents()
                Loop Until stopwatch.Elapsed > value
                Return stopwatch.Elapsed
            Else
                Return TimeSpan.Zero
            End If
        End Function

        ''' <summary> Delays execution by the given timespan </summary>
        ''' <param name="stopwatch"> The stop watch. </param>
        ''' <param name="value">     The value to add to the elapsed time. </param>
        ''' <returns> The elapsed <see cref="System.TimeSpan"/> </returns>
        <Extension()>
        Public Async Function Delay(ByVal stopwatch As Stopwatch, ByVal value As TimeSpan) As Threading.Tasks.Task(Of TimeSpan)
            If stopwatch IsNot Nothing AndAlso value > TimeSpan.Zero Then
                If Not stopwatch.IsRunning Then stopwatch.Restart()
                Await Threading.Tasks.Task.Delay(value)
                Return stopwatch.Elapsed
            Else
                Return TimeSpan.Zero
            End If
        End Function

        ''' <summary> Query if 'stopwatch' is expired. </summary>
        ''' <param name="stopwatch"> The stop watch. </param>
        ''' <param name="timeoutTimespan">     The value. </param>
        ''' <returns> <c>true</c> if expired; otherwise <c>false</c> </returns>
        <Extension()>
        Public Function IsExpired(ByVal stopwatch As Stopwatch, ByVal timeoutTimespan As TimeSpan) As Boolean
            Return stopwatch IsNot Nothing AndAlso timeoutTimespan > TimeSpan.Zero AndAlso stopwatch.Elapsed > timeoutTimespan
        End Function

        ''' <summary> Elapsed exact milliseconds. </summary>
        ''' <param name="stopwatch"> The stop watch. </param>
        ''' <returns> A Double. </returns>
        <Extension()>
        Public Function ElapsedExactMilliseconds(ByVal stopwatch As Stopwatch) As Double
            Return If(stopwatch IsNot Nothing, stopwatch.ElapsedTicks / TimeSpan.TicksPerMillisecond, 0)
        End Function

    End Module

End Namespace

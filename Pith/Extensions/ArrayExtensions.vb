﻿Imports System.Runtime.CompilerServices
Namespace ArrayExtensions

    Partial Public Module Methods

        ''' <summary>
        ''' Determines if the two specified arrays have the same values.
        ''' </summary>
        ''' <param name="left">The left value.</param>
        ''' <param name="right">The right value.</param><returns></returns>
        ''' <remarks>
        ''' <see cref="T:Array"/> or <see cref="T:Arraylist"/> equals methods cannot be used because it expects the two 
        ''' entities to be the same for equality.
        ''' </remarks>
        <Extension()>
        Public Function ValueEquals(Of T)(ByVal left As T(), ByVal right As T()) As Boolean
            Dim result As Boolean = True
            If left Is Nothing Then
                result = right Is Nothing
            ElseIf right Is Nothing Then
                result = False
            Else
                For i As Integer = 0 To left.Length - 1
                    If Not left(i).Equals(right(i)) Then
                        result = False
                        Exit For
                    End If
                Next
            End If
            Return result
        End Function

    End Module

End Namespace

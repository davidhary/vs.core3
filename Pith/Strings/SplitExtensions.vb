﻿Imports System.Runtime.CompilerServices
Namespace SplitExtensions
    ''' <summary> Includes Split extensions for <see cref="String">String</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    Public Module Methods

        Private Const Space As String = " "
        Private Const SpaceCharacter As Char = " "c

        ''' <summary> Splits the string to words by adding spaces between lower and upper case characters. </summary>
        ''' <param name="value"> The <c>String</c> value to split. </param>
        ''' <returns> A <c>String</c> of words separated by spaces. </returns>
        <Extension()>
        Public Function SplitWords(ByVal value As String) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return String.Empty
            Else
                Dim isSpace As Boolean = False
                Dim isLowerCase As Boolean = False
                Dim newValue As New System.Text.StringBuilder
                For Each c As Char In value
                    If Not isSpace AndAlso isLowerCase AndAlso Char.IsUpper(c) Then
                        newValue.Append(Methods.Space)
                    End If
                    isSpace = c.Equals(SpaceCharacter)
                    isLowerCase = Not Char.IsUpper(c)
                    newValue.Append(c)
                Next
                Return newValue.ToString()
            End If
        End Function

        Private Const Element As String = "$1"
        ''' <summary> The complete split command finding case as well as numeric and string of upper case characters. </summary>
        ''' <remarks>This will match against each word in Camel and Pascal case strings, while properly handling acronyms and including numbers.
        ''' (^[a-z]+)                               Match against any lower-case letters at the start of the command.
        ''' ([0-9]+)                                Match against one Or more consecutive numbers (anywhere in the string, including at the start).
        ''' ([A-Z]{1}[a-z]+)                        Match against Title case words (one upper case followed by lower case letters).
        ''' ([A-Z]+(?=([A-Z][a-z])|($)|([0-9])))    Match against multiple consecutive upper-case letters, leaving the last upper case letter 
        '''                                         out the match if it Is followed by lower case letters, and including it if it's followed 
        '''                                         by the end of the string or a number.</remarks>
        Private Const SplitTitleCommand As String = "((^[a-z]+)|([0-9]+)|([A-Z]{1}[a-z]+)|([A-Z]+(?=([A-Z][a-z])|($)|([0-9]))))"

        ''' <summary>
        ''' Splits camel case match against each word in Camel and Pascal case strings, while properly
        ''' handling acronyms and including numbers.
        ''' </summary>
        ''' <param name="value"> The <c>String</c> value to split. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function SplitTitleCase(ByVal value As String) As String
            Return value.SplitTitleCase(Methods.Space)
        End Function

        ''' <summary>
        ''' Splits camel case match against each word in Camel and Pascal case strings, while properly
        ''' handling acronyms and including numbers.
        ''' </summary>
        ''' <param name="value">     The <c>String</c> value to split. </param>
        ''' <param name="delimiter"> The delimited. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function SplitTitleCase(ByVal value As String, ByVal delimiter As String) As String
            Return System.Text.RegularExpressions.Regex.Replace(value, Methods.SplitTitleCommand, $"{Methods.Element}{delimiter}",
                                                                System.Text.RegularExpressions.RegexOptions.Compiled).Trim()
        End Function

    End Module
End Namespace

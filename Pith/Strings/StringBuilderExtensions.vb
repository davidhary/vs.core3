﻿Imports System.Runtime.CompilerServices
Namespace StringBuilderExtensions
    ''' <summary> Includes extensions for <see cref="System.Text.StringBuilder">string builder</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created </history>
    Public Module Methods

        ''' <summary> Converts a builder to a string trim end line. </summary>
        ''' <param name="builder"> The builder. </param>
        ''' <returns> Builder as a String. </returns>
        <Extension()>
        Public Function ToStringTrimEndLine(ByVal builder As System.Text.StringBuilder) As String
            If builder Is Nothing Then
                Return String.Empty
            Else
                Return builder.ToString.TrimEnd(Environment.NewLine.ToCharArray)
            End If
        End Function

        ''' <summary> Appends a line if has value. </summary>
        ''' <param name="builder"> The builder. </param>
        ''' <param name="value">   The value. </param>
        <Extension()>
        Public Sub AppendLineIf(ByVal builder As System.Text.StringBuilder, ByVal value As String)
            If Not String.IsNullOrWhiteSpace(value) Then builder?.AppendLine(value)
        End Sub

        ''' <summary> Appends a line if has value. </summary>
        ''' <param name="builder"> The builder. </param>
        ''' <param name="format">  Describes the format to use. </param>
        ''' <param name="args">    A variable-length parameters list containing arguments. </param>
        <Extension()>
        Public Sub AppendLineIf(ByVal builder As System.Text.StringBuilder, ByVal format As String, ByVal ParamArray args() As Object)
            If Not String.IsNullOrWhiteSpace(format) Then builder?.AppendLine(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Sub

        ''' <summary> Returns an array of strings split by the new line characters. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> An array that represents the data in this object. </returns>
        <Extension()>
        Public Function ToArray(ByVal value As System.Text.StringBuilder) As String()
            If value Is Nothing Then Return New String() {}
            Return value.ToString.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
        End Function

        ''' <summary> Adds text to string builder starting with a new line. </summary>
        ''' <param name="builder"> The builder. </param>
        ''' <param name="value">   The value. </param>
        <Extension()>
        Public Sub AddWord(ByVal builder As System.Text.StringBuilder, ByVal value As String)
            StringBuilderExtensions.AddWord(builder, value, ",")
        End Sub

        ''' <summary> Adds text to string builder adding a delimiter if the string builder is not empty. </summary>
        ''' <param name="builder">   The builder. </param>
        ''' <param name="value">     The value. </param>
        ''' <param name="delimiter"> The delimiter. </param>
        <Extension()>
        Public Sub AddWord(ByVal builder As System.Text.StringBuilder, ByVal value As String, ByVal delimiter As String)
            If Not String.IsNullOrWhiteSpace(value) Then
                If builder Is Nothing Then
                    builder = New System.Text.StringBuilder
                End If
                If builder.Length > 0 Then
                    builder.Append(delimiter)
                End If
                builder.Append(value)
            End If
        End Sub

    End Module
End Namespace

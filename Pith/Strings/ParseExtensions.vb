﻿Imports System.Runtime.CompilerServices
Namespace ParseExtensions

    ''' <summary> Parse extension methods. </summary>
    ''' <license>
    ''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    Public Module ParseMethods

        ''' <summary> Returns true if the string represents any number. </summary>
        ''' <param name="value"> Specifies the number candidate. </param>
        ''' <returns> <c>True</c> if the string represents any number; otherwise, <c>False</c>. </returns>
        <Extension()>
        Public Function IsNumber(ByVal value As String) As Boolean
            Dim a As Double
            Return System.Double.TryParse(value, Globalization.NumberStyles.Number Or
                                          Globalization.NumberStyles.AllowExponent, System.Globalization.CultureInfo.CurrentCulture, a)
        End Function

        ''' <summary> Numeric contents. </summary>
        ''' <param name="value"> Specifies the number candidate. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function NumericContents(ByVal value As String) As String
            Return System.Text.RegularExpressions.Regex.Replace(value, "[^\d]", "")
        End Function

    End Module

End Namespace

﻿''' <summary> A thread safe monitor. </summary>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="7/29/2017" by="David" revision=""> Created. </history> 
Public NotInheritable Class ThreadSafeMonitor
    Implements IDisposable

    Private _BusyCount As Integer

    ''' <summary> RuturnsTrue if busy. </summary>
    ''' <returns> <c>true</c> if busy; otherwise <c>false</c> </returns>
    Public Function Busy() As Boolean
        Return Me._BusyCount > 0
    End Function

    ''' <summary> Increments the busy count. </summary>
    Public Sub Enter()
        System.Threading.Interlocked.Increment(Me._BusyCount)
    End Sub

    ''' <summary> Increments the busy count and return an instance of the <see cref="ThreadSafeMonitor"/>. </summary>
    ''' <remarks>
    ''' Use the following code to block re-entry
    ''' <code>
    ''' Private Monitor as New ThreadSafeMonitor 
    ''' Private Sub OnCollectionChanged(e As NotifyCollectionChangedEventArgs)
    '''    Dim evt As NotifyCollectionChangedEventHandler = CollectionChangedEvent
    '''    If evt IsNot Nothing Then
    '''        Using Me.Monitor.SyncMonitor()
    '''           Me.Context.Send(Sub(state) evt(Me, e), Nothing)
    '''        End Using
    '''    End If
    ''' End Sub
    ''' </code>
    ''' </remarks>
    Public Function SyncMonitor() As IDisposable
        System.Threading.Interlocked.Increment(Me._BusyCount)
        Return Me
    End Function

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> Decrements the busy count. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        System.Threading.Interlocked.Decrement(Me._BusyCount)
    End Sub

End Class

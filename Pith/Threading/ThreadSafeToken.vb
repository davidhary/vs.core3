﻿Imports System.Threading
''' <summary> A Thread safe token </summary>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/29/2017" by="David" revision=""> Created. </history>
<DebuggerDisplay("Value = {Value}")>
Public Class ThreadSafeToken(Of T)
    Implements IDisposable

#Region " CONSTRUCTOR "

    Public Sub New()
        MyBase.New()
        Me._SlimLock = New ReaderWriterLockSlim()
    End Sub

#Region " IDISPOSABLE SUPPORT "

    ''' <summary> Gets or sets the sentinel to detect redundant calls. </summary>
    ''' <value> The sentinel to detect redundant calls. </value>
    Protected ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Pith.ThreadSafeToken(Of T) and
    ''' optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.IsDisposed Then
            If disposing Then
            End If
            If Me._SlimLock IsNot Nothing Then Me._SlimLock.Dispose() : Me._SlimLock = Nothing
        End If
        Me._IsDisposed = True
    End Sub

    ''' <summary> Finalizes this object. </summary>
    Protected Overrides Sub Finalize()
        '  override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(False)
        MyBase.Finalize()
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        ' uncomment the following line if Finalize() is overridden above.
        GC.SuppressFinalize(Me)
    End Sub

#End Region

#End Region

    Private _SlimLock As ReaderWriterLockSlim

    Private _Value As T
    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property Value As T
        Get
            Me._SlimLock.EnterReadLock()
            Try
                Return Me._Value
            Finally
                Me._SlimLock.ExitReadLock()
            End Try
        End Get
        Set(value As T)
            Me._SlimLock.EnterWriteLock()
            Try
                Me._Value = value
            Finally
                Me._SlimLock.ExitWriteLock()
            End Try
        End Set
    End Property

End Class

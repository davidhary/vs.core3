﻿Imports System
Imports System.Threading.Tasks

''' <summary> Dependent Tasks runner. </summary>
''' <license>
''' (c) 2017 Code Cleaner. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="6/10/2017" by="Code Cleaner">      >
''' https://www.codeproject.com/script/Membership/View.aspx?mid=10213156
''' https://www.codeproject.com/Tips/1191285/DependentTaskRunner-How-to-Run-Tasks-with-Dependen
''' https://github.com/TheCodeCleaner/DependentTaskRunner. </history>
Public Class TaskRunner(Of T As Class)

    ''' <summary> The get dependencies function. </summary>
    Private ReadOnly _getDependenciesFunction As Func(Of T, IEnumerable(Of T))

    ''' <summary> The perform task function. </summary>
    Private ReadOnly _performTaskFunction As Func(Of T, Task(Of Boolean))

    ''' <summary> Constructor. </summary>
    ''' <param name="getDependenciesFunction"> The get dependencies function. </param>
    ''' <param name="performTaskFunction">     The perform task function. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")>
    Public Sub New(ByVal getDependenciesFunction As Func(Of T, IEnumerable(Of T)), ByVal performTaskFunction As Func(Of T, Task(Of Boolean)))
        _getDependenciesFunction = getDependenciesFunction
        _performTaskFunction = performTaskFunction
    End Sub

    ''' <summary> Performs the tasks action. </summary>
    ''' <param name="tasks"> The tasks. </param>
    Public Sub PerformTasks(ByVal tasks As IEnumerable(Of T))
        Dim tasksState As TasksState(Of T) = New TasksState(Of T) With {.DoneTasks = New HashSet(Of T)(), .TasksToDo = New List(Of T)(tasks), .TasksInProgress = New HashSet(Of T)()}
        Me.PerformTasks(tasksState)
    End Sub

    ''' <summary> Performs the tasks action. </summary>
    ''' <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
    ''' <param name="tasksState"> State of the tasks. </param>
    Private Sub PerformTasks(ByVal tasksState As TasksState(Of T))
        Dim task As T
        SyncLock tasksState
            If (Not tasksState.TasksToDo.Any()) AndAlso (Not tasksState.TasksInProgress.Any()) Then
                Return
            End If
            task = Me.FindTaskToDo(tasksState)
        End SyncLock

        If task Is Nothing Then
            If tasksState.TasksInProgress.Any() Then
                Return
            End If
            Throw New InvalidOperationException("There are some tasks with dependencies that can't be satisfied.")
        End If

        MoveTaskToInProgress(tasksState, task)
        System.Threading.Tasks.Task.Run(Sub() PerformTasks(tasksState))

        Me.PerformTaskAndTheTasksDependentOnIt(tasksState, task)
    End Sub

    ''' <summary> Performs the task and the tasks dependent on iterator action. </summary>
    ''' <param name="tasksState"> State of the tasks. </param>
    ''' <param name="task">       The task. </param>
    Private Sub PerformTaskAndTheTasksDependentOnIt(ByVal tasksState As TasksState(Of T), ByVal task As T)
        Me._performTaskFunction(task).ContinueWith(Sub(taskResult)
                                                       If Not taskResult.Result Then
                                                           Return
                                                       End If
                                                       MoveTaskToDone(tasksState, task)
                                                       PerformTasks(tasksState)
                                                   End Sub).Wait()
    End Sub

    ''' <summary> Searches for the first task to do. </summary>
    ''' <param name="tasksState"> State of the tasks. </param>
    ''' <returns> The found task to do. </returns>
    Private Function FindTaskToDo(ByVal tasksState As TasksState(Of T)) As T
        For Each task As T In tasksState.TasksToDo
            Dim dependencies As IEnumerable(Of T) = _getDependenciesFunction(task)
            If dependencies Is Nothing OrElse dependencies.All(Function(d) tasksState.DoneTasks.Contains(d)) Then
                Return task
            End If
        Next task
        Return Nothing
    End Function

    ''' <summary> Move task to in progress. </summary>
    ''' <param name="tasksState"> State of the tasks. </param>
    ''' <param name="task">       The task. </param>
    Private Shared Sub MoveTaskToInProgress(ByVal tasksState As TasksState(Of T), ByVal task As T)
        SyncLock tasksState
            tasksState.TasksToDo.Remove(task)
            tasksState.TasksInProgress.Add(task)
        End SyncLock
    End Sub

    ''' <summary> Move task to done. </summary>
    ''' <param name="tasksState"> State of the tasks. </param>
    ''' <param name="task">       The task. </param>
    Private Shared Sub MoveTaskToDone(ByVal tasksState As TasksState(Of T), ByVal task As T)
        SyncLock tasksState
            tasksState.TasksInProgress.Remove(task)
            tasksState.DoneTasks.Add(task)
        End SyncLock
    End Sub

    ''' <summary> Tasks state keeps track of the active and completed tasks. </summary>
    Private Class TasksState(Of TTask)

        ''' <summary> Gets or sets the tasks to do. </summary>
        ''' <value> The tasks to do. </value>
        Public Property TasksToDo() As List(Of TTask)

        ''' <summary> Gets or sets the done tasks. </summary>
        ''' <value> The done tasks. </value>
        Public Property DoneTasks() As HashSet(Of TTask)

        ''' <summary> Gets or sets the tasks in progress. </summary>
        ''' <value> The tasks in progress. </value>
        Public Property TasksInProgress() As HashSet(Of TTask)
    End Class

End Class

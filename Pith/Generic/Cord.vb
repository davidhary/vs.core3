''' <summary> Implements a generic line class as a line between two generic
''' <typeparam name="T"> Specifies the generic type of the instance of the class. </typeparam>
''' <see cref="Point(Of T)">points</see>. </summary>
''' <license> (c) 2006 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="04/10/2006" by="David" revision="1.1.2291"> Created. </history>
Public Class Cord(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})

    Inherits Line(Of T)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs a <see cref="Line(of T)"/> instance by its origin and insertion points. </summary>
    ''' <param name="origin">    Specifies the <see cref="Point(of T)">origin</see> or start point of
    ''' the line. </param>
    ''' <param name="insertion"> Specifies the <see cref="Point(of T)">insertion</see> or end point of
    ''' the line. </param>
    Public Sub New(ByVal origin As Point(Of T), ByVal insertion As Point(Of T))
        MyBase.New()
        Me.SetLine(origin, insertion)
    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary> Gets or sets the insertion point of the line. </summary>
    ''' <value> A <see cref="Point(of T)">Point</see> property. </value>
    Public Property Insertion() As Point(Of T)
        Get
            Return New Point(Of T)(Me.X2, Me.Y2)
        End Get
        Set(ByVal Value As Point(Of T))
            If Value IsNot Nothing Then
                Me.X2 = Value.X
                Me.Y2 = Value.Y
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the origin point of the line. </summary>
    ''' <value> A <see cref="Point(of T)">Point</see> property. </value>
    Public Property Origin() As Point(Of T)
        Get
            Return New Point(Of T)(Me.X1, Me.Y1)
        End Get
        Set(ByVal Value As Point(Of T))
            If Value IsNot Nothing Then
                Me.X1 = Value.X
                Me.Y1 = Value.Y
            End If
        End Set
    End Property

    ''' <summary> Sets the line coordinates. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="origin">    Specifies the origin point of the line. </param>
    ''' <param name="insertion"> Specifies the insertion point of the line. </param>
    Public Overloads Sub SetLine(ByVal origin As Point(Of T), ByVal insertion As Point(Of T))

        If origin Is Nothing Then
            Throw New ArgumentNullException(NameOf(origin))
        End If
        If insertion Is Nothing Then
            Throw New ArgumentNullException(NameOf(insertion))
        End If
        Me.Origin = New Point(Of T)(origin)
        Me.Insertion = New Point(Of T)(insertion)

    End Sub

#End Region

End Class

﻿Imports System.ComponentModel
''' <summary> A control capable of trace message broadcasting. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="12/26/2015" by="David" revision="2.1.5836"> Created. </history>
Public Class TalkerControlBase
    Inherits PropertyNotifyControlBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Protected Sub New(ByVal talker As ITraceMessageTalker)
        MyBase.New
        Me.ConstructorSafeSetter(talker)
    End Sub

    ''' <summary> Default constructor. </summary>
    Protected Sub New()
        Me.New(New TraceMessageTalker)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Pith.PropertyNotifyControlBase and
    ''' optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.Talker = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " TRACE LEVEL CONTROLS "

#Region " TRACE EVENT DISPLAY "

    ''' <summary> List trace event levels. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="control"> The control. </param>
    Public Shared Sub ListTraceEventLevels(ByVal control As System.Windows.Forms.ComboBox)
        If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
        Dim comboEnabled As Boolean = control.Enabled
        control.Enabled = False
        control.DataSource = Nothing
        control.ValueMember = "Key"
        control.DisplayMember = "Value"
        control.Items.Clear()
        control.Items.Add(New KeyValuePair(Of TraceEventType, String)(TraceEventType.Critical, "Critical"))
        control.Items.Add(New KeyValuePair(Of TraceEventType, String)(TraceEventType.Error, "Error"))
        control.Items.Add(New KeyValuePair(Of TraceEventType, String)(TraceEventType.Warning, "Warning"))
        control.Items.Add(New KeyValuePair(Of TraceEventType, String)(TraceEventType.Information, "Information"))
        control.Items.Add(New KeyValuePair(Of TraceEventType, String)(TraceEventType.Verbose, "Verbose"))

        ' This does not seem to work. It lists the items in the combo box, but the control items are empty.

        ' control.DataSource = isr.Core.Pith.EnumExtensions.ValueDescriptionPairs(TraceEventType.Critical).ToArray
        ' control.SelectedIndex = -1
        control.Enabled = comboEnabled
        control.Invalidate()
    End Sub

    ''' <summary> Select item. </summary>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   The value. </param>
    Public Shared Sub SelectItem(ByVal control As Windows.Forms.ToolStripComboBox, ByVal value As TraceEventType)
        If control IsNot Nothing Then
            control.SelectedItem = New KeyValuePair(Of TraceEventType, String)(value, value.ToString)
        End If
    End Sub

    ''' <summary> Selected value. </summary>
    ''' <param name="control">      The control. </param>
    ''' <param name="defaultValue"> The default value. </param>
    ''' <returns> A TraceEventType. </returns>
    Public Shared Function SelectedValue(ByVal control As Windows.Forms.ToolStripComboBox, ByVal defaultValue As TraceEventType) As TraceEventType
        If control IsNot Nothing AndAlso control.SelectedItem IsNot Nothing Then
            Dim kvp As KeyValuePair(Of TraceEventType, String) = CType(control.SelectedItem, KeyValuePair(Of TraceEventType, String))
            If [Enum].IsDefined(GetType(TraceEventType), kvp.Key) Then
                defaultValue = kvp.Key
            End If
        End If
        Return defaultValue
    End Function

#End Region

#End Region

End Class


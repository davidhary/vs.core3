﻿''' <summary> Interface for a trace message talker. </summary>
''' <license>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
Public Interface ITraceMessageTalker
    Inherits ITalker

    ''' <summary> Gets or sets a message describing the trace. </summary>
    ''' <value> A message describing the trace. </value>
    ReadOnly Property TraceMessage As TraceMessage

    ''' <summary> Publishes the message. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Function Publish(ByVal value As TraceMessage) As String

    ''' <summary> Publishes the message. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Function Publish(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object) As String

    ''' <summary> Publishes overriding the listeners trace level. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    ''' <returns> A String. </returns>
    Function PublishOverride(ByVal value As TraceMessage) As String

    ''' <summary> Publishes overriding the listeners trace level. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Function PublishOverride(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object) As String

    ''' <summary> Determines if the message with the specified level is publishable on the listener type. </summary>
    ''' <param name="listenerType"> Type of the listener. </param>
    ''' <param name="value">        The value. </param>
    ''' <returns> <c>true</c> if publishable; otherwise <c>false</c> </returns>
    Function Publishable(ByVal listenerType As ListenerType, ByVal value As TraceEventType) As Boolean

    ''' <summary> Gets or sets the listeners. </summary>
    ''' <value> The listeners. </value>
    ReadOnly Property Listeners As MessageListenerCollection

    ''' <summary> Gets the trace log level. </summary>
    ''' <value> The trace level. </value>
    ReadOnly Property TraceLogLevel As TraceEventType

    ''' <summary> Gets the trace Show level. </summary>
    ''' <value> The trace level. </value>
    ReadOnly Property TraceShowLevel As TraceEventType

    ''' <summary> Event queue for all listeners interested in DateChanged events. </summary>
    Event DateChanged As EventHandler(Of System.EventArgs)

End Interface


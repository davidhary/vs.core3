Imports System.Drawing
Imports System.Windows.Forms
Imports isr.Core.Pith
''' <summary> A form listening to trace messages. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="12/26/2015" by="David" revision="2.1.5836"> Created. </history>
Public Class ListenerFormBase
    Inherits Form

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Gets the initializing components sentinel. </summary>
    ''' <value> The initializing components sentinel. </value>
    Protected Property InitializingComponents As Boolean

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    Protected Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        Me.InitializeComponent()
        Me.InitializingComponents = False
        Me.ConstructorSafeSetter(New TraceMessageTalker)
    End Sub

    ''' <summary> Initializes the component. </summary>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(331, 341)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = My.Resources.favicon
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "FormBase"
        Me.ResumeLayout(False)
    End Sub

    ''' <summary> Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.Talker = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#Region " CLASS STYLE "

    ''' <summary> The enable drop shadow version. </summary>
    Public Const EnableDropShadowVersion As Integer = 5

    ''' <summary> Gets or sets the class style. </summary>
    ''' <value> The class style. </value>
    Protected Property ClassStyle As ClassStyleConstants = ClassStyleConstants.None

    ''' <summary> Adds a drop shadow parameter. </summary>
    ''' <remarks> From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx. </remarks>
    ''' <value> Options that control the create. </value>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                                        Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CInt(Me.ClassStyle)
            Return cp
        End Get
    End Property

#End Region

#End Region

#Region " SHOW "

    ''' <summary>
    ''' Shows the <see cref="RichTextBox">rich text box</see> form with these messages.
    ''' </summary>
    ''' <param name="mdiForm"> The MDI form. </param>
    ''' <param name="owner">   The owner. </param>
    Public Overloads Sub Show(ByVal mdiForm As System.Windows.Forms.Form, ByVal owner As System.Windows.Forms.IWin32Window)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        MyBase.Show(owner)
    End Sub

    ''' <summary> Shows the <see cref="RichTextBox">rich text box</see> form with these messages. </summary>
    Public Overloads Sub ShowDialog(ByVal mdiForm As System.Windows.Forms.Form)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        MyBase.ShowDialog()
    End Sub

#End Region

End Class

''' <summary> Collection of listener forms. </summary>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="1/4/2016" by="David" revision=""> Created. </history>
Public Class ListenerFormCollection
    Inherits Collections.Generic.List(Of isr.Core.Pith.ListenerFormBase)

    ''' <summary> Adds and shows a new form,. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="form"> The form. </param>
    ''' <param name="log">  The log. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix")>
    Public Sub ShowNew(ByVal form As ListenerFormBase, ByVal log As IMessageListener)
        If form Is Nothing Then Throw New ArgumentNullException(NameOf(form))
        form.AddListener(log)
        Me.Add(form)
        AddHandler form.FormClosed, AddressOf Me.OnClosed
        form.Show()
    End Sub

    ''' <summary> Adds a form. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="form"> The form. </param>
    ''' <returns> A ListenerFormBase. </returns>
    Public Function AddForm(ByVal form As ListenerFormBase) As ListenerFormBase
        If form Is Nothing Then Throw New ArgumentNullException(NameOf(form))
        Me.Add(form)
        AddHandler form.FormClosed, AddressOf Me.OnClosed
        Return form
    End Function

    ''' <summary> Handles a member form closed event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Private Sub OnClosed(ByVal sender As Object, ByVal e As EventArgs)
        Dim f As ListenerFormBase = TryCast(sender, ListenerFormBase)
        RemoveHandler f.FormClosed, AddressOf Me.OnClosed
        Me.Remove(f)
        If f IsNot Nothing Then f.Dispose() : f = Nothing
    End Sub

    Public Sub RemoveDispose(ByVal value As ListenerFormBase)
        Dim f As ListenerFormBase = value
        If f IsNot Nothing Then
            RemoveHandler f.FormClosed, AddressOf Me.OnClosed
            Me.Remove(f)
            f.Dispose()
            f = Nothing
        End If
    End Sub

    ''' <summary>
    ''' Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    Public Sub ClearDispose()
        Do While Me.Any
            Me.RemoveDispose(Me.Item(0))
            Application.DoEvents()
        Loop
    End Sub

End Class


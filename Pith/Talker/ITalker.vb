﻿''' <summary> Interface for talker. </summary>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
Public Interface ITalker

    ''' <summary> Adds a listener. </summary>
    ''' <param name="listener"> The listener. </param>
    Sub AddListener(ByVal listener As IMessageListener)

    ''' <summary> Adds the listeners such as the top level trace messages box and log. </summary>
    ''' <param name="talker"> The talker. </param>
    Sub AddListeners(ByVal talker As ITraceMessageTalker)

    ''' <summary> Adds the listeners such as the top level trace messages box and log. </summary>
    ''' <param name="listeners"> The listeners. </param>
    Sub AddListeners(ByVal listeners As IEnumerable(Of IMessageListener))

    ''' <summary> Applies the trace level to all listeners of the specified type. </summary>
    ''' <param name="listenerType"> Type of the listener. </param>
    ''' <param name="value">        The value. </param>
    Sub ApplyListenerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType)

    ''' <summary> Gets or sets the private listeners. </summary>
    ''' <value> The private listeners. </value>
    ReadOnly Property PrivateListeners As IEnumerable(Of IMessageListener)

    ''' <summary> Adds a private listener. </summary>
    ''' <param name="listener"> The listener. </param>
    Sub AddPrivateListener(ByVal listener As IMessageListener)

    ''' <summary> Adds private listeners. </summary>
    ''' <param name="listeners"> The listeners. </param>
    Sub AddPrivateListeners(ByVal listeners As IEnumerable(Of IMessageListener))

    ''' <summary> Adds private listeners. </summary>
    ''' <param name="talker"> The talker. </param>
    Sub AddPrivateListeners(ByVal talker As ITraceMessageTalker)

    ''' <summary> Removes the private listener described by listener. </summary>
    ''' <param name="listener"> The listener. </param>
    Sub RemovePrivateListener(ByVal listener As IMessageListener)

    ''' <summary> Removes the private listeners. </summary>
    Sub RemovePrivateListeners()

    ''' <summary> Removes the listeners if the talker was not assigned. </summary>
    Sub RemoveListeners()

    ''' <summary> Removes a listeners if the talker was not assigned. </summary>
    Sub RemoveListener(ByVal listener As IMessageListener)

    ''' <summary> Removes the listeners if the talker was not assigned. </summary>
    Sub RemoveListeners(ByVal listeners As IEnumerable(Of IMessageListener))

    ''' <summary> Applies the trace level type to all talkers. </summary>
    ''' <param name="listenerType"> Type of listener. </param>
    ''' <param name="value">        The value. </param>
    Sub ApplyTalkerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType)

    ''' <summary> Applies the talker trace levels described by talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Sub ApplyTalkerTraceLevels(ByVal talker As ITraceMessageTalker)

    ''' <summary> Applies the talker listener trace levels described by talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Sub ApplyListenerTraceLevels(ByVal talker As ITraceMessageTalker)

End Interface


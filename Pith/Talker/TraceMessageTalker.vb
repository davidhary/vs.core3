﻿Imports isr.Core.Pith
''' <summary> Trace Message Talker -- broadcasts trace messages to trace message listeners. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="10/10/2014" by="David" revision=""> Created. </history>
Public Class TraceMessageTalker
    Implements ITraceMessageTalker

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    Public Sub New()
        MyBase.New
        Me._Listeners = New MessageListenerCollection
        Me._TraceMessage = TraceMessage.Empty
        Me._TraceLogLevel = TraceEventType.Information
        Me._TraceShowLevel = TraceEventType.Information
    End Sub

#End Region

#Region " LISTENERS "

    ''' <summary> Adds a listener. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="listener"> The listener. </param>
    Public Sub AddListener(ByVal listener As IMessageListener) Implements ITraceMessageTalker.AddListener
        If listener Is Nothing Then Throw New ArgumentNullException(NameOf(listener))
        Me.Listeners.Add(listener)
    End Sub

    ''' <summary> Adds the listeners such as the top level trace messages box and log. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Sub AddListeners(ByVal talker As ITraceMessageTalker) Implements ITraceMessageTalker.AddListeners
        If talker Is Nothing Then Throw New ArgumentNullException(NameOf(talker))
        Me.AddListeners(talker.Listeners)
    End Sub

    ''' <summary> Adds the listeners such as the top level trace messages box and log. </summary>
    ''' <param name="listeners"> The listeners. </param>
    Public Sub AddListeners(ByVal listeners As IEnumerable(Of IMessageListener)) Implements ITraceMessageTalker.AddListeners
        If listeners Is Nothing Then Throw New ArgumentNullException(NameOf(listeners))
        For Each listener As IMessageListener In listeners
            Me.AddListener(listener)
        Next
    End Sub

    ''' <summary> Removes the listeners if the talker was not assigned. </summary>
    Public Sub RemoveListeners() Implements ITalker.RemoveListeners
        Me.Listeners?.Clear()
    End Sub

    Private _PrivateListeners As List(Of IMessageListener)
    ''' <summary> Gets the private listeners. </summary>
    ''' <value> The private listeners. </value>
    Public ReadOnly Property PrivateListeners As IEnumerable(Of IMessageListener) Implements ITalker.PrivateListeners
        Get
            If Me._PrivateListeners Is Nothing Then _PrivateListeners = New List(Of IMessageListener)
            Return Me._PrivateListeners
        End Get
    End Property

    ''' <summary> Adds a private listener. </summary>
    ''' <param name="listener"> The listener. </param>
    Public Sub AddPrivateListener(ByVal listener As IMessageListener) Implements ITalker.AddPrivateListener
        If Me.PrivateListeners IsNot Nothing Then
            Me._PrivateListeners.Add(listener)
        End If
        Me.AddListener(listener)
    End Sub

    ''' <summary> Adds private listeners. </summary>
    ''' <param name="listeners"> The listeners. </param>
    Public Sub AddPrivateListeners(ByVal listeners As IEnumerable(Of IMessageListener)) Implements ITalker.AddPrivateListeners
        If listeners Is Nothing Then Throw New ArgumentNullException(NameOf(listeners))
        If Me.PrivateListeners IsNot Nothing Then
            Me._PrivateListeners.AddRange(listeners)
        End If
        Me.AddListeners(listeners)
    End Sub

    ''' <summary> Adds private listeners. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="talker"> The talker. </param>
    Public Sub AddPrivateListeners(ByVal talker As ITraceMessageTalker) Implements ITalker.AddPrivateListeners
        If talker Is Nothing Then Throw New ArgumentNullException(NameOf(talker))
        Me.AddPrivateListeners(talker.Listeners)
    End Sub

    ''' <summary> Removes the private listener described by listener. </summary>
    ''' <param name="listener"> The listener. </param>
    Public Sub RemovePrivateListener(ByVal listener As IMessageListener) Implements ITalker.RemovePrivateListener
        Me.RemoveListener(listener)
        If Me.PrivateListeners IsNot Nothing Then
            Me._PrivateListeners.Remove(listener)
        End If
    End Sub

    ''' <summary> Removes the private listeners. </summary>
    Public Sub RemovePrivateListeners() Implements ITalker.RemovePrivateListeners
        For Each listener As IMessageListener In Me.PrivateListeners
            Me.RemoveListener(listener)
        Next
        Me._PrivateListeners.Clear()
    End Sub

    ''' <summary> Removes the listener described by listener. </summary>
    ''' <param name="listener"> The listener. </param>
    Public Sub RemoveListener(ByVal listener As IMessageListener) Implements ITalker.RemoveListener
        Me.Listeners?.Remove(listener)
    End Sub

    Public Sub RemoveListeners(ByVal listeners As IEnumerable(Of IMessageListener)) Implements ITalker.RemoveListeners
        If listeners Is Nothing Then Throw New ArgumentNullException(NameOf(listeners))
        For Each listener As IMessageListener In listeners
            Me.RemoveListener(listener)
        Next
    End Sub

    ''' <summary> Gets the listeners. </summary>
    ''' <value> The listeners. </value>
    Public ReadOnly Property Listeners As MessageListenerCollection Implements ITraceMessageTalker.Listeners

    ''' <summary> Gets or sets the trace level. </summary>
    ''' <value> The trace level. </value>
    Public Property TraceLogLevel As TraceEventType Implements ITraceMessageTalker.TraceLogLevel

    ''' <summary> Gets or sets the trace level. </summary>
    ''' <value> The trace level. </value>
    Public Property TraceShowLevel As TraceEventType Implements ITraceMessageTalker.TraceShowLevel

    ''' <summary> Applies the listener trace level. </summary>
    ''' <param name="listenerType"> Type of the trace level. </param>
    ''' <param name="value">        The value. </param>
    Public Sub ApplyListenerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType) Implements ITalker.ApplyListenerTraceLevel
        Me.Listeners.ApplyTraceLevel(listenerType, value)
    End Sub

    ''' <summary> Applies the trace level type to all talkers. </summary>
    ''' <param name="listenerType"> Type of the trace level. </param>
    ''' <param name="value">        The value. </param>
    Public Sub ApplyTalkerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType) Implements ITalker.ApplyTalkerTraceLevel
        If listenerType = ListenerType.Logger Then
            Me.TraceLogLevel = value
        Else
            Me.TraceShowLevel = value
        End If
    End Sub

    ''' <summary> Applies the talker trace levels described by talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Sub ApplyTalkerTraceLevels(ByVal talker As ITraceMessageTalker) Implements ITraceMessageTalker.ApplyTalkerTraceLevels
        If talker Is Nothing Then Throw New ArgumentNullException(NameOf(talker))
        Me.ApplyTalkerTraceLevel(ListenerType.Logger, talker.TraceLogLevel)
        Me.ApplyTalkerTraceLevel(ListenerType.Display, talker.TraceShowLevel)
    End Sub

    ''' <summary> Applies the listener trace levels described by talker. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="talker"> The talker. </param>
    Public Sub ApplyListenerTraceLevels(ByVal talker As ITraceMessageTalker) Implements ITraceMessageTalker.ApplyListenerTraceLevels
        If talker Is Nothing Then Throw New ArgumentNullException(NameOf(talker))
        Me.ApplyListenerTraceLevel(ListenerType.Logger, talker.TraceLogLevel)
        Me.ApplyListenerTraceLevel(ListenerType.Display, talker.TraceShowLevel)
    End Sub

#End Region

#Region " PUBLISH "

    ''' <summary>
    ''' Determines if the message with the specified level is publishable on the listener type.
    ''' </summary>
    ''' <param name="listenerType"> Type of the listener. </param>
    ''' <param name="value">        The value. </param>
    ''' <returns> <c>true</c> if publishable; otherwise <c>false</c> </returns>
    Function Publishable(ByVal listenerType As ListenerType, ByVal value As TraceEventType) As Boolean Implements ITraceMessageTalker.Publishable
        Return If(listenerType = ListenerType.Logger, value <= Me.TraceLogLevel, value <= Me.TraceShowLevel)
    End Function

    ''' <summary> Publishes the message. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Public Function Publish(ByVal eventType As TraceEventType, ByVal id As Integer,
                            ByVal format As String, ByVal ParamArray args() As Object) As String Implements ITraceMessageTalker.Publish
        Return Me.Publish(New TraceMessage(eventType, id, format, args))
    End Function

    ''' <summary> Publishes the message. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Public Function Publish(ByVal value As TraceMessage) As String Implements ITraceMessageTalker.Publish
        Dim details As String = ""
        If value IsNot Nothing Then
            Me.ReportDateChanged()
            Me._TraceMessage = New TraceMessage(value)
            details = value.Details
            Me._Publish(value)
        End If
        Return details
    End Function

    ''' <summary> Publishes the message. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Private Sub _Publish(ByVal value As TraceMessage)
        If Me.Listeners IsNot Nothing Then
            For Each listener As ITraceMessageListener In Me.Listeners
                If listener IsNot Nothing AndAlso Not listener.IsDisposed AndAlso
                    Me.Publishable(listener.ListenerType, value.EventType) Then
                    listener.TraceEvent(value)
                    System.Windows.Forms.Application.DoEvents()
                End If
            Next
        End If
    End Sub

    ''' <summary> Publishes overriding the listeners trace level. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Public Function PublishOverride(ByVal eventType As TraceEventType, ByVal id As Integer,
                            ByVal format As String, ByVal ParamArray args() As Object) As String Implements ITraceMessageTalker.PublishOverride
        Return Me.PublishOverride(New TraceMessage(eventType, id, format, args))
    End Function

    ''' <summary> Publishes overriding the listeners trace level. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    ''' <returns> A String. </returns>
    Public Function PublishOverride(ByVal value As TraceMessage) As String Implements ITraceMessageTalker.PublishOverride
        Dim details As String = ""
        If value IsNot Nothing Then
            Me.ReportDateChanged()
            Me._TraceMessage = New TraceMessage(value)
            details = value.Details
            Me._PublishOverride(value)
        End If
        Return details
    End Function


    ''' <summary> Publish override. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Private Sub _PublishOverride(ByVal value As TraceMessage)
        If Me.Listeners IsNot Nothing Then
            For Each listener As ITraceMessageListener In Me.Listeners
                If listener IsNot Nothing AndAlso Not listener.IsDisposed Then
                    listener.TraceEventOverride(value)
                    System.Windows.Forms.Application.DoEvents()
                End If
            Next
        End If
    End Sub

    ''' <summary> Gets or sets a message describing the trace. </summary>
    ''' <value> A message describing the trace. </value>
    Public ReadOnly Property TraceMessage As TraceMessage Implements ITraceMessageTalker.TraceMessage

#End Region

#Region " DATE CHANGED "

    ''' <summary> Event queue for all listeners interested in DateChanged events. </summary>
    Public Event DateChanged As EventHandler(Of System.EventArgs) Implements ITraceMessageTalker.DateChanged

    ''' <summary> Raises the date changed event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Public Overridable Sub OnDateChanged(ByVal e As EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.DateChangedEvent
        evt?.Invoke(Me, System.EventArgs.Empty)
    End Sub

    ''' <summary> Gets or sets the last date. </summary>
    ''' <value> The last date. </value>
    Private Property LastDate As DateTime

    ''' <summary> Reports date changed. </summary>
    Private Sub ReportDateChanged()
        If DateTime.Now.Date > Me.LastDate Then
            Me.LastDate = DateTime.Now.Date
            Me.OnDateChanged(EventArgs.Empty)
        End If
    End Sub

#End Region

End Class


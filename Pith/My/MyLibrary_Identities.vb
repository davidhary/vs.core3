﻿Imports System.ComponentModel

Namespace My

    ''' <summary> Values that represent project trace event identifiers. </summary>
    Public Enum ProjectTraceEventId
        <Description("Not specified")> None
        <Description("Pith")> Pith = &H10 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Composites")> Composites = ProjectTraceEventId.Pith + &H1
        <Description("Controls")> [Controls] = ProjectTraceEventId.Pith + &H2
        <Description("Engineering")> Engineering = ProjectTraceEventId.Pith + &H3
        <Description("Forms")> Forms = ProjectTraceEventId.Pith + &H4
        <Description("Input Output")> InputOutput = ProjectTraceEventId.Pith + &H5
        <Description("Linq Statistics")> LinqStatistics = ProjectTraceEventId.Pith + &H6
        <Description("Message Box")> MessageBox = ProjectTraceEventId.Pith + &H7

        <Description("Diagnosis Tester")> DiagnosisTester = ProjectTraceEventId.Pith + &HA
        <Description("My Blue Splash Screen")> MyBlueSplashScreen = ProjectTraceEventId.Pith + &HB
        <Description("Exception Message Test")> ExceptionMessageTest = ProjectTraceEventId.Pith + &HC
        <Description("My Exception Message Box Test")> MyExceptionMessageBoxTest = ProjectTraceEventId.Pith + &HD

        <Description("Automata")> Automata = &H11 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Data")> Data = &H12 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Digital I/O Lan")> DigitalInputOutputLan = &H13 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("High Potential")> HighPotential = &H14 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("IO")> IO = &H15 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Modbus")> Modbus = &H16 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Net")> Net = &H17 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Rich Text Box")> RichTextBox = &H18 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Open Layers")> OpenLayers = &H19 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Optima")> Optima = &H20 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Serial")> Serial = &H21 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Signals")> Signals = &H22 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Thermal Transient")> ThermalTransient = &H23 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Universal Library")> UniversalLibrary = &H24 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Visualizing")> Visualizing = &H25 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Wisdom")> Wisdom = &H26 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Virtual Instruments")> VirtualInstruments = &H10 * TraceEventConstants.SolutionNamespaceSize

        <Description("Application Namespace Identity")> ApplicationNamespace = TraceEventConstants.ApplicationNamespaceIdentity

    End Enum

End Namespace

﻿Imports System.ComponentModel
<Assembly: Runtime.CompilerServices.InternalsVisibleTo(My.MyLibrary.TestAssemblyStrongName)>
Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <license>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="11/26/2015" by="David" revision=""> Created. </history>
    Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ' ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = ProjectTraceEventId.Pith

        Public Const AssemblyTitle As String = "Core Pith Library"
        Public Const AssemblyDescription As String = "Core Pith Library"
        Public Const AssemblyProduct As String = "Core.Pith.2018"
        Public Const TestAssemblyStrongName As String = "isr.Core.Pith.Tests,PublicKey=" & isr.Core.Pith.My.SolutionInfo.PublicKey

    End Class

End Namespace


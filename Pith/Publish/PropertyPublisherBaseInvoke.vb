﻿Imports System.Threading
Imports System.ComponentModel
Imports isr.Core.Pith.EventHandlerExtensions
Partial Public MustInherit Class PropertyPublisherBase
    Inherits PropertyNotifyBase
    Implements IPublisher

#Region " PROPERTY CHANGED EVENT IMPLEMENTATION "

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overrides Sub InvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then MyBase.InvokePropertyChanged(e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overrides Sub SafeInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then MyBase.SafeInvokePropertyChanged(e)
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overrides Sub SafeBeginInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then MyBase.SafeBeginInvokePropertyChanged(e)
    End Sub

#End Region

#Region " NOTIFICATIONS "

    ''' <summary>
    ''' Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins Invoke
    ''' or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value.
    ''' </summary>
    ''' <param name="e"> Property Changed event information. </param>
    Protected Overrides Sub AsyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then MyBase.AsyncNotifyPropertyChanged(e)
    End Sub

    ''' <summary>
    ''' Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes or
    ''' Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property Value.
    ''' </summary>
    ''' <param name="e"> Property Changed event information. </param>
    Protected Overrides Sub SyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then MyBase.SyncNotifyPropertyChanged(e)
    End Sub

#End Region

#End Region

#Region " PROPERTY CHANGING EVENT IMPLEMENTATION "

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property Value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overrides Sub InvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        If Publishable Then MyBase.InvokePropertyChanging(e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property Value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overrides Sub SafeInvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        If Me.Publishable Then MyBase.SafeInvokePropertyChanging(e)
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanging">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overrides Sub SafeBeginInvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        If Me.Publishable Then MyBase.SafeBeginInvokePropertyChanging(e)
    End Sub

#End Region

#Region " SYNCHRONOUS NOTIFICATIONS "

    ''' <summary>
    ''' Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes or
    ''' Dynamically Invokes) a change <see cref="PropertyChanging">event</see> in Property Value.
    ''' </summary>
    ''' <param name="e"> Property Changing event information. </param>
    Protected Overrides Sub SyncNotifyPropertyChanging(ByVal e As PropertyChangingEventArgs)
        If Me.Publishable Then MyBase.SyncNotifyPropertyChanging(e)
    End Sub

#End Region

#Region " ASYNCHRONOUS NOTIFICATIONS "

    ''' <summary>
    ''' Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins Invoke
    ''' or Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property Value.
    ''' </summary>
    ''' <param name="e"> Property Changing event information. </param>
    Protected Overrides Sub AsyncNotifyPropertyChanging(ByVal e As PropertyChangingEventArgs)
        If Me.Publishable Then MyBase.AsyncNotifyPropertyChanging(e)
    End Sub

#End Region

#End Region

#Region " SAFE EVENTS "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    Protected Sub SafePublish(ByVal handler As EventHandler(Of ComponentModel.CancelEventArgs),
                              ByVal e As ComponentModel.CancelEventArgs)
        If Me.Publishable Then handler.SafeInvoke(Me, e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    Protected Sub SafePublish(ByVal handler As EventHandler(Of System.EventArgs))
        If Me.Publishable Then handler.SafeInvoke(Me)
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    Protected Sub SafeBeginPublish(ByVal handler As EventHandler(Of System.EventArgs))
        If Me.Publishable Then handler.SafeBeginInvoke(Me)
    End Sub

#End Region

End Class


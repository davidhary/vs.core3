﻿''' <summary>  Defines the contract that must be implemented by publishers. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/22/13" by="David" revision="1.2.5013"> Created. </history>
Public MustInherit Class PublisherBase
    Implements IPublisher

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="PublisherBase" /> class. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " PUBLISHER BASE "

    ''' <summary> Gets or sets a value indicating whether elements can be published. </summary>
    ''' <value> <c>True</c> if elements are publishable; otherwise, <c>False</c>. </value>
    Protected Property Publishable As Boolean Implements IPublisher.Publishable

    ''' <summary> Publishes all elements. </summary>
    Public MustOverride Sub Publish() Implements IPublisher.Publish

    ''' <summary> Resumes publishing. </summary>
    Public Overridable Sub ResumePublishing() Implements IPublisher.ResumePublishing
        Me.Publishable = True
    End Sub

    ''' <summary> Suspends publishing. </summary>
    Public Overridable Sub SuspendPublishing() Implements IPublisher.SuspendPublishing
        Me.Publishable = False
    End Sub

#End Region

End Class

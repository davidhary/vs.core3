﻿Imports System.Threading
Imports System.ComponentModel
Imports isr.Core.Pith.EventHandlerExtensions
Partial Public Class PropertyPublisherBase
    Implements IPublisher

#Region " PUBLISHER BASE "

    ''' <summary> Gets or sets a value indicating whether elements can be published. </summary>
    ''' <value> <c>True</c> if elements are publishable; otherwise, <c>False</c>. </value>
    Protected ReadOnly Property Publishable As Boolean Implements IPublisher.Publishable

    ''' <summary> Publishes all elements. </summary>
    Public MustOverride Sub Publish() Implements IPublisher.Publish

    ''' <summary> Resumes publishing. </summary>
    Public Overridable Sub ResumePublishing() Implements IPublisher.ResumePublishing
        Me._Publishable = True
        Me.SafePostPropertyChanged(NameOf(PropertyPublisherBase.Publishable))
    End Sub

    ''' <summary> Suspends publishing. </summary>
    Public Overridable Sub SuspendPublishing() Implements IPublisher.SuspendPublishing
        Me._Publishable = False
        Me.SafePostPropertyChanged(NameOf(PropertyPublisherBase.Publishable))
    End Sub

#End Region

#Region " PROPERTY CHANGED EVENT IMPLEMENTATION "

#Region " SEND/POST "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overrides Sub SafeSendPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then MyBase.SafeSendPropertyChanged(e)
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overrides Sub SafePostPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then MyBase.SafePostPropertyChanged(e)
    End Sub

#End Region

#End Region

#Region " PROPERTY CHANGING EVENT IMPLEMENTATION "

#Region " SEND/POST "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property Value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overrides Sub SafeSendPropertyChanging(ByVal e As PropertyChangingEventArgs)
        If Me.Publishable Then MyBase.SafeSendPropertyChanging(e)
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanging">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overrides Sub SafePostPropertyChanging(ByVal e As PropertyChangingEventArgs)
        If Me.Publishable Then MyBase.SafePostPropertyChanging(e)
    End Sub

#End Region

#End Region

#Region " SAFE EVENTS "

    ''' <summary> Synchronously notifies an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    Protected Sub SafeSyncPublish(ByVal handler As EventHandler(Of System.EventArgs))
        If Me.Publishable Then handler.SafeSend(Me)
    End Sub

    ''' <summary> Asynchronously notifies an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    Protected Sub SafeAsyncPublish(ByVal handler As EventHandler(Of System.EventArgs))
        If Me.Publishable Then handler.SafePost(Me)
    End Sub

#End Region

End Class


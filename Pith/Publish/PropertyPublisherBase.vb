﻿Imports System.Threading
Imports System.ComponentModel
Imports isr.Core.Pith.EventHandlerExtensions
''' <summary>  Defines the contract that must be implemented by property publishers. </summary>
''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/28/12" by="David" revision="1.2.4654"> Documented. </history>
Public MustInherit Class PropertyPublisherBase
    Inherits PropertyNotifyBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="PropertyPublisherBase" /> class. </summary>
    Protected Sub New()
        MyBase.New()
        Me._Publishable = True
    End Sub

#End Region

End Class


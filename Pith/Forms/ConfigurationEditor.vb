﻿Imports isr.Core.Pith.ExceptionExtensions
''' <summary> Editor for configuration. </summary>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/31/2016" by="David" revision=""> Created. </history>
Public Class ConfigurationEditor
    Inherits Core.Pith.FormBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Prevents a default instance of the <see cref="ConfigurationEditor" /> class from being
    ''' created. </summary>
    Private Sub New()

        MyBase.New()

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

    End Sub

    ''' <summary> Gets the locking object to enforce thread safety when creating the singleton
    ''' instance. </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property _Instance As ConfigurationEditor

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As ConfigurationEditor
        If Not Instantiated Then
            SyncLock _SyncLocker
                _Instance = New ConfigurationEditor
            End SyncLock
        End If
        Return _Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock _SyncLocker
                Return _Instance IsNot Nothing AndAlso Not _Instance.IsDisposed
            End SyncLock
        End Get
    End Property

    ''' <summary> Dispose instance. </summary>
    Public Shared Sub DisposeInstance()
        SyncLock _SyncLocker
            If _Instance IsNot Nothing AndAlso Not _Instance.IsDisposed Then
                _Instance.Dispose()
                _Instance = Nothing
            End If
        End SyncLock
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                          <c>False</c> to release only unmanaged resources when called from the
    '''                          runtime finalize. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If components IsNot Nothing Then components.Dispose() : components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " SHOW "

    ''' <summary> Hides the dialog. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> A DialogResult. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2222:DoNotDecreaseInheritedMemberVisibility")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Shadows Function ShowDialog(owner As System.Windows.Forms.IWin32Window) As System.Windows.Forms.DialogResult
        Me.Text = "Illegal Call"
        Return MyBase.ShowDialog(owner)
    End Function

    ''' <summary> Hides the dialog. </summary>
    ''' <returns> A System.Windows.Forms.DialogResult. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2222:DoNotDecreaseInheritedMemberVisibility")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Shadows Function ShowDialog() As System.Windows.Forms.DialogResult
        Me.Text = "Illegal Call"
        Return MyBase.ShowDialog()
    End Function

    ''' <summary> Shows the dialog. </summary>
    ''' <param name="settings"> Options for controlling the operation. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shadows Function ShowDialog(ByVal settings As System.Configuration.ApplicationSettingsBase) As System.Windows.Forms.DialogResult
        Return Me.ShowDialog(Nothing, settings)
    End Function

    ''' <summary> Shows the dialog. </summary>
    ''' <param name="mdiForm">  The MDI form. </param>
    ''' <param name="settings"> Options for controlling the operation. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shadows Function ShowDialog(ByVal mdiForm As System.Windows.Forms.Form, ByVal settings As System.Configuration.ApplicationSettingsBase) As System.Windows.Forms.DialogResult
        Me.Show(mdiForm, settings)
        Do While Me.Visible
            System.Windows.Forms.Application.DoEvents()
        Loop
        Return Me.DialogResult
    End Function

    ''' <summary> Displays this dialog with title 'illegal call'. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2222:DoNotDecreaseInheritedMemberVisibility")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Shadows Sub Show()
        Me.Text = "Illegal Call"
        MyBase.Show()
    End Sub

    ''' <summary> Shows this form. </summary>
    ''' <param name="settings"> Options for controlling the operation. </param>
    Public Shadows Sub Show(ByVal settings As System.Configuration.ApplicationSettingsBase)
        Me.Show(Nothing, settings)
    End Sub

    ''' <summary> Shows this form. </summary>
    ''' <param name="mdiForm">  The MDI form. </param>
    ''' <param name="settings"> Options for controlling the operation. </param>
    Public Shadows Sub Show(ByVal mdiForm As System.Windows.Forms.Form, ByVal settings As System.Configuration.ApplicationSettingsBase)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        Me._Settings = settings
        MyBase.Show()
    End Sub

#End Region

#Region " FORM EVENTS "

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub OnShown(e As EventArgs)
        Try
            Me._PropertyGrid.SelectedObject = Me._Settings
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
        Finally
            MyBase.OnShown(e)
        End Try
    End Sub

#End Region

#Region " HANDLERS "

    Private Sub _CancelButton_Click(sender As Object, e As EventArgs) Handles _IgnoreButton.Click
        Me.Close()
    End Sub

    Private Sub _AcceptButton_Click(sender As Object, e As EventArgs) Handles _AcceptButton.Click
        Me.SaveExit(sender, e)
    End Sub

    Private _Settings As System.Configuration.ApplicationSettingsBase
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _ReadMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadMenuItem.Click
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._PropertyGrid.SelectedObject = Nothing
            System.Windows.Forms.Application.DoEvents()
            Threading.Thread.Sleep(100)
            Me._PropertyGrid.SelectedObject = Me._Settings
        Catch ex As Exception
            Windows.Forms.MessageBox.Show(ex.ToFullBlownString, "Exception reading Settings",
                                          Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Error, Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _SaveMenuItem_Click(sender As Object, e As EventArgs) Handles _SaveMenuItem.Click
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._Settings.Save()
            System.Windows.Forms.Application.DoEvents()
            Threading.Thread.Sleep(100)
        Catch ex As Exception
            Windows.Forms.MessageBox.Show(ex.ToFullBlownString, "Exception saving Settings",
                                          Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Error, Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub SaveExit(sender As Object, e As EventArgs)
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._Settings.Save()
            System.Windows.Forms.Application.DoEvents()
            Threading.Thread.Sleep(100)
            Me.Close()
        Catch ex As Exception
            Windows.Forms.MessageBox.Show(ex.ToFullBlownString, "Exception saving Settings",
                                          Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Error, Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    Private Sub _SaveExitMenuItem_Click(sender As Object, e As EventArgs) Handles _SaveExitMenuItem.Click
        Me.SaveExit(sender, e)
    End Sub

    Private Sub _ExitMenuItem_Click(sender As Object, e As EventArgs) Handles _ExitMenuItem.Click
        Me.Close()
    End Sub

#End Region

End Class
Imports System.Drawing
Imports System.Windows.Forms
''' <summary> Form with drop shadow. </summary>
''' <license> (c) 2007 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="08/13/2007" by="Nicholas Seward" revision="1.0.2781.x"> http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx. 
''' http://www.codeproject.com/Articles/1108900/Resize-and-Drag-a-FormBorderStyle-None-Form-in-NET </history>
''' <history date="08/13/2007" by="David" revision="1.0.2781.x">           Convert from C#. </history>
Public Class NakedFormBase
    Inherits Form

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    Protected Sub New()
        MyBase.New()
        Me.InitializeComponent()
    End Sub

    ''' <summary> Initializes the component. </summary>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(331, 341)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = My.Resources.favicon
        Me.Name = "FormBase"
        Me.ResumeLayout(False)
    End Sub

#Region " CLASS STYLE "

    ''' <summary> The enable drop shadow version. </summary>
    Public Const EnableDropShadowVersion As Integer = 5

    ''' <summary> Gets or sets the class style. </summary>
    ''' <value> The class style. </value>
    Protected Property ClassStyle As ClassStyleConstants = ClassStyleConstants.None

    ''' <summary> Adds a drop shadow parameter. </summary>
    ''' <remarks> From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx. </remarks>
    ''' <value> Options that control the create. </value>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                                        Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CInt(Me.ClassStyle)
            Return cp
        End Get
    End Property

#End Region

#End Region

#Region " SAFE NATIVE METHODS"

    Private NotInheritable Class SafeNativeMethods
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Sends a message. </summary>
        ''' <param name="hWnd">   The window. </param>
        ''' <param name="msg">    The message. </param>
        ''' <param name="wParam"> The parameter. </param>
        ''' <param name="lParam"> The parameter. </param>
        ''' <returns> An Int32. </returns>
        <Runtime.InteropServices.DllImport("user32.dll")>
        Friend Shared Function SendMessage(ByVal hWnd As IntPtr,
                                        ByVal msg As UInt32,
                                        ByVal wParam As UIntPtr,
                                        ByVal lParam As IntPtr) As IntPtr
        End Function

        <CodeAnalysis.SuppressMessage("Microsoft.Interoperability", "CA1414:MarkBooleanPInvokeArgumentsWithMarshalAs")>
        <Runtime.InteropServices.DllImportAttribute("user32.dll")>
        Friend Shared Function ReleaseCapture() As Boolean
        End Function

    End Class

    Private WM_NCLBUTTONDOWN As UInt32 = &HA1
    Private HT_CAPTION As UIntPtr = New UIntPtr(&H2)
    ' Private HTBORDER As UIntPtr = New UIntPtr(18)
    Private HTBOTTOM As UIntPtr = New UIntPtr(15)
    Private HTBOTTOMLEFT As UIntPtr = New UIntPtr(16)
    Private HTBOTTOMRIGHT As UIntPtr = New UIntPtr(17)
    Private HTLEFT As UIntPtr = New UIntPtr(10)
    Private HTRIGHT As UIntPtr = New UIntPtr(11)
    Private HTTOP As UIntPtr = New UIntPtr(12)
    Private HTTOPLEFT As UIntPtr = New UIntPtr(13)
    Private HTTOPRIGHT As UIntPtr = New UIntPtr(14)

    Private Enum onBorder
        None = 0
        Top = 1
        Right = 2
        Bottom = 4
        Left = 8
        TopRight = 3
        RightBottom = 6
        LeftTop = 9
        BottomLeft = 12
    End Enum

#End Region

#Region " FORM EVENT HANDLERS "

    Private testOnBorder As onBorder
    Private BorderWidth As Integer = 8

    Private Function InPanelBorder(ByVal pos As Point) As onBorder
        Dim pointMe As Point = New Point(0, 0)
        Dim result As onBorder = onBorder.None
        If pos.Y < pointMe.Y + BorderWidth Then result = result Or onBorder.Top
        If pos.Y > pointMe.Y + Me.Height - BorderWidth Then result = result Or onBorder.Bottom
        If pos.X < pointMe.X + BorderWidth Then result = result Or onBorder.Left
        If pos.X > pointMe.X + Me.Width - BorderWidth Then result = result Or onBorder.Right
        Return result
    End Function

    Protected Overrides Sub OnMouseMove(e As MouseEventArgs)
        If e Is Nothing Then Return
        testOnBorder = inPanelBorder(e.Location)
        Select Case testOnBorder
            Case onBorder.None
                Me.Cursor = Cursors.Arrow
            Case onBorder.Top
                Me.Cursor = Cursors.SizeNS
            Case onBorder.Right
                Me.Cursor = Cursors.SizeWE
            Case onBorder.TopRight
                Me.Cursor = Cursors.SizeNESW
            Case onBorder.RightBottom
                Me.Cursor = Cursors.SizeNWSE
            Case onBorder.Bottom
                Me.Cursor = Cursors.SizeNS
            Case onBorder.BottomLeft
                Me.Cursor = Cursors.SizeNESW
            Case onBorder.Left
                Me.Cursor = Cursors.SizeWE
            Case onBorder.LeftTop
                Me.Cursor = Cursors.SizeNWSE
        End Select
    End Sub

    Protected Overrides Sub OnMouseDown(e As MouseEventArgs)
        Dim dir As UIntPtr = New UIntPtr(0)
        Select Case testOnBorder
            Case onBorder.Top
                dir = HTTOP
            Case onBorder.TopRight
                dir = HTTOPRIGHT
            Case onBorder.Right
                dir = HTRIGHT
            Case onBorder.RightBottom
                dir = HTBOTTOMRIGHT
            Case onBorder.Bottom
                dir = HTBOTTOM
            Case onBorder.BottomLeft
                dir = HTBOTTOMLEFT
            Case onBorder.Left
                dir = HTLEFT
            Case onBorder.LeftTop
                dir = HTTOPLEFT
            Case Else
                dir = HT_CAPTION
        End Select
        SafeNativeMethods.ReleaseCapture()
        SafeNativeMethods.SendMessage(Me.Handle, WM_NCLBUTTONDOWN, dir, New IntPtr(0))
    End Sub

#End Region

End Class


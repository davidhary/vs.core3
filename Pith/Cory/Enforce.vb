﻿''' <summary> An enforce. </summary>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="11/28/2016" by="David" revision=""> Created. </history>
Public NotInheritable Class Enforce

    Private Sub New()
    End Sub

    ''' <summary> Argument not null. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="argument"> The argument. </param>
    ''' <param name="message">  The message. </param>
    Public Shared Sub ArgumentNotNull(Of T As Class)(ByVal argument As T, ByVal message As String)
        If argument Is Nothing Then Throw New ArgumentNullException(message)
    End Sub

    ''' <summary> Argument not empty. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="argument">    The argument. </param>
    ''' <param name="message"> The message. </param>
    Public Shared Sub ArgumentNotEmpty(ByVal argument As String, ByVal message As String)
        If String.IsNullOrWhiteSpace(argument) Then Throw New ArgumentNullException(message)
    End Sub

End Class


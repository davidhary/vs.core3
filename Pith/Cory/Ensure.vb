﻿''' <summary> A helper class for ensuring parameter conditions. </summary>
''' <license> (c) 2016 Cory Charlton.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="12/12/2016" by="David" revision="3.1.6192.x"> https://github.com/CoryCharlton/CCSWE.Core. </history>
Public NotInheritable Class Ensure

    Private Sub New()
    End Sub

    Private Shared Function GetException(Of TException As {Exception, New})(ByVal name As String, ByVal message As String) As Exception
        Dim exception As Exception = CType(Activator.CreateInstance(GetType(TException), message), TException)

        If TypeOf exception Is ArgumentNullException Then
            Return New ArgumentNullException(name, message)
        End If

        If TypeOf exception Is ArgumentOutOfRangeException Then
            Return New ArgumentOutOfRangeException(name, message)
        End If

        If TypeOf exception Is ArgumentException Then
            Return New ArgumentException(message, name)
        End If

        Return exception
    End Function

    ''' <summary>
    ''' Throws an <see cref="ArgumentOutOfRangeException"/> if the expression evaluates to <c>false</c>.
    ''' </summary>
    ''' <param name="name">The name of the parameter we are validating.</param>
    ''' <param name="expression">The expression that will be evaluated.</param>
    ''' <exception cref="ArgumentOutOfRangeException">Thrown when the expression evaluates to <c>false</c></exception>.
    Public Shared Sub IsInRange(ByVal name As String, ByVal expression As Boolean)
        Ensure.IsValid(Of ArgumentOutOfRangeException)(name, expression, $"The value passed for '{name}' is out of range.")
    End Sub

    ''' <summary>
    ''' Throws an <see cref="ArgumentNullException"/> if the value is <c>null</c>.
    ''' </summary>
    ''' <param name="name">The name of the parameter we are validating.</param>
    ''' <param name="value">The value that will be evaluated.</param>
    ''' <exception cref="ArgumentNullException">Thrown when the value is <c>null</c></exception>.
    Public Shared Sub IsNotNull(Of T As Class)(ByVal name As String, ByVal value As T)
        Ensure.IsValid(Of ArgumentNullException)(name, value IsNot Nothing, $"The value passed for '{name}' is null.")
    End Sub

    ''' <summary>
    ''' Throws an <see cref="ArgumentException"/> if the value is <c>null</c> or <c>whitespace</c>.
    ''' </summary>
    ''' <param name="name">The name of the parameter we are validating.</param>
    ''' <param name="value">The value that will be evaluated.</param>
    ''' <exception cref="ArgumentException">Thrown when the value is <c>null</c> or <c>whitespace</c>.</exception>.
    Public Shared Sub IsNotNullOrWhiteSpace(ByVal name As String, ByVal value As String)
        Ensure.IsValid(Of ArgumentException)(name, (Not String.IsNullOrWhiteSpace(value)), $"The value passed for '{name}' is empty, null, or whitespace.")
    End Sub

    ''' <summary>
    ''' Throws an <see cref="ArgumentException"/> if the expression evaluates to <c>false</c>.
    ''' </summary>
    ''' <param name="name">The name of the parameter we are validating.</param>
    ''' <param name="expression">The expression that will be evaluated.</param>
    ''' <exception cref="ArgumentException">Thrown when the expression evaluates to <c>false</c></exception>.
    Public Shared Sub IsValid(ByVal name As String, ByVal expression As Boolean)
        Ensure.IsValid(Of ArgumentException)(name, expression, $"The value passed for '{name}' is not valid.")
    End Sub

    ''' <summary>
    ''' Throws an exception if the expression evaluates to <c>false</c>.
    ''' </summary>
    ''' <typeparam name="TException">The type of <see cref="Exception"/> to throw.</typeparam>
    ''' <param name="name">The name of the parameter we are validating.</param>
    ''' <param name="expression">The expression that will be evaluated.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")>
    Public Shared Sub IsValid(Of TException As {Exception, New})(ByVal name As String, ByVal expression As Boolean)
        Ensure.IsValid(Of TException)(name, expression, $"The value passed for '{name}' is not valid.")
    End Sub

    ''' <summary>
    ''' Throws an exception if the expression evaluates to <c>false</c>.
    ''' </summary>
    ''' <typeparam name="TException">The type of <see cref="Exception"/> to throw.</typeparam>
    ''' <param name="name">The name of the parameter we are validating.</param>
    ''' <param name="expression">The expression that will be evaluated.</param>
    ''' <param name="message">The message associated with the <see cref="Exception"/></param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")>
    Public Shared Sub IsValid(Of TException As {Exception, New})(ByVal name As String, ByVal expression As Boolean, ByVal message As String)
        If expression Then
            Return
        Else
            Throw Ensure.GetException(Of TException)(name, message)
        End If
    End Sub

End Class


﻿

Imports System.Management
Imports Microsoft.Win32
''' <summary> A sealed class designed to provide machine information for the assembly. </summary>
''' <remarks>
''' <para>Machine identification (c) 2016 Zeev Goldstein
''' http://www.codeproject.com/Tips/1125745/Machine-Finger-Print-The-Right-and-Efficient-Way#_comments
''' </para>. </remarks>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="9/8/2016" by="David" revision=""> Created. </history>
Public NotInheritable Class MachineInfo

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="ApplicationInfo" /> class. </summary>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " REGISTERY ACCESS "

    ''' <summary> Reads a key from the registry. </summary>
    ''' <param name="hive">          The hive. </param>
    ''' <param name="keyFolderName"> Pathname of the key folder. </param>
    ''' <param name="keyName">       Name of the key. </param>
    ''' <param name="defaultValue">  The default value. </param>
    ''' <returns> The key. </returns>
    Public Shared Function ReadRegistry(ByVal hive As RegistryHive, ByVal keyFolderName As String, ByVal keyName As String, ByVal defaultValue As String) As String
        Dim rv As RegistryView = RegistryView.Registry32
        If Environment.Is64BitOperatingSystem Then
            rv = RegistryView.Registry64
        Else
            rv = RegistryView.Registry32
        End If
        Dim readValue As Object = Nothing
        Using regKeyBase As RegistryKey = RegistryKey.OpenBaseKey(hive, rv)
            Using regKey As RegistryKey = regKeyBase.OpenSubKey(keyFolderName, RegistryKeyPermissionCheck.ReadSubTree)
                readValue = regKey.GetValue(keyName, CObj(defaultValue))
            End Using
        End Using
        If readValue IsNot Nothing Then defaultValue = readValue.ToString()
        Return defaultValue
    End Function

#End Region

#Region " MACHINE IDENTIFICATION (c) 2016 Zeev Goldstein "

    Private Shared _WindowsGuid As String

    ''' <summary> Windows unique identifier. </summary>
    ''' <returns> A String. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Shared Function WindowsGuid() As String
        Try
            If String.IsNullOrEmpty(_WindowsGuid) Then
                Dim rv As RegistryView = RegistryView.Registry32
                If Environment.Is64BitOperatingSystem Then
                    rv = RegistryView.Registry64
                Else
                    rv = RegistryView.Registry32
                End If
                Dim readValue As Object = Nothing
                Dim defaultValue As String = "defaultValue"
                Using regKeyBase As RegistryKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, rv)
                    Using regKey As RegistryKey = regKeyBase.OpenSubKey("SOFTWARE\Microsoft\Cryptography", RegistryKeyPermissionCheck.ReadSubTree)
                        readValue = regKey.GetValue("MachineGuid", CObj(defaultValue))
                    End Using
                End Using
                If readValue IsNot Nothing AndAlso readValue.ToString() <> "defaultValue" Then
                    MachineInfo._WindowsGuid = readValue.ToString()
                End If
            End If
        Catch ex As Exception
            MachineInfo._WindowsGuid = ""
        End Try
        Return MachineInfo._WindowsGuid
    End Function

    Private Shared _MachineUUID As String

    ''' <summary> Machine UUID. </summary>
    ''' <returns> A String. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Shared Function MachineUuid() As String
        Try
            If String.IsNullOrEmpty(MachineInfo._MachineUUID) Then
                Using searcher As New ManagementObjectSearcher("root\CIMV2", "SELECT * FROM Win32_ComputerSystemProduct")
                    For Each queryObj As ManagementObject In searcher.Get()
                        MachineInfo._MachineUUID += CStr(queryObj("UUID"))
                    Next
                End Using
            End If
        Catch err As ManagementException
        End Try
        Return MachineInfo._MachineUUID
    End Function

    ''' <summary> Builds machine unique identifier. </summary>
    ''' <param name="preSalt">   The pre-salt. </param>
    ''' <param name="postSalt">  The post salt. </param>
    ''' <param name="algorithm"> The algorithm. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildMachineUniqueId(ByVal preSalt As String, ByVal postSalt As String, ByVal algorithm As Security.Cryptography.HashAlgorithm) As String
        If algorithm Is Nothing Then Throw New ArgumentNullException(NameOf(algorithm))
        Dim hashedUniqueId As New System.Text.StringBuilder
        Dim uniqueId As String = $"{preSalt}{MachineInfo.WindowsGuid}{MachineInfo.MachineUuid}{postSalt}"
        Dim bytesToHash() As Byte = System.Text.Encoding.ASCII.GetBytes(uniqueId)
        bytesToHash = algorithm.ComputeHash(bytesToHash)
        For Each b As Byte In bytesToHash
            hashedUniqueId.Append(b.ToString("x2"))
        Next
        Return hashedUniqueId.ToString
    End Function

    Private Shared _MachineUniqueIdMD5 As String

    ''' <summary> Builds a machine unique identifier using insecure MD5 hash. </summary>
    ''' <returns> A String. </returns>
    Public Shared Function BuildMachineUniqueIdMD5() As String
        If String.IsNullOrWhiteSpace(MachineInfo._MachineUniqueIdMD5) Then
            Using algorithm As New Security.Cryptography.MD5CryptoServiceProvider
                MachineInfo._MachineUniqueIdMD5 = MachineInfo.BuildMachineUniqueId("20", "16", algorithm)
            End Using
        End If
        Return MachineInfo._MachineUniqueIdMD5
    End Function

    Private Shared _MachineUniqueIdSha1 As String

    ''' <summary> Builds a machine unique identifier using SHA1. </summary>
    ''' <returns> A String. </returns>
    Public Shared Function BuildMachineUniqueIdSha1() As String
        If String.IsNullOrWhiteSpace(MachineInfo._MachineUniqueIdSha1) Then
            Using algorithm As New Security.Cryptography.SHA1CryptoServiceProvider
                MachineInfo._MachineUniqueIdSha1 = MachineInfo.BuildMachineUniqueId("20", "16", algorithm)
            End Using
        End If
        Return MachineInfo._MachineUniqueIdSha1
    End Function

#End Region

End Class

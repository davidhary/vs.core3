﻿''' <summary> A sealed class designed to provide application log access to the library. </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/02/2011" by="David" revision="x.x.4050.x"> Created </history>
Public NotInheritable Class EmbeddedResourceManager

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="EmbeddedResourceManager" /> class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " EMBEDDED RESOURCES  "

    ''' <summary> Builds full resource name. </summary>
    ''' <param name="assembly">     The assembly. </param>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> The full resource name, which starts with the Assembly name. </returns>
    Public Shared Function BuildFullResourceName(ByVal assembly As System.Reflection.Assembly, ByVal resourceName As String) As String
        If assembly Is Nothing Then Throw New ArgumentNullException(NameOf(assembly))
        If String.IsNullOrWhiteSpace(resourceName) Then
            Return String.Empty
        Else
            Dim assemblyName As String = assembly.GetName().Name
            If resourceName.StartsWith(assemblyName, StringComparison.OrdinalIgnoreCase) Then
                Return resourceName
            Else
                Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0}.{1}", assembly.GetName().Name, resourceName)
            End If
        End If
    End Function

    ''' <summary> Queries if a given embedded resource exists. </summary>
    ''' <param name="assembly">     The assembly. </param>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> <c>True</c> if the resource exists; otherwise, <c>False</c>. </returns>
    Public Shared Function EmbeddedResourceExists(ByVal assembly As System.Reflection.Assembly, ByVal resourceName As String) As Boolean
        If assembly Is Nothing Then Throw New ArgumentNullException(NameOf(assembly))
        ' Retrieve a list of resource names contained by the assembly.
        Dim resourceNames As String() = assembly.GetManifestResourceNames()
        Return resourceNames.Contains(EmbeddedResourceManager.BuildFullResourceName(assembly, resourceName), StringComparer.OrdinalIgnoreCase)
    End Function

    ''' <summary>
    ''' Tries reading text from an embedded resource file. Returns empty if not found.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="assembly">     The assembly. </param>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> The embedded text resource. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")>
    Public Shared Function TryReadEmbeddedTextResource(ByVal assembly As System.Reflection.Assembly,
                                                       ByVal resourceName As String) As String
        If assembly Is Nothing Then Throw New ArgumentNullException(NameOf(assembly))
        Dim contents As String = ""
        Using resourceStream As System.IO.Stream =
            assembly.GetManifestResourceStream(EmbeddedResourceManager.BuildFullResourceName(assembly, resourceName))
            If resourceStream IsNot Nothing Then
                Using sr As New System.IO.StreamReader(resourceStream)
                    contents = sr.ReadToEnd()
                End Using
            End If
        End Using
        Return contents
    End Function

    ''' <summary> Tries to read embedded image resource. Returns nothing if not found. </summary>
    ''' <param name="assembly">     The assembly. </param>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> A System.Drawing.Image. </returns>
    Public Shared Function TryReadEmbeddedImageResource(ByVal assembly As System.Reflection.Assembly,
                                                        ByVal resourceName As String) As System.Drawing.Image
        If assembly Is Nothing Then Throw New ArgumentNullException(NameOf(assembly))
        Using resourceStream As System.IO.Stream =
                    assembly.GetManifestResourceStream(EmbeddedResourceManager.BuildFullResourceName(assembly, resourceName))
            If resourceStream Is Nothing Then
                Return Nothing
            Else
                Return System.Drawing.Image.FromStream(resourceStream)
            End If
        End Using
    End Function

    ''' <summary> Try read embedded GIF resource. Returns nothing if not found. </summary>
    ''' <param name="assembly">     The assembly. </param>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> A System.Drawing.Image. </returns>
    Public Shared Function TryReadEmbeddedGifResource(ByVal assembly As System.Reflection.Assembly,
                                                      ByVal resourceName As String) As System.Drawing.Image
        If assembly Is Nothing Then Throw New ArgumentNullException(NameOf(assembly))
        Dim resourceStream As System.IO.Stream =
                    assembly.GetManifestResourceStream(EmbeddedResourceManager.BuildFullResourceName(assembly, resourceName))
        If resourceStream Is Nothing Then
            Return Nothing
        Else
            Return System.Drawing.Image.FromStream(resourceStream)
        End If
    End Function

    ''' <summary> Try read embedded icon resource. Returns nothing if not found. </summary>
    ''' <param name="assembly">     The assembly. </param>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> A System.Drawing.Icon. </returns>
    Public Shared Function TryReadEmbeddedIconResource(ByVal assembly As System.Reflection.Assembly,
                                                       ByVal resourceName As String) As System.Drawing.Icon
        If assembly Is Nothing Then Throw New ArgumentNullException(NameOf(assembly))
        Using resourceStream As System.IO.Stream =
                    assembly.GetManifestResourceStream(EmbeddedResourceManager.BuildFullResourceName(assembly, resourceName))
            If resourceStream Is Nothing Then
                Return Nothing
            Else
                Return New System.Drawing.Icon(resourceStream)
            End If
        End Using
    End Function

#End Region

End Class

﻿Imports System.Collections.Concurrent
Imports System.Threading
Imports System.Threading.Tasks
Imports isr.Core.Pith
Imports isr.Core.Pith.DiagnosticsExtensions
Imports isr.Core.Pith.ExceptionExtensions
''' <summary> Extends the <see cref="Microsoft.VisualBasic.Logging.Log">log</see>. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="2/16/2014" by="David" revision=""> Created. </history>
Public Class MyLog
    Inherits Logging.Log
    Implements IDisposable

#Region " CONSTRUCTION "

    Public Const DefaultFileLogTraceListenerName As String = "FileLog"
    Public Const DefaultSourceName As String = "DefaultSource"
    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(My.MyLibrary.AssemblyProduct)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name)
        Me._resetKnownState()
    End Sub

    ''' <summary> Creates a new MyLog. </summary>
    ''' <param name="name"> The name. </param>
    ''' <returns> A MyLog. </returns>
    Public Shared Function Create(ByVal name As String) As MyLog
        Dim result As MyLog = Nothing
        Try
            result = New MyLog(name)
        Catch
            If result IsNot Nothing Then result.Dispose()
            result = Nothing
            Throw
        End Try
        Return result
    End Function

    ''' <summary> Gets or sets the trace source. </summary>
    ''' <value> The trace source. </value>
    Public Overloads Property TraceSource As MyTraceSource

    ''' <summary> Resets to known state. </summary>
    Private Sub _ResetKnownState()
        Me._ContentsQueue = New ConcurrentQueue(Of TraceMessage)
        ' instantiate cancellation token
        Me._CancellationTokenSource = New CancellationTokenSource
        Me._CancellationToken = Me.CancellationTokenSource.Token
        Dim name As String = MyBase.TraceSource.Name
        Me._TraceSource = New MyTraceSource(name)
        ' a private internal trace level is now used instead of the trace source to facilitate overridingthe trace source level.
        Me._TraceSource.ApplyTraceLevel(TraceEventType.Verbose)
        If Not Me.TraceSource.HasBeenConfigured Then
            MyBase.InitializeWithDefaultsSinceNoConfigExists()
        End If
        Me._applyTraceLevel(TraceEventType.Verbose)
    End Sub

#Region " Disposable Support "

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> Gets the is disposed. </summary>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <value> The is disposed. </value>
    Public ReadOnly Property IsDisposed As Boolean Implements ITraceMessageListener.IsDisposed

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Pith.MyLog and optionally releases the
    ''' managed resources.
    ''' </summary>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.CancellationTokenSource?.Cancel()
                If Me.CancellationTokenSource IsNot Nothing Then Me._CancellationTokenSource.Dispose() : Me._CancellationTokenSource = Nothing
                If Me.MyTask IsNot Nothing Then Me.MyTask.Dispose() : Me._MyTask = Nothing
                Me.TraceSource = Nothing
            End If
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region
#End Region

#Region " WRITE LOG ENTRY "

    ''' <summary> Writes an entry. </summary>
    ''' <remarks> Overloads the underlying method. </remarks>
    ''' <param name="message">  The message details. </param>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    Public Overloads Sub WriteEntry(ByVal message As String, ByVal severity As TraceEventType)
        Me.TraceSource.TraceEvent(severity, message)
    End Sub

    ''' <summary> Writes an entry. </summary>
    ''' <remarks> Overloads the underlying method. </remarks>
    ''' <param name="message">  The message details. </param>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    Public Overloads Sub WriteEntry(ByVal message As String, ByVal severity As TraceEventType, ByVal id As Integer)
        Me.TraceSource.TraceEvent(severity, id, message)
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="details">  The message details. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal details As String) As String
        If details IsNot Nothing Then
            Me.WriteEntry(details, severity)
            Return details
        End If
        Return String.Empty
    End Function

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    ''' <param name="details">  The message details. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal id As Integer, ByVal details As String) As String
        If details IsNot Nothing Then
            Me.WriteEntry(details, severity, id)
            Return details
        End If
        Return String.Empty
    End Function

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="format">   The message format. </param>
    ''' <param name="args">     Specified the message arguments. </param>
    ''' <returns> The message details or empty. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String
        If format IsNot Nothing Then
            Me.WriteLogEntry(severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End If
        Return String.Empty
    End Function

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    ''' <param name="format">   The message format. </param>
    ''' <param name="args">     Specified the message arguments. </param>
    ''' <returns> The message details or empty. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal id As Integer,
                                      ByVal format As String, ByVal ParamArray args() As Object) As String
        If format IsNot Nothing Then
            Me.WriteLogEntry(severity, id, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End If
        Return String.Empty
    End Function


    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="messages"> Message information to me. </param>
    ''' <returns> The message details or empty. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal messages As String()) As String
        If messages IsNot Nothing Then
            Return Me.WriteLogEntry(severity, String.Join(",", messages))
        End If
        Return String.Empty
    End Function

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    ''' <param name="messages"> Message information to me. </param>
    ''' <returns> The message details or empty. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal id As Integer, ByVal messages As String()) As String
        If messages IsNot Nothing Then
            Return Me.WriteLogEntry(severity, id, String.Join(",", messages))
        End If
        Return String.Empty
    End Function

#End Region

#Region " WRITE EXCEPTION DETAILS "

    ''' <summary> Writes an exception. </summary>
    ''' <param name="ex"> The exception. </param>
    Public Overloads Sub WriteException(ByVal ex As Exception)
        Me.WriteException(ex, TraceEventType.Error, "", 0)
    End Sub

    ''' <summary> Writes an exception. </summary>
    ''' <param name="ex">             The exception. </param>
    ''' <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
    ''' data. </param>
    ''' <param name="additionalInfo"> Additional information. </param>
    Public Overloads Sub WriteException(ByVal ex As Exception, ByVal severity As TraceEventType, ByVal additionalInfo As String)
        Me.WriteException(ex, severity, additionalInfo, 0)
    End Sub

    ''' <summary> Writes an exception. </summary>
    ''' <param name="ex">             The exception. </param>
    ''' <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
    ''' data. </param>
    ''' <param name="additionalInfo"> Additional information. </param>
    ''' <param name="id">             The identifier. </param>
    Public Overloads Sub WriteException(ByVal ex As Exception, ByVal severity As TraceEventType, ByVal additionalInfo As String, ByVal id As Integer)
        If ex IsNot Nothing Then
            Dim builder As New System.Text.StringBuilder
            builder.Append(ex.ToFullBlownString)
            If Not String.IsNullOrWhiteSpace(additionalInfo) Then
                builder.Append("; ")
                builder.Append(additionalInfo)
            End If
            Me.WriteEntry(builder.ToString, severity, id)
        End If
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="ex">             The exception. </param>
    ''' <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
    ''' data. </param>
    ''' <param name="additionalInfo"> Additional information. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType, ByVal additionalInfo As String)
        If ex IsNot Nothing Then Me.WriteException(ex, severity, additionalInfo)
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="ex">             The exception. </param>
    ''' <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
    ''' data. </param>
    ''' <param name="id">             The identifier. </param>
    ''' <param name="additionalInfo"> Additional information. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType, ByVal id As Integer, ByVal additionalInfo As String)
        If ex IsNot Nothing Then Me.WriteException(ex, severity, additionalInfo, id)
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="ex">       The exception. </param>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="format">   The additional information format. </param>
    ''' <param name="args">     The additional information arguments. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType,
                                     ByVal format As String, ByVal ParamArray args() As Object)
        Me.WriteExceptionDetails(ex, severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="ex">       The exception. </param>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="format">   The additional information format. </param>
    ''' <param name="args">     The additional information arguments. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType,
                                         ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        Me.WriteExceptionDetails(ex, severity, id, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="ex">  The exception. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception)
        Me.WriteExceptionDetails(ex, TraceEventType.Error, "")
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="ex">  The exception. </param>
    ''' <param name="id">  The identifier. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception, ByVal id As Integer)
        Me.WriteExceptionDetails(ex, TraceEventType.Error, id, "")
    End Sub

#End Region

#Region " WRITE LOG ENTRY -- OVERRIDE TRACE LEVEL "

    ''' <summary> Writes a message to the application log listeners. Overrides the current log level. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="details">  The message details. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntryOverride(ByVal severity As TraceEventType, ByVal details As String) As String
        If details IsNot Nothing Then
            ' save the current trace level.
            Dim lastSourceLevel As Diagnostics.SourceLevels = Me.TraceSource.Switch.Level
            ' set the requested level.
            Me.TraceSource.Switch.Level = severity.ToSourceLevel
            ' write the entry.
            Me.WriteEntry(details, severity)
            ' restore the level.
            Me.TraceSource.Switch.Level = lastSourceLevel
            Return details
        End If
        Return String.Empty
    End Function

    ''' <summary> Writes a message to the application log listeners. Overrides the current log level. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="details">  The message details. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntryOverride(ByVal severity As TraceEventType,
                                              ByVal id As Integer, ByVal details As String) As String
        If details IsNot Nothing Then
            ' save the current trace level.
            Dim lastSourceLevel As Diagnostics.SourceLevels = Me.TraceSource.Switch.Level
            ' set the requested level.
            Me.TraceSource.Switch.Level = severity.ToSourceLevel
            ' write the entry.
            Me.WriteEntry(details, severity, id)
            ' restore the level.
            Me.TraceSource.Switch.Level = lastSourceLevel
            Return details
        End If
        Return String.Empty
    End Function

    ''' <summary> Writes a message to the application log listeners. Overrides the current log level. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="format">   The message details. </param>
    ''' <param name="args">     The arguments. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntryOverride(ByVal severity As TraceEventType,
                                          ByVal format As String, ByVal ParamArray args() As Object) As String
        If format IsNot Nothing Then
            Return Me.WriteLogEntryOverride(severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End If
        Return String.Empty
    End Function

    ''' <summary> Writes a message to the application log listeners. Overrides the current log level. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    ''' <param name="format">   The message details. </param>
    ''' <param name="args">     The arguments. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntryOverride(ByVal severity As TraceEventType,
                                              ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object) As String
        If format IsNot Nothing Then
            Return Me.WriteLogEntryOverride(severity, id, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End If
        Return String.Empty
    End Function

#End Region

#Region " LOG TRACE MESSAGES "

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="message"> The message. </param>
    Public Sub WriteLogEntry(ByVal message As TraceMessage)
        If message IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(message.Details) Then
            Me.WriteLogEntry(message.EventType, message.Id, message.Details)
        End If
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="message">        The message. </param>
    ''' <param name="additionalInfo"> Information describing the additional. </param>
    Public Sub WriteLogEntry(ByVal message As TraceMessage, ByVal additionalInfo As String)
        If message IsNot Nothing Then
            If String.IsNullOrWhiteSpace(additionalInfo) Then
                Me.WriteLogEntry(message)
            ElseIf Not String.IsNullOrWhiteSpace(message.Details) Then
                Me.WriteLogEntry(message.EventType, message.Id, "{0},{1}", message.Details, additionalInfo)
            End If
        End If
    End Sub

#End Region

#Region " FILE  "

    ''' <summary> Gets the file log trace listener. </summary>
    ''' <value> The file log trace listener. </value>
    Public ReadOnly Property FileLogTraceListener As Logging.FileLogTraceListener
        Get
            Return Me.TraceSource.DefaultFileLogWriter
        End Get
    End Property

    ''' <summary> Gets the filename of the full log file. </summary>
    ''' <value> The filename of the full log file. </value>
    Public ReadOnly Property FullLogFileName As String
        Get
            Return Me.FileLogTraceListener.FullLogFileName
        End Get
    End Property

    ''' <summary> Gets the size of the default file log writer file. </summary>
    ''' <value> The size of the file. </value>
    Public ReadOnly Property FileSize As Long
        Get
            Return DefaultFileLogTraceListener.FileSize(Me.FullLogFileName)
        End Get
    End Property

    ''' <summary> Checks if the default file log writer file exists. </summary>
    ''' <returns> <c>True</c> if the log file exists. </returns>
    Public ReadOnly Property LogFileExists() As Boolean
        Get
            Return DefaultFileLogTraceListener.FileSize(Me.FullLogFileName) > 2
        End Get
    End Property

    ''' <summary> Opens log file. </summary>
    ''' <returns> The Process. </returns>
    Public Function OpenLogFile() As Process
        Dim proc As String = "explorer.exe"
        Dim args As String = $"{ControlChars.Quote}{Me.FullLogFileName}{ControlChars.Quote}"
        Return Process.Start(proc, args)
    End Function

    ''' <summary> Opens folder location. </summary>
    ''' <returns> The Process. </returns>
    Public Function OpenFolderLocation() As Process
        Dim proc As String = "explorer.exe"
        Dim fi As New System.IO.FileInfo(Me.FullLogFileName)
        Dim args As String = $"{ControlChars.Quote}{fi.DirectoryName}{ControlChars.Quote}"
        Return Process.Start(proc, args)
    End Function

#End Region

#Region " REPLACE TRACE LISTENER "

    ''' <summary> Replaces the default file log trace listener with a new one. </summary>
    ''' <param name="logWriter"> The
    ''' <see cref="Logging.FileLogTraceListener">log writer</see>. </param>
    ''' <returns>The <see cref="Logging.FileLogTraceListener">file log trace listener. </see></returns>
    Public Function ReplaceDefaultTraceListener(ByVal logWriter As Logging.FileLogTraceListener) As Logging.FileLogTraceListener
        If Me.TraceSource IsNot Nothing Then
            Me.TraceSource.ReplaceDefaultTraceListener(logWriter)
            MyBase.TraceSource.ReplaceDefaultTraceListener(logWriter)
        End If
        Return logWriter
    End Function

    ''' <summary> Replaces the default file log trace listener with a new one for the current user. </summary>
    ''' <remarks> The current user application data folder is used. </remarks>
    ''' <returns> The <see cref="Logging.FileLogTraceListener">file log trace listener. </see> </returns>
    Public Function ReplaceDefaultTraceListener() As Logging.FileLogTraceListener
        Return Me.ReplaceDefaultTraceListener(UserLevel.CurrentUser)
    End Function

    ''' <summary> Replaces the default file log trace listener with a new one. </summary>
    ''' <param name="userLevel"> if set to <c>True</c> uses the all users application data folder;
    ''' otherwise the current user application data folder is used. </param>
    ''' <returns> The <see cref="Logging.FileLogTraceListener">file log trace listener. </see> </returns>
    Public Function ReplaceDefaultTraceListener(ByVal userLevel As UserLevel) As Logging.FileLogTraceListener
        Dim tempListener As Logging.FileLogTraceListener = Nothing
        Dim listener As Logging.FileLogTraceListener = Nothing
        Try
            tempListener = New DefaultFileLogTraceListener(userLevel)
            listener = tempListener
            Me.ReplaceDefaultTraceListener(listener)
        Finally
            If tempListener IsNot Nothing Then tempListener.Dispose()
        End Try
        Return listener
    End Function

#End Region

#Region " ASYNC QUEUED MESSAGES "

    ''' <summary> The cancellation token source. </summary>
    Private ReadOnly Property CancellationTokenSource As CancellationTokenSource

    ''' <summary> The cancellation token. </summary>
    Private ReadOnly Property CancellationToken As CancellationToken

    Private Function IsCancellationRequested() As Boolean
        Return Me.CancellationToken.IsCancellationRequested
    End Function

    Protected ReadOnly Property ContentsQueue As ConcurrentQueue(Of TraceMessage)

    ''' <summary> Gets contents queue. </summary>
    ''' <returns> The contents queue. </returns>
    Private Function GetContentsQueue() As ConcurrentQueue(Of TraceMessage)
        Return Me._ContentsQueue
    End Function

    ''' <summary> Gets the content. </summary>
    ''' <returns> The content. </returns>
    Private Function GetContent() As TraceMessage
        Dim result As TraceMessage = Nothing
        Me.ContentsQueue.TryDequeue(result)
        Return result
    End Function

    ''' <summary> Uses the message worker to flush any queued messages. </summary>
    ''' <remarks> If the calling thread is different from the thread that created the TextBox control,
    ''' this method creates a SetTextCallback and calls itself asynchronously using the Invoke
    ''' method. </remarks>
    Public Sub FlushMessages()
        ' Me.FlushMessagesWorker()
        Me.FlushMessagesTask()
    End Sub

#End Region

#Region " ASYNC QUEUED MESSAGES: TASK "

    ''' <summary> Gets the task. </summary>
    ''' <value> The task. </value>
    Protected ReadOnly Property MyTask As Task

    ''' <summary> Query if this object is busy. </summary>
    ''' <returns> <c>true</c> if busy; otherwise <c>false</c> </returns>
    Private Function IsBusy() As Boolean
        Return Not Me.IsDisposed AndAlso Me.MyTask IsNot Nothing AndAlso Me.MyTask.Status = TaskStatus.Running
    End Function

    ''' <summary> Uses the message worker to flush any queued messages. </summary>
    ''' <remarks> If the calling thread is different from the thread that created the TextBox control,
    ''' this method creates a SetTextCallback and calls itself asynchronously using the Invoke
    ''' method. </remarks>
    Private Sub FlushMessagesTask()
        If Not Me.IsBusy Then
            'Me._task = New Task(AddressOf Me.UpdateDisplay)
            'Me.Task.Start()
            Me._MyTask = Me.StartAsyncTask()
        End If
    End Sub

    ''' <summary> Start Asynchronous task. </summary>
    ''' <returns> A Task. </returns>
    Private Async Function StartAsyncTask() As Task
        Await Task.Run(AddressOf Me.TraceQueue)
    End Function

    ''' <summary>
    ''' This event handler sets the Text property of the TextBox control. It is called on the thread
    ''' that created the TextBox control, so the call is thread-safe.
    ''' </summary>
    Private Sub TraceQueue()
        Do While Me.GetContentsQueue()?.Any AndAlso Not Me.IsCancellationRequested
            ' the check for tracing is now down when adding the event to the queue.
            ' Dim value As TraceMessage = Me.GetContent: If value IsNot Nothing AndAlso Me.ShouldTrace(value.EventType) Then Me.TraceSource.TraceEvent(value)
            Me.TraceSource.TraceEvent(Me.GetContent)
        Loop
    End Sub

#End Region

End Class


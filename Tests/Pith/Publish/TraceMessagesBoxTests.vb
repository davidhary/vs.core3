﻿Namespace Pith.Tests
    '''<summary>
    '''This is a test class for TraceMessagesBoxTest and is intended
    '''to contain all TraceMessagesBoxTest Unit Tests
    '''</summary>
    <TestClass()>
    Public Class TraceMessagesBoxTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " TRACE MESSAGES BOX TESTS "

        '''<summary>
        '''A test for AddMessage
        '''</summary>
        <TestMethod()>
        Public Sub AddMessageTest()
            Using target As isr.Core.Pith.TraceMessagesBox = New isr.Core.Pith.TraceMessagesBox()
                Dim value As isr.Core.Pith.TraceMessage = New isr.Core.Pith.TraceMessage(TraceEventType.Information, 1, "Message {0}", DateTime.Now)
                target.AddMessage(value)
                Do Until target.MyTask.GetAwaiter().IsCompleted
                    Windows.Forms.Application.DoEvents()
                Loop
                Assert.IsTrue(target.NewMessagesAdded, $"new messages added")
            End Using
        End Sub
#End Region

    End Class
End Namespace
﻿Namespace Pith.Tests
    '''<summary>
    '''This is a test class for unpublished messages
    '''</summary>
    <TestClass()>
    Public Class UnpublishedLogTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " UNPUBLISHED LOG TESTS  "

        ''' <summary> Tests logging unpublished messages. </summary>
        ''' <param name="expected"> The expected. </param>
        Public Shared Sub TestLoggingUnpublishedMessages(ByVal expected As TraceEventType)

            Dim payload As String = "Message 1"
            Dim expectedMessage As New isr.Core.Pith.TraceMessage(expected, isr.Core.Pith.My.MyLibrary.TraceEventId, payload)

            Dim initialLogFileSize As Long = isr.Core.Pith.My.MyLibrary.MyLog.FileSize

            Dim actualLogFileName As String = isr.Core.Pith.My.MyLibrary.MyLog.FullLogFileName
            Assert.IsFalse(String.IsNullOrWhiteSpace(actualLogFileName), $"Tests if log file name '{actualLogFileName}' is empty")

            ' clear trace message log
            isr.Core.Pith.My.MyLibrary.UnpublishedTraceMessages.FetchContent()

            ' check content
            Dim actualContents As String = isr.Core.Pith.My.MyLibrary.UnpublishedTraceMessages.FetchContent()
            Assert.IsTrue(String.IsNullOrWhiteSpace(actualContents), "Unpublished messages should clear")

            ' log an unpublished trace message to the library.
            isr.Core.Pith.My.MyLibrary.LogUnpublishedMessage(expectedMessage)

            Dim actualMessage As isr.Core.Pith.TraceMessage = isr.Core.Pith.My.MyLibrary.UnpublishedTraceMessages.TryDequeue
            Assert.AreEqual(expectedMessage.Id, actualMessage.Id, " Message trace events identities match")

            isr.Core.Pith.My.MyLibrary.MyLog.FlushMessages()
            Windows.Forms.Application.DoEvents()
            Dim sw As Stopwatch = Stopwatch.StartNew()
            isr.Core.Pith.StopwatchExtensions.Wait(sw, TimeSpan.FromMilliseconds(100))
            Dim newLogFileSize As Long = isr.Core.Pith.My.MyLibrary.MyLog.FileSize
            Assert.IsTrue(newLogFileSize > initialLogFileSize, $"New file Size {newLogFileSize} > initial file size {initialLogFileSize}")


        End Sub

        ''' <summary> (Unit Test Method) tests log unpublished messages. </summary>
        <TestMethod()>
        Public Sub LogUnpublishedMessagesTest()
            TestLoggingUnpublishedMessages(TraceEventType.Warning)
        End Sub
#End Region

    End Class
End Namespace
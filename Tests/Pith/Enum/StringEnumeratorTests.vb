﻿Imports isr.Core.Pith
Namespace Pith.Tests
    '''<summary>
    '''This is a test class for StringEnumeratorTest and is intended
    '''to contain all StringEnumeratorTest Unit Tests
    '''</summary>
    <TestClass()>
    Public Class StringEnumeratorTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
            ' needs to add delay here to prevent an application domain exception.
            isr.Core.Pith.StopwatchExtensions.Wait(Stopwatch.StartNew, TimeSpan.FromMilliseconds(200))
            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " STRING ENUMERATOR TESTS "

        '''<summary>
        '''A test for TryParse
        '''</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")>
        Public Sub TryParseTestHelper(Of T As Structure)(ByVal value As String, ByVal enumValueExpected As T)
            Dim target As StringEnumerator(Of T) = New StringEnumerator(Of T)()
            Dim expected As Boolean = True
            Dim actual As Boolean
            Dim enumValue As T = Nothing
            actual = target.TryParse(value, enumValue)
            Assert.AreEqual(enumValueExpected, CType(enumValue, T))
            Assert.AreEqual(expected, actual)
        End Sub

        <TestMethod()>
        Public Sub TryParseTestValueTest()
            TryParseTestHelper(Of TestValue)("2", TestValue.Error)
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="expected")>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="colorValues")>
        <TestMethod()>
        Public Sub TryParseColorTest()
            Dim colorStrings() As String = {"0", "2", "8", "blue", "Blue", "Yellow", "Red, Green"}
            Dim colorValues() As Colors = {Colors.None, Colors.Green, Colors.Red Or Colors.Green Or Colors.Blue,
                                           Colors.Blue, Colors.Blue, Colors.None, Colors.Red Or Colors.Green}
            For Each colorString As String In colorStrings
                Dim colorValue As Colors
                Dim expected As Boolean = True
                Dim actual As Boolean = [Enum].TryParse(colorString, colorValue)
                If actual Then
                    If [Enum].IsDefined(GetType(Colors), colorValue) Or colorValue.ToString().Contains(",") Then
                        Trace.Write(String.Format("Converted '{0}' to {1}.", colorString, colorValue.ToString()))
                    Else
                        Trace.Write(String.Format("{0} is not an underlying value of the Colors enumeration.", colorString))
                    End If
                Else
                    Trace.Write(String.Format("{0} is not a member of the Colors enumeration.", colorString))
                End If
            Next
        End Sub

        Private Enum TestValue
            None
            Info
            [Error]
        End Enum

        <Flags()> Enum Colors As Integer
            None = 0
            Red = 1
            Green = 2
            Blue = 4
        End Enum
#End Region

    End Class
End Namespace
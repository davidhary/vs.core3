﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.Pith
Namespace Pith.Tests
    '''<summary>
    '''This is a test class for EfficientEnumTest and is intended
    '''to contain all EfficientEnumTest Unit Tests
    '''</summary>
    <TestClass()>
    Public Class EnumExtenderTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            EnumExtenderTests.ColorEnum = Nothing
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " ENUM EXTENDER TESTS "

        Private Shared ColorEnum As EnumExtender(Of Color) = New EnumExtender(Of Color)()

        Private Enum Color
            <ComponentModel.Description("While Color")> White
            <ComponentModel.Description("Black Color")> Black
            <ComponentModel.Description("Red Color")> Red
            <ComponentModel.Description("Yellow Color")> Yellow
            <ComponentModel.Description("Blue Color")> Blue
            <ComponentModel.Description("Green Color")> Green
            <ComponentModel.Description("Cyan Color")> Cyan
            <ComponentModel.Description("Magenta Color")> Magenta
            <ComponentModel.Description("Pink Color")> Pink
            <ComponentModel.Description("Purple Color")> Purple
            <ComponentModel.Description("Orange Color")> Orange
            <ComponentModel.Description("Brown Color")> Brown
        End Enum

        Private Shared _enumStrings As String() = New String() {"White", "Black", "Red", "Yellow", "Blue", "Green", "Cyan", "Magenta", "Pink", "Purple", "Orange", "Brown"}

        Private Const _iterations As Integer = 100000

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="c1")>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="c2")>
        Shared Sub Main()
            Dim randomNumber As Random = New Random()
            Using TempPerformanceMonitor As PerformanceMonitor = New PerformanceMonitor("{Built-in Enum class}")
                For i As Integer = 0 To _iterations - 1
                    Dim index As Integer = randomNumber.Next(0, 11)
                    Dim c1 As Color = CType(System.Enum.ToObject(GetType(Color), index), Color)
                    Dim c2 As Color = CType(System.Enum.Parse(GetType(Color), _enumStrings(index)), Color)
                Next i
            End Using

            ' Verify initialization of the data out of the comparative measurement.
            ' As you can see, this initialization is the gain for the later efficiency.
            Dim colorEnum As New EnumExtender(Of Color)
            Using TempPerformanceMonitor As PerformanceMonitor = New PerformanceMonitor("{StrongQuickEnum<Color> class}")
                For i As Integer = 0 To _iterations - 1
                    Dim index As Integer = randomNumber.Next(0, 11)
                    Dim c1 As Color = colorEnum.ToObject(index)
                    Dim c2 As Color = colorEnum.Parse(EnumExtenderTests._enumStrings(index))
                Next i
            End Using
            Console.ReadLine()
        End Sub

        Friend Class PerformanceMonitor
            Implements IDisposable
            Private _Timestarted As Long
            Private _Name As String

            Friend Sub New(ByVal name As String)
                Me._Name = name
                Me._Timestarted = DateTime.Now.Ticks
            End Sub

            Public Sub Dispose() Implements IDisposable.Dispose
                Console.WriteLine("Operation " & Me._Name & ":" & Constants.vbTab + Constants.vbTab + (DateTime.Now.Ticks - Me._Timestarted).ToString())
            End Sub
        End Class

        <TestMethod()>
        Public Sub ToObjectTest1()
            Dim value As Integer = CInt(Color.Cyan)
            Dim expected As Color = Color.Cyan
            Dim actual As Color = ColorEnum.ToObject(value)
            Assert.AreEqual(expected, actual)
        End Sub

        <TestMethod()>
        Public Sub ToObjectTest2()
            Dim value As Integer = CInt(Color.Cyan)
            Dim expected As Color = Color.Cyan
            Dim actual As Color = ColorEnum.ToObject(value)
            Assert.AreEqual(expected, actual)
        End Sub

        <TestMethod()>
        Public Sub ParseTest1()
            Dim value As String = NameOf(Color.White)
            Dim ignoreCase As Boolean = False
            Dim expected As Color = Color.White
            Dim actual As Color = ColorEnum.Parse(value, ignoreCase)
            Assert.AreEqual(expected, actual)
        End Sub

        '''<summary>
        '''A test for Parse
        '''</summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")>
        Public Shared Sub ParseTestHelper(Of T)()
            Dim value As String = NameOf(Color.Cyan)
            Dim expected As Color = Color.Cyan
            Dim actual As Color = ColorEnum.Parse(value)
            Assert.AreEqual(expected, actual)
        End Sub

        <TestMethod()>
        Public Sub ParseTest()
            EnumExtenderTests.ParseTestHelper(Of Color)()
        End Sub

        '''<summary>
        '''A test for IsDefined
        '''</summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")>
        Public Shared Sub IsDefinedTestHelper(Of T)()
            Dim value As Object = Color.Cyan
            Dim expected As Boolean = True
            Dim actual As Boolean = ColorEnum.IsDefined(value)
            Assert.AreEqual(expected, actual)
        End Sub

        <TestMethod()>
        Public Sub IsDefinedTest()
            EnumExtenderTests.IsDefinedTestHelper(Of Color)()
        End Sub

        '''<summary>
        '''A test for GetValues
        '''</summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")>
        Public Shared Sub GetValuesTestHelper(Of T)()
            Dim expected As Array = [Enum].GetValues(GetType(Color))
            Dim actual As Array = ColorEnum.Values
            Dim a As Integer() = New Integer(expected.Length - 1) {}
            actual.CopyTo(a, 0)
            Dim b As Integer() = New Integer(actual.Length - 1) {}
            expected.CopyTo(b, 0)
            Assert.IsTrue((a.Length = b.Length AndAlso a.Intersect(b).Count() = a.Length), "Values are equal")
        End Sub

        <TestMethod()>
        Public Sub GetValuesTest()
            EnumExtenderTests.GetValuesTestHelper(Of Color)()
        End Sub

        '''<summary>
        '''A test for GetUnderlyingType
        '''</summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")>
        Public Shared Sub GetUnderlyingTypeTestHelper(Of T)()
            Dim expected As Type = GetType(Integer)
            Dim actual As Type = ColorEnum.UnderlyingType
            Assert.AreEqual(expected, actual)
        End Sub

        <TestMethod()>
        Public Sub GetUnderlyingTypeTest()
            EnumExtenderTests.GetUnderlyingTypeTestHelper(Of Color)()
        End Sub

        '''<summary>
        '''A test for GetNames
        '''</summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")>
        Public Shared Sub GetNamesTestHelper(Of T)()
            Dim expected() As String = _enumStrings
            Dim actual() As String = ColorEnum.Names.ToArray
            Assert.IsTrue((expected.Length = actual.Length AndAlso expected.Intersect(actual).Count() = expected.Length), "Names are equal")
        End Sub

        <TestMethod()>
        Public Sub GetNamesTest()
            EnumExtenderTests.GetNamesTestHelper(Of Color)()
        End Sub

        '''<summary>
        '''A test for GetName
        '''</summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")>
        Public Shared Sub GetNameTestHelper(Of T)()
            Dim value As Color = Color.Black
            Dim expected As String = NameOf(Color.Black)
            Dim actual As String = ColorEnum.Name(value)
            Assert.AreEqual(expected, actual, $"Name of {value}")
        End Sub

        <TestMethod()>
        Public Sub GetNameTest()
            EnumExtenderTests.GetNameTestHelper(Of Color)()
        End Sub

        '''<summary>
        '''A test for EfficientEnum`1 Constructor
        '''</summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")>
        Public Shared Sub EfficientEnumConstructorTestHelper(Of T)()
            Dim target As EnumExtender(Of T) = New EnumExtender(Of T)
            Dim expectedDesction As String = "Black Color"
            Dim actualDescription As String = target.Description(Color.Black)
            Assert.AreEqual(expectedDesction, actualDescription, $"Description of {Color.Black}")
        End Sub

        <TestMethod()>
        Public Sub EfficientEnumConstructorTest()
            EnumExtenderTests.EfficientEnumConstructorTestHelper(Of Color)()
        End Sub
#End Region

    End Class
End Namespace
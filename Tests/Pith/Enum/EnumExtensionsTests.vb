﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.Pith.EnumExtensions
Namespace Pith.Tests
    '''<summary>
    '''This is a test class for EnumExtensionsTest and is intended
    '''to contain all EnumExtensionsTest Unit Tests
    '''</summary>
    <TestClass()>
    Public Class EnumExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " Enum extension tests "

        '''<summary>
        '''A test for GetDescription
        '''</summary>
        <TestMethod()>
        Public Sub GetDescriptionTest()
            Dim value As Windows.Forms.MessageBoxButtons = Windows.Forms.MessageBoxButtons.AbortRetryIgnore
            Dim expected As String = Windows.Forms.MessageBoxButtons.AbortRetryIgnore.ToString
            Dim actual As String
            actual = value.Description
            Assert.AreEqual(expected, actual)
            Assert.AreEqual(expected, value.Description())
        End Sub

#End Region

    End Class
End Namespace
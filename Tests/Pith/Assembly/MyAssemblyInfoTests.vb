﻿Imports isr.Core.Pith
Namespace Pith.Tests
    '''<summary>
    '''This is a test class for MyAssemblyInfoTest and is intended
    '''to contain all MyAssemblyInfoTest Unit Tests
    '''</summary>
    <TestClass()>
    Public Class MyAssemblyInfoTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " MY ASSEMBLY INFO TESTS "

        '''<summary> A test for Public Key Token. </summary>
        <TestMethod()>
        Public Sub PublicKeyTokenTest()
            Dim target As MyAssemblyInfo = New MyAssemblyInfo(My.Application.Info)
            Dim expected As String = target.Assembly.GetName.ToString().Split(","c)(3)
            expected = expected.Substring(expected.IndexOf("=", StringComparison.OrdinalIgnoreCase) + 1)
            Dim actual As String = target.PublicKeyToken
            Assert.AreEqual(expected, actual, True, Globalization.CultureInfo.CurrentCulture)
        End Sub

        ''' <summary> The build number of today. </summary>
        ''' <returns> The build number for today. </returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")>
        Public Function BuildNumber() As Integer
            Dim target As MyAssemblyInfo = New MyAssemblyInfo(My.Application.Info)
            Return CInt(Math.Floor(target.FileInfo.LastWriteTime.Subtract(Date.Parse("1/1/2000")).TotalDays))
        End Function

        '''<summary> A test for FileVersionInfo. </summary>
        <TestMethod()>
        Public Sub FileVersionInfoTest()
            Dim target As MyAssemblyInfo = New MyAssemblyInfo(My.Application.Info)
            Dim expected As Integer = BuildNumber()
            Dim actual As Integer = target.FileVersionInfo.FileBuildPart
            Assert.AreEqual(expected, actual, "Expected build number")
        End Sub

        '''<summary> A test for ProductVersion. </summary>
        <TestMethod()>
        Public Sub ProductVersionTest()
            Dim target As MyAssemblyInfo = New MyAssemblyInfo(My.Application.Info)
            Dim expected As String = BuildNumber.ToString
            Dim actual As String = target.ProductVersion("").Split("."c)(2)
            Assert.AreEqual(expected, actual, "Product version")
        End Sub

#End Region

    End Class
End Namespace
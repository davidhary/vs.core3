﻿Imports isr.Core.Pith.SplitExtensions
Imports isr.Core.Pith.EscapeSequencesExtensions
Imports isr.Core.Pith.CompactExtensions
Namespace Pith.Tests
    ''' <summary> String extensions tests. </summary>
    ''' <license>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="3/14/2018" by="David" revision=""> Created. </history>
    <TestClass()>
    Public Class StringExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub


        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " SPLIT EXTENSION TESTS "

        ''' <summary> (Unit Test Method) tests base 64 unique identifier. </summary>
        <TestMethod()>
        Public Sub SplitWordsTest()
            Dim input As String = "CamelCase"
            Dim expected As String = "Camel Case"
            Dim actual As String = input.SplitWords
            Assert.AreEqual(expected, actual, $"Split {input}")
            input = "CamelCASE"
            expected = "Camel CASE"
            actual = input.SplitWords
            Assert.AreEqual(expected, actual, $"Split {input}")
            input = "CamelCase123"
            expected = "Camel Case123"
            actual = input.SplitWords
            Assert.AreEqual(expected, actual, $"Split {input}")
            input = "CamelIPv4"
            expected = "Camel IPv4"
            actual = input.SplitWords
            Assert.AreEqual(expected, actual, $"Split {input}")
        End Sub

        ''' <summary> (Unit Test Method) tests base 64 unique identifier. </summary>
        <TestMethod()>
        Public Sub SplitTitleCaseTest()
            Dim input As String = "CamelCase"
            Dim expected As String = "Camel Case"
            Dim actual As String = input.SplitTitleCase
            Assert.AreEqual(expected, actual, $"Split {input}")
            input = "CamelCase123"
            expected = "Camel Case 123"
            actual = input.SplitTitleCase
            Assert.AreEqual(expected, actual, $"Split {input}")
            input = "LocalIPV4"
            expected = "Local IPV 4"
            actual = input.SplitTitleCase
            Assert.AreEqual(expected, actual, $"Split {input}")
            input = "LocalREMOTE"
            expected = "Local REMOTE"
            actual = input.SplitTitleCase
            Assert.AreEqual(expected, actual, $"Split {input}")
            input = "LocalIPv4"
            expected = "Local I Pv 4"
            actual = input.SplitTitleCase
            Assert.AreEqual(expected, actual, $"Split {input}")
        End Sub

#End Region

#Region " COMPACT TESTS "

        ''' <summary> (Unit Test Method) tests compact string. </summary>
        <TestMethod>
        Public Sub CompactStringTest()
            Dim candidate As String = "Compact this string"
            Dim textBox As New System.Windows.Forms.TextBox With {.Width = 100, .Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 10), .AutoSize = False}
            Dim compactedValue As String = String.Empty
            Dim expcetedValue As String = String.Empty

            compactedValue = candidate.Compact(textBox, System.Windows.Forms.TextFormatFlags.EndEllipsis)
            expcetedValue = "Compact thi..."
            Assert.AreEqual(expcetedValue, compactedValue, $"Failed compacting in a text box with {System.Windows.Forms.TextFormatFlags.EndEllipsis}")

            compactedValue = candidate.Compact(textBox, System.Windows.Forms.TextFormatFlags.PathEllipsis)
            expcetedValue = "pact t...his str"
            Assert.AreEqual(expcetedValue, compactedValue, $"Failed compacting in a text box with {System.Windows.Forms.TextFormatFlags.PathEllipsis}")

            compactedValue = candidate.Compact(textBox, System.Windows.Forms.TextFormatFlags.WordEllipsis)
            expcetedValue = "...ct this string"
            Assert.AreEqual(expcetedValue, compactedValue, $"Failed compacting in a text box with {System.Windows.Forms.TextFormatFlags.WordEllipsis}")

            Dim label As New System.Windows.Forms.ToolStripStatusLabel With {.Width = 100, .Font = New Drawing.Font(Drawing.FontFamily.GenericSansSerif, 10), .AutoSize = False}
            compactedValue = candidate.Compact(label, System.Windows.Forms.TextFormatFlags.PathEllipsis)
            expcetedValue = "pact t...his str"
            Assert.AreEqual(expcetedValue, compactedValue, $"Failed compacting in a tool strip label with {System.Windows.Forms.TextFormatFlags.PathEllipsis}")
        End Sub

#End Region

#Region " ESCAPE SEQUENCE TESTS "

        ''' <summary> (Unit Test Method) tests escape sequence. </summary>
        <TestMethod>
        Public Sub EscapeSequenceTest()
            Dim IdentityQueryCommand As String = "*IDN?"
            Dim escapedValue As String = $"{IdentityQueryCommand}{Core.Pith.EscapeSequencesExtensions.NewLineEscape}"
            Dim expcetedValue As String = $"{IdentityQueryCommand}{Core.Pith.EscapeSequencesExtensions.NewLineChar}"
            Dim actualValue As String = escapedValue.Replace(Core.Pith.EscapeSequencesExtensions.NewLineEscape, ChrW(Core.Pith.EscapeSequencesExtensions.NewLineValue))
            Assert.AreEqual(expcetedValue, actualValue, $"Failed replacing escape value in {escapedValue}")
            actualValue = escapedValue.ReplaceCommonEscapeSequences()
            Assert.AreEqual(expcetedValue, actualValue, $"Extension failed replacing escape value in {escapedValue}")
            actualValue = actualValue.InsertCommonEscapeSequences()
            Assert.AreEqual(escapedValue, actualValue, $"Extension failed inserting escape value")
        End Sub

#End Region

    End Class
End Namespace
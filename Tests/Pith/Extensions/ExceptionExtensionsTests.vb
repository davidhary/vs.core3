﻿Imports isr.Core.Pith
Namespace Pith.Tests
    ''' <summary> An exception extensions tests. </summary>
    ''' <license>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="1/26/2018" by="David"> > Created. </history>
    <TestClass()>
    Public Class ExceptionExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub


        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " TO FULL BLOWN STRING "

        ''' <summary> Throw argument exception. </summary>
        ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        '''                                      illegal values. </exception>
        ''' <param name="version"> The version. </param>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="version")>
        Private Shared Sub ThrowArgumentException(ByVal version As Version)
            Throw New ArgumentException("Forced argument exception", NameOf(version))
        End Sub

        ''' <summary> (Unit Test Method) tests full blown exception. </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="result")>
        <TestMethod()>
        Public Sub FullBlownExceptionTest()
            Dim version As Version = New Version(0, 0, 0, 0)
            Dim expectedKey As String = "0-Name"
            Dim expectedValue As String = NameOf(version)
            Try
                ExceptionExtensionsTests.ThrowArgumentException(version)
            Catch ex As ArgumentException
                Dim result As String = ExceptionExtensions.ToFullBlownString(ex)
                ' this checks the exception data.
                Assert.IsTrue(ex.Data.Contains(expectedKey), $"Data contains key {expectedKey}")
                Assert.AreEqual(expectedValue, CStr(ex.Data.Item(expectedKey)), $"Data value of {expectedKey}")
                ' this checks the stack trace
                Dim fullText As String = ExceptionExtensions.ToFullBlownString(ex)
                Dim searchFor As String = NameOf(ExceptionExtensionsTests.FullBlownExceptionTest)
                Assert.IsTrue(fullText.Contains(searchFor), $"{fullText} contains {searchFor}")
            Catch ex As Exception
                Assert.Fail("Expected exception not caught")
            End Try
        End Sub


#End Region

    End Class
End Namespace
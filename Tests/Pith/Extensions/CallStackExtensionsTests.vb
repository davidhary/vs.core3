﻿Imports isr.Core.Pith.StackTraceExtensions
Namespace Pith.Tests
    '''<summary>
    '''This is a test class for ExtensionsTest and is intended
    '''to contain all ExtensionsTest Unit Tests
    '''</summary>
    <TestClass()>
    Public Class CallStackExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " CALL STACK EXTENSION TESTS "

        '''<summary>
        '''A test for ParseCallStack for user.
        '''</summary>
        <TestMethod()>
        Public Sub ParseUserCallStackFrameTest()
            Dim trace As StackTrace = New StackTrace(True)
            Dim frame As StackFrame = New StackFrame(True)
            Dim callStackType As CallStackType = CallStackType.UserCallStack
            Dim expected As String = "Tests.ParseUserCallStackFrameTest() in "
            Dim actual As String = trace.ParseCallStack(frame, callStackType).Trim
            Assert.IsTrue(actual.Contains(expected), $"Actual trace 
{actual}
contains 
{expected}")
        End Sub

        '''<summary>
        '''A test for ParseCallStack for user.
        '''</summary>
        <TestMethod()>
        Public Sub ParseFullCallStackFrameTest()
            Dim trace As StackTrace = New StackTrace(True)
            Dim frame As StackFrame = New StackFrame(True)
            Dim callStackType As CallStackType = CallStackType.FullCallStack
            Dim expected As String = ""
            Dim actual As String
            actual = trace.ParseCallStack(frame, callStackType)
            Assert.AreNotEqual(expected, actual)
        End Sub

        '''<summary>
        '''A test for ParseCallStack
        '''</summary>
        <TestMethod()>
        Public Sub ParseUserCallStackTest()
            Dim trace As StackTrace = New StackTrace(True)
            Dim skipLines As Integer = 0
            Dim totalLines As Integer = 0
            Dim callStackType As CallStackType = CallStackType.UserCallStack
            Dim expected As String = "s->   at isr."
            Dim actual As String = trace.ParseCallStack(skipLines, totalLines, callStackType).Trim
            Assert.IsTrue(actual.StartsWith(expected, StringComparison.OrdinalIgnoreCase), $"Trace results Actual: { actual.Substring(0, expected.Length)} Starts with {expected}")
        End Sub

        '''<summary>
        '''A test for ParseCallStack
        '''</summary>
        <TestMethod()>
        Public Sub ParseFullCallStackTest()
            Dim trace As StackTrace = New StackTrace(True)
            Dim skipLines As Integer = 0
            Dim totalLines As Integer = 0
            Dim callStackType As CallStackType = CallStackType.FullCallStack
            Dim expected As String = ""
            Dim actual As String
            actual = trace.ParseCallStack(skipLines, totalLines, callStackType)
            Assert.AreNotEqual(expected, actual)
        End Sub
#End Region

    End Class
End Namespace
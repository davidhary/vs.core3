﻿Imports isr.Core.Pith.GuidExtensions
Namespace Pith.Tests
    ''' <summary> GUID extensions tests. </summary>
    ''' <license>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="3/14/2018" by="David" revision=""> Created. </history>
    <TestClass()>
    Public Class GuidExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub


        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region
#Region " GUID EXTENSION TESTS "

        ''' <summary> (Unit Test Method) tests base 64 unique identifier. </summary>
        <TestMethod()>
        Public Sub Base64GuidTest()
            Dim newGuid As Guid = Guid.NewGuid
            Dim base64Guid As String = ""
            Dim fromBase64 As Guid = Nothing
            base64Guid = Convert.ToBase64String(newGuid.ToByteArray)
            fromBase64 = New Guid(Convert.FromBase64String(base64Guid))
            Assert.AreEqual(newGuid, fromBase64, "Converting from GUID to base 64 and back")
            Dim ExtBase64Guid As String = newGuid.NewBase64Guid()
            Assert.AreEqual(base64Guid, ExtBase64Guid.ToBase64String, "Comparing conversion and extension")
            fromBase64 = base64Guid.FromBase64Guid
            Assert.AreEqual(newGuid, fromBase64, "Extension conversion from GUID to base 64 and back")
        End Sub

#End Region

    End Class
End Namespace
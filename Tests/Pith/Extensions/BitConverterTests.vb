﻿Imports isr.Core.Pith.BitConverterExtensions
Namespace Pith.Tests
    ''' <summary> A bit converter tests. </summary>
    ''' <license>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="3/8/2018" by="David" revision="1.0.0.0"> Created. </history>
    <TestClass()> Public Class BitConverterTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <ClassInitialize()>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            TestInfo.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
        Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub


    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext

#End Region

#Region " BIT CONVERTER TESTS "

    ''' <summary> (Unit Test Method) tests true if little endian. </summary>
    <TestMethod()>
    Public Sub TrueIfLittleEndianTest()
        Dim isLittleEndian As Boolean = True
        Dim expected As Boolean = isLittleEndian
        Dim actual As Boolean = BitConverter.IsLittleEndian
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> (Unit Test Method) tests byte converter short. </summary>
    <TestMethod()>
    Public Sub ByteConverterShortTest()
        Dim value As UShort = 1234
        Dim values As Byte() = value.BigEndInt8
        Dim expected As UShort = values.BigEndUnsignedShort(0)
        Dim actual As UShort = values.BigEndUnsignedInt16(0)
        Assert.AreEqual(value, expected)
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> (Unit Test Method) tests bit converter short. </summary>
    <TestMethod()>
    Public Sub BitConverterShortTest()
        Dim value As UShort = 1234
        Dim values As Byte() = value.BigEndBytes
        Dim actual As UShort = values.BigEndUnsignedInt16(0)
        Dim expected As UShort = values.BigEndUnsignedShort(0)
        Assert.AreEqual(value, expected)
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> (Unit Test Method) tests bit converter single. </summary>
    <TestMethod()>
    Public Sub BitConverterSingleTest()
        Dim expected As Single = 1234.567
        Dim values As Byte() = expected.BigEndBytes
        Dim actual As Single = values.BigEndSingle(0)
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()>
    Public Sub BitConverterEndianessTest()
        Dim value As UShort = 1234
        Dim bigEndBytes As Byte() = value.BigEndBytes
        Dim bigEndInt8 As Byte() = value.BigEndInt8
        Dim actual As Byte = bigEndBytes(0)
        Dim expected As Byte = bigEndInt8(0)
        Assert.AreEqual(expected, actual)
        actual = bigEndBytes(1)
        expected = bigEndInt8(1)
        Assert.AreEqual(expected, actual)
        Dim littleBytes As Byte() = BitConverter.GetBytes(value)
        actual = littleBytes(0)
        expected = bigEndBytes(1)
        Assert.AreEqual(expected, actual)
        actual = littleBytes(1)
        expected = bigEndBytes(0)
        Assert.AreEqual(expected, actual)
    End Sub
#End Region


End Class
End Namespace
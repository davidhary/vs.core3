﻿Imports isr.Core.Pith
Namespace Pith.Tests
    ''' <summary> tests of equalities. </summary>
    ''' <license>
    ''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="10/10/2017" by="David" revision=""> Created. </history>
    <TestClass()>
    Public Class LineTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " EQUALITY TESTS "

        ''' <summary> Query if 'value' is empty. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> <c>true</c> if empty; otherwise <c>false</c> </returns>
        Public Shared Function IsEmpty(ByVal value As String) As Boolean
            Return String.IsNullOrEmpty(value) AndAlso Not value Is Nothing
        End Function

        Public Shared Function InfoGetter(ByVal value As String) As String
            Dim result As String = "?"
            If value Is Nothing Then
                result = "null"
            ElseIf String.IsNullOrEmpty(value) Then
                result = "empty"
            ElseIf String.IsNullOrWhiteSpace(value) Then
                result = "white"
            Else
                result = value
            End If
            Return result
        End Function

        ''' <summary> (Unit Test Method) tests string equality. </summary>
        ''' <param name="left">  The left. </param>
        ''' <param name="right"> The right. </param>
        Public Shared Sub EqualityTest(ByVal left As String, ByVal right As String)
            Assert.IsTrue(left = right, $"1. Left.{InfoGetter(left)} == Right.{InfoGetter(right)}")
            Assert.IsTrue(left = left, $"2. Left.{InfoGetter(left)} == Left.{InfoGetter(left)}")
            Assert.IsTrue(String.Equals(left, right), $"3. String.Equals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsTrue(String.Equals(left, left), $"4. String.Equals(Left.{InfoGetter(left)}, left.{InfoGetter(left)})")
            Assert.IsTrue(Object.ReferenceEquals(left, right), $"5. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsTrue(Object.ReferenceEquals(left, left), $"6. Object.ReferenceEquals(Left.{InfoGetter(left)}, left.{InfoGetter(left)})")
            Assert.IsTrue(CObj(left) Is CObj(right), $"7. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
            Assert.IsTrue(CObj(left) Is CObj(left), $"8. (Object)Left.{InfoGetter(left)} Is (Object)left.{InfoGetter(left)}")
        End Sub

        ''' <summary> (Unit Test Method) tests string equality. </summary>
        ''' <remarks> 
        ''' Results: 
        ''' <code>
        '''   TRUE: String.Empty == String.Empty
        '''   TRUE: String.Equals(String.Empty, String.Empty)
        '''   TRUE: String.ReferenceEquals(String.Empty, String.Empty)
        '''   TRUE: String.Empty Is String.Empty
        '''   
        '''   TRUE: null == null
        '''   TRUE: String.Equals(null, null)
        '''   TRUE: String.ReferenceEquals(null, null)
        '''   TRUE: null Is null
        '''   
        '''   TRUE: "" == ""
        '''   TRUE: String.Equals("", "")
        '''   TRUE: String.ReferenceEquals("", "")
        '''   TRUE: "" Is ""
        '''   
        '''   TRUE: String.Empty == ""
        '''   TRUE: String.Equals(String.Empty, "")
        '''   TRUE: String.ReferenceEquals(String.Empty, "")
        '''   TRUE: String.Empty Is ""
        '''   
        '''   TRUE: "a" == "a"
        '''   TRUE: String.Equals("a", "a")
        '''   TRUE: String.ReferenceEquals("a", "a")
        '''   TRUE: "a" Is "a"
        ''' </code>
        ''' Conclusions: 
        ''' (1) Nothing Equals Nothing and Empty equals "" (note below that Empty == Nothing but not equals Nothing )
        ''' </remarks>
        <TestMethod()>
        Public Sub StringEqualityTest()
            Dim left As String = String.Empty
            Dim right As String = String.Empty
            EqualityTest(left, right)
            left = Nothing
            right = Nothing
            EqualityTest(left, right)
            left = ""
            right = ""
            EqualityTest(left, right)
            left = String.Empty
            right = ""
            EqualityTest(left, right)
            left = "a"
            right = "a"
            EqualityTest(left, right)
        End Sub

        ''' <summary> (Unit Test Method) tests string inequality. </summary>
        ''' <param name="left">  The left. </param>
        ''' <param name="right"> The right. </param>
        Public Shared Sub InequalityTest(ByVal left As String, ByVal right As String)
            Assert.IsFalse(String.Equals(left, right), $"2. !String.Equals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsFalse(Object.ReferenceEquals(left, right), $"3. !Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsFalse(CObj(left) Is CObj(right), $"4. !(Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
        End Sub

        ''' <summary> (Unit Test Method) tests string equality. </summary>
        ''' <remarks> 
        ''' Results: 
        ''' <code>
        '''   TRUE: String.Empty = null
        '''   FALSE: String.Equals(String.Empty, null)
        '''   FALSE: String.ReferenceEquals(String.Empty, String.Empty)
        '''   FALSE: String.Empty Is String.Empty
        '''   
        '''   TRUE: null == ""
        '''   FALSE: String.Equals(null, "")
        '''   FALSE: String.ReferenceEquals(null, "")
        '''   FALSE: null Is ""
        '''   
        '''   FALSE: "a" == ""
        '''   FALSE: String.Equals("a", "")
        '''   FASLE: String.ReferenceEquals("a", "")
        '''   FALSE: "a" Is ""
        '''   
        '''   FALSE: "a" == "b"
        '''   FALSE: String.Equals("a", "b")
        '''   FALSE: String.ReferenceEquals("a", "b")
        '''   FALSE: "a" Is "b"
        ''' </code>
        ''' Conclusions:
        ''' (1) String.Equals() correct; except if
        ''' (2) Nothing == Empty but Not("" = Nothing)  !!!!
        ''' </remarks>
        <TestMethod()>
        Public Sub StringInequalityTest()
            Dim left As String = String.Empty
            Dim right As String = Nothing
            left = String.Empty
            right = Nothing
            Assert.IsTrue(left = right, $"1. Left.{InfoGetter(left)} == Right.{InfoGetter(right)}")
            InequalityTest(left, right)
            left = Nothing
            right = ""
            Assert.IsTrue(left = right, $"1. Left.{InfoGetter(left)} != Right.{InfoGetter(right)}")
            InequalityTest(left, right)
            left = "a"
            right = ""
            Assert.IsFalse(left = right, $"1. Left.{InfoGetter(left)} != Right.{InfoGetter(right)}")
            InequalityTest(left, right)
            left = "a"
            right = "b"
            Assert.IsFalse(left = right, $"1. Left.{InfoGetter(left)} != Right.{InfoGetter(right)}")
            InequalityTest(left, right)
        End Sub

        Public Shared Function InfoGetter(ByVal value As IEquatable(Of TimeSpan)) As String
            Dim result As String = "?"
            If value Is Nothing Then
                result = "null"
            Else
                result = value.ToString
            End If
            Return result
        End Function

        Public Shared Sub EqualityTest(ByVal left As IEquatable(Of TimeSpan), ByVal right As IEquatable(Of TimeSpan))
            Assert.IsTrue(Object.Equals(left, right), $"1. Object.Equals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsTrue(Object.Equals(left, left), $"2. Object.Equals(Left.{InfoGetter(left)}, left.{InfoGetter(left)})")
            Assert.IsFalse(Object.ReferenceEquals(left, right), $"3. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsTrue(Object.ReferenceEquals(left, left), $"4. Object.ReferenceEquals(Left.{InfoGetter(left)}, left.{InfoGetter(left)})")
            Assert.IsFalse(CObj(left) Is CObj(right), $"5. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
            Assert.IsTrue(CObj(left) Is CObj(left), $"6. (Object)Left.{InfoGetter(left)} Is (Object)left.{InfoGetter(left)}")
        End Sub

        Public Shared Sub InequalityTest(ByVal left As IEquatable(Of TimeSpan), ByVal right As IEquatable(Of TimeSpan))
            Assert.IsFalse(Object.Equals(left, right), $"1. !Object.Equals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsFalse(Object.ReferenceEquals(left, right), $"2. !Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsFalse(CObj(left) Is CObj(right), $"3. !(Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
        End Sub

        ''' <summary> (Unit Test Method) tests Structure equality. </summary>
        ''' <remarks> 
        ''' Results: 
        ''' <code>
        '''  The follwoing applies to timespan value, nothing or zero and from zero seconds
        '''   TRUE: TimeSpan.Zero == TimeSpan.Zero
        '''   TRUE: Object.Equals(TimeSpan.Zero, TimeSpan.Zero)
        '''   FALSE: Object.ReferenceEquals(left, right)
        '''   TRUE: Object.ReferenceEquals(left, left)
        '''   FALSE: left Is right
        '''   TRUE: left Is left
        '''   TRUE: TimeSpan.Zero == TimeSpan = nothing
        ''' </code>
        ''' Conclusions: 
        ''' (1) Nothing (Null) objects are equal.
        ''' (2) Time span = nothing is the same as Timespan.Zero
        ''' </remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")>
        <TestMethod()>
        Public Sub StructureEqualityTest()
            Dim left As TimeSpan = TimeSpan.Zero
            Dim right As TimeSpan = TimeSpan.Zero
            left = TimeSpan.Zero
            right = TimeSpan.Zero
            Assert.IsTrue(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            EqualityTest(left, right)
            left = Nothing
            right = Nothing
            Assert.IsTrue(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            EqualityTest(left, right)
            left = TimeSpan.Zero
            right = TimeSpan.FromSeconds(0)
            Assert.IsTrue(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            EqualityTest(left, right)
            left = TimeSpan.Zero
            right = Nothing
            Assert.IsTrue(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            EqualityTest(left, right)
        End Sub

        ''' <summary> (Unit Test Method) tests structure inequality. </summary>
        ''' <remarks> 
        ''' Results: 
        ''' <code>
        '''  The follwoing applies to timespan value, nothing or zero and from zero seconds
        '''   FALSE: TimeSpan.One == TimeSpan.Zero or nothing
        '''   FALSE: Object.Equals(TimeSpan.One, TimeSpan.Zero or nothing)
        '''   FALSE: Object.ReferenceEquals(left, right)
        '''   FALSE: left Is right
        '''   TRUE: TimeSpan.Zero == TimeSpan nothing
        ''' </code>
        ''' Conclusions: 
        ''' (1) Nothing (Null) objects are equal.
        ''' (2) Time span = nothing is the same as Timespan.Zero
        ''' </remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")>
        <TestMethod()>
        Public Sub StructureInequalityTest()
            Dim left As TimeSpan = TimeSpan.Zero
            Dim right As TimeSpan = Nothing
            left = TimeSpan.FromSeconds(1)
            right = Nothing
            Assert.IsFalse(left = right, $"a. Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            InequalityTest(left, right)
            left = TimeSpan.FromSeconds(1)
            right = TimeSpan.Zero
            Assert.IsFalse(left = right, $"b. Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            InequalityTest(left, right)
        End Sub

        ''' <summary> Information getter. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> A String. </returns>
        Public Shared Function InfoGetter(ByVal value As Object) As String
            Dim result As String = "?"
            If value Is Nothing Then
                result = "null"
            Else
                result = value.ToString
            End If
            Return result
        End Function

        Public Shared Sub EqualityTest(ByVal left As Object, ByVal right As Object)
            Assert.IsTrue(Object.Equals(left, right), $"1. Object.Equals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsTrue(Object.Equals(left, left), $"2. Object.Equals(Left.{InfoGetter(left)}, left.{InfoGetter(left)})")
            Assert.IsTrue(Object.ReferenceEquals(left, left), $"3. Object.ReferenceEquals(Left.{InfoGetter(left)}, left.{InfoGetter(left)})")
            Assert.IsTrue(CObj(left) Is CObj(left), $"4. (Object)Left.{InfoGetter(left)} Is (Object)left.{InfoGetter(left)}")
        End Sub

        Public Shared Sub InequalityTest(ByVal left As Object, ByVal right As Object)
            Assert.IsFalse(Object.Equals(left, right), $"1. !Object.Equals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsFalse(Object.ReferenceEquals(left, right), $"2. !Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsFalse(CObj(left) Is CObj(right), $"3. !(Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
        End Sub


        ''' <summary> (Unit Test Method) tests object equality. </summary>
        ''' <remarks> 
        ''' Results: 
        ''' <code>
        '''  The follwoing applies to version nothing or zero
        '''   TRUE: Version(0,0) == Version(0,0)
        '''   TRUE: Object.Equals(Version(0,0), Version(0,0))
        '''   FALSE: Object.ReferenceEquals(left, right)
        '''   TRUE: Object.ReferenceEquals(left(null), right(null))
        '''   TRUE: Object.ReferenceEquals(left, left)
        '''   FALSE: left Is right
        '''   TRUE: left(null) Is right(null)
        '''   TRUE: left Is left
        '''   TRUE: TimeSpan.Zero == TimeSpan = nothing
        ''' </code>
        ''' Conclusions: Nothing (Null) objects are equal.
        ''' </remarks>
        <TestMethod()>
        Public Sub VersionEqualityTest()
            Dim left As Version = New Version(0, 0)
            Dim right As Version = New Version(0, 0)
            left = New Version(0, 0)
            right = New Version(0, 0)
            Assert.IsTrue(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            Assert.IsFalse(Object.ReferenceEquals(left, right), $"a. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsFalse(CObj(left) Is CObj(right), $"b. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
            EqualityTest(left, right)
            left = Nothing
            right = Nothing
            TestInfo.TraceMessage($"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            Assert.IsTrue(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            Assert.IsTrue(Object.ReferenceEquals(left, right), $"a. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsTrue(CObj(left) Is CObj(right), $"b. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
            EqualityTest(left, right)
        End Sub

        ''' <summary> (Unit Test Method) tests object inequality. </summary>
        <TestMethod()>
        Public Sub VersionInequalityTest()
            Dim left As Version = New Version(0, 0)
            Dim right As Version = New Version(0, 0)
            left = New Version(0, 0)
            right = New Version(0, 1)
            TestInfo.TraceMessage($"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            Assert.IsFalse(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            InequalityTest(left, right)
            left = New Version(0, 0)
            right = Nothing
            TestInfo.TraceMessage($"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            Assert.IsFalse(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            InequalityTest(left, right)
        End Sub


        ''' <summary> (Unit Test Method) tests nullable equality. </summary>
        ''' <remarks>
        ''' Results:
        ''' <code>
        '''   NULL: New Integer? == New Integer?
        '''   TRUE: Object.Equals(left(New Integer?) , right(New Integer?))
        '''   TRUE: Object.ReferenceEquals(left(New Integer?) , right(New Integer?))
        '''   TRUE: Object.ReferenceEquals(left(New Integer?) , left(New Integer?))
        '''   TRUE: left(New Integer?) Is right(New Integer?)
        '''   TRUE: left(New Integer?) Is left(New Integer?)
        '''   
        '''   TRUE: New Integer?(1) == New Integer?(1)
        '''   TRUE: Object.Equals(left(New Integer?(1)) , right(New Integer?(1)))
        '''   FALSE: Object.ReferenceEquals(left(New Integer?(1)) , right(New Integer?(1)))
        '''   TRUE: Object.ReferenceEquals(left(New Integer?(1)) , left(New Integer?(1)))
        '''   FALSE: left(New Integer?(1)) Is right(New Integer?(1))
        '''   TRUE: left(New Integer?(1)) Is left(New Integer?(1))
        '''   
        '''   NULL: Integer?(null) == Integer?(null)
        '''   TRUE: Object.Equals(left(Integer?(null)) , right(Integer?(null)))
        '''   TRUE: Object.ReferenceEquals(left(Integer?(null)) , right(Integer?(null)))
        '''   TRUE: Object.ReferenceEquals(left(Integer?(null)) , left(Integer?(null)))
        '''   TRUE: left(Integer?(null)) Is right(Integer?(null))
        '''   TRUE: left(Integer?(null)) Is left(Integer?(null))
        '''   
        '''   NULL: New Integer? == Integer?(null)
        '''   TRUE: Object.Equals(left(New Integer?) , right(Integer?(null)))
        '''   TRUE: Object.ReferenceEquals(left(New Integer?) , right(Integer?(null)))
        '''   TRUE: Object.ReferenceEquals(left(New Integer?) , left(New Integer?))
        '''   TRUE: left(New Integer?) Is right(Integer?(null))
        '''   TRUE: left(New Integer?) Is left(New Integer?)
        ''' </code>
        ''' Conclusions: Nothing (Null) objects are equal but equality operations yield nothing.
        ''' </remarks>
        <TestMethod()>
        Public Sub NullableEqualityTest()
            Dim left As New Integer?
            Dim right As New Integer?
            left = New Integer?
            right = New Integer?
            Assert.IsTrue((left = right) Is Nothing, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            Assert.IsTrue(Object.ReferenceEquals(left, right), $"a. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsTrue(CObj(left) Is CObj(right), $"b. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
            EqualityTest(left, right)
            left = New Integer?(1)
            right = New Integer?(1)
            Assert.IsTrue((left = right).Value, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            Assert.IsFalse(Object.ReferenceEquals(left, right), $"a. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsFalse(CObj(left) Is CObj(right), $"b. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
            EqualityTest(left, right)
            left = Nothing
            right = Nothing
            Assert.IsTrue((left = right) Is Nothing, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            Assert.IsTrue(Object.ReferenceEquals(left, right), $"a. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsTrue(CObj(left) Is CObj(right), $"b. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
            EqualityTest(left, right)
            left = New Integer?
            right = Nothing
            Assert.IsTrue((left = right) Is Nothing, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            Assert.IsTrue(Object.ReferenceEquals(left, right), $"a. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsTrue(CObj(left) Is CObj(right), $"b. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
            EqualityTest(left, right)
        End Sub

        <TestMethod()>
        Public Sub NullableInequalityTest()
            Dim left As New Integer?
            Dim right As New Integer?
            left = New Integer?(0)
            right = New Integer?
            Assert.IsTrue((left = right) Is Nothing, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)} Is Not defined = Nothing")
            InequalityTest(left, right)
            left = New Integer?(0)
            right = Nothing
            Assert.IsTrue((left = right) Is Nothing, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)} Is Not defined = Nothing")
            InequalityTest(left, right)
        End Sub

#End Region

#Region " LINE TESTS "

        <TestMethod()>
        Public Sub LineEqualityTest()
            Dim left As LineF = New LineF(0, 0)
            Dim right As LineF = New LineF(0, 0)
            left = New LineF(0, 0)
            right = New LineF(0, 0)
            Assert.IsTrue(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            Assert.IsFalse(Object.ReferenceEquals(left, right), $"a. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsFalse(CObj(left) Is CObj(right), $"b. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
            EqualityTest(left, right)
            left = Nothing
            right = Nothing
            TestInfo.TraceMessage($"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            Assert.IsTrue(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            Assert.IsTrue(Object.ReferenceEquals(left, right), $"a. Object.ReferenceEquals(Left.{InfoGetter(left)}, Right.{InfoGetter(right)})")
            Assert.IsTrue(CObj(left) Is CObj(right), $"b. (Object)Left.{InfoGetter(left)} Is (Object)Right.{InfoGetter(right)}")
            EqualityTest(left, right)
        End Sub

        ''' <summary> (Unit Test Method) tests object inequality. </summary>
        <TestMethod()>
        Public Sub LineInequalityTest()
            Dim left As LineF = New LineF(0, 0)
            Dim right As LineF = New LineF(0, 0)
            left = New LineF(0, 0)
            right = New LineF(1, 0)
            TestInfo.TraceMessage($"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            Assert.IsFalse(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            InequalityTest(left, right)
            left = New LineF(0, 0)
            right = Nothing
            TestInfo.TraceMessage($"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            Assert.IsFalse(left = right, $"Left.{InfoGetter(left)} == right.{InfoGetter(right)}")
            InequalityTest(left, right)
        End Sub

#End Region

    End Class
End Namespace
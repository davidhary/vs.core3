﻿Imports isr.Core.Pith
Namespace Pith.Tests

    ''' <summary> Action Event Args tests. </summary>
    ''' <license>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="4/8/2018" by="David"> > Created. </history>
    <TestClass()>
    Public Class ActionEventArgsTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " ACTION EVENT ARGMENTS TESTS "

        <TestMethod()>
        Public Sub ActionEventTest()
            Dim args As ActionEventArgs = Nothing
            args = New ActionEventArgs(TraceEventType.Error)
            Dim message As String = "Information"
            Dim eventType As TraceEventType = TraceEventType.Information
            args.RegisterOutcomeEvent(eventType, message)
            Assert.IsFalse(args.Failed, $"Action {args.Details} at {eventType} failed")
            Assert.AreEqual(eventType, args.OutcomeEvent, $"Action {args.Details} event unexpected")
            Assert.IsTrue(args.HasDetails, $"Action {args.Details} at {eventType} reported not having details")

            eventType = TraceEventType.Error
            message = "Error"
            args.RegisterOutcomeEvent(eventType, message)
            Assert.IsTrue(args.Failed, $"Action {args.Details} at {eventType} did not failed")
            Assert.AreEqual(eventType, args.OutcomeEvent, $"Action {args.Details} event unexpected")

            eventType = TraceEventType.Critical
            message = "Critical"
            args.RegisterOutcomeEvent(eventType, message)
            Assert.IsTrue(args.Failed, $"Action {args.Details} at {eventType} did not failed")
            Assert.AreEqual(eventType, args.OutcomeEvent, $"Action {args.Details} event unexpected")
        End Sub

        <TestMethod()>
        Public Sub ActionEventCancellationTest()
            Dim args As ActionEventArgs = Nothing
            args = New ActionEventArgs(TraceEventType.Error)
            Dim message As String = "Cancel"
            args.RegisterFailure(message)
            Assert.IsTrue(args.Failed, $"Action {args.Details} failed to cancel")
        End Sub

#End Region

    End Class
End Namespace
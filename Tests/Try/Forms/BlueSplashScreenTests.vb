﻿'''<summary>
'''This is a test class for the blue splash screen.
'''</summary>
<TestClass()>
Public Class BlueSplashScreenTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <ClassInitialize()>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
    End Sub

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext

#End Region

    ''' <summary> (Unit Test Method) displays a splash screen test. </summary>
    ''' <remarks>
    ''' This unit test causes a warning when ran from the test agent:  
    ''' This is an issue with the way the picture box displays a gif image.
    ''' System.AppDomainUnloadedException: Attempted to access an unloaded AppDomain. This can happen
    ''' if the test(s) started a thread but did not stop it. Make sure that all the threads started
    ''' by the test(s) are stopped before completion.
    ''' https://stackoverflow.com/questions/42979071/appdomainunloadedexception-when-unit-testing-a-winforms-form-with-an-animated-gi.
    ''' </remarks>
    <TestMethod()>
    Public Sub DisplaySplashScreenTest()
        Dim sw As Stopwatch = Stopwatch.StartNew
        Using SplashScreen As isr.Core.WindowsForms.BlueSplash = New isr.Core.WindowsForms.BlueSplash()
            SplashScreen.Show()
            SplashScreen.TopmostSetter(True)
            For i As Integer = 1 To 10
                SplashScreen.DisplayMessage($"Splash message {Date.Now.ToLongTimeString}")
                isr.Core.Pith.StopwatchExtensions.Wait(sw, TimeSpan.FromMilliseconds(300))
            Next
        End Using
    End Sub

    ''' <summary> Tests the process exception on a another thread. </summary>
    <TestMethod()>
    Public Sub DisplaySplashScreenThreadTest()
        Dim oThread As New Threading.Thread(New Threading.ThreadStart(AddressOf DisplaySplashScreenTest))
        oThread.Start()
        oThread.Join()
    End Sub


End Class

﻿Imports System.Collections.Specialized
Imports System.Diagnostics.CodeAnalysis
Imports isr.Core.Pith
Imports isr.Core.Pith.CollectionExtensions
Public Class NameValueCollectionExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <ClassInitialize()>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            TestInfo.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
        Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext

#End Region


    Private Shared ReadOnly collection As NameValueCollection = New NameValueCollection From {{"ValidEntry", "ValidEntry"}}

    <TestMethod()>
    Public Sub DoesNotThrowExceptionWhenKeyDoesNotExist()
        Assert.Equals(collection.GetValueAs("InvalidEntry", "DefaultValue"), "DefaultValue")
    End Sub

    <TestMethod()>
    Public Sub DoesNotThrowExceptionWhenKeyExists()
        Assert.Equals(collection.GetValueAs(Of String)("ValidEntry"), "ValidEntry")
    End Sub

    <SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <TestMethod()>
    Public Sub ThrowsExceptionWhenKeyIsNullOrWhiteSpace()
        Try
            collection.GetValueAs(Of String)(Nothing)
        Catch ex As ArgumentException
        Catch
            Assert.Fail("expected exception was not thrown")
        End Try
        Try
            collection.GetValueAs(Of String)(Nothing)
        Catch ex As ArgumentException
        Catch
            Assert.Fail("expected exception was not thrown")
        End Try
        MyAssert.Throws(Of ArgumentException)(Function() collection.GetValueAs(Of String)(" "))
        MyAssert.Throws(Of ArgumentException)(Function() collection.GetValueAs(Of String)(vbTab))
        MyAssert.Throws(Of ArgumentException)(Function() collection.GetValueAs(Nothing, String.Empty))
        MyAssert.Throws(Of ArgumentException)(Function() collection.GetValueAs(" ", String.Empty))
        MyAssert.Throws(Of ArgumentException)(Function() collection.GetValueAs(vbTab, String.Empty))
    End Sub

End Class

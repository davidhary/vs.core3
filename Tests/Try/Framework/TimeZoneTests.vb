﻿Imports System.IO
Imports isr.Core.Pith.FileInfoExtensions
Imports isr.Core.Pith.StackTraceExtensions

''' <summary> A time zone tests. </summary>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/14/2018" by="David" revision=""> Created. </history>
<TestClass()>
Public Class TimeZoneTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <ClassInitialize()>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            TestInfo.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
        Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub


    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext

#End Region

#Region " TIME ZONE TESTS "

    ''' <summary> Query if 'timeZone' is daylight saving time. </summary>
    ''' <param name="timeZone">  The time zone. </param>
    ''' <param name="dateValue"> The date value. </param>
    ''' <returns> <c>true</c> if daylight saving time; otherwise <c>false</c> </returns>
    Public Overloads Shared Function IsDaylightSavingTime(ByVal timeZone As String, dateValue As String) As Boolean
        Return TimeZoneInfo.FindSystemTimeZoneById(timeZone).IsDaylightSavingTime(DateTimeOffset.Parse(dateValue))
    End Function

    <TestMethod()>
    Public Sub ArizonaTimeZoneTest()
        Dim dayString As String = "1/1/2017"
        Dim timeZone As String = "US Mountain Standard Time"
        Dim expected As Boolean = False
        Dim actual As Boolean = TimeZoneTests.IsDaylightSavingTime(timeZone, dayString)
        Assert.AreEqual(expected, actual)
        dayString = "6/1/2017"
        actual = TimeZoneTests.IsDaylightSavingTime(timeZone, dayString)
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()>
    Public Sub PacificTimeZoneTest()
        Dim dayString As String = "1/1/2017"
        Dim timeZone As String = "Pacific Standard Time"
        Dim expected As Boolean = False
        Dim actual As Boolean = TimeZoneTests.IsDaylightSavingTime(timeZone, dayString)
        Assert.AreEqual(expected, actual)
        dayString = "6/1/2017"
        expected = True
        actual = TimeZoneTests.IsDaylightSavingTime(timeZone, dayString)
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()>
    Public Sub GetArizonaTimeZoneTest()
        Dim expected As String = "US Mountain Standard Time"
        Dim actual As String = TimeZoneInfo.FindSystemTimeZoneById(expected).Id
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()>
    Public Sub LocalPacificStandardTimeTest()
        Dim tz As TimeZoneInfo = TimeZoneInfo.Local
        Dim expected As String = TestInfo.FirstTimeZone
        Dim actual As String = tz.Id
        Assert.AreEqual(expected, actual)
    End Sub

    Public Overloads Shared Function IsDaylightSavingTime(ByVal dateValue As String) As Boolean
        Dim tz As TimeZoneInfo = TimeZoneInfo.Local
        Return tz.IsDaylightSavingTime(DateTimeOffset.Parse(dateValue))
    End Function

    Public Overloads Shared Function IsDaylightSavingTime() As Boolean
        Dim tz As TimeZoneInfo = TimeZoneInfo.Local
        Return tz.IsDaylightSavingTime(DateTime.Now)
    End Function

    <TestMethod()>
    Public Sub DaylightSavingsTimeAffirmativeTest()
        Dim expectedBoolean As Boolean = False
        Dim actualBoolean As Boolean = TimeZoneTests.IsDaylightSavingTime("1/1/2017")
        Assert.AreEqual(expectedBoolean, actualBoolean)
    End Sub

    <TestMethod()>
    Public Sub DaylightSavingsTimeNegativeTest()
        Dim expectedBoolean As Boolean = True
        Dim actualBoolean As Boolean = TimeZoneTests.IsDaylightSavingTime("6/1/2017")
        Assert.AreEqual(expectedBoolean, actualBoolean)
    End Sub

    <TestMethod()>
    Public Sub UtcTimeOffsetTest()
        Dim expected As TimeSpan = TimeSpan.FromHours(If(TimeZoneTests.IsDaylightSavingTime(), TestInfo.FirstTimeZoneOffset + 1, TestInfo.FirstTimeZoneOffset))
        Dim actual As TimeSpan = DateTimeOffset.Now.Offset
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()>
    Public Sub UtcTimeConversionTest()
        Dim timeNow As DateTime = DateTime.Now
        Dim expected As DateTime = timeNow.ToUniversalTime
        Dim actual As DateTime = timeNow.Subtract(DateTimeOffset.Now.Offset)
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()>
    Public Sub UtcTimeTest()
        Dim timeNow As DateTime = DateTime.Now
        Dim expected As DateTime = timeNow.ToUniversalTime
        Dim actual As DateTime = timeNow.Subtract(DateTimeOffset.Parse(timeNow.Date.ToShortDateString).Offset)
        Assert.AreEqual(expected, actual)
    End Sub

#End Region

End Class

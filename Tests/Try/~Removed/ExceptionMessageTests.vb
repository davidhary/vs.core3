﻿Imports System.Windows.Forms
Imports Microsoft.VisualStudio.TestTools.UnitTesting
'''<summary>
'''This is a test class for ExceptionMessageTest and is intended
'''to contain all ExceptionMessageTest Unit Tests
'''</summary>
<TestClass()>
Public Class ExceptionMessageTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <ClassInitialize()>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
    End Sub

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext

#End Region

    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1802:UseLiteralsWhereAppropriate")>
    Private Shared ReadOnly TraceEventId As Integer = 111
    '''<summary>
    '''A test for DisplayException
    '''</summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <TestMethod(), TestCategory("UI")>
    Public Sub DisplayExceptionTest()
        Try
            Throw New DivideByZeroException()
        Catch ex As Exception
            Dim expected As DialogResult = DialogResult.OK
            Dim actual As DialogResult
            ex.Data.Add("@isr", "Exception test.")
            Dim box As New MyMessageBox(ex)
            actual = box.ShowDialog(Nothing)
            Assert.AreEqual(expected, actual)
        End Try
    End Sub

    ''' <summary> Tests the process exception on a another thread. </summary>
    <TestMethod(), TestCategory("UI")>
    Public Sub TestProcessExceptionThread()
        Dim oThread As New Threading.Thread(New Threading.ThreadStart(AddressOf TestProcessException))
        oThread.Start()
        oThread.Join()
    End Sub

    ''' <summary> Tests the process exception. </summary>
    ''' <exception cref="DivideByZeroException">        Thrown when an attempt is made to divide a
    ''' number by zero. </exception>
    <TestMethod(), TestCategory("UI")>
    Public Sub TestProcessException()
        Try
            Throw New DivideByZeroException()
        Catch ex As DivideByZeroException
            ex.Data.Add("@isr", "Exception test.")
            Dim result As DialogResult = MyMessageBox.ShowDialogAbortIgnore(Nothing, ex, MessageBoxIcon.Error)
            Windows.Forms.MessageBox.Show(result.ToString & " Requested")
        End Try
    End Sub



End Class

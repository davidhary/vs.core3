﻿Imports System.IO
Imports isr.Core.Pith.FileInfoExtensions
Imports isr.Core.Pith.StackTraceExtensions
'''<summary>
'''This is a test class for Machine Info.
'''</summary>
<TestClass()>
Public Class MachineInfoTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <ClassInitialize()>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            TestInfo.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
        Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
    End Sub

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext

#End Region


    '''<summary>
    '''A test for Machine ID Using insecure MD5 algorithm,
    '''</summary>
    ''' <remarks> Causes application domain exception with test agent. </remarks>
    <TestMethod()>
    Public Sub MachineUniqueIdMD5Test()
        ' on FIG10Dev
        Dim expected As String = "7ae637a47a35f672ed013462c7b92649"
        Dim actual As String = isr.Core.Pith.MachineInfo.BuildMachineUniqueIdMD5
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> (Unit Test Method) machine unique identifier SHA 1. </summary>
    ''' <remarks> Causes application domain exception with test agent. </remarks>
    <TestMethod()>
    Public Sub MachineUniqueIdSha1()
        ' on FIG10Dev
        Dim expected As String = "85f9c966b0a3f128c41186a0160aa0c9cce68e65"
        Dim actual As String = isr.Core.Pith.MachineInfo.BuildMachineUniqueIdSha1
        Assert.AreEqual(expected, actual)
    End Sub

End Class

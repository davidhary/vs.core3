﻿Imports isr.Core.Pith
Imports isr.Core.Pith.VisualBasicLoggingExtensions
'''<summary>
'''This is a test class for VisualBasicLoggingExtensionsTest and is intended
'''to contain all VisualBasicLoggingExtensionsTest Unit Tests
'''</summary>
<TestClass()>
Public Class LoggingExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <ClassInitialize()>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            TestInfo.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
        Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
    End Sub

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext

#End Region

    '''<summary>
    '''A test for ReplaceDefaultTraceListener with trace event.
    '''</summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="value")>
    Public Sub TraceEventLogTest(ByVal expected As TraceEventType)

        Dim actual As TraceEventType = expected
        Assert.AreEqual(expected, actual)

        Dim value As Logging.FileLogTraceListener = Nothing
        value = My.Application.Log.ReplaceDefaultTraceListener(UserLevel.AllUsers)
        If Not My.Application.Log.DefaultFileLogWriterFileExists Then
            My.Application.Log.TraceSource.TraceEvent(TraceEventType.Critical, 1, "trace event into {0}", My.Application.Log.DefaultFileLogWriterFilePath)
        End If
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Verbose, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Information, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Warning, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Critical, 1, "setting trace level to {0}", expected)
        My.Application.Log.ApplyTraceLevel(TraceEventType.Information)
        actual = My.Application.Log.TraceLevel
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Verbose, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Information, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Warning, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, 1, "trace event level {0}", My.Application.Log.TraceLevel)

    End Sub

    '''<summary>
    '''A test for ReplaceDefaultTraceListener with trace event.
    '''</summary>
    <TestMethod()>
    Public Sub TraceEventLogTestInformation()
        Me.TraceEventLogTest(TraceEventType.Information)
    End Sub

    '''<summary>
    '''A test for ReplaceDefaultTraceListener with logging log.
    '''</summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")>
    Public Sub LoggingLogTest(ByVal expected As TraceEventType)
        Dim actual As TraceEventType = expected
        Assert.AreEqual(expected, actual)
        Dim value As Logging.FileLogTraceListener = Nothing
        value = My.Application.Log.ReplaceDefaultTraceListener(UserLevel.AllUsers)
        If Not My.Application.Log.DefaultFileLogWriterFileExists Then
            My.Application.Log.TraceSource.TraceEvent(TraceEventType.Critical, 1, "trace event into {0}", My.Application.Log.DefaultFileLogWriterFilePath)
        End If
        My.Application.Log.WriteLogEntry(TraceEventType.Critical, "Tracing into {0}", value.FullLogFileName)
        My.Application.Log.WriteLogEntry(TraceEventType.Verbose, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Information, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Warning, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Error, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Critical, "setting trace level to {0}", expected)
        actual = My.Application.Log.TraceLevel
        My.Application.Log.WriteLogEntry(TraceEventType.Verbose, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Information, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Warning, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Error, "trace level {0}", My.Application.Log.TraceLevel)
    End Sub


    '''<summary>
    '''A test for ReplaceDefaultTraceListener with logging log.
    '''</summary>
    <TestMethod()>
    Public Sub LoggingLogTestInformation()
        Me.LoggingLogTest(TraceEventType.Information)
    End Sub

End Class

﻿Imports isr.Core.Controls
Namespace Controls.Tests
    '''<summary>
    '''This is a test class for MachineLogOnTest and is intended
    '''to contain all MachineLogOnTest Unit Tests
    '''</summary>
    <TestClass()>
Public Class MachineLogOnTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <ClassInitialize()>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
        Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext

#End Region

    '''<summary>
    '''A test for Validate
    '''</summary>
    <TestMethod()>
    Public Sub ValidateRole()
        Using target As MachineLogOn = New MachineLogOn()
            Dim userName As String = "admin"
            Dim password As String = "a"
            Dim roles As New List(Of String) From {"Administrators"}
            Dim allowedUserRoles As New ArrayList(roles)
            Dim expected As Boolean = True
            Dim actual As Boolean = target.Authenticate(userName, password, allowedUserRoles)
            Assert.AreEqual(expected, actual, target.ValidationMessage)
        End Using
    End Sub

    '''<summary>
    '''A test for Validate
    '''</summary>
    <TestMethod()>
    Public Sub ValidateUser()
        Using target As MachineLogOn = New MachineLogOn()
            Dim userName As String = "david"
            Dim password As String = "a"
            Dim expected As Boolean = True
            Dim actual As Boolean
            actual = target.Authenticate(userName, password)
            Assert.AreEqual(expected, actual, target.ValidationMessage)
        End Using
    End Sub
End Class
End Namespace

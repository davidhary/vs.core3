﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.LinqStatistics.EnumerableStats
Namespace LinqStatistics.Tests

    ''' <summary> A mode tests. </summary>
    ''' <license>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="4/13/2018" by="David" revision=""> Created. </history>
    <TestClass>
    Public Class ModeTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

        <TestMethod>
        Public Sub ModeUniform()
            Dim source As IEnumerable(Of Integer) = New Integer() {1, 1, 1}

            Dim result? As Integer = source.Mode()
            Assert.IsTrue(result.GetValueOrDefault(0) = 1)
        End Sub

        <TestMethod>
        Public Sub ModeNone()
            Dim source As IEnumerable(Of Integer) = New Integer() {1, 2, 3}

            Dim result? As Integer = source.Mode()
            Assert.IsFalse(result.HasValue)
        End Sub


        <TestMethod>
        Public Sub ModeOne()
            Dim source As IEnumerable(Of Integer) = New Integer() {1, 2, 2, 3}

            Dim result? As Integer = source.Mode()
            Assert.IsTrue(result.GetValueOrDefault(0) = 2)
        End Sub


        <TestMethod>
        Public Sub ModeTwo()
            Dim source As IEnumerable(Of Integer) = New Integer() {1, 2, 2, 3, 3, 3}

            Dim result? As Integer = source.Mode()
            Assert.IsTrue(result.GetValueOrDefault(0) = 3)
        End Sub

        <TestMethod>
        Public Sub ModeThree()
            Dim source As IEnumerable(Of Integer) = New Integer() {1, 2, 2, 3, 3}

            Dim result? As Integer = source.Mode()
            Assert.IsTrue(result.GetValueOrDefault(0) = 2)
        End Sub


        <TestMethod>
        Public Sub ModeNullable()
            Dim source As IEnumerable(Of Integer?) = New Integer?() {1, 3, 2, 3, Nothing, 2, 3}

            Dim result? As Integer = source.Mode()
            Assert.IsTrue(result.GetValueOrDefault(0) = 3)
        End Sub

        <TestMethod>
        Public Sub ModeMultiple()
            Dim source As IEnumerable(Of Integer) = New Integer() {1, 3, 2, 2, 3}

            Dim result As IEnumerable(Of Integer) = source.Modes()
            Assert.IsTrue(result.SequenceEqual(New Integer() {2, 3}))
        End Sub

        <TestMethod>
        Public Sub ModesMultiple2()
            Dim source As IEnumerable(Of Integer) = New Integer() {1, 3, 2, 2, 3, 3}

            Dim result As IEnumerable(Of Integer) = source.Modes()
            Assert.IsTrue(result.SequenceEqual(New Integer() {3, 2}))
        End Sub

        <TestMethod>
        Public Sub ModesMultipleNullable()
            Dim source As IEnumerable(Of Integer?) = New Integer?() {1, 2, Nothing, 2, 3, 3, 3}

            Dim result As IEnumerable(Of Integer) = source.Modes()
            Assert.IsTrue(result.SequenceEqual(New Integer() {3, 2}))
        End Sub

        <TestMethod>
        Public Sub ModesSingleValue()
            Dim source As IEnumerable(Of Integer) = New Integer() {1}

            Dim result? As Integer = source.Mode()
            Assert.IsTrue(result Is Nothing)
        End Sub
    End Class
End Namespace
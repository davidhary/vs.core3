﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.LinqStatistics.EnumerableStats
Namespace LinqStatistics.Tests

    ''' <summary> A population standard deviation tests. </summary>
    ''' <license>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="4/13/2018" by="David" revision=""> Created. </history>
    <TestClass>
    Public Class PopulationStandardDeviationTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

        <TestMethod>
        Public Sub SigmaDouble()
            Dim source As IEnumerable(Of Double) = TestData.GetDoubles()

            Dim result As Double = source.PopulationStandardDeviation()

            Assert.AreEqual(1.75552698640608, result, 0.000000000001)
        End Sub

        <TestMethod>
        Public Sub SigmaNullableDouble()
            Dim source As IEnumerable(Of Double?) = TestData.GetNullableDoubles()

            Dim result? As Double = source.PopulationStandardDeviation()

            Assert.AreEqual(1.75552698640608, CDbl(result), 0.000000000001)
        End Sub

        <TestMethod>
        Public Sub SigmaInt()
            Dim source As IEnumerable(Of Integer) = TestData.GetInts()

            Dim result As Double = source.PopulationStandardDeviation()

            Assert.AreEqual(1.4790199457749, result, 0.000000000001)
        End Sub

        <TestMethod>
        Public Sub SigmaNullableInt()
            Dim source As IEnumerable(Of Integer?) = TestData.GetNullableInts()

            Dim result? As Double = source.PopulationStandardDeviation()

            Assert.AreEqual(1.4790199457749, CDbl(result), 0.000000000001)
        End Sub

        <TestMethod>
        Public Sub SigmaPopulationDouble()
            Dim source As IEnumerable(Of Double) = TestData.GetDoubles()

            Dim result As Double = source.PopulationStandardDeviation()

            Assert.AreEqual(1.7555269864060763, result, 0.000000000001)
        End Sub

        <TestMethod>
        Public Sub SigmaPopulationNullableDouble()
            Dim source As IEnumerable(Of Double?) = TestData.GetNullableDoubles()

            Dim result? As Double = source.PopulationStandardDeviation()

            Assert.AreEqual(1.7555269864060763, CDbl(result), 0.000000000001)
        End Sub

        <TestMethod>
        Public Sub SigmaPopulationInt()
            Dim source As IEnumerable(Of Integer) = TestData.GetInts()

            Dim result As Double = source.PopulationStandardDeviation()

            Assert.AreEqual(1.479019945774904, result, 0.000000000001)
        End Sub

        <TestMethod>
        Public Sub SigmaPopulationNullableInt()
            Dim source As IEnumerable(Of Integer?) = TestData.GetNullableInts()

            Dim result? As Double = source.PopulationStandardDeviation()

            Assert.AreEqual(1.479019945774904, CDbl(result), 0.000000000001)
        End Sub
    End Class
End Namespace
﻿Imports System.Windows
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.LinqStatistics.EnumerableStats
Namespace LinqStatistics.Tests

    ''' <summary> Summary description for HistogramTests. </summary>
    ''' <license>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="4/13/2018" by="David" revision=""> Created. </history>
    <TestClass>
    Public Class HistogramTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="dummyResult")>
        <TestMethod>
        Public Sub HistogramTest()
            Dim useLinq As Boolean = False
            ' use a fixed seed to get a predictable random.
            Dim source As IEnumerable(Of Double) = TestData.GetRandomNormalDoubles(1, 10000)
            Dim lowerLimit As Double = -2
            Dim upperLimit As Double = 2
            Dim count As Integer = 20
            Dim binWidth As Double = (upperLimit - lowerLimit) / count
            Dim sw As Stopwatch = Stopwatch.StartNew
            Dim result As IEnumerable(Of Point)
            Dim linqSpeed As Long = 0
            Dim directSpeed As Long = 0
            If useLinq Then
                result = source.Histogram(lowerLimit, upperLimit, count)
                linqSpeed = sw.ElapsedTicks
                sw.Restart()
                Dim dummyResult As IEnumerable(Of Point) = source.HistogramDirect(lowerLimit, upperLimit, count)
                directSpeed = sw.ElapsedTicks
            Else
                Dim dummyResult As IEnumerable(Of Point) = source.Histogram(lowerLimit, upperLimit, count)
                linqSpeed = sw.ElapsedTicks
                sw.Restart()
                result = source.HistogramDirect(lowerLimit, upperLimit, count)
                directSpeed = sw.ElapsedTicks
            End If
            ' Linq speed 33235 could be over three times longer than direct 9143 speed.
            Assert.IsTrue(directSpeed < linqSpeed, $"Expected direct speed {directSpeed} to be lower than linq speed {linqSpeed}")

            ' count test: There are two extra bins above the high and below the low limits.
            Assert.AreEqual(result.Count, count + 2)

            ' abscissa range test: First bin is at the low limit; last is at the high limit.
            Assert.AreEqual(lowerLimit, result(0).X, 0.1 * binWidth)
            Assert.AreEqual(upperLimit, result(count + 1).X, 0.1 * binWidth)

            ' Second bin is at half the bin width past the low limit; last off by half bin width of the last bin.
            Assert.AreEqual(lowerLimit + 0.5 * binWidth, result(1).X, 0.1 * binWidth)
            Assert.AreEqual(upperLimit - 0.5 * binWidth, result(count).X, 0.1 * binWidth)

            ' expected value assuming random returns the same values each time.
            Dim expectedLowCount As Integer = 208
            Assert.AreEqual(expectedLowCount, CInt(result(0).Y))

            Dim expectedHighCount As Integer = 230
            Assert.AreEqual(expectedHighCount, CInt(result(count + 1).Y))
        End Sub

    End Class

End Namespace
﻿Imports System.Text
Imports isr.Core.Pith.RandomExtensions
Namespace LinqStatistics.Tests

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    Friend NotInheritable Class TestData

        Private Sub New()
        End Sub

        Public Shared Function GetInts() As IEnumerable(Of Integer)
            Return New Integer() {2, 3, 4, 6}
        End Function

        Public Shared Function GetNullableInts() As IEnumerable(Of Integer?)
            Return New Integer?() {2, Nothing, 3, 4, 6}
        End Function

        Public Shared Function GetDoubles() As IEnumerable(Of Double)
            Return New Double() {2.1, 3.5, 4.6, 6.9}
        End Function

        Public Shared Function GetNullableDoubles() As IEnumerable(Of Double?)
            Return New Double?() {2.1, 3.5, Nothing, 4.6, 6.9}
        End Function

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Shared Function GetFloats() As IEnumerable(Of Single)
            Return New Single() {2.1F, 3.5F, 4.6F, 6.9F}
        End Function

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Shared Function GetRandomNormalDoubles(ByVal count As Integer) As IEnumerable(Of Double)
            Return TestData.GetRandomNormalDoubles(New Random(), count)
        End Function

        Public Shared Function GetRandomNormalDoubles(ByVal seed As Integer, ByVal count As Integer) As IEnumerable(Of Double)
            Return TestData.GetRandomNormalDoubles(New Random(seed), count)
        End Function

        Public Shared Function GetRandomNormalDoubles(rnd As Random, ByVal count As Integer) As IEnumerable(Of Double)
            Dim l As New List(Of Double)
            For i As Integer = 1 To count
                l.Add(rnd.NextNormal)
            Next
            Return l.ToArray
        End Function
    End Class
End Namespace

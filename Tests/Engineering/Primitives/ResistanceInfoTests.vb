﻿Imports isr.Core.Engineering
Namespace Engineering.Tests
    '''<summary>
    '''This is a test class for ResistanceInfoTest and is intended
    '''to contain all ResistanceInfoTest Unit Tests
    '''</summary>
    <TestClass()>
    Public Class ResistanceInfoTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

        '''<summary>
        '''A test for TryParse
        '''</summary>
        Public Shared Sub TryParse(ByVal resistanceCode As String, ByVal resistanceExpected As Double, ByVal expected As Boolean)
            Dim resistance As Double = 0.0!
            Dim details As String = ""
            Dim actual As Boolean
            actual = ResistanceInfo.TryParse(resistanceCode, resistance, details)
            Assert.AreEqual(resistanceExpected, resistance, details)
            Assert.AreEqual(expected, actual, details)
        End Sub

        '''<summary>
        '''A test for TryParse
        '''</summary>
        <TestMethod()>
        Public Sub TryParseTest()
            ResistanceInfoTests.TryParse("121", 120, True)
            ResistanceInfoTests.TryParse("230", 23, True)
            ResistanceInfoTests.TryParse("1000", 100, True)
            ResistanceInfoTests.TryParse("2R67", 2.67, True)
            ResistanceInfoTests.TryParse("2.67", 2.67, True)
            ResistanceInfoTests.TryParse("12K6675", 12667.5, True)
            ResistanceInfoTests.TryParse("100R5", 100.5, True)
            ResistanceInfoTests.TryParse("R0015", 0.0015, True)
            ResistanceInfoTests.TryParse("100R", 100, True)
        End Sub

    End Class
End Namespace

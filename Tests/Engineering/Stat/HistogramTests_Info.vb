﻿Imports isr.Core.Pith.RandomExtensions
Namespace Engineering.Tests

    Partial Friend Class TestBridges

        ''' <summary> Gets the random normal doubles in this collection. </summary>
        ''' <param name="seed">  The seed. </param>
        ''' <param name="count"> Number of. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process the random normal doubles in this
        ''' collection.
        ''' </returns>
        Public Shared Function GetRandomNormalDoubles(ByVal seed As Integer, ByVal count As Integer) As IEnumerable(Of Double)
            Return TestBridges.GetRandomNormalDoubles(New Random(seed), count)
        End Function

        ''' <summary> Gets the random normal doubles in this collection. </summary>
        ''' <param name="randomNumberGenerator"> The random number generator. </param>
        ''' <param name="count">                 Number of. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process the random normal doubles in this
        ''' collection.
        ''' </returns>
        Public Shared Function GetRandomNormalDoubles(ByVal randomNumberGenerator As Random, ByVal count As Integer) As IEnumerable(Of Double)
            Dim l As New List(Of Double)
            For i As Integer = 1 To count
                l.Add(randomNumberGenerator.NextNormal)
            Next
            Return l.ToArray
        End Function

    End Class
End Namespace

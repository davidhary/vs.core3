﻿Namespace Engineering.Tests
    ''' <summary> Moving average tests. </summary>
    ''' <license>
    ''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="1/27/2016" by="David" revision="1.0.0.0"> Created. </history>
    <TestClass()>
    Public Class MovingAverageTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()

            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

        Private Const DefaultWindowLength As Integer = 5

        ''' <summary> Assert moving average. </summary>
        ''' <param name="count">         Number of. </param>
        ''' <param name="movingAverage"> The moving average. </param>
        ''' <param name="sample">        The sample. </param>
        Private Shared Sub AssertMovingAverage(ByVal count As Integer, ByVal movingAverage As Core.Engineering.MovingAverage, ByVal sample As Core.Engineering.SampleStatistics)
            Assert.AreEqual(sample.Minimum, movingAverage.Minimum, $"Minimum after adding {count} values")
            Assert.AreEqual(sample.Maximum, movingAverage.Maximum, $"Maximum after adding {count} values")
            Assert.AreEqual(sample.Mean, movingAverage.Mean, $"Mean after adding {count} values")
        End Sub

        ''' <summary> Adds a value. </summary>
        ''' <param name="rng">           The random number generator. </param>
        ''' <param name="queue">         The queue. </param>
        ''' <param name="movingAverage"> The moving average. </param>
        Private Shared Sub AddValue(ByVal rng As Random, ByVal queue As Queue(Of Double), ByVal movingAverage As Core.Engineering.MovingAverage)
            movingAverage.AddValue(rng.NextDouble)
            queue.Enqueue(movingAverage.Values.Last)
            Do While queue.Count > movingAverage.Count
                queue.Dequeue()
            Loop
        End Sub

        <TestMethod()>
        Public Sub MovingAverageTest()
            Dim movingAverage As New isr.Core.Engineering.MovingAverage(MovingAverageTests.DefaultWindowLength)
            Dim rng As New Random(DateTime.Now.Second)
            Dim sample As New isr.Core.Engineering.SampleStatistics()
            Dim queue As New Queue(Of Double)
            Dim count As Integer = 0
            count += 1
            MovingAverageTests.AddValue(rng, queue, movingAverage)
            sample.ClearKnownState() : sample.AddValues(queue.ToArray)
            MovingAverageTests.AssertMovingAverage(count, movingAverage, sample)
            count += 1
            MovingAverageTests.AddValue(rng, queue, movingAverage)
            sample.ClearKnownState() : sample.AddValues(queue.ToArray)
            MovingAverageTests.AssertMovingAverage(count, movingAverage, sample)
            count += 1
            MovingAverageTests.AddValue(rng, queue, movingAverage)
            sample.ClearKnownState() : sample.AddValues(queue.ToArray)
            MovingAverageTests.AssertMovingAverage(count, movingAverage, sample)
            count += 1
            MovingAverageTests.AddValue(rng, queue, movingAverage)
            sample.ClearKnownState() : sample.AddValues(queue.ToArray)
            MovingAverageTests.AssertMovingAverage(count, movingAverage, sample)
            count += 1
            MovingAverageTests.AddValue(rng, queue, movingAverage)
            sample.ClearKnownState() : sample.AddValues(queue.ToArray)
            MovingAverageTests.AssertMovingAverage(count, movingAverage, sample)
            count += 1
            MovingAverageTests.AddValue(rng, queue, movingAverage)
            sample.ClearKnownState() : sample.AddValues(queue.ToArray)
            MovingAverageTests.AssertMovingAverage(count, movingAverage, sample)

        End Sub

    End Class
End Namespace

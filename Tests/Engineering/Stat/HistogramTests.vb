﻿Imports System.Windows
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.Engineering.EnumerableStats
Namespace Engineering.Tests

    ''' <summary>
    ''' Summary description for HistogramTests
    ''' </summary>
    <TestClass>
Public Class HistogramTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <ClassInitialize()>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

        ''' <summary> (Unit Test Method) tests histogram. </summary>
        <TestMethod>
        Public Sub HistogramTest()
            ' use a fixed seed to get a predictable random.
            Dim source As IEnumerable(Of Double) = TestBridges.GetRandomNormalDoubles(1, 10000)
            Dim lowerLimit As Double = -2
        Dim upperLimit As Double = 2
        Dim count As Integer = 20
        Dim binWidth As Double = (upperLimit - lowerLimit) / count
        Dim result As IEnumerable(Of Point)
        Dim directSpeed As Long = 0
        Dim sw As Stopwatch = Stopwatch.StartNew
        result = source.Histogram(lowerLimit, upperLimit, count)
        sw.Restart()
        ' needs to run twice to make sure the code is compiled.
        result = source.Histogram(lowerLimit, upperLimit, count)
        directSpeed = sw.ElapsedTicks
        Dim linqSpeed As Long = 33235
        Assert.IsTrue((directSpeed < linqSpeed), $"Expected speed {directSpeed} to be lower than {linqSpeed}")


        ' count test: There are two extra bins above the high and below the low limits.
        Assert.AreEqual(result.Count, count + 2)

        ' abscissa range test: First bin is at the low limit; last is at the high limit.
        Assert.AreEqual(lowerLimit, result(0).X, 0.1 * binWidth)
        Assert.AreEqual(upperLimit, result(count + 1).X, 0.1 * binWidth)

        ' Second bin is at half the bin width past the low limit; last off by half bin width of the last bin.
        Assert.AreEqual(lowerLimit + 0.5 * binWidth, result(1).X, 0.1 * binWidth)
        Assert.AreEqual(upperLimit - 0.5 * binWidth, result(count).X, 0.1 * binWidth)

        ' expected value assuming random returns the same values each time.
        Dim expectedLowCount As Integer = 208
        Assert.AreEqual(expectedLowCount, CInt(result(0).Y))

        Dim expectedHighCount As Integer = 230
        Assert.AreEqual(expectedHighCount, CInt(result(count + 1).Y))
    End Sub

End Class

End Namespace
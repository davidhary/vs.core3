﻿Namespace Engineering.Tests
    ''' <summary> A moving window test. </summary>
    ''' <license>
    ''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="1/27/2016" by="David" revision="1.0.0.0"> Created. </history>
    <TestClass()>
    Public Class MovingWindowTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()

            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

        Private Const DefaultWindowLength As Integer = 5

        '''<summary>
        '''A test for Moving Window
        '''</summary>
        ''' <remarks>Passed 01/27/2016</remarks>
        <TestMethod()>
        Public Sub MovingWindowTest()
            Dim mw As New Core.Engineering.MovingWindow(MovingWindowTests.DefaultWindowLength) With {
            .RelativeWindow = New Core.Pith.RangeR(-0.05, 0.05),
            .ConformityRange = New Core.Pith.RangeR(0, 10),
            .OverflowRange = New Core.Pith.RangeR(-100, 100),
            .MaximumConsecutiveFailureCount = 3,
            .MaximumFailureCount = 10,
            .UpdateRule = Core.Engineering.MovingWindowUpdateRule.StopOnWithinWindow}
            mw.AddValue(0.9)
            mw.AddValue(1.1)
            mw.AddValue(0.95)
            mw.AddValue(1.05)
            mw.AddValue(1.0)

            Dim actualCount As Integer = mw.Count
            Dim expectedCount As Integer = 5
            Assert.AreEqual(expectedCount, actualCount)

            Dim actualMean As Double = mw.Mean
            Dim expectedMean As Double = 1
            Assert.AreEqual(expectedMean, actualMean, "Mean after adding the initial values")

            Dim actualMax As Double = mw.Maximum
            Dim expectedMax As Double = 1.1
            Assert.AreEqual(expectedMax, actualMax, "Maximum after adding the initial values")
            Dim actualMin As Double = mw.Minimum
            Dim expectedMin As Double = 0.9
            Assert.AreEqual(expectedMin, actualMin, "Minimum after adding the initial values")

            ' adding a value within the window, is not added to the moving window 
            mw.AddValue(1.01)
            Dim actualOutcome As Core.Engineering.MovingWindowStatus = mw.Status
            Dim expectedOutcome As Core.Engineering.MovingWindowStatus = Core.Engineering.MovingWindowStatus.Within
            Assert.AreEqual(expectedOutcome, actualOutcome)

            ' the moving window can stop because it is full and the new value falls within the window.
            Dim actualStopStatus As Boolean = mw.IsStopStatus
            Dim expectedStopStatus As Boolean = True
            Assert.AreEqual(expectedStopStatus, actualStopStatus, $"Expected stop status after {Core.Engineering.MovingWindowStatus.Within}")

            ' adding a value within the window, is not added to the moving window: the mean states unaffected.
            actualMean = mw.Mean
            expectedMean = 1
            Assert.AreEqual(expectedMean, actualMean, "Mean after adding a new value within the window")

            mw.AddValue(1.2)
            actualOutcome = mw.Status
            expectedOutcome = Core.Engineering.MovingWindowStatus.Above
            Assert.AreEqual(expectedOutcome, actualOutcome, $"Expected status after {Core.Engineering.MovingWindowStatus.Above} value")

            actualStopStatus = mw.IsStopStatus
            expectedStopStatus = False
            Assert.AreEqual(expectedStopStatus, actualStopStatus, $"Expected stop status after {Core.Engineering.MovingWindowStatus.Above}")

            actualMean = mw.Mean
            expectedMean = 1.06
            Assert.AreEqual(expectedMean, actualMean, "Mean after adding a large vale")
            actualMax = mw.Maximum
            expectedMax = 1.2
            Assert.AreEqual(expectedMax, actualMax)
            actualMin = mw.Minimum
            expectedMin = 0.9
            Assert.AreEqual(expectedMin, actualMin)

            mw.AddValue(0.96)
            mw.AddValue(1.03)
            actualOutcome = mw.Status
            expectedOutcome = Core.Engineering.MovingWindowStatus.Within
            Assert.AreEqual(expectedOutcome, actualOutcome, "Expected status before failures are added")

            actualStopStatus = mw.IsStopStatus
            expectedStopStatus = True
            Assert.AreEqual(expectedStopStatus, actualStopStatus, $"Expected stop status before failures {Core.Engineering.MovingWindowStatus.Above}")

            actualCount = mw.ConsecutiveFailureCount
            expectedCount = 0
            Assert.AreEqual(expectedCount, actualCount, "Consecutive failure count before consecutive failures")

            actualCount = mw.FailureCount
            expectedCount = 0
            Assert.AreEqual(expectedCount, actualCount, "Failure count before failures")

            mw.AddValue(mw.ConformityRange.Max + 1)

            actualCount = mw.ConsecutiveFailureCount
            expectedCount = 0
            Assert.AreEqual(expectedCount, actualCount, "Consecutive failure count before consecutive failures")

            actualCount = mw.FailureCount
            expectedCount = 1
            Assert.AreEqual(expectedCount, actualCount, "Failure count after one over range failure")

            actualOutcome = mw.Status
            expectedOutcome = Core.Engineering.MovingWindowStatus.Filling
            Assert.AreEqual(expectedOutcome, actualOutcome, $"Status after {Core.Engineering.ReadingStatus.NonConformal} failure")

            mw.AddValue(New Double?(), TimeSpan.Zero)

            actualStopStatus = mw.IsStopStatus
            expectedStopStatus = False
            Assert.AreEqual(expectedStopStatus, actualStopStatus, $"Expected stop status after {Core.Engineering.ReadingStatus.Indeterminable} failure")

            actualCount = mw.ConsecutiveFailureCount
            expectedCount = 0
            Assert.AreEqual(expectedCount, actualCount, "Consecutive failure count before consecutive failures")

            actualCount = mw.FailureCount
            expectedCount = 2
            Assert.AreEqual(expectedCount, actualCount, $"Failure count after one {Core.Engineering.ReadingStatus.NonConformal} and one {Core.Engineering.ReadingStatus.Indeterminable} failure")

            actualOutcome = mw.Status
            expectedOutcome = Core.Engineering.MovingWindowStatus.Filling
            Assert.AreEqual(expectedOutcome, actualOutcome, $"Status after {Core.Engineering.ReadingStatus.Indeterminable} failure")

            mw.AddValue(New Double?(), TimeSpan.Zero)

            actualCount = mw.ConsecutiveFailureCount
            expectedCount = 2
            Assert.AreEqual(expectedCount, actualCount, $"Consecutive failure count after two {Core.Engineering.ReadingStatus.Indeterminable} consecutive failures")

            actualCount = mw.FailureCount
            expectedCount = 3
            Assert.AreEqual(expectedCount, actualCount, $"Failure count after one {Core.Engineering.ReadingStatus.NonConformal} and two {Core.Engineering.ReadingStatus.Indeterminable} failure")

            actualOutcome = mw.Status
            expectedOutcome = Core.Engineering.MovingWindowStatus.Filling
            Assert.AreEqual(expectedOutcome, actualOutcome, $"Status after {Core.Engineering.ReadingStatus.Indeterminable} failure")

            mw.AddValue(mw.ConformityRange.Min - 1)

            actualCount = mw.ConsecutiveFailureCount
            expectedCount = 0
            Assert.AreEqual(expectedCount, actualCount, $"Consecutive failure count after reset of consecutive failures")

            actualCount = mw.FailureCount
            expectedCount = 4
            Assert.AreEqual(expectedCount, actualCount, $"Failure count after two {Core.Engineering.ReadingStatus.NonConformal} and two {Core.Engineering.ReadingStatus.Indeterminable} failure")

            actualOutcome = mw.Status
            expectedOutcome = Core.Engineering.MovingWindowStatus.Filling
            Assert.AreEqual(expectedOutcome, actualOutcome, $"Status after {Core.Engineering.ReadingStatus.NonConformal} failure")

            mw.AddValue(mw.ConformityRange.Min - 2)

            actualCount = mw.ConsecutiveFailureCount
            expectedCount = 2
            Assert.AreEqual(expectedCount, actualCount, $"Consecutive failure count after two {Core.Engineering.ReadingStatus.NonConformal} consecutive failures")

            actualCount = mw.FailureCount
            expectedCount = 5
            Assert.AreEqual(expectedCount, actualCount, $"Failure count after three {Core.Engineering.ReadingStatus.NonConformal} and two {Core.Engineering.ReadingStatus.Indeterminable} failure")

            actualOutcome = mw.Status
            expectedOutcome = Core.Engineering.MovingWindowStatus.Filling
            Assert.AreEqual(expectedOutcome, actualOutcome, $"Status after {Core.Engineering.ReadingStatus.NonConformal} failure")

            actualStopStatus = mw.IsStopStatus
            expectedStopStatus = False
            Assert.AreEqual(expectedStopStatus, actualStopStatus, $"Expected stop status after two consecutive {Core.Engineering.ReadingStatus.NonConformal} failures")

            mw.AddValue(mw.ConformityRange.Min - 3)

            actualCount = mw.ConsecutiveFailureCount
            expectedCount = 3
            Assert.AreEqual(expectedCount, actualCount, $"Consecutive failure count after three {Core.Engineering.ReadingStatus.NonConformal} consecutive failures")

            actualStopStatus = mw.IsStopStatus
            expectedStopStatus = True
            Assert.AreEqual(expectedStopStatus, actualStopStatus, $"Expected stop status after three consecutive {Core.Engineering.ReadingStatus.NonConformal} failures")

            mw.AddValue(mw.OverflowRange.Min)

            actualOutcome = mw.Status
            expectedOutcome = Core.Engineering.MovingWindowStatus.Filling
            Assert.AreEqual(expectedOutcome, actualOutcome, $"Status after {Core.Engineering.ReadingStatus.Overflow} failure")

            actualCount = mw.ConsecutiveFailureCount
            expectedCount = 0
            Assert.AreEqual(expectedCount, actualCount, $"Consecutive failure count after first {Core.Engineering.ReadingStatus.Overflow} failure")

            actualCount = mw.FailureCount
            expectedCount = 7
            Assert.AreEqual(expectedCount, actualCount,
                            $"Failure count after four {Core.Engineering.ReadingStatus.NonConformal}, two {Core.Engineering.ReadingStatus.Indeterminable} and one {Core.Engineering.ReadingStatus.NonConformal} failure")

            actualStopStatus = mw.IsStopStatus
            expectedStopStatus = False
            Assert.AreEqual(expectedStopStatus, actualStopStatus, $"Expected stop status after first (min) {Core.Engineering.ReadingStatus.Overflow} failure")

            mw.AddValue(mw.OverflowRange.Max)

            actualOutcome = mw.Status
            expectedOutcome = Core.Engineering.MovingWindowStatus.Filling
            Assert.AreEqual(expectedOutcome, actualOutcome, $"Status after second (max) {Core.Engineering.ReadingStatus.Overflow} failure")

            actualCount = mw.ConsecutiveFailureCount
            expectedCount = 2
            Assert.AreEqual(expectedCount, actualCount, $"Consecutive failure count after second (max) {Core.Engineering.ReadingStatus.Overflow} failure")

            actualCount = mw.FailureCount
            expectedCount = 8
            Assert.AreEqual(expectedCount, actualCount,
                            $"Failure count after four {Core.Engineering.ReadingStatus.NonConformal}, two {Core.Engineering.ReadingStatus.Indeterminable} and two {Core.Engineering.ReadingStatus.NonConformal} failures")

            actualStopStatus = mw.IsStopStatus
            expectedStopStatus = False
            Assert.AreEqual(expectedStopStatus, actualStopStatus, $"Expected stop status after second (max) {Core.Engineering.ReadingStatus.Overflow} failure")

            mw.ClearKnownState()

            actualMean = mw.Mean
            expectedMean = 0
            Assert.AreEqual(expectedMean, actualMean, "Mean after clear known state")

            actualMax = mw.Maximum
            expectedMax = Double.MinValue
            Assert.AreEqual(expectedMax, actualMax, "Maximum after clear known state")

            actualMin = mw.Minimum
            expectedMin = Double.MaxValue
            Assert.AreEqual(expectedMin, actualMin, "Minimum after clear known state")

            actualCount = mw.ConsecutiveFailureCount
            expectedCount = 0
            Assert.AreEqual(expectedCount, actualCount, $"Consecutive failure count after clear known state")

            actualCount = mw.FailureCount
            expectedCount = 0
            Assert.AreEqual(expectedCount, actualCount, $"Failure count after clear known state")

            actualStopStatus = mw.IsStopStatus
            expectedStopStatus = False
            Assert.AreEqual(expectedStopStatus, actualStopStatus, $"Expected stop status after clear known state")

            actualOutcome = mw.Status
            expectedOutcome = Core.Engineering.MovingWindowStatus.None
            Assert.AreEqual(expectedOutcome, actualOutcome, $"Status after clear known state")

        End Sub

    End Class
End Namespace

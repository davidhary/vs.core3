﻿Imports System.Windows
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.Engineering.LinqStatistics

Namespace LinqStatistics.UnitTests
    ''' <summary>
    ''' Summary description for HistogramTests
    ''' </summary>
    <TestClass>
    Public Class HistogramTests
        Public Sub New()
            MyBase.New()
        End Sub

        Private testContextInstance As TestContext

        ''' <summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext
            Get
                Return testContextInstance
            End Get
            Set(ByVal value As TestContext)
                testContextInstance = value
            End Set
        End Property

#Region "Additional test attributes"
        '''
        ''' You can use the following additional attributes as you write your tests:
        '''
        ''' Use ClassInitialize to run code before running the first test in the class
        ''' [ClassInitialize()]
        ''' public static void MyClassInitialize(TestContext testContext) { }
        '''
        ''' Use ClassCleanup to run code after all tests in a class have run
        ''' [ClassCleanup()]
        ''' public static void MyClassCleanup() { }
        '''
        ''' Use TestInitialize to run code before running each test 
        ''' [TestInitialize()]
        ''' public void MyTestInitialize() { }
        '''
        ''' Use TestCleanup to run code after each test has run
        ''' [TestCleanup()]
        ''' public void MyTestCleanup() { }
        '''
#End Region

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="dummyResult")>
        <TestMethod>
        Public Sub HistogramTest()
            Dim useLinq As Boolean = False
            ' use a fixed seed to get a predictable random.
            Dim source As IEnumerable(Of Double) = TestData.GetRandomNormalDoubles(1, 10000)
            Dim lowerLimit As Double = -2
            Dim upperLimit As Double = 2
            Dim count As Integer = 20
            Dim binWidth As Double = (upperLimit - lowerLimit) / count
            Dim sw As Stopwatch = Stopwatch.StartNew
            Dim result As IEnumerable(Of Point)
            Dim linqSpeed As Long = 0
            Dim directSpeed As Long = 0
            If useLinq Then
                result = source.Histogram(lowerLimit, upperLimit, count)
                linqSpeed = sw.ElapsedTicks
                sw.Restart()
                Dim dummyResult As IEnumerable(Of Point) = source.HistogramDirect(lowerLimit, upperLimit, count)
                directSpeed = sw.ElapsedTicks
            Else
                Dim dummyResult As IEnumerable(Of Point) = source.Histogram(lowerLimit, upperLimit, count)
                linqSpeed = sw.ElapsedTicks
                sw.Restart()
                result = source.HistogramDirect(lowerLimit, upperLimit, count)
                directSpeed = sw.ElapsedTicks
            End If
            ' Linq speed 6796/7792 is between 1.8 to 2.0 times longer than direct 3697 speed.
            Assert.AreNotEqual(directSpeed, linqSpeed)

            ' count test: There are two extra bins above the high and below the low limits.
            Assert.AreEqual(result.Count, count + 2)

            ' abscissa range test: First bin is at the low limit; last is at the high limit.
            Assert.AreEqual(lowerLimit, result(0).X, 0.1 * binWidth)
            Assert.AreEqual(upperLimit, result(count + 1).X, 0.1 * binWidth)

            ' Second bin is at half the bin width past the low limit; last off by half bin width of the last bin.
            Assert.AreEqual(lowerLimit + 0.5 * binWidth, result(1).X, 0.1 * binWidth)
            Assert.AreEqual(upperLimit - 0.5 * binWidth, result(count).X, 0.1 * binWidth)

            ' expected value assuming random returns the same values each time.
            Dim expectedLowCount As Integer = 208
            Assert.AreEqual(expectedLowCount, CInt(result(0).Y))

            Dim expectedHighCount As Integer = 230
            Assert.AreEqual(expectedHighCount, CInt(result(count + 1).Y))
        End Sub

    End Class


End Namespace

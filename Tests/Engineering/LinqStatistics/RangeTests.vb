﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.Engineering.LinqStatistics

Namespace LinqStatistics.UnitTests
	''' <summary>
	''' Summary description for RangeTests
	''' </summary>
	<TestClass> _
	Public Class RangeTests
		Public Sub New()
			'
			' TODO: Add constructor logic here
			'
		End Sub

		Private testContextInstance As TestContext

		''' <summary>
		'''Gets or sets the test context which provides
		'''information about and functionality for the current test run.
		'''</summary>
		Public Property TestContext() As TestContext
			Get
				Return testContextInstance
			End Get
			Set(ByVal value As TestContext)
				testContextInstance = value
			End Set
		End Property

		#Region "Additional test attributes"
		'
		' You can use the following additional attributes as you write your tests:
		'
		' Use ClassInitialize to run code before running the first test in the class
		' [ClassInitialize()]
		' public static void MyClassInitialize(TestContext testContext) { }
		'
		' Use ClassCleanup to run code after all tests in a class have run
		' [ClassCleanup()]
		' public static void MyClassCleanup() { }
		'
		' Use TestInitialize to run code before running each test 
		' [TestInitialize()]
		' public void MyTestInitialize() { }
		'
		' Use TestCleanup to run code after each test has run
		' [TestCleanup()]
		' public void MyTestCleanup() { }
		'
		#End Region

		<TestMethod> _
		Public Sub Range()
			Dim source As IEnumerable(Of Integer) = New Integer() { 1, 2, 3, 4, 5 }

			Dim result As Integer = source.Range()

			Assert.IsTrue(result = 4)
		End Sub
	End Class
End Namespace

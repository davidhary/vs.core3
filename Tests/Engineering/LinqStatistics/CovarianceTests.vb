﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.Engineering.LinqStatistics

Namespace LinqStatistics.UnitTests
	''' <summary>
	''' Summary description for CovarianceTests
	''' </summary>
	<TestClass> _
	Public Class CovarianceTests
		Public Sub New()
			'
			' TODO: Add constructor logic here
			'
		End Sub

		Private testContextInstance As TestContext

		''' <summary>
		'''Gets or sets the test context which provides
		'''information about and functionality for the current test run.
		'''</summary>
		Public Property TestContext() As TestContext
			Get
				Return testContextInstance
			End Get
			Set(ByVal value As TestContext)
				testContextInstance = value
			End Set
		End Property

		#Region "Additional test attributes"
		'
		' You can use the following additional attributes as you write your tests:
		'
		' Use ClassInitialize to run code before running the first test in the class
		' [ClassInitialize()]
		' public static void MyClassInitialize(TestContext testContext) { }
		'
		' Use ClassCleanup to run code after all tests in a class have run
		' [ClassCleanup()]
		' public static void MyClassCleanup() { }
		'
		' Use TestInitialize to run code before running each test 
		' [TestInitialize()]
		' public void MyTestInitialize() { }
		'
		' Use TestCleanup to run code after each test has run
		' [TestCleanup()]
		' public void MyTestCleanup() { }
		'
		#End Region

		<TestMethod> _
		Public Sub Covariance()
			Dim source As IEnumerable(Of Double) = TestData.GetDoubles()
			Dim other As IEnumerable(Of Double) = TestData.GetDoubles()


			Dim result As Double = source.Covariance(other)

			Assert.AreEqual(result, 3.081875, Double.Epsilon)
		End Sub

		<TestMethod> _
		Public Sub Covariance1()
			Dim source As IEnumerable(Of Double) = TestData.GetDoubles()
			Dim other As IEnumerable(Of Double) = TestData.GetInts().Select(Function(x) CDbl(x))


			Dim result As Double = source.Covariance(other)

			Assert.AreEqual(result, 2.59375, Double.Epsilon)
		End Sub


		<TestMethod> _
		Public Sub PearsonIdentity()
			Dim source As IEnumerable(Of Double) = TestData.GetDoubles()
			Dim other As IEnumerable(Of Double) = TestData.GetDoubles()


			Dim result As Double = source.Pearson(other)

			Assert.AreEqual(result, 1.0, Double.Epsilon)
		End Sub

		<TestMethod> _
		Public Sub Pearson1()
			Dim source As IEnumerable(Of Double) = TestData.GetDoubles()
			Dim other As IEnumerable(Of Double) = TestData.GetInts().Select(Function(x) CDbl(x))

			Dim result As Double = source.Pearson(other)

			Assert.AreEqual(result, 0.998956491208287, Double.Epsilon)
		End Sub
	End Class
End Namespace

﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.Engineering.LinqStatistics

Namespace LinqStatistics.UnitTests
    <TestClass>
    Public Class PopulationStandardDeviationTests
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Stdev")>
        <TestMethod>
        Public Sub StdevDouble()
            Dim source As IEnumerable(Of Double) = TestData.GetDoubles()

            Dim result As Double = source.PopulationStandardDeviation()

            Assert.AreEqual(result, 2.0271079563424013, Double.Epsilon)
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Stdev")>
        <TestMethod>
        Public Sub StdevNullableDouble()
            Dim source As IEnumerable(Of Double?) = TestData.GetNullableDoubles()

            Dim result? As Double = source.PopulationStandardDeviation()

            Assert.AreEqual(CDbl(result), 2.0271079563424013, Double.Epsilon)
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Stdev")>
        <TestMethod>
        Public Sub StdevInt()
            Dim source As IEnumerable(Of Integer) = TestData.GetInts()

            Dim result As Double = source.PopulationStandardDeviation()

            Assert.AreEqual(result, 1.707825127659933, Double.Epsilon)
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Stdev")>
        <TestMethod>
        Public Sub StdevNullableInt()
            Dim source As IEnumerable(Of Integer?) = TestData.GetNullableInts()

            Dim result? As Double = source.PopulationStandardDeviation()

            Assert.AreEqual(CDbl(result), 1.707825127659933, Double.Epsilon)
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Stdev")>
        <TestMethod>
        Public Sub StdevPDouble()
            Dim source As IEnumerable(Of Double) = TestData.GetDoubles()

            Dim result As Double = source.PopulationStandardDeviation()

            Assert.AreEqual(result, 1.7555269864060763, Double.Epsilon)
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Stdev")>
        <TestMethod>
        Public Sub StdevPNullableDouble()
            Dim source As IEnumerable(Of Double?) = TestData.GetNullableDoubles()

            Dim result? As Double = source.PopulationStandardDeviation()

            Assert.AreEqual(CDbl(result), 1.7555269864060763, Double.Epsilon)
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="PInt")>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Stdev")>
        <TestMethod>
        Public Sub StdevPInt()
            Dim source As IEnumerable(Of Integer) = TestData.GetInts()

            Dim result As Double = source.PopulationStandardDeviation()

            Assert.AreEqual(result, 1.479019945774904, Double.Epsilon)
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Stdev")>
        <TestMethod>
        Public Sub StdevPNullableInt()
            Dim source As IEnumerable(Of Integer?) = TestData.GetNullableInts()

            Dim result? As Double = source.PopulationStandardDeviation()

            Assert.AreEqual(CDbl(result), 1.479019945774904, Double.Epsilon)
		End Sub
	End Class
End Namespace

﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.Engineering.LinqStatistics

Namespace LinqStatistics.UnitTests
	<TestClass> _
	Public Class MedianTests
		<TestMethod> _
		Public Sub MedianDouble()
			Dim source As IEnumerable(Of Double) = TestData.GetDoubles()

			Dim result As Double = source.Median()

			Assert.AreEqual(result, 4.05, Double.Epsilon)
		End Sub

		<TestMethod> _
		Public Sub MedianNullableDouble()
			Dim source As IEnumerable(Of Double?) = TestData.GetNullableDoubles()

			Dim result? As Double = source.Median()

			Assert.AreEqual(CDbl(result), 4.05, Double.Epsilon)
		End Sub

		<TestMethod> _
		Public Sub MedianInt()
			Dim source As IEnumerable(Of Integer) = TestData.GetInts()

			Dim result As Double = source.Median()

			Assert.AreEqual(result, 3.5, Double.Epsilon)
		End Sub

		<TestMethod> _
		Public Sub MedianIntOddCount()
			Dim source As IEnumerable(Of Integer) = TestData.GetInts().Concat(New List(Of Integer) From {4})

			Dim result As Double = source.Median()

			Assert.AreEqual(result, 4, Double.Epsilon)
		End Sub


		<TestMethod> _
		Public Sub MedianNullableInt()
			Dim source As IEnumerable(Of Integer?) = TestData.GetNullableInts()

			Dim result? As Double = source.Median()

			Assert.AreEqual(CDbl(result), 3.5, Double.Epsilon)
		End Sub
		<TestMethod> _
		Public Sub MedianNullableIntOddCount()
			Dim source As IEnumerable(Of Integer?) = TestData.GetNullableInts().Concat(New List(Of Integer?) From {4})

			Dim result? As Double = source.Median()

			Assert.AreEqual(CDbl(result), 4, Double.Epsilon)
		End Sub
	End Class
End Namespace

﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.Engineering.LinqStatistics

Namespace LinqStatistics.UnitTests
	''' <summary>
	''' Summary description for ModeTests
	''' </summary>
	<TestClass> _
	Public Class ModeTests
		Public Sub New()
			'
			' TODO: Add constructor logic here
			'
		End Sub

		Private testContextInstance As TestContext

		''' <summary>
		'''Gets or sets the test context which provides
		'''information about and functionality for the current test run.
		'''</summary>
		Public Property TestContext() As TestContext
			Get
				Return testContextInstance
			End Get
			Set(ByVal value As TestContext)
				testContextInstance = value
			End Set
		End Property

		#Region "Additional test attributes"
		'
		' You can use the following additional attributes as you write your tests:
		'
		' Use ClassInitialize to run code before running the first test in the class
		' [ClassInitialize()]
		' public static void MyClassInitialize(TestContext testContext) { }
		'
		' Use ClassCleanup to run code after all tests in a class have run
		' [ClassCleanup()]
		' public static void MyClassCleanup() { }
		'
		' Use TestInitialize to run code before running each test 
		' [TestInitialize()]
		' public void MyTestInitialize() { }
		'
		' Use TestCleanup to run code after each test has run
		' [TestCleanup()]
		' public void MyTestCleanup() { }
		'
		#End Region

		<TestMethod> _
		Public Sub ModeUniform()
			Dim source As IEnumerable(Of Integer) = New Integer() { 1, 1, 1 }

			Dim result? As Integer = source.Mode()
            Assert.IsTrue(result.GetValueOrDefault(0) = 1)
        End Sub

		<TestMethod> _
		Public Sub ModeNone()
			Dim source As IEnumerable(Of Integer) = New Integer() { 1, 2, 3 }

			Dim result? As Integer = source.Mode()
			Assert.IsFalse(result.HasValue)
		End Sub


		<TestMethod> _
		Public Sub ModeOne()
			Dim source As IEnumerable(Of Integer) = New Integer() { 1, 2, 2, 3 }

			Dim result? As Integer = source.Mode()
            Assert.IsTrue(result.GetValueOrDefault(0) = 2)
        End Sub


		<TestMethod> _
		Public Sub ModeTwo()
			Dim source As IEnumerable(Of Integer) = New Integer() { 1, 2, 2, 3, 3, 3 }

			Dim result? As Integer = source.Mode()
            Assert.IsTrue(result.GetValueOrDefault(0) = 3)
        End Sub

		<TestMethod> _
		Public Sub ModeThree()
			Dim source As IEnumerable(Of Integer) = New Integer() { 1, 2, 2, 3, 3 }

			Dim result? As Integer = source.Mode()
            Assert.IsTrue(result.GetValueOrDefault(0) = 2)
        End Sub


		<TestMethod> _
		Public Sub ModeNullable()
			Dim source As IEnumerable(Of Integer?) = New Integer?() { 1, 3, 2, 3, Nothing, 2, 3 }

			Dim result? As Integer = source.Mode()
            Assert.IsTrue(result.GetValueOrDefault(0) = 3)
        End Sub

		<TestMethod> _
		Public Sub ModeMultiple()
			Dim source As IEnumerable(Of Integer) = New Integer() { 1, 3, 2, 2, 3 }

			Dim result As IEnumerable(Of Integer) = source.Modes()
			Assert.IsTrue(result.SequenceEqual(New Integer() { 2, 3 }))
		End Sub

		<TestMethod> _
		Public Sub ModesMultiple2()
			Dim source As IEnumerable(Of Integer) = New Integer() { 1, 3, 2, 2, 3, 3 }

			Dim result As IEnumerable(Of Integer) = source.Modes()
			Assert.IsTrue(result.SequenceEqual(New Integer() { 3, 2 }))
		End Sub

		<TestMethod> _
		Public Sub ModesMultipleNullable()
			Dim source As IEnumerable(Of Integer?) = New Integer?() { 1, 2, Nothing, 2, 3, 3, 3 }

			Dim result As IEnumerable(Of Integer) = source.Modes()
			Assert.IsTrue(result.SequenceEqual(New Integer() { 3, 2 }))
		End Sub

		<TestMethod> _
		Public Sub ModesSingleValue()
			Dim source As IEnumerable(Of Integer) = New Integer() { 1 }

			Dim result? As Integer = source.Mode()
			Assert.IsTrue(result Is Nothing)
		End Sub
	End Class
End Namespace

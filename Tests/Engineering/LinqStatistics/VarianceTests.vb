﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.Engineering.LinqStatistics

Namespace LinqStatistics.UnitTests
	<TestClass> _
	Public Class VarianceTests
		<TestMethod> _
		Public Sub VarDouble()
			Dim source As IEnumerable(Of Double) = TestData.GetDoubles()

			Dim result As Double = source.Variance()

			Assert.AreEqual(result, 4.1091666666666667, Double.Epsilon)
		End Sub

		<TestMethod> _
		Public Sub VarNullableDouble()
			Dim source As IEnumerable(Of Double?) = TestData.GetNullableDoubles()

			Dim result? As Double = source.Variance()

			Assert.AreEqual(CDbl(result), 4.1091666666666667, Double.Epsilon)
		End Sub

		<TestMethod> _
		Public Sub VarInt()
			Dim source As IEnumerable(Of Integer) = TestData.GetInts()

			Dim result As Double = source.Variance()

			Assert.AreEqual(result, 2.91666666666666667, Double.Epsilon)
		End Sub

		<TestMethod> _
		Public Sub VarNullableInt()
			Dim source As IEnumerable(Of Integer?) = TestData.GetNullableInts()

			Dim result? As Double = source.Variance()

			Assert.AreEqual(CDbl(result), 2.91666666666666667, Double.Epsilon)
		End Sub

		<TestMethod> _
		Public Sub VarPDouble()
			Dim source As IEnumerable(Of Double) = TestData.GetDoubles()

			Dim result As Double = source.PopulationVariance ()

			Assert.AreEqual(result, 3.081875, Double.Epsilon)
		End Sub

		<TestMethod> _
		Public Sub VarPNullableDouble()
			Dim source As IEnumerable(Of Double?) = TestData.GetNullableDoubles()

			Dim result? As Double = source.PopulationVariance ()

			Assert.AreEqual(CDbl(result), 3.081875, Double.Epsilon)
		End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="PInt")>
        <TestMethod>
        Public Sub VarPInt()
			Dim source As IEnumerable(Of Integer) = TestData.GetInts()

			Dim result As Double = source.PopulationVariance ()

			Assert.AreEqual(result, 2.1875, Double.Epsilon)
		End Sub

		<TestMethod> _
		Public Sub VarPNullableInt()
			Dim source As IEnumerable(Of Integer?) = TestData.GetNullableInts()

			Dim result? As Double = source.PopulationVariance ()

			Assert.AreEqual(CDbl(result), 2.1875, Double.Epsilon)
		End Sub
	End Class
End Namespace

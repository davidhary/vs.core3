﻿Imports isr.Core.Engineering
Namespace Engineering.Tests
    ''' <summary> Unit tests for testing attenuated voltage and current sources. </summary>
    ''' <license>
    ''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="9/18/2017" by="David" revision="1.0.0.0"> Created. </history>
    <TestClass()> Public Class AttenuatedSourceTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            Assert.IsTrue(VoltageSourceTestInfo.Get.Exists, $"{GetType(VoltageSourceTestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()


            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " CONVERSION TESTS "

        ''' <summary> (Unit Test Method) tests convert attenuated current source. </summary>
        <TestMethod()>
        Public Sub ConvertAttenuatedCurrentSourceTest()

            Dim attenuatedCurrentSource As AttenuatedCurrentSource = New AttenuatedCurrentSource(CDec(CurrentSourceTestInfo.Get.NominalCurrent),
                                                                                             CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                                 CurrentSourceTestInfo.Get.ParallelConductance)

            ' create converted voltage sources using alternative conversions
            Dim sources As New List(Of AttenuatedVoltageSource)
            Dim descriptions As New List(Of String)
            sources.Add(attenuatedCurrentSource.ToAttenuatedVoltageSource(CurrentSourceTestInfo.Get.NominalVoltage))
            descriptions.Add("Voltage source from attenuated current source")
            sources.Add(New VoltageSource(attenuatedCurrentSource).ToAttenuatedVoltageSource(CDec(CurrentSourceTestInfo.Get.NominalVoltage)))
            descriptions.Add("Voltage source from voltage source")
            sources.Add(attenuatedCurrentSource.ToVoltageSource.ToAttenuatedVoltageSource(CDec(CurrentSourceTestInfo.Get.NominalVoltage)))
            descriptions.Add("Voltage source from current source")

            Dim doubleEpsilon As Double = 0.0000000001
            Dim actualValue As Double = 1
            Dim expectedValue As Double = 1
            Dim message As String = ""
            Dim outcome As String = ""
            Dim item As String = ""
            Dim description As String = ""

            Dim source As AttenuatedVoltageSource
            For i As Integer = 0 To sources.Count - 1
                source = sources(i)
                description = descriptions(i)

                item = $"nominal voltage"
                actualValue = source.NominalVoltage
                expectedValue = CurrentSourceTestInfo.Get.NominalVoltage
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

                ' the two sources have identical output resistance
                item = $"equivalent resistance"
                actualValue = source.Resistance
                expectedValue = Conductor.ToResistance(attenuatedCurrentSource.Conductance)
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

                ' the sources have the same open load voltage
                item = $"open load voltage"
                actualValue = source.LoadVoltage(Resistor.OpenResistance)
                expectedValue = attenuatedCurrentSource.LoadVoltage(Resistor.OpenResistance)
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

                ' the sources have the same short load voltage
                item = $"short load voltage"
                actualValue = source.LoadVoltage(Resistor.ShortResistance)
                expectedValue = attenuatedCurrentSource.LoadVoltage(Resistor.ShortResistance)
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

                ' the sources have the same load voltage
                item = $"load voltage"
                actualValue = source.LoadVoltage(CurrentSourceTestInfo.Get.LoadResistance)
                expectedValue = attenuatedCurrentSource.LoadVoltage(CurrentSourceTestInfo.Get.LoadResistance)
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

            Next

        End Sub

        ''' <summary> (Unit Test Method) tests convert attenuated voltage source. </summary>
        <TestMethod()>
        Public Sub ConvertAttenuatedVoltageSourceTest()

            Dim mimnimumImpedance As Double = CurrentSourceTestInfo.Get.NominalVoltage / CurrentSourceTestInfo.Get.NominalCurrent
            Dim impedance As Double = isr.Core.Engineering.AttenuatedVoltageSource.ToEquivalentResistance(CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                                          CurrentSourceTestInfo.Get.ParallelConductance)
            Dim minImpedanceFactor As Double = 2 * mimnimumImpedance / impedance
            ' make sure that the voltage source impedance is high enough for conversion from 5v to 4 ma.
            Dim attenuatedVoltageSource As AttenuatedVoltageSource = New AttenuatedVoltageSource(CDec(CurrentSourceTestInfo.Get.NominalVoltage),
                                                                                             minImpedanceFactor * CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                             minImpedanceFactor * CurrentSourceTestInfo.Get.ParallelConductance)

            ' create converted current sources using alternative conversions
            Dim sources As New List(Of AttenuatedCurrentSource)
            Dim descriptions As New List(Of String)
            sources.Add(attenuatedVoltageSource.ToAttenuatedCurrentSource(CurrentSourceTestInfo.Get.NominalCurrent))
            descriptions.Add("Current source from attenuated voltage source")
            sources.Add(New CurrentSource(attenuatedVoltageSource).ToAttenuatedCurrentSource(CDec(CurrentSourceTestInfo.Get.NominalCurrent)))
            descriptions.Add("Current source from current source")
            sources.Add(attenuatedVoltageSource.ToCurrentSource.ToAttenuatedCurrentSource(CDec(CurrentSourceTestInfo.Get.NominalCurrent)))
            descriptions.Add("Current source from voltage source")

            Dim doubleEpsilon As Double = 0.0000000001
            Dim actualValue As Double = 1
            Dim expectedValue As Double = 1
            Dim message As String = ""
            Dim outcome As String = ""
            Dim item As String = ""
            Dim description As String = ""

            Dim source As AttenuatedCurrentSource
            For i As Integer = 0 To sources.Count - 1
                source = sources(i)
                description = descriptions(i)

                item = $"nominal current"
                actualValue = source.NominalCurrent
                expectedValue = CurrentSourceTestInfo.Get.NominalCurrent
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

                ' the two sources have identical output resistance
                item = $"equivalent resistance"
                actualValue = Conductor.ToResistance(source.Conductance)
                expectedValue = attenuatedVoltageSource.Resistance
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

                ' the sources have the same short load current
                item = $"short load current"
                actualValue = source.LoadCurrent(Conductor.ShortConductance)
                expectedValue = attenuatedVoltageSource.LoadCurrent(Resistor.ShortResistance)
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

                ' the sources have the same open load current
                item = $"open load current"
                actualValue = source.LoadCurrent(Conductor.OpenConductance)
                expectedValue = attenuatedVoltageSource.LoadCurrent(Resistor.OpenResistance)
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

                ' the sources have the same load current
                item = $"load current"
                actualValue = source.LoadCurrent(CurrentSourceTestInfo.Get.LoadConductance)
                expectedValue = attenuatedVoltageSource.LoadCurrent(CurrentSourceTestInfo.Get.LoadResistance)
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

            Next

        End Sub

#End Region

#Region " EXCEPTION CONVERSION TESTS "

        ''' <summary> (Unit Test Method) tests low nominal voltage source exception. </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="source")>
        <TestMethod()>
        <ExpectedException(GetType(InvalidOperationException), "Conversion from an attenuated current source to an attenuated voltage source with low nominal allowed")>
        Public Sub LowNominalVoltageSourceExceptionTest()
            Dim currentSource As AttenuatedCurrentSource = New AttenuatedCurrentSource(CDec(CurrentSourceTestInfo.Get.NominalCurrent),
                                                                                       CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                       CurrentSourceTestInfo.Get.ParallelConductance)
            Dim source As AttenuatedVoltageSource = currentSource.ToAttenuatedVoltageSource(0.99 * currentSource.LoadVoltage(Resistor.OpenResistance))
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="source")>
        <TestMethod()>
        <ExpectedException(GetType(InvalidOperationException), "Conversion from an attenuated voltage source to an attenuated current source with low nominal current allowed")>
        Public Sub LowNominalCurrentSourceExceptionTest()
            Dim voltageSource As AttenuatedVoltageSource = New AttenuatedVoltageSource(CDec(CurrentSourceTestInfo.Get.NominalVoltage),
                                                                                       CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                       CurrentSourceTestInfo.Get.ParallelResistance)
            Dim source As AttenuatedCurrentSource = voltageSource.ToAttenuatedCurrentSource(0.99 * voltageSource.LoadCurrent(Resistor.ShortResistance))
        End Sub

        <TestMethod()>
        <ExpectedException(GetType(ArgumentNullException), "Null Attenuated Current Source inappropriately allowed")>
        Public Sub NullAttenuatedCurrentSourceExceptionTest()
            Dim currentSource As AttenuatedCurrentSource = Nothing
            Dim source As AttenuatedVoltageSource = New AttenuatedVoltageSource(CDec(CurrentSourceTestInfo.Get.NominalVoltage),
                                                                                CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                CurrentSourceTestInfo.Get.ParallelConductance)
            source.FromAttenuatedCurrentSource(currentSource, CDec(CurrentSourceTestInfo.Get.NominalVoltage))
        End Sub

        <TestMethod()>
        <ExpectedException(GetType(ArgumentNullException), "Null Attenuated Voltage Source inappropriately allowed")>
        Public Sub NullAttenuatedVoltageSourceExceptionTest()
            Dim VoltageSource As AttenuatedVoltageSource = Nothing
            Dim source As AttenuatedCurrentSource = New AttenuatedCurrentSource(CDec(CurrentSourceTestInfo.Get.NominalCurrent),
                                                                                CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                CurrentSourceTestInfo.Get.ParallelConductance)
            source.FromAttenuatedVoltageSource(VoltageSource, CDec(CurrentSourceTestInfo.Get.NominalCurrent))
        End Sub


#End Region

    End Class
End Namespace

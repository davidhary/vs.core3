﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.Engineering

<TestClass()>
Public Class BalanceBridgeSpanUnitTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <ClassInitialize()>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            TestInfo.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
        Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub


    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext

#End Region

#Region " SPAN TESTS - NOT WORKING BECAUSE THE COMUDAS BRIDGES ARE NOT BALANCED "

    <TestMethod, Ignore>
    Public Sub LowTempFullScalePressureOutputTest()

        Dim reportBridge As SampleBridge = TestInfo.LowTempFullScalePressureBridge
        Dim bridge As BalanceBridge = reportBridge.BalanceBridge

        Dim expectedBalance As Boolean = False
        Dim actualBalance As Boolean = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance)

        ' TO_DO: Use the two bridges to estimate the equivalent source span resistance.
        Dim actualResistance As Double = bridge.BridgeResistance
        Dim expectedResistance As Double = reportBridge.EquivalentResistance
        ' The equivalent resistance is derived for sensitivity -- not the same as the bridge resistance
        ' Assert.AreEqual(expectedResistance, actualResistance, expectedResistance * BalanceBridgeSpanUnitTests.SourceResistanceTolerance)

        Dim outputVoltage As Double = bridge.Output(reportBridge.BridgeVoltage)
        Dim expectedVoltage As Double = reportBridge.OutputVoltage
        Dim epsilon As Double = TestInfo.OutputVoltageEpsilon
        Assert.AreEqual(expectedVoltage, outputVoltage, TestInfo.OutputVoltageEpsilon)

        Dim voltageSource As New AttenuatedVoltageSource(reportBridge.NominalVoltage,
                                                      reportBridge.VoltageSourceSeriesResistance,
                                                      reportBridge.VoltageSourceParallelResistance)
        outputVoltage = bridge.Output(voltageSource)
        expectedVoltage = reportBridge.NominalFullScaleVoltage  ' actual = 0.02001
        Assert.AreEqual(expectedVoltage, outputVoltage, TestInfo.FullScaleVoltageEpsilon)

        Dim currentSource As AttenuatedCurrentSource = voltageSource.ToAttenuatedCurrentSource(CDec(reportBridge.NominalCurrent))
        actualResistance = currentSource.SeriesResistance
        expectedResistance = reportBridge.CurrentSourceSeriesResistance ' 150
        epsilon = expectedResistance * TestInfo.SourceResistanceTolerance
        Assert.AreEqual(expectedResistance, actualResistance, epsilon)

        actualResistance = 1 / currentSource.ParallelConductance
        expectedResistance = reportBridge.CurrentSourceParallelResistance ' reported 632
        epsilon = expectedResistance * TestInfo.SourceResistanceTolerance
        Assert.AreEqual(expectedResistance, actualResistance, epsilon)

        ' test full scale current source bridge output
        outputVoltage = bridge.Output(currentSource)
        expectedVoltage = reportBridge.NominalFullScaleVoltage  ' actual =0.020006
        Assert.AreEqual(expectedVoltage, outputVoltage, TestInfo.FullScaleVoltageEpsilon)

    End Sub

    <TestMethod, Ignore()>
    Public Sub BridgeCompensationChangeTest()

        Dim reportBridge As SampleBridge = TestInfo.HighTempZeroPressureBridge
        Dim bridge As BalanceBridge = reportBridge.BalanceBridge

        Dim expectedBalance As Boolean = True
        Dim actualBalance As Boolean = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance, "Bridge is expected to be balanced at zero pressure")

        ' zero the bottom right resistance
        Dim r2Gain As Double = 0.99
        bridge.BridgeBalance = New BridgeBalance(New Double() {bridge.BridgeBalance.Values(0), bridge.BridgeBalance.Values(1),
                                                 r2Gain * bridge.BridgeBalance.Values(2), bridge.BridgeBalance.Values(3)}, bridge.BridgeBalance.Layout)
        expectedBalance = True
        actualBalance = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance)

        ' test balance
        Dim outputVoltage As Double = bridge.NakedBridge.Output(reportBridge.BridgeVoltage)
        Dim expectedVoltage As Double = reportBridge.OutputVoltage
        Assert.AreEqual(expectedVoltage, outputVoltage, TestInfo.OutputVoltageEpsilon)

        ' restore the bottom right resistance
        bridge = reportBridge.BalanceBridge
        bridge.BridgeBalance = New BridgeBalance(New Double() {bridge.BridgeBalance.Values(0), bridge.BridgeBalance.Values(1) / r2Gain,
                                                 bridge.BridgeBalance.Values(2), bridge.BridgeBalance.Values(3)}, bridge.BridgeBalance.Layout)

        expectedBalance = True
        actualBalance = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance)

        ' test balance
        outputVoltage = bridge.NakedBridge.Output(reportBridge.BridgeVoltage)
        expectedVoltage = reportBridge.OutputVoltage
        Assert.AreEqual(expectedVoltage, outputVoltage, TestInfo.OutputVoltageEpsilon)

        ' adjust the top left conductance
        Dim rz4Gain As Double = 1.01
        bridge = reportBridge.BalanceBridge
        bridge.BridgeBalance = New BridgeBalance(New Double() {bridge.BridgeBalance.Values(0), bridge.BridgeBalance.Values(1) / r2Gain,
                                                 bridge.BridgeBalance.Values(2), rz4Gain * bridge.BridgeBalance.Values(3)}, bridge.BridgeBalance.Layout)

        expectedBalance = True
        actualBalance = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance)

        ' test balance
        outputVoltage = bridge.NakedBridge.Output(reportBridge.BridgeVoltage)
        expectedVoltage = reportBridge.OutputVoltage
        Assert.AreEqual(expectedVoltage, outputVoltage, TestInfo.OutputVoltageEpsilon)

        ' restore the top left conductance
        bridge = reportBridge.BalanceBridge
        bridge.BridgeBalance = New BridgeBalance(New Double() {bridge.BridgeBalance.Values(0), bridge.BridgeBalance.Values(1) / r2Gain,
                                                 bridge.BridgeBalance.Values(2), bridge.BridgeBalance.Values(3) / rz4Gain}, bridge.BridgeBalance.Layout)

        ' test full scale change
        reportBridge = TestInfo.LowTempFullScalePressureBridge
        bridge = reportBridge.BalanceBridge

        Dim voltageSource As New AttenuatedVoltageSource(reportBridge.NominalVoltage,
                                                      reportBridge.VoltageSourceSeriesResistance,
                                                      reportBridge.VoltageSourceParallelResistance)
        outputVoltage = bridge.Output(voltageSource)
        expectedVoltage = reportBridge.NominalFullScaleVoltage  ' actual = 0.02001
        Assert.AreEqual(expectedVoltage, outputVoltage, TestInfo.FullScaleVoltageEpsilon)

        bridge = reportBridge.BalanceBridge
        bridge.BridgeBalance = New BridgeBalance(New Double() {bridge.BridgeBalance.Values(0), bridge.BridgeBalance.Values(1) * r2Gain,
                                                 bridge.BridgeBalance.Values(2), bridge.BridgeBalance.Values(3) * rz4Gain}, bridge.BridgeBalance.Layout)
        ' bridge.BalanceValues = New Double() {bridge.BalanceValues(0), bridge.BalanceValues(1) * r2Gain, bridge.BalanceValues(2), bridge.BalanceValues(3)}
        outputVoltage = bridge.Output(voltageSource)
        expectedVoltage = reportBridge.NominalFullScaleVoltage  ' actual = 0.02001
        Assert.AreEqual(expectedVoltage, outputVoltage, TestInfo.FullScaleVoltageEpsilon)

        bridge = reportBridge.BalanceBridge
        outputVoltage = bridge.Output(voltageSource)
        expectedVoltage = reportBridge.NominalFullScaleVoltage  ' actual = 0.02001
        Assert.AreEqual(expectedVoltage, outputVoltage, TestInfo.FullScaleVoltageEpsilon)

    End Sub

#End Region

End Class

﻿Imports isr.Core.Engineering
Namespace Engineering.Tests

    ''' <summary> A balance bridge unit tests. </summary>
    ''' <license>
    ''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="9/23/2017" by="David" revision=""> Created. </history>
    <TestClass()> Public Class BalanceBridgeTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <ClassInitialize()>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            testInfo.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext

#End Region

#Region " CONSTRUCTION TESTS "

    ''' <summary> (Unit Test Method) tests build balance bridge. </summary>
    <TestMethod()> Public Sub BuildBalanceBridgeTest()

        Dim sampleBridge As SampleBridge = TestBridges.LowTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        ' test expected values
        Dim expectedResistance As Double = 0
        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            expectedResistance = Resistor.ShuntConductor(bridge.NakedBridge.TopRight, bridge.BridgeBalance.Values(0))
        ElseIf bridge.BridgeBalance.Layout = BalanceLayout.RightShuntRightSeries Then
            expectedResistance = Resistor.ShuntConductor(bridge.NakedBridge.TopRight, bridge.BridgeBalance.Values(0)) + bridge.BridgeBalance.Values(1)
        End If
        Dim actualResistance As Double = bridge.TopRight
        Assert.AreEqual(expectedResistance, actualResistance, TestBridges.ElementEpsilon)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            expectedResistance = Resistor.ToSeries(bridge.NakedBridge.BottomRight, bridge.BridgeBalance.Values(1))
        ElseIf bridge.BridgeBalance.Layout = BalanceLayout.RightShuntRightSeries Then
            expectedResistance = Resistor.ShuntConductor(bridge.NakedBridge.BottomRight, bridge.BridgeBalance.Values(3)) + bridge.BridgeBalance.Values(2)
        End If
        actualResistance = bridge.BottomRight
        Assert.AreEqual(expectedResistance, actualResistance, TestBridges.ElementEpsilon)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            expectedResistance = Resistor.ShuntConductor(bridge.NakedBridge.TopLeft, bridge.BridgeBalance.Values(3))
        ElseIf bridge.BridgeBalance.Layout = BalanceLayout.RightShuntRightSeries Then
            expectedResistance = bridge.NakedBridge.TopLeft
        End If
        actualResistance = bridge.TopLeft
        Assert.AreEqual(expectedResistance, actualResistance, TestBridges.ElementEpsilon)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            expectedResistance = Resistor.ToSeries(bridge.NakedBridge.BottomLeft, bridge.BridgeBalance.Values(2))
        ElseIf bridge.BridgeBalance.Layout = BalanceLayout.RightShuntRightSeries Then
            expectedResistance = bridge.NakedBridge.BottomLeft
        End If
        actualResistance = bridge.BottomLeft
        Assert.AreEqual(expectedResistance, actualResistance, TestBridges.ElementEpsilon)

    End Sub

    ''' <summary> (Unit Test Method) tests build balance bridge. </summary>
    <TestMethod()> Public Sub BuildInitialBalanceBridgeTest()

        Dim sampleBridge As SampleBridge = TestBridges.LowTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = New BalanceBridge(sampleBridge.Bridge, sampleBridge.BalanceBridge.BridgeBalance.Layout)

        ' test expected values
        Dim expectedResistance As Double = 0
        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            expectedResistance = Resistor.ShuntConductor(bridge.NakedBridge.TopRight, bridge.BridgeBalance.Values(0))
        ElseIf bridge.BridgeBalance.Layout = BalanceLayout.RightShuntRightSeries Then
            expectedResistance = Resistor.ShuntConductor(bridge.NakedBridge.TopRight, bridge.BridgeBalance.Values(0)) + bridge.BridgeBalance.Values(1)
        End If
        Dim actualResistance As Double = bridge.TopRight
        Assert.AreEqual(expectedResistance, actualResistance, TestBridges.ElementEpsilon)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            expectedResistance = Resistor.ToSeries(bridge.NakedBridge.BottomRight, bridge.BridgeBalance.Values(1))
        ElseIf bridge.BridgeBalance.Layout = BalanceLayout.RightShuntRightSeries Then
            expectedResistance = Resistor.ShuntConductor(bridge.NakedBridge.BottomRight, bridge.BridgeBalance.Values(3)) + bridge.BridgeBalance.Values(2)
        End If
        actualResistance = bridge.BottomRight
        Assert.AreEqual(expectedResistance, actualResistance, TestBridges.ElementEpsilon)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            expectedResistance = Resistor.ShuntConductor(bridge.NakedBridge.TopLeft, bridge.BridgeBalance.Values(3))
        ElseIf bridge.BridgeBalance.Layout = BalanceLayout.RightShuntRightSeries Then
            expectedResistance = bridge.NakedBridge.TopLeft
        End If
        actualResistance = bridge.TopLeft
        Assert.AreEqual(expectedResistance, actualResistance, TestBridges.ElementEpsilon)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            expectedResistance = Resistor.ToSeries(bridge.NakedBridge.BottomLeft, bridge.BridgeBalance.Values(2))
        ElseIf bridge.BridgeBalance.Layout = BalanceLayout.RightShuntRightSeries Then
            expectedResistance = bridge.NakedBridge.BottomLeft
        End If
        actualResistance = bridge.BottomLeft
        Assert.AreEqual(expectedResistance, actualResistance, TestBridges.ElementEpsilon)

    End Sub

    ''' <summary> (Unit Test Method) tests build invalid bridge. </summary>
    <TestMethod()>
    Public Sub BuildInvalidBridgeTest()

        Dim sampleBridge As SampleBridge = TestBridges.LowTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        Dim actualValidity As Boolean = False
        Dim expectedValidity As Boolean = False
        Assert.AreEqual(expectedValidity, actualValidity)

        actualValidity = bridge.IsValid
        expectedValidity = True
        Assert.AreEqual(expectedValidity, actualValidity)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        Else
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        End If
        actualValidity = bridge.IsValid
        expectedValidity = True
        Assert.AreEqual(expectedValidity, actualValidity)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.ShortConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        Else
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.ShortConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        End If
        actualValidity = bridge.IsValid
        expectedValidity = False
        Assert.AreEqual(expectedValidity, actualValidity)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        Else
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        End If
        actualValidity = bridge.IsValid
        expectedValidity = True
        Assert.AreEqual(expectedValidity, actualValidity)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.OpenResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        Else
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.OpenResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        End If
        actualValidity = bridge.IsValid
        expectedValidity = False
        Assert.AreEqual(expectedValidity, actualValidity)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        Else
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        End If
        actualValidity = bridge.IsValid
        expectedValidity = True
        Assert.AreEqual(expectedValidity, actualValidity)


        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, Resistor.OpenResistance, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        Else
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, Resistor.OpenResistance, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        End If
        actualValidity = bridge.IsValid
        expectedValidity = False
        Assert.AreEqual(expectedValidity, actualValidity)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        Else
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        End If
        actualValidity = bridge.IsValid
        expectedValidity = True
        Assert.AreEqual(expectedValidity, actualValidity)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, Conductor.ShortConductance}, bridge.BridgeBalance.Layout)
        Else
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, Conductor.ShortConductance}, bridge.BridgeBalance.Layout)
        End If
        actualValidity = bridge.IsValid
        expectedValidity = False
        Assert.AreEqual(expectedValidity, actualValidity)

        If bridge.BridgeBalance.Layout = BalanceLayout.TopShuntBottomSeries Then
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        Else
            bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        End If
        actualValidity = bridge.IsValid
        expectedValidity = True
        Assert.AreEqual(expectedValidity, actualValidity)

    End Sub


    ''' <summary> (Unit Test Method) tests null bridge exception. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="bridge")>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentNullException), "Null bridge inappropriately allowed")>
    Public Sub NullBridgeExceptionTest()
        Dim wheatstone As Wheatstone = Nothing '  New Wheatstone(New Double() {369.59, 363.04, 357.69, 377.06}, WheatstoneLayout.Counterclockwise)
        ' try build the bridge using a null bridge; should issue the expected exception
        Dim bridge As BalanceBridge = New BalanceBridge(wheatstone,
                                                        New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357},
                                                        BalanceLayout.TopShuntBottomSeries)
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="bridge")>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentNullException), "Null bridge values inappropriately allowed")>
    Public Sub NullValuesExceptionTest()
        Dim wheatstone As Wheatstone = New Wheatstone(New Double() {369.59, 363.04, 357.69, 377.06}, WheatstoneLayout.Counterclockwise)
        ' try build the bridge using a null bridge; should issue the expected exception
        Dim bridge As BalanceBridge = New BalanceBridge(wheatstone,
                                                        Nothing,
                                                        BalanceLayout.TopShuntBottomSeries)
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="bridge")>
    <TestMethod()>
    <ExpectedException(GetType(InvalidOperationException), "Invalid balance values inappropriately allowed")>
    Public Sub InvalidValuesExceptionTest()
        Dim wheatstone As Wheatstone = New Wheatstone(New Double() {369.59, 363.04, 357.69, 377.06}, WheatstoneLayout.Counterclockwise)
        ' try build the bridge using a null bridge; should issue the expected exception
        Dim bridge As BalanceBridge = New BalanceBridge(wheatstone,
                                                        New Double() {Conductor.OpenConductance, 10.3},
                                                        BalanceLayout.TopShuntBottomSeries)
    End Sub

#End Region

#Region " OUTPUT TESTS "

    ''' <summary> (Unit Test Method) tests low temporary zero pressure output. </summary>
    <TestMethod()> Public Sub LowTempZeroPressureOutputTest()

        Dim sampleBridge As SampleBridge = TestBridges.LowTempZeroPressureBridge
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        Dim relativeOffset As Double = sampleBridge.OutputVoltage / sampleBridge.BridgeVoltage
        Dim expectedRelativeOffset As Double = bridge.NakedBridge.Output
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, bridge.OutputEpsilon)

        relativeOffset = bridge.Output
        expectedRelativeOffset = 0
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, sampleBridge.RelativeOffsetEpsilon)

        Dim expectedBalance As Boolean = Math.Abs(bridge.Output) < bridge.OutputEpsilon
        Dim actualBalance As Boolean = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance)

        Dim outputVoltage As Double = bridge.NakedBridge.Output(sampleBridge.BridgeVoltage)
        Dim expectedVoltage As Double = sampleBridge.OutputVoltage
        Assert.AreEqual(expectedVoltage, outputVoltage, TestBridges.OutputVoltageEpsilon)

    End Sub

    ''' <summary> (Unit Test Method) tests high temporary zero pressure output. </summary>
    <TestMethod()> Public Sub HighTempZeroPressureOutputTest()

        Dim sampleBridge As SampleBridge = TestBridges.HighTempZeroPressureBridge
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        Dim relativeOffset As Double = sampleBridge.OutputVoltage / sampleBridge.BridgeVoltage
        Dim expectedRelativeOffset As Double = bridge.NakedBridge.Output
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, bridge.OutputEpsilon)

        relativeOffset = bridge.Output
        expectedRelativeOffset = 0
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, sampleBridge.RelativeOffsetEpsilon)

        Dim expectedBalance As Boolean = Math.Abs(bridge.Output) < bridge.OutputEpsilon
        Dim actualBalance As Boolean = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance)

        Dim outputVoltage As Double = bridge.NakedBridge.Output(sampleBridge.BridgeVoltage)
        Dim expectedVoltage As Double = sampleBridge.OutputVoltage
        Assert.AreEqual(expectedVoltage, outputVoltage, TestBridges.OutputVoltageEpsilon)

    End Sub

    ''' <summary> (Unit Test Method) tests balance bridge change. </summary>
    <TestMethod()> Public Sub BalanceBridgeChangeTest()

        Dim sampleBridge As SampleBridge = TestBridges.HighTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        sampleBridge = Nothing
        sampleBridge = TestBridges.LowTempZeroPressureBridge
        bridge.NakedBridge = sampleBridge.Bridge

        Dim relativeOffset As Double = sampleBridge.OutputVoltage / sampleBridge.BridgeVoltage
        Dim expectedRelativeOffset As Double = bridge.NakedBridge.Output
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, bridge.OutputEpsilon)

        relativeOffset = bridge.Output
        expectedRelativeOffset = 0
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, sampleBridge.RelativeOffsetEpsilon)

        Dim expectedBalance As Boolean = Math.Abs(bridge.Output) < bridge.OutputEpsilon
        Dim actualBalance As Boolean = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance)

        Dim outputVoltage As Double = bridge.NakedBridge.Output(sampleBridge.BridgeVoltage)
        Dim expectedVoltage As Double = sampleBridge.OutputVoltage
        Assert.AreEqual(expectedVoltage, outputVoltage, TestBridges.OutputVoltageEpsilon)

    End Sub

    ''' <summary> (Unit Test Method) tests balance bridge values no change. </summary>
    <TestMethod()> Public Sub BalanceBridgeValuesNoChangeTest()

        Dim sampleBridge As SampleBridge = TestBridges.HighTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        Dim relativeOffset As Double = sampleBridge.OutputVoltage / sampleBridge.BridgeVoltage
        Dim expectedRelativeOffset As Double = bridge.NakedBridge.Output
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, bridge.OutputEpsilon)

        relativeOffset = bridge.Output
        expectedRelativeOffset = 0
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, sampleBridge.RelativeOffsetEpsilon)

        Dim expectedBalance As Boolean = Math.Abs(bridge.Output) < bridge.OutputEpsilon
        Dim actualBalance As Boolean = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance)

        Dim outputVoltage As Double = bridge.NakedBridge.Output(sampleBridge.BridgeVoltage)
        Dim expectedVoltage As Double = sampleBridge.OutputVoltage
        Assert.AreEqual(expectedVoltage, outputVoltage, TestBridges.OutputVoltageEpsilon)

    End Sub

    ''' <summary> (Unit Test Method) tests balance bridge values change. </summary>
    <TestMethod()> Public Sub BalanceBridgeValuesChangeTest()

        Dim sampleBridge As SampleBridge = TestBridges.HighTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        bridge.BridgeBalance = New BridgeBalance(New Double() {Conductor.OpenConductance, 11.5, Resistor.ShortResistance, 1 / 52357}, bridge.BridgeBalance.Layout)
        Dim relativeOffset As Double = sampleBridge.OutputVoltage / sampleBridge.BridgeVoltage
        Dim expectedRelativeOffset As Double = bridge.NakedBridge.Output
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, 0.001 * bridge.OutputEpsilon)

        relativeOffset = bridge.Output
        expectedRelativeOffset = 0
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, sampleBridge.RelativeOffsetEpsilon)

        Dim expectedBalance As Boolean = Math.Abs(bridge.Output) < bridge.OutputEpsilon
        Dim actualBalance As Boolean = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance)

        Dim outputVoltage As Double = bridge.NakedBridge.Output(sampleBridge.BridgeVoltage)
        Dim expectedVoltage As Double = sampleBridge.OutputVoltage
        Assert.AreEqual(expectedVoltage, outputVoltage, 0.001 * TestBridges.OutputVoltageEpsilon)

    End Sub

#End Region

#Region " COMPENSATION TESTS "

    ''' <summary> Validates the compensation. </summary>
    ''' <param name="sampleBridge"> The sample bridge. </param>
    ''' <param name="bridge">       The bridge. </param>
    Private Shared Sub ValidateCompensation(ByVal sampleBridge As SampleBridge, ByVal bridge As BalanceBridge)
        Dim relativeOffset As Double = 0
        Dim expectedRelativeOffset As Double = 0

        relativeOffset = bridge.Output
        expectedRelativeOffset = 0
        Assert.AreEqual(expectedRelativeOffset, relativeOffset, bridge.OutputEpsilon)

        Dim expectedBalance As Boolean = Math.Abs(bridge.Output) < bridge.OutputEpsilon
        Dim actualBalance As Boolean = bridge.IsOutputBalanced()
        Assert.AreEqual(expectedBalance, actualBalance)

        Dim outputVoltage As Double = bridge.Output(sampleBridge.BridgeVoltage)
        Dim expectedVoltage As Double = 0
        Assert.AreEqual(expectedVoltage, outputVoltage, TestBridges.OutputVoltageEpsilon)
    End Sub

    ''' <summary> (Unit Test Method) tests low temporary zero pressure shunt compensation. </summary>
    <TestMethod()> Public Sub LowTempZeroPressureShuntCompensationTest()

        Dim sampleBridge As SampleBridge = TestBridges.LowTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        ' apply shunt balance
        bridge.ApplyShuntCompensation()
        BalanceBridgeTests.ValidateCompensation(sampleBridge, bridge)

    End Sub

    ''' <summary> (Unit Test Method) tests low temporary zero pressure Series compensation. </summary>
    <TestMethod()> Public Sub LowTempZeroPressureSeriesCompensationTest()

        Dim sampleBridge As SampleBridge = TestBridges.LowTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        ' apply shunt balance
        bridge.ApplySeriesCompensation()
        BalanceBridgeTests.ValidateCompensation(sampleBridge, bridge)

    End Sub


    ''' <summary> (Unit Test Method) tests high temporary zero pressure shunt compensation. </summary>
    <TestMethod()> Public Sub HighTempZeroPressureShuntCompensationTest()

        Dim sampleBridge As SampleBridge = TestBridges.HighTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        ' apply shunt balance
        bridge.ApplyShuntCompensation()
        BalanceBridgeTests.ValidateCompensation(sampleBridge, bridge)

    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests high temporary zero pressure series compensation.
    ''' </summary>
    <TestMethod()> Public Sub HighTempZeroPressureSeriesCompensationTest()

        Dim sampleBridge As SampleBridge = TestBridges.HighTempZeroPressureBridge.Clone
        Dim bridge As BalanceBridge = sampleBridge.BalanceBridge

        ' apply Series balance
        bridge.ApplySeriesCompensation()
        BalanceBridgeTests.ValidateCompensation(sampleBridge, bridge)

    End Sub

#End Region

End Class

#Region " SAMPLE BRIDGE "

    ''' <summary> A sample bridge. </summary>
    ''' <license>
    ''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="9/23/2017" by="David" revision=""> Created. </history>
    Public Class SampleBridge

        ''' <summary> Constructor using counter clock wise notation. </summary>
        ''' <param name="elements">         The Wheatstone Bridge Elements. </param>
        ''' <param name="wheatstoneLayout"> The Wheatstone Bridge layout. </param>
        Public Sub New(ByVal elements As IEnumerable(Of Double), ByVal wheatstoneLayout As WheatstoneLayout)
            MyBase.New
            Me.Bridge = New Wheatstone(elements, wheatstoneLayout)
        End Sub

        ''' <summary> Validates the sample bridge described by value. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value"> The value. </param>
        ''' <returns> A SampleBridge. </returns>
        Private Shared Function ValidateSampleBridge(ByVal value As SampleBridge) As SampleBridge
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            Return value
        End Function

        ''' <summary> Cloning Constructor. </summary>
        ''' <param name="value"> The value. </param>
        Public Sub New(ByVal value As SampleBridge)
            MyBase.New
            Me.Bridge = New Wheatstone(SampleBridge.ValidateSampleBridge(value).Bridge)
            Me.BalanceBridge = New BalanceBridge(value.BalanceBridge)
            Me.SupplyVoltage = value.SupplyVoltage
            Me.SupplyVoltageDrop = value.SupplyVoltage
            Me.SupplyVoltageDrop = value.SupplyVoltageDrop
            Me.OutputVoltage = value.OutputVoltage
            Me.WheatstoneLayout = value.WheatstoneLayout
            Me.EquivalentResistance = value.EquivalentResistance
            Me.RelativeOffsetEpsilon = value.RelativeOffsetEpsilon

            Me.NominalVoltage = value.NominalVoltage
            Me.VoltageSourceSeriesResistance = value.VoltageSourceSeriesResistance
            Me.VoltageSourceParallelResistance = value.VoltageSourceParallelResistance
            Me.NominalFullScaleVoltage = value.NominalFullScaleVoltage
            Me.NominalCurrent = value.NominalCurrent
            Me.CurrentSourceSeriesResistance = value.CurrentSourceSeriesResistance
            Me.CurrentSourceParallelResistance = value.CurrentSourceParallelResistance
        End Sub

        ''' <summary> Makes a deep copy of this object. </summary>
        ''' <returns> A copy of this object. </returns>
        Public Function Clone() As SampleBridge
            Return New SampleBridge(Me)
        End Function


        Public Property Bridge As Wheatstone
        Public Property SupplyVoltage As Double = 5.02058
        Public Property SupplyVoltageDrop As Double = 3.67312
        Public ReadOnly Property BridgeVoltage As Double
            Get
                Return Me.SupplyVoltage - Me.SupplyVoltageDrop
            End Get
        End Property
        Public Property OutputVoltage As Double = 0.011728

        Public Property WheatstoneLayout As WheatstoneLayout = WheatstoneLayout.Clockwise

        Public Property EquivalentResistance As Double = 782
        Public Property RelativeOffsetEpsilon As Double = 0.005

        Public Property NominalVoltage As Double = 5
        Public Property VoltageSourceSeriesResistance As Double = 1538
        Public Property VoltageSourceParallelResistance As Double = 1593
        Public Property NominalFullScaleVoltage As Double = 0.02

        Public Property NominalCurrent As Double = 0.004
        ' 636 this is calculated based on 5v, 0.004 v and the above resistances, original value is 632
        Public Property CurrentSourceSeriesResistance As Double = 636
        Public Property CurrentSourceParallelResistance As Double = 150

        Public Property BalanceBridge As BalanceBridge

    End Class

#End Region

End Namespace

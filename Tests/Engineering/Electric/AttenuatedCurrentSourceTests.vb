﻿Imports isr.Core.Engineering
Namespace Engineering.Tests

    ''' <summary> Unit tests for the attenuated current source. </summary>
    ''' <license>
    ''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="12/12/2017" by="David" revision=""> Created. </history>
    <TestClass()> Public Class AttenuatedCurrentSourceTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            Assert.IsTrue(CurrentSourceTestInfo.Get.Exists, $"{GetType(CurrentSourceTestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()


            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

#Region " CONSTRUCTION TESTS "

        <TestMethod()>
        Public Sub BuildTest()

            Dim attenuation As Double = AttenuatedCurrentSource.ToAttenuation(CurrentSourceTestInfo.Get.SeriesResistance, CurrentSourceTestInfo.Get.ParallelConductance)
            Dim equivalenctConductance As Double = Conductor.SeriesResistor(CurrentSourceTestInfo.Get.ParallelConductance, CurrentSourceTestInfo.Get.SeriesResistance)

            ' construct a current source 
            Dim currentSource As CurrentSource = New CurrentSource(CurrentSourceTestInfo.Get.NominalCurrent, equivalenctConductance)

            Dim attenuatedSource As AttenuatedCurrentSource = Nothing
            Dim doubleEpsilon As Double = 0.0000000001
            Dim actualValue As Double = 1
            Dim expectedValue As Double = 1
            Dim message As String = ""
            Dim outcome As String = ""
            Dim item As String = ""
            Dim attenuatedSourcetarget As String = ""
            Dim sourceTarget As String = ""
            Dim target As String = ""

            For i As Integer = 1 To 3
                Select Case i
                    Case 1
                        attenuatedSourcetarget = "nominal attenuated current source"
                        attenuatedSource = New AttenuatedCurrentSource(CDec(CurrentSourceTestInfo.Get.NominalCurrent),
                                                                   CurrentSourceTestInfo.Get.SeriesResistance, CurrentSourceTestInfo.Get.ParallelConductance)
                        sourceTarget = "current source from nominal"
                    Case 2
                        attenuatedSourcetarget = "equivalent attenuated current source"
                        attenuatedSource = New AttenuatedCurrentSource(CDbl(CurrentSourceTestInfo.Get.NominalCurrent / attenuation),
                                                                   CurrentSourceTestInfo.Get.SeriesResistance, CurrentSourceTestInfo.Get.ParallelConductance)
                        sourceTarget = "current source from equivalent"
                    Case 3
                        attenuatedSourcetarget = "converted attenuated current source"
                        currentSource = New CurrentSource(CurrentSourceTestInfo.Get.NominalCurrent / attenuation, equivalenctConductance)
                        attenuatedSource = CurrentSource.ToAttenuatedCurrentSource(currentSource, attenuation)
                        sourceTarget = "current source from converted"
                End Select
                currentSource = attenuatedSource.ToCurrentSource

                ' test the attenuated voltage source
                target = attenuatedSourcetarget
                actualValue = attenuatedSource.NominalCurrent
                expectedValue = CurrentSourceTestInfo.Get.NominalCurrent
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                item = "nominal current"
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

                target = attenuatedSourcetarget
                actualValue = attenuatedSource.Current
                expectedValue = CurrentSourceTestInfo.Get.NominalCurrent / attenuation
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                item = "short load current"
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

                target = sourceTarget
                actualValue = currentSource.Current
                expectedValue = CurrentSourceTestInfo.Get.NominalCurrent / attenuation
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                item = "short load current"
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

                target = attenuatedSourcetarget
                actualValue = attenuatedSource.Conductance
                expectedValue = equivalenctConductance
                item = "equivalent conductance"
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

                target = sourceTarget
                actualValue = attenuatedSource.Conductance
                expectedValue = equivalenctConductance
                item = "equivalent conductance"
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

                target = attenuatedSourcetarget
                actualValue = attenuatedSource.SeriesResistance
                expectedValue = CurrentSourceTestInfo.Get.SeriesResistance
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                item = "series resistance"
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

                target = attenuatedSourcetarget
                actualValue = attenuatedSource.Attenuation
                expectedValue = attenuation
                item = "attenuation"
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

                target = attenuatedSourcetarget
                actualValue = attenuatedSource.ParallelConductance
                expectedValue = CurrentSourceTestInfo.Get.ParallelConductance
                item = "parallel conductance"
                outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
                Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
                TestInfo.VerboseMessage(message)

            Next

        End Sub

#End Region

#Region " EXCEPTION CONVERSION TESTS "

        ''' <summary>
        ''' (Unit Test Method) tests exception for converting a current source to an attenuated current source with invalid attenuation.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="source")>
        <TestMethod()>
        <ExpectedException(GetType(ArgumentOutOfRangeException), "Attenuation smaller than 1 inappropriately allowed")>
        Public Sub ConvertAttenuationExceptionTest()
            Dim attenuation As Double = AttenuatedVoltageSource.MinimumAttenuation - 0.01
            Dim currentSource As CurrentSource = New CurrentSource(CurrentSourceTestInfo.Get.NominalCurrent, CurrentSourceTestInfo.Get.SourceConductance)
            Dim source As AttenuatedCurrentSource = CurrentSource.ToAttenuatedCurrentSource(currentSource, attenuation)

        End Sub

        ''' <summary>
        ''' (Unit Test Method) tests exception for converting to an attenuated source using a null source.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="source")>
        <TestMethod()>
        <ExpectedException(GetType(ArgumentNullException), "Conversion with null source allowed")>
        Public Sub ConvertNothingExceptionTest()
            Dim attenuation As Double = AttenuatedVoltageSource.MinimumAttenuation + 0.01
            Dim currentSource As CurrentSource = Nothing
            Dim source As AttenuatedCurrentSource = CurrentSource.ToAttenuatedCurrentSource(currentSource, attenuation)
        End Sub

#End Region

#Region " LOAD AND PROPERTY CHANGE TESTS "


        ''' <summary> (Unit Test Method) tests loading an attenuated current source. </summary>
        <TestMethod()>
        Public Sub LoadTest()

            Dim source As AttenuatedCurrentSource = New AttenuatedCurrentSource(CDec(CurrentSourceTestInfo.Get.NominalCurrent),
                                                                                CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                CurrentSourceTestInfo.Get.ParallelConductance)

            Dim doubleEpsilon As Double = 0.0000000001
            Dim actualValue As Double = 1
            Dim expectedValue As Double = 1
            Dim message As String = ""
            Dim outcome As String = ""
            Dim item As String = ""
            Dim target As String = ""

            ' test the attenuated voltage source
            target = "attenuated current source"

            ' test open current
            item = "open load current"
            actualValue = source.LoadCurrent(Conductor.OpenConductance)
            expectedValue = 0
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            item = "short load current"
            actualValue = source.LoadCurrent(Conductor.ShortConductance)
            expectedValue = CDec(CurrentSourceTestInfo.Get.NominalCurrent) / source.Attenuation
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' test short voltage
            item = "short voltage"
            actualValue = source.LoadVoltage(0)
            expectedValue = 0
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' test load current
            item = "load current"
            actualValue = source.LoadCurrent(CurrentSourceTestInfo.Get.LoadConductance)
            expectedValue = CurrentSourceTestInfo.Get.NominalCurrent * Conductor.OutputCurrent(CurrentSourceTestInfo.Get.ParallelConductance,
                                                                        Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance,
                                                                                                                 CurrentSourceTestInfo.Get.SeriesResistance)))
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' test load voltage
            item = "load voltage"
            actualValue = source.LoadVoltage(CurrentSourceTestInfo.Get.LoadResistance)
            expectedValue = CurrentSourceTestInfo.Get.NominalCurrent * CurrentSourceTestInfo.Get.LoadResistance *
                                Conductor.OutputCurrent(CurrentSourceTestInfo.Get.ParallelConductance,
                                                        Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance,
                                                                                                 CurrentSourceTestInfo.Get.SeriesResistance)))
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

        End Sub

        ''' <summary> (Unit Test Method) tests changing properties of an attenuated voltage source. </summary>
        <TestMethod()>
        Public Sub PropertiesChangeTest()

            ' test changing Attenuated voltage source properties
            Dim source As AttenuatedCurrentSource = New AttenuatedCurrentSource(CDec(CurrentSourceTestInfo.Get.NominalCurrent),
                                                                                CurrentSourceTestInfo.Get.SeriesResistance,
                                                                                CurrentSourceTestInfo.Get.ParallelConductance)
            Dim doubleEpsilon As Double = 0.0000000001
            Dim actualValue As Double = 1
            Dim expectedValue As Double = 1
            Dim message As String = ""
            Dim outcome As String = ""
            Dim item As String = ""
            Dim target As String = ""

            ' test the attenuated voltage source
            target = "attenuated current source"

            Dim currentGain As Double = 1.5

            ' test shot load current
            item = $"short load current {currentGain}={NameOf(currentGain)}"
            source.NominalCurrent *= currentGain
            actualValue = source.LoadCurrent(Conductor.ShortConductance)
            expectedValue = currentGain * CurrentSourceTestInfo.Get.NominalCurrent / source.Attenuation
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' test open current
            item = $"open current {currentGain}={NameOf(currentGain)}"
            actualValue = source.LoadCurrent(Conductor.OpenConductance)
            expectedValue = 0
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' test load current
            item = $"load current {currentGain}={NameOf(currentGain)}"
            actualValue = source.LoadCurrent(CurrentSourceTestInfo.Get.LoadConductance)
            expectedValue = currentGain * CurrentSourceTestInfo.Get.NominalCurrent * Conductor.OutputCurrent(CurrentSourceTestInfo.Get.ParallelConductance,
                                                                        Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance,
                                                                                                                 CurrentSourceTestInfo.Get.SeriesResistance)))
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            source.NominalCurrent /= currentGain
            Dim seriesResistanceGain As Double = 1.2
            source.SeriesResistance *= seriesResistanceGain

            item = $"short load current {seriesResistanceGain}={NameOf(seriesResistanceGain)}"
            actualValue = source.LoadCurrent(Conductor.ShortConductance)
            expectedValue = CurrentSourceTestInfo.Get.NominalCurrent / source.Attenuation
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' test short current
            item = $"open load current {seriesResistanceGain}={NameOf(seriesResistanceGain)}"
            actualValue = source.LoadCurrent(Conductor.OpenConductance)
            expectedValue = 0
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' test load current
            item = $"load current{seriesResistanceGain}={NameOf(seriesResistanceGain)}"
            actualValue = source.LoadCurrent(CurrentSourceTestInfo.Get.LoadConductance)
            expectedValue = CurrentSourceTestInfo.Get.NominalCurrent * Conductor.OutputCurrent(CurrentSourceTestInfo.Get.ParallelConductance,
                                                                       Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance,
                                                                                                                seriesResistanceGain * CurrentSourceTestInfo.Get.SeriesResistance)))
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' test load voltage
            item = $"load voltage {seriesResistanceGain}={NameOf(seriesResistanceGain)}"
            actualValue = source.LoadVoltage(CurrentSourceTestInfo.Get.LoadResistance)
            expectedValue = CurrentSourceTestInfo.Get.LoadResistance * CurrentSourceTestInfo.Get.NominalCurrent *
                        Conductor.OutputCurrent(CurrentSourceTestInfo.Get.ParallelConductance,
                                                                       Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance,
                                                                                                                seriesResistanceGain * CurrentSourceTestInfo.Get.SeriesResistance)))
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            Dim parallelConductanceGain As Double = 1.2
            source.SeriesResistance /= seriesResistanceGain
            source.ParallelConductance *= parallelConductanceGain

            ' test short current
            item = $"Short load current {parallelConductanceGain}={NameOf(parallelConductanceGain)}"
            actualValue = source.LoadCurrent(Conductor.ShortConductance)
            expectedValue = CurrentSourceTestInfo.Get.NominalCurrent / source.Attenuation
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' test open current
            item = $"open load current {parallelConductanceGain}={NameOf(parallelConductanceGain)}"
            actualValue = source.LoadCurrent(Conductor.OpenConductance)
            expectedValue = 0
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' test load voltage
            item = $"load voltage {parallelConductanceGain}={NameOf(parallelConductanceGain)}"
            actualValue = source.LoadVoltage(CurrentSourceTestInfo.Get.LoadResistance)
            expectedValue = CurrentSourceTestInfo.Get.LoadResistance * CurrentSourceTestInfo.Get.NominalCurrent *
                        Conductor.OutputCurrent(parallelConductanceGain * CurrentSourceTestInfo.Get.ParallelConductance,
                                                                       Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance,
                                                                                                                CurrentSourceTestInfo.Get.SeriesResistance)))
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' test load current
            item = $"load current {parallelConductanceGain}={NameOf(parallelConductanceGain)}"
            actualValue = source.LoadCurrent(CurrentSourceTestInfo.Get.LoadConductance)
            expectedValue = CurrentSourceTestInfo.Get.NominalCurrent *
                        Conductor.OutputCurrent(parallelConductanceGain * CurrentSourceTestInfo.Get.ParallelConductance,
                                                                       Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance,
                                                                                                                CurrentSourceTestInfo.Get.SeriesResistance)))
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            Dim attenuationChange As Double = 1.2
            source.ParallelConductance /= parallelConductanceGain
            Dim newAttenuation As Double = source.Attenuation * attenuationChange
            Dim parallelR As Double = Conductor.ToResistance(source.Conductance * newAttenuation)
            Dim seriesR As Double = 1 / source.Conductance - parallelR
            Dim newNominal As Double = newAttenuation * CurrentSourceTestInfo.Get.NominalCurrent / source.Attenuation
            source.Attenuation *= attenuationChange

            ' test short current
            item = $"short load current {attenuationChange}={NameOf(attenuationChange)}"
            actualValue = source.LoadCurrent(Conductor.ShortConductance)
            expectedValue = newNominal / source.Attenuation
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' test open load current
            item = $"open load current {attenuationChange}={NameOf(attenuationChange)}"
            actualValue = source.LoadCurrent(Conductor.OpenConductance)
            expectedValue = 0
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' test load voltage
            item = $"load voltage {attenuationChange}={NameOf(attenuationChange)}"
            actualValue = source.LoadVoltage(CurrentSourceTestInfo.Get.LoadResistance)
            expectedValue = CurrentSourceTestInfo.Get.LoadResistance * newNominal * Conductor.OutputCurrent(Resistor.ToConductance(parallelR),
                                                                                                             Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance, seriesR)))
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

            ' test load current
            item = $"load current {attenuationChange}={NameOf(attenuationChange)}"
            actualValue = source.LoadCurrent(CurrentSourceTestInfo.Get.LoadConductance)
            expectedValue = newNominal * Conductor.OutputCurrent(Resistor.ToConductance(parallelR), Resistor.ToConductance(Resistor.ToSeries(CurrentSourceTestInfo.Get.LoadResistance, seriesR)))
            outcome = If(Math.Abs(expectedValue - actualValue) < doubleEpsilon, "=", "!=")
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}"
            Assert.AreEqual(expectedValue, actualValue, doubleEpsilon, message)
            TestInfo.VerboseMessage(message)

        End Sub

#End Region

    End Class

End Namespace

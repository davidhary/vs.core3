﻿Imports isr.Core.Engineering
Namespace Engineering.Tests

    ''' <summary>
    ''' Provides bridges for testing.
    ''' </summary>
    Friend NotInheritable Class TestBridges

        Private Sub New()
            MyBase.New
        End Sub

        Public Shared Property ElementEpsilon As Double = 0.000001
        Public Shared Property OutputVoltageEpsilon As Double = 0.0001
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Shared Property FullScaleVoltageEpsilon As Double = 0.0001
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Shared Property SourceResistanceTolerance As Double = 0.001

        Private Shared _LowTempZeroPressureBridge As SampleBridge
        Public Shared ReadOnly Property LowTempZeroPressureBridge As SampleBridge
            Get
                If _LowTempZeroPressureBridge Is Nothing Then
                    _LowTempZeroPressureBridge = New SampleBridge(New Double() {369.59, 363.04, 357.69, 377.06}, WheatstoneLayout.Counterclockwise)
                    With _LowTempZeroPressureBridge
                        .SupplyVoltage = 5.02058
                        .SupplyVoltageDrop = 3.67312
                        .OutputVoltage = 0.011728

                        .BalanceBridge = New BalanceBridge(.Bridge, New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, BalanceLayout.TopShuntBottomSeries)
                        .EquivalentResistance = 782
                        .RelativeOffsetEpsilon = 0.005

                        .NominalVoltage = 5
                        .VoltageSourceSeriesResistance = 1538
                        .VoltageSourceParallelResistance = 1593
                        .NominalFullScaleVoltage = 0.02
                        .NominalCurrent = 0.004
                        ' 636 this is calculated based on 5v, 0.004 v and the above resistances
                        .CurrentSourceSeriesResistance = 636
                        .CurrentSourceParallelResistance = 150
                    End With
                End If
                Return _LowTempZeroPressureBridge
            End Get
        End Property

        Private Shared _LowTempFullScalePressureBridge As SampleBridge
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Shared ReadOnly Property LowTempFullScalePressureBridge As SampleBridge
            Get
                If _LowTempFullScalePressureBridge Is Nothing Then
                    _LowTempFullScalePressureBridge = New SampleBridge(New Double() {381.78, 356.88, 369.62, 370.89}, WheatstoneLayout.Counterclockwise)
                    With _LowTempFullScalePressureBridge
                        .SupplyVoltage = 5.02075
                        .SupplyVoltageDrop = 3.66533
                        .OutputVoltage = 0.02001

                        .BalanceBridge = New BalanceBridge(.Bridge, New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, BalanceLayout.TopShuntBottomSeries)
                        .EquivalentResistance = 782
                        .RelativeOffsetEpsilon = 0.005

                        .NominalVoltage = 5
                        .VoltageSourceSeriesResistance = 1538
                        .VoltageSourceParallelResistance = 1593
                        .NominalFullScaleVoltage = 0.02
                        .NominalCurrent = 0.004
                        ' 636 this is calculated based on 5v, 0.004 v and the above resistances
                        .CurrentSourceSeriesResistance = 636
                        .CurrentSourceParallelResistance = 150

                    End With
                End If
                Return _LowTempFullScalePressureBridge
            End Get
        End Property
        Private Shared _HighTempZeroPressureBridge As SampleBridge
        Public Shared ReadOnly Property HighTempZeroPressureBridge As SampleBridge
            Get
                If _HighTempZeroPressureBridge Is Nothing Then
                    _HighTempZeroPressureBridge = New SampleBridge(New Double() {563.79, 563.19, 556.32, 573.16}, WheatstoneLayout.Counterclockwise)
                    With _HighTempZeroPressureBridge
                        .SupplyVoltage = 5.02155
                        .SupplyVoltageDrop = 3.21047
                        .OutputVoltage = 0.013019 ' possibly a transcription error: 0.011728

                        .BalanceBridge = New BalanceBridge(.Bridge, New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, BalanceLayout.TopShuntBottomSeries)
                        .EquivalentResistance = 782
                        .RelativeOffsetEpsilon = 0.006

                        .NominalVoltage = 5
                        .VoltageSourceSeriesResistance = 1538
                        .VoltageSourceParallelResistance = 1593
                        .NominalFullScaleVoltage = 0.02
                        .NominalCurrent = 0.004
                        ' 636 this is calculated based on 5v, 0.004 v and the above resistances
                        .CurrentSourceSeriesResistance = 636
                        .CurrentSourceParallelResistance = 150
                    End With
                End If
                Return _HighTempZeroPressureBridge
            End Get
        End Property

        Private Shared _HighTempFullScalePressureBridge As SampleBridge
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Shared ReadOnly Property HighTempFullScalePressureBridge As SampleBridge
            Get
                If _HighTempFullScalePressureBridge Is Nothing Then
                    _HighTempFullScalePressureBridge = New SampleBridge(New Double() {577.21, 555.43, 569.81, 565.23}, WheatstoneLayout.Counterclockwise)
                    With _HighTempFullScalePressureBridge
                        .SupplyVoltage = 5.02152
                        .SupplyVoltageDrop = 3.20471
                        .OutputVoltage = 0.02001

                        .BalanceBridge = New BalanceBridge(.Bridge, New Double() {Conductor.OpenConductance, 10.3, Resistor.ShortResistance, 1 / 52357}, BalanceLayout.TopShuntBottomSeries)
                        .EquivalentResistance = 782
                        .RelativeOffsetEpsilon = 0.005

                        .NominalVoltage = 5
                        .VoltageSourceSeriesResistance = 1538
                        .VoltageSourceParallelResistance = 1593
                        .NominalFullScaleVoltage = 0.02
                        .NominalCurrent = 0.004
                        ' 636 this is calculated based on 5v, 0.004 v and the above resistances
                        .CurrentSourceSeriesResistance = 636
                        .CurrentSourceParallelResistance = 150
                    End With
                End If
                Return _HighTempFullScalePressureBridge
            End Get
        End Property

    End Class
End Namespace
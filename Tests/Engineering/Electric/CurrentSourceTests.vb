﻿Imports isr.Core.Engineering
Namespace Engineering.Tests

    ''' <summary> A current source unit tests. </summary>
    ''' <license>
    ''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="9/18/2017" by="David" revision="1.0.0.0"> Created. </history>
    <TestClass()> Public Class CurrentSourceTests

#Region " SHARED CONSTANTS "

    ''' <summary> Gets or sets the current source current. </summary>
    ''' <value> The current. </value>
    Public Shared Property Current As Double = 0.004

    ''' <summary> Gets or sets the conductance of the current source. </summary>
    ''' <value> The conductance. </value>
    Public Shared Property Conductance As Double = 0.001

    ''' <summary> Gets or sets the load resistance. </summary>
    ''' <value> The load resistance. </value>
    Public Shared Property LoadResistance As Double = 500

    ''' <summary> Gets or sets the voltage of the voltage source. </summary>
    ''' <value> The voltage. </value>
    Public Shared Property Voltage As Double = 5

    ''' <summary> Gets or sets the resistance of the voltage source. </summary>
    ''' <value> The resistance. </value>
    Public Shared Property Resistance As Double = 1000

    ''' <summary> Gets or sets the short resistance. </summary>
    ''' <value> The short resistance. </value>
    Public Shared Property ShortResistance As Double = 0

    ''' <summary> Gets or sets the open resistance. </summary>
    ''' <value> The short resistance. </value>
    Public Shared Property OpenResistance As Double = Double.PositiveInfinity

    ''' <summary> Gets or sets the short conductance. </summary>
    ''' <value> The short conductance. </value>
    Public Shared Property ShortConductance As Double = Double.PositiveInfinity

    ''' <summary> Gets or sets the open conductance. </summary>
    ''' <value> The open conductance. </value>
    Public Shared Property OpenConductance As Double = 0

    ''' <summary> The current change scale. </summary>
    Public Shared Property CurrentChangeScale As Double = 1.5

    ''' <summary> The conductance change scale. </summary>
    Public Shared Property ConductanceChangeScale As Double = 0.8

    ''' <summary> Gets or sets the current to voltage level epsilon. </summary>
    ''' <value> The current to voltage level epsilon. </value>
    Public Shared Property CurrentToVoltageLevelEpsilon As Double = 0.000000001

    ''' <summary> Gets or sets the current to voltage resistance epsilon. </summary>
    ''' <value> The current to voltage resistance epsilon. </value>
    Public Shared Property CurrentToVoltageResistanceEpsilon As Double = 0.000000001

    ''' <summary> Gets or sets the current to voltage current level epsilon. </summary>
    ''' <value> The current to voltage current level epsilon. </value>
    Public Shared Property CurrentToVoltageCurrentLevelEpsilon As Double = 0.000000001

    ''' <summary> Gets or sets the current to voltage conductance epsilon. </summary>
    ''' <value> The current to voltage conductance epsilon. </value>
    Public Shared Property CurrentToVoltageConductanceEpsilon As Double = 0.000000001

#End Region

#Region " CONSTUCTOR TESTS "

    ''' <summary> (Unit Test Method) tests build current source. </summary>
    <TestMethod()> Public Sub BuildCurrentSourceTest()

        Dim source As CurrentSource = New CurrentSource(CurrentSourceTests.Current, CurrentSourceTests.Conductance)
        Dim actualValue As Double = 1
        Dim expectedValue As Double = 1

        ' test source Current
        actualValue = source.Current
        expectedValue = CurrentSourceTests.Current
        Assert.AreEqual(expectedValue, actualValue)

        ' test source conductance
        actualValue = source.Conductance
        expectedValue = CurrentSourceTests.Conductance
        Assert.AreEqual(expectedValue, actualValue)

    End Sub


    ''' <summary> (Unit Test Method) tests current to voltage source. </summary>
    <TestMethod()> Public Sub CurrentToVoltageSourceTest()

        ' create current source
        Dim currentSource As CurrentSource = New CurrentSource(VoltageSourceTests.Current, VoltageSourceTests.Conductance)

        ' convert to voltage source
        Dim source As VoltageSource = currentSource.ToVoltageSource
        Dim actualValue As Double = 1
        Dim expectedValue As Double = 1

        ' test source voltage
        actualValue = source.Voltage
        expectedValue = currentSource.Current / currentSource.Conductance
        Assert.AreEqual(expectedValue, actualValue, CurrentSourceTests.CurrentToVoltageLevelEpsilon)

        ' test source resistance
        actualValue = source.Resistance
        expectedValue = 1 / currentSource.Conductance
        Assert.AreEqual(expectedValue, actualValue, CurrentSourceTests.CurrentToVoltageResistanceEpsilon)

        Dim finalCurrentSource As CurrentSource = source.ToCurrentSource
        Assert.AreEqual(currentSource.Current, finalCurrentSource.Current, CurrentSourceTests.CurrentToVoltageCurrentLevelEpsilon)
        Assert.AreEqual(currentSource.Conductance, finalCurrentSource.Conductance, CurrentSourceTests.CurrentToVoltageConductanceEpsilon)

    End Sub


    ''' <summary> (Unit Test Method) tests null voltage source exception. </summary>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentNullException), "Null voltage source inappropriately allowed")>
    Public Sub NullVoltageSourceExceptionTest()

        ' null voltage source
        Dim voltageSource As VoltageSource = Nothing

        ' convert to current source
        Dim currentSource As CurrentSource = New CurrentSource(CurrentSourceTests.Current, CurrentSourceTests.Conductance)
        currentSource.FromVoltageSource(voltageSource)

    End Sub

#End Region

#Region " CURRENT TESTS "

    ''' <summary> (Unit Test Method) tests current source. </summary>
    <TestMethod()> Public Sub CurrentSourceTest()

        Dim source As CurrentSource = New CurrentSource(CurrentSourceTests.Current, CurrentSourceTests.Conductance)
        Dim actualValue As Double = 1
        Dim expectedValue As Double = 1

        ' test load current
        actualValue = source.LoadCurrent(1 / CurrentSourceTests.LoadResistance)
        expectedValue = CurrentSourceTests.Current * Resistor.Parallel(1 / CurrentSourceTests.Conductance,
                                                                           CurrentSourceTests.LoadResistance) /
                                                                           CurrentSourceTests.LoadResistance
        Assert.AreEqual(expectedValue, actualValue)


        ' test open voltage
        actualValue = source.LoadVoltage(CurrentSourceTests.OpenResistance)
        expectedValue = CurrentSourceTests.Current / CurrentSourceTests.Conductance
        Assert.AreEqual(expectedValue, actualValue)

        ' test short voltage
        actualValue = source.LoadVoltage(CurrentSourceTests.ShortResistance)
        expectedValue = 0
        Assert.AreEqual(expectedValue, actualValue)

        ' test load voltage
        actualValue = source.LoadVoltage(CurrentSourceTests.LoadResistance)
        expectedValue = CurrentSourceTests.Current * Resistor.Parallel(1 / CurrentSourceTests.Conductance,
                                                                                         CurrentSourceTests.LoadResistance)
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    ''' <summary> (Unit Test Method) tests current source change. </summary>
    <TestMethod()> Public Sub CurrentSourceChangeTest()

        Dim source As CurrentSource = New CurrentSource(CurrentSourceTests.Current, CurrentSourceTests.Conductance)
        Dim actualValue As Double = 1
        Dim expectedValue As Double = 1

        source.Current *= CurrentChangeScale
        ' test open voltage
        actualValue = source.LoadVoltage(CurrentSourceTests.OpenResistance)
        expectedValue = CurrentChangeScale * CurrentSourceTests.Current / CurrentSourceTests.Conductance
        Assert.AreEqual(expectedValue, actualValue)

        ' test short voltage
        actualValue = source.LoadVoltage(CurrentSourceTests.ShortResistance)
        expectedValue = 0
        Assert.AreEqual(expectedValue, actualValue)

        ' test load voltage
        actualValue = source.LoadVoltage(CurrentSourceTests.LoadResistance)
        expectedValue = CurrentChangeScale * CurrentSourceTests.Current *
                                        Resistor.Parallel(1 / CurrentSourceTests.Conductance, CurrentSourceTests.LoadResistance)
        Assert.AreEqual(expectedValue, actualValue)

        ' test load current
        actualValue = source.LoadCurrent(1 / CurrentSourceTests.LoadResistance)
        expectedValue = CurrentChangeScale * CurrentSourceTests.Current *
                                        Resistor.Parallel(1 / CurrentSourceTests.Conductance,
                                                          CurrentSourceTests.LoadResistance) / CurrentSourceTests.LoadResistance
        Assert.AreEqual(expectedValue, actualValue)

        source.Current /= CurrentChangeScale
        source.Conductance *= ConductanceChangeScale

        ' test open voltage
        actualValue = source.LoadVoltage(Double.PositiveInfinity)
        expectedValue = CurrentSourceTests.Current / (ConductanceChangeScale * CurrentSourceTests.Conductance)
        Assert.AreEqual(expectedValue, actualValue)

        ' test short voltage
        actualValue = source.LoadVoltage(CurrentSourceTests.ShortResistance)
        expectedValue = 0
        Assert.AreEqual(expectedValue, actualValue)

        ' test load voltage
        actualValue = source.LoadVoltage(CurrentSourceTests.LoadResistance)
        expectedValue = CurrentSourceTests.Current * Resistor.Parallel(1 / (ConductanceChangeScale * CurrentSourceTests.Conductance),
                                                                                         CurrentSourceTests.LoadResistance)
        Assert.AreEqual(expectedValue, actualValue)

        ' test load current
        actualValue = source.LoadCurrent(1 / CurrentSourceTests.LoadResistance)
        expectedValue = CurrentSourceTests.Current * Resistor.Parallel(1 / (ConductanceChangeScale * CurrentSourceTests.Conductance),
                                                                                         CurrentSourceTests.LoadResistance) /
                                                                                         CurrentSourceTests.LoadResistance
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

#End Region

End Class
End Namespace
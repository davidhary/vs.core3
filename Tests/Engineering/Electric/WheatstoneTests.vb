﻿Imports isr.Core.Engineering
Namespace Engineering.Tests
    ''' <summary> ATests for a Wheatstone bridge. </summary>
    ''' <license>
    ''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="9/15/2017" by="David" revision="1.0.0.0"> Created. </history>
    <TestClass()> Public Class WheatstoneTests

#Region " ADDITIONAL TEST ATTRIBUTES "
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

#Region " TEST INPUTS "

    ''' <summary> Gets or sets the bridge resistance. </summary>
    ''' <value> The bridge resistance. </value>
    Public Shared Property BridgeResistance As Double = 1000

    ''' <summary> Gets or sets the relative offset. </summary>
    ''' <value> The relative offset. </value>
    Public Shared Property RelativeOffset As Double = 0.002

    ''' <summary> Gets or sets the zero bridge voltage. </summary>
    ''' <value> The zero bridge voltage. </value>
    Public Shared Property ZeroBridgeVoltage As Double = 0

    ''' <summary> Gets or sets the low bridge voltage. </summary>
    ''' <value> The low bridge voltage. </value>
    Public Shared Property LowBridgeVoltage As Double = 1

    ''' <summary> Gets or sets the medium bridge voltage. </summary>
    ''' <value> The medium bridge voltage. </value>
    Public Shared Property MediumBridgeVoltage As Double = 10

    ''' <summary> Gets or sets the high bridge voltage. </summary>
    ''' <value> The high bridge voltage. </value>
    Public Shared Property HighBridgeVoltage As Double = 24

    ''' <summary> Gets or sets the relative offset epsilon scale. </summary>
    ''' <value> The relative offset epsilon scale. </value>
    Public Shared Property RelativeOffsetEpsilonScale As Double = 0.006

    ''' <summary> Gets or sets the relative offset epsilon. </summary>
    ''' <value> The relative offset epsilon. </value>
    Public Shared Property RelativeOffsetEpsilon As Double = 0.000000000001

#End Region

#Region " CONSTUCTOR TESTS "

    ''' <summary> (Unit Test Method) tests build of a balanced bridge. </summary>
    <TestMethod()>
    Public Sub BuildBalancedBridgeTest()

        ' build a balanced bridge
        Dim bridge As Wheatstone = New Wheatstone(WheatstoneTests.BridgeResistance)

        ' test value of all bridge elements
        Dim actualValue As Double = bridge.TopRight
        Dim expectedValue As Double = WheatstoneTests.BridgeResistance
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = bridge.BottomRight
        expectedValue = WheatstoneTests.BridgeResistance
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = bridge.BottomLeft
        expectedValue = WheatstoneTests.BridgeResistance
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = bridge.TopLeft
        expectedValue = WheatstoneTests.BridgeResistance
        Assert.AreEqual(expectedValue, actualValue)

        ' test validity
        Dim actualValidity As Boolean = bridge.IsValid
        Dim expectedValidity As Boolean = True
        Assert.AreEqual(expectedValidity, actualValidity)

        ' test balance
        Dim actualBalance As Boolean = bridge.IsOutputBalanced
        Dim expectedBalance As Boolean = True
        Assert.AreEqual(expectedBalance, actualBalance)

        ' test imbalance
        actualValue = bridge.ProductImbalance
        expectedValue = bridge.BottomLeft * bridge.TopRight - bridge.BottomRight * bridge.TopLeft
        Assert.AreEqual(expectedValue, actualValue, bridge.OutputEpsilon)

        ' test equivalent resistance
        actualValue = bridge.BridgeResistance
        expectedValue = (bridge.TopLeft + bridge.BottomLeft) * (bridge.TopRight + bridge.BottomRight) /
                        (bridge.TopLeft + bridge.BottomLeft + bridge.TopRight + bridge.BottomRight)
        Assert.AreEqual(expectedValue, actualValue, bridge.OutputEpsilon)

    End Sub

    ''' <summary> (Unit Test Method) tests build of invalid bridges. </summary>
    <TestMethod()>
    Public Sub BuildInvalidBridgeTest()

        ' build a balanced bridge
        Dim bridge As Wheatstone = New Wheatstone(Resistor.ShortResistance, WheatstoneTests.BridgeResistance,
                                                  WheatstoneTests.BridgeResistance, WheatstoneTests.BridgeResistance)

        Dim actualValidity As Boolean = bridge.IsValid
        Dim expectedValidity As Boolean = False
        Assert.AreEqual(expectedValidity, actualValidity)

        bridge = New Wheatstone(Resistor.OpenResistance, WheatstoneTests.BridgeResistance,
                                WheatstoneTests.BridgeResistance, WheatstoneTests.BridgeResistance)
        actualValidity = bridge.IsValid
        expectedValidity = False
        Assert.AreEqual(expectedValidity, actualValidity)

        bridge = New Wheatstone(WheatstoneTests.BridgeResistance, Resistor.ShortResistance,
                                WheatstoneTests.BridgeResistance, WheatstoneTests.BridgeResistance)
        actualValidity = bridge.IsValid
        expectedValidity = False
        Assert.AreEqual(expectedValidity, actualValidity)

        bridge = New Wheatstone(WheatstoneTests.BridgeResistance, Resistor.OpenResistance,
                                WheatstoneTests.BridgeResistance, WheatstoneTests.BridgeResistance)
        actualValidity = bridge.IsValid
        expectedValidity = False
        Assert.AreEqual(expectedValidity, actualValidity)

        bridge = New Wheatstone(WheatstoneTests.BridgeResistance, WheatstoneTests.BridgeResistance,
                                Resistor.ShortResistance, WheatstoneTests.BridgeResistance)
        actualValidity = bridge.IsValid
        expectedValidity = False
        Assert.AreEqual(expectedValidity, actualValidity)

        bridge = New Wheatstone(WheatstoneTests.BridgeResistance, WheatstoneTests.BridgeResistance,
                                Resistor.OpenResistance, WheatstoneTests.BridgeResistance)
        actualValidity = bridge.IsValid
        expectedValidity = False
        Assert.AreEqual(expectedValidity, actualValidity)

        bridge = New Wheatstone(WheatstoneTests.BridgeResistance, WheatstoneTests.BridgeResistance,
                                WheatstoneTests.BridgeResistance, Resistor.ShortResistance)
        actualValidity = bridge.IsValid
        expectedValidity = False
        Assert.AreEqual(expectedValidity, actualValidity)

        bridge = New Wheatstone(WheatstoneTests.BridgeResistance, WheatstoneTests.BridgeResistance,
                                WheatstoneTests.BridgeResistance, Resistor.OpenResistance)
        actualValidity = bridge.IsValid
        expectedValidity = False
        Assert.AreEqual(expectedValidity, actualValidity)

    End Sub


    ''' <summary> (Unit Test Method) tests null bridge exception. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="newBridge")>
    <TestMethod()>
    <ExpectedException(GetType(ArgumentNullException), "Null bridge inappropriately allowed")>
    Public Sub NullBridgeExceptionTest()
        Dim bridge As Wheatstone = Nothing
        ' try build the bridge using a null bridge; should issue the expected exception
        Dim newBridge As Wheatstone = New Wheatstone(bridge)
    End Sub

    ''' <summary> (Unit Test Method) tests build of positive offset bridge. </summary>
    <TestMethod()>
    Public Sub BuildPositiveOffsetBridgeTest()

        ' build a bridge with known positive offset
        Dim bridge As Wheatstone = New Wheatstone(WheatstoneTests.BridgeResistance, WheatstoneTests.RelativeOffset)

        ' test value of all bridge elements
        Dim actualValue As Double = bridge.TopRight
        Dim expectedValue As Double = WheatstoneTests.BridgeResistance * (1 + WheatstoneTests.RelativeOffset)
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = bridge.BottomRight
        expectedValue = WheatstoneTests.BridgeResistance * (1 - WheatstoneTests.RelativeOffset)
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = bridge.BottomLeft
        expectedValue = WheatstoneTests.BridgeResistance * (1 + WheatstoneTests.RelativeOffset)
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = bridge.TopLeft
        expectedValue = WheatstoneTests.BridgeResistance * (1 - WheatstoneTests.RelativeOffset)
        Assert.AreEqual(expectedValue, actualValue)

        ' test validity
        Dim actualValidity As Boolean = bridge.IsValid
        Dim expectedValidity As Boolean = True
        Assert.AreEqual(expectedValidity, actualValidity)

        ' test balance
        Dim actualBalance As Boolean = bridge.IsOutputBalanced
        Dim expectedBalance As Boolean = False
        Assert.AreEqual(expectedBalance, actualBalance)

        ' test offset
        actualValue = bridge.Output
        expectedValue = WheatstoneTests.RelativeOffset
        Assert.AreEqual(expectedBalance, actualBalance)

        ' test imbalance
        actualValue = bridge.ProductImbalance
        expectedValue = bridge.BottomLeft * bridge.TopRight - bridge.BottomRight * bridge.TopLeft
        Assert.AreEqual(expectedValue, actualValue, bridge.OutputEpsilon)

        ' test equivalent resistance
        actualValue = bridge.BridgeResistance
        expectedValue = (bridge.TopLeft + bridge.BottomLeft) * (bridge.TopRight + bridge.BottomRight) /
                        (bridge.TopLeft + bridge.BottomLeft + bridge.TopRight + bridge.BottomRight)
        Assert.AreEqual(expectedValue, actualValue, bridge.OutputEpsilon)
    End Sub

    ''' <summary> (Unit Test Method) tests build negative offset bridge. </summary>
    <TestMethod()>
    Public Sub BuildNegativeOffsetBridgeTest()

        ' build a bridge with known negative offset
        Dim bridge As Wheatstone = New Wheatstone(WheatstoneTests.BridgeResistance, -WheatstoneTests.RelativeOffset)

        ' test value of all bridge elements
        Dim actualValue As Double = bridge.TopRight
        Dim expectedValue As Double = WheatstoneTests.BridgeResistance * (1 - WheatstoneTests.RelativeOffset)
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = bridge.BottomRight
        expectedValue = WheatstoneTests.BridgeResistance * (1 + WheatstoneTests.RelativeOffset)
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = bridge.BottomLeft
        expectedValue = WheatstoneTests.BridgeResistance * (1 - WheatstoneTests.RelativeOffset)
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = bridge.TopLeft
        expectedValue = WheatstoneTests.BridgeResistance * (1 + WheatstoneTests.RelativeOffset)
        Assert.AreEqual(expectedValue, actualValue)

        ' test validity
        Dim actualValidity As Boolean = bridge.IsValid
        Dim expectedValidity As Boolean = True
        Assert.AreEqual(expectedValidity, actualValidity)

        ' test balance
        Dim actualBalance As Boolean = bridge.IsOutputBalanced
        Dim expectedBalance As Boolean = False
        Assert.AreEqual(expectedBalance, actualBalance)

        ' test offset
        actualValue = bridge.Output
        expectedValue = -WheatstoneTests.RelativeOffset
        Assert.AreEqual(expectedBalance, actualBalance)

        ' test imbalance
        actualValue = bridge.ProductImbalance
        expectedValue = bridge.BottomLeft * bridge.TopRight - bridge.BottomRight * bridge.TopLeft
        Assert.AreEqual(expectedValue, actualValue, bridge.OutputEpsilon)

        ' test equivalent resistance
        actualValue = bridge.BridgeResistance
        expectedValue = (bridge.TopLeft + bridge.BottomLeft) * (bridge.TopRight + bridge.BottomRight) /
                        (bridge.TopLeft + bridge.BottomLeft + bridge.TopRight + bridge.BottomRight)
        Assert.AreEqual(expectedValue, actualValue, bridge.OutputEpsilon)
    End Sub

#End Region

#Region " OUTPUT TESTS "

    ''' <summary> (Unit Test Method) tests balanced bridge output. </summary>
    <TestMethod()>
    Public Sub BalancedBridgeOutputTest()

        ' build a balanced bridge
        Dim bridge As Wheatstone = New Wheatstone(WheatstoneTests.BridgeResistance)

        ' test bridge output: bridge is balanced: all outputs are to be zero
        Dim actualValue As Double = bridge.Output(WheatstoneTests.ZeroBridgeVoltage)
        Dim expectedValue As Double = WheatstoneTests.ZeroBridgeVoltage
        Assert.AreEqual(expectedValue, actualValue)
        actualValue = bridge.Output(WheatstoneTests.LowBridgeVoltage)
        Assert.AreEqual(expectedValue, actualValue)
        actualValue = bridge.Output(WheatstoneTests.HighBridgeVoltage)
        Assert.AreEqual(expectedValue, actualValue)
        actualValue = bridge.Output(-WheatstoneTests.LowBridgeVoltage)
        Assert.AreEqual(expectedValue, actualValue)
        actualValue = bridge.Output(-WheatstoneTests.HighBridgeVoltage)
        Assert.AreEqual(expectedValue, actualValue)
    End Sub

    ''' <summary> (Unit Test Method) tests positive offset bridge output. </summary>
    <TestMethod()>
    Public Sub PositiveOffsetBridgeOutputTest()

        ' build a bridge with known positive offset
        Dim bridge As Wheatstone = New Wheatstone(WheatstoneTests.BridgeResistance, WheatstoneTests.RelativeOffset)

        ' test bridge output
        Dim bridgeVoltage As Double = WheatstoneTests.ZeroBridgeVoltage
        Dim relativeOffset As Double = WheatstoneTests.RelativeOffset
        Dim epsilon As Double = Math.Max(WheatstoneTests.RelativeOffsetEpsilon, WheatstoneTests.RelativeOffsetEpsilonScale * bridge.OutputEpsilon)
        Dim actualValue As Double = 1
        Dim expectedValue As Double = 0

        bridgeVoltage = WheatstoneTests.ZeroBridgeVoltage
        expectedValue = relativeOffset * bridgeVoltage
        actualValue = bridge.Output(bridgeVoltage)
        Assert.AreEqual(expectedValue, actualValue)

        bridgeVoltage = WheatstoneTests.LowBridgeVoltage
        expectedValue = relativeOffset * bridgeVoltage
        actualValue = bridge.Output(bridgeVoltage)
        Assert.AreEqual(expectedValue, actualValue, epsilon)

        bridgeVoltage = WheatstoneTests.HighBridgeVoltage
        expectedValue = relativeOffset * bridgeVoltage
        actualValue = bridge.Output(bridgeVoltage)
        Assert.AreEqual(expectedValue, actualValue, epsilon)

        bridgeVoltage = -WheatstoneTests.ZeroBridgeVoltage
        expectedValue = relativeOffset * bridgeVoltage
        actualValue = bridge.Output(bridgeVoltage)
        Assert.AreEqual(expectedValue, actualValue, epsilon)

        bridgeVoltage = -WheatstoneTests.LowBridgeVoltage
        expectedValue = relativeOffset * bridgeVoltage
        actualValue = bridge.Output(bridgeVoltage)
        Assert.AreEqual(expectedValue, actualValue, epsilon)

        bridgeVoltage = -WheatstoneTests.HighBridgeVoltage
        expectedValue = relativeOffset * bridgeVoltage
        actualValue = bridge.Output(bridgeVoltage)
        Assert.AreEqual(expectedValue, actualValue, epsilon)
    End Sub

    ''' <summary> (Unit Test Method) tests negative offset bridge output. </summary>
    <TestMethod()>
    Public Sub NegativeOffsetBridgeOutputTest()

        ' build a bridge with known positive offset
        Dim bridge As Wheatstone = New Wheatstone(WheatstoneTests.BridgeResistance, -WheatstoneTests.RelativeOffset)

        ' test bridge output
        Dim bridgeVoltage As Double = WheatstoneTests.ZeroBridgeVoltage
        Dim relativeOffset As Double = -WheatstoneTests.RelativeOffset
        Dim epsilon As Double = Math.Max(WheatstoneTests.RelativeOffsetEpsilon, WheatstoneTests.RelativeOffsetEpsilonScale * bridge.OutputEpsilon)
        Dim actualValue As Double = 1
        Dim expectedValue As Double = 0

        bridgeVoltage = WheatstoneTests.ZeroBridgeVoltage
        expectedValue = relativeOffset * bridgeVoltage
        actualValue = bridge.Output(bridgeVoltage)
        Assert.AreEqual(expectedValue, actualValue)

        bridgeVoltage = WheatstoneTests.LowBridgeVoltage
        expectedValue = relativeOffset * bridgeVoltage
        actualValue = bridge.Output(bridgeVoltage)
        Assert.AreEqual(expectedValue, actualValue, epsilon)

        bridgeVoltage = WheatstoneTests.HighBridgeVoltage
        expectedValue = relativeOffset * bridgeVoltage
        actualValue = bridge.Output(bridgeVoltage)
        Assert.AreEqual(expectedValue, actualValue, epsilon)

        bridgeVoltage = -WheatstoneTests.ZeroBridgeVoltage
        expectedValue = relativeOffset * bridgeVoltage
        actualValue = bridge.Output(bridgeVoltage)
        Assert.AreEqual(expectedValue, actualValue, epsilon)

        bridgeVoltage = -WheatstoneTests.LowBridgeVoltage
        expectedValue = relativeOffset * bridgeVoltage
        actualValue = bridge.Output(bridgeVoltage)
        Assert.AreEqual(expectedValue, actualValue, epsilon)

        bridgeVoltage = -WheatstoneTests.HighBridgeVoltage
        expectedValue = relativeOffset * bridgeVoltage
        actualValue = bridge.Output(bridgeVoltage)
        Assert.AreEqual(expectedValue, actualValue, epsilon)
    End Sub

#End Region

#Region " EQUALITY TESTS "

    ''' <summary> (Unit Test Method) tests equals. </summary>
    <TestMethod()>
    Public Sub EqualsTest()

        ' build identical bridges
        Dim bridge1 As Wheatstone = New Wheatstone(WheatstoneTests.BridgeResistance, -WheatstoneTests.RelativeOffset)
        Dim bridge2 As Wheatstone = New Wheatstone(WheatstoneTests.BridgeResistance, -WheatstoneTests.RelativeOffset)

        ' test bridge equality
        Dim actualValue As Boolean = Wheatstone.Equals(bridge1, bridge2)
        Dim expectedValue As Boolean = True
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = bridge1 = bridge2
        expectedValue = True
        Assert.AreEqual(expectedValue, actualValue)

        actualValue = bridge1 <> bridge2
        expectedValue = False
        Assert.AreEqual(expectedValue, actualValue)

        Assert.AreEqual(bridge1, bridge2)

        ' change bridge 1 to a positive offset bridge
        bridge1 = New Wheatstone(WheatstoneTests.BridgeResistance, +WheatstoneTests.RelativeOffset)
        Assert.AreNotEqual(bridge1, bridge2)

        ' change bridge 2 to a positive offset bridge
        bridge2 = New Wheatstone(WheatstoneTests.BridgeResistance, +WheatstoneTests.RelativeOffset)
        Assert.AreEqual(bridge1, bridge2)

    End Sub

#End Region

End Class
End Namespace

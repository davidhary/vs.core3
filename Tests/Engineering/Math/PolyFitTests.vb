﻿Imports System.Windows
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Namespace Engineering.Tests

    ''' <summary> A polygon fit tests. </summary>
    ''' <license>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    ''' <history date="4/13/2018" by="David" revision=""> Created. </history>
    <TestClass()> Public Class PolyFitTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
                TestInfo.InitializeTraceListener()
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            Assert.IsTrue(PolyFitTestInfo.Get.Exists, $"{GetType(PolyFitTestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()

            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

        ''' <summary> Builds a determinant. </summary>
        ''' <param name="values"> The values. </param>
        ''' <returns> A Double(,) </returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="Body")>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="Return")>
        Public Shared Function BuildDeterminant(ByVal values As IEnumerable(Of Double)) As Double(,)
            Dim colCount As Integer = 3
            Dim rowCount As Integer = 3
            Dim a As Double(,)
            ReDim a(colCount - 1, rowCount - 1)
            Dim i As Integer = 0
            For r As Integer = 0 To rowCount - 1
                For c As Integer = 0 To colCount - 1
                    a(c, r) = values(i)
                    i += 1
                Next
            Next
            Return a
        End Function

        ''' <summary> (Unit Test Method) builds determinant test method. </summary>
        <TestMethod()> Public Sub BuildDeterminantTestMethod()
            Dim values As Double() = {2, -3, -2, -6, 3, 3, -2, -3, -2}
            Dim determinant As Double(,) = PolyFitTests.BuildDeterminant(values)
            Dim columnNumber As Integer = 0
            Dim rowNumber As Integer = 0
            Dim expectedValue As Double = 2
            Dim actualValue As Double = determinant(columnNumber, rowNumber)
            Assert.AreEqual(expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})")
            columnNumber = 1
            expectedValue = -3
            actualValue = determinant(columnNumber, rowNumber)
            Assert.AreEqual(expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})")
            rowNumber = 2
            expectedValue = -3
            actualValue = determinant(columnNumber, rowNumber)
            Assert.AreEqual(expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})")

            'actualValue = QuadraticPolynomial.Determinant(Me.BuildDeterminant(values))
            'Assert.AreEqual(expectedValue, actualValue)
        End Sub

        ''' <summary> (Unit Test Method) calculates the determinant test method. </summary>
        <TestMethod()> Public Sub CalculateDeterminantTestMethod()
            Dim values As Double() = {2, -3, -2, -6, 3, 3, -2, -3, -2}
            Dim expectedValue As Double = 12
            Dim actualValue As Double = Core.Engineering.QuadraticPolynomial.Determinant(PolyFitTests.BuildDeterminant(values))
            Assert.AreEqual(expectedValue, actualValue)
            values = {-4, 5, 2, -3, 4, 2, -1, 2, 5}
            expectedValue = -3
            actualValue = Core.Engineering.QuadraticPolynomial.Determinant(PolyFitTests.BuildDeterminant(values))
            Assert.AreEqual(expectedValue, actualValue)
        End Sub

        ''' <summary> (Unit Test Method) Cramer substitution test method. </summary>
        <TestMethod()> Public Sub CramerSubstitutionTestMethod()
            Dim values As Double() = {2, -1, 6, -3, 4, -5, 8, -7, -9}
            Dim constants As Double() = {10, 11, 12}
            Dim coefficients As Double(,) = PolyFitTests.BuildDeterminant(values)
            Dim expectedValue As Double = -141
            Dim determinant As Double = Core.Engineering.QuadraticPolynomial.Determinant(coefficients)
            Dim actualValue As Double = determinant
            Assert.AreEqual(expectedValue, actualValue)
            Dim result As Double(,) = Core.Engineering.QuadraticPolynomial.CramerSubstitution(0, coefficients, constants)
            Dim columnNumber As Integer = 0
            Dim rowNumber As Integer = 0
            expectedValue = 10
            actualValue = result(columnNumber, rowNumber)
            Assert.AreEqual(expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})")
            columnNumber = 1
            expectedValue = -1
            actualValue = result(columnNumber, rowNumber)
            Assert.AreEqual(expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})")
            rowNumber = 2
            expectedValue = -7
            actualValue = result(columnNumber, rowNumber)
            Assert.AreEqual(expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})")

        End Sub

        ''' <summary> (Unit Test Method) Cramer rule test method. </summary>
        <TestMethod()> Public Sub CramerRuleTestMethod()
            Dim values As Double() = {2, -1, 6, -3, 4, -5, 8, -7, -9}
            Dim constants As Double() = {10, 11, 12}
            Dim coefficients As Double(,) = PolyFitTests.BuildDeterminant(values)
            Dim expectedValue As Double = -141
            Dim determinant As Double = Core.Engineering.QuadraticPolynomial.Determinant(coefficients)
            Dim actualValue As Double = determinant
            Assert.AreEqual(expectedValue, actualValue)
            Dim result As Double() = Core.Engineering.QuadraticPolynomial.CramerRule(coefficients, constants)
            expectedValue = -1499
            actualValue = result(0)
            Assert.AreEqual(expectedValue, actualValue)
            expectedValue = -1492
            actualValue = result(1)
            Assert.AreEqual(expectedValue, actualValue)
            expectedValue = 16
            actualValue = result(2)
            Assert.AreEqual(expectedValue, actualValue)
        End Sub

        ''' <summary> (Unit Test Method) polynomial fit test method three points. </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="FitTest")>
        <TestMethod()> Public Sub PolyFitTestMethodThreePoints()
            Dim quadPoly As New Core.Engineering.QuadraticPolynomial(100, 0.5, 0.02)
            Dim values As New List(Of System.Windows.Point)
            Dim x As Double = 0
            x = -50 : values.Add(New System.Windows.Point(x, quadPoly.Evaluate(x)))
            x = 1 : values.Add(New System.Windows.Point(x, quadPoly.Evaluate(x)))
            x = 75 : values.Add(New System.Windows.Point(x, quadPoly.Evaluate(x)))
            Dim fitPoly As New Core.Engineering.QuadraticPolynomial
            Dim expectedValue As Double = 1 ' goodness of fit = R-Squared
            Dim actualValue As Double = fitPoly.PolyFit(values)
            expectedValue = 1 ' goodness of fit.
            actualValue = fitPoly.GoodnessOfFit
            Assert.AreEqual(expectedValue, actualValue, 0.001)
            expectedValue = 0
            actualValue = fitPoly.StandardError
            Assert.AreEqual(expectedValue, actualValue, 0.001)
            expectedValue = quadPoly.ConstantCoefficient
            actualValue = fitPoly.ConstantCoefficient
            Assert.AreEqual(expectedValue, actualValue, 0.001)
            expectedValue = quadPoly.LinearCoefficient
            actualValue = fitPoly.LinearCoefficient
            Assert.AreEqual(expectedValue, actualValue, 0.001)
            expectedValue = quadPoly.QuadraticCoefficient
            actualValue = fitPoly.QuadraticCoefficient
            Assert.AreEqual(expectedValue, actualValue, 0.001)
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="FitTest")>
        <TestMethod()> Public Sub PolyFitTestMethodFourPoints()
            Dim quadPoly As New Core.Engineering.QuadraticPolynomial(100, 0.5, 0.02)
            Dim values As New List(Of System.Windows.Point)
            Dim x As Double = 0
            x = -50 : values.Add(New System.Windows.Point(x, quadPoly.Evaluate(x)))
            x = 1 : values.Add(New System.Windows.Point(x, quadPoly.Evaluate(x)))
            x = 75 : values.Add(New System.Windows.Point(x, quadPoly.Evaluate(x)))
            x = 125 : values.Add(New System.Windows.Point(x, quadPoly.Evaluate(x)))
            Dim fitPoly As New Core.Engineering.QuadraticPolynomial
            Dim expectedValue As Double = 1 ' goodness of fit = R-Squared
            Dim actualValue As Double = fitPoly.PolyFit(values)
            expectedValue = 1 ' goodness of fit.
            actualValue = fitPoly.GoodnessOfFit
            Assert.AreEqual(expectedValue, actualValue, 0.001)
            expectedValue = 0
            actualValue = fitPoly.StandardError
            Assert.AreEqual(expectedValue, actualValue, 0.001)
            expectedValue = quadPoly.ConstantCoefficient
            actualValue = fitPoly.ConstantCoefficient
            Assert.AreEqual(expectedValue, actualValue, 0.001)
            expectedValue = quadPoly.LinearCoefficient
            actualValue = fitPoly.LinearCoefficient
            Assert.AreEqual(expectedValue, actualValue, 0.001)
            expectedValue = quadPoly.QuadraticCoefficient
            actualValue = fitPoly.QuadraticCoefficient
            Assert.AreEqual(expectedValue, actualValue, 0.001)
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="FitTest")>
        <TestMethod()> Public Sub PolyFitTestMethodFourPointsWithError()
            Dim quadPoly As New Core.Engineering.QuadraticPolynomial(100, 0.5, 0.02)
            Dim values As New List(Of System.Windows.Point)
            Dim x As Double = 0
            Dim e As Double = 0 ' error
            x = -50 : e = 0.1 : values.Add(New System.Windows.Point(x, e + quadPoly.Evaluate(x)))
            Dim ssq As Double = Math.Pow(e, 2)
            x = 1 : e = 0.001 : values.Add(New System.Windows.Point(x, e + quadPoly.Evaluate(x)))
            ssq += Math.Pow(e, 2)
            x = 75 : e = 0.02 : values.Add(New System.Windows.Point(x, e + quadPoly.Evaluate(x)))
            ssq += Math.Pow(e, 2)
            x = 125 : e = 0.003 : values.Add(New System.Windows.Point(x, e + quadPoly.Evaluate(x)))
            ssq += Math.Pow(e, 2)
            Dim fitPoly As New Core.Engineering.QuadraticPolynomial
            Dim expectedValue As Double = 1 ' goodness of fit = R-Squared
            Dim actualValue As Double = fitPoly.PolyFit(values)
            expectedValue = 1 ' goodness of fit.
            actualValue = fitPoly.GoodnessOfFit
            Assert.AreEqual(expectedValue, actualValue, 0.001)
            Dim delta As Double = Math.Sqrt(ssq / values.Count)
            expectedValue = 0
            actualValue = fitPoly.StandardError
            Assert.AreEqual(expectedValue, actualValue, delta)
            expectedValue = quadPoly.ConstantCoefficient
            actualValue = fitPoly.ConstantCoefficient
            Assert.AreEqual(expectedValue, actualValue, delta)
            expectedValue = quadPoly.LinearCoefficient
            actualValue = fitPoly.LinearCoefficient
            Assert.AreEqual(expectedValue, actualValue, delta)
            expectedValue = quadPoly.QuadraticCoefficient
            actualValue = fitPoly.QuadraticCoefficient
            Assert.AreEqual(expectedValue, actualValue, delta)
        End Sub

        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="FitTest")>
        <TestMethod()> Public Sub PolyFitTestMethodFourDataPoints()
            Dim values As New List(Of System.Windows.Point)
            Dim xo As Double = 273
            Dim p1 As New Point(303, 1048) : values.Add(p1)
            Dim p2 As New Point(327, 1161) : values.Add(p2)
            Dim p3 As New Point(345, 1239) : values.Add(p3)
            Dim p4 As New Point(350, 1257) : values.Add(p4)
            Dim fitPoly As New Core.Engineering.QuadraticPolynomial
            Dim expectedValue As Double = 0
            Dim actualValue As Double = fitPoly.PolyFit(values)
            expectedValue = 1 ' goodness of fit.
            actualValue = fitPoly.GoodnessOfFit
            Assert.AreEqual(expectedValue, actualValue, 0.1)
            expectedValue = 0.01 + p1.X
            Dim delta As Double = fitPoly.StandardError
            Assert.IsTrue(delta < expectedValue, $"Is low standard error {delta}<{expectedValue}")
            delta = 3 * delta
            expectedValue = fitPoly.Evaluate(p1.X)
            Assert.AreEqual(expectedValue, p1.Y, delta)
            expectedValue = fitPoly.Evaluate(p2.X)
            Assert.AreEqual(expectedValue, p2.Y, delta)
            expectedValue = fitPoly.Evaluate(p3.X)
            Assert.AreEqual(expectedValue, p3.Y, delta)
            expectedValue = fitPoly.Evaluate(p4.X)
            Assert.AreEqual(expectedValue, p4.Y, delta)
            expectedValue = 1000
            actualValue = fitPoly.Evaluate(xo + 25)
            Assert.IsTrue(actualValue > expectedValue, $"Nominal value {actualValue}>{expectedValue}")
            expectedValue = 0
            actualValue = fitPoly.Slope(p1.X)
            Assert.IsTrue(actualValue > expectedValue, $"Positive slope @{p1.X} {actualValue}>{expectedValue}")

            actualValue = fitPoly.Slope(p2.X)
            Assert.IsTrue(actualValue > expectedValue, $"Positive slope @{p2.X} {actualValue}>{expectedValue}")

            actualValue = fitPoly.Slope(p3.X)
            Assert.IsTrue(actualValue > expectedValue, $"Positive slope @{p3.X} {actualValue}>{expectedValue}")

            actualValue = fitPoly.Slope(p4.X)
            Assert.IsTrue(actualValue > expectedValue, $"Positive slope @{p4.X} {actualValue}>{expectedValue}")

        End Sub

    End Class
End Namespace
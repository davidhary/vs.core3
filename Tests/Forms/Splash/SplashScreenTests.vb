﻿Namespace WindowsForms.Tests
    '''<summary>
    '''This is a test class for the splash screen.
    '''</summary>
    <TestClass()>
    Public Class SplashScreenTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

        ''' <summary> (Unit Test Method) displays a splash screen test. </summary>
        <TestMethod()>
        Public Sub DisplaySplashScreenTest()
            Using target As isr.Core.WindowsForms.SplashScreen = New isr.Core.WindowsForms.SplashScreen()
                target.Show()
                target.TopmostSetter(True)
                Dim sw As Stopwatch = Stopwatch.StartNew
                For i As Integer = 1 To 10
                    target.DisplayMessage($"Splash message {Date.Now.ToLongTimeString}")
                    isr.Core.Pith.StopwatchExtensions.Wait(sw, TimeSpan.FromMilliseconds(200))
                Next
            End Using
        End Sub

        ''' <summary> Tests the process exception on a another thread. </summary>
        <TestMethod()>
        Public Sub DisplaySplashScreenThreadTest()
            Dim oThread As New Threading.Thread(New Threading.ThreadStart(AddressOf DisplaySplashScreenTest))
            oThread.Start()
            oThread.Join()
        End Sub

    End Class
End Namespace
﻿Namespace InputOutput.Tests

    '''<summary>
    '''This is a test class for KnownFoldersTest and is intended
    '''to contain all KnownFoldersTest Unit Tests
    '''</summary>
    <TestClass()>
    Public Class KnownFoldersTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        ''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        <TestInitialize()> Public Sub MyTestInitialize()
            Assert.IsTrue(TestInfo.Exists, $"{GetType(TestInfo)}.{NameOf(TestInfo)} settings not found")
            TestInfo.ClearMessageQueue()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        <TestCleanup()> Public Sub MyTestCleanup()
            TestInfo.AssertMessageQueue()
        End Sub

        '''<summary>
        '''Gets or sets the test context which provides
        '''information about and functionality for the current test run.
        '''</summary>
        Public Property TestContext() As TestContext

#End Region

        ''' <summary> A test for GetPath. </summary>
        ''' <param name="knownFolder"> A known folder enum value. </param>
        ''' <param name="expected">    The expected value. </param>
        Public Shared Sub TestGetPath(ByVal knownFolder As Core.InputOutput.KnownFolderPaths.KnownFolder, ByVal expected As String)
            Dim actual As String
            actual = Core.InputOutput.KnownFolderPaths.KnownFolders.GetPath(knownFolder)
            Assert.AreEqual(expected, actual, True, Globalization.CultureInfo.CurrentCulture)
        End Sub

        ''' <summary> A test for Get Path on multiple <see cref="Core.InputOutput.KnownFolderPaths"/> folders. </summary>
        <TestMethod()>
        Public Sub GetPathTest()
            KnownFoldersTests.TestGetPath(Core.InputOutput.KnownFolderPaths.KnownFolder.Documents, String.Format("C:\Users\{0}\Documents", Environment.UserName))
            KnownFoldersTests.TestGetPath(Core.InputOutput.KnownFolderPaths.KnownFolder.UserProfiles, "C:\Users")
            KnownFoldersTests.TestGetPath(Core.InputOutput.KnownFolderPaths.KnownFolder.ProgramData, "C:\ProgramData")
        End Sub

        ''' <summary> A test for Get Default Path. </summary>
        ''' <param name="knownFolder"> A known folder enum value. </param>
        ''' <param name="expected">    The expected value. </param>
        Public Shared Sub TestGetDefaultPath(ByVal knownFolder As Core.InputOutput.KnownFolderPaths.KnownFolder, ByVal expected As String)
            Dim actual As String
            actual = Core.InputOutput.KnownFolderPaths.KnownFolders.GetDefaultPath(knownFolder)
            Assert.AreEqual(expected, actual, True, Globalization.CultureInfo.CurrentCulture)
        End Sub

        ''' <summary> A test for GetDefaultPath. </summary>
        <TestMethod()>
        Public Sub GetDefaultPathTest()
            KnownFoldersTests.TestGetDefaultPath(Core.InputOutput.KnownFolderPaths.KnownFolder.Documents, String.Format("C:\Users\{0}\Documents", Environment.UserName))
            KnownFoldersTests.TestGetDefaultPath(Core.InputOutput.KnownFolderPaths.KnownFolder.UserProfiles, "C:\Users")
            KnownFoldersTests.TestGetDefaultPath(Core.InputOutput.KnownFolderPaths.KnownFolder.ProgramData, "C:\ProgramData")
        End Sub
    End Class
End Namespace
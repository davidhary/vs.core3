# Core3 Libraries Project

These projects includes classes and test program for core support and extensions of the Visual Studio framework.

## Getting Started

Clone the project along with its requisite projects to their respective relative path. All projects are installed relative to a common path.

### Prerequisites

The following projects are also required:
* [Core](https://www.bitbucket.org/davidhary/vs.core3) - Legacy Core Libraries
* [Share](https://www.bitbucket.org/davidhary/vs.share) - Shared snippets

```
git clone git@bitbucket.org:davidhary/vs.share.git
git clone git@bitbucket.org:davidhary/vs.core3.git
```

### Installing

Install the projects into the following folders:

#### Projects relative path
```
.\Libraries\VS\Share
.\Libraries\VS\Core\Core3
```

## Testing

The project includes a few unit test classes. Test applications are under the *Apps* solution folder. 

## Deployment

Deployment projects have not been created for this project.

## Built, Tested and Facilitated By

* [Visual Studio](https://www.visualstudio.com/) - Visual Studio 2015
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - Wix Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker

## Authors

* **David Hary** - *Initial Workarounds* - [ATE Coder](http://www.integratedscientificresources.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md] file at (https://www.bitbucket.org/davidhary/vs.core/src) for details

## Acknowledgments

* Hat tip to anyone who's code was used
* [Its all a remix] (www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [Stack overflow] (https://www.stackoveflow.com)
* [Safe Events] (http://www.CodeProject.com/KB/cs/EventSafeTrigger.aspx)
* [aeonhack] (http://www.DreamInCode.net/forums/user/334492-aeonhack/)
* [dream in code] (http://www.DreamInCode.net/code/snippet5016.htm)
* [Drop Shadow and Fade From] (http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx)
* [Wallace Kelly] (http://WallaceKelly.BlogSpot.com/) - Engineering Format
* [Strong Enumerations] (http://www.CodeProject.com/KB/cs/efficient_strong_enum.aspx)
* [Enumeration Extensions] (http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx) 
* [String Enumerator] (http://www.codeproject.com/Articles/17472/StringEnumerator)
* [Cory Charlton] (https://github.com/CoryCharlton/CCSWE.Core) Collection extensions

## Revision Changes

* Version 3.0.6093 09/04/16
	Adds post and send event handler extensions.

Imports System.Drawing
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TextInputBox

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="InputBox")>
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._enteredValueTextBox = New System.Windows.Forms.TextBox()
        Me._cancelButton = New System.Windows.Forms.Button()
        Me._acceptButton = New System.Windows.Forms.Button()
        Me._enteredValueTextBoxLabel = New System.Windows.Forms.Label()
        Me._validationErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me._validationErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_enteredValueTextBox
        '
        Me._enteredValueTextBox.Font = New Font(Me.Font, FontStyle.Bold)
        Me._enteredValueTextBox.Location = New Point(9, 27)
        Me._enteredValueTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._enteredValueTextBox.Name = "_enteredValueTextBox"
        Me._enteredValueTextBox.Size = New Size(186, 25)
        Me._enteredValueTextBox.TabIndex = 28
        Me._enteredValueTextBox.Text = "TextBox1"
        '
        '_cancelButton
        '
        Me._cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._cancelButton.Font = New Font(Me.Font, FontStyle.Bold)
        Me._cancelButton.Location = New Point(9, 59)
        Me._cancelButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._cancelButton.Name = "_cancelButton"
        Me._cancelButton.Size = New Size(87, 30)
        Me._cancelButton.TabIndex = 27
        Me._cancelButton.Text = "&Cancel"
        '
        '_acceptButton
        '
        Me._acceptButton.Enabled = False
        Me._acceptButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._acceptButton.Font = New Font(Me.Font, FontStyle.Bold)
        Me._acceptButton.Location = New Point(108, 59)
        Me._acceptButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._acceptButton.Name = "_acceptButton"
        Me._acceptButton.Size = New Size(87, 30)
        Me._acceptButton.TabIndex = 26
        Me._acceptButton.Text = "&OK"
        '
        '_enteredValueTextBoxLabel
        '
        Me._enteredValueTextBoxLabel.BackColor = SystemColors.Control
        Me._enteredValueTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._enteredValueTextBoxLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._enteredValueTextBoxLabel.ForeColor = SystemColors.WindowText
        Me._enteredValueTextBoxLabel.Location = New Point(9, 5)
        Me._enteredValueTextBoxLabel.Name = "_enteredValueTextBoxLabel"
        Me._enteredValueTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._enteredValueTextBoxLabel.Size = New Size(156, 21)
        Me._enteredValueTextBoxLabel.TabIndex = 25
        Me._enteredValueTextBoxLabel.Text = "Enter a Value: "
        Me._enteredValueTextBoxLabel.TextAlign = ContentAlignment.BottomLeft
        '
        '_validationErrorProvider
        '
        Me._validationErrorProvider.ContainerControl = Me
        '
        'InputBox
        '
        Me.ClientSize = New Size(209, 98)
        Me.ControlBox = False
        Me.Controls.Add(Me._enteredValueTextBox)
        Me.Controls.Add(Me._cancelButton)
        Me.Controls.Add(Me._acceptButton)
        Me.Controls.Add(Me._enteredValueTextBoxLabel)
        Me.Name = "InputBox"
        Me.Text = "InputBox"
        CType(Me._validationErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _EnteredValueTextBox As System.Windows.Forms.TextBox
    Private WithEvents _CancelButton As System.Windows.Forms.Button
    Private WithEvents _AcceptButton As System.Windows.Forms.Button
    Private WithEvents _EnteredValueTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ValidationErrorProvider As System.Windows.Forms.ErrorProvider
End Class

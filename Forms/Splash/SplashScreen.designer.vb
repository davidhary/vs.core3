<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SplashScreen

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SplashScreen))
        Me._rightPanel = New System.Windows.Forms.Panel()
        Me._productNameLabel = New System.Windows.Forms.Label()
        Me._productTitleLabel = New System.Windows.Forms.Label()
        Me._productVersionLabel = New System.Windows.Forms.Label()
        Me._licenseeLabel = New System.Windows.Forms.Label()
        Me._platformTitleLabel = New System.Windows.Forms.Label()
        Me._productFamilyLabel = New System.Windows.Forms.Label()
        Me._companyNameLabel = New System.Windows.Forms.Label()
        Me._leftPanel = New System.Windows.Forms.Panel()
        Me._disksPictureBox = New System.Windows.Forms.PictureBox()
        Me._companyLogoPictureBox = New System.Windows.Forms.PictureBox()
        Me._cancelButton = New System.Windows.Forms.Button()
        Me._statusLabel = New System.Windows.Forms.Label()
        Me._copyrightLabel = New System.Windows.Forms.Label()
        Me._copyrightWarningLabel = New System.Windows.Forms.Label()
        Me._rightPanel.SuspendLayout()
        Me._leftPanel.SuspendLayout()
        CType(Me._disksPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._companyLogoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_rightPanel
        '
        Me._rightPanel.Controls.Add(Me._productNameLabel)
        Me._rightPanel.Controls.Add(Me._productTitleLabel)
        Me._rightPanel.Controls.Add(Me._productVersionLabel)
        Me._rightPanel.Controls.Add(Me._licenseeLabel)
        Me._rightPanel.Controls.Add(Me._platformTitleLabel)
        Me._rightPanel.Controls.Add(Me._productFamilyLabel)
        Me._rightPanel.Controls.Add(Me._companyNameLabel)
        Me._rightPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._rightPanel.Location = New System.Drawing.Point(133, 0)
        Me._rightPanel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._rightPanel.Name = "_rightPanel"
        Me._rightPanel.Size = New System.Drawing.Size(392, 266)
        Me._rightPanel.TabIndex = 33
        '
        '_productNameLabel
        '
        Me._productNameLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._productNameLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._productNameLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._productNameLabel.Font = New System.Drawing.Font(Me.Font.FontFamily, 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._productNameLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(66, Byte), Integer))
        Me._productNameLabel.Location = New System.Drawing.Point(0, 90)
        Me._productNameLabel.Name = "_productNameLabel"
        Me._productNameLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._productNameLabel.Size = New System.Drawing.Size(392, 47)
        Me._productNameLabel.TabIndex = 13
        Me._productNameLabel.Tag = "Product"
        Me._productNameLabel.Text = "Product"
        Me._productNameLabel.UseMnemonic = False
        '
        '_productTitleLabel
        '
        Me._productTitleLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._productTitleLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._productTitleLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._productTitleLabel.Font = New System.Drawing.Font(Me.Font.FontFamily, 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._productTitleLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(214, Byte), Integer))
        Me._productTitleLabel.Location = New System.Drawing.Point(0, 59)
        Me._productTitleLabel.Name = "_productTitleLabel"
        Me._productTitleLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._productTitleLabel.Size = New System.Drawing.Size(392, 31)
        Me._productTitleLabel.TabIndex = 26
        Me._productTitleLabel.Text = "Title"
        Me._productTitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._productTitleLabel.UseMnemonic = False
        '
        '_productVersionLabel
        '
        Me._productVersionLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._productVersionLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._productVersionLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._productVersionLabel.Font = New System.Drawing.Font(Me.Font.FontFamily, 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._productVersionLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(148, Byte), Integer))
        Me._productVersionLabel.Location = New System.Drawing.Point(0, 137)
        Me._productVersionLabel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._productVersionLabel.Name = "_productVersionLabel"
        Me._productVersionLabel.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._productVersionLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._productVersionLabel.Size = New System.Drawing.Size(392, 39)
        Me._productVersionLabel.TabIndex = 16
        Me._productVersionLabel.Tag = "Version"
        Me._productVersionLabel.Text = "Version {0}.{1:00}.{2}"
        Me._productVersionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._productVersionLabel.UseMnemonic = False
        '
        '_licenseeLabel
        '
        Me._licenseeLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._licenseeLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._licenseeLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._licenseeLabel.Font = New System.Drawing.Font(Me.Font.FontFamily, 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._licenseeLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(214, Byte), Integer))
        Me._licenseeLabel.Location = New System.Drawing.Point(0, 17)
        Me._licenseeLabel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._licenseeLabel.Name = "_licenseeLabel"
        Me._licenseeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._licenseeLabel.Size = New System.Drawing.Size(392, 42)
        Me._licenseeLabel.TabIndex = 25
        Me._licenseeLabel.Text = "Licensee"
        Me._licenseeLabel.UseMnemonic = False
        '
        '_platformTitleLabel
        '
        Me._platformTitleLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._platformTitleLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._platformTitleLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._platformTitleLabel.Font = New System.Drawing.Font(Me.Font.FontFamily, 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._platformTitleLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(148, Byte), Integer))
        Me._platformTitleLabel.Location = New System.Drawing.Point(0, 176)
        Me._platformTitleLabel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._platformTitleLabel.Name = "_platformTitleLabel"
        Me._platformTitleLabel.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._platformTitleLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._platformTitleLabel.Size = New System.Drawing.Size(392, 27)
        Me._platformTitleLabel.TabIndex = 20
        Me._platformTitleLabel.Tag = "Platform"
        Me._platformTitleLabel.Text = "Platform"
        Me._platformTitleLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me._platformTitleLabel.UseMnemonic = False
        '
        '_productFamilyLabel
        '
        Me._productFamilyLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._productFamilyLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._productFamilyLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._productFamilyLabel.Font = New System.Drawing.Font(Me.Font.FontFamily, 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._productFamilyLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(214, Byte), Integer))
        Me._productFamilyLabel.Location = New System.Drawing.Point(0, 0)
        Me._productFamilyLabel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._productFamilyLabel.Name = "_productFamilyLabel"
        Me._productFamilyLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._productFamilyLabel.Size = New System.Drawing.Size(392, 17)
        Me._productFamilyLabel.TabIndex = 20
        Me._productFamilyLabel.Text = "An Integrated Scientific Resources Product"
        Me._productFamilyLabel.UseMnemonic = False
        '
        '_companyNameLabel
        '
        Me._companyNameLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._companyNameLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._companyNameLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._companyNameLabel.Font = New System.Drawing.Font(Me.Font.FontFamily, 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._companyNameLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(66, Byte), Integer))
        Me._companyNameLabel.Location = New System.Drawing.Point(0, 203)
        Me._companyNameLabel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._companyNameLabel.Name = "_companyNameLabel"
        Me._companyNameLabel.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._companyNameLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._companyNameLabel.Size = New System.Drawing.Size(392, 63)
        Me._companyNameLabel.TabIndex = 28
        Me._companyNameLabel.Tag = "Company"
        Me._companyNameLabel.Text = "Company"
        Me._companyNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._companyNameLabel.UseMnemonic = False
        '
        '_leftPanel
        '
        Me._leftPanel.Controls.Add(Me._disksPictureBox)
        Me._leftPanel.Controls.Add(Me._companyLogoPictureBox)
        Me._leftPanel.Controls.Add(Me._cancelButton)
        Me._leftPanel.Dock = System.Windows.Forms.DockStyle.Left
        Me._leftPanel.Location = New System.Drawing.Point(0, 0)
        Me._leftPanel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._leftPanel.Name = "_leftPanel"
        Me._leftPanel.Size = New System.Drawing.Size(133, 266)
        Me._leftPanel.TabIndex = 34
        '
        '_disksPictureBox
        '
        Me._disksPictureBox.BackColor = System.Drawing.Color.Transparent
        Me._disksPictureBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._disksPictureBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._disksPictureBox.Image = CType(resources.GetObject("_disksPictureBox.Image"), System.Drawing.Image)
        Me._disksPictureBox.Location = New System.Drawing.Point(0, 160)
        Me._disksPictureBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._disksPictureBox.Name = "_disksPictureBox"
        Me._disksPictureBox.Size = New System.Drawing.Size(133, 112)
        Me._disksPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me._disksPictureBox.TabIndex = 23
        Me._disksPictureBox.TabStop = False
        '
        '_companyLogoPictureBox
        '
        Me._companyLogoPictureBox.BackColor = System.Drawing.Color.Transparent
        Me._companyLogoPictureBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._companyLogoPictureBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._companyLogoPictureBox.Image = CType(resources.GetObject("_companyLogoPictureBox.Image"), System.Drawing.Image)
        Me._companyLogoPictureBox.Location = New System.Drawing.Point(0, 0)
        Me._companyLogoPictureBox.Margin = New System.Windows.Forms.Padding(3, 13, 3, 4)
        Me._companyLogoPictureBox.Name = "_companyLogoPictureBox"
        Me._companyLogoPictureBox.Padding = New System.Windows.Forms.Padding(0, 4, 0, 0)
        Me._companyLogoPictureBox.Size = New System.Drawing.Size(133, 160)
        Me._companyLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me._companyLogoPictureBox.TabIndex = 23
        Me._companyLogoPictureBox.TabStop = False
        '
        '_cancelButton
        '
        Me._cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._cancelButton.Location = New System.Drawing.Point(24, 179)
        Me._cancelButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._cancelButton.Name = "_cancelButton"
        Me._cancelButton.Size = New System.Drawing.Size(87, 30)
        Me._cancelButton.TabIndex = 0
        Me._cancelButton.Text = "Cancel"
        Me._cancelButton.UseVisualStyleBackColor = True
        '
        '_statusLabel
        '
        Me._statusLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._statusLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._statusLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._statusLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(148, Byte), Integer))
        Me._statusLabel.Location = New System.Drawing.Point(0, 266)
        Me._statusLabel.Name = "_statusLabel"
        Me._statusLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._statusLabel.Size = New System.Drawing.Size(525, 35)
        Me._statusLabel.TabIndex = 32
        Me._statusLabel.Tag = "status"
        Me._statusLabel.Text = "status"
        Me._statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me._statusLabel.UseMnemonic = False
        '
        '_copyrightLabel
        '
        Me._copyrightLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._copyrightLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._copyrightLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._copyrightLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(148, Byte), Integer))
        Me._copyrightLabel.Location = New System.Drawing.Point(0, 301)
        Me._copyrightLabel.Name = "_copyrightLabel"
        Me._copyrightLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._copyrightLabel.Size = New System.Drawing.Size(525, 35)
        Me._copyrightLabel.TabIndex = 31
        Me._copyrightLabel.Tag = "Copyright"
        Me._copyrightLabel.Text = "Copyright"
        Me._copyrightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me._copyrightLabel.UseMnemonic = False
        '
        '_copyrightWarningLabel
        '
        Me._copyrightWarningLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._copyrightWarningLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._copyrightWarningLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._copyrightWarningLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(148, Byte), Integer))
        Me._copyrightWarningLabel.Location = New System.Drawing.Point(0, 336)
        Me._copyrightWarningLabel.Name = "_copyrightWarningLabel"
        Me._copyrightWarningLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._copyrightWarningLabel.Size = New System.Drawing.Size(525, 35)
        Me._copyrightWarningLabel.TabIndex = 30
        Me._copyrightWarningLabel.Tag = "trademarks"
        Me._copyrightWarningLabel.Text = "trademarks"
        Me._copyrightWarningLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me._copyrightWarningLabel.UseMnemonic = False
        '
        'SplashScreen
        '
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CancelButton = Me._cancelButton
        Me.ClientSize = New System.Drawing.Size(525, 371)
        Me.ControlBox = False
        Me.Controls.Add(Me._rightPanel)
        Me.Controls.Add(Me._leftPanel)
        Me.Controls.Add(Me._statusLabel)
        Me.Controls.Add(Me._copyrightLabel)
        Me.Controls.Add(Me._copyrightWarningLabel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SplashScreen"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me._rightPanel.ResumeLayout(False)
        Me._leftPanel.ResumeLayout(False)
        CType(Me._disksPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._companyLogoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _RightPanel As System.Windows.Forms.Panel
    Private WithEvents _ProductNameLabel As System.Windows.Forms.Label
    Private WithEvents _ProductTitleLabel As System.Windows.Forms.Label
    Private WithEvents _ProductVersionLabel As System.Windows.Forms.Label
    Private WithEvents _LicenseeLabel As System.Windows.Forms.Label
    Private WithEvents _PlatformTitleLabel As System.Windows.Forms.Label
    Private WithEvents _ProductFamilyLabel As System.Windows.Forms.Label
    Private WithEvents _CompanyNameLabel As System.Windows.Forms.Label
    Private WithEvents _LeftPanel As System.Windows.Forms.Panel
    Private WithEvents _DisksPictureBox As System.Windows.Forms.PictureBox
    Private WithEvents _CompanyLogoPictureBox As System.Windows.Forms.PictureBox
    Private WithEvents _StatusLabel As System.Windows.Forms.Label
    Private WithEvents _CopyrightLabel As System.Windows.Forms.Label
    Private WithEvents _CopyrightWarningLabel As System.Windows.Forms.Label
    Private WithEvents _CancelButton As System.Windows.Forms.Button
End Class

﻿Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports isr.Core.Pith.EventHandlerExtensions
Imports isr.Core.Pith.ExceptionExtensions
''' <summary> Non-visual component to show a notification window in the right lower corner of the
''' screen. </summary>
''' <license> (c) 2011 Simon Baer.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="7/11/2014" by="David" revision=""> Created/modified in 2011 by Simon Baer.
''' http://www.codeproject.com/KB/dialog/notificationwindow.aspx
''' Based on the Code Project article by Nicolas Wälti:
''' http://www.codeproject.com/KB/cpp/PopupNotifier.aspx </history>
''' <history date="7/11/2014" by="David" revision=""> Updated. </history>
<ToolboxBitmapAttribute(GetType(PopupNotifier), "Icon.ico"), DefaultEvent("Click")> _
Public Class PopupNotifier
    Inherits Component

    ''' <summary> Event that is raised when the text in the notification window is clicked. </summary>
    Public Event Click As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveClickEventHandler(ByVal value As EventHandler(Of EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.Click, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Event that is raised when the notification window is manually closed. </summary>
    Public Event Close As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveCloseEventHandler(ByVal value As EventHandler(Of EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.Close, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> <c>true</c> if this object is disposed. </summary>
    Private isDisposed As Boolean = False

    ''' <summary> The popup form. </summary>
    Private _PopupForm As PopupNotifierForm

    ''' <summary> The animation timer. </summary>
    Private _AnimationTimer As Timer

    ''' <summary> The wait timer. </summary>
    Private _WaitTimer As Timer

    ''' <summary> <c>true</c> if this object is appearing. </summary>
    Private isAppearing As Boolean = True

    ''' <summary> <c>true</c> if mouse is on. </summary>
    Private mouseIsOn As Boolean = False

    ''' <summary> The maximum position. </summary>
    Private maxPosition As Integer

    ''' <summary> The maximum opacity. </summary>
    Private maxOpacity As Double

    ''' <summary> The position start. </summary>
    Private posStart As Integer

    ''' <summary> The position stop. </summary>
    Private posStop As Integer

    ''' <summary> The opacity start. </summary>
    Private opacityStart As Double

    ''' <summary> The opacity stop. </summary>
    Private opacityStop As Double

    ''' <summary> The software. </summary>
    Private _StopWatch As System.Diagnostics.Stopwatch

#Region " Properties "

    ''' <summary> Color of the window header. </summary>
    ''' <value> The color of the header. </value>
    <Category("Header"), DefaultValue(GetType(Color), "ControlDark"), Description("Color of the window header.")>
    Public Property HeaderColor() As Color

    ''' <summary> Color of the window background. </summary>
    ''' <value> The color of the body. </value>
    <Category("Appearance"), DefaultValue(GetType(Color), "Control"), Description("Color of the window background.")>
    Public Property BodyColor() As Color

    ''' <summary> Color of the title text. </summary>
    ''' <value> The color of the title. </value>
    <Category("Title"), DefaultValue(GetType(Color), "Gray"), Description("Color of the title text.")>
    Public Property TitleColor() As Color

    ''' <summary> Color of the content text. </summary>
    ''' <value> The color of the content. </value>
    <Category("Content"), DefaultValue(GetType(Color), "ControlText"), Description("Color of the content text.")>
    Public Property ContentColor() As Color

    ''' <summary> Color of the window border. </summary>
    ''' <value> The color of the border. </value>
    <Category("Appearance"), DefaultValue(GetType(Color), "WindowFrame"), Description("Color of the window border.")>
    Public Property BorderColor() As Color

    ''' <summary> Border color of the close and options buttons when the mouse is over them. </summary>
    ''' <value> The color of the button border. </value>
    <Category("Buttons"), DefaultValue(GetType(Color), "WindowFrame"), Description("Border color of the close and options buttons when the mouse is over them.")>
    Public Property ButtonBorderColor() As Color

    ''' <summary> Background color of the close and options buttons when the mouse is over them. </summary>
    ''' <value> The color of the button hover. </value>
    <Category("Buttons"), DefaultValue(GetType(Color), "Highlight"), Description("Background color of the close and options buttons when the mouse is over them.")>
    Public Property ButtonHoverColor() As Color

    ''' <summary> Color of the content text when the mouse is hovering over it. </summary>
    ''' <value> The color of the content hover. </value>
    <Category("Content"), DefaultValue(GetType(Color), "HotTrack"), Description("Color of the content text when the mouse is hovering over it.")>
    Public Property ContentHoverColor() As Color

    ''' <summary> Gradient of window background color. </summary>
    ''' <value> The gradient power. </value>
    <Category("Appearance"), DefaultValue(50), Description("Gradient of window background color.")>
    Public Property GradientPower() As Integer

    ''' <summary> Font of the content text. </summary>
    ''' <value> The content font. </value>
    <Category("Content"), Description("Font of the content text.")>
    Public Property ContentFont() As Font

    ''' <summary> Font of the title. </summary>
    ''' <value> The title font. </value>
    <Category("Title"), Description("Font of the title.")>
    Public Property TitleFont() As Font

    ''' <summary> Size of the image. </summary>
    Private _ImageSize As New Size(0, 0)

    ''' <summary> Gets or sets the size of the image. </summary>
    ''' <value> The size of the image. </value>
    <Category("Image"), Description("Size of the icon image.")>
    Public Property ImageSize() As Size
        Get
            If Me._ImageSize.Width = 0 Then
                If Me.Image IsNot Nothing Then
                    Return Me.Image.Size
                Else
                    Return New Size(0, 0)
                End If
            Else
                Return Me._ImageSize
            End If
        End Get
        Set(ByVal value As Size)
            Me._ImageSize = value
        End Set
    End Property

    ''' <summary> Resets the image size. </summary>
    Public Sub ResetImageSize()
        Me._ImageSize = Size.Empty
    End Sub

    ''' <summary> Determine if we should serialize image size. </summary>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    Private Function ShouldSerializeImageSize() As Boolean
        Return ((Not Me._ImageSize.Equals(Size.Empty)))
    End Function

    ''' <summary> Icon image to display. </summary>
    ''' <value> The image. </value>
    <Category("Image"), Description("Icon image to display.")>
    Public Property Image() As Image

    ''' <summary> Whether to show a 'grip' image within the window header. </summary>
    ''' <value> The show grip. </value>
    <Category("Header"), DefaultValue(True), Description("Whether to show a 'grip' image within the window header.")>
    Public Property ShowGrip() As Boolean

    ''' <summary> Whether to scroll the window or only fade it. </summary>
    ''' <value> The scroll. </value>
    <Category("Behavior"), DefaultValue(True), Description("Whether to scroll the window or only fade it.")>
    Public Property Scroll() As Boolean

    ''' <summary> Content text to display. </summary>
    ''' <value> The content text. </value>
    <Category("Content"), Description("Content text to display.")>
    Public Property ContentText() As String

    ''' <summary> Title text to display. </summary>
    ''' <value> The title text. </value>
    <Category("Title"), Description("Title text to display.")>
    Public Property TitleText() As String

    ''' <summary> Padding of title text. </summary>
    ''' <value> The title padding. </value>
    <Category("Title"), Description("Padding of title text.")>
    Public Property TitlePadding() As Padding

    ''' <summary> Resets the title padding. </summary>
    Private Sub ResetTitlePadding()
        Me.TitlePadding = Padding.Empty
    End Sub

    ''' <summary> Determine if we should serialize title padding. </summary>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    Private Function ShouldSerializeTitlePadding() As Boolean
        Return ((Not Me.TitlePadding.Equals(Padding.Empty)))
    End Function

    ''' <summary> Padding of content text. </summary>
    ''' <value> The content padding. </value>
    <Category("Content"), Description("Padding of content text.")>
    Public Property ContentPadding() As Padding

    ''' <summary> Resets the content padding. </summary>
    Private Sub ResetContentPadding()
        ContentPadding = Padding.Empty
    End Sub

    ''' <summary> Determine if we should serialize content padding. </summary>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    Private Function ShouldSerializeContentPadding() As Boolean
        Return ((Not ContentPadding.Equals(Padding.Empty)))
    End Function

    ''' <summary> Padding of icon image. </summary>
    ''' <value> The image padding. </value>
    <Category("Image"), Description("Padding of icon image.")>
    Public Property ImagePadding() As Padding

    ''' <summary> Resets the image padding. </summary>
    Private Sub ResetImagePadding()
        ImagePadding = Padding.Empty
    End Sub

    ''' <summary> Determine if we should serialize image padding. </summary>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    Private Function ShouldSerializeImagePadding() As Boolean
        Return ((Not ImagePadding.Equals(Padding.Empty)))
    End Function

    ''' <summary> Height of window header. </summary>
    ''' <value> The height of the header. </value>
    <Category("Header"), DefaultValue(9), Description("Height of window header.")>
    Public Property HeaderHeight() As Integer

    ''' <summary> Whether to show the close button. </summary>
    ''' <value> The show close button. </value>
    <Category("Buttons"), DefaultValue(True), Description("Whether to show the close button.")>
    Public Property ShowCloseButton() As Boolean

    ''' <summary> Whether to show the options button. </summary>
    ''' <value> The show options button. </value>
    <Category("Buttons"), DefaultValue(False), Description("Whether to show the options button.")>
    Public Property ShowOptionsButton() As Boolean

    ''' <summary> Context menu to open when clicking on the options button. </summary>
    ''' <value> The options menu. </value>
    <Category("Behavior"), Description("Context menu to open when clicking on the options button.")>
    Public Property OptionsMenu() As ContextMenuStrip

    ''' <summary> Time in milliseconds the window is displayed. </summary>
    ''' <value> The delay. </value>
    <Category("Behavior"), DefaultValue(3000), Description("Time in milliseconds the window is displayed.")>
    Public Property Delay() As Integer

    ''' <summary> Time in milliseconds needed to make the window appear or disappear. </summary>
    ''' <value> The animation duration. </value>
    <Category("Behavior"), DefaultValue(1000), Description("Time in milliseconds needed to make the window appear or disappear.")>
    Public Property AnimationDuration() As Integer

    ''' <summary> Interval in milliseconds used to draw the animation. </summary>
    ''' <value> The animation interval. </value>
    <Category("Behavior"), DefaultValue(10), Description("Interval in milliseconds used to draw the animation.")>
    Public Property AnimationInterval() As Integer

    ''' <summary> Size of the window. </summary>
    ''' <value> The size. </value>
    <Category("Appearance"), Description("Size of the window.")>
    Public Property Size() As Size

#End Region

    ''' <summary> Create a new instance of the popup component. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Sub New()
        ' set default values
        Me.HeaderColor = SystemColors.ControlDark
        Me.BodyColor = SystemColors.Control
        Me.TitleColor = System.Drawing.Color.Gray
        Me.ContentColor = SystemColors.ControlText
        Me.BorderColor = SystemColors.WindowFrame
        Me.ButtonBorderColor = SystemColors.WindowFrame
        Me.ButtonHoverColor = SystemColors.Highlight
        Me.ContentHoverColor = SystemColors.HotTrack
        Me.GradientPower = 50
        Me.ContentFont = SystemFonts.DialogFont
        Me.TitleFont = SystemFonts.CaptionFont
        Me.ShowGrip = True
        Me.Scroll = True
        Me.TitlePadding = New Padding(0)
        Me.ContentPadding = New Padding(0)
        Me.ImagePadding = New Padding(0)
        Me.HeaderHeight = 9
        Me.ShowCloseButton = True
        Me.ShowOptionsButton = False
        Me.Delay = 3000
        Me.AnimationInterval = 10
        Me.AnimationDuration = 1000
        Me.Size = New Size(400, 100)

        Me._PopupForm = New PopupNotifierForm(Me) With {
            .TopMost = True,
            .FormBorderStyle = System.Windows.Forms.FormBorderStyle.None,
            .StartPosition = System.Windows.Forms.FormStartPosition.Manual
        }
        AddHandler Me._PopupForm.MouseEnter, AddressOf _PopupForm_MouseEnter
        AddHandler Me._PopupForm.MouseLeave, AddressOf _PopupForm_MouseLeave
        AddHandler Me._PopupForm.CloseClick, AddressOf _PopupForm_CloseClick
        AddHandler Me._PopupForm.LinkClick, AddressOf _PopupForm_LinkClick
        AddHandler Me._PopupForm.ContextMenuOpened, AddressOf _PopupForm_ContextMenuOpened
        AddHandler Me._PopupForm.ContextMenuClosed, AddressOf _PopupForm_ContextMenuClosed

        Me._AnimationTimer = New Timer()
        AddHandler Me._AnimationTimer.Tick, AddressOf _AnimationTimer_Tick

        Me._WaitTimer = New Timer()
        AddHandler Me._WaitTimer.Tick, AddressOf _WaitTimer_Tick
    End Sub

    ''' <summary> Show the notification window if it is not already visible. If the window is currently
    ''' disappearing, it is shown again. </summary>
    Public Sub Popup()
        If Not Me.isDisposed Then
            If Not _PopupForm.Visible Then
                Me._PopupForm.Size = Size
                If Me.Scroll Then
                    Me.posStart = Screen.PrimaryScreen.WorkingArea.Bottom
                    Me.posStop = Screen.PrimaryScreen.WorkingArea.Bottom - Me._PopupForm.Height
                Else
                    Me.posStart = Screen.PrimaryScreen.WorkingArea.Bottom - Me._PopupForm.Height
                    Me.posStop = Screen.PrimaryScreen.WorkingArea.Bottom - Me._PopupForm.Height
                End If
                Me.opacityStart = 0
                Me.opacityStop = 1

                Me._PopupForm.Opacity = Me.opacityStart
                Me._PopupForm.Location = New Point(Screen.PrimaryScreen.WorkingArea.Right - Me._PopupForm.Size.Width - 1, Me.posStart)
                Me._PopupForm.Show()
                Me.isAppearing = True

                Me._WaitTimer.Interval = Me.Delay
                Me._AnimationTimer.Interval = Me.AnimationInterval
                Me._AnimationTimer.Start()
                Me._StopWatch = System.Diagnostics.Stopwatch.StartNew()
                System.Diagnostics.Debug.WriteLine("Animation started.")
            Else
                If Not Me.isAppearing Then
                    Me._PopupForm.Top = Me.maxPosition
                    Me._PopupForm.Opacity = Me.maxOpacity
                    Me._AnimationTimer.Stop()
                    System.Diagnostics.Debug.WriteLine("Animation stopped.")
                    Me._WaitTimer.Stop()
                    Me._WaitTimer.Start()
                    System.Diagnostics.Debug.WriteLine("Wait timer started.")
                End If
                _PopupForm.Invalidate()
            End If
        End If
    End Sub

    ''' <summary> Hide the notification window. </summary>
    Public Sub Hide()
        System.Diagnostics.Debug.WriteLine("Animation stopped.")
        System.Diagnostics.Debug.WriteLine("Wait timer stopped.")
        Me._AnimationTimer.Stop()
        Me._WaitTimer.Stop()
        Me._PopupForm.Hide()
    End Sub

    ''' <summary> The custom options menu has been closed. Restart the timer for closing the
    ''' notification window. </summary>
    ''' <param name="sender"> . </param>
    ''' <param name="e">      . </param>
    Private Sub _PopupForm_ContextMenuClosed(ByVal sender As Object, ByVal e As EventArgs)
        System.Diagnostics.Debug.WriteLine("Menu closed.")
        If Not mouseIsOn Then
            Me._WaitTimer.Interval = Delay
            Me._WaitTimer.Start()
            System.Diagnostics.Debug.WriteLine("Wait timer started.")
        End If
    End Sub

    ''' <summary> The custom options menu has been opened. The window must not be closed as long as the
    ''' menu is open. </summary>
    ''' <param name="sender"> . </param>
    ''' <param name="e">      . </param>
    Private Sub _PopupForm_ContextMenuOpened(ByVal sender As Object, ByVal e As EventArgs)
        System.Diagnostics.Debug.WriteLine("Menu opened.")
        Me._WaitTimer.Stop()
        System.Diagnostics.Debug.WriteLine("Wait timer stopped.")
    End Sub

    ''' <summary> The text has been clicked. Raise the 'Click' event. </summary>
    ''' <param name="sender"> . </param>
    ''' <param name="e">      . </param>
    Private Sub _PopupForm_LinkClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.ClickEvent
        evt?.Invoke(Me, System.EventArgs.Empty)
    End Sub

    ''' <summary> The close button has been clicked. Hide the notification window and raise the 'Close'
    ''' event. </summary>
    ''' <param name="sender"> . </param>
    ''' <param name="e">      . </param>
    Private Sub _PopupForm_CloseClick(ByVal sender As Object, ByVal e As EventArgs)
        Me.Hide()
        Dim evt As EventHandler(Of System.EventArgs) = Me.CloseEvent
        evt?.Invoke(Me, System.EventArgs.Empty)
    End Sub

    ''' <summary> Update form position and opacity to show/hide the window. </summary>
    ''' <param name="sender"> . </param>
    ''' <param name="e">      . </param>
    Private Sub _AnimationTimer_Tick(ByVal sender As Object, ByVal e As EventArgs)

        Dim elapsed As Long = Me._StopWatch.ElapsedMilliseconds

        Dim posCurrent As Integer = CInt(Me.posStart + ((Me.posStop - Me.posStart) * elapsed \ Me.AnimationDuration))
        Dim neg As Boolean = (Me.posStop - Me.posStart) < 0
        If (neg AndAlso posCurrent < Me.posStop) OrElse ((Not neg) AndAlso posCurrent > Me.posStop) Then
            posCurrent = posStop
        End If

        Dim opacityCurrent As Double = Me.opacityStart + (CInt((Me.opacityStop - Me.opacityStart) * elapsed) \ Me.AnimationDuration)
        neg = (Me.opacityStop - Me.opacityStart) < 0
        If (neg AndAlso opacityCurrent < Me.opacityStop) OrElse ((Not neg) AndAlso opacityCurrent > Me.opacityStop) Then
            opacityCurrent = Me.opacityStop
        End If

        Me._PopupForm.Top = posCurrent
        Me._PopupForm.Opacity = opacityCurrent

        ' animation has ended
        If elapsed > Me.AnimationDuration Then
            Dim posTemp As Integer = Me.posStart
            Me.posStart = Me.posStop
            Me.posStop = posTemp

            Dim opacityTemp As Double = Me.opacityStart
            Me.opacityStart = Me.opacityStop
            Me.opacityStop = opacityTemp

            Me._StopWatch.Reset()
            Me._AnimationTimer.Stop()
            System.Diagnostics.Debug.WriteLine("Animation stopped.")

            If Me.isAppearing Then
                Me.isAppearing = False
                Me.maxPosition = Me._PopupForm.Top
                Me.maxOpacity = Me._PopupForm.Opacity
                If Not Me.mouseIsOn Then
                    Me._WaitTimer.Stop()
                    Me._WaitTimer.Start()
                    System.Diagnostics.Debug.WriteLine("Wait timer started.")
                End If
            Else
                Me._PopupForm.Hide()
            End If
        End If
    End Sub

    ''' <summary> The wait timer has elapsed, start the animation to hide the window. </summary>
    ''' <param name="sender"> . </param>
    ''' <param name="e">      . </param>
    Private Sub _WaitTimer_Tick(ByVal sender As Object, ByVal e As EventArgs)
        System.Diagnostics.Debug.WriteLine("Wait timer elapsed.")
        Me._WaitTimer.Stop()
        Me._AnimationTimer.Interval = Me.AnimationInterval
        Me._AnimationTimer.Start()
        Me._StopWatch.Restart()
        System.Diagnostics.Debug.WriteLine("Animation started.")
    End Sub

    ''' <summary> Start wait timer if the mouse leaves the form. </summary>
    ''' <param name="sender"> . </param>
    ''' <param name="e">      . </param>
    Private Sub _PopupForm_MouseLeave(ByVal sender As Object, ByVal e As EventArgs)
        System.Diagnostics.Debug.WriteLine("MouseLeave")
        If _PopupForm.Visible AndAlso (Me.OptionsMenu Is Nothing OrElse (Not Me.OptionsMenu.Visible)) Then
            Me._WaitTimer.Interval = Me.Delay
            Me._WaitTimer.Start()
            System.Diagnostics.Debug.WriteLine("Wait timer started.")
        End If
        mouseIsOn = False
    End Sub

    ''' <summary> Stop wait timer if the mouse enters the form. </summary>
    ''' <param name="sender"> . </param>
    ''' <param name="e">      . </param>
    Private Sub _PopupForm_MouseEnter(ByVal sender As Object, ByVal e As EventArgs)
        System.Diagnostics.Debug.WriteLine("MouseEnter")
        If Not Me.isAppearing Then
            Me._PopupForm.Top = Me.maxPosition
            Me._PopupForm.Opacity = Me.maxOpacity
            Me._AnimationTimer.Stop()
            System.Diagnostics.Debug.WriteLine("Animation stopped.")
        End If

        Me._WaitTimer.Stop()
        System.Diagnostics.Debug.WriteLine("Wait timer stopped.")

        Me.mouseIsOn = True
    End Sub

    ''' <summary> Dispose the notification form. </summary>
    ''' <param name="disposing"> . </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.isDisposed AndAlso disposing Then
                Me.RemoveClickEventHandler(Me.ClickEvent)
                Me.RemoveCloseEventHandler(Me.CloseEvent)
                If Me._PopupForm IsNot Nothing Then Me._PopupForm.Dispose() : Me._PopupForm = Nothing
            End If
        Finally
            Me.isDisposed = True
            MyBase.Dispose(disposing)
        End Try
    End Sub

End Class

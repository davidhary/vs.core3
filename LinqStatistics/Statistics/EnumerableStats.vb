﻿Namespace EnumerableStats

    ''' <summary> An enumerable statistics. </summary>
    ''' <license>
    ''' (c) 2012 Don Kackman. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' 
    ''' </license>
    ''' <history date="7/30/2016" by="David" revision="3.0.6055">
    ''' http://www.codeproject.com/Articles/42492/Using-LINQ-to-Calculate-Basic-Statistics. </history>
    Partial Public Module Methods

        ''' <summary> Enumerates coalesce in this collection. </summary>
        ''' <param name="source"> Source for the. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process coalesce in this collection.
        ''' </returns>
        <System.Runtime.CompilerServices.Extension>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")>
        Public Function Coalesce(Of T As Structure)(ByVal source As IEnumerable(Of T?)) As IEnumerable(Of T)
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            Debug.Assert(source IsNot Nothing)
            Return source.Where(Function(x) x.HasValue).Select(Function(x) CType(x, T))
        End Function
    End Module

End Namespace

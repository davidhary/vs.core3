﻿Imports isr.Core.Controls.ColorExtensions
''' <summary> A toggle switch renderer base. </summary>
''' <license>
''' (c) 2015 Johnny J.. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/4/2015" by="David" revision=""> Created. </history>
Public MustInherit Class ToggleSwitchRendererBase

#Region "Private Members"

    Private _ToggleSwitch As ToggleSwitch

#End Region ' Private Members

#Region "Constructor"

    Protected Sub New()
    End Sub

    Friend Sub SetToggleSwitch(ByVal toggleSwitch As ToggleSwitch)
        _toggleSwitch = toggleSwitch
    End Sub

    Friend ReadOnly Property ToggleSwitch() As ToggleSwitch
        Get
            Return _toggleSwitch
        End Get
    End Property

#End Region ' Constructor

#Region "Render Methods"

    Public Sub RenderBackground(ByVal e As System.Windows.Forms.PaintEventArgs)
        If e Is Nothing Then Return
        If _toggleSwitch Is Nothing Then Return

        e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias

        Dim controlRectangle As New Rectangle(0, 0, _toggleSwitch.Width, _toggleSwitch.Height)

        FillBackground(e.Graphics, controlRectangle)
        RenderBorder(e.Graphics, controlRectangle)
    End Sub

    Public Sub RenderControl(ByVal e As System.Windows.Forms.PaintEventArgs)
        If e Is Nothing Then Return
        If _toggleSwitch Is Nothing Then Return

        e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias

        Dim buttonRectangle As Rectangle = GetButtonRectangle()
        Dim totalToggleFieldWidth As Integer = ToggleSwitch.Width - buttonRectangle.Width

        If buttonRectangle.X > 0 Then
            Dim leftRectangle As New Rectangle(0, 0, buttonRectangle.X, ToggleSwitch.Height)

            If leftRectangle.Width > 0 Then
                RenderLeftToggleField(e.Graphics, leftRectangle, totalToggleFieldWidth)
            End If
        End If

        If buttonRectangle.X + buttonRectangle.Width < e.ClipRectangle.Width Then
            Dim rightRectangle As New Rectangle(buttonRectangle.X + buttonRectangle.Width, 0, ToggleSwitch.Width - buttonRectangle.X - buttonRectangle.Width, ToggleSwitch.Height)

            If rightRectangle.Width > 0 Then
                RenderRightToggleField(e.Graphics, rightRectangle, totalToggleFieldWidth)
            End If
        End If

        RenderButton(e.Graphics, buttonRectangle)
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="g")>
    Public Sub FillBackground(ByVal g As Graphics, ByVal controlRectangle As Rectangle)
        If g Is Nothing Then Return
        Dim backColor As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled,
                                     ToggleSwitch.BackColor.ToGrayscale(),
                                     ToggleSwitch.BackColor)

        Using backBrush As Brush = New SolidBrush(backColor)
            g.FillRectangle(backBrush, controlRectangle)
        End Using
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="g")>
    Public MustOverride Sub RenderBorder(ByVal g As Graphics, ByVal borderRectangle As Rectangle)
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="g")>
    Public MustOverride Sub RenderLeftToggleField(ByVal g As Graphics, ByVal leftRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="g")>
    Public MustOverride Sub RenderRightToggleField(ByVal g As Graphics, ByVal rightRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="g")>
    Public MustOverride Sub RenderButton(ByVal g As Graphics, ByVal buttonRectangle As Rectangle)

#End Region ' Render Methods

#Region "Helper Methods"

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")>
    Public MustOverride Function GetButtonWidth() As Integer
    Public MustOverride Function GetButtonRectangle() As Rectangle
    Public MustOverride Function GetButtonRectangle(ByVal buttonWidth As Integer) As Rectangle

#End Region ' Helper Methods
End Class


﻿Imports System.Drawing.Drawing2D
Imports isr.Core.Controls.ColorExtensions
''' <summary> A toggle switch fancy renderer. </summary>
''' <license>
''' (c) 2015 Johnny J.. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/4/2015" by="David" revision=""> Created. </history>
Public Class ToggleSwitchFancyRenderer
    Inherits ToggleSwitchRendererBase
    Implements IDisposable

#Region "Constructor"

    Private _InnerControlPath As GraphicsPath = Nothing

    Public Sub New()
        OuterBorderColor1 = Color.FromArgb(255, 197, 199, 201)
        OuterBorderColor2 = Color.FromArgb(255, 207, 209, 212)
        InnerBorderColor1 = Color.FromArgb(200, 205, 208, 207)
        InnerBorderColor2 = Color.FromArgb(255, 207, 209, 212)
        LeftSideBackColor1 = Color.FromArgb(255, 61, 110, 6)
        LeftSideBackColor2 = Color.FromArgb(255, 93, 170, 9)
        RightSideBackColor1 = Color.FromArgb(255, 149, 0, 0)
        RightSideBackColor2 = Color.FromArgb(255, 198, 0, 0)
        ButtonNormalBorderColor1 = Color.FromArgb(255, 212, 209, 211)
        ButtonNormalBorderColor2 = Color.FromArgb(255, 197, 199, 201)
        ButtonNormalUpperSurfaceColor1 = Color.FromArgb(255, 252, 251, 252)
        ButtonNormalUpperSurfaceColor2 = Color.FromArgb(255, 247, 247, 247)
        ButtonNormalLowerSurfaceColor1 = Color.FromArgb(255, 233, 233, 233)
        ButtonNormalLowerSurfaceColor2 = Color.FromArgb(255, 225, 225, 225)
        ButtonHoverBorderColor1 = Color.FromArgb(255, 212, 207, 209)
        ButtonHoverBorderColor2 = Color.FromArgb(255, 223, 223, 223)
        ButtonHoverUpperSurfaceColor1 = Color.FromArgb(255, 240, 239, 240)
        ButtonHoverUpperSurfaceColor2 = Color.FromArgb(255, 235, 235, 235)
        ButtonHoverLowerSurfaceColor1 = Color.FromArgb(255, 221, 221, 221)
        ButtonHoverLowerSurfaceColor2 = Color.FromArgb(255, 214, 214, 214)
        ButtonPressedBorderColor1 = Color.FromArgb(255, 176, 176, 176)
        ButtonPressedBorderColor2 = Color.FromArgb(255, 176, 176, 176)
        ButtonPressedUpperSurfaceColor1 = Color.FromArgb(255, 189, 188, 189)
        ButtonPressedUpperSurfaceColor2 = Color.FromArgb(255, 185, 185, 185)
        ButtonPressedLowerSurfaceColor1 = Color.FromArgb(255, 175, 175, 175)
        ButtonPressedLowerSurfaceColor2 = Color.FromArgb(255, 169, 169, 169)
        ButtonShadowColor1 = Color.FromArgb(50, 0, 0, 0)
        ButtonShadowColor2 = Color.FromArgb(0, 0, 0, 0)

        ButtonShadowWidth = 7
        CornerRadius = 6
    End Sub

#Region " Disposable Support "

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks> Do not make this method Overridable (virtual) because a derived class should not be
    ''' able to override this method. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> Gets or sets the dispose status sentinel of the base class.  This applies to the
    ''' derived class provided proper implementation. </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me._innerControlPath?.Dispose() : Me._innerControlPath = Nothing
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

#End Region


#End Region ' Constructor

#Region "Public Properties"

    Public Property OuterBorderColor1() As Color
    Public Property OuterBorderColor2() As Color
    Public Property InnerBorderColor1() As Color
    Public Property InnerBorderColor2() As Color
    Public Property LeftSideBackColor1() As Color
    Public Property LeftSideBackColor2() As Color
    Public Property RightSideBackColor1() As Color
    Public Property RightSideBackColor2() As Color
    Public Property ButtonNormalBorderColor1() As Color
    Public Property ButtonNormalBorderColor2() As Color
    Public Property ButtonNormalUpperSurfaceColor1() As Color
    Public Property ButtonNormalUpperSurfaceColor2() As Color
    Public Property ButtonNormalLowerSurfaceColor1() As Color
    Public Property ButtonNormalLowerSurfaceColor2() As Color
    Public Property ButtonHoverBorderColor1() As Color
    Public Property ButtonHoverBorderColor2() As Color
    Public Property ButtonHoverUpperSurfaceColor1() As Color
    Public Property ButtonHoverUpperSurfaceColor2() As Color
    Public Property ButtonHoverLowerSurfaceColor1() As Color
    Public Property ButtonHoverLowerSurfaceColor2() As Color
    Public Property ButtonPressedBorderColor1() As Color
    Public Property ButtonPressedBorderColor2() As Color
    Public Property ButtonPressedUpperSurfaceColor1() As Color
    Public Property ButtonPressedUpperSurfaceColor2() As Color
    Public Property ButtonPressedLowerSurfaceColor1() As Color
    Public Property ButtonPressedLowerSurfaceColor2() As Color
    Public Property ButtonShadowColor1() As Color
    Public Property ButtonShadowColor2() As Color

    Public Property ButtonShadowWidth() As Integer
    Public Property CornerRadius() As Integer

#End Region ' Public Properties

#Region "Render Method Implementations"

    Public Overrides Sub RenderBorder(ByVal g As Graphics, ByVal borderRectangle As Rectangle)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        'Draw outer border
        Using outerBorderPath As GraphicsPath = GetRoundedRectanglePath(borderRectangle, CornerRadius)
            g.SetClip(outerBorderPath)

            'INSTANT VB NOTE: The variable outerBorderColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
            Dim outerBorderColor1_Renamed As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, OuterBorderColor1.ToGrayscale(), OuterBorderColor1)
            'INSTANT VB NOTE: The variable outerBorderColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
            Dim outerBorderColor2_Renamed As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, OuterBorderColor2.ToGrayscale(), OuterBorderColor2)

            Using outerBorderBrush As Brush = New LinearGradientBrush(borderRectangle, outerBorderColor1_Renamed, outerBorderColor2_Renamed, LinearGradientMode.Vertical)
                g.FillPath(outerBorderBrush, outerBorderPath)
            End Using

            g.ResetClip()
        End Using

        'Draw inner border
        Dim innerborderRectangle As New Rectangle(borderRectangle.X + 1, borderRectangle.Y + 1, borderRectangle.Width - 2, borderRectangle.Height - 2)

        Using innerBorderPath As GraphicsPath = GetRoundedRectanglePath(innerborderRectangle, CornerRadius)
            g.SetClip(innerBorderPath)

            'INSTANT VB NOTE: The variable innerBorderColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
            Dim innerBorderColor1_Renamed As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, InnerBorderColor1.ToGrayscale(), InnerBorderColor1)
            'INSTANT VB NOTE: The variable innerBorderColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
            Dim innerBorderColor2_Renamed As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, InnerBorderColor2.ToGrayscale(), InnerBorderColor2)

            Using borderBrush As Brush = New LinearGradientBrush(borderRectangle, innerBorderColor1_Renamed, innerBorderColor2_Renamed, LinearGradientMode.Vertical)
                g.FillPath(borderBrush, innerBorderPath)
            End Using

            g.ResetClip()
        End Using

        Dim backgroundRectangle As New Rectangle(borderRectangle.X + 2, borderRectangle.Y + 2, borderRectangle.Width - 4, borderRectangle.Height - 4)
        _innerControlPath = GetRoundedRectanglePath(backgroundRectangle, CornerRadius)
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
    Public Overrides Sub RenderLeftToggleField(ByVal g As Graphics, ByVal leftRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        Dim buttonWidth As Integer = GetButtonWidth()

        'Draw inner background
        Dim gradientRectWidth As Integer = leftRectangle.Width + buttonWidth \ 2
        Dim gradientRectangle As New Rectangle(leftRectangle.X, leftRectangle.Y, gradientRectWidth, leftRectangle.Height)

        'INSTANT VB NOTE: The variable leftSideBackColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim leftSideBackColor1_Renamed As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, LeftSideBackColor1.ToGrayscale(), LeftSideBackColor1)
        'INSTANT VB NOTE: The variable leftSideBackColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim leftSideBackColor2_Renamed As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, LeftSideBackColor2.ToGrayscale(), LeftSideBackColor2)

        If _innerControlPath IsNot Nothing Then
            g.SetClip(_innerControlPath)
            g.IntersectClip(gradientRectangle)
        Else
            g.SetClip(gradientRectangle)
        End If

        Using backgroundBrush As Brush = New LinearGradientBrush(gradientRectangle, leftSideBackColor1_Renamed, leftSideBackColor2_Renamed, LinearGradientMode.Vertical)
            g.FillRectangle(backgroundBrush, gradientRectangle)
        End Using

        g.ResetClip()

        Dim leftShadowRectangle As New Rectangle() With {
            .X = leftRectangle.X + leftRectangle.Width - ButtonShadowWidth,
            .Y = leftRectangle.Y,
            .Width = ButtonShadowWidth + CornerRadius,
            .Height = leftRectangle.Height}

        If _innerControlPath IsNot Nothing Then
            g.SetClip(_innerControlPath)
            g.IntersectClip(leftShadowRectangle)
        Else
            g.SetClip(leftShadowRectangle)
        End If

        Using buttonShadowBrush As Brush = New LinearGradientBrush(leftShadowRectangle, ButtonShadowColor2, ButtonShadowColor1, LinearGradientMode.Horizontal)
            g.FillRectangle(buttonShadowBrush, leftShadowRectangle)
        End Using

        g.ResetClip()

        'Draw image or text
        If ToggleSwitch.OnSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(ToggleSwitch.OnText)) Then
            Dim fullRectangle As New RectangleF(leftRectangle.X + 1 - (totalToggleFieldWidth - leftRectangle.Width), 1, totalToggleFieldWidth - 1, ToggleSwitch.Height - 2)

            If _innerControlPath IsNot Nothing Then
                g.SetClip(_innerControlPath)
                g.IntersectClip(fullRectangle)
            Else
                g.SetClip(fullRectangle)
            End If

            If ToggleSwitch.OnSideImage IsNot Nothing Then
                Dim imageSize As Size = ToggleSwitch.OnSideImage.Size
                Dim imageRectangle As Rectangle

                Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                If ToggleSwitch.OnSideScaleImageToFit Then
                    Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                    Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                    If ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                    ElseIf ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                    If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle, 0, 0, ToggleSwitch.OnSideImage.Width, ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle)
                    End If
                Else
                    If ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                    ElseIf ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                    If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle, 0, 0, ToggleSwitch.OnSideImage.Width, ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImageUnscaled(ToggleSwitch.OnSideImage, imageRectangle)
                    End If
                End If
            ElseIf Not String.IsNullOrEmpty(ToggleSwitch.OnText) Then
                Dim textSize As SizeF = g.MeasureString(ToggleSwitch.OnText, ToggleSwitch.OnFont)

                Dim textXPos As Single = fullRectangle.X

                If ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                    textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                ElseIf ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                    textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                End If

                Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                Dim textForeColor As Color = ToggleSwitch.OnForeColor

                If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                    textForeColor = textForeColor.ToGrayscale()
                End If

                Using textBrush As Brush = New SolidBrush(textForeColor)
                    g.DrawString(ToggleSwitch.OnText, ToggleSwitch.OnFont, textBrush, textRectangle)
                End Using
            End If

            g.ResetClip()
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
    Public Overrides Sub RenderRightToggleField(ByVal g As Graphics, ByVal rightRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        Dim buttonWidth As Integer = GetButtonWidth()

        'Draw inner background
        Dim gradientRectWidth As Integer = rightRectangle.Width + buttonWidth \ 2
        Dim gradientRectangle As New Rectangle(rightRectangle.X - buttonWidth \ 2, rightRectangle.Y, gradientRectWidth, rightRectangle.Height)

        'INSTANT VB NOTE: The variable rightSideBackColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim rightSideBackColor1_Renamed As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, RightSideBackColor1.ToGrayscale(), RightSideBackColor1)
        'INSTANT VB NOTE: The variable rightSideBackColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim rightSideBackColor2_Renamed As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, RightSideBackColor2.ToGrayscale(), RightSideBackColor2)

        If _innerControlPath IsNot Nothing Then
            g.SetClip(_innerControlPath)
            g.IntersectClip(gradientRectangle)
        Else
            g.SetClip(gradientRectangle)
        End If

        Using backgroundBrush As Brush = New LinearGradientBrush(gradientRectangle, rightSideBackColor1_Renamed, rightSideBackColor2_Renamed, LinearGradientMode.Vertical)
            g.FillRectangle(backgroundBrush, gradientRectangle)
        End Using

        g.ResetClip()

        Dim rightShadowRectangle As New Rectangle() With {
            .X = rightRectangle.X - CornerRadius,
            .Y = rightRectangle.Y,
            .Width = ButtonShadowWidth + CornerRadius,
            .Height = rightRectangle.Height}

        If _innerControlPath IsNot Nothing Then
            g.SetClip(_innerControlPath)
            g.IntersectClip(rightShadowRectangle)
        Else
            g.SetClip(rightShadowRectangle)
        End If

        Using buttonShadowBrush As Brush = New LinearGradientBrush(rightShadowRectangle, ButtonShadowColor1, ButtonShadowColor2, LinearGradientMode.Horizontal)
            g.FillRectangle(buttonShadowBrush, rightShadowRectangle)
        End Using

        g.ResetClip()

        'Draw image or text
        If ToggleSwitch.OffSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(ToggleSwitch.OffText)) Then
            Dim fullRectangle As New RectangleF(rightRectangle.X, 1, totalToggleFieldWidth - 1, ToggleSwitch.Height - 2)

            If _innerControlPath IsNot Nothing Then
                g.SetClip(_innerControlPath)
                g.IntersectClip(fullRectangle)
            Else
                g.SetClip(fullRectangle)
            End If

            If ToggleSwitch.OffSideImage IsNot Nothing Then
                Dim imageSize As Size = ToggleSwitch.OffSideImage.Size
                Dim imageRectangle As Rectangle

                Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                If ToggleSwitch.OffSideScaleImageToFit Then
                    Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                    Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                    If ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                    ElseIf ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                    If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle, 0, 0, ToggleSwitch.OnSideImage.Width, ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle)
                    End If
                Else
                    If ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                    ElseIf ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                    If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle, 0, 0, ToggleSwitch.OnSideImage.Width, ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImageUnscaled(ToggleSwitch.OffSideImage, imageRectangle)
                    End If
                End If
            ElseIf Not String.IsNullOrEmpty(ToggleSwitch.OffText) Then
                Dim textSize As SizeF = g.MeasureString(ToggleSwitch.OffText, ToggleSwitch.OffFont)

                Dim textXPos As Single = fullRectangle.X

                If ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                    textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                ElseIf ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                    textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                End If

                Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                Dim textForeColor As Color = ToggleSwitch.OffForeColor

                If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                    textForeColor = textForeColor.ToGrayscale()
                End If

                Using textBrush As Brush = New SolidBrush(textForeColor)
                    g.DrawString(ToggleSwitch.OffText, ToggleSwitch.OffFont, textBrush, textRectangle)
                End Using
            End If

            g.ResetClip()
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
    Public Overrides Sub RenderButton(ByVal g As Graphics, ByVal buttonRectangle As Rectangle)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        'Draw button surface
        Dim buttonUpperSurfaceColor1 As Color = ButtonNormalUpperSurfaceColor1
        Dim buttonUpperSurfaceColor2 As Color = ButtonNormalUpperSurfaceColor2
        Dim buttonLowerSurfaceColor1 As Color = ButtonNormalLowerSurfaceColor1
        Dim buttonLowerSurfaceColor2 As Color = ButtonNormalLowerSurfaceColor2

        If ToggleSwitch.IsButtonPressed Then
            buttonUpperSurfaceColor1 = ButtonPressedUpperSurfaceColor1
            buttonUpperSurfaceColor2 = ButtonPressedUpperSurfaceColor2
            buttonLowerSurfaceColor1 = ButtonPressedLowerSurfaceColor1
            buttonLowerSurfaceColor2 = ButtonPressedLowerSurfaceColor2
        ElseIf ToggleSwitch.IsButtonHovered Then
            buttonUpperSurfaceColor1 = ButtonHoverUpperSurfaceColor1
            buttonUpperSurfaceColor2 = ButtonHoverUpperSurfaceColor2
            buttonLowerSurfaceColor1 = ButtonHoverLowerSurfaceColor1
            buttonLowerSurfaceColor2 = ButtonHoverLowerSurfaceColor2
        End If

        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
            buttonUpperSurfaceColor1 = buttonUpperSurfaceColor1.ToGrayscale()
            buttonUpperSurfaceColor2 = buttonUpperSurfaceColor2.ToGrayscale()
            buttonLowerSurfaceColor1 = buttonLowerSurfaceColor1.ToGrayscale()
            buttonLowerSurfaceColor2 = buttonLowerSurfaceColor2.ToGrayscale()
        End If

        buttonRectangle.Inflate(-1, -1)

        Dim upperHeight As Integer = buttonRectangle.Height \ 2

        Dim upperGradientRect As New Rectangle(buttonRectangle.X, buttonRectangle.Y, buttonRectangle.Width, upperHeight)
        Dim lowerGradientRect As New Rectangle(buttonRectangle.X, buttonRectangle.Y + upperHeight, buttonRectangle.Width, buttonRectangle.Height - upperHeight)

        Using buttonPath As GraphicsPath = GetRoundedRectanglePath(buttonRectangle, CornerRadius)
            g.SetClip(buttonPath)
            g.IntersectClip(upperGradientRect)

            'Draw upper button surface gradient
            Using buttonUpperSurfaceBrush As Brush = New LinearGradientBrush(buttonRectangle, buttonUpperSurfaceColor1, buttonUpperSurfaceColor2, LinearGradientMode.Vertical)
                g.FillPath(buttonUpperSurfaceBrush, buttonPath)
            End Using

            g.ResetClip()

            g.SetClip(buttonPath)
            g.IntersectClip(lowerGradientRect)

            'Draw lower button surface gradient
            Using buttonLowerSurfaceBrush As Brush = New LinearGradientBrush(buttonRectangle, buttonLowerSurfaceColor1, buttonLowerSurfaceColor2, LinearGradientMode.Vertical)
                g.FillPath(buttonLowerSurfaceBrush, buttonPath)
            End Using

            g.ResetClip()

            g.SetClip(buttonPath)

            'Draw button border
            Dim buttonBorderColor1 As Color = ButtonNormalBorderColor1
            Dim buttonBorderColor2 As Color = ButtonNormalBorderColor2

            If ToggleSwitch.IsButtonPressed Then
                buttonBorderColor1 = ButtonPressedBorderColor1
                buttonBorderColor2 = ButtonPressedBorderColor2
            ElseIf ToggleSwitch.IsButtonHovered Then
                buttonBorderColor1 = ButtonHoverBorderColor1
                buttonBorderColor2 = ButtonHoverBorderColor2
            End If

            If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                buttonBorderColor1 = buttonBorderColor1.ToGrayscale()
                buttonBorderColor2 = buttonBorderColor2.ToGrayscale()
            End If

            Using buttonBorderBrush As Brush = New LinearGradientBrush(buttonRectangle, buttonBorderColor1, buttonBorderColor2, LinearGradientMode.Vertical)
                Using buttonBorderPen As New Pen(buttonBorderBrush)
                    g.DrawPath(buttonBorderPen, buttonPath)
                End Using
            End Using

            g.ResetClip()

            'Draw button image
            Dim buttonImage As Image = If(ToggleSwitch.ButtonImage, (If(ToggleSwitch.Checked, ToggleSwitch.OnButtonImage, ToggleSwitch.OffButtonImage)))

            If buttonImage IsNot Nothing Then
                g.SetClip(buttonPath)

                Dim alignment As ToggleSwitchButtonAlignment = If(ToggleSwitch.ButtonImage IsNot Nothing, ToggleSwitch.ButtonAlignment, (If(ToggleSwitch.Checked, ToggleSwitch.OnButtonAlignment, ToggleSwitch.OffButtonAlignment)))

                Dim imageSize As Size = buttonImage.Size

                Dim imageRectangle As Rectangle

                Dim imageXPos As Integer = buttonRectangle.X

                Dim scaleImage As Boolean = If(ToggleSwitch.ButtonImage IsNot Nothing, ToggleSwitch.ButtonScaleImageToFit, (If(ToggleSwitch.Checked, ToggleSwitch.OnButtonScaleImageToFit, ToggleSwitch.OffButtonScaleImageToFit)))

                If scaleImage Then
                    Dim canvasSize As Size = buttonRectangle.Size
                    Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                    If alignment = ToggleSwitchButtonAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(buttonRectangle.X) + ((CSng(buttonRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                    ElseIf alignment = ToggleSwitchButtonAlignment.Right Then
                        imageXPos = CInt(Fix(CSng(buttonRectangle.X) + CSng(buttonRectangle.Width) - CSng(resizedImageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(buttonRectangle.Y) + ((CSng(buttonRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                    If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImage(buttonImage, imageRectangle)
                    End If
                Else
                    If alignment = ToggleSwitchButtonAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(buttonRectangle.X) + ((CSng(buttonRectangle.Width) - CSng(imageSize.Width)) / 2)))
                    ElseIf alignment = ToggleSwitchButtonAlignment.Right Then
                        imageXPos = CInt(Fix(CSng(buttonRectangle.X) + CSng(buttonRectangle.Width) - CSng(imageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(buttonRectangle.Y) + ((CSng(buttonRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                    If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImageUnscaled(buttonImage, imageRectangle)
                    End If
                End If

                g.ResetClip()
            End If
        End Using
    End Sub

#End Region ' Render Method Implementations

#Region "Helper Method Implementations"

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Function GetRoundedRectanglePath(ByVal rectangle As Rectangle, ByVal radius As Integer) As GraphicsPath
        Dim gp As New GraphicsPath()
        Dim diameter As Integer = 2 * radius

        If diameter > ToggleSwitch.Height Then
            diameter = ToggleSwitch.Height
        End If

        If diameter > ToggleSwitch.Width Then
            diameter = ToggleSwitch.Width
        End If

        gp.AddArc(rectangle.X, rectangle.Y, diameter, diameter, 180, 90)
        gp.AddArc(rectangle.X + rectangle.Width - diameter, rectangle.Y, diameter, diameter, 270, 90)
        gp.AddArc(rectangle.X + rectangle.Width - diameter, rectangle.Y + rectangle.Height - diameter, diameter, diameter, 0, 90)
        gp.AddArc(rectangle.X, rectangle.Y + rectangle.Height - diameter, diameter, diameter, 90, 90)
        gp.CloseFigure()

        Return gp
    End Function

    Public Overrides Function GetButtonWidth() As Integer
        Dim buttonWidth As Single = 1.61F * ToggleSwitch.Height
        Return CInt(Fix(buttonWidth))
    End Function

    Public Overrides Function GetButtonRectangle() As Rectangle
        Dim buttonWidth As Integer = GetButtonWidth()
        Return GetButtonRectangle(buttonWidth)
    End Function

    Public Overrides Function GetButtonRectangle(ByVal buttonWidth As Integer) As Rectangle
        Dim buttonRect As New Rectangle(ToggleSwitch.ButtonValue, 0, buttonWidth, ToggleSwitch.Height)
        Return buttonRect
    End Function

#End Region ' Helper Method Implementations
End Class

﻿Imports isr.Core.Controls.ColorExtensions
''' <summary> A toggle switch metro renderer. </summary>
''' <license>
''' ((c) 2015 Johnny J.. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/4/2015" by="David" revision=""> Created. </history>
Public Class ToggleSwitchMetroRenderer
    Inherits ToggleSwitchRendererBase

#Region "Constructor"

    Public Sub New()
        BackColor = Color.White
        LeftSideColor = Color.FromArgb(255, 23, 153, 0)
        LeftSideColorHovered = Color.FromArgb(255, 36, 182, 9)
        LeftSideColorPressed = Color.FromArgb(255, 121, 245, 100)
        RightSideColor = Color.FromArgb(255, 166, 166, 166)
        RightSideColorHovered = Color.FromArgb(255, 181, 181, 181)
        RightSideColorPressed = Color.FromArgb(255, 189, 189, 189)
        BorderColor = Color.FromArgb(255, 166, 166, 166)
        ButtonColor = Color.Black
        ButtonColorHovered = Color.Black
        ButtonColorPressed = Color.Black
    End Sub

#End Region ' Constructor

#Region "Public Properties"

    Public Property BackColor() As Color
    Public Property LeftSideColor() As Color
    Public Property LeftSideColorHovered() As Color
    Public Property LeftSideColorPressed() As Color
    Public Property RightSideColor() As Color
    Public Property RightSideColorHovered() As Color
    Public Property RightSideColorPressed() As Color
    Public Property BorderColor() As Color
    Public Property ButtonColor() As Color
    Public Property ButtonColorHovered() As Color
    Public Property ButtonColorPressed() As Color

#End Region ' Public Properties

#Region "Render Method Implementations"

    Public Overrides Sub RenderBorder(ByVal g As Graphics, ByVal borderRectangle As Rectangle)
        If g Is Nothing Then Return
        Dim renderColor As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, BorderColor.ToGrayscale(), BorderColor)

        g.SetClip(borderRectangle)

        Using borderPen As New Pen(renderColor)
            g.DrawRectangle(borderPen, borderRectangle.X, borderRectangle.Y, borderRectangle.Width - 1, borderRectangle.Height - 1)
        End Using

        g.ResetClip()
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
    Public Overrides Sub RenderLeftToggleField(ByVal g As Graphics, ByVal leftRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        Dim adjustedLeftRect As New Rectangle(leftRectangle.X + 2, 2, leftRectangle.Width - 2, leftRectangle.Height - 4)

        If adjustedLeftRect.Width > 0 Then
            Dim leftColor As Color = LeftSideColor

            If ToggleSwitch.IsLeftSidePressed Then
                leftColor = LeftSideColorPressed
            ElseIf ToggleSwitch.IsLeftSideHovered Then
                leftColor = LeftSideColorHovered
            End If

            If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                leftColor = leftColor.ToGrayscale()
            End If

            g.SetClip(adjustedLeftRect)

            Using leftBrush As Brush = New SolidBrush(leftColor)
                g.FillRectangle(leftBrush, adjustedLeftRect)
            End Using

            If ToggleSwitch.OnSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(ToggleSwitch.OnText)) Then
                Dim fullRectangle As New RectangleF(leftRectangle.X + 2 - (totalToggleFieldWidth - leftRectangle.Width), 2, totalToggleFieldWidth - 2, ToggleSwitch.Height - 4)

                g.IntersectClip(fullRectangle)

                If ToggleSwitch.OnSideImage IsNot Nothing Then
                    Dim imageSize As Size = ToggleSwitch.OnSideImage.Size
                    Dim imageRectangle As Rectangle

                    Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                    If ToggleSwitch.OnSideScaleImageToFit Then
                        Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                        Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                        If ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                        ElseIf ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                        End If

                        imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                            g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle, 0, 0, ToggleSwitch.OnSideImage.Width, ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                        Else
                            g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle)
                        End If
                    Else
                        If ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                        ElseIf ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                        End If

                        imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                            g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle, 0, 0, ToggleSwitch.OnSideImage.Width, ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                        Else
                            g.DrawImageUnscaled(ToggleSwitch.OnSideImage, imageRectangle)
                        End If
                    End If
                ElseIf Not String.IsNullOrEmpty(ToggleSwitch.OnText) Then
                    Dim textSize As SizeF = g.MeasureString(ToggleSwitch.OnText, ToggleSwitch.OnFont)

                    Dim textXPos As Single = fullRectangle.X

                    If ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                        textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                    ElseIf ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                        textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                    End If

                    Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                    Dim textForeColor As Color = ToggleSwitch.OnForeColor

                    If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                        textForeColor = textForeColor.ToGrayscale()
                    End If

                    Using textBrush As Brush = New SolidBrush(textForeColor)
                        g.DrawString(ToggleSwitch.OnText, ToggleSwitch.OnFont, textBrush, textRectangle)
                    End Using
                End If
            End If

            g.ResetClip()
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
    Public Overrides Sub RenderRightToggleField(ByVal g As Graphics, ByVal rightRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        Dim adjustedRightRect As New Rectangle(rightRectangle.X, 2, rightRectangle.Width - 2, rightRectangle.Height - 4)

        If adjustedRightRect.Width > 0 Then
            Dim rightColor As Color = RightSideColor

            If ToggleSwitch.IsRightSidePressed Then
                rightColor = RightSideColorPressed
            ElseIf ToggleSwitch.IsRightSideHovered Then
                rightColor = RightSideColorHovered
            End If

            If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                rightColor = rightColor.ToGrayscale()
            End If

            g.SetClip(adjustedRightRect)

            Using rightBrush As Brush = New SolidBrush(rightColor)
                g.FillRectangle(rightBrush, adjustedRightRect)
            End Using

            If ToggleSwitch.OffSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(ToggleSwitch.OffText)) Then
                Dim fullRectangle As New RectangleF(rightRectangle.X, 2, totalToggleFieldWidth - 2, ToggleSwitch.Height - 4)

                g.IntersectClip(fullRectangle)

                If ToggleSwitch.OffSideImage IsNot Nothing Then
                    Dim imageSize As Size = ToggleSwitch.OffSideImage.Size
                    Dim imageRectangle As Rectangle

                    Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                    If ToggleSwitch.OffSideScaleImageToFit Then
                        Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                        Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                        If ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                        ElseIf ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                        End If

                        imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                            g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle, 0, 0, ToggleSwitch.OnSideImage.Width, ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                        Else
                            g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle)
                        End If
                    Else
                        If ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                        ElseIf ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                        End If

                        imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                            g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle, 0, 0, ToggleSwitch.OnSideImage.Width, ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                        Else
                            g.DrawImageUnscaled(ToggleSwitch.OffSideImage, imageRectangle)
                        End If
                    End If
                ElseIf Not String.IsNullOrEmpty(ToggleSwitch.OffText) Then
                    Dim textSize As SizeF = g.MeasureString(ToggleSwitch.OffText, ToggleSwitch.OffFont)

                    Dim textXPos As Single = fullRectangle.X

                    If ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                        textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                    ElseIf ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                        textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                    End If

                    Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                    Dim textForeColor As Color = ToggleSwitch.OffForeColor

                    If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                        textForeColor = textForeColor.ToGrayscale()
                    End If

                    Using textBrush As Brush = New SolidBrush(textForeColor)
                        g.DrawString(ToggleSwitch.OffText, ToggleSwitch.OffFont, textBrush, textRectangle)
                    End Using
                End If
            End If
        End If
    End Sub

    Public Overrides Sub RenderButton(ByVal g As Graphics, ByVal buttonRectangle As Rectangle)
        If g Is Nothing Then Return
        Dim renderColor As Color = ButtonColor

        If ToggleSwitch.IsButtonPressed Then
            renderColor = ButtonColorPressed
        ElseIf ToggleSwitch.IsButtonHovered Then
            renderColor = ButtonColorHovered
        End If

        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
            renderColor = renderColor.ToGrayscale()
        End If

        g.SetClip(buttonRectangle)

        Using backBrush As Brush = New SolidBrush(renderColor)
            g.FillRectangle(backBrush, buttonRectangle)
        End Using

        g.ResetClip()
    End Sub

#End Region ' Render Method Implementations

#Region "Helper Method Implementations"

    Public Overrides Function GetButtonWidth() As Integer
        Return CInt(CDbl(ToggleSwitch.Height) / 3 * 2)
    End Function

    Public Overrides Function GetButtonRectangle() As Rectangle
        Dim buttonWidth As Integer = GetButtonWidth()
        Return GetButtonRectangle(buttonWidth)
    End Function

    Public Overrides Function GetButtonRectangle(ByVal buttonWidth As Integer) As Rectangle
        Dim buttonRect As New Rectangle(ToggleSwitch.ButtonValue, 0, buttonWidth, ToggleSwitch.Height)
        Return buttonRect
    End Function

#End Region ' Helper Method Implementations
End Class


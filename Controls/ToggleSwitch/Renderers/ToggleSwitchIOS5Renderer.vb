﻿Imports System.Drawing.Drawing2D
Imports isr.Core.Controls.ColorExtensions
''' <summary> A toggle switch ios 5 renderer. </summary>
''' <license>
''' (c) 2015 Johnny J.. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/4/2015" by="David" revision=""> Created. </history>
Public Class ToggleSwitchIos5Renderer
    Inherits ToggleSwitchRendererBase

#Region "Constructor"

    Public Sub New()
        BorderColor = Color.FromArgb(255, 202, 202, 202)
        LeftSideUpperColor1 = Color.FromArgb(255, 48, 115, 189)
        LeftSideUpperColor2 = Color.FromArgb(255, 17, 123, 220)
        LeftSideLowerColor1 = Color.FromArgb(255, 65, 143, 218)
        LeftSideLowerColor2 = Color.FromArgb(255, 130, 190, 243)
        LeftSideUpperBorderColor = Color.FromArgb(150, 123, 157, 196)
        LeftSideLowerBorderColor = Color.FromArgb(150, 174, 208, 241)
        RightSideUpperColor1 = Color.FromArgb(255, 191, 191, 191)
        RightSideUpperColor2 = Color.FromArgb(255, 229, 229, 229)
        RightSideLowerColor1 = Color.FromArgb(255, 232, 232, 232)
        RightSideLowerColor2 = Color.FromArgb(255, 251, 251, 251)
        RightSideUpperBorderColor = Color.FromArgb(150, 175, 175, 175)
        RightSideLowerBorderColor = Color.FromArgb(150, 229, 230, 233)
        ButtonShadowColor = Color.Transparent
        ButtonNormalOuterBorderColor = Color.FromArgb(255, 149, 172, 194)
        ButtonNormalInnerBorderColor = Color.FromArgb(255, 235, 235, 235)
        ButtonNormalSurfaceColor1 = Color.FromArgb(255, 216, 215, 216)
        ButtonNormalSurfaceColor2 = Color.FromArgb(255, 251, 250, 251)
        ButtonHoverOuterBorderColor = Color.FromArgb(255, 141, 163, 184)
        ButtonHoverInnerBorderColor = Color.FromArgb(255, 223, 223, 223)
        ButtonHoverSurfaceColor1 = Color.FromArgb(255, 205, 204, 205)
        ButtonHoverSurfaceColor2 = Color.FromArgb(255, 239, 238, 239)
        ButtonPressedOuterBorderColor = Color.FromArgb(255, 111, 128, 145)
        ButtonPressedInnerBorderColor = Color.FromArgb(255, 176, 176, 176)
        ButtonPressedSurfaceColor1 = Color.FromArgb(255, 162, 161, 162)
        ButtonPressedSurfaceColor2 = Color.FromArgb(255, 187, 187, 187)
    End Sub

#End Region ' Constructor

#Region "Public Properties"

    Public Property BorderColor() As Color
    Public Property LeftSideUpperColor1() As Color
    Public Property LeftSideUpperColor2() As Color
    Public Property LeftSideLowerColor1() As Color
    Public Property LeftSideLowerColor2() As Color
    Public Property LeftSideUpperBorderColor() As Color
    Public Property LeftSideLowerBorderColor() As Color
    Public Property RightSideUpperColor1() As Color
    Public Property RightSideUpperColor2() As Color
    Public Property RightSideLowerColor1() As Color
    Public Property RightSideLowerColor2() As Color
    Public Property RightSideUpperBorderColor() As Color
    Public Property RightSideLowerBorderColor() As Color
    Public Property ButtonShadowColor() As Color
    Public Property ButtonNormalOuterBorderColor() As Color
    Public Property ButtonNormalInnerBorderColor() As Color
    Public Property ButtonNormalSurfaceColor1() As Color
    Public Property ButtonNormalSurfaceColor2() As Color
    Public Property ButtonHoverOuterBorderColor() As Color
    Public Property ButtonHoverInnerBorderColor() As Color
    Public Property ButtonHoverSurfaceColor1() As Color
    Public Property ButtonHoverSurfaceColor2() As Color
    Public Property ButtonPressedOuterBorderColor() As Color
    Public Property ButtonPressedInnerBorderColor() As Color
    Public Property ButtonPressedSurfaceColor1() As Color
    Public Property ButtonPressedSurfaceColor2() As Color

#End Region ' Public Properties

#Region "Render Method Implementations"

    Public Overrides Sub RenderBorder(ByVal g As Graphics, ByVal borderRectangle As Rectangle)
        'Draw this one AFTER the button is drawn in the RenderButton method
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
    Public Overrides Sub RenderLeftToggleField(ByVal g As Graphics, ByVal leftRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality

        Dim buttonWidth As Integer = GetButtonWidth()

        'Draw upper gradient field
        Dim gradientRectWidth As Integer = leftRectangle.Width + buttonWidth \ 2
        Dim upperGradientRectHeight As Integer = CInt(Fix(CDbl(0.8) * CDbl(leftRectangle.Height - 2)))

        Dim controlRectangle As New Rectangle(0, 0, ToggleSwitch.Width, ToggleSwitch.Height)
        Dim controlClipPath As GraphicsPath = GetControlClipPath(controlRectangle)

        Dim upperGradientRectangle As New Rectangle(leftRectangle.X, leftRectangle.Y + 1, gradientRectWidth, upperGradientRectHeight - 1)

        g.SetClip(controlClipPath)
        g.IntersectClip(upperGradientRectangle)

        Using upperGradientPath As New GraphicsPath()
            upperGradientPath.AddArc(upperGradientRectangle.X, upperGradientRectangle.Y, ToggleSwitch.Height, ToggleSwitch.Height, 135, 135)
            upperGradientPath.AddLine(upperGradientRectangle.X, upperGradientRectangle.Y, upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y)
            upperGradientPath.AddLine(upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y, upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y + upperGradientRectangle.Height)
            upperGradientPath.AddLine(upperGradientRectangle.X, upperGradientRectangle.Y + upperGradientRectangle.Height, upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y + upperGradientRectangle.Height)

            Dim upperColor1 As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, LeftSideUpperColor1.ToGrayscale(), LeftSideUpperColor1)
            Dim upperColor2 As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, LeftSideUpperColor2.ToGrayscale(), LeftSideUpperColor2)

            Using upperGradientBrush As Brush = New LinearGradientBrush(upperGradientRectangle, upperColor1, upperColor2, LinearGradientMode.Vertical)
                g.FillPath(upperGradientBrush, upperGradientPath)
            End Using
        End Using

        g.ResetClip()

        'Draw lower gradient field
        Dim lowerGradientRectHeight As Integer = CInt(Fix(Math.Ceiling(CDbl(0.5) * CDbl(leftRectangle.Height - 2))))

        Dim lowerGradientRectangle As New Rectangle(leftRectangle.X, leftRectangle.Y + (leftRectangle.Height \ 2), gradientRectWidth, lowerGradientRectHeight)

        g.SetClip(controlClipPath)
        g.IntersectClip(lowerGradientRectangle)

        Using lowerGradientPath As New GraphicsPath()
            lowerGradientPath.AddArc(1, lowerGradientRectangle.Y, CInt(Fix(0.75 * (ToggleSwitch.Height - 1))), ToggleSwitch.Height - 1, 215, 45) 'Arc from side to top
            lowerGradientPath.AddLine(lowerGradientRectangle.X + buttonWidth \ 2, lowerGradientRectangle.Y, lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y)
            lowerGradientPath.AddLine(lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y, lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y + lowerGradientRectangle.Height)
            lowerGradientPath.AddLine(lowerGradientRectangle.X + buttonWidth \ 4, lowerGradientRectangle.Y + lowerGradientRectangle.Height, lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y + lowerGradientRectangle.Height)
            lowerGradientPath.AddArc(1, 1, ToggleSwitch.Height - 1, ToggleSwitch.Height - 1, 90, 70) 'Arc from side to bottom

            Dim lowerColor1 As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, LeftSideLowerColor1.ToGrayscale(), LeftSideLowerColor1)
            Dim lowerColor2 As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, LeftSideLowerColor2.ToGrayscale(), LeftSideLowerColor2)

            Using lowerGradientBrush As Brush = New LinearGradientBrush(lowerGradientRectangle, lowerColor1, lowerColor2, LinearGradientMode.Vertical)
                g.FillPath(lowerGradientBrush, lowerGradientPath)
            End Using
        End Using

        g.ResetClip()

        controlRectangle = New Rectangle(0, 0, ToggleSwitch.Width, ToggleSwitch.Height)
        controlClipPath = GetControlClipPath(controlRectangle)

        g.SetClip(controlClipPath)

        'Draw upper inside border
        Dim upperBordercolor As Color = LeftSideUpperBorderColor

        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
            upperBordercolor = upperBordercolor.ToGrayscale()
        End If

        Using upperBorderPen As New Pen(upperBordercolor)
            g.DrawLine(upperBorderPen, leftRectangle.X, leftRectangle.Y + 1, leftRectangle.X + leftRectangle.Width + (buttonWidth \ 2), leftRectangle.Y + 1)
        End Using

        'Draw lower inside border
        Dim lowerBordercolor As Color = LeftSideLowerBorderColor

        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
            lowerBordercolor = lowerBordercolor.ToGrayscale()
        End If

        Using lowerBorderPen As New Pen(lowerBordercolor)
            g.DrawLine(lowerBorderPen, leftRectangle.X, leftRectangle.Y + leftRectangle.Height - 1, leftRectangle.X + leftRectangle.Width + (buttonWidth \ 2), leftRectangle.Y + leftRectangle.Height - 1)
        End Using

        'Draw image or text
        If ToggleSwitch.OnSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(ToggleSwitch.OnText)) Then
            Dim fullRectangle As New RectangleF(leftRectangle.X + 2 - (totalToggleFieldWidth - leftRectangle.Width), 2, totalToggleFieldWidth - 2, ToggleSwitch.Height - 4)

            g.IntersectClip(fullRectangle)

            If ToggleSwitch.OnSideImage IsNot Nothing Then
                Dim imageSize As Size = ToggleSwitch.OnSideImage.Size
                Dim imageRectangle As Rectangle

                Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                If ToggleSwitch.OnSideScaleImageToFit Then
                    Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                    Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                    If ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                    ElseIf ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                    If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle, 0, 0, ToggleSwitch.OnSideImage.Width, ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle)
                    End If
                Else
                    If ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                    ElseIf ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                    If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle, 0, 0, ToggleSwitch.OnSideImage.Width, ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImageUnscaled(ToggleSwitch.OnSideImage, imageRectangle)
                    End If
                End If
            ElseIf Not String.IsNullOrEmpty(ToggleSwitch.OnText) Then
                Dim textSize As SizeF = g.MeasureString(ToggleSwitch.OnText, ToggleSwitch.OnFont)

                Dim textXPos As Single = fullRectangle.X

                If ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                    textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                ElseIf ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                    textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                End If

                Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                Dim textForeColor As Color = ToggleSwitch.OnForeColor

                If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                    textForeColor = textForeColor.ToGrayscale()
                End If

                Using textBrush As Brush = New SolidBrush(textForeColor)
                    g.DrawString(ToggleSwitch.OnText, ToggleSwitch.OnFont, textBrush, textRectangle)
                End Using
            End If
        End If

        g.ResetClip()
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
    Public Overrides Sub RenderRightToggleField(ByVal g As Graphics, ByVal rightRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality

        Dim buttonRectangle As Rectangle = GetButtonRectangle()

        Dim controlRectangle As New Rectangle(0, 0, ToggleSwitch.Width, ToggleSwitch.Height)
        Dim controlClipPath As GraphicsPath = GetControlClipPath(controlRectangle)

        'Draw upper gradient field
        Dim gradientRectWidth As Integer = rightRectangle.Width + buttonRectangle.Width \ 2
        Dim upperGradientRectHeight As Integer = CInt(Fix(CDbl(0.8) * CDbl(rightRectangle.Height - 2)))

        Dim upperGradientRectangle As New Rectangle(rightRectangle.X - buttonRectangle.Width \ 2, rightRectangle.Y + 1, gradientRectWidth - 1, upperGradientRectHeight - 1)

        g.SetClip(controlClipPath)
        g.IntersectClip(upperGradientRectangle)

        Using upperGradientPath As New GraphicsPath()
            upperGradientPath.AddLine(upperGradientRectangle.X, upperGradientRectangle.Y, upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y)
            upperGradientPath.AddArc(upperGradientRectangle.X + upperGradientRectangle.Width - ToggleSwitch.Height + 1, upperGradientRectangle.Y - 1, ToggleSwitch.Height, ToggleSwitch.Height, 270, 115)
            upperGradientPath.AddLine(upperGradientRectangle.X + upperGradientRectangle.Width, upperGradientRectangle.Y + upperGradientRectangle.Height, upperGradientRectangle.X, upperGradientRectangle.Y + upperGradientRectangle.Height)
            upperGradientPath.AddLine(upperGradientRectangle.X, upperGradientRectangle.Y + upperGradientRectangle.Height, upperGradientRectangle.X, upperGradientRectangle.Y)

            Dim upperColor1 As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, RightSideUpperColor1.ToGrayscale(), RightSideUpperColor1)
            Dim upperColor2 As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, RightSideUpperColor2.ToGrayscale(), RightSideUpperColor2)

            Using upperGradientBrush As Brush = New LinearGradientBrush(upperGradientRectangle, upperColor1, upperColor2, LinearGradientMode.Vertical)
                g.FillPath(upperGradientBrush, upperGradientPath)
            End Using
        End Using

        g.ResetClip()

        'Draw lower gradient field
        Dim lowerGradientRectHeight As Integer = CInt(Fix(Math.Ceiling(CDbl(0.5) * CDbl(rightRectangle.Height - 2))))

        Dim lowerGradientRectangle As New Rectangle(rightRectangle.X - buttonRectangle.Width \ 2, rightRectangle.Y + (rightRectangle.Height \ 2), gradientRectWidth - 1, lowerGradientRectHeight)

        g.SetClip(controlClipPath)
        g.IntersectClip(lowerGradientRectangle)

        Using lowerGradientPath As New GraphicsPath()
            lowerGradientPath.AddLine(lowerGradientRectangle.X, lowerGradientRectangle.Y, lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y)
            lowerGradientPath.AddArc(lowerGradientRectangle.X + lowerGradientRectangle.Width - CInt(Fix(0.75 * (ToggleSwitch.Height - 1))), lowerGradientRectangle.Y, CInt(Fix(0.75 * (ToggleSwitch.Height - 1))), ToggleSwitch.Height - 1, 270, 45) 'Arc from top to side
            lowerGradientPath.AddArc(ToggleSwitch.Width - ToggleSwitch.Height, 0, ToggleSwitch.Height, ToggleSwitch.Height, 20, 70) 'Arc from side to bottom
            lowerGradientPath.AddLine(lowerGradientRectangle.X + lowerGradientRectangle.Width, lowerGradientRectangle.Y + lowerGradientRectangle.Height, lowerGradientRectangle.X, lowerGradientRectangle.Y + lowerGradientRectangle.Height)
            lowerGradientPath.AddLine(lowerGradientRectangle.X, lowerGradientRectangle.Y + lowerGradientRectangle.Height, lowerGradientRectangle.X, lowerGradientRectangle.Y)

            Dim lowerColor1 As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, RightSideLowerColor1.ToGrayscale(), RightSideLowerColor1)
            Dim lowerColor2 As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, RightSideLowerColor2.ToGrayscale(), RightSideLowerColor2)

            Using lowerGradientBrush As Brush = New LinearGradientBrush(lowerGradientRectangle, lowerColor1, lowerColor2, LinearGradientMode.Vertical)
                g.FillPath(lowerGradientBrush, lowerGradientPath)
            End Using
        End Using

        g.ResetClip()

        controlRectangle = New Rectangle(0, 0, ToggleSwitch.Width, ToggleSwitch.Height)
        controlClipPath = GetControlClipPath(controlRectangle)

        g.SetClip(controlClipPath)

        'Draw upper inside border
        Dim upperBordercolor As Color = RightSideUpperBorderColor

        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
            upperBordercolor = upperBordercolor.ToGrayscale()
        End If

        Using upperBorderPen As New Pen(upperBordercolor)
            g.DrawLine(upperBorderPen, rightRectangle.X - (buttonRectangle.Width \ 2), rightRectangle.Y + 1, rightRectangle.X + rightRectangle.Width, rightRectangle.Y + 1)
        End Using

        'Draw lower inside border
        Dim lowerBordercolor As Color = RightSideLowerBorderColor

        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
            lowerBordercolor = lowerBordercolor.ToGrayscale()
        End If

        Using lowerBorderPen As New Pen(lowerBordercolor)
            g.DrawLine(lowerBorderPen, rightRectangle.X - (buttonRectangle.Width \ 2), rightRectangle.Y + rightRectangle.Height - 1, rightRectangle.X + rightRectangle.Width, rightRectangle.Y + rightRectangle.Height - 1)
        End Using

        'Draw image or text
        If ToggleSwitch.OffSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(ToggleSwitch.OffText)) Then
            Dim fullRectangle As New RectangleF(rightRectangle.X, 2, totalToggleFieldWidth - 2, ToggleSwitch.Height - 4)

            g.IntersectClip(fullRectangle)

            If ToggleSwitch.OffSideImage IsNot Nothing Then
                Dim imageSize As Size = ToggleSwitch.OffSideImage.Size
                Dim imageRectangle As Rectangle

                Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                If ToggleSwitch.OffSideScaleImageToFit Then
                    Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                    Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                    If ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                    ElseIf ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                    If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle, 0, 0, ToggleSwitch.OnSideImage.Width, ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle)
                    End If
                Else
                    If ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                    ElseIf ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                        imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                    If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle, 0, 0, ToggleSwitch.OnSideImage.Width, ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImageUnscaled(ToggleSwitch.OffSideImage, imageRectangle)
                    End If
                End If
            ElseIf Not String.IsNullOrEmpty(ToggleSwitch.OffText) Then
                Dim textSize As SizeF = g.MeasureString(ToggleSwitch.OffText, ToggleSwitch.OffFont)

                Dim textXPos As Single = fullRectangle.X

                If ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                    textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                ElseIf ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                    textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                End If

                Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                Dim textForeColor As Color = ToggleSwitch.OffForeColor

                If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                    textForeColor = textForeColor.ToGrayscale()
                End If

                Using textBrush As Brush = New SolidBrush(textForeColor)
                    g.DrawString(ToggleSwitch.OffText, ToggleSwitch.OffFont, textBrush, textRectangle)
                End Using
            End If
        End If

        g.ResetClip()
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
    Public Overrides Sub RenderButton(ByVal g As Graphics, ByVal buttonRectangle As Rectangle)
        If g Is Nothing Then Return
        If ToggleSwitch.IsButtonOnLeftSide Then
            buttonRectangle.X += 1
        ElseIf ToggleSwitch.IsButtonOnRightSide Then
            buttonRectangle.X -= 1
        End If

        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality

        'Draw button shadow
        buttonRectangle.Inflate(1, 1)

        Dim shadowClipRectangle As New Rectangle(buttonRectangle.Location, buttonRectangle.Size)
        shadowClipRectangle.Inflate(0, -1)

        If ToggleSwitch.IsButtonOnLeftSide Then
            shadowClipRectangle.X += shadowClipRectangle.Width \ 2
            shadowClipRectangle.Width = shadowClipRectangle.Width \ 2
        ElseIf ToggleSwitch.IsButtonOnRightSide Then
            shadowClipRectangle.Width = shadowClipRectangle.Width \ 2
        End If

        g.SetClip(shadowClipRectangle)

        'INSTANT VB NOTE: The variable buttonShadowColor was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim buttonShadowColor_Renamed As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, ButtonShadowColor.ToGrayscale(), ButtonShadowColor)

        Using buttonShadowPen As New Pen(buttonShadowColor_Renamed)
            g.DrawEllipse(buttonShadowPen, buttonRectangle)
        End Using

        g.ResetClip()

        buttonRectangle.Inflate(-1, -1)

        'Draw outer button border
        Dim buttonOuterBorderColor As Color = ButtonNormalOuterBorderColor

        If ToggleSwitch.IsButtonPressed Then
            buttonOuterBorderColor = ButtonPressedOuterBorderColor
        ElseIf ToggleSwitch.IsButtonHovered Then
            buttonOuterBorderColor = ButtonHoverOuterBorderColor
        End If

        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
            buttonOuterBorderColor = buttonOuterBorderColor.ToGrayscale()
        End If

        Using outerBorderBrush As Brush = New SolidBrush(buttonOuterBorderColor)
            g.FillEllipse(outerBorderBrush, buttonRectangle)
        End Using

        'Draw inner button border
        buttonRectangle.Inflate(-1, -1)

        Dim buttonInnerBorderColor As Color = ButtonNormalInnerBorderColor

        If ToggleSwitch.IsButtonPressed Then
            buttonInnerBorderColor = ButtonPressedInnerBorderColor
        ElseIf ToggleSwitch.IsButtonHovered Then
            buttonInnerBorderColor = ButtonHoverInnerBorderColor
        End If

        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
            buttonInnerBorderColor = buttonInnerBorderColor.ToGrayscale()
        End If

        Using innerBorderBrush As Brush = New SolidBrush(buttonInnerBorderColor)
            g.FillEllipse(innerBorderBrush, buttonRectangle)
        End Using

        'Draw button surface
        buttonRectangle.Inflate(-1, -1)

        Dim buttonUpperSurfaceColor As Color = ButtonNormalSurfaceColor1

        If ToggleSwitch.IsButtonPressed Then
            buttonUpperSurfaceColor = ButtonPressedSurfaceColor1
        ElseIf ToggleSwitch.IsButtonHovered Then
            buttonUpperSurfaceColor = ButtonHoverSurfaceColor1
        End If

        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
            buttonUpperSurfaceColor = buttonUpperSurfaceColor.ToGrayscale()
        End If

        Dim buttonLowerSurfaceColor As Color = ButtonNormalSurfaceColor2

        If ToggleSwitch.IsButtonPressed Then
            buttonLowerSurfaceColor = ButtonPressedSurfaceColor2
        ElseIf ToggleSwitch.IsButtonHovered Then
            buttonLowerSurfaceColor = ButtonHoverSurfaceColor2
        End If

        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
            buttonLowerSurfaceColor = buttonLowerSurfaceColor.ToGrayscale()
        End If

        Using buttonSurfaceBrush As Brush = New LinearGradientBrush(buttonRectangle, buttonUpperSurfaceColor, buttonLowerSurfaceColor, LinearGradientMode.Vertical)
            g.FillEllipse(buttonSurfaceBrush, buttonRectangle)
        End Using

        g.CompositingMode = CompositingMode.SourceOver
        g.CompositingQuality = CompositingQuality.HighQuality

        'Draw outer control border
        Dim controlRectangle As New Rectangle(0, 0, ToggleSwitch.Width, ToggleSwitch.Height)

        Using borderPath As GraphicsPath = GetControlClipPath(controlRectangle)
            Dim controlBorderColor As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, BorderColor.ToGrayscale(), BorderColor)

            Using borderPen As New Pen(controlBorderColor)
                g.DrawPath(borderPen, borderPath)
            End Using
        End Using

        g.ResetClip()

        'Draw button image
        Dim buttonImage As Image = If(ToggleSwitch.ButtonImage, (If(ToggleSwitch.Checked, ToggleSwitch.OnButtonImage, ToggleSwitch.OffButtonImage)))

        If buttonImage IsNot Nothing Then
            g.SetClip(GetButtonClipPath())

            Dim alignment As ToggleSwitchButtonAlignment = If(ToggleSwitch.ButtonImage IsNot Nothing, ToggleSwitch.ButtonAlignment, (If(ToggleSwitch.Checked, ToggleSwitch.OnButtonAlignment, ToggleSwitch.OffButtonAlignment)))

            Dim imageSize As Size = buttonImage.Size

            Dim imageRectangle As Rectangle

            Dim imageXPos As Integer = buttonRectangle.X

            Dim scaleImage As Boolean = If(ToggleSwitch.ButtonImage IsNot Nothing, ToggleSwitch.ButtonScaleImageToFit, (If(ToggleSwitch.Checked, ToggleSwitch.OnButtonScaleImageToFit, ToggleSwitch.OffButtonScaleImageToFit)))

            If scaleImage Then
                Dim canvasSize As Size = buttonRectangle.Size
                Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                If alignment = ToggleSwitchButtonAlignment.Center Then
                    imageXPos = CInt(Fix(CSng(buttonRectangle.X) + ((CSng(buttonRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                ElseIf alignment = ToggleSwitchButtonAlignment.Right Then
                    imageXPos = CInt(Fix(CSng(buttonRectangle.X) + CSng(buttonRectangle.Width) - CSng(resizedImageSize.Width)))
                End If

                imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(buttonRectangle.Y) + ((CSng(buttonRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                    g.DrawImage(buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                Else
                    g.DrawImage(buttonImage, imageRectangle)
                End If
            Else
                If alignment = ToggleSwitchButtonAlignment.Center Then
                    imageXPos = CInt(Fix(CSng(buttonRectangle.X) + ((CSng(buttonRectangle.Width) - CSng(imageSize.Width)) / 2)))
                ElseIf alignment = ToggleSwitchButtonAlignment.Right Then
                    imageXPos = CInt(Fix(CSng(buttonRectangle.X) + CSng(buttonRectangle.Width) - CSng(imageSize.Width)))
                End If

                imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(buttonRectangle.Y) + ((CSng(buttonRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                    g.DrawImage(buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                Else
                    g.DrawImageUnscaled(buttonImage, imageRectangle)
                End If
            End If

            g.ResetClip()
        End If
    End Sub

#End Region ' Render Method Implementations

#Region "Helper Method Implementations"

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Shared Function GetControlClipPath(ByVal controlRectangle As Rectangle) As GraphicsPath
        Dim borderPath As New GraphicsPath()
        borderPath.AddArc(controlRectangle.X, controlRectangle.Y, controlRectangle.Height, controlRectangle.Height, 90, 180)
        borderPath.AddArc(controlRectangle.Width - controlRectangle.Height, controlRectangle.Y, controlRectangle.Height, controlRectangle.Height, 270, 180)
        borderPath.CloseFigure()
        Return borderPath
    End Function

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")>
    Public Function GetButtonClipPath() As GraphicsPath
        Dim buttonRectangle As Rectangle = GetButtonRectangle()

        Dim buttonPath As New GraphicsPath()

        buttonPath.AddArc(buttonRectangle.X, buttonRectangle.Y, buttonRectangle.Height, buttonRectangle.Height, 0, 360)

        Return buttonPath
    End Function

    Public Overrides Function GetButtonWidth() As Integer
        Return ToggleSwitch.Height - 2
    End Function

    Public Overrides Function GetButtonRectangle() As Rectangle
        Dim buttonWidth As Integer = GetButtonWidth()
        Return GetButtonRectangle(buttonWidth)
    End Function

    Public Overrides Function GetButtonRectangle(ByVal buttonWidth As Integer) As Rectangle
        Dim buttonRect As New Rectangle(ToggleSwitch.ButtonValue, 1, buttonWidth, buttonWidth)
        Return buttonRect
    End Function

#End Region ' Helper Method Implementations
End Class

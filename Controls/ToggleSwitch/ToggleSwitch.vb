﻿Imports System.ComponentModel
Imports isr.Core.Pith.EventHandlerExtensions
Imports isr.Core.Pith.ExceptionExtensions
''' <summary> A toggle switch. </summary>
''' <license> (c) 2015 Johnny J.. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="9/11/2015" by="Johnny J." revision="1.0.0.0"> 
'''   http://www.codeproject.com/Articles/1029499/ToggleSwitch-Winforms-Control. </history>
<DefaultValue("Checked"), DefaultEvent("CheckedChanged"), ToolboxBitmap(GetType(CheckBox))>
Public Class ToggleSwitch
    Inherits Control

#Region "Constructor Etc."

    Public Sub New()

        MyBase.New

        Me.SetStyle(ControlStyles.ResizeRedraw Or ControlStyles.SupportsTransparentBackColor Or
                    ControlStyles.AllPaintingInWmPaint Or ControlStyles.UserPaint Or
                    ControlStyles.OptimizedDoubleBuffer Or ControlStyles.DoubleBuffer, True)

        Me._onFont = MyBase.Font
        Me._offFont = MyBase.Font

        Me._SetRenderer(New ToggleSwitchMetroRenderer())

        _animationTimer.Enabled = False
        _animationTimer.Interval = _animationInterval
        AddHandler _animationTimer.Tick, AddressOf AnimationTimer_Tick
    End Sub

    Private Sub _SetRenderer(ByVal renderer As ToggleSwitchRendererBase)
        renderer?.SetToggleSwitch(Me)
        _renderer = renderer
        If _renderer IsNot Nothing Then
            Refresh()
        End If
    End Sub

    Public Sub SetRenderer(ByVal renderer As ToggleSwitchRendererBase)
        Me._SetRenderer(renderer)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveCheckedChangedEventHandler(Me.CheckedChangedEvent)
                If Me._offFont IsNot Nothing Then Me._offFont.Dispose() : Me._offFont = Nothing
                If Me._onFont IsNot Nothing Then Me._onFont.Dispose() : Me._offFont = Nothing
                If Me._animationTimer IsNot Nothing Then Me._animationTimer.Dispose()
                If Me._buttonImage IsNot Nothing Then Me._buttonImage.Dispose() : Me._buttonImage = Nothing
                If Me._offButtonImage IsNot Nothing Then Me._offButtonImage.Dispose() : Me._offButtonImage = Nothing
                If Me._offSideImage IsNot Nothing Then Me._offSideImage.Dispose() : Me._offSideImage = Nothing
                If Me._onButtonImage IsNot Nothing Then Me._onButtonImage.Dispose() : Me._onButtonImage = Nothing
                If Me._onSideImage IsNot Nothing Then Me._onSideImage.Dispose() : Me._onSideImage = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region ' Constructor Etc.

#Region "Public Properties"

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods")>
    <Bindable(False), DefaultValue(GetType(ToggleSwitchStyle), "Metro"), Category("Appearance"), Description("Gets or sets the style of the ToggleSwitch")>
    Public Property Style() As ToggleSwitchStyle
        Get
            Return _style
        End Get
        Set(ByVal value As ToggleSwitchStyle)
            If value <> _style Then
                _style = value

                Select Case _style
                    Case ToggleSwitchStyle.Metro
                        SetRenderer(New ToggleSwitchMetroRenderer())
                    Case ToggleSwitchStyle.Android
                        SetRenderer(New ToggleSwitchAndroidRenderer())
                    Case ToggleSwitchStyle.Ios5
                        SetRenderer(New ToggleSwitchIos5Renderer())
                    Case ToggleSwitchStyle.BrushedMetal
                        SetRenderer(New ToggleSwitchBrushedMetalRenderer())
                    Case ToggleSwitchStyle.OS10
                        SetRenderer(New ToggleSwitchOS10Renderer())
                    Case ToggleSwitchStyle.Carbon
                        SetRenderer(New ToggleSwitchCarbonRenderer())
                    Case ToggleSwitchStyle.IPhone
                        SetRenderer(New ToggleSwitchIPhoneRenderer())
                    Case ToggleSwitchStyle.Fancy
                        SetRenderer(New ToggleSwitchFancyRenderer())
                    Case ToggleSwitchStyle.Modern
                        SetRenderer(New ToggleSwitchModernRenderer())
                End Select
            End If

            Refresh()
        End Set
    End Property

    <Bindable(True), DefaultValue(False), Category("Data"), Description("Gets or sets the Checked value of the ToggleSwitch")>
    Public Property Checked() As Boolean
        Get
            Return _checked
        End Get
        Set(ByVal value As Boolean)
            If value <> _checked Then
                Do While _animating
                    Application.DoEvents()
                Loop

                If value = True Then
                    Dim buttonWidth As Integer = _renderer.GetButtonWidth()
                    _animationTarget = Width - buttonWidth
                    BeginAnimation(True)
                Else
                    _animationTarget = 0
                    BeginAnimation(False)
                End If
            End If
        End Set
    End Property

    <Bindable(True), DefaultValue(True), Category("Behavior"), Description("Gets or sets whether the user can change the value of the button or not")>
    Public Property AllowUserChange() As Boolean
        Get
            Return _allowUserChange
        End Get
        Set(ByVal value As Boolean)
            _allowUserChange = value
        End Set
    End Property

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property CheckedString() As String
        Get
            Return If(Checked, (If(String.IsNullOrEmpty(OnText), "ON", OnText)), (If(String.IsNullOrEmpty(OffText), "OFF", OffText)))
        End Get
    End Property

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property ButtonRectangle() As Rectangle
        Get
            Return _renderer.GetButtonRectangle()
        End Get
    End Property

    <Bindable(False), DefaultValue(True), Category("Appearance"), Description("Gets or sets if the ToggleSwitch should be grayed out when disabled")>
    Public Property GrayWhenDisabled() As Boolean
        Get
            Return _grayWhenDisabled
        End Get
        Set(ByVal value As Boolean)
            If value <> _grayWhenDisabled Then
                _grayWhenDisabled = value

                If Not Enabled Then
                    Refresh()
                End If
            End If
        End Set
    End Property

    <Bindable(False), DefaultValue(True), Category("Behavior"), Description("Gets or sets if the ToggleSwitch should toggle when the button is clicked")>
    Public Property ToggleOnButtonClick() As Boolean
        Get
            Return _toggleOnButtonClick
        End Get
        Set(ByVal value As Boolean)
            _toggleOnButtonClick = value
        End Set
    End Property

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="OnSide")>
    <Bindable(False), DefaultValue(True), Category("Behavior"), Description("Gets or sets if the ToggleSwitch should toggle when the track besides the button is clicked")>
    Public Property ToggleOnSideClick() As Boolean
        Get
            Return _toggleOnSideClick
        End Get
        Set(ByVal value As Boolean)
            _toggleOnSideClick = value
        End Set
    End Property

    <Bindable(False), DefaultValue(50), Category("Behavior"), Description("Gets or sets how much the button need to be on the other side (in percent) before it snaps")>
    Public Property ThresholdPercentage() As Integer
        Get
            Return _thresholdPercentage
        End Get
        Set(ByVal value As Integer)
            _thresholdPercentage = value
        End Set
    End Property

    <Bindable(False), DefaultValue(GetType(Color), "Black"), Category("Appearance"), Description("Gets or sets the fore-color of the text when Checked=false")>
    Public Property OffForeColor() As Color
        Get
            Return _offForeColor
        End Get
        Set(ByVal value As Color)
            If value <> _offForeColor Then
                _offForeColor = value
                Me.Refresh()
            End If
        End Set
    End Property

    <Bindable(False), DefaultValue(GetType(Font), "Microsoft Sans Serif, 8.25pt"), Category("Appearance"), Description("Gets or sets the font of the text when Checked=false")>
    Public Property OffFont() As Font
        Get
            Return _offFont
        End Get
        Set(ByVal value As Font)
            If Not Font.Equals(value, Me.OffFont) Then
                _offFont = value
                Me.Refresh()
            End If
        End Set
    End Property

    <Bindable(False), DefaultValue(""), Category("Appearance"), Description("Gets or sets the text when Checked=true")>
    Public Property OffText() As String
        Get
            Return _offText
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, _offText) Then
                _offText = value
                Refresh()
            End If
        End Set
    End Property

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="OffSide")>
    <Bindable(False), System.ComponentModel.DefaultValue(CType(Nothing, Object)), Category("Appearance"), Description("Gets or sets the side image when Checked=false - Note: Settings the OffSideImage overrules the OffText property. Only the image will be shown")>
    Public Property OffSideImage() As Image
        Get
            Return _offSideImage
        End Get
        Set(ByVal value As Image)
            If Not Image.Equals(value, Me.OffSideImage) Then
                _offSideImage = value
                Refresh()
            End If
        End Set
    End Property

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="OffSide")>
    <Bindable(False), DefaultValue(False), Category("Behavior"), Description("Gets or sets whether the side image visible when Checked=false should be scaled to fit")>
    Public Property OffSideScaleImageToFit() As Boolean
        Get
            Return _offSideScaleImage
        End Get
        Set(ByVal value As Boolean)
            If value <> _offSideScaleImage Then
                _offSideScaleImage = value
                Refresh()
            End If
        End Set
    End Property

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="OffSide")>
    <Bindable(False), DefaultValue(GetType(ToggleSwitchAlignment), "Center"), Category("Appearance"), Description("Gets or sets how the text or side image visible when Checked=false should be aligned")>
    Public Property OffSideAlignment() As ToggleSwitchAlignment
        Get
            Return _offSideAlignment
        End Get
        Set(ByVal value As ToggleSwitchAlignment)
            If value <> _offSideAlignment Then
                _offSideAlignment = value
                Refresh()
            End If
        End Set
    End Property

    <Bindable(False), System.ComponentModel.DefaultValue(CType(Nothing, Object)), Category("Appearance"), Description("Gets or sets the button image when Checked=false and ButtonImage is not set")>
    Public Property OffButtonImage() As Image
        Get
            Return _offButtonImage
        End Get
        Set(ByVal value As Image)
            If Not Image.Equals(value, Me.OffButtonImage) Then
                _offButtonImage = value
                Refresh()
            End If
        End Set
    End Property

    <Bindable(False), DefaultValue(False), Category("Behavior"), Description("Gets or sets whether the button image visible when Checked=false should be scaled to fit")>
    Public Property OffButtonScaleImageToFit() As Boolean
        Get
            Return _offButtonScaleImage
        End Get
        Set(ByVal value As Boolean)
            If value <> _offButtonScaleImage Then
                _offButtonScaleImage = value
                Refresh()
            End If
        End Set
    End Property

    <Bindable(False), DefaultValue(GetType(ToggleSwitchButtonAlignment), "Center"), Category("Appearance"), Description("Gets or sets how the button image visible when Checked=false should be aligned")>
    Public Property OffButtonAlignment() As ToggleSwitchButtonAlignment
        Get
            Return _offButtonAlignment
        End Get
        Set(ByVal value As ToggleSwitchButtonAlignment)
            If value <> _offButtonAlignment Then
                _offButtonAlignment = value
                Refresh()
            End If
        End Set
    End Property

    <Bindable(False), DefaultValue(GetType(Color), "Black"), Category("Appearance"), Description("Gets or sets the fore-color of the text when Checked=true")>
    Public Property OnForeColor() As Color
        Get
            Return _onForeColor
        End Get
        Set(ByVal value As Color)
            If value <> _onForeColor Then
                _onForeColor = value
                Refresh()
            End If
        End Set
    End Property

    <Bindable(False), DefaultValue(GetType(Font), "Microsoft Sans Serif, 8,25pt"), Category("Appearance"), Description("Gets or sets the font of the text when Checked=true")>
    Public Property OnFont() As Font
        Get
            Return _onFont
        End Get
        Set(ByVal value As Font)
            If Not Font.Equals(value, Me.OnFont) Then
                _onFont = value
                Refresh()
            End If
        End Set
    End Property

    <Bindable(False), DefaultValue(""), Category("Appearance"), Description("Gets or sets the text when Checked=true")>
    Public Property OnText() As String
        Get
            Return _onText
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, Me.OnText) Then
                _onText = value
                Refresh()
            End If
        End Set
    End Property

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="OnSide")>
    <Bindable(False), System.ComponentModel.DefaultValue(CType(Nothing, Object)), Category("Appearance"), Description("Gets or sets the side image visible when Checked=true - Note: Settings the OnSideImage overrules the OnText property. Only the image will be shown.")>
    Public Property OnSideImage() As Image
        Get
            Return _onSideImage
        End Get
        Set(ByVal value As Image)
            If value IsNot _onSideImage Then
                _onSideImage = value
                Refresh()
            End If
        End Set
    End Property

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="OnSide")>
    <Bindable(False), DefaultValue(False), Category("Behavior"), Description("Gets or sets whether the side image visible when Checked=true should be scaled to fit")>
    Public Property OnSideScaleImageToFit() As Boolean
        Get
            Return _onSideScaleImage
        End Get
        Set(ByVal value As Boolean)
            If value <> _onSideScaleImage Then
                _onSideScaleImage = value
                Refresh()
            End If
        End Set
    End Property

    <Bindable(False), System.ComponentModel.DefaultValue(CType(Nothing, Object)), Category("Appearance"), Description("Gets or sets the button image")>
    Public Property ButtonImage() As Image
        Get
            Return _buttonImage
        End Get
        Set(ByVal value As Image)
            If value IsNot _buttonImage Then
                _buttonImage = value
                Refresh()
            End If
        End Set
    End Property

    <Bindable(False), DefaultValue(False), Category("Behavior"), Description("Gets or sets whether the button image should be scaled to fit")>
    Public Property ButtonScaleImageToFit() As Boolean
        Get
            Return _buttonScaleImage
        End Get
        Set(ByVal value As Boolean)
            If value <> _buttonScaleImage Then
                _buttonScaleImage = value
                Refresh()
            End If
        End Set
    End Property

    <Bindable(False), DefaultValue(GetType(ToggleSwitchButtonAlignment), "Center"), Category("Appearance"), Description("Gets or sets how the button image should be aligned")>
    Public Property ButtonAlignment() As ToggleSwitchButtonAlignment
        Get
            Return _buttonAlignment
        End Get
        Set(ByVal value As ToggleSwitchButtonAlignment)
            If value <> _buttonAlignment Then
                _buttonAlignment = value
                Refresh()
            End If
        End Set
    End Property

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="OnSide")>
    <Bindable(False), DefaultValue(GetType(ToggleSwitchAlignment), "Center"), Category("Appearance"), Description("Gets or sets how the text or side image visible when Checked=true should be aligned")>
    Public Property OnSideAlignment() As ToggleSwitchAlignment
        Get
            Return _onSideAlignment
        End Get
        Set(ByVal value As ToggleSwitchAlignment)
            If value <> _onSideAlignment Then
                _onSideAlignment = value
                Refresh()
            End If
        End Set
    End Property

    <Bindable(False), System.ComponentModel.DefaultValue(CType(Nothing, Object)), Category("Appearance"), Description("Gets or sets the button image visible when Checked=true and ButtonImage is not set")>
    Public Property OnButtonImage() As Image
        Get
            Return _onButtonImage
        End Get
        Set(ByVal value As Image)
            If value IsNot _onButtonImage Then
                _onButtonImage = value
                Refresh()
            End If
        End Set
    End Property

    <Bindable(False), DefaultValue(False), Category("Behavior"), Description("Gets or sets whether the button image visible when Checked=true should be scaled to fit")>
    Public Property OnButtonScaleImageToFit() As Boolean
        Get
            Return _onButtonScaleImage
        End Get
        Set(ByVal value As Boolean)
            If value <> _onButtonScaleImage Then
                _onButtonScaleImage = value
                Refresh()
            End If
        End Set
    End Property

    <Bindable(False), DefaultValue(GetType(ToggleSwitchButtonAlignment), "Center"), Category("Appearance"), Description("Gets or sets how the button image visible when Checked=true should be aligned")>
    Public Property OnButtonAlignment() As ToggleSwitchButtonAlignment
        Get
            Return _onButtonAlignment
        End Get
        Set(ByVal value As ToggleSwitchButtonAlignment)
            If value <> _onButtonAlignment Then
                _onButtonAlignment = value
                Refresh()
            End If
        End Set
    End Property

    <Bindable(False), DefaultValue(True), Category("Behavior"), Description("Gets or sets whether the toggle change should be animated or not")>
    Public Property UseAnimation() As Boolean
        Get
            Return _useAnimation
        End Get
        Set(ByVal value As Boolean)
            _useAnimation = value
        End Set
    End Property

    <Bindable(False), DefaultValue(1), Category("Behavior"), Description("Gets or sets the interval in ms between animation frames")>
    Public Property AnimationInterval() As Integer
        Get
            Return _animationInterval
        End Get
        Set(ByVal value As Integer)
            If value <> Me.AnimationInterval AndAlso value > 0 Then
                _animationInterval = value
            End If
        End Set
    End Property

    <Bindable(False), DefaultValue(10), Category("Behavior"), Description("Gets or sets the step in pixes the button should be moved between each animation interval")>
    Public Property AnimationStep() As Integer
        Get
            Return _animationStep
        End Get
        Set(ByVal value As Integer)
            If value <> Me.AnimationStep AndAlso value > 0 Then
                _animationStep = value
            End If
        End Set
    End Property

#Region "Hidden Base Properties"

    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="value")>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Shadows Property Text() As String
        Get
            Return String.Empty
        End Get
        Set(ByVal value As String)
            MyBase.Text = ""
        End Set
    End Property

    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="value")>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Shadows Property ForeColor() As Color
        Get
            Return Color.Black
        End Get
        Set(ByVal value As Color)
            MyBase.ForeColor = Color.Black
        End Set
    End Property

    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="value")>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Shadows Property Font() As Font
        Get
            Return MyBase.Font
        End Get
        Set(ByVal value As Font)
            MyBase.Font = New Font(MyBase.Font, FontStyle.Regular)
        End Set
    End Property

#End Region ' Hidden Base Properties

#End Region ' Public Properties

#Region "Internal Properties"

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsButtonHovered() As Boolean
        Get
            Return _isButtonHovered AndAlso Not _isButtonPressed
        End Get
    End Property

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsButtonPressed() As Boolean
        Get
            Return _isButtonPressed
        End Get
    End Property

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsLeftSideHovered() As Boolean
        Get
            Return _isLeftFieldHovered AndAlso Not _isLeftFieldPressed
        End Get
    End Property

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsLeftSidePressed() As Boolean
        Get
            Return _isLeftFieldPressed
        End Get
    End Property

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsRightSideHovered() As Boolean
        Get
            Return _isRightFieldHovered AndAlso Not _isRightFieldPressed
        End Get
    End Property

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsRightSidePressed() As Boolean
        Get
            Return _isRightFieldPressed
        End Get
    End Property

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend Property ButtonValue() As Integer
        Get
            If _animating OrElse _moving Then
                Return _buttonValue
            ElseIf _checked Then
                Return Width - _renderer.GetButtonWidth()
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            If value <> _buttonValue Then
                _buttonValue = value
                Refresh()
            End If
        End Set
    End Property

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsButtonOnLeftSide() As Boolean
        Get
            Return (ButtonValue <= 0)
        End Get
    End Property

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsButtonOnRightSide() As Boolean
        Get
            Return (ButtonValue >= (Width - _renderer.GetButtonWidth()))
        End Get
    End Property

    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsButtonMovingLeft() As Boolean
        Get
            Return (_animating AndAlso (Not IsButtonOnLeftSide) AndAlso _animationResult = False)
        End Get
    End Property

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property IsButtonMovingRight() As Boolean
        Get
            Return (_animating AndAlso (Not IsButtonOnRightSide) AndAlso _animationResult = True)
        End Get
    End Property

    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Friend ReadOnly Property AnimationResult() As Boolean
        Get
            Return _animationResult
        End Get
    End Property

#End Region ' Private Properties

#Region "Overridden Control Methods"

    Protected Overrides ReadOnly Property DefaultSize() As Size
        Get
            Return New Size(50, 19)
        End Get
    End Property

    Protected Overrides Sub OnPaintBackground(ByVal pevent As PaintEventArgs)
        If pevent Is Nothing Then Return
        pevent.Graphics.ResetClip()
        MyBase.OnPaintBackground(pevent)
        If _renderer IsNot Nothing Then
            _renderer.RenderBackground(pevent)
        End If
    End Sub
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e Is Nothing Then Return
        e.Graphics.ResetClip()

        MyBase.OnPaint(e)

        If _renderer IsNot Nothing Then
            _renderer.RenderControl(e)
        End If
    End Sub

    Protected Overrides Sub OnMouseMove(ByVal e As MouseEventArgs)
        If e Is Nothing Then Return
        _lastMouseEventArgs = e

        Dim buttonWidth As Integer = _renderer.GetButtonWidth()
        Dim rectangle As Rectangle = _renderer.GetButtonRectangle(buttonWidth)

        If _moving Then
            Dim val As Integer = _xValue + (e.Location.X - _xOffset)

            If val < 0 Then
                val = 0
            End If

            If val > Width - buttonWidth Then
                val = Width - buttonWidth
            End If

            ButtonValue = val
            Refresh()
            Return
        End If

        If rectangle.Contains(e.Location) Then
            _isButtonHovered = True
            _isLeftFieldHovered = False
            _isRightFieldHovered = False
        Else
            If e.Location.X > rectangle.X + rectangle.Width Then
                _isButtonHovered = False
                _isLeftFieldHovered = False
                _isRightFieldHovered = True
            ElseIf e.Location.X < rectangle.X Then
                _isButtonHovered = False
                _isLeftFieldHovered = True
                _isRightFieldHovered = False
            End If
        End If

        Refresh()
    End Sub

    Protected Overrides Sub OnMouseDown(ByVal e As MouseEventArgs)
        If e Is Nothing Then Return
        If _animating OrElse (Not AllowUserChange) Then
            Return
        End If

        Dim buttonWidth As Integer = _renderer.GetButtonWidth()
        'INSTANT VB NOTE: The variable buttonRectangle was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim buttonRectangle_Renamed As Rectangle = _renderer.GetButtonRectangle(buttonWidth)

        _savedButtonValue = ButtonValue

        If buttonRectangle_Renamed.Contains(e.Location) Then
            _isButtonPressed = True
            _isLeftFieldPressed = False
            _isRightFieldPressed = False

            _moving = True
            _xOffset = e.Location.X
            _buttonValue = buttonRectangle_Renamed.X
            _xValue = ButtonValue
        Else
            If e.Location.X > buttonRectangle_Renamed.X + buttonRectangle_Renamed.Width Then
                _isButtonPressed = False
                _isLeftFieldPressed = False
                _isRightFieldPressed = True
            ElseIf e.Location.X < buttonRectangle_Renamed.X Then
                _isButtonPressed = False
                _isLeftFieldPressed = True
                _isRightFieldPressed = False
            End If
        End If

        Refresh()
    End Sub

    Protected Overrides Sub OnMouseUp(ByVal e As MouseEventArgs)
        If _animating OrElse (Not AllowUserChange) Then
            Return
        End If

        Dim buttonWidth As Integer = _renderer.GetButtonWidth()

        Dim wasLeftSidePressed As Boolean = IsLeftSidePressed
        Dim wasRightSidePressed As Boolean = IsRightSidePressed

        _isButtonPressed = False
        _isLeftFieldPressed = False
        _isRightFieldPressed = False

        If _moving Then
            Dim percentage As Integer = CInt(Fix((100 * CDbl(ButtonValue)) / (CDbl(Width) - CDbl(buttonWidth))))

            If _checked Then
                If percentage <= (100 - _thresholdPercentage) Then
                    _animationTarget = 0
                    BeginAnimation(False)
                ElseIf ToggleOnButtonClick AndAlso _savedButtonValue = ButtonValue Then
                    _animationTarget = 0
                    BeginAnimation(False)
                Else
                    _animationTarget = Width - buttonWidth
                    BeginAnimation(True)
                End If
            Else
                If percentage >= _thresholdPercentage Then
                    _animationTarget = Width - buttonWidth
                    BeginAnimation(True)
                ElseIf ToggleOnButtonClick AndAlso _savedButtonValue = ButtonValue Then
                    _animationTarget = Width - buttonWidth
                    BeginAnimation(True)
                Else
                    _animationTarget = 0
                    BeginAnimation(False)
                End If
            End If

            _moving = False
            Return
        End If

        If IsButtonOnRightSide Then
            _buttonValue = Width - buttonWidth
            _animationTarget = 0
        Else
            _buttonValue = 0
            _animationTarget = Width - buttonWidth
        End If

        If wasLeftSidePressed AndAlso ToggleOnSideClick Then
            SetValueInternal(False)
        ElseIf wasRightSidePressed AndAlso ToggleOnSideClick Then
            SetValueInternal(True)
        End If

        Refresh()
    End Sub

    Protected Overrides Sub OnMouseLeave(ByVal e As EventArgs)
        _isButtonHovered = False
        _isLeftFieldHovered = False
        _isRightFieldHovered = False
        _isButtonPressed = False
        _isLeftFieldPressed = False
        _isRightFieldPressed = False

        Refresh()
    End Sub

    Protected Overrides Sub OnEnabledChanged(ByVal e As EventArgs)
        MyBase.OnEnabledChanged(e)
        Refresh()
    End Sub

    Protected Overrides Sub OnRegionChanged(ByVal e As EventArgs)
        MyBase.OnRegionChanged(e)
        Refresh()
    End Sub

    Protected Overrides Sub OnSizeChanged(ByVal e As EventArgs)
        If _animationTarget > 0 Then
            Dim buttonWidth As Integer = _renderer.GetButtonWidth()
            _animationTarget = Width - buttonWidth
        End If

        MyBase.OnSizeChanged(e)
    End Sub

#End Region ' Overridden Control Methods

#Region "Private Methods"

    Private Sub SetValueInternal(ByVal checkedValue As Boolean)
        If checkedValue = _checked Then
            Return
        End If

        Do While _animating
            Application.DoEvents()
        Loop

        BeginAnimation(checkedValue)
    End Sub

    Private Sub BeginAnimation(ByVal checkedValue As Boolean)
        _animating = True
        _animationResult = checkedValue

        If _animationTimer IsNot Nothing AndAlso _useAnimation Then
            _animationTimer.Interval = _animationInterval
            _animationTimer.Enabled = True
        Else
            AnimationComplete()
        End If
    End Sub

    Private Sub AnimationTimer_Tick(ByVal sender As Object, ByVal e As EventArgs)
        _animationTimer.Enabled = False

        Dim animationDone As Boolean = False
        Dim newButtonValue As Integer

        If IsButtonMovingRight Then
            newButtonValue = ButtonValue + _animationStep

            If newButtonValue > _animationTarget Then
                newButtonValue = _animationTarget
            End If

            ButtonValue = newButtonValue

            animationDone = ButtonValue >= _animationTarget
        Else
            newButtonValue = ButtonValue - _animationStep

            If newButtonValue < _animationTarget Then
                newButtonValue = _animationTarget
            End If

            ButtonValue = newButtonValue

            animationDone = ButtonValue <= _animationTarget
        End If

        If animationDone Then
            AnimationComplete()
        Else
            _animationTimer.Enabled = True
        End If
    End Sub

    Private Sub AnimationComplete()
        _animating = False
        _moving = False
        _checked = _animationResult

        _isButtonHovered = False
        _isButtonPressed = False
        _isLeftFieldHovered = False
        _isLeftFieldPressed = False
        _isRightFieldHovered = False
        _isRightFieldPressed = False

        Refresh()

        RaiseEvent CheckedChanged(Me, New EventArgs())

        If _lastMouseEventArgs IsNot Nothing Then
            OnMouseMove(_lastMouseEventArgs)
        End If

        _lastMouseEventArgs = Nothing
    End Sub

#End Region ' Private Methods

#Region " EVENTS "

    <Description("Raised when the ToggleSwitch has changed state")>
    Public Event CheckedChanged As EventHandler(Of EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveCheckedChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.CheckedChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

#End Region

#Region "Private Members"

    Private ReadOnly _animationTimer As New Timer()
    Private _Renderer As ToggleSwitchRendererBase

    Private _Style As ToggleSwitchStyle = ToggleSwitchStyle.Metro
    Private _Checked As Boolean = False
    Private _Moving As Boolean = False
    Private _Animating As Boolean = False
    Private _AnimationResult As Boolean = False
    Private _AnimationTarget As Integer = 0
    Private _UseAnimation As Boolean = True
    Private _AnimationInterval As Integer = 1
    Private _AnimationStep As Integer = 10
    Private _AllowUserChange As Boolean = True

    Private _IsLeftFieldHovered As Boolean = False
    Private _IsButtonHovered As Boolean = False
    Private _IsRightFieldHovered As Boolean = False
    Private _IsLeftFieldPressed As Boolean = False
    Private _IsButtonPressed As Boolean = False
    Private _IsRightFieldPressed As Boolean = False

    Private _ButtonValue As Integer = 0
    Private _SavedButtonValue As Integer = 0
    Private _XOffset As Integer = 0
    Private _XValue As Integer = 0
    Private _ThresholdPercentage As Integer = 50
    Private _GrayWhenDisabled As Boolean = True
    Private _ToggleOnButtonClick As Boolean = True
    Private _ToggleOnSideClick As Boolean = True

    Private _LastMouseEventArgs As MouseEventArgs = Nothing

    Private _ButtonScaleImage As Boolean
    Private _ButtonAlignment As ToggleSwitchButtonAlignment = ToggleSwitchButtonAlignment.Center
    Private _ButtonImage As Image = Nothing

    Private _OffText As String = ""
    Private _OffForeColor As Color = Color.Black
    Private _OffFont As Font
    Private _OffSideImage As Image = Nothing
    Private _OffSideScaleImage As Boolean
    Private _OffSideAlignment As ToggleSwitchAlignment = ToggleSwitchAlignment.Center
    Private _OffButtonImage As Image = Nothing
    Private _OffButtonScaleImage As Boolean
    Private _OffButtonAlignment As ToggleSwitchButtonAlignment = ToggleSwitchButtonAlignment.Center

    Private _OnText As String = ""
    Private _OnForeColor As Color = Color.Black
    Private _OnFont As Font
    Private _OnSideImage As Image = Nothing
    Private _OnSideScaleImage As Boolean
    Private _OnSideAlignment As ToggleSwitchAlignment = ToggleSwitchAlignment.Center
    Private _OnButtonImage As Image = Nothing
    Private _OnButtonScaleImage As Boolean
    Private _OnButtonAlignment As ToggleSwitchButtonAlignment = ToggleSwitchButtonAlignment.Center

#End Region ' Private Members

End Class

#Region "Enums"

''' <summary> Values that represent toggle switch styles. </summary>
Public Enum ToggleSwitchStyle
    Metro
    Android
    Ios5
    BrushedMetal
    OS10
    Carbon
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="IPhone")>
    IPhone
    Fancy
    Modern
End Enum

''' <summary> Values that represent toggle switch alignments. </summary>
Public Enum ToggleSwitchAlignment
    Near
    Center
    Far
End Enum

''' <summary> Values that represent toggle switch button alignments. </summary>
Public Enum ToggleSwitchButtonAlignment
    Left
    Center
    Right
End Enum

#End Region ' Enums


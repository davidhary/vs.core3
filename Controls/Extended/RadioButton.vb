﻿Imports System.ComponentModel
''' <summary> Radio button. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="9/27/2013" by="David" revision=""> Created. </history>
<DesignerCategory("code"), System.ComponentModel.Description("Extended Radio Button")>
Public Class RadioButton
    Inherits System.Windows.Forms.RadioButton

    Private _Readonly As Boolean

    ''' <summary> Gets or sets the read only property. </summary>
    ''' <value> The read only. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Indicates whether the radio button is read only.")>
    Public Property [ReadOnly]() As Boolean
        Get
            Return Me._readonly
        End Get
        Set(ByVal value As Boolean)
            If Me._readonly <> value Then
                Me._readonly = value
            End If
        End Set
    End Property

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Click" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnClick(ByVal e As System.EventArgs)
        If Not Me._readonly Then
            MyBase.OnClick(e)
        End If
    End Sub

End Class

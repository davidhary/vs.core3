Imports System.ComponentModel
''' <summary> Extends the standard <see cref="System.Windows.Forms.NumericUpDown">numeric up
''' down</see>control. </summary>
''' <license> (c) 2013 Claudio NiCora HTTP://CoolSoft.AlterVista.org.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="9/27/2013" by="David"> Created from http://www.CodeProject.com/KB/edit/NumericUpDownEx.aspx. </history>
<DesignerCategory("code"), System.ComponentModel.Description("Numeric Up Down")>
Public Class NumericUpDown
    Inherits NumericUpDownBase

    ''' <summary> object creator. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Embedded resource names. </summary>
    ''' <returns> The resource names. </returns>
    Public Shared Function EmbeddedResourceNames() As IEnumerable(Of String)
        Return System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceNames
    End Function

End Class


Imports System.ComponentModel
''' <summary> A progress label. </summary>
''' <license>
''' (c) 2007 Hypercubed. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="9/19/2016" by="David">
''' >
''' http://www.codeproject.com/script/Membership/View.aspx?mid=722189
''' http://www.codeproject.com/Articles/21419/Label-with-ProgressBar-in-a-StatusStrip.
''' </history>
<DesignerCategory("code"), Description("Label with progress bar")>
Public Class ProgressLabel
    Inherits Label

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.Paint" />
    '''  event.
    ''' </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" />
    '''                   that contains the event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e Is Nothing Then Return

        Dim percent As Double = Me.Value / 100
        Dim rect As Rectangle = e.ClipRectangle

        Dim height As Integer = Me.BarHeight
        If height = 0 Then height = rect.Height

        rect.Width = CInt(rect.Width * percent)
        rect.Y = CInt(0.5 * (rect.Height - height))
        rect.Height = height

        Using brush As SolidBrush = New SolidBrush(BarColor)
            ' Draw bar
            Dim g As Graphics = e.Graphics
            g.FillRectangle(brush, rect)
        End Using
        MyBase.OnPaint(e)

    End Sub

    ''' <summary> The value. </summary>
    Private _Value As Integer = 0               ' Current value

    ''' <summary> Progress Value. </summary>
    ''' <value> The value. </value>
    <Category("Behavior"), Description("Progress Value"), DefaultValue(0)>
    Public Property Value() As Integer
        Get
            Return _Value
        End Get
        Set(ByVal Value As Integer)

            ' Make sure that the value does not stray outside the valid range.
            Select Case Value
                Case Is < 0
                    _Value = 0
                Case Is > 100
                    _Value = 100
                Case Else
                    _Value = Value
            End Select

            ' Invalidate the control to get a repaint.
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The bar color. </summary>
    Private _BarColor As Color = Color.Blue   ' Color of bar

    ''' <summary> Progress Color. </summary>
    ''' <value> The color of the bar. </value>
    <Category("Behavior"), Description("Progress Color"), DefaultValue(GetType(Color), "Blue")>
    Public Property BarColor() As Color
        Get
            Return _BarColor
        End Get

        Set(ByVal Value As Color)
            _BarColor = Value

            ' Invalidate the control to get a repaint.
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> Height of the bar. </summary>
    Private _BarHeight As Integer = 0

    ''' <summary> Progress Height. </summary>
    ''' <value> The height of the bar. </value>
    <Category("Behavior"), Description("Progress Height"), DefaultValue(0)>
    Public Property BarHeight() As Integer
        Get
            Return _BarHeight
        End Get
        Set(ByVal value As Integer)
            Select Case value
                Case Is > Me.Size.Height, Is < 0
                    _BarHeight = Me.Size.Height
                Case Else
                    _BarHeight = value
            End Select

            ' Invalidate the control to get a repaint.
            Me.Invalidate()
        End Set
    End Property

    Private _CaptionFormat As String
    ''' <summary> Specifies the format of the overlay. </summary>
    ''' <value> The caption format. </value>
    <Category("Appearance"), DefaultValue("{0} %"),
        Description("Specifies the format of the overlay.")>
    Public Property CaptionFormat As String
        Get
            If String.IsNullOrEmpty(Me._CaptionFormat) Then
                Return "{0} %"
            Else
                Return Me._CaptionFormat
            End If
        End Get
        Set(value As String)
            Me._CaptionFormat = value
        End Set
    End Property

    ''' <summary> The default caption format. </summary>
    Public Const DefaultCaptionFormat As String = "{0} %"

    ''' <summary> Updates the progress described by value. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub UpdateProgress(ByVal value As Integer)
        Dim format As String = Me.CaptionFormat
        If String.IsNullOrEmpty(format) Then format = DefaultCaptionFormat
        Me.UpdateProgress(value, String.Format(Globalization.CultureInfo.CurrentCulture, format, value))
    End Sub

    ''' <summary> Updates the progress described by arguments. </summary>
    ''' <param name="value">   The value. </param>
    ''' <param name="caption"> The caption. </param>
    Public Sub UpdateProgress(ByVal value As Integer, ByVal caption As String)
        If (value >= 0 Xor Me.Visible) Then Me.Visible = value >= 0
        If Me.Visible Then
            Me.Text = caption
            Me.Value = value
        End If
    End Sub

End Class


Imports System.Runtime.InteropServices
Imports System.Drawing.Printing
Imports System.ComponentModel
''' <summary>
''' An extension to <see cref="System.Windows.Forms.RichTextBox">windows rich text box</see> suitable
''' for printing.
''' </summary>
''' <license>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/2/2015" by="David" revision=""> Created. </history>
''' <history date="1/8/2015" by="David">              > Created. </history>
<DesignerCategory("code"), System.ComponentModel.Description("Extended Rich Text Box")>
Public Class RichTextBox
    Inherits System.Windows.Forms.RichTextBox

#Region " STRUCTURES AND IMPORTS "

    <StructLayout(LayoutKind.Sequential)>
    Private Structure STRUCT_RECT
        Public left As Int32
        Public top As Int32
        Public right As Int32
        Public bottom As Int32
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Private Structure STRUCT_CHARRANGE
        Public cpMin As Int32
        Public cpMax As Int32
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Private Structure STRUCT_FORMATRANGE
        Public hdc As IntPtr
        Public hdcTarget As IntPtr
        Public rc As STRUCT_RECT
        Public rcPage As STRUCT_RECT
        Public chrg As STRUCT_CHARRANGE
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Private Structure STRUCT_CHARFORMAT
        Public cbSize As Integer
        Public dwMask As UInt32
        Public dwEffects As UInt32
        Public yHeight As Int32
        Public yOffset As Int32
        Public crTextColor As Int32
        Public bCharSet As Byte
        Public bPitchAndFamily As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)>
        Public szFaceName() As Char
    End Structure

    Private NotInheritable Class SafeNativeMethods
        Private Sub New()
            MyBase.New()
        End Sub
        ''' <summary> Sends a message. </summary>
        ''' <param name="hWnd">   The window. </param>
        ''' <param name="msg">    The message. </param>
        ''' <param name="wParam"> The parameter. </param>
        ''' <param name="lParam"> The parameter. </param>
        ''' <returns> An Int32. </returns>
        <DllImport("user32.dll")>
        Friend Shared Function SendMessage(ByVal hWnd As IntPtr,
                                        ByVal msg As UInt32,
                                        ByVal wParam As UIntPtr,
                                        ByVal lParam As IntPtr) As IntPtr
        End Function

    End Class

    ' Windows Message defines
    Private Const WM_USER As Int32 = &H400&
    Private Const EM_FORMATRANGE As Int32 = WM_USER + 57
    Private Const EM_GETCHARFORMAT As Int32 = WM_USER + 58
    Private Const EM_SETCHARFORMAT As Int32 = WM_USER + 68

    ' Defines for EM_GETCHARFORMAT/EM_SETCHARFORMAT
    Private SCF_SELECTION As UIntPtr = New UIntPtr(&H1&)
#If NoUsed Then
    Private SCF_WORD As Int32 = &H2&
    Private SCF_ALL As Int32 = &H4&
#End If

    ' Defines for STRUCT_CHARFORMAT member dwMask
    ' (Long because UInt32 is not an intrinsic type)
    Private Const CFM_BOLD As Long = &H1&
    Private Const CFM_ITALIC As Long = &H2&
    Private Const CFM_UNDERLINE As Long = &H4&
    Private Const CFM_STRIKEOUT As Long = &H8&
    Private Const CFM_PROTECTED As Long = &H10&
    Private Const CFM_LINK As Long = &H20&
    Private Const CFM_SIZE As Long = &H80000000&
    Private Const CFM_COLOR As Long = &H40000000&
    Private Const CFM_FACE As Long = &H20000000&
    Private Const CFM_OFFSET As Long = &H10000000&
    Private Const CFM_CHARSET As Long = &H8000000&

    ' Defines for STRUCT_CHARFORMAT member dwEffects
    Private Const CFE_BOLD As Long = &H1&
    Private Const CFE_ITALIC As Long = &H2&
    Private Const CFE_UNDERLINE As Long = &H4&
    Private Const CFE_STRIKEOUT As Long = &H8&
    Private Const CFE_PROTECTED As Long = &H10&
    Private Const CFE_LINK As Long = &H20&
    Private Const CFE_AUTOCOLOR As Long = &H40000000&

#End Region

#Region " EXTENDED FUNCTIONS "

    ''' <summary> Calculate or render the contents of the <see cref="RichTextBox">rich text box</see> for printing Parameter. </summary>
    ''' <param name="measureOnly"> If true, only the calculation is performed;
    ''' otherwise the text is rendered as well. </param>
    ''' <param name="e">           The PrintPageEventArgs object from the PrintPage event. </param>
    ''' <param name="charFrom">    Index of first character to be printed. </param>
    ''' <param name="charTo">      Index of last character to be printed. </param>
    ''' <returns> (Index of last character that fitted on the page) + 1. </returns>
    Public Function FormatRange(ByVal measureOnly As Boolean,
                                ByVal e As PrintPageEventArgs,
                                ByVal charFrom As Integer,
                                ByVal charTo As Integer) As Integer
        If e Is Nothing Then Return 0

        ' Specify which characters to print
        Dim cr As STRUCT_CHARRANGE
        cr.cpMin = charFrom
        cr.cpMax = charTo

        ' Specify the area inside page margins
        Dim rc As STRUCT_RECT
        rc.top = HundredthInchToTwips(e.MarginBounds.Top)
        rc.bottom = HundredthInchToTwips(e.MarginBounds.Bottom)
        rc.left = HundredthInchToTwips(e.MarginBounds.Left)
        rc.right = HundredthInchToTwips(e.MarginBounds.Right)

        ' Specify the page area
        Dim rcPage As STRUCT_RECT
        rcPage.top = HundredthInchToTwips(e.PageBounds.Top)
        rcPage.bottom = HundredthInchToTwips(e.PageBounds.Bottom)
        rcPage.left = HundredthInchToTwips(e.PageBounds.Left)
        rcPage.right = HundredthInchToTwips(e.PageBounds.Right)

        ' Get device context of output device
        Dim hdc As IntPtr
        hdc = e.Graphics.GetHdc()

        ' Fill in the FORMATRANGE structure
        Dim fr As STRUCT_FORMATRANGE
        fr.chrg = cr
        fr.hdc = hdc
        fr.hdcTarget = hdc
        fr.rc = rc
        fr.rcPage = rcPage

        ' Non-Zero wParam means render, Zero means measure
        Dim wParam As UIntPtr
        If measureOnly Then
            wParam = UIntPtr.Zero
        Else
            wParam = New UIntPtr(1)
        End If

        ' Allocate memory for the FORMATRANGE structure and
        ' copy the contents of our structure to this memory
        Dim lParam As IntPtr
        lParam = Marshal.AllocCoTaskMem(Marshal.SizeOf(fr))
        Marshal.StructureToPtr(fr, lParam, False)

        ' Send the actual Win32 message
        Dim res As IntPtr
        res = SafeNativeMethods.SendMessage(Handle, EM_FORMATRANGE, wParam, lParam)

        ' Free allocated memory
        Marshal.FreeCoTaskMem(lParam)

        ' and release the device context
        e.Graphics.ReleaseHdc(hdc)

        Return CInt(res)
    End Function

    ''' <summary> Converts between 1/100 inch (unit used by the .NET framework)
    ''' and twips (1/1440 inch, used by Win32 API calls). </summary>
    ''' <param name="n"> Value in 1/100 inch Return value: Value in twips. </param>
    ''' <returns> An Int32. </returns>
    Private Shared Function HundredthInchToTwips(ByVal n As Integer) As Int32
        Return Convert.ToInt32(n * 14.4)
    End Function

    ''' <summary> Frees cached data from rich edit control after printing. </summary>
    Public Function FormatRangeDone() As Integer
        Return CInt(SafeNativeMethods.SendMessage(Me.Handle, EM_FORMATRANGE, UIntPtr.Zero, IntPtr.Zero))
    End Function

    ''' <summary> Sets the font only for the selected part of the rich text box without modifying the
    ''' other properties like size or style. </summary>
    ''' <param name="face"> Name of the font to use. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function SetSelectionFont(ByVal face As String) As Boolean
        If String.IsNullOrEmpty(face) Then Return False
        Dim cf As New STRUCT_CHARFORMAT()
        cf.cbSize = Marshal.SizeOf(cf)
        cf.dwMask = Convert.ToUInt32(CFM_FACE)

        ' ReDim face name to relevant size
        ReDim cf.szFaceName(32)
        face.CopyTo(0, cf.szFaceName, 0, Math.Min(31, face.Length))

        Dim lParam As IntPtr
        lParam = Marshal.AllocCoTaskMem(Marshal.SizeOf(cf))
        Marshal.StructureToPtr(cf, lParam, False)

        Dim res As IntPtr
        res = SafeNativeMethods.SendMessage(Handle, EM_SETCHARFORMAT, SCF_SELECTION, lParam)
        Return res = IntPtr.Zero

    End Function

    ''' <summary> Sets the font size only for the selected part of the rich text box without modifying
    ''' the other properties like font or style. </summary>
    ''' <param name="size"> new point size to use. </param>
    ''' <returns> true on success, false on failure. </returns>
    Public Function SetSelectionSize(ByVal size As Integer) As Boolean
        Dim cf As New STRUCT_CHARFORMAT()
        cf.cbSize = Marshal.SizeOf(cf)
        cf.dwMask = Convert.ToUInt32(CFM_SIZE)
        ' yHeight is in 1/20 pt
        cf.yHeight = size * 20

        Dim lParam As IntPtr
        lParam = Marshal.AllocCoTaskMem(Marshal.SizeOf(cf))
        Marshal.StructureToPtr(cf, lParam, False)

        Dim res As IntPtr
        res = SafeNativeMethods.SendMessage(Handle, EM_SETCHARFORMAT, SCF_SELECTION, lParam)
        Return res = IntPtr.Zero

    End Function

    ''' <summary> Sets the bold style only for the selected part of the rich text box without modifying
    ''' the other properties like font or size. </summary>
    ''' <param name="bold"> make selection bold (true) or regular (false) </param>
    ''' <returns> true on success, false on failure. </returns>
    Public Function SetSelectionBold(ByVal bold As Boolean) As Boolean
        If bold Then
            Return SetSelectionStyle(CFM_BOLD, CFE_BOLD)
        Else
            Return SetSelectionStyle(CFM_BOLD, 0)
        End If
    End Function

    ''' <summary> Sets the italic style only for the selected part of the rich text box without
    ''' modifying the other properties like font or size. </summary>
    ''' <param name="italic"> make selection italic (true) or regular (false) </param>
    ''' <returns> true on success, false on failure. </returns>
    Public Function SetSelectionItalic(ByVal italic As Boolean) As Boolean
        If italic Then
            Return SetSelectionStyle(CFM_ITALIC, CFE_ITALIC)
        Else
            Return SetSelectionStyle(CFM_ITALIC, 0)
        End If
    End Function

    ''' <summary> Sets the underlined style only for the selected part of the rich text box ' without
    ''' modifying the other properties like font or size. </summary>
    ''' <param name="underlined"> make selection underlined (true) or regular (false) </param>
    ''' <returns> true on success, false on failure. </returns>
    Public Function SetSelectionUnderlined(ByVal underlined As Boolean) As Boolean
        If underlined Then
            Return SetSelectionStyle(CFM_UNDERLINE, CFE_UNDERLINE)
        Else
            Return SetSelectionStyle(CFM_UNDERLINE, 0)
        End If
    End Function

    ''' <summary> Set the style only for the selected part of the rich text box with the possibility to
    ''' mask out some styles that are not to be modified. </summary>
    ''' <param name="mask">   modify which styles? </param>
    ''' <param name="effect"> new values for the styles. </param>
    ''' <returns> true on success, false on failure. </returns>
    Private Function SetSelectionStyle(ByVal mask As Int32, ByVal effect As Int32) As Boolean
        Dim cf As New STRUCT_CHARFORMAT()
        cf.cbSize = Marshal.SizeOf(cf)
        cf.dwMask = Convert.ToUInt32(mask)
        cf.dwEffects = Convert.ToUInt32(effect)

        Dim lParam As IntPtr
        lParam = Marshal.AllocCoTaskMem(Marshal.SizeOf(cf))
        Marshal.StructureToPtr(cf, lParam, False)

        Dim res As IntPtr
        res = SafeNativeMethods.SendMessage(Handle, EM_SETCHARFORMAT, SCF_SELECTION, lParam)
        Return res = IntPtr.Zero
    End Function
#End Region

End Class
﻿Imports System.ComponentModel
Imports System.Security.Permissions
Imports isr.Core.Pith.EventHandlerExtensions
Imports isr.Core.Pith.ExceptionExtensions
''' <summary> Numeric Up Down base control. </summary>
''' <remarks> Features:<para>
''' Text selection properties similar to text box control;</para><para>
''' Controls handling mouse wheel events;</para><para>
''' Fixes mouse enter and leave event handling;</para><para>
''' Adds value-incrementing and -decrementing events allowing to alter the increment or decrement
''' dynamically;</para><para>
''' Adds Wrap Value property to allow the wrapping of the value to maximum or minimum when
''' reaching the minimum or maximum, respectively;</para><para>
''' Adds option to show up the up/down buttons when the control has focus regardless or mouse
''' over.</para><para>
''' Disables up/down events when read only.</para><para>
''' Adds up/down cursor.</para><para>
''' Adds engineering scaling.</para><para>
''' Author:   Claudio NiCora</para><para>
''' WebSite:  http://CoolSoft.AlterVista.org </para><para>
''' CodeProject: http://www.CodeProject.com/KB/edit/NumericUpDownEx.aspx </para><para>
''' Feel free to contribute here: HTTP://CoolSoft.AlterVista.org</para>
''' </remarks>
''' <license> (c) 2013 Claudio NiCora.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="4/5/2014" by="David"> Created. </history>
<DesignerCategory("code"), System.ComponentModel.Description("Numeric Up Down Base Control")>
Public MustInherit Class NumericUpDownBase
    Inherits System.Windows.Forms.NumericUpDown

#Region " CONSTRUCTION "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    Protected Sub New()
        MyBase.New()

        ' extract a reference to the underlying TextBox field
        ' Me._TextBox = GetPrivateField(Of TextBox)(Me, "upDownEdit")
        Me._TextBox = TryCast(Me.Controls(1), System.Windows.Forms.TextBox)
        If Me._TextBox Is Nothing OrElse
            Me._TextBox.GetType.FullName <> "System.Windows.Forms.UpDownBase+UpDownEdit" Then
            Throw New ArgumentNullException(Me.GetType.FullName & ": Can't get a reference to the internal Text Box field.")
        End If

        ' extract a reference to the underlying UpDownButtons field
        Me._UpDownButtons = Me.Controls(0)
        ' Me._UpDownButtons = GetPrivateField(Of Control)(Me, "upDownButtons")
        If Me._UpDownButtons Is Nothing OrElse
            Me._UpDownButtons.GetType.FullName <> "System.Windows.Forms.UpDownBase+UpDownButtons" Then
            Throw New ArgumentNullException(Me.GetType.FullName & ": Can't get a reference to the internal UpDown buttons field.")
        Else
            Me._UpDownButtons.Cursor = Me._UpDownCursor
        End If

        Me._hasFocus = False
        Me._UpDownDisplayMode = UpDownButtonsDisplayMode.Always
        Me._InterceptMouseWheel = InterceptMouseWheelMode.Always

        ' add handlers (MouseEnter and MouseLeave events of NumericUpDown
        ' are not working properly)
        AddHandler Me.TextBox.MouseEnter, AddressOf Me._mouseEnterLeave
        AddHandler Me.TextBox.MouseLeave, AddressOf Me._mouseEnterLeave
        AddHandler Me.UpDownButtons.MouseEnter, AddressOf Me._mouseEnterLeave
        AddHandler Me.UpDownButtons.MouseLeave, AddressOf Me._mouseEnterLeave
        AddHandler MyBase.MouseEnter, AddressOf Me._mouseEnterLeave
        AddHandler MyBase.MouseLeave, AddressOf Me._mouseEnterLeave

    End Sub

    ''' <summary> Releases the unmanaged resources used by the NumericUpDown and optionally releases
    ''' the managed resources. </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    ''' release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Try : RemoveHandler Me.TextBox.MouseEnter, AddressOf Me._mouseEnterLeave : Catch : End Try
                Try : RemoveHandler Me.TextBox.MouseLeave, AddressOf Me._mouseEnterLeave : Catch : End Try
                Try : RemoveHandler Me.UpDownButtons.MouseEnter, AddressOf Me._mouseEnterLeave : Catch : End Try
                Try : RemoveHandler Me.UpDownButtons.MouseLeave, AddressOf Me._mouseEnterLeave : Catch : End Try
                Try : RemoveHandler MyBase.MouseEnter, AddressOf Me._mouseEnterLeave : Catch : End Try
                Try : RemoveHandler MyBase.MouseLeave, AddressOf Me._mouseEnterLeave : Catch : End Try
                Me.RemoveNumericTextChangedEventHandler(Me.NumericTextChangedEvent)
                Me.RemoveValueDecrementingEventHandler(Me.ValueDecrementingEvent)
                Me.RemoveValueIncrementingEventHandler(Me.ValueIncrementingEvent)
                Me.RemoveMouseEnterEvent(Me.MouseEnterEvent)
                Me.RemoveMouseLeaveEvent(Me.MouseLeaveEvent)
            End If
        Catch
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " Windows Procedure "

    ''' <summary> Windows Procedure override to kill wN_MouseWheel message. </summary>
    ''' <param name="m"> [in,out] The Windows <see cref="T:System.Windows.Forms.Message" /> to
    ''' process. </param>
    <SecurityPermission(SecurityAction.Demand, Flags:=SecurityPermissionFlag.UnmanagedCode)>
    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        Const WM_MOUSEWHEEL As Integer = &H20A

        If m.Msg = WM_MOUSEWHEEL Then
            Select Case Me.InterceptMouseWheel
                Case InterceptMouseWheelMode.Always
                    ' standard message
                    MyBase.WndProc(m)
                Case InterceptMouseWheelMode.WhenMouseOver
                    If Me._mouseOver Then
                        ' standard message
                        MyBase.WndProc(m)
                    End If
                Case InterceptMouseWheelMode.Never
                    ' kill the message
                    Exit Sub
            End Select
        Else
            MyBase.WndProc(m)
        End If

    End Sub

#End Region

#Region " ON EVENTS "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" />  that contains the
    ''' event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
        If e Is Nothing Then Return
        If Me.UpDownButtons IsNot Nothing AndAlso Me.UpDownButtons.Visible = False Then
            e.Graphics.Clear(Me.BackColor)
        End If
        MyBase.OnPaint(e)
    End Sub

#End Region

#Region " UP DOWN BUTTONS "

    ''' <summary> Reference to the underlying UpDownButtons control. </summary>
    Protected Property UpDownButtons As Control

    Private _UpDownCursor As Cursor
    ''' <summary> Gets or sets the up down cursor. </summary>
    ''' <value> The up down cursor. </value>
    <DefaultValue(GetType(System.Windows.Forms.Cursor), "System.Windows.Forms.Cursors.Default")>
    <Description("The up/down cursor"), Category("Appearance")>
    Public Property UpDownCursor As Cursor
        Get
            Return Me._UpDownCursor
        End Get
        Set(value As Cursor)
            Me._UpDownCursor = value
            If Me._UpDownButtons IsNot Nothing Then
                Me._UpDownButtons.Cursor = value
            End If
        End Set
    End Property

#End Region

#Region " TEXT BOX "

    ''' <summary> Gets or sets the text box. </summary>
    ''' <value> The text box. </value>
    Protected Property TextBox As System.Windows.Forms.TextBox

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.GotFocus" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    ''' <remarks> select all the text on focus enter. </remarks>
    Protected Overrides Sub OnGotFocus(ByVal e As System.EventArgs)
        If Me.AutoSelect Then
            Me.TextBox.SelectAll()
        End If
        ' Update UpDownButtons visibility
        If Me.ShowUpDownButtons Then
            Me.UpdateUpDownButtonsVisibility()
        End If
        MyBase.OnGotFocus(e)
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.LostFocus" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLostFocus(e As EventArgs)
        Me._hasFocus = False
        ' Update UpDownButtons visibility
        If Me.ShowUpDownButtons Then
            Me.UpdateUpDownButtonsVisibility()
        End If
        MyBase.OnLostFocus(e)
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseUp" /> event. </summary>
    ''' <remarks> MouseUp will kill the SelectAll made on GotFocus. Will restore it, but only if user
    ''' have not made a partial text selection. </remarks>
    ''' <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    ''' event data. </param>
    Protected Overrides Sub OnMouseUp(ByVal mevent As System.Windows.Forms.MouseEventArgs)
        If Me.AutoSelect AndAlso Me.TextBox.SelectionLength = 0 Then
            Me.TextBox.SelectAll()
        End If
        MyBase.OnMouseUp(mevent)
    End Sub

    ''' <summary> Gets or sets the selection start. </summary>
    ''' <value> The selection start. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property SelectionStart As Integer
        Get
            Return Me.TextBox.SelectionStart
        End Get
        Set(value As Integer)
            Me.TextBox.SelectionStart = value
        End Set
    End Property

    ''' <summary> Gets or sets the text to be displayed in the
    ''' <see cref="T:System.Windows.Forms.NumericUpDown" /> control. </summary>
    ''' <value> Null. </value>
    ''' <remarks> After <see cref="ResetText">reset</see> this value clears not reflecting the <see cref="Value">value</see>.
    '''           Use <see cref="Sync">Sync</see> to sync the <see cref="Text">text</see> and <see cref="Value">value</see>. </remarks>
    Public Overrides Property Text As String
        Get
            Return MyBase.Text
        End Get
        Set(value As String)
            MyBase.Text = value
        End Set
    End Property

    ''' <summary> Synchronizes the <see cref="Text">text</see> and <see cref="Value">value</see>. . </summary>
    Public Sub Sync()
        Dim textValue As String = CStr(Me.Value)
        If Not String.Equals(Me.Text, textValue) Then
            Me.Text = textValue
        End If
    End Sub

    ''' <summary> Event queue for all listeners interested in NumericTextChanged events. </summary>
    Public Event NumericTextChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveNumericTextChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.NumericTextChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.TextChanged" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnTextChanged(e As System.EventArgs)
        MyBase.OnTextChanged(e)
        Dim evt As EventHandler(Of System.EventArgs) = Me.NumericTextChangedEvent
        evt?.Invoke(Me, e)
    End Sub

#End Region

#Region " REFLECTION "

    ''' <summary> Extracts a reference to a private underlying field. </summary>
    ''' <param name="ctrl">      The control. </param>
    ''' <param name="fieldName"> Name of the field. </param>
    ''' <returns> The private field. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")>
    Protected Friend Shared Function GetPrivateField(Of T As Control)(ByVal ctrl As System.Windows.Forms.NumericUpDown, ByVal fieldName As String) As T
        ' find internal TextBox
        Dim fi As Reflection.FieldInfo = GetType(System.Windows.Forms.NumericUpDown).GetField(fieldName,
                                                                                              Reflection.BindingFlags.FlattenHierarchy Or
                                                                                              Reflection.BindingFlags.NonPublic Or
                                                                                              Reflection.BindingFlags.Instance)
        ' take some caution... they could change field name in the future!
        If fi Is Nothing Then
            Return Nothing
        Else
            Return TryCast(fi.GetValue(ctrl), T)
        End If
    End Function

#End Region

#Region " READ ONLY "

    Private _ReadOnlyBackColor As Color
    ''' <summary> Gets or sets the color of the read only back. </summary>
    ''' <value> The color of the read only back. </value>
    <DefaultValue(GetType(Drawing.Color), "SystemColors.Control")>
    <Description("Back color when read only"), Category("Appearance")>
    Public Property ReadOnlyBackColor() As Color
        Get
            If Me._ReadOnlyBackColor.IsEmpty Then
                Me._ReadOnlyBackColor = SystemColors.Control
            End If
            Return Me._ReadOnlyBackColor
        End Get
        Set(value As Color)
            Me._ReadOnlyBackColor = value
        End Set
    End Property

    Private _ReadOnlyForeColor As Color
    ''' <summary> Gets or sets the color of the read only foreground. </summary>
    ''' <value> The color of the read only foreground. </value>
    <DefaultValue(GetType(Drawing.Color), "SystemColors.WindowText")>
    <Description("Fore color when read only"), Category("Appearance")>
    Public Property ReadOnlyForeColor() As Color
        Get
            If Me._ReadOnlyForeColor.IsEmpty Then
                Me._ReadOnlyForeColor = SystemColors.WindowText
            End If
            Return Me._ReadOnlyForeColor
        End Get
        Set(value As Color)
            Me._ReadOnlyForeColor = value
        End Set
    End Property

    Private _ReadWriteBackColor As System.Drawing.Color
    ''' <summary> Gets or sets the color of the read write back. </summary>
    ''' <value> The color of the read write back. </value>
    <DefaultValue(GetType(System.Drawing.Color), "SystemColors.Window")>
    <Description("Back color when control is read/write"), Category("Appearance")>
    Public Property ReadWriteBackColor() As System.Drawing.Color
        Get
            If Me._ReadWriteBackColor.IsEmpty Then
                Me._ReadWriteBackColor = SystemColors.Window
            End If
            Return Me._ReadWriteBackColor
        End Get
        Set(value As Color)
            Me._ReadWriteBackColor = value
        End Set
    End Property

    Private _ReadWriteForeColor As System.Drawing.Color
    ''' <summary> Gets or sets the color of the read write foreground. </summary>
    ''' <value> The color of the read write foreground. </value>
    <DefaultValue(GetType(System.Drawing.Color), "System.Drawing.SystemColors.ControlText")>
    <Description("Fore color when control is read/write"), Category("Appearance")>
    Public Property ReadWriteForeColor() As System.Drawing.Color
        Get
            If Me._ReadWriteForeColor.IsEmpty Then
                Me._ReadWriteForeColor = SystemColors.ControlText
            End If
            Return Me._ReadWriteForeColor
        End Get
        Set(value As Color)
            Me._ReadWriteForeColor = value
        End Set
    End Property

    ''' <summary> Gets or sets a value indicating whether the text can be changed by the use of the up
    ''' or down buttons only. </summary>
    ''' <value> <c>True</c> if [read only]; otherwise, <c>False</c>. </value>
    Public Shadows Property [ReadOnly]() As Boolean
        Get
            Return MyBase.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            MyBase.ReadOnly = value
            Me._UpDownButtons.Enabled = Not value
            If value Then
                Me.BackColor = Me.ReadOnlyBackColor
                Me.ForeColor = Me.ReadOnlyForeColor
            Else
                Me.BackColor = Me.ReadWriteBackColor
                Me.ForeColor = Me.ReadWriteForeColor
            End If
        End Set
    End Property

#End Region

#Region " NEW PROPERTIES "

    ''' <summary> Gets or sets the automatic select. </summary>
    ''' <value> The automatic select. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Automatically select control text when it receives focus.")>
    Public Property AutoSelect() As Boolean

    ''' <summary> Gets or sets the intercept mouse wheel. </summary>
    ''' <value> The intercept mouse wheel. </value>
    <DefaultValue(GetType(InterceptMouseWheelMode), "Always")>
    <Category("Behavior")>
    <Description("Enables MouseWheel only under certain conditions.")>
    Public Property InterceptMouseWheel() As InterceptMouseWheelMode

    ''' <summary> The show up down buttons. </summary>
    Private _UpDownDisplayMode As UpDownButtonsDisplayMode

    ''' <summary> Gets or sets the mode for displaying the up down buttons. </summary>
    ''' <value> The show up down buttons. </value>
    <DefaultValue(GetType(UpDownButtonsDisplayMode), "Always")>
    <Category("Behavior")>
    <Description("Set Up/Down Buttons visibility mode.")>
    Public Property UpDownDisplayMode() As UpDownButtonsDisplayMode
        Get
            Return Me._UpDownDisplayMode
        End Get
        Set(ByVal value As UpDownButtonsDisplayMode)
            Me._UpDownDisplayMode = value
            Me.UpdateUpDownButtonsVisibility()
        End Set
    End Property

    ''' <summary> Gets the sentinel indication the up down buttons display mode is visible. </summary>
    ''' <value> The sentinel indication the up down buttons display mode is visible. </value>
    Protected ReadOnly Property ShowUpDownButtons As Boolean
        Get
            Return Me.UpDownDisplayMode = UpDownButtonsDisplayMode.WhenFocus OrElse
                   Me._UpDownDisplayMode = UpDownButtonsDisplayMode.WhenFocusOrMouseOver
        End Get
    End Property

    ''' <summary> If set, incrementing value will cause it to restart from Minimum when Maximum is
    ''' reached (and vice versa). </summary>
    ''' <value> The wrap value. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("If set, incrementing value will cause it to restart from Minimum when Maximum is reached (and vice versa).")>
    Public Property WrapValue() As Boolean

#End Region

#Region " UP DOWN BUTTONS VISIBILITY MANAGEMENT "

    ''' <summary> Tracks the mouse position. <c>True</c> if mouse is over. </summary>
    Private _MouseOver As Boolean

    ''' <summary> Tracks the focus. <c>True</c> if control has focus. </summary>
    Private _HasFocus As Boolean

    ''' <summary> Show or hide the UpDownButtons, according to ShowUpDownButtons property value. </summary>
    Protected Sub UpdateUpDownButtonsVisibility()

        ' test new state
        Dim newVisible As Boolean
        Select Case Me._UpDownDisplayMode
            Case UpDownButtonsDisplayMode.WhenMouseOver
                newVisible = Me._mouseOver
            Case UpDownButtonsDisplayMode.WhenFocus
                newVisible = Me._hasFocus
            Case UpDownButtonsDisplayMode.WhenFocusOrMouseOver
                newVisible = Me._hasFocus OrElse Me._mouseOver
            Case Else
                newVisible = True
        End Select


        ' assign only if needed
        If Me.UpDownButtons.Visible <> newVisible Then
            If newVisible Then
                If Not Me.ReadOnly Then
                    Me.TextBox.Width = Me.ClientRectangle.Width - Me.UpDownButtons.Width
                    Me.UpDownButtons.Visible = newVisible
                    Me.OnTextBoxResize(Me.TextBox, EventArgs.Empty)
                    Me.Invalidate()
                End If
            Else
                Me.TextBox.Width = Me.ClientRectangle.Width
                Me.UpDownButtons.Visible = newVisible
                Me.OnTextBoxResize(Me.TextBox, EventArgs.Empty)
                Me.Invalidate()
            End If
        End If

    End Sub

    ''' <summary> Custom text box size management. </summary>
    ''' <param name="source"> The source of the event. </param>
    ''' <param name="e">      An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnTextBoxResize(ByVal source As Object, ByVal e As System.EventArgs)

        If Me.TextBox Is Nothing Then Exit Sub
        If Me._UpDownDisplayMode = UpDownButtonsDisplayMode.Always Then
            ' standard management
            MyBase.OnTextBoxResize(source, e)
        Else
            ' custom management

            ' change position if Right to left
            Dim fixPos As Boolean = Me.RightToLeft = System.Windows.Forms.RightToLeft.Yes Xor Me.UpDownAlign = LeftRightAlignment.Left

            If Me._mouseOver Then
                If Not Me.ReadOnly Then
                    Me.TextBox.Width = Me.ClientSize.Width - Me.TextBox.Left - Me.UpDownButtons.Width - 2
                    If fixPos Then Me.TextBox.Location = New Point(16, Me.TextBox.Location.Y)
                End If
            Else
                If fixPos Then Me.TextBox.Location = New Point(2, Me.TextBox.Location.Y)
                Me.TextBox.Width = Me.ClientSize.Width - Me.TextBox.Left - 2
            End If

        End If

    End Sub

#End Region

#Region " FIXED EVENTS "

    ''' <summary> Event queue for all listeners interested in MouseEnter events. </summary>
    ''' <remarks> Raised correctly when mouse enters the text box. </remarks>
    Shadows Event MouseEnter As EventHandler(Of EventArgs)

    ''' <summary> Removes mouse enter event. </summary>
    ''' <param name="value"> The value. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveMouseEnterEvent(ByVal value As EventHandler(Of System.EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.MouseEnter, CType(d, EventHandler(Of System.EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Event queue for all listeners interested in MouseLeave events. </summary>
    ''' <remarks> Raised correctly when mouse leaves the text box. </remarks>
    Shadows Event MouseLeave As EventHandler(Of EventArgs)

    ''' <summary> Removes mouse leave event. </summary>
    ''' <param name="value"> The value. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveMouseLeaveEvent(ByVal value As EventHandler(Of System.EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.MouseLeave, CType(d, EventHandler(Of System.EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Event handler. Called by  for mouse enter leave events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _MouseEnterLeave(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim cr As Drawing.Rectangle = RectangleToScreen(ClientRectangle)
        Dim mp As Drawing.Point = MousePosition

        ' actual state
        Dim isOver As Boolean = cr.Contains(mp)

        ' test if status changed
        If Me._mouseOver Xor isOver Then
            ' update state
            Me._mouseOver = isOver
            If Me._mouseOver Then
                Dim evt As EventHandler(Of EventArgs) = Me.MouseEnterEvent
                evt?.Invoke(Me, e)
            Else
                Dim evt As EventHandler(Of EventArgs) = Me.MouseLeaveEvent
                evt?.Invoke(Me, e)
            End If
        End If

        ' update UpDownButtons visibility
        If Me._UpDownDisplayMode <> UpDownButtonsDisplayMode.Always Then
            Me.UpdateUpDownButtonsVisibility()
        End If

    End Sub

#End Region

#Region " NEW EVENTS "

    ''' <summary> Event queue for all listeners interested in ValueDecrementing events. </summary>
    ''' <remarks> Raised BEFORE value decrements. </remarks>
    Public Event ValueDecrementing As EventHandler(Of CancelEventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveValueDecrementingEventHandler(ByVal value As EventHandler(Of CancelEventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.ValueDecrementing, CType(d, EventHandler(Of CancelEventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Event queue for all listeners interested in ValueIncrementing events. </summary>
    ''' <remarks> Raised BEFORE value increments. </remarks>
    Public Event ValueIncrementing As EventHandler(Of CancelEventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveValueIncrementingEventHandler(ByVal value As EventHandler(Of CancelEventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.ValueIncrementing, CType(d, EventHandler(Of CancelEventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Decrements the value of the spin box (also known as an up-down control). 
    '''           Raises the new  <see cref="ValueDecrementing">event</see>.
    '''           </summary>
    Public Overrides Sub DownButton()
        If Me.ReadOnly Then
            Return
        End If
        Dim e As New CancelEventArgs
        Dim evt As EventHandler(Of CancelEventArgs) = Me.ValueDecrementingEvent
        evt?.Invoke(Me, e)
        If e.Cancel Then Exit Sub
        ' decrement with wrap
        If Me.WrapValue AndAlso Value - Increment < Minimum Then
            Value = Maximum
        Else
            MyBase.DownButton()
        End If
    End Sub

    ''' <summary> Increments the value of the spin box (also known as an up-down control).
    '''           Raises the new  <see cref="ValueIncrementing">event</see>.
    '''            </summary>
    Public Overrides Sub UpButton()
        If Me.ReadOnly Then
            Return
        End If
        Dim e As New CancelEventArgs
        Dim evt As EventHandler(Of CancelEventArgs) = Me.ValueIncrementingEvent
        evt?.Invoke(Me, e)
        If e.Cancel Then Exit Sub
        ' increment with wrap
        If Me.WrapValue AndAlso Value + Increment > Maximum Then
            Value = Minimum
        Else
            MyBase.UpButton()
        End If
    End Sub

#End Region

#Region " VALUE "

    ''' <summary> Query if this object has value. </summary>
    ''' <returns> <c>true</c> if value; otherwise <c>false</c> </returns>
    Public Function HasValue() As Boolean
        Return Not String.IsNullOrWhiteSpace(Me.Text)
    End Function

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    <DefaultValue("")>
    <Description("Value"), Category("Appearance")>
    Public Overloads Property Value() As Decimal
        Get
            Return MyBase.Value
        End Get
        Set(ByVal value As Decimal)
            MyBase.Value = Math.Max(Me.Minimum, Math.Min(Me.Maximum, value))
            Me.Sync()
        End Set
    End Property

    ''' <summary> Gets or sets the null value. </summary>
    ''' <value> The null value. </value>
    Public Property NullValue As Decimal?
        Get
            If Me.HasValue Then
                Return Me.Value
            Else
                Return New Decimal?
            End If
        End Get
        Set(value As Decimal?)
            If value.HasValue Then
                Me.Value = value.Value
            Else
                Me.Text = ""
            End If
        End Set
    End Property

    ''' <summary> Determines whether the specified value is within the minimum/maximum range. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>True</c> if [is in range] [the specified value]; otherwise, <c>False</c>. </returns>
    Public Function IsInRange(ByVal value As Decimal) As Boolean
        Return value >= Me.Minimum AndAlso value <= Me.Maximum
    End Function

#End Region

End Class

''' <summary> Values that represent Up Down Buttons Display Mode. </summary>
Public Enum UpDownButtonsDisplayMode
    ''' <summary> Up-down buttons are always visible (default behavior). </summary>
    <Description("Up-down buttons are always visible (default behavior). ")> Always
    ''' <summary> Up-down buttons are visible only when mouse is over the control. </summary>
    <Description("Up-down buttons are visible only when mouse is over the control.")> WhenMouseOver
    ''' <summary> Up-down buttons are visible only when control has the focus. </summary>
    <Description("Up-down buttons are visible only when control has the focus.")> WhenFocus
    ''' <summary> Up-down buttons are visible when control has focus or mouse is over the control. </summary>
    <Description("Up-down buttons are visible when control has focus or mouse is over the control.")> WhenFocusOrMouseOver
End Enum

''' <summary> Values that represent Intercept Mouse Wheel Mode. </summary>
Public Enum InterceptMouseWheelMode
    ''' <summary> Mouse Wheel always works (default behavior). </summary>
    <Description("Mouse Wheel always works (default behavior)")> Always
    ''' <summary> Mouse Wheel works only when mouse is over the (focused) control. </summary>
    <Description("Mouse Wheel works only when mouse is over the (focused) control")> WhenMouseOver
    ''' <summary> Mouse Wheel never works. </summary>
    <Description("Mouse Wheel never works")> Never
End Enum


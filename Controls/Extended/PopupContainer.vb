﻿''' <summary> Pop-up container. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="10/6/2014" by="David" revision="2.1.5392"> Created. </history>
Public Class PopupContainer
    Inherits ToolStripDropDown

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Margin = Padding.Empty
        Me.Padding = Padding.Empty
    End Sub

    ''' <summary> Gets or sets the host. </summary>
    ''' <value> The host. </value>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private ReadOnly Property Host As ToolStripControlHost

    ''' <summary> Constructor. </summary>
    ''' <param name="containedControl"> The contained control. </param>
    Public Sub New(ByVal containedControl As Control)
        Me.New()
        Me._Host = Nothing
        Try
            Me._Host = New ToolStripControlHost(containedControl)
        Catch
            Me._Host?.Dispose() : Me._Host = Nothing
            Throw
        End Try
        Me._Host.BackColor = System.Drawing.Color.Transparent
        Me._Host.Margin = Padding.Empty
        Me._Host.Padding = Padding.Empty
        Me.Items.Add(Me._Host)
    End Sub

    ''' <summary> Releases the unmanaged resources used by the
    ''' <see cref="T:System.Windows.Forms.ToolStripDropDown" /> and optionally releases the managed
    ''' resources. </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    ''' release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' this causes stack overflow.
                ' If Me._host IsNot Nothing Me._host.Dispose(): Me._host = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

End Class

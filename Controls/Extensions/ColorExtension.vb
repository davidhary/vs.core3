﻿Namespace ColorExtensions
    ''' <summary> Graphics extension Methods. </summary>
    ''' <license>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    Public Module Methods

        ''' <summary> Converts a <see cref="Color"/> to a gray scale. </summary>
        ''' <param name="originalColor"> The original color. </param>
        ''' <returns> originalColor as a Drawing.Color. </returns>
        <System.Runtime.CompilerServices.Extension>
        Public Function ToGrayscale(ByVal originalColor As Drawing.Color) As Drawing.Color
            If originalColor.Equals(Drawing.Color.Transparent) Then
                Return originalColor
            End If

            Dim grayscale As Integer = CInt(Fix((originalColor.R * 0.299) + (originalColor.G * 0.587) + (originalColor.B * 0.114)))
            Return Drawing.Color.FromArgb(grayscale, grayscale, grayscale)
        End Function

    End Module

End Namespace

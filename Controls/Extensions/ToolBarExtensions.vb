﻿Imports System.Runtime.CompilerServices
Namespace ToolBarExtensions
    ''' <summary> Includes extensions for <see cref="ToolBar">Tool Bar</see>. </summary>
    ''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/19/2010" by="David" revision="1.2.3975.x"> Created. </history>
    Public Module Methods

        ''' <summary> Sets the <see cref="ToolBarButton">control</see> Image Index to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The ImageIndex box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeImageIndexSetter(ByVal control As ToolBarButton, ByVal value As Integer) As Integer
            If control IsNot Nothing AndAlso control.Parent IsNot Nothing Then
                If control.Parent.InvokeRequired Then
                    control.Parent.Invoke(New Action(Of ToolBarButton, Integer)(AddressOf ToolBarExtensions.SafeImageIndexSetter), New Object() {control, value})
                Else
                    control.ImageIndex = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="ToolBarButton">Tool Bar Button</see> Pushed value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. The setter disables the control before altering the Pushed state
        ''' allowing the control code to use the enabled state for preventing the execution of the
        ''' control Pushed change actions. </summary>
        ''' <param name="control"> Push box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentPushedSetter(ByVal control As ToolBarButton, ByVal value As Boolean) As Boolean
            If control IsNot Nothing AndAlso control.Parent IsNot Nothing Then
                If control.Parent.InvokeRequired Then
                    control.Parent.Invoke(New Action(Of ToolBarButton, Boolean)(AddressOf ToolBarExtensions.SafeSilentPushedSetter), New Object() {control, value})
                Else
                    Dim wasEnabled As Boolean = control.Enabled
                    control.Enabled = False
                    control.Pushed = value
                    control.Enabled = wasEnabled
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="ToolBarButton">Tool Bar Button</see> Pushed value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Push box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafePushedSetter(ByVal control As ToolBarButton, ByVal value As Boolean) As Boolean
            If control IsNot Nothing AndAlso control.Parent IsNot Nothing Then
                If control.Parent.InvokeRequired Then
                    control.Parent.Invoke(New Action(Of ToolBarButton, Boolean)(AddressOf ToolBarExtensions.SafePushedSetter), New Object() {control, value})
                Else
                    control.Pushed = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="ToolBarButton">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The <see cref="ToolBarButton"/> control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As ToolBarButton, ByVal value As String) As String
            If control IsNot Nothing AndAlso control.Parent IsNot Nothing Then
                If String.IsNullOrWhiteSpace(value) Then
                    value = ""
                End If
                If control.Parent.InvokeRequired Then
                    control.Parent.Invoke(New Action(Of ToolBarButton, String)(AddressOf ToolBarExtensions.SafeTextSetter), New Object() {control, value})
                Else
                    control.Text = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="ToolBarButton">control</see> enabled value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeVisibleSetter(ByVal control As ToolBarButton, ByVal value As Boolean) As Boolean
            If control IsNot Nothing AndAlso control.Parent IsNot Nothing Then
                If control.Parent.InvokeRequired Then
                    control.Parent.Invoke(New Action(Of Control, Boolean)(AddressOf SafeSetterExtensions.SafeVisibleSetter), New Object() {control, value})
                Else
                    control.Visible = value
                End If
            End If
            Return value
        End Function

    End Module
End Namespace

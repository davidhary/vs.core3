﻿Imports System.Runtime.CompilerServices
Namespace ControlExtensions
    ''' <summary> Includes extensions for controls. </summary>
    ''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/19/2010" by="David" revision="1.2.3975.x"> Created. </history>
    Public Module Methods

#Region " COPY PROPERTIES "

        ''' <summary> Copies properties from a source to a destination controls. </summary>
        ''' <remarks> Use this method to copy the properties of a control to a new control. </remarks>
        ''' <param name="source">      specifies an instance of the control which properties are copied
        ''' from. </param>
        ''' <param name="destination"> specifies an instance of the control which properties are copied
        ''' to. </param>
        ''' <example> This example adds a button to a form.
        ''' <code>
        ''' Private buttons As New ArrayList()
        ''' Private Sub AddButton(ByVal sourceButton As Button)
        ''' Dim newButton As New Button()
        ''' WinFormsSupport.CopyControlProperties(sourceButton, newButton)
        ''' Me.Controls.Add(newButton)
        ''' AddHandler newButton.Click, AddressOf Me._ExitButton_Click
        ''' With newButton
        ''' .Name = String.Format( System.Globalization.CultureInfo.CurrentCulture, "newButton {0}", buttons.count.toString)
        ''' .Text = .Name
        ''' .Top = .Top + .Height * (buttons.Count + 1)
        ''' buttons.Add(newButton)
        ''' End With
        ''' End Sub
        ''' </code>
        ''' To run this example, paste the code fragment into the method region of a Visual Basic form.
        ''' Run the program by pressing F5.</example>
        <Extension()>
        Public Sub CopyControlProperties(ByVal source As System.Windows.Forms.Control, ByVal destination As System.Windows.Forms.Control)

            If source Is Nothing OrElse destination Is Nothing Then Return
            Dim dontCopyNames() As String = {"WindowTarget"}
            Dim pinfos(), pinfo As System.Reflection.PropertyInfo
            If Not (source Is Nothing OrElse destination Is Nothing) Then
                pinfos = source.GetType.GetProperties()
                For Each pinfo In pinfos
                    ' copy only those properties without parameters - you'll rarely need indexed properties
                    If pinfo.GetIndexParameters().Length = 0 Then
                        ' skip properties that appear in the list of disallowed names.
                        If Array.IndexOf(dontCopyNames, pinfo.Name) < 0 Then
                            ' can only copy those properties that can be read and written.  
                            If pinfo.CanRead And pinfo.CanWrite Then
                                ' set the destination based on the source.
                                pinfo.SetValue(destination, pinfo.GetValue(source, Nothing), Nothing)
                            End If
                        End If
                    End If
                Next
            End If

        End Sub

#End Region

#Region " READ ONLY TAB STOPS "

        ''' <summary> Disables tab stops on read only text box. </summary>
        ''' <param name="controls"> The controls. </param>
        <Extension()>
        Public Sub DisableReadOnlyTabStop(ByVal controls As System.Windows.Forms.Control.ControlCollection)
            If controls IsNot Nothing Then
                For Each tb As System.Windows.Forms.TextBoxBase In controls.OfType(Of System.Windows.Forms.TextBoxBase)()
                    tb.TabStop = Not tb.ReadOnly
                Next
            End If
        End Sub

        ''' <summary> Disables tab stops on read only text box. </summary>
        ''' <param name="control"> The control to enable or disable. </param>
        <Extension()>
        Public Sub DisableReadOnlyTabStop(ByVal control As System.Windows.Forms.Control)
            If control IsNot Nothing AndAlso control.Controls IsNot Nothing Then
                control.Controls.DisableReadOnlyTabStop()
                For Each c As System.Windows.Forms.Control In control.Controls
                    c.DisableReadOnlyTabStop()
                Next
            End If
        End Sub

#End Region

#Region " ENABLE CONTROLS "

        ''' <summary> Recursively enables or disabled the control and all its children . </summary>
        ''' <param name="control"> The control to enable or disable. </param>
        ''' <param name="value">   Specifies a value to set. </param>
        <Extension()>
        Public Sub RecursivelyEnable(ByVal control As System.Windows.Forms.Control, ByVal value As Boolean)
            If control IsNot Nothing Then
                control.Enabled = value
                control.Controls.RecursivelyEnable(value)
            End If
        End Sub

        ''' <summary> Recursively enables or disabled the control and all its children . </summary>
        ''' <param name="controls"> The controls. </param>
        ''' <param name="value">    Specifies a value to set. </param>
        <Extension()>
        Public Sub RecursivelyEnable(ByVal controls As System.Windows.Forms.Control.ControlCollection, ByVal value As Boolean)
            If controls IsNot Nothing Then
                For Each c As System.Windows.Forms.Control In controls
                    c.RecursivelyEnable(value)
                Next
            End If
        End Sub

#End Region

    End Module
End Namespace

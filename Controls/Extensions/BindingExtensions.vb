﻿Imports System.Runtime.CompilerServices
Namespace BindingExtensions
    ''' <summary> Includes extensions for Binding. </summary>
    ''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/19/2010" by="David" revision="1.2.3975.x"> Created. </history>
    Public Module Methods

#Region " BINDING COLLECTIONS EXTENSIONS "

        ''' <summary> Clears all bindings. Verifies that binding indeed were cleared. </summary>
        ''' <remarks> The base <see cref="ControlBindingsCollection.Clear">clear</see> may not clear fast
        ''' enough for the binding to be cleared when a new binding is added. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value"> The value. </param>
        <Extension()>
        Public Sub ClearAll(ByVal value As ControlBindingsCollection)
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            Do While value.Count > 0
                Try
                    value.RemoveAt(0)
                    System.Windows.Forms.Application.DoEvents()
                Catch ex As ArgumentOutOfRangeException
                End Try
            Loop
        End Sub

        ''' <summary> Checks if the binding already exists. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">        The value. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        ''' <returns> <c>True</c> if binding exists, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Exists(ByVal value As ControlBindingsCollection, ByVal propertyName As String) As Boolean
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If String.IsNullOrWhiteSpace(propertyName) Then Throw New ArgumentNullException(NameOf(propertyName))
            For Each b As Binding In value
                If String.Equals(b.PropertyName, propertyName, StringComparison.OrdinalIgnoreCase) Then
                    Return True
                End If
            Next
            Return False
        End Function

        ''' <summary> Checks if the binding already exists. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   The value. </param>
        ''' <param name="binding"> The binding. </param>
        ''' <returns> <c>True</c> if binding exists, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Exists(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As Boolean
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            Return BindingExtensions.Exists(value, binding.PropertyName)
        End Function

        ''' <summary> Removes the specified binding based on the binding property name. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">        The value. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        <Extension()>
        Public Sub RemoveAndVerify(ByVal value As ControlBindingsCollection, ByVal propertyName As String)
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If String.IsNullOrWhiteSpace(propertyName) Then Throw New ArgumentNullException(NameOf(propertyName))
            Do While BindingExtensions.Exists(value, propertyName)
                For Each b As Binding In value
                    If String.Equals(b.PropertyName, propertyName, StringComparison.OrdinalIgnoreCase) Then
                        Try
                            value.Remove(b)
                            System.Windows.Forms.Application.DoEvents()
                        Catch ex As ArgumentOutOfRangeException
                            ' remove could cause an exception due to the way the binding is checked and released.
                            ' as it happens, the check for exists passes but the binding is no longer there when trying to
                            ' remove it.
                        End Try
                        Exit For
                    End If
                Next
            Loop
        End Sub

        ''' <summary> Removes the specified binding based on the binding property name. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   The value. </param>
        ''' <param name="binding"> The binding. </param>
        <Extension()>
        Public Sub RemoveAndVerify(ByVal value As ControlBindingsCollection, ByVal binding As Binding)
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            BindingExtensions.RemoveAndVerify(value, binding.PropertyName)
        End Sub

        ''' <summary> Replaces the binding for the bound property of the control. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   The value. </param>
        ''' <param name="binding"> The binding. </param>
        ''' <returns> System.Windows.Forms.Binding. </returns>
        <Extension()>
        Public Function Replace(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As System.Windows.Forms.Binding
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            BindingExtensions.RemoveAndVerify(value, binding)
            value.Add(binding)
            Return binding
        End Function

        ''' <summary> Selects an existing binding if there. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   The value. </param>
        ''' <param name="binding"> The binding. </param>
        ''' <returns> System.Windows.Forms.Binding. </returns>
        <Extension()>
        Public Function SelectExisting(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As System.Windows.Forms.Binding
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            For Each b As Binding In value
                If String.Equals(b.PropertyName, binding.PropertyName, StringComparison.OrdinalIgnoreCase) Then
                    Return b
                End If
            Next
            Return Nothing
        End Function

        ''' <summary> Adds a binding to the control. Disables the control while adding the binding to allow
        ''' the disabling of control events while the binding is added. Note that the control event of
        ''' value change occurs before the bound count increments so the bound count cannot be used to
        ''' determine the control bindable status. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   The value. </param>
        ''' <param name="binding"> . </param>
        ''' <returns> The binding. </returns>
        <Extension()>
        Public Function SilentAdd(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As System.Windows.Forms.Binding
            If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
            If binding Is Nothing Then Throw New ArgumentNullException(NameOf(binding))
            Dim isEnabled As Boolean = value.Control.Enabled
            value.Control.Enabled = False
            value.Add(binding)
            value.Control.Enabled = isEnabled
            Return binding
        End Function
#End Region

    End Module

End Namespace

﻿Imports System.Runtime.CompilerServices
Namespace RadioButtonExtensions
    ''' <summary> Includes extensions for <see cref="RadioButton">Radio Button</see>. </summary>
    ''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/19/2010" by="David" revision="1.2.3975.x"> Created. </history>
    Public Module Methods

        ''' <summary> Sets the <see cref="Control">check box</see> checked value to the
        ''' <paramref name="value">value</paramref>.
        ''' The setter disables the control before altering the checked state allowing the control code
        ''' to use the enabled state for preventing the execution of the control checked change actions. </summary>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SilentCheckedSetter(ByVal control As System.Windows.Forms.RadioButton, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                Dim wasEnabled As Boolean = control.Enabled
                control.Enabled = False
                control.Checked = value
                control.Enabled = wasEnabled
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Control">check box</see> checked value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. The setter disables the control before altering the checked state
        ''' allowing the control code to use the enabled state for preventing the execution of the
        ''' control checked change actions. </summary>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentCheckedSetter(ByVal control As System.Windows.Forms.RadioButton, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of RadioButton, Boolean)(AddressOf RadioButtonExtensions.SafeSilentCheckedSetter), New Object() {control, value})
                Else
                    SilentCheckedSetter(control, value)
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Control">check box</see> checked value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Check box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeCheckedSetter(ByVal control As System.Windows.Forms.RadioButton, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of RadioButton, Boolean)(AddressOf RadioButtonExtensions.SafeCheckedSetter), New Object() {control, value})
                Else
                    control.Checked = value
                End If
            End If
            Return value
        End Function

    End Module
End Namespace

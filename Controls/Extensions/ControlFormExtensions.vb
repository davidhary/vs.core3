﻿Imports System.Runtime.CompilerServices
Namespace ControlFormExtensions
    ''' <summary> Includes extensions for <see cref="Control"/> and <see cref="Form"/>. </summary>
    ''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/19/2010" by="David" revision="1.2.3975.x"> Created. </history>
    Public Module Methods

        ''' <summary> Returns the control requesting help based on the mouse position clicked. </summary>
        ''' <param name="container"> Specifies the container control requesting the help. </param>
        ''' <param name="helpEvent"> Help event information. </param>
        ''' <returns> Returns the control upon which the operator clicked with the help icon or nothing if
        ''' no control was clicked. </returns>
        <Extension()>
        Public Function ControlRequestingHelp(ByVal container As System.Windows.Forms.Control,
                                          ByVal helpEvent As System.Windows.Forms.HelpEventArgs) As System.Windows.Forms.Control

            If helpEvent Is Nothing OrElse container Is Nothing Then
                Return Nothing
            End If

            ' Convert screen coordinates to client coordinates
            Dim clientCoordinatePoint As System.Drawing.Point = container.PointToClient(helpEvent.MousePos)

            ' look for the control upon which the operator clicked.
            For i As Integer = 0 To container.Controls.Count
                If container.Controls.Item(i).Bounds.Contains(clientCoordinatePoint) Then
                    Return container.Controls.Item(i)
                End If
            Next

            ' if no control was located, return nothing
            Return Nothing

        End Function

    End Module
End Namespace

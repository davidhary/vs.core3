﻿Imports System.Runtime.CompilerServices
Namespace ListControlExtensions
    ''' <summary> Includes extensions for <see cref="ListControl">List Control</see>. </summary>
    ''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/19/2010" by="David" revision="1.2.3975.x"> Created. </history>
    Public Module Methods

        ''' <summary> Selects the <see cref="Control">combo box</see> index by setting the Selected Index
        ''' to the <paramref name="value">value</paramref>.
        ''' The control is disabled when set so that the handling of the changed event can be skipped. </summary>
        ''' <param name="control"> The control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> The index. </returns>
        <Extension()>
        Public Function SilentSelectIndex(ByVal control As ListControl, ByVal value As Integer) As Integer
            If control IsNot Nothing Then
                Dim enabled As Boolean = control.Enabled
                control.Enabled = False
                If control.SelectedIndex <> value Then
                    control.SelectedIndex = value
                End If
                control.Enabled = enabled
            End If
            Return value
        End Function

        ''' <summary> Gets the selected or default <paramref name="defaultValue">value</paramref>. </summary>
        ''' <param name="control">      The control. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> ``0. </returns>
        <Extension()>
        Public Function GetSelectedValue(Of T)(ByVal control As ListControl, ByVal defaultValue As T) As T
            If control Is Nothing OrElse control.SelectedValue Is Nothing Then
                Return defaultValue
            Else
                Return CType(control.SelectedValue, T)
            End If
        End Function

#If To_Remove Then
        ' Setting the Selected Value does not work!
        ''' <summary>
        ''' Selects the <see cref="Control">combo box</see> value by setting the Selected value to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
    ''' <typeparam name="T"></typeparam>
        ''' <param name="control">The control.</param>
        ''' <param name="value">The value.</param><returns></returns>
        <Extension()> 
        Public Function SafeSelectValue(Of T)(ByVal control As ListControl, ByVal value As T) As T
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ListControl, T)(AddressOf ListControlExtensions.SafeSelectValue), New Object() {control, value})
                Else
                    If control.SelectedValue Is Nothing OrElse Not control.SelectedValue.Equals(value) Then
                        control.SelectedValue = value
                    End If
                End If
            End If
            Return value
        End Function

        ''' <summary>
        ''' Selects the <see cref="Control">combo box</see> value by setting the Selected value to the 
        ''' <paramref name="value">value</paramref>.
        ''' The control is disabled when set so that the handling of the changed event can be skipped.
        ''' </summary>
    ''' <typeparam name="T"></typeparam>
        ''' <param name="control">The control.</param>
        ''' <param name="value">The value.</param><returns></returns>
        <Extension()> 
        Public Function SilentSelectValue(Of T)(ByVal control As ListControl, ByVal value As T) As T
            If control IsNot Nothing AndAlso control.SelectedValue Is Nothing OrElse Not control.SelectedValue.Equals(value) Then
                Dim enabled As Boolean = control.Enabled
                control.Enabled = False
                control.SelectedValue = value
                control.Enabled = enabled
            End If
            Return value
        End Function

        <Extension()>
        Public Function SilentSelectValue(Of T)(ByVal control As ComboBox, ByVal value As T) As T
            If control IsNot Nothing AndAlso control.SelectedValue Is Nothing OrElse Not control.SelectedValue.Equals(value) Then
                Dim enabled As Boolean = control.Enabled
                control.Enabled = False
                control.SelectedValue = value
                control.Enabled = enabled
            End If
            Return value
        End Function

        ''' <summary>
        ''' Selects the <see cref="Control">combo box</see> value by setting the Selected value to the 
        ''' <paramref name="value">value</paramref>.
        ''' The control is disabled when set so that the handling of the changed event can be skipped.
        ''' This setter is thread safe.
        ''' </summary>
    ''' <typeparam name="T"></typeparam>
        ''' <param name="control">The control.</param>
        ''' <param name="value">The value.</param><returns></returns>
        <Extension()> 
        Public Function SafeSilentSelectValue(Of T)(ByVal control As ListControl, ByVal value As T) As T
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, Object)(AddressOf ListControlExtensions.SafeSilentSelectValue), New Object() {control, value})
                Else
                    SilentSelectValue(Of T)(control, value)
                End If
            End If
            Return value
        End Function

#End If

    End Module
End Namespace
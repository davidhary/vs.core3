﻿Imports System.Runtime.CompilerServices
Namespace ProgressBarExtensions
    ''' <summary> Includes extensions for <see cref="ProgressBar">Progress Bar</see>. </summary>
    ''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/19/2010" by="David" revision="1.2.3975.x"> Created. </history>
    Public Module Methods

        ''' <summary> Draws text into a  <see cref="System.Windows.Forms.ProgressBar">progress bar</see>
        ''' center middle using the control font. </summary>
        ''' <remarks> Call this method after changing the progress bar's value. If for some reason, the
        ''' changing of the progress bar's value doesn't refresh it and clear the previously drawn text,
        ''' call the Refresh method of the progress bar before calling this method. </remarks>
        ''' <param name="control"> The target progress bar to add text into. </param>
        ''' <param name="value">   The text to add into the progress bar. Leave null or empty to
        ''' automatically add the percent. </param>
        <Extension()>
        Public Sub SafelyDrawText(ByVal control As System.Windows.Forms.ProgressBar, ByVal value As String)
            If control IsNot Nothing Then
                ProgressBarExtensions.SafelyDrawText(control, value, Drawing.ContentAlignment.MiddleCenter, control.Font)
            End If
        End Sub

        ''' <summary> Draws text into a  <see cref="System.Windows.Forms.ProgressBar">progress bar</see>. </summary>
        ''' <remarks> Call this method after changing the progress bar's value. If for some reason, the
        ''' changing of the progress bar's value doesn't refresh it and clear the previously drawn text,
        ''' call the Refresh method of the progress bar before calling this method. </remarks>
        ''' <param name="control">   The target progress bar to add text into. </param>
        ''' <param name="value">     The text to add into the progress bar. Leave null or empty to
        ''' automatically add the percent. </param>
        ''' <param name="textAlign"> Where the text is to be placed. </param>
        <Extension()>
        Public Sub SafelyDrawText(ByVal control As System.Windows.Forms.ProgressBar, ByVal value As String, ByVal textAlign As Drawing.ContentAlignment)
            If control IsNot Nothing Then
                If String.IsNullOrWhiteSpace(value) Then
                    Dim fraction As Double = CDbl(control.Value - control.Minimum) / CDbl(control.Maximum - control.Minimum)
                    value = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:0%}", fraction)
                End If
                SafeSetterExtensions.SafelyDrawText(CType(control, System.Windows.Forms.Control), value, textAlign, control.Font)
            End If
        End Sub

        ''' <summary> Draws text into a  <see cref="System.Windows.Forms.ProgressBar">progress bar</see>. </summary>
        ''' <remarks> Call this method after changing the progress bar's value. If for some reason, the
        ''' changing of the progress bar's value doesn't refresh it and clear the previously drawn text,
        ''' call the Refresh method of the progress bar before calling this method. </remarks>
        ''' <param name="control">   The target progress bar to add text into. </param>
        ''' <param name="value">     The text to add into the progress bar. Leave null or empty to
        ''' automatically add the percent. </param>
        ''' <param name="textAlign"> Where the text is to be placed. </param>
        ''' <param name="textFont">  The font the text should be drawn in. </param>
        <Extension()>
        Public Sub SafelyDrawText(ByVal control As System.Windows.Forms.ProgressBar, ByVal value As String, ByVal textAlign As Drawing.ContentAlignment,
                                  ByVal textFont As System.Drawing.Font)
            If control IsNot Nothing Then
                If String.IsNullOrWhiteSpace(value) Then
                    control.Refresh()
                    Dim fraction As Double = CDbl(control.Value - control.Minimum) / CDbl(control.Maximum - control.Minimum)
                    value = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:0%}", fraction)
                End If
                SafeSetterExtensions.SafelyDrawText(CType(control, System.Windows.Forms.Control), value, textAlign, textFont)
            End If
        End Sub

        ''' <summary> Sets the <see cref="ProgressBar">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range. </summary>
        ''' <param name="control"> The progress bar control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function ValueSetter(ByVal control As ProgressBar, ByVal value As Integer) As Integer
            If control IsNot Nothing Then
                value = If(value < control.Minimum, control.Minimum, If(value > control.Maximum, control.Maximum, value))
                If control.Value <> value Then control.Value = value
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="ProgressBar">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range. </summary>
        ''' <param name="control"> The progress bar control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function ValueUpdater(ByVal control As ProgressBar, ByVal value As Integer) As Integer
            If control IsNot Nothing Then
                If control.Value <> value Then
                    Return ProgressBarExtensions.ValueSetter(control, value)
                End If
                Return CInt(control.Value)
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="ProgressBar">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The progress bar control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function SafeValueSetter(ByVal control As ProgressBar, ByVal value As Integer) As Integer
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ProgressBar, Integer)(AddressOf ProgressBarExtensions.SafeValueSetter), New Object() {control, value})
                Else
                    value = If(value < control.Minimum, control.Minimum, If(value > control.Maximum, control.Maximum, value))
                    If control.Value <> value Then control.Value = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Updates the <see cref="ProgressBar">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The progress bar control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function SafeValueUpdater(ByVal control As ProgressBar, ByVal value As Integer) As Integer
            If control IsNot Nothing Then
                If control.Value <> value Then
                    Return ProgressBarExtensions.SafeValueSetter(control, value)
                End If
            End If
            Return value
        End Function

    End Module
End Namespace

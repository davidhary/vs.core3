﻿Imports System.DirectoryServices.AccountManagement
Imports isr.Core.Pith.ExceptionExtensions
''' <summary> Machine log on. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="10/6/2014" by="David" revision="2.1.5392"> Created. </history>
Public Class MachineLogOn
    Inherits LogOnBase

    ''' <summary> Validates a user name and password. </summary>
    ''' <param name="userName"> Specifies a user name. </param>
    ''' <param name="password"> Specifies a password. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overloads Overrides Function Authenticate(ByVal userName As String, ByVal password As String) As Boolean
        Dim result As Boolean = False
        If String.IsNullOrWhiteSpace(userName) Then
            Me.ValidationMessage = String.Format("User name is empty")
        ElseIf String.IsNullOrWhiteSpace(password) Then
            Me.ValidationMessage = String.Format("User password is empty")
        Else
            Try
                Using ctx As PrincipalContext = New PrincipalContext(ContextType.Machine)
                    Using user As UserPrincipal = UserPrincipal.FindByIdentity(ctx, userName)
                        If user Is Nothing Then
                            Me.ValidationMessage = String.Format("User '{0}' not identified on this machine", userName)
                        Else
                            Me.UserRoles = DomainLogOn.EnumerateUserRoles(user)
                            result = ctx.ValidateCredentials(userName, password)
                            If result Then
                                MyBase.UserName = userName
                            Else
                                MyBase.UserName = ""
                                Me.ValidationMessage = String.Format("Failed validating credentials for user '{0}'", userName)
                            End If
                        End If
                    End Using
                End Using
            Catch ex As Exception
                Me.ValidationMessage = ex.ToFullBlownString
            End Try
        End If
        Me.IsAuthenticated = result
        Me.Failed = Not result
        Return result
    End Function

    ''' <summary> Tries to find a user in a group role. </summary>
    ''' <param name="userName">         Specifies a user name. </param>
    ''' <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    ''' the name of enumeration flags. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overloads Overrides Function TryFindUser(userName As String, ByVal allowedUserRoles As ArrayList) As Boolean
        Dim result As Boolean = False
        Me.Failed = False
        If String.IsNullOrWhiteSpace(userName) Then
            Me.ValidationMessage = String.Format("User name is empty")
            Me.Failed = True
        ElseIf allowedUserRoles Is Nothing Then
            Me.ValidationMessage = String.Format("User roles not specified")
            Me.Failed = True
        ElseIf allowedUserRoles.Count = 0 Then
            Me.ValidationMessage = String.Format("Expected user roles not set")
            Me.Failed = True
        Else
            Try
                Using ctx As PrincipalContext = New PrincipalContext(ContextType.Machine)
                    Using user As UserPrincipal = UserPrincipal.FindByIdentity(ctx, userName)
                        If user Is Nothing Then
                            Me.ValidationMessage = String.Format("User '{0}' not identified on this machine", userName)
                        Else
                            Me.UserRoles = DomainLogOn.EnumerateUserRoles(user)
                            If DomainLogOn.EnumerateUserRoles(user, allowedUserRoles).Count > 0 Then
                                MyBase.UserName = userName
                                result = True
                            End If
                            If Not result Then
                                Me.ValidationMessage = String.Format("User '{0}' role was not found among the allowed user roles.", userName)
                            End If
                        End If
                    End Using
                End Using
            Catch ex As Exception
                Me.ValidationMessage = ex.ToFullBlownString
            End Try
        End If
        Return result
    End Function

End Class

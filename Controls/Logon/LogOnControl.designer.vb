﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class LogOnControl

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._passwordTextBox = New System.Windows.Forms.TextBox()
        Me._passwordTextBoxLabel = New System.Windows.Forms.Label()
        Me._userNameTextBox = New System.Windows.Forms.TextBox()
        Me._userNameTextBoxLabel = New System.Windows.Forms.Label()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._LogOnLinkLabel = New System.Windows.Forms.LinkLabel()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_passwordTextBox
        '
        Me._passwordTextBox.Location = New System.Drawing.Point(95, 31)
        Me._passwordTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._passwordTextBox.Name = "_passwordTextBox"
        Me._passwordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me._passwordTextBox.Size = New System.Drawing.Size(81, 25)
        Me._passwordTextBox.TabIndex = 3
        '
        '_passwordTextBoxLabel
        '
        Me._passwordTextBoxLabel.Location = New System.Drawing.Point(5, 30)
        Me._passwordTextBoxLabel.Name = "_passwordTextBoxLabel"
        Me._passwordTextBoxLabel.Size = New System.Drawing.Size(90, 26)
        Me._passwordTextBoxLabel.TabIndex = 2
        Me._passwordTextBoxLabel.Text = "Password: "
        Me._passwordTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_userNameTextBox
        '
        Me._userNameTextBox.Location = New System.Drawing.Point(96, 4)
        Me._userNameTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._userNameTextBox.Name = "_userNameTextBox"
        Me._userNameTextBox.Size = New System.Drawing.Size(81, 25)
        Me._userNameTextBox.TabIndex = 1
        Me._userNameTextBox.Text = "David"
        '
        '_userNameTextBoxLabel
        '
        Me._userNameTextBoxLabel.Location = New System.Drawing.Point(6, 3)
        Me._userNameTextBoxLabel.Name = "_userNameTextBoxLabel"
        Me._userNameTextBoxLabel.Size = New System.Drawing.Size(89, 26)
        Me._userNameTextBoxLabel.TabIndex = 0
        Me._userNameTextBoxLabel.Text = "User Name: "
        Me._userNameTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_LogOnLinkLabel
        '
        Me._LogOnLinkLabel.Location = New System.Drawing.Point(113, 60)
        Me._LogOnLinkLabel.Name = "_LogOnLinkLabel"
        Me._LogOnLinkLabel.Size = New System.Drawing.Size(59, 17)
        Me._LogOnLinkLabel.TabIndex = 4
        Me._LogOnLinkLabel.TabStop = True
        Me._LogOnLinkLabel.Text = "Log In"
        Me._LogOnLinkLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LogOnControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Controls.Add(Me._LogOnLinkLabel)
        Me.Controls.Add(Me._passwordTextBox)
        Me.Controls.Add(Me._passwordTextBoxLabel)
        Me.Controls.Add(Me._userNameTextBox)
        Me.Controls.Add(Me._userNameTextBoxLabel)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "LogOnControl"
        Me.Size = New System.Drawing.Size(179, 81)
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _PasswordTextBox As System.Windows.Forms.TextBox
    Private WithEvents _PasswordTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _UserNameTextBox As System.Windows.Forms.TextBox
    Private WithEvents _UserNameTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _LogOnLinkLabel As System.Windows.Forms.LinkLabel

End Class

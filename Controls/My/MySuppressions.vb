Imports System.Diagnostics.CodeAnalysis
#Region "CA1020:AvoidNamespacesWithFewTypes"

' Namespaces should generally have more than five types. 
' Namespaces, not currently having the prescribed type count, were defined with an eye towards inclusion of additional future types.
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes",
    Scope:="namespace", Target:="isr.Core.Controls",
    Justification:="Ignoring this warning...we want these namespaces, but don't have enough classes to go in them to satisfy the rule.")> 

<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.BindingExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.ButtonExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.CheckBoxExtensions")>
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.ColorExtensions")>
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.ComboBoxExtensions")>
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.ControlCollectionExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.ControlContainerExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.ControlExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.ControlFormExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.ControlImageExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.DataGridViewExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.LabelExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.ListControlExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.NumericUpDownExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.PanelExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.ProgressBarExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.RadioButtonExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.SafeSetterExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.StatusBarExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.TableLayoutPanelExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.TextBoxExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.ToolBarExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.ToolStripExtensions")> 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.TreeViewExtensions")>

#End Region

<Assembly: CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="4#", Scope:="member", Target:="isr.Core.Controls.DataGridViewExtensions.Extensions.#Print(System.Windows.Forms.DataGridView,System.Drawing.Printing.PrintPageEventArgs,System.Int32&,System.Boolean&,System.Single&)")>
<Assembly: CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="4#", Scope:="member", Target:="isr.Core.Controls.DataGridViewExtensions.Extensions.#Print(System.Windows.Forms.DataGridView,System.Int32[],System.Drawing.Printing.PrintPageEventArgs,System.Int32&,System.Boolean&)")>
<Assembly: CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="3#", Scope:="member", Target:="isr.Core.Controls.DataGridViewExtensions.Extensions.#Print(System.Windows.Forms.DataGridView,System.Drawing.Printing.PrintPageEventArgs,System.Int32&,System.Boolean&,System.Single&)")>
<Assembly: CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="3#", Scope:="member", Target:="isr.Core.Controls.DataGridViewExtensions.Extensions.#Print(System.Windows.Forms.DataGridView,System.Int32[],System.Drawing.Printing.PrintPageEventArgs,System.Int32&,System.Boolean&)")>
<Assembly: CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#", Scope:="member", Target:="isr.Core.Controls.DataGridViewExtensions.Extensions.#Print(System.Windows.Forms.DataGridView,System.Drawing.Printing.PrintPageEventArgs,System.Int32&,System.Boolean&,System.Single&)")>
<Assembly: CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="4#", Scope:="member", Target:="isr.Core.Controls.DataGridViewExtensions.Extensions.#Print(isr.Core.Controls.DataGridView,System.Drawing.Printing.PrintPageEventArgs,System.Int32&,System.Boolean&,System.Single&)")>
<Assembly: CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="4#", Scope:="member", Target:="isr.Core.Controls.DataGridViewExtensions.Extensions.#Print(isr.Core.Controls.DataGridView,System.Int32[],System.Drawing.Printing.PrintPageEventArgs,System.Int32&,System.Boolean&)")>
<Assembly: CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="3#", Scope:="member", Target:="isr.Core.Controls.DataGridViewExtensions.Extensions.#Print(isr.Core.Controls.DataGridView,System.Drawing.Printing.PrintPageEventArgs,System.Int32&,System.Boolean&,System.Single&)")>
<Assembly: CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="3#", Scope:="member", Target:="isr.Core.Controls.DataGridViewExtensions.Extensions.#Print(isr.Core.Controls.DataGridView,System.Int32[],System.Drawing.Printing.PrintPageEventArgs,System.Int32&,System.Boolean&)")>
<Assembly: CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#", Scope:="member", Target:="isr.Core.Controls.DataGridViewExtensions.Extensions.#Print(isr.Core.Controls.DataGridView,System.Drawing.Printing.PrintPageEventArgs,System.Int32&,System.Boolean&,System.Single&)")>
<Assembly: CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Core.Controls.DrawingExtensions")>


﻿Imports System.Drawing.Drawing2D
''' <summary> A shaped button. </summary>
''' <license>
''' (c) 2017 Pritam Zope, All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/8/2017" by="David" revision="3.1.6276"> 
''' https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane. </history>
Public Class ShapedButton
    Inherits System.Windows.Forms.Button

#Region " CONSTRUCTION "

    Public Sub New()
        Me.Size = New Size(100, 40)
        Me.BackColor = Color.Transparent
        Me.FlatStyle = FlatStyle.Flat
        Me.FlatAppearance.BorderSize = 0
        Me.FlatAppearance.MouseOverBackColor = Color.Transparent
        Me.FlatAppearance.MouseDownBackColor = Color.Transparent
        _ButtonText = Me.Text
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 20.25!, FontStyle.Bold, GraphicsUnit.Point, CType(0, Byte))
    End Sub

#End Region

#Region " SHAPE "

    Private _RadiusPercent As Integer = 25
    Public Property RadiusPercent As Integer
        Get
            Return Me._RadiusPercent
        End Get
        Set(value As Integer)
            Me._RadiusPercent = value
            Me.Invalidate()
        End Set
    End Property

    Private _ButtonShape As ButtonShape

    Public Property ButtonShape() As ButtonShape
        Get
            Return _ButtonShape
        End Get
        Set(ByVal value As ButtonShape)
            _ButtonShape = value
            Invalidate()
        End Set
    End Property

#End Region

#Region " TEXT "

    Private _ButtonText As String = ""

    Public Property ButtonText() As String
        Get
            Return _ButtonText
        End Get
        Set(ByVal value As String)
            _ButtonText = value
            Invalidate()
        End Set
    End Property

    Private _TextLocation As Point = New Point(100, 25)
    Public Property TextLocation As Point
        Get
            Return Me._TextLocation
        End Get
        Set(value As Point)
            Me._TextLocation = value
            Me.Invalidate()
        End Set
    End Property

    Private _ShowButtonText As Boolean = True

    Public Property ShowButtonText() As Boolean
        Get
            Return Me._ShowButtonText
        End Get
        Set(ByVal value As Boolean)
            Me._ShowButtonText = value
            Me.Invalidate()
        End Set
    End Property

#End Region

#Region " BORDER "

    Private _BorderWidth As Integer = 2
    Public Property BorderWidth() As Integer
        Get
            Return _BorderWidth
        End Get
        Set(ByVal value As Integer)
            _BorderWidth = value
            Invalidate()
        End Set
    End Property


    Private Sub SetBorderColor(ByVal bdrColor As Color)
        Dim red As Integer = bdrColor.R - 40
        Dim green As Integer = bdrColor.G - 40
        Dim blue As Integer = bdrColor.B - 40
        If red < 0 Then
            red = 0
        End If
        If green < 0 Then
            green = 0
        End If
        If blue < 0 Then
            blue = 0
        End If

        _ButtonBorderColor = Color.FromArgb(red, green, blue)
    End Sub


    Private _ButtonBorderColor As Color = Color.FromArgb(220, 220, 220)
    Private _BorderColor As Color = Color.Transparent

    Public Property BorderColor() As Color
        Get
            Return _BorderColor
        End Get
        Set(ByVal value As Color)
            _BorderColor = value
            If _BorderColor = Color.Transparent Then
                _ButtonBorderColor = Color.FromArgb(220, 220, 220)
            Else
                SetBorderColor(_BorderColor)
            End If

        End Set
    End Property

#End Region

#Region " BUTTON COLORS "

    Private _StartColor As Color = Color.DodgerBlue
    Public Property StartColor() As Color
        Get
            Return _StartColor
        End Get
        Set(ByVal value As Color)
            _StartColor = value
            Invalidate()
        End Set
    End Property

    Private _EndColor As Color = Color.MidnightBlue

    Public Property EndColor() As Color
        Get
            Return _EndColor
        End Get
        Set(ByVal value As Color)
            _EndColor = value
            Invalidate()
        End Set
    End Property

    Private _MouseHoverStartColor As Color = Color.Turquoise

    Public Property MouseHoverStartColor() As Color
        Get
            Return _MouseHoverStartColor
        End Get
        Set(ByVal value As Color)
            _MouseHoverStartColor = value
            Invalidate()
        End Set
    End Property

    Private _MouseHoverEndColor As Color = Color.DarkSlateGray
    Public Property MouseHoverEndColor() As Color
        Get
            Return _MouseHoverEndColor
        End Get
        Set(ByVal value As Color)
            _MouseHoverEndColor = value
            Invalidate()
        End Set
    End Property

    Private _MouseClickStartColor As Color = Color.Yellow

    Public Property MouseClickStartColor() As Color
        Get
            Return _MouseClickStartColor
        End Get
        Set(ByVal value As Color)
            _MouseClickStartColor = value
            Invalidate()
        End Set
    End Property

    Private _MouseClickEndColor As Color = Color.Red
    Public Property MouseClickEndColor() As Color
        Get
            Return _MouseClickEndColor
        End Get
        Set(ByVal value As Color)
            _MouseClickEndColor = value
            Invalidate()
        End Set
    End Property

    Private _StartOpacity As Integer = 250
    Public Property StartOpacity() As Integer
        Get
            Return _StartOpacity
        End Get
        Set(ByVal value As Integer)
            _StartOpacity = value
            If _StartOpacity > 255 Then
                _StartOpacity = 255
                Invalidate()
            Else
                Invalidate()
            End If
        End Set
    End Property

    Private _EndOpacity As Integer = 250
    Public Property EndOpacity() As Integer
        Get
            Return _EndOpacity
        End Get
        Set(ByVal value As Integer)
            _EndOpacity = value
            If _EndOpacity > 255 Then
                _EndOpacity = 255
                Invalidate()
            Else
                Invalidate()
            End If
        End Set
    End Property

    Private _GradientAngle As Integer = 90
    Public Property GradientAngle() As Integer
        Get
            Return _GradientAngle
        End Get
        Set(ByVal value As Integer)
            _GradientAngle = value
            Invalidate()
        End Set
    End Property


    Private _CachedStartColor, _CachedEndColor As Color
    Protected Overrides Sub OnMouseEnter(ByVal e As EventArgs)
        MyBase.OnMouseEnter(e)
        _CachedStartColor = _StartColor
        _CachedEndColor = _EndColor
        _StartColor = _MouseHoverStartColor
        _EndColor = _MouseHoverEndColor
    End Sub

    Protected Overrides Sub OnMouseLeave(ByVal e As EventArgs)
        MyBase.OnMouseLeave(e)
        _StartColor = _CachedStartColor
        _EndColor = _CachedEndColor
        SetBorderColor(_BorderColor)
    End Sub

    Protected Overrides Sub OnMouseDown(ByVal mevent As MouseEventArgs)
        MyBase.OnMouseDown(mevent)
        _StartColor = _MouseClickStartColor
        _EndColor = _MouseClickEndColor

        _ButtonBorderColor = _BorderColor
        SetBorderColor(_BorderColor)
        Me.Invalidate()
    End Sub

    Protected Overrides Sub OnMouseUp(ByVal mevent As MouseEventArgs)
        MyBase.OnMouseUp(mevent)
        OnMouseLeave(mevent)
        _StartColor = _CachedStartColor
        _EndColor = _CachedEndColor
        SetBorderColor(_BorderColor)
        Me.Invalidate()
    End Sub

    Protected Overrides Sub OnLostFocus(ByVal e As EventArgs)
        MyBase.OnLostFocus(e)
        _StartColor = _CachedStartColor
        _EndColor = _CachedEndColor
        Me.Invalidate()
    End Sub

#End Region

#Region " RENDER "

    Protected Overrides Sub OnResize(ByVal e As EventArgs)
        MyBase.OnResize(e)
        Me.TextLocation = New Point(CInt((Me.Width \ 3) - 1), CInt((Me.Height \ 3) + 5))
    End Sub


    'draw circular button function
    Private Sub DrawCircularButton(ByVal g As Graphics)
        Dim c1 As Color = Color.FromArgb(_StartOpacity, _StartColor)
        Dim c2 As Color = Color.FromArgb(_EndOpacity, _EndColor)


        Using b As Brush = New System.Drawing.Drawing2D.LinearGradientBrush(ClientRectangle, c1, c2, _GradientAngle)
            g.FillEllipse(b, 5, 5, Me.Width - 10, Me.Height - 10)
        End Using

        Using b As New SolidBrush(_ButtonBorderColor)
            Using p As New Pen(b)
                For i As Integer = 0 To _BorderWidth - 1
                    g.DrawEllipse(p, 5 + i, 5, Me.Width - 10, Me.Height - 10)
                Next i
            End Using
        End Using


        If Me._ShowButtonText Then
            Dim p As New Point(Me.TextLocation.X, Me.TextLocation.Y)
            Using sb As New SolidBrush(Me.ForeColor)
                g.DrawString(_ButtonText, Me.Font, sb, p)
            End Using
        End If
    End Sub


    'draw round rectangular button function
    Private Sub DrawRoundRectangularButton(ByVal g As Graphics)

        Dim c1 As Color = Color.FromArgb(_StartOpacity, _StartColor)
        Dim c2 As Color = Color.FromArgb(_EndOpacity, _EndColor)

        Dim rec As New Rectangle(5, 5, Me.Width, Me.Height)

        ' create the radius variable and set it equal to 20% the height of the rectangle
        ' this will determine the amount of bend at the corners
        Dim radius As Integer = CInt(Fix(0.01 * Math.Min(rec.Height, rec.Height) * Me.RadiusPercent))

        ' make sure that we have a valid radius, too small and we have a problem
        If radius < 1 Then radius = 2
        Dim diameter As Integer = radius * 2

        ' create an x and y variable so that we can reduce the length of our code lines
        Dim x As Integer = rec.Left
        Dim y As Integer = rec.Top
        Dim h As Integer = rec.Height
        Dim w As Integer = rec.Width

        Using _region As Region = New System.Drawing.Region(rec)
            Using path As New GraphicsPath()
                path.AddArc(X, Y, diameter, diameter, 180, 90)
                path.AddArc(W - diameter - X, Y, diameter, diameter, 270, 90)
                path.AddArc(W - diameter - X, H - diameter - Y, diameter, diameter, 0, 90)
                path.AddArc(X, H - diameter - Y, diameter, diameter, 90, 90)
                path.CloseFigure()
                _region.Intersect(path)
            End Using

            Using b As Brush = New System.Drawing.Drawing2D.LinearGradientBrush(ClientRectangle, c1, c2, _GradientAngle)
                g.FillRegion(b, _region)
            End Using
        End Using


        Using p As New Pen(Me._ButtonBorderColor)
            For i As Integer = 0 To _BorderWidth - 1
                g.DrawArc(p, X + i, Y + i, diameter, diameter, 180, 90)
                g.DrawLine(p, X + radius, Y + i, Me.Width - X - radius, Y + i)
                g.DrawArc(p, Me.Width - diameter - X - i, Y + i, diameter, diameter, 270, 90)
                g.DrawLine(p, X + i, Y + radius, X + i, Me.Height - Y - radius)

                g.DrawLine(p, Me.Width - X - i, Y + radius, Me.Width - X - i, Me.Height - Y - radius)
                g.DrawArc(p, Me.Width - diameter - X - i, Me.Height - diameter - Y - i, diameter, diameter, 0, 90)
                g.DrawLine(p, X + radius, Me.Height - Y - i, Me.Width - X - radius, Me.Height - Y - i)
                g.DrawArc(p, X + i, Me.Height - diameter - Y - i, diameter, diameter, 90, 90)
            Next i
        End Using

        If _ShowButtonText Then
            Using sb As New SolidBrush(Me.ForeColor)
                g.DrawString(Me.ButtonText, Me.Font, sb, Me.TextLocation)
            End Using
        End If

    End Sub


    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e Is Nothing Then Return
        e.Graphics.SmoothingMode = SmoothingMode.AntiAlias
        MyBase.OnPaint(e)

        Select Case _ButtonShape
            Case ButtonShape.Circle
                Me.DrawCircularButton(e.Graphics)

            Case ButtonShape.RoundRect
                Me.DrawRoundRectangularButton(e.Graphics)
        End Select
    End Sub

#End Region

End Class

''' <summary> Values that represent button shapes. </summary>
Public Enum ButtonShape
    RoundRect
    Circle
End Enum

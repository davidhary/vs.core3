Imports System.Drawing.Drawing2D
''' <summary> A Zope tab control. </summary>
''' <license>
''' (c) 2017 Pritam Zope, All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/8/2017" by="David" revision="3.1.6276"> 
''' https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane. </history>
Public Class ZopeTabControl
    Inherits System.Windows.Forms.TabControl

    Public Sub New()
        MyBase.New
        Me.DrawMode = TabDrawMode.OwnerDrawFixed
        Me.Padding = New System.Drawing.Point(22, 4)
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
    End Sub


    Private _ActiveTabStartColor As Color = Color.Yellow
    Public Property ActiveTabStartColor() As Color
        Get
            Return _ActiveTabStartColor
        End Get
        Set(ByVal value As Color)
            _ActiveTabStartColor = value
            Invalidate()
        End Set
    End Property


    Private _ActiveTabEndColor As Color = Color.DarkOrange
    Public Property ActiveTabEndColor() As Color
        Get
            Return _ActiveTabEndColor
        End Get
        Set(ByVal value As Color)
            _ActiveTabEndColor = value
            Invalidate()
        End Set
    End Property


    Private _InactiveTabStartColor As Color = Color.LightGreen

    Public Property InactiveTabStartColor() As Color
        Get
            Return _InactiveTabStartColor
        End Get
        Set(ByVal value As Color)
            _InactiveTabStartColor = value
            Invalidate()
        End Set
    End Property

    Private _InactiveTabEndColor As Color = Color.DarkBlue
    Public Property InactiveTabEndColor() As Color
        Get
            Return _InactiveTabEndColor
        End Get
        Set(ByVal value As Color)
            _InactiveTabEndColor = value
            Invalidate()
        End Set
    End Property

    Private _StartOpacity As Integer = 150


    Public Property StartOpacity() As Integer
        Get
            Return _StartOpacity
        End Get
        Set(ByVal value As Integer)
            _StartOpacity = value
            If _StartOpacity > 255 Then
                _StartOpacity = 255
                Invalidate()
            Else
                Invalidate()
            End If
        End Set
    End Property


    Private _EndOpacity As Integer = 150
    Public Property EndOpacity() As Integer
        Get
            Return _EndOpacity
        End Get
        Set(ByVal value As Integer)
            _EndOpacity = value
            If _EndOpacity > 255 Then
                _EndOpacity = 255
                Invalidate()
            Else
                Invalidate()
            End If
        End Set
    End Property

    Private _GradientAngle As Integer = 90

    Public Property GradientAngle() As Integer
        Get
            Return _GradientAngle
        End Get
        Set(ByVal value As Integer)
            _GradientAngle = value
            Invalidate()
        End Set
    End Property

    Private _TextColor As Color = Color.Navy

    Public Property TextColor() As Color
        Get
            Return _TextColor
        End Get
        Set(ByVal value As Color)
            _TextColor = value
            Invalidate()
        End Set
    End Property

    Private _CloseButtonColor As Color = Color.Red

    Public Property CloseButtonColor() As Color
        Get
            Return _CloseButtonColor
        End Get
        Set(ByVal value As Color)
            _CloseButtonColor = value
            Invalidate()
        End Set
    End Property

    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        MyBase.OnPaint(e)
    End Sub


    'method for drawing tab items 
    Protected Overrides Sub OnDrawItem(ByVal e As DrawItemEventArgs)
        If e Is Nothing Then Return
        MyBase.OnDrawItem(e)
        Dim rc As Rectangle = GetTabRect(e.Index)

        'if tab is selected
        If String.Equals(Me.SelectedTab.Text, Me.TabPages(e.Index).Text) Then
            Dim c1 As Color = Color.FromArgb(_StartOpacity, _ActiveTabStartColor)
            Dim c2 As Color = Color.FromArgb(_EndOpacity, _ActiveTabEndColor)
            Using br As New LinearGradientBrush(rc, c1, c2, _GradientAngle)
                e.Graphics.FillRectangle(br, rc)
            End Using
        Else
            Dim c1 As Color = Color.FromArgb(_StartOpacity, _InactiveTabStartColor)
            Dim c2 As Color = Color.FromArgb(_EndOpacity, _InactiveTabEndColor)
            Using br As New LinearGradientBrush(rc, c1, c2, _GradientAngle)
                e.Graphics.FillRectangle(br, rc)
            End Using
        End If

        Me.TabPages(e.Index).BorderStyle = BorderStyle.FixedSingle
        Me.TabPages(e.Index).ForeColor = SystemColors.ControlText

        'draw close button on tabs

        Dim paddedBounds As Rectangle = e.Bounds
        paddedBounds.Inflate(-5, -4)
        Using b As New SolidBrush(Me._TextColor)
            e.Graphics.DrawString(Me.TabPages(e.Index).Text, Me.Font, b, paddedBounds)
        End Using

        Dim pad As Point = Me.Padding

        'drawing close button to tab items
        Using b As New SolidBrush(Me._CloseButtonColor)
            Using f As New Font(Me.Font.FontFamily, 10, FontStyle.Bold)
                e.Graphics.DrawString("X", f, b, e.Bounds.Right + 1 - 18, e.Bounds.Top + pad.Y - 2)
            End Using
        End Using
        e.DrawFocusRectangle()
    End Sub


    'action for when mouse click on close button
    Protected Overrides Sub OnMouseDown(ByVal e As MouseEventArgs)
        If e Is Nothing Then Return
        MyBase.OnMouseDown(e)
        Dim i As Integer = 0
        Do While i < Me.TabPages.Count
            Dim r As Rectangle = Me.GetTabRect(i)
            Dim closeButton As New Rectangle(r.Right + 1 - 15, r.Top + 4, 12, 12)
            If closeButton.Contains(e.Location) Then
                If Me.isTabClosing() Then Me.TabPages.RemoveAt(i)
                Exit Do
            End If
            i += 1
        Loop
    End Sub

    Public Event TabClosing As EventHandler(Of System.ComponentModel.CancelEventArgs)

    Protected Sub OnTabClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        Dim evt As EventHandler(Of System.ComponentModel.CancelEventArgs) = TabClosingEvent
        evt?.Invoke(Me, e)
    End Sub

    Private Function IsTabClosing() As Boolean
        Dim cancelArgs As New System.ComponentModel.CancelEventArgs
        Me.OnTabClosing(cancelArgs)
        Return Not cancelArgs.Cancel
    End Function

End Class

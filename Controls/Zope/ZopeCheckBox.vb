''' <summary> A zope check box. </summary>
''' <license>
''' (c) 2017 Pritam Zope, All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/8/2017" by="David" revision="3.1.6276"> 
''' https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane. </history>
Public Class ZopeCheckBox
    Inherits System.Windows.Forms.CheckBox

    Public Sub New()
        MyBase.New
        Me.ForeColor = Color.White
        Me.AutoSize = False
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
    End Sub

    Private _DisplayText As String = ""
    Public Property DisplayText() As String
        Get
            Return _DisplayText
        End Get
        Set(ByVal value As String)
            _DisplayText = value
            Invalidate()
        End Set
    End Property

    Private _StartColor As Color = Color.SteelBlue
    Public Property StartColor() As Color
        Get
            Return _StartColor
        End Get
        Set(ByVal value As Color)
            _StartColor = value
            Invalidate()
        End Set
    End Property

    Private _EndColor As Color = Color.DarkBlue
    Public Property EndColor() As Color
        Get
            Return _EndColor
        End Get
        Set(ByVal value As Color)
            _EndColor = value
            Invalidate()
        End Set
    End Property

    Private _MouseHoverStartColor As Color = Color.Yellow
    Public Property MouseHoverStartColor() As Color
        Get
            Return _MouseHoverStartColor
        End Get
        Set(ByVal value As Color)
            _MouseHoverStartColor = value
            Invalidate()
        End Set
    End Property

    Private _MouseHoverEndColor As Color = Color.DarkOrange
    Public Property MouseHoverEndColor() As Color
        Get
            Return _MouseHoverEndColor
        End Get
        Set(ByVal value As Color)
            _MouseHoverEndColor = value
            Invalidate()
        End Set
    End Property

    Private _StartOpacity As Integer = 150
    Public Property StartOpacity() As Integer
        Get
            Return _StartOpacity
        End Get
        Set(ByVal value As Integer)
            _StartOpacity = value
            If _StartOpacity > 255 Then
                _StartOpacity = 255
                Invalidate()
            Else
                Invalidate()
            End If
        End Set
    End Property

    Private _EndOpacity As Integer = 150
    Public Property EndOpacity() As Integer
        Get
            Return _EndOpacity
        End Get
        Set(ByVal value As Integer)
            _EndOpacity = value
            If _EndOpacity > 255 Then
                _EndOpacity = 255
                Invalidate()
            Else
                Invalidate()
            End If
        End Set
    End Property

    Private _GradientAngle As Integer = 90
    Public Property GradientAngle() As Integer
        Get
            Return _GradientAngle
        End Get
        Set(ByVal value As Integer)
            _GradientAngle = value
            Invalidate()
        End Set
    End Property

    Private _TextLocation As Point = New Point(14, 4)
    Public Property TextLocation As Point
        Get
            Return Me._TextLocation
        End Get
        Set(value As Point)
            Me._TextLocation = value
            Me.Invalidate()
        End Set
    End Property

    Private _BoxSize As Size = New Size(18, 18)
    Public Property BoxSize() As Size
        Get
            Return _BoxSize
        End Get
        Set(ByVal value As Size)
            _BoxSize = value
            Invalidate()
        End Set
    End Property

    Private _BoxLocation As Point = New Point(0, 0)
    Public Property BoxLocation As Point
        Get
            Return Me._BoxLocation
        End Get
        Set(value As Point)
            Me._BoxLocation = value
            Me.Invalidate()
        End Set
    End Property

    Private _CachedStartColor, _CachedEndColor As Color
    Protected Overrides Sub OnMouseEnter(ByVal e As EventArgs)
        MyBase.OnMouseEnter(e)
        _CachedStartColor = Me.StartColor
        _CachedEndColor = Me.EndColor
        _StartColor = _MouseHoverStartColor
        _EndColor = _MouseHoverEndColor
    End Sub
    Protected Overrides Sub OnMouseLeave(ByVal e As EventArgs)
        MyBase.OnMouseLeave(e)
        _StartColor = _CachedStartColor
        _EndColor = _CachedEndColor
    End Sub
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e Is Nothing Then Return
        MyBase.OnPaint(e)
        Me.AutoSize = False
        _DisplayText = Me._DisplayText
        If Me.TextLocation.X = 100 AndAlso Me.TextLocation.Y = 25 Then
            Me.TextLocation = New Point((Me.Width \ 3) + 10, (Me.Height \ 2) - 1)
        End If
        'drawing string & filling gradient rectangle
        Dim c1 As Color = Color.FromArgb(_StartOpacity, _StartColor)
        Dim c2 As Color = Color.FromArgb(_EndOpacity, _EndColor)
        Using b As Brush = New System.Drawing.Drawing2D.LinearGradientBrush(ClientRectangle, c1, c2, _GradientAngle)
            e.Graphics.FillRectangle(b, ClientRectangle)
        End Using
        Dim p As New Point(Me.TextLocation.X, Me.TextLocation.Y)
        Using frcolor As New SolidBrush(Me.ForeColor)
            e.Graphics.DrawString(_DisplayText, Me.Font, frcolor, p)
        End Using
        Dim rc As New Rectangle(Me.BoxLocation, Me.BoxSize)
        'drawing check box
        ControlPaint.DrawCheckBox(e.Graphics, rc, If(Me.Checked, ButtonState.Checked, ButtonState.Normal))
    End Sub
End Class



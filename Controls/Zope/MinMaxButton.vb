﻿''' <summary> A minimum maximum button. </summary>
''' <license>
''' (c) 2017 Pritam Zope, All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/8/2017" by="David" revision="3.1.6276"> 
''' https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane. </history>
Public Class MinMaxButton
    Inherits System.Windows.Forms.Button

    Public Sub New()
        MyBase.New
        Me.Size = New System.Drawing.Size(31, 24)
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.White
        Me.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Text = "_"
        _DisplayText = Me.Text
    End Sub

    Private _CustomFormState As CustomFormState

    Public Property CustomFormState() As CustomFormState
        Get
            Return _customFormState
        End Get
        Set(ByVal value As CustomFormState)
            _customFormState = value
            Invalidate()
        End Set
    End Property

    Private _DisplayText As String = "_"

    Public Property DisplayText() As String
        Get
            Return _DisplayText
        End Get
        Set(ByVal value As String)
            _DisplayText = value
            Invalidate()
        End Set
    End Property

    Private _BusyBackColor As Color = System.Drawing.Color.Gray
    Public Property BusyBackColor() As Color
        Get
            Return _BusyBackColor
        End Get
        Set(ByVal value As Color)
            _BusyBackColor = value
            Invalidate()
        End Set
    End Property

    Private _MouseHoverColor As Color = System.Drawing.Color.FromArgb(180, 200, 240)

    Public Property MouseHoverColor() As Color
        Get
            Return _MouseHoverColor
        End Get
        Set(ByVal value As Color)
            _MouseHoverColor = value
            Invalidate()
        End Set
    End Property

    Private _MouseClickColor As Color = System.Drawing.Color.FromArgb(160, 180, 200)
    Public Property MouseClickColor() As Color
        Get
            Return _MouseClickColor
        End Get
        Set(ByVal value As Color)
            _MouseClickColor = value
            Invalidate()
        End Set
    End Property

    Private _TextLocation As Point = New Point(6, -20)
    Public Property TextLocation As Point
        Get
            Return Me._TextLocation
        End Get
        Set(value As Point)
            Me._TextLocation = value
            Me.Invalidate()
        End Set
    End Property

    Private _CachedBackColor As Color
    Protected Overrides Sub OnMouseEnter(ByVal e As EventArgs)
        MyBase.OnMouseEnter(e)
        _CachedBackColor = _BusyBackColor
        _BusyBackColor = _MouseHoverColor
    End Sub
    'method mouse leave
    Protected Overrides Sub OnMouseLeave(ByVal e As EventArgs)
        MyBase.OnMouseLeave(e)
        _BusyBackColor = _CachedBackColor
    End Sub

    Protected Overrides Sub OnMouseDown(ByVal mevent As MouseEventArgs)
        MyBase.OnMouseDown(mevent)
        _BusyBackColor = _MouseClickColor
    End Sub

    Protected Overrides Sub OnMouseUp(ByVal mevent As MouseEventArgs)
        MyBase.OnMouseUp(mevent)
        _BusyBackColor = _CachedBackColor
    End Sub


    Protected Overrides Sub OnPaint(ByVal pevent As PaintEventArgs)
        If pevent Is Nothing Then Return
        MyBase.OnPaint(pevent)

        Select Case _customFormState
            Case CustomFormState.Normal
                Using b As New SolidBrush(Me.BusyBackColor)
                    pevent.Graphics.FillRectangle(b, ClientRectangle)
                End Using

                'draw and fill the rectangles of maximized window     
                Using p As New Pen(Me.ForeColor)
                    Using b As New SolidBrush(Me.ForeColor)
                        For i As Integer = 0 To 1
                            pevent.Graphics.DrawRectangle(p, Me.TextLocation.X + i + 1, Me.TextLocation.Y, 10, 10)
                            pevent.Graphics.FillRectangle(b, Me.TextLocation.X + 1, Me.TextLocation.Y - 1, 12, 4)
                        Next i
                    End Using
                End Using

            Case CustomFormState.Maximize
                Using b As New SolidBrush(Me.BusyBackColor)
                    pevent.Graphics.FillRectangle(b, ClientRectangle)
                End Using
                Using p As New Pen(Me.ForeColor)
                    Using b As New SolidBrush(Me.ForeColor)
                        'draw and fill the rectangles of maximized window     
                        For i As Integer = 0 To 1
                            pevent.Graphics.DrawRectangle(p, Me.TextLocation.X + 5, Me.TextLocation.Y, 8, 8)
                            pevent.Graphics.FillRectangle(b, Me.TextLocation.X + 5, Me.TextLocation.Y - 1, 9, 4)
                            pevent.Graphics.DrawRectangle(p, Me.TextLocation.X + 2, Me.TextLocation.Y + 5, 8, 8)
                            pevent.Graphics.FillRectangle(b, Me.TextLocation.X + 2, Me.TextLocation.Y + 4, 9, 4)
                        Next i
                    End Using
                End Using

        End Select

    End Sub


End Class

''' <summary> Values that represent custom form states. </summary>
Public Enum CustomFormState
    Normal
    Maximize
End Enum

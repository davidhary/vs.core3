
''' <summary> Panel Control. </summary>
''' <license>
''' (c) 2017 Pritam Zope, All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/8/2017" by="David" revision="3.1.6276"> 
''' https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane. </history>
Public Class ZopePanel
    Inherits System.Windows.Forms.Panel

    Public Sub New()
        MyBase.New
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
    End Sub

    Private _StartColor As Color = Color.SteelBlue
    Public Property StartColor() As Color
        Get
            Return Me._StartColor
        End Get
        Set(ByVal value As Color)
            Me._StartColor = value
            Me.Invalidate()
        End Set
    End Property

    Private _EndColor As Color = Color.DarkBlue
    Public Property EndColor() As Color
        Get
            Return Me._EndColor
        End Get
        Set(ByVal value As Color)
            Me._EndColor = value
            Me.Invalidate()
        End Set
    End Property

    Private _StartOpacity As Integer = 150
    Public Property StartOpacity() As Integer
        Get
            Return Me._StartOpacity
        End Get
        Set(ByVal value As Integer)
            Me._StartOpacity = value
            If Me._StartOpacity > 255 Then
                Me._StartOpacity = 255
                Me.Invalidate()
            Else
                Me.Invalidate()
            End If
        End Set
    End Property

    Private _EndOpacity As Integer = 150
    Public Property EndOpacity() As Integer
        Get
            Return Me._EndOpacity
        End Get
        Set(ByVal value As Integer)
            Me._EndOpacity = value
            If Me._EndOpacity > 255 Then
                Me._EndOpacity = 255
                Me.Invalidate()
            Else
                Me.Invalidate()
            End If
        End Set
    End Property

    Private _GradientAngle As Integer = 90
    Public Property GradientAngle() As Integer
        Get
            Return Me._GradientAngle
        End Get
        Set(ByVal value As Integer)
            Me._GradientAngle = value
            Me.Invalidate()
        End Set
    End Property

    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e Is Nothing Then Return
        MyBase.OnPaint(e)
        Dim startingColor As Color = Color.FromArgb(_StartOpacity, _StartColor)
        Dim endingColor As Color = Color.FromArgb(_EndOpacity, _EndColor)
        Using b As Brush = New System.Drawing.Drawing2D.LinearGradientBrush(ClientRectangle, startingColor, endingColor, _GradientAngle)
            e.Graphics.FillRectangle(b, ClientRectangle)
        End Using
    End Sub
End Class

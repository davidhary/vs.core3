﻿Imports System.Text

''' <summary> A Zope button. </summary>
''' <license>
''' (c) 2017 Pritam Zope, All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/8/2017" by="David" revision="3.1.6276"> 
''' https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane. </history>
Public Class ZopeButton
    Inherits System.Windows.Forms.Button

    Public Sub New()
        MyBase.New()
        Me.Size = New System.Drawing.Size(31, 24)
        Me.ForeColor = System.Drawing.Color.White
        Me.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 20.25!, FontStyle.Bold, GraphicsUnit.Point, CType(0, Byte))
        Me.Text = "_"
        Me._DisplayText = Me.Text
    End Sub

    Private _DisplayText As String = "_"
    Public Property DisplayText() As String
        Get
            Return _DisplayText
        End Get
        Set(ByVal value As String)
            _DisplayText = value
            Me.Invalidate()
        End Set
    End Property

    Private _BusyBackColor As Color = System.Drawing.Color.Teal
    Public Property BusyBackColor() As Color
        Get
            Return _BusyBackColor
        End Get
        Set(ByVal value As Color)
            _BusyBackColor = value
            Invalidate()
        End Set
    End Property

    Private _MouseHoverColor As Color = System.Drawing.Color.FromArgb(0, 0, 140)
    Public Property MouseHoverColor() As Color
        Get
            Return _MouseHoverColor
        End Get
        Set(ByVal value As Color)
            _MouseHoverColor = value
            Invalidate()
        End Set
    End Property

    Private _MouseClickColor As Color = System.Drawing.Color.FromArgb(160, 180, 200)
    Public Property MouseClickColor() As Color
        Get
            Return _MouseClickColor
        End Get
        Set(ByVal value As Color)
            _MouseClickColor = value
            Invalidate()
        End Set
    End Property

    Private _MouseColorsEnabled As Boolean = True

    Public Property MouseColorsEnabled() As Boolean
        Get
            Return Me._MouseColorsEnabled
        End Get
        Set(ByVal value As Boolean)
            Me._MouseColorsEnabled = value
            Me.Invalidate()
        End Set
    End Property

    Private _TextLocationLeft As Integer = 6

    Public Property TextLocationLeft() As Integer
        Get
            Return _TextLocationLeft
        End Get
        Set(ByVal value As Integer)
            _TextLocationLeft = value
            Invalidate()
        End Set
    End Property

    Private _TextLocationTop As Integer = -20

    Public Property TextLocationTop() As Integer
        Get
            Return _TextLocationTop
        End Get
        Set(ByVal value As Integer)
            _TextLocationTop = value
            Invalidate()
        End Set
    End Property

    Private _CachedColor As Color

    Protected Overrides Sub OnMouseEnter(ByVal e As EventArgs)
        MyBase.OnMouseEnter(e)
        Me._cachedColor = Me.BusyBackColor
        Me._BusyBackColor = Me.MouseHoverColor
    End Sub

    'method mouse leave
    Protected Overrides Sub OnMouseLeave(ByVal e As EventArgs)
        MyBase.OnMouseLeave(e)
        If Me._MouseColorsEnabled Then Me._BusyBackColor = Me._cachedColor
    End Sub

    Protected Overrides Sub OnMouseDown(ByVal mevent As MouseEventArgs)
        MyBase.OnMouseDown(mevent)
        If Me.MouseColorsEnabled Then Me._BusyBackColor = Me.MouseClickColor
    End Sub

    Protected Overrides Sub OnMouseUp(ByVal mevent As MouseEventArgs)
        MyBase.OnMouseUp(mevent)
        If Me.MouseColorsEnabled Then Me._BusyBackColor = Me._cachedColor
    End Sub

    Protected Overrides Sub OnPaint(ByVal pevent As PaintEventArgs)
        If pevent Is Nothing Then Return
        MyBase.OnPaint(pevent)
        Me._DisplayText = Me.Text
        If Me._TextLocationLeft = 100 AndAlso Me._TextLocationTop = 25 Then
            Me._TextLocationLeft = ((Me.Width) \ 3) + 10
            Me._TextLocationTop = (Me.Height \ 2) - 1
        End If

        Dim p As New Point(Me.TextLocationLeft, Me.TextLocationTop)
        Using b As New SolidBrush(Me._BusyBackColor)
            pevent.Graphics.FillRectangle(b, Me.ClientRectangle)
        End Using
        Using b As New SolidBrush(Me.ForeColor)
            pevent.Graphics.DrawString(Me.DisplayText, Me.Font, b, p)
        End Using
    End Sub

End Class

''' <summary> A zope label. </summary>
''' <license>
''' (c) 2017 Pritam Zope, All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/8/2017" by="David" revision="3.1.6276"> 
''' https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane. </history>
Public Class ZopeLabel
    Inherits System.Windows.Forms.Label

    Public Sub New()
        MyBase.New
        Me.ForeColor = Color.Transparent
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
    End Sub

    Private _DisplayText As String = "ZopeLabel"

    Public Property DisplayText() As String
        Get
            Return _DisplayText
        End Get
        Set(ByVal value As String)
            _DisplayText = value
            Invalidate()
        End Set
    End Property

    Private _StartColor As Color = Color.LightGreen
    Public Property StartColor() As Color
        Get
            Return _StartColor
        End Get
        Set(ByVal value As Color)
            _StartColor = value
            Invalidate()
        End Set
    End Property

    Private _EndColor As Color = Color.DarkBlue
    Public Property EndColor() As Color
        Get
            Return _EndColor
        End Get
        Set(ByVal value As Color)
            _EndColor = value
            Invalidate()
        End Set
    End Property

    Private _StartOpacity As Integer = 150
    Public Property StartOpacity() As Integer
        Get
            Return _StartOpacity
        End Get
        Set(ByVal value As Integer)
            _StartOpacity = value
            If _StartOpacity > 255 Then
                _StartOpacity = 255
                Invalidate()
            Else
                Invalidate()
            End If
        End Set
    End Property

    Private _EndOpacity As Integer = 150
    Public Property EndOpacity() As Integer
        Get
            Return _EndOpacity
        End Get
        Set(ByVal value As Integer)
            _EndOpacity = value
            If _EndOpacity > 255 Then
                _EndOpacity = 255
                Invalidate()
            Else
                Invalidate()
            End If
        End Set
    End Property

    Private _GradientAngle As Integer = 90
    Public Property GradientAngle() As Integer
        Get
            Return _GradientAngle
        End Get
        Set(ByVal value As Integer)
            _GradientAngle = value
            Invalidate()
        End Set
    End Property

    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e Is Nothing Then Return
        MyBase.OnPaint(e)
        Me._DisplayText = Me.DisplayText
        Dim c1 As Color = Color.FromArgb(Me.StartOpacity, Me.StartColor)
        Dim c2 As Color = Color.FromArgb(Me.EndOpacity, Me.EndColor)
        Using b As Brush = New System.Drawing.Drawing2D.LinearGradientBrush(New Rectangle(0, 0, 50, 50), c1, c2, Me.GradientAngle)
            e.Graphics.DrawString(Me.DisplayText, Me.Font, b, New Point(0, 0))
        End Using
    End Sub

End Class

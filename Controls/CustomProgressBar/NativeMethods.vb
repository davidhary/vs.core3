﻿
Partial Friend NotInheritable Class NativeMethods

#Region "Parameters"

    '         Sent to a window to request that it draw its client area in the 
    '        specified device context, most commonly in a printer device context. 
    Public Const WM_PRINTCLIENT As Integer = &H318

    ' Paints all descendants of a window in bottom-to-top painting order using double-buffering. 
    Public Const WS_EX_COMPOSITED As Integer = &H2000000

#End Region

#Region "Structures"

    '  Contains basic information about a physical font. This is the Unicode version of the structure. 
    <System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet:=System.Runtime.InteropServices.CharSet.Unicode)>
    Public Structure TEXTMETRIC
        ' The height (ascent + descent) of characters. 
        Public tmHeight As Integer

        ' The ascent (units above the base line) of characters. 
        Public tmAscent As Integer

        ' The descent (units below the base line) of characters. 
        Public tmDescent As Integer

        ' The amount of leading (space) inside the bounds set by the tmHeight member. 
        Public tmInternalLeading As Integer

        ' The amount of extra leading (space) that the application adds between rows. 
        Public tmExternalLeading As Integer

        ' The average width of characters in the font (generally defined as the width of the letter x). 
        Public tmAveCharWidth As Integer

        ' The width of the widest character in the font. 
        Public tmMaxCharWidth As Integer

        ' The weight of the font. 
        Public tmWeight As Integer

        ' The extra width per string that may be added to some synthesized fonts. 
        Public tmOverhang As Integer

        ' The horizontal aspect of the device for which the font was designed. 
        Public tmDigitizedAspectX As Integer

        ' The vertical aspect of the device for which the font was designed. 
        Public tmDigitizedAspectY As Integer

        ' The value of the first character defined in the font. 
        Public tmFirstChar As Char

        ' The value of the last character defined in the font. 
        Public tmLastChar As Char

        ' The value of the character to be substituted for characters not in the font. 
        Public tmDefaultChar As Char

        ' The value of the character that will be used to define word breaks for text justification. 
        Public tmBreakChar As Char

        ' Specifies an italic font if it is nonzero. 
        Public tmItalic As Byte

        ' Specifies an underlined font if it is nonzero. 
        Public tmUnderlined As Byte

        ' A strikeout font if it is nonzero. 
        Public tmStruckOut As Byte

        ' Specifies information about the pitch, the technology, and the family of a physical font. 
        Public tmPitchAndFamily As Byte

        ' The character set of the font. 
        Public tmCharSet As Byte
    End Structure

    ' Contains basic information about a physical font. This is the ANSI version of the structure. 
    <System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet:=System.Runtime.InteropServices.CharSet.Ansi)>
    Public Structure TEXTMETRICA
        ' The height (ascent + descent) of characters. 
        Public tmHeight As Integer

        ' The ascent (units above the base line) of characters. 
        Public tmAscent As Integer

        ' The descent (units below the base line) of characters. 
        Public tmDescent As Integer

        ' The amount of leading (space) inside the bounds set by the tmHeight member. 
        Public tmInternalLeading As Integer

        ' The amount of extra leading (space) that the application adds between rows. 
        Public tmExternalLeading As Integer

        ' The average width of characters in the font (generally defined as the width of the letter x). 
        Public tmAveCharWidth As Integer

        ' The width of the widest character in the font. 
        Public tmMaxCharWidth As Integer

        ' The weight of the font. 
        Public tmWeight As Integer

        ' The extra width per string that may be added to some synthesized fonts. 
        Public tmOverhang As Integer

        ' The horizontal aspect of the device for which the font was designed. 
        Public tmDigitizedAspectX As Integer

        ' The vertical aspect of the device for which the font was designed. 
        Public tmDigitizedAspectY As Integer

        ' The value of the first character defined in the font. 
        Public tmFirstChar As Byte

        ' The value of the last character defined in the font. 
        Public tmLastChar As Byte

        ' The value of the character to be substituted for characters not in the font. 
        Public tmDefaultChar As Byte

        ' The value of the character that will be used to define word breaks for text justification. 
        Public tmBreakChar As Byte

        ' Specifies an italic font if it is nonzero. 
        Public tmItalic As Byte

        ' Specifies an underlined font if it is nonzero. 
        Public tmUnderlined As Byte

        ' A strikeout font if it is nonzero. 
        Public tmStruckOut As Byte

        ' Specifies information about the pitch, the technology, and the family of a physical font. 
        Public tmPitchAndFamily As Byte

        ' The character set of the font. 
        Public tmCharSet As Byte
    End Structure

#End Region

#Region " paint Structures"
#If paint Then

    ' Sent when the system makes a request to paint (a portion of) a window. 
    Public Const WM_PAINT As Integer = &HF

    ' Contains information to be used to paint the client area of a window. 
    <System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet:=System.Runtime.InteropServices.CharSet.Auto)>
    Public Structure PAINTSTRUCT
        ' A handle to the display DC to use for painting. 
        Public hdc As IntPtr

        ' Indicates whether the background should be erased. 
        Public fErase As Boolean

        '             A RECT structure that specifies the upper left and lower right 
        '            corners of the rectangle in which the painting is requested, 
        Public rcPaint As RECT

        ' Reserved; used internally by the system. 
        Public fRestore As Boolean

        ' Reserved; used internally by the system. 
        Public fIncUpdate As Boolean
    End Structure

    ' Defines the coordinates of the upper-left and lower-right corners of a rectangle. 
    <System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet:=System.Runtime.InteropServices.CharSet.Auto)>
    Public Structure RECT
        ' The x-coordinate of the upper-left corner of the rectangle. 
        Public Left As Integer

        ' The y-coordinate of the upper-left corner of the rectangle. 
        Public Top As Integer

        ' The x-coordinate of the lower-right corner of the rectangle. 
        Public Right As Integer

        ' The y-coordinate of the lower-right corner of the rectangle. 
        Public Bottom As Integer
    End Structure

#End If

#End Region

End Class

﻿Imports System.ComponentModel
Imports System.Globalization
Imports System.Runtime.InteropServices
''' <summary>
''' Represents a Windows progress bar control which displays 
''' its <see cref="ProgressBar.Value" /> as text on a faded background.
''' </summary>
''' <remarks>
''' CustomProgressBar is a specialized type of <see cref="ProgressBar" />, which it extends 
''' to fade its background colors and to display its <see cref="CustomProgressBar.Text" />. 
''' 
''' <para>You can manipulate the background fading intensity by changing the value of 
''' property <see cref="CustomProgressBar.Fade" /> which accepts values between 0 and 255. 
''' Lower values make the background darker; higher values make the background lighter.</para>
''' 
''' <para>The current <see cref="ProgressBar.Text" /> is displayed using the values of properties 
''' <see cref="CustomProgressBar.Font" /> and <see cref="CustomProgressBar.ForeColor" />.</para>
''' 
''' <para><note type="inherit">When you derive from CustomProgressBar, adding new functionality to the 
''' derived class, if your derived class references objects that must be disposed of before an instance of 
''' your class is destroyed, you must override the <see cref="CustomProgressBar.Dispose(System.Boolean)" /> 
''' method, and call <see cref="System.ComponentModel.Component.Dispose()">Dispose()</see> on all objects 
''' that are referenced in your class, before calling <c>Dispose(disposing)</c> on the base class.</note></para>
''' </remarks>
''' <license>
''' (c) 2016 Hiske Bekkering. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/1/2016" by="Hiske Bekkering" revision=""> Created. </history>
<Description("Provides a ProgressBar which displays its Value as text on a faded background."),
    Designer(GetType(CustomProgressBarDesigner)), ToolboxBitmap(GetType(ProgressBar))>
Public Class CustomProgressBar
    Inherits ProgressBar

#Region " Construction & Destruction "

    ''' <summary>
    ''' Initializes a new instance of the 
    ''' <see cref="CustomProgressBar" /> class.
    ''' </summary>
    Public Sub New()
        MyBase.New()
        MyBase.ForeColor = SystemColors.ControlText
        Me._FadeBrush = New SolidBrush(Color.FromArgb(Me.Fade, Color.White))
    End Sub

    ''' <summary> Creates a new CustomProgressBar. </summary>
    ''' <returns> A CustomProgressBar. </returns>
    Public Shared Function Create() As CustomProgressBar
        Dim result As CustomProgressBar = Nothing
        Try
            result = New CustomProgressBar
        Catch
            result?.Dispose()
            Throw
        End Try
        Return result
    End Function

    ''' <summary> 
    ''' Releases the unmanaged resources used by the <see cref="CustomProgressBar" /> 
    ''' and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"><b>True</b> to release both managed and unmanaged 
    ''' resources; <b>false</b> to release only unmanaged resources.</param> 
    ''' <remarks>
    ''' This method is called by the public <see cref="Control.Dispose" /> method and 
    ''' the <see cref="Object.Finalize" /> method. Dispose invokes Dispose with the 
    ''' <i>disposing</i> parameter set to <b>true</b>. Finalize invokes Dispose with 
    ''' <i>disposing</i> set to <b>false</b>.
    ''' 
    ''' <para><note type="inherit">Dispose might be called multiple times by other objects. 
    ''' When overriding <i>Dispose(Boolean)</i>, be careful not to reference objects that 
    ''' have been previously disposed of in an earlier call to Dispose.
    ''' 
    ''' <para>If your derived class references objects that must be disposed of before an 
    ''' instance of your class is destroyed, you must call <see cref="Control.Dispose" /> on 
    ''' all objects that are referenced in your class, before calling <c>Dispose(disposing)</c> 
    ''' on the base class.</para></note></para>
    ''' </remarks>
    ''' <overloads>Releases all resources used by the <see cref="CustomProgressBar" />.
    ''' <para>This member is overloaded. For complete information about this member, 
    ''' click a name in the overload list.</para></overloads>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If _FadeBrush IsNot Nothing Then
                _FadeBrush.Dispose()
                _FadeBrush = Nothing
            End If
        End If

        MyBase.Dispose(disposing)
    End Sub

#End Region

#Region " Public & Protected Properties "

    ''' <summary>
    ''' Returns the parameters used to create the window 
    ''' for the <see cref="CustomProgressBar" /> control.
    ''' </summary>
    ''' <value>A <see cref="System.Windows.Forms.CreateParams" /> object that contains the 
    ''' required creation parameters for the <see cref="CustomProgressBar" /> control.</value>
    ''' <remarks>
    ''' The information returned by the CreateParams property is used to 
    ''' pass information about the initial state and appearance of this 
    ''' control, at the time an instance of this class is being created.
    ''' 
    ''' <para><note type="inherit">When overriding the CreateParams property in a derived 
    ''' class, use the base class's CreateParams property to extend the base implementation. 
    ''' Otherwise, you must provide all the implementation.</note></para>
    ''' </remarks>
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim myParams As CreateParams = MyBase.CreateParams

            ' Make the control use double buffering
            myParams.ExStyle = myParams.ExStyle Or NativeMethods.WS_EX_COMPOSITED

            Return myParams
        End Get
    End Property

    Private _Fade As Integer = 150
    Private _FadeBrush As SolidBrush
    ''' <summary>
    ''' Gets or sets the opacity of the white overlay brush which fades 
    ''' the background colors of the <see cref="CustomProgressBar" />.
    ''' </summary>
    ''' <value>An <see cref="System.Int32" /> representing the alpha 
    ''' value of the overlay color. The default is <b>150</b>.</value>
    ''' <remarks>
    ''' You can use this property to manipulate the density of the background coloring of this control, 
    ''' to allow for better readability of any text within the <see cref="CustomProgressBar" />. You can use 
    ''' the <see cref="CustomProgressBar.Font" /> and <see cref="CustomProgressBar.ForeColor" /> properties 
    ''' to further optimize the display of text.
    ''' 
    ''' <para>Acceptable values for this property are between 0 and 255 inclusive. The default is 150; 
    ''' lower values make the background darker; higher values make the background lighter.</para>
    ''' </remarks>
    ''' <exception cref="System.ArgumentOutOfRangeException">The value assigned to the property is less than 0 or greater than 255.</exception>
    <Category("Appearance"), DefaultValue(150),
        Description("Specifies the opacity of the white overlay brush which fades the background colors of the CustomProgressBar.")>
    Public Property Fade() As Integer
        Get
            Return Me._Fade
        End Get
        Set(ByVal value As Integer)
            If value < 0 OrElse value > 255 Then
                Dim str() As Object = {value}
                Throw New ArgumentOutOfRangeException("value", String.Format(System.Globalization.CultureInfo.CurrentCulture,
                                                                             "A value of '{0}' is not valid for 'Fade'. 'Fade' must be between 0 and 255.", str))
            End If

            Me._Fade = value

            ' Clean up previous brush
            If Me._FadeBrush IsNot Nothing Then
                Me._FadeBrush.Dispose()
            End If

            Me._FadeBrush = New SolidBrush(Color.FromArgb(value, Color.White))

            Me.Invalidate()
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the <see cref="System.Drawing.Font" /> of 
    ''' the text displayed by the <see cref="CustomProgressBar" />.
    ''' </summary>
    ''' <value>The <see cref="System.Drawing.Font" /> to 
    ''' apply to the text displayed by the control.</value>
    ''' <remarks>
    ''' You can use the Font property to change the <see cref="System.Drawing.Font" /> 
    ''' to use when drawing text. To change the text <see cref="Color" />, 
    ''' use the <see cref="CustomProgressBar.ForeColor" /> property.
    ''' 
    ''' <para>The Font property is an ambient property. An ambient property is 
    ''' a control property that, if not set, is retrieved from the parent control.</para>
    ''' 
    ''' <para>Because the <see cref="System.Drawing.Font" /> class is immutable (meaning 
    ''' that you cannot adjust any of its properties), you can only assign the Font property 
    ''' a new Font. However, you can base the new font on the existing font.</para>
    ''' 
    ''' <para><note type="inherit">When overriding the Font property in a derived class, use the 
    ''' base class's Font property to extend the base implementation. Otherwise, you must provide 
    ''' all the implementation.</note></para>
    ''' </remarks>
    <Browsable(True), EditorBrowsable(EditorBrowsableState.Always)>
    Public Overrides Property Font() As Font
        Get
            Return MyBase.Font
        End Get
        Set(ByVal value As Font)
            MyBase.Font = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the color of the text displayed by <see cref="CustomProgressBar" />.
    ''' </summary>
    ''' <value>A <see cref="Color" /> that represents the 
    ''' control's foreground color. The default is <b>ControlText</b>.</value>
    ''' <remarks>
    ''' You can use the ForeColor property to change the color of the text within the 
    ''' <see cref="CustomProgressBar" /> to match the text of other controls on your form.
    ''' To change the <see cref="System.Drawing.Font" /> to use when drawing text, use the 
    ''' <see cref="CustomProgressBar.Font">CustomProgressBar.Font</see> property.
    ''' 
    ''' <para><note type="inherit">When overriding the ForeColor property in a derived class, 
    ''' use the base class's ForeColor property to extend the base implementation. Otherwise, 
    ''' you must provide all the implementation.</note></para>
    ''' </remarks>
    <DefaultValue(GetType(System.Drawing.Color), "ControlText")>
    Public Overrides Property ForeColor() As Color
        Get
            Return MyBase.ForeColor
        End Get

        Set(ByVal value As Color)
            MyBase.ForeColor = value
        End Set
    End Property

    Private _CaptionFormat As String
    ''' <summary> Specifies the format of the overlay. </summary>
    ''' <value> The caption format. </value>
    <Category("Appearance"), DefaultValue("{0} %"),
        Description("Specifies the format of the overlay.")>
    Public Property CaptionFormat As String
        Get
            If String.IsNullOrEmpty(Me._CaptionFormat) Then
                Return "{0} %"
            Else
                Return Me._CaptionFormat
            End If
        End Get
        Set(value As String)
            Me._CaptionFormat = value
        End Set
    End Property

    ''' <summary>
    ''' Gets the text associated with this <see cref="CustomProgressBar" />.
    ''' </summary>
    ''' <value>A <see cref="String" /> representing the text displayed in the control.</value>
    ''' <remarks>
    ''' The <see cref="CustomProgressBar" /> control supports display of a single line of 
    ''' text, consisting of the <see cref="ProgressBar.Value" /> followed by a percent sign.
    ''' 
    ''' <para>The text is displayed using the values of properties 
    ''' <see cref="CustomProgressBar.Font" /> and <see cref="CustomProgressBar.ForeColor" />.</para>
    ''' </remarks>
    <Browsable(False), EditorBrowsable(EditorBrowsableState.Always), Bindable(False)>
    Public Overrides Property Text() As String
        Get
            Dim format As String = Me.CaptionFormat
            If String.IsNullOrEmpty(format) Then format = "{0} %"
            Return String.Format(CultureInfo.CurrentCulture, format, Value)
        End Get
        Set(value As String)
            MyBase.Text = value
        End Set
    End Property

#End Region

#Region " Public & Protected Methods "

    ''' <summary>
    ''' Processes Windows messages.
    ''' </summary>
    ''' <param name="m">The Windows Message to process.</param>
    ''' <remarks>
    ''' All messages are sent to the WndProc method after getting filtered 
    ''' through the PreProcessMessage method. The WndProc method corresponds 
    ''' exactly to the Windows WindowProc function. 
    ''' 
    ''' <para><note type="inherit">Inheriting controls should call the base class's 
    ''' WndProc method to process any messages that they do not handle.</note></para>
    ''' </remarks>
    Protected Overrides Sub WndProc(ByRef m As Message)
        Dim message As Integer = m.Msg

        If message = NativeMethods.WM_PAINT Then
            WmPaint(m)
            Return
        End If

        If message = NativeMethods.WM_PRINTCLIENT Then
            WmPrintClient(m)
            Return
        End If

        MyBase.WndProc(m)
    End Sub

    ''' <summary>
    ''' Returns a string representation for this <see cref="CustomProgressBar" />.
    ''' </summary>
    ''' <returns>A <see cref="String" /> that describes this control.</returns>
    Public Overrides Function ToString() As String
        Dim builder As New System.Text.StringBuilder()
        builder.Append(Me.GetType().FullName)
        builder.Append(", Minimum: ")
        builder.Append(Me.Minimum.ToString(CultureInfo.CurrentCulture))
        builder.Append(", Maximum: ")
        builder.Append(Me.Maximum.ToString(CultureInfo.CurrentCulture))
        builder.Append(", Value: ")
        builder.Append(Me.Value.ToString(CultureInfo.CurrentCulture))
        Return builder.ToString()
    End Function

#End Region

#Region " Private Members "

    Private Sub PaintPrivate(ByVal device As IntPtr)
        ' Create a Graphics object for the device context
        Using graphics As Graphics = System.Drawing.Graphics.FromHdc(device)
            Dim rect As Rectangle = ClientRectangle

            If _FadeBrush IsNot Nothing Then
                ' Paint a translucent white layer on top, to fade the colors a bit
                graphics.FillRectangle(_FadeBrush, rect)
            End If

            TextRenderer.DrawText(graphics, Text, Font, rect, ForeColor)
        End Using
    End Sub

    Private Sub WmPaint(ByRef m As Message)
        ' Create a wrapper for the Handle
        Dim myHandle As New HandleRef(Me, Handle)

        ' Prepare the window for painting and retrieve a device context
        Dim pAINTSTRUCT As New NativeMethods.PAINTSTRUCT()
        Dim hDC As IntPtr = UnsafeNativeMethods.BeginPaint(myHandle, pAINTSTRUCT)

        Try
            ' Apply hDC to message
            m.WParam = hDC

            ' Let Windows paint
            MyBase.WndProc(m)

            ' Custom painting
            PaintPrivate(hDC)
        Finally
            ' Release the device context that BeginPaint retrieved
            UnsafeNativeMethods.EndPaint(myHandle, pAINTSTRUCT)
        End Try
    End Sub

    Private Sub WmPrintClient(ByRef m As Message)
        ' Retrieve the device context
        Dim hDC As IntPtr = m.WParam

        ' Let Windows paint
        MyBase.WndProc(m)

        ' Custom painting
        PaintPrivate(hDC)
    End Sub

#End Region

End Class

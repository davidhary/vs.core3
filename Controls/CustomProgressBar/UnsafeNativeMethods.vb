﻿Imports System.Runtime.InteropServices
Imports System.Security
''' <summary> An unsafe native methods. </summary>
''' <remarks> Potentially dangerous P/Invoke method declarations; be careful not to expose these publicly. </remarks>
''' <license>
''' (c) 2016 Hiske Bekkering. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/1/2016" by="Hiske Bekkering" revision=""> Created. </history>
<SuppressUnmanagedCodeSecurity>
Friend NotInheritable Class UnsafeNativeMethods

    Private Sub New()
    End Sub

    ''' <summary> Begins a paint. </summary>
    ''' <remarks>
    ''' Prepares the specified window for painting and fills a PAINTSTRUCT structure with information
    ''' about the painting.
    ''' </remarks>
    ''' <param name="hWnd">  The window. </param>
    ''' <param name="lpPaint"> The paint structure. </param>
    ''' <returns> An IntPtr. </returns>
    <DllImport("user32.dll", CharSet:=CharSet.Auto, ExactSpelling:=False)>
    Friend Shared Function BeginPaint(ByVal hWnd As HandleRef, <[In](), Out()> ByRef lpPaint As NativeMethods.PAINTSTRUCT) As IntPtr
    End Function

    ''' <summary> Ends a paint. </summary>
    ''' <remarks>
    ''' Marks the end of painting in the specified window. This function is required for each call to
    ''' the BeginPaint function, but only after painting is complete.
    ''' </remarks>
    ''' <param name="hWnd">    The window. </param>
    ''' <param name="lpPaint"> The paint structure. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <DllImport("user32.dll", CharSet:=CharSet.Auto, ExactSpelling:=False)>
    Friend Shared Function EndPaint(ByVal hWnd As HandleRef, ByRef lpPaint As NativeMethods.PAINTSTRUCT) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

End Class


﻿Imports System.ComponentModel
''' <summary> A corner radius. </summary>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
<CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2218:OverrideGetHashCodeOnOverridingEquals")>
<CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1815:OverrideEqualsAndOperatorEqualsOnValueTypes")>
Public Structure CornerRadius

    Public Sub New(ByVal radius As Integer)
        Me._BottomLeft = radius
        Me._BottomRight = radius
        Me._TopLeft = radius
        Me._TopRight = radius
    End Sub

    Public Sub New(ByVal bottomLeft As Integer, ByVal bottomRight As Integer, ByVal topLeft As Integer, ByVal topRight As Integer)
        Me._BottomLeft = bottomLeft
        Me._BottomRight = bottomRight
        Me._TopLeft = topLeft
        Me._TopRight = topRight
    End Sub

    Public Sub New(ByVal value As CornerRadius)
        Me._BottomLeft = value.BottomLeft
        Me._BottomRight = value.BottomRight
        Me._TopLeft = value.TopLeft
        Me._TopRight = value.TopRight
    End Sub

    Public Shared Function Parse(ByVal value As String) As CornerRadius
        If String.IsNullOrWhiteSpace(value) Then
            Return CornerRadius.Empty
        Else
            Dim values As String() = value.Split(","c)
            If value.Count < 4 Then
                Return CornerRadius.Empty
            Else
                Return New CornerRadius(Convert.ToInt32(values(0), Globalization.CultureInfo.InvariantCulture),
                                        Convert.ToInt32(values(1), Globalization.CultureInfo.InvariantCulture),
                                        Convert.ToInt32(values(2), Globalization.CultureInfo.InvariantCulture),
                                        Convert.ToInt32(values(3), Globalization.CultureInfo.InvariantCulture))
            End If
        End If
    End Function

    Public Shared ReadOnly Property Unity As CornerRadius 
	Get
          Return New CornerRadius(1, 1, 1, 1)
	End Get
    End Property 



    Public Shared ReadOnly Property Empty As CornerRadius
	Get
          Return New CornerRadius(0, 0, 0, 0)
	End Get
    End Property 


    ''' <summary> Gets the is empty. </summary>
    ''' <value> The is empty. </value>
    Public ReadOnly Property IsEmpty As Boolean


        Get
            Return Me.BottomLeft <= 0 OrElse Me.BottomRight <= 0 OrElse Me.TopLeft <= 0 OrElse Me.TopRight <= 0
        End Get
    End Property

    <DescriptionAttribute("Set the Radius of the Lower Left Corner")>
    <RefreshProperties(RefreshProperties.Repaint)>
    <NotifyParentProperty(True)>
    <DefaultValue(0)>
    Public Property BottomLeft As Integer

    <DescriptionAttribute("Set the Radius of the Lower Right Corner")>
    <RefreshProperties(RefreshProperties.Repaint)>
    <NotifyParentProperty(True)>
    <DefaultValue(0)>
    Public Property BottomRight As Integer

    <DescriptionAttribute("Set the Radius of the Upper Left Corner")>
    <RefreshProperties(RefreshProperties.Repaint)>
    <NotifyParentProperty(True)>
    <DefaultValue(0)>
    Public Property TopLeft As Integer

    <DescriptionAttribute("Set the Radius of the Upper Right Corner")>
    <RefreshProperties(RefreshProperties.Repaint)>
    <NotifyParentProperty(True)>
    <DefaultValue(0)>
    Public Property TopRight As Integer

    Public Overrides Function ToString() As String
        Return String.Format("{0}, {1}, {2}, {3}", Me.BottomLeft, Me.BottomRight, Me.TopLeft, Me.TopRight)
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <param name="obj"> The object to compare with the current instance. </param>
    ''' <returns>
    ''' true if <paramref name="obj" /> and this instance are the same type and represent the same
    ''' value; otherwise, false.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2231:OverloadOperatorEqualsOnOverridingValueTypeEquals")>
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
    	Return obj IsNot Nothing AndAlso Me.GetType() Is obj.GetType() AndAlso Me.Equals(CType(obj, CornerRadius))
	End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <param name="other"> The corner radius to compare to this object. </param>
    ''' <returns>
    ''' true if <paramref name="other" /> and this instance are the same type and represent the same
    ''' value; otherwise, false.
    ''' </returns>
    Public Overloads Function Equals(ByVal other As CornerRadius) As Boolean
        Return Me.BottomLeft = other.BottomLeft AndAlso Me.BottomRight = other.BottomRight AndAlso
               Me.TopLeft = other.TopLeft AndAlso Me.TopRight = other.TopRight
    End Function

End Structure

''' <summary> A corner radius converter. </summary>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="4/4/2016" by="David" revision=""> Created. </history>
<CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")>
Friend Class CornerRadiusConverter
    Inherits ExpandableObjectConverter

    ''' <summary>
    ''' Returns whether changing a value on this object requires a call to
    ''' <see cref="M:System.ComponentModel.TypeConverter.CreateInstance(System.Collections.IDictionary)" />
    ''' to create a new value, using the specified context.
    ''' </summary>
    ''' <param name="context"> An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
    '''                        provides a format context. </param>
    ''' <returns>
    ''' true if changing a property on this object requires a call to
    ''' <see cref="M:System.ComponentModel.TypeConverter.CreateInstance(System.Collections.IDictionary)" />
    ''' to create a new value; otherwise, false.
    ''' </returns>
    Public Overrides Function GetCreateInstanceSupported(ByVal context As ITypeDescriptorContext) As Boolean
        Return True
    End Function

    ''' <summary>
    ''' Creates an instance of the type that this
    ''' <see cref="T:System.ComponentModel.TypeConverter" /> is associated with, using the specified
    ''' context, given a set of property values for the object.
    ''' </summary>
    ''' <param name="context">        An <see cref="T:System.ComponentModel.ITypeDescriptorContext" />
    '''                               that provides a format context. </param>
    ''' <param name="propertyValues"> An <see cref="T:System.Collections.IDictionary" /> of new
    '''                               property values. </param>
    ''' <returns>
    ''' An <see cref="T:System.Object" /> representing the given
    ''' <see cref="T:System.Collections.IDictionary" />, or null if the object cannot be created.
    ''' This method always returns null.
    ''' </returns>
    Public Overrides Function CreateInstance(ByVal context As ITypeDescriptorContext,
                                             ByVal propertyValues As System.Collections.IDictionary) As Object
        If propertyValues Is Nothing Then Throw New ArgumentNullException(NameOf(propertyValues))
        Dim lL As Int32 = CType(propertyValues("BottomLeft"), Int32)
        Dim lR As Int32 = CType(propertyValues("BottomRight"), Int32)
        Dim uL As Int32 = CType(propertyValues("TopLeft"), Int32)
        Dim uR As Int32 = CType(propertyValues("TopRight"), Int32)
        Dim crn As New CornerRadius(LL, LR, UL, UR)
        Return crn
    End Function

    ''' <summary>
    ''' Returns whether this converter can convert an object of the given type to the type of this
    ''' converter, using the specified context.
    ''' </summary>
    ''' <param name="context">    An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
    '''                           provides a format context. </param>
    ''' <param name="sourceType"> A <see cref="T:System.Type" /> that represents the type you want to
    '''                           convert from. </param>
    ''' <returns> true if this converter can perform the conversion; otherwise, false. </returns>
    Public Overloads Overrides Function CanConvertFrom(ByVal context As ITypeDescriptorContext,
      ByVal sourceType As System.Type) As Boolean
        If (sourceType Is GetType(String)) Then
            Return True
        End If
        Return MyBase.CanConvertFrom(context, sourceType)
    End Function

    ''' <summary>
    ''' Converts the given object to the type of this converter, using the specified context and
    ''' culture information.
    ''' </summary>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="context"> An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
    '''                        provides a format context. </param>
    ''' <param name="culture"> The <see cref="T:System.Globalization.CultureInfo" /> to use as the
    '''                        current culture. </param>
    ''' <param name="value">   The <see cref="T:System.Object" /> to convert. </param>
    ''' <returns> An <see cref="T:System.Object" /> that represents the converted value. </returns>
    Public Overloads Overrides Function ConvertFrom(ByVal context As ITypeDescriptorContext,
                                                    ByVal culture As System.Globalization.CultureInfo, ByVal value As Object) As Object
        If TypeOf value Is String Then
            Try
                Dim s As String = CType(value, String)
                Dim cornerParts(4) As String
                cornerParts = Split(s, ",")
                If Not IsNothing(cornerParts) Then
                    If IsNothing(cornerParts(0)) Then cornerParts(0) = "0"
                    If IsNothing(cornerParts(1)) Then cornerParts(1) = "0"
                    If IsNothing(cornerParts(2)) Then cornerParts(2) = "0"
                    If IsNothing(cornerParts(3)) Then cornerParts(3) = "0"
                    Return New CornerRadius(CInt(cornerParts(0).Trim),
                                               CInt(cornerParts(1).Trim),
                                               CInt(cornerParts(2).Trim),
                                               CInt(cornerParts(3).Trim))
                End If
            Catch ex As Exception
                Throw New ArgumentException(FormattableString.Invariant($"Can not convert '{value}' to type CornerRadius"))
            End Try
        Else
            Return New CornerRadius()
        End If
        Return MyBase.ConvertFrom(context, culture, value)
    End Function

    Public Overloads Overrides Function ConvertTo(ByVal context As ITypeDescriptorContext,
                                                  ByVal culture As System.Globalization.CultureInfo,
                                                  ByVal value As Object, ByVal destinationType As System.Type) As Object
        Dim _Corners As CornerRadius = CType(value, CornerRadius)
        If (destinationType Is GetType(System.String) AndAlso TypeOf value Is CornerRadius) Then
            Return _Corners.ToString
        Else
            Return MyBase.ConvertTo(context, culture, value, destinationType)
        End If
    End Function

End Class






Imports System.ComponentModel
Imports System.Drawing.Imaging
''' <summary> An image toggle button. </summary>
''' <license>
''' (c) 2008 Vartan Simonian. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/31/2016" by="David" revision=""> Created. </history>
Public Class ImageToggle
    Inherits PictureBox
    Implements IButtonControl

#Region " IButtonControl Members "

    ''' <summary>
    ''' Gets or sets the value returned to the parent form when the button is clicked.
    ''' </summary>
    ''' <value> One of the <see cref="T:System.Windows.Forms.DialogResult" /> values. </value>
    Public Property DialogResult() As DialogResult Implements IButtonControl.DialogResult

    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")>
    Private _IsDefault As Boolean = False
    ''' <summary>
    ''' Notifies a control that it is the default button so that its appearance and behavior is
    ''' adjusted accordingly.
    ''' </summary>
    ''' <param name="value"> true if the control should behave as a default button; otherwise false. </param>
    Public Sub NotifyDefault(ByVal value As Boolean) Implements IButtonControl.NotifyDefault
        Me._IsDefault = value
    End Sub

    ''' <summary>
    ''' Generates a <see cref="E:System.Windows.Forms.Control.Click" /> event for the control.
    ''' </summary>
    Public Sub PerformClick() Implements IButtonControl.PerformClick
        Me.OnClick(EventArgs.Empty)
    End Sub

#End Region

#Region " IMAGES "

    Private hover As Boolean = False
    Private down As Boolean = False

    Private _CheckedHoverImage As Image

    ''' <summary> Image to show when the Checked button is hovered over. </summary>
    ''' <value> The hover image. </value>
    <Category("Appearance"), Description("Image to show when the button is hovered over.")>
    Public Property CheckedHoverImage() As Image
        Get
            Return _CheckedHoverImage
        End Get
        Set(ByVal value As Image)
            _CheckedHoverImage = value
            If Me.hover Then
                Me.Image = value
            End If
        End Set
    End Property

    Private _CheckedImage As Image

    ''' <summary> Image to show when the button is depressed. </summary>
    ''' <value> The down image. </value>
    <Category("Appearance"), Description("Image to show when the button is depressed.")>
    Public Property CheckedImage() As Image
        Get
            Return _CheckedImage
        End Get
        Set(ByVal value As Image)
            _CheckedImage = value
            If Me.down Then
                Me.Image = value
            End If
        End Set
    End Property


    Private _UncheckedHoverImage As Image

    ''' <summary> Image to show when the unchecked button is hovered over. </summary>
    ''' <value> The hover image. </value>
    <Category("Appearance"), Description("Image to show when the button is hovered over.")>
    Public Property UncheckedHoverImage() As Image
        Get
            Return _UncheckedHoverImage
        End Get
        Set(ByVal value As Image)
            _UncheckedHoverImage = value
            If Me.hover Then
                Me.Image = value
            End If
        End Set
    End Property

    Private _UncheckedImage As Image

    ''' <summary> Image to show when the button is not in any other state. </summary>
    ''' <value> The normal image. </value>
    <Category("Appearance"), Description("Image to show when the button is not in any other state.")>
    Public Property UncheckedImage() As Image
        Get
            Return Me._UncheckedImage
        End Get
        Set(ByVal value As Image)
            Me._UncheckedImage = value
            If Not (Me.hover OrElse Me.down) Then
                Me.Image = value
            End If
        End Set
    End Property

#End Region

#Region " Overrides "

    <Browsable(True), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        Category("Appearance"), Description("The text associated with the control.")>
    Public Overrides Property Text() As String
        Get
            Return MyBase.Text
        End Get
        Set(ByVal value As String)
            MyBase.Text = value
        End Set
    End Property

    <Browsable(True), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        Category("Appearance"), Description("The font used to display text in the control.")>
    Public Overrides Property Font() As Font
        Get
            Return MyBase.Font
        End Get
        Set(ByVal value As Font)
            MyBase.Font = value
        End Set
    End Property

#End Region

#Region " Description Changes "

    ''' <summary>
    ''' Controls how the ImageButton will handle image placement and control sizing.
    ''' </summary>
    ''' <value> The size mode. </value>
    <Description("Controls how the ImageButton will handle image placement and control sizing.")>
    Public Shadows Property SizeMode() As PictureBoxSizeMode
        Get
            Return MyBase.SizeMode
        End Get
        Set(ByVal value As PictureBoxSizeMode)
            MyBase.SizeMode = value
        End Set
    End Property

    ''' <summary> Controls what type of border the ImageButton should have. </summary>
    ''' <value> The border style. </value>
    <Description("Controls what type of border the ImageButton should have.")>
    Public Shadows Property BorderStyle() As BorderStyle
        Get
            Return MyBase.BorderStyle
        End Get
        Set(ByVal value As BorderStyle)
            MyBase.BorderStyle = value
        End Set
    End Property

#End Region

#Region "Hiding"

    ''' <summary> Gets or sets the image. </summary>
    ''' <value> The image. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Shadows Property Image() As Image
        Get
            Return MyBase.Image
        End Get
        Set(ByVal value As Image)
            MyBase.Image = value
        End Set
    End Property

    ''' <summary> Gets or sets the background image layout. </summary>
    ''' <value> The background image layout. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Shadows Property BackgroundImageLayout() As ImageLayout
        Get
            Return MyBase.BackgroundImageLayout
        End Get
        Set(ByVal value As ImageLayout)
            MyBase.BackgroundImageLayout = value
        End Set
    End Property

    ''' <summary> Gets or sets the background image. </summary>
    ''' <value> The background image. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Shadows Property BackgroundImage() As Image
        Get
            Return MyBase.BackgroundImage
        End Get
        Set(ByVal value As Image)
            MyBase.BackgroundImage = value
        End Set
    End Property

    ''' <summary> Gets or sets the image location. </summary>
    ''' <value> The image location. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Shadows Property ImageLocation() As String
        Get
            Return MyBase.ImageLocation
        End Get
        Set(ByVal value As String)
            MyBase.ImageLocation = value
        End Set
    End Property

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Shadows Property ErrorImage() As Image
        Get
            Return MyBase.ErrorImage
        End Get
        Set(ByVal value As Image)
            MyBase.ErrorImage = value
        End Set
    End Property

    ''' <summary> Gets or sets the initial image. </summary>
    ''' <value> The initial image. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Shadows Property InitialImage() As Image
        Get
            Return MyBase.InitialImage
        End Get
        Set(ByVal value As Image)
            MyBase.InitialImage = value
        End Set
    End Property

    ''' <summary> Gets or sets the wait on load. </summary>
    ''' <value> The wait on load. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Shadows Property WaitOnLoad() As Boolean
        Get
            Return MyBase.WaitOnLoad
        End Get
        Set(ByVal value As Boolean)
            MyBase.WaitOnLoad = value
        End Set
    End Property

#End Region

#Region " MESSAGE PROCESS "

    Private Const WM_KEYDOWN As Integer = &H100
    Private Const WM_KEYUP As Integer = &H101

    Private holdingSpace As Boolean = False

    Public Overrides Function PreProcessMessage(ByRef msg As Message) As Boolean
        If msg.Msg = WM_KEYUP Then
            If holdingSpace Then
                If CInt(Fix(msg.WParam)) = CInt(Keys.Space) Then
                    OnMouseUp(Nothing)
                    PerformClick()
                ElseIf CInt(Fix(msg.WParam)) = CInt(Keys.Escape) OrElse CInt(Fix(msg.WParam)) = CInt(Keys.Tab) Then
                    holdingSpace = False
                    OnMouseUp(Nothing)
                End If
            End If
            Return True
        ElseIf msg.Msg = WM_KEYDOWN Then
            If CInt(Fix(msg.WParam)) = CInt(Keys.Space) Then
                holdingSpace = True
                OnMouseDown(Nothing)
            ElseIf CInt(Fix(msg.WParam)) = CInt(Keys.Enter) Then
                PerformClick()
            End If
            Return True
        Else
            Return MyBase.PreProcessMessage(msg)
        End If
    End Function

#End Region

#Region " MOUSE EVENTS "

    Protected Sub UpdateImageOnMouseMove()
        Me.hover = True
        If Me.ReadOnly Then
        ElseIf Me.Checked Then
            If Me.down Then
                If (Me.CheckedImage IsNot Nothing) AndAlso (Me.Image IsNot Me.CheckedImage) Then
                    Me.Image = Me.CheckedImage
                End If
            Else
                If Me._CheckedHoverImage IsNot Nothing Then
                    Me.Image = Me.CheckedHoverImage
                Else
                    Me.Image = Me.UncheckedImage
                End If
            End If
        Else
            If Me.down Then
                If Me.CheckedHoverImage IsNot Nothing Then
                    Me.Image = Me.CheckedHoverImage
                Else
                    If Me._UncheckedHoverImage IsNot Nothing Then
                        Me.Image = Me.UncheckedHoverImage
                    Else
                        Me.Image = Me.UncheckedImage
                    End If
                End If
            Else
                If Me.UncheckedHoverImage IsNot Nothing Then
                    Me.Image = Me.UncheckedHoverImage
                Else
                    Me.Image = Me.UncheckedImage
                End If
            End If
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseMove" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseMove(ByVal e As MouseEventArgs)
        Me.UpdateImageOnMouseMove()
        MyBase.OnMouseMove(e)
    End Sub

    ''' <summary> Updates the image mouse leave. </summary>
    Protected Sub UpdateImageMouseLeave()
        Me.hover = False
        If Me.Checked Then
            Me.Image = Me.CheckedImage
        Else
            Me.Image = Me.UncheckedImage
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseLeave" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnMouseLeave(ByVal e As EventArgs)
        Me.UpdateImageMouseLeave()
        MyBase.OnMouseLeave(e)
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseDown" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseDown(ByVal e As MouseEventArgs)
        Me.Focus()
        Me.OnMouseUp(Nothing)
        Me.down = True
        If Me.ReadOnly Then
        ElseIf Me.Checked Then
            If Me.CheckedHoverImage IsNot Nothing Then
                Me.Image = Me.CheckedHoverImage
            End If
        Else
            If Me.CheckedImage IsNot Nothing Then
                Me.Image = Me.CheckedImage
            End If
        End If
        MyBase.OnMouseDown(e)
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseUp" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseUp(ByVal e As MouseEventArgs)
        Me.down = False
        If Me.ReadOnly Then
        ElseIf Me.Checked Then
            If Me.hover Then
                If Me.CheckedHoverImage IsNot Nothing Then
                    Me.Image = Me.CheckedHoverImage
                End If
            Else
                Me.Image = Me.CheckedImage
            End If
        Else
            If Me.hover Then
                If Me.UncheckedHoverImage IsNot Nothing Then
                    Me.Image = Me.UncheckedHoverImage
                End If
            Else
                Me.Image = Me.UncheckedImage
            End If
        End If
        MyBase.OnMouseUp(e)
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.LostFocus" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLostFocus(ByVal e As EventArgs)
        Me.holdingSpace = False
        Me.OnMouseUp(Nothing)
        MyBase.OnLostFocus(e)
    End Sub

#End Region

#Region " GRAPHICS EVENTS"

    ''' <summary> Renders the image described by the paint events. </summary>
    ''' <param name="pe"> Paint event information. </param>
    Private Sub RenderImage(ByVal pe As PaintEventArgs)
        If Me.Image IsNot Nothing Then
            Dim matrix As New ColorMatrix()
            Dim value As Single = If(Me.Enabled, 1.0F, 0.6F)
            matrix.Matrix33 = value
            Dim g As Graphics = pe.Graphics
            g.Clear(Me.BackColor)
            Using attributes As New ImageAttributes()
                attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap)
                Using bmp As New Bitmap(Me.Image, New Size(Me.Width, Me.Height))
                    g.DrawImage(bmp, New Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, bmp.Width, bmp.Height, GraphicsUnit.Pixel, attributes)
                End Using
            End Using
        Else
            MyBase.OnPaint(pe)
        End If
    End Sub

    ''' <summary> Draw text. </summary>
    ''' <param name="pe"> Paint event information. </param>
    Private Sub DrawText(ByVal pe As PaintEventArgs)
        If ((Not String.IsNullOrEmpty(MyBase.Text))) AndAlso (pe IsNot Nothing) AndAlso (MyBase.Font IsNot Nothing) Then
            Dim drawStringSize As SizeF = pe.Graphics.MeasureString(MyBase.Text, MyBase.Font)
            Dim drawPoint As PointF
            If MyBase.Image IsNot Nothing Then
                drawPoint = New PointF(MyBase.Image.Width \ 2 - CInt(drawStringSize.Width) \ 2,
                                       MyBase.Image.Height \ 2 - CInt(drawStringSize.Height) \ 2)
            Else
                drawPoint = New PointF(Me.Width \ 2 - CInt(drawStringSize.Width) \ 2,
                                       Me.Height \ 2 - CInt(drawStringSize.Height) \ 2)
            End If
            Using drawBrush As New SolidBrush(MyBase.ForeColor)
                pe.Graphics.DrawString(MyBase.Text, MyBase.Font, drawBrush, drawPoint)
            End Using
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
    ''' <param name="pe"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                   event data. </param>
    Protected Overrides Sub OnPaint(ByVal pe As PaintEventArgs)
        Me.RenderImage(pe)
        Me.DrawText(pe)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.TextChanged" /> event.
    ''' </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnTextChanged(ByVal e As EventArgs)
        Me.Refresh()
        MyBase.OnTextChanged(e)
    End Sub

#End Region

#Region " VALUE "

    Private _Readonly As Boolean
    ''' <summary> Gets or sets the read only value. </summary>
    ''' <value> The read only value. </value>
    <Category("Appearance"), Description("Read only value."), DefaultValue(False)>
    Public Property [ReadOnly] As Boolean
        Get
            Return Me._Readonly
        End Get
        Set(value As Boolean)
            If value <> Me.ReadOnly Then
                Me._Readonly = value
                Me.Refresh()
            End If
        End Set
    End Property

    Private _Checked As Boolean
    ''' <summary> Gets or sets the checked value. </summary>
    ''' <value> The checked value. </value>
    <Category("Appearance"), Description("Checked value."), DefaultValue(False)>
    Public Property Checked As Boolean
        Get
            Return Me._Checked
        End Get
        Set(value As Boolean)
            If value <> Me.Checked Then
                Me._Checked = value
                Me.UpdateImageMouseLeave()
                Me.OnCheckChanged(EventArgs.Empty)
            End If
        End Set
    End Property

    Public Event CheckChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Raises the system. event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnCheckChanged(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.CheckChangedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Click" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnClick(e As EventArgs)
        MyBase.OnClick(e)
        If Not Me.ReadOnly Then
            Me._Checked = Not Me.Checked
            Me.OnCheckChanged(e)
        End If
    End Sub

#End Region

End Class


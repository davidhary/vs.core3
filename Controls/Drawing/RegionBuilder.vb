﻿Imports System.Runtime.InteropServices
Public NotInheritable Class RegionBuilder

    Private Sub New()
        MyBase.New
    End Sub

    Public Shared Function BuildRoundRegion(ByVal rect As Rectangle, ByVal radius As Integer) As Integer
        Return SafeNativeMethods.CreateRoundRectRgn(rect.X, rect.Y, rect.X + rect.Width, rect.Y + rect.Height, radius, radius).ToInt32
    End Function

    Private NotInheritable Class SafeNativeMethods
        Private Sub New()
            MyBase.New()
        End Sub

        <DllImport("Gdi32.dll", EntryPoint:="CreateRoundRectRgn")>
        Public Shared Function CreateRoundRectRgn(ByVal nLeftRect As Integer, ByVal nTopRect As Integer,
                                                  ByVal nRightRect As Integer, ByVal nBottomRect As Integer,
                                                  ByVal nWidthEllipse As Integer, ByVal nHeightEllipse As Integer) As IntPtr
        End Function

    End Class


End Class

Imports System.Drawing.Design
Imports System.Windows.Forms.Design
Imports System.Drawing.Drawing2D
''' <summary> A shape type editor control. </summary>
''' <license>
''' (c) 2016 Yang Kok Wah. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="5/14/2016" by="David" revision=""> 
''' http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </history>
Friend Class ShapeTypeEditorControl
    Inherits System.Windows.Forms.UserControl

    Public Sub New(initial_shape As ShapeType)
        MyBase.New()
        Me.InitializeComponent()
        Me.Shape = initial_shape

        'Find the number of shapes in the enumeration
        Dim numshape As Integer = [Enum].GetValues(GetType(ShapeType)).GetLength(0)

        'Find the number of rows and columns to accommodate the shapes
        Dim numcol As Integer = 0, numrow As Integer = 0
        numrow = CInt(Math.Truncate(Math.Sqrt(numshape)))
        numcol = numshape \ numrow
        If numshape Mod numcol > 0 Then
            numcol += 1
        End If

        ' Record the specifications
        _numrow = numrow
        _numcol = numcol

        _valid_width = _numcol * _width + (_numcol - 1) * 6 + 2 * 4
        _valid_height = _numrow * _height + (_numrow - 1) * 6 + 2 * 4
    End Sub

    Private Sub InitializeComponent()
        ' 
        ' ShapeTypeEditorControl
        ' 
        Me.BackColor = System.Drawing.Color.LightGray
        Me.Name = "ShapeTypeEditorControl"

    End Sub

    ' Stores the shape
    Public Shape As ShapeType

    ' Specification for the UI

    'number of shapes
    ' Private _Numshape As Integer

    'number of rows
    'number of columns
    'width of each shape
    'height of each shape
#Disable Warning IDE0044 ' Add read only modifier
    Private _numrow As Integer, _numcol As Integer, _valid_width As Integer, _valid_height As Integer, _width As Integer = 20, _height As Integer = 20
#Enable Warning IDE0044 ' Add read only modifier

    Protected Overrides Sub OnPaint(e As System.Windows.Forms.PaintEventArgs)
        If e Is Nothing Then Return
        Using bm As New Bitmap(Me.Width, Me.Height, e.Graphics)
            Dim g As Graphics = Graphics.FromImage(bm)
            g.FillRectangle(Brushes.LightGray, New Rectangle(0, 0, bm.Width, bm.Height))
            e.Graphics.FillRectangle(Brushes.LightGray, New Rectangle(0, 0, Me.Width, Me.Height))
            Dim x As Integer = 4, y As Integer = 4
            Dim n As Integer = 0
            For Each shape As ShapeType In [Enum].GetValues(GetType(ShapeType))
                Using path As New GraphicsPath()
                    ShapeControl.UpdateOutline(path, shape, _width, _height)
                    g.FillRectangle(Brushes.LightGray, 0, 0, bm.Width, bm.Height)
                    g.FillPath(Brushes.Yellow, path)
                    g.DrawPath(Pens.Red, path)
                    e.Graphics.DrawImage(bm, x, y, New Rectangle(New Point(0, 0), New Size(_width + 1, _height + 1)), GraphicsUnit.Pixel)
                    If Me.Shape.Equals(shape) Then
                        e.Graphics.DrawRectangle(Pens.Red, New Rectangle(New Point(x - 2, y - 2), New Size(_width + 4, _height + 4)))
                    End If
                    n += 1
                    x = (n Mod _numcol) * (_width)
                    x = x + (n Mod _numcol) * 6 + 4
                    y = (n \ _numcol) * (_height)
                    y = y + (n \ _numcol) * 6 + 4
                End Using
            Next
        End Using
    End Sub


    Protected Overrides Sub OnMouseDown(e As System.Windows.Forms.MouseEventArgs)
        If e Is Nothing Then Return
        If e.Button.Equals(MouseButtons.Left) Then
            If e.X > _valid_width Then
                Return
            End If
            If e.Y > _valid_height Then
                Return
            End If

            Dim x As Integer, y As Integer
            Dim n As Integer
            x = e.X
            y = e.Y
            n = (y \ (_valid_height \ _numrow)) * _numcol + ((x \ (_valid_width \ _numcol)) Mod _numcol)

            Dim count As Integer = 0

            For Each shape As ShapeType In [Enum].GetValues(GetType(ShapeType))
                If count = n Then
                    Me.Shape = shape
                    'close the editor immediately
                    SendKeys.Send("{ENTER}")
                End If
                count += 1
            Next
        End If

    End Sub

End Class




Public Class ShapeTypeEditor
    Inherits System.Drawing.Design.UITypeEditor
    Public Sub New()
    End Sub

    ' Indicates whether the UITypeEditor provides a form-based (modal) dialog, 
    ' drop down dialog, or no UI outside of the properties window.
    <System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name:="FullTrust")>
    Public Overrides Function GetEditStyle(context As System.ComponentModel.ITypeDescriptorContext) As System.Drawing.Design.UITypeEditorEditStyle
        Return UITypeEditorEditStyle.DropDown
    End Function

    ' Displays the UI for value selection.
    <System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name:="FullTrust")>
    Public Overrides Function EditValue(context As System.ComponentModel.ITypeDescriptorContext, provider As System.IServiceProvider, value As Object) As Object
        If provider Is Nothing Then Throw New ArgumentNullException(NameOf(provider))
        ' Return the value if the value is not of type ShapeType
        If value Is Nothing OrElse value.[GetType]() IsNot GetType(ShapeType) Then
            Return value
        End If

        ' Uses the IWindowsFormsEditorService to display a 
        ' drop-down UI in the Properties window.
        Dim edSvc As IWindowsFormsEditorService = DirectCast(provider.GetService(GetType(IWindowsFormsEditorService)), IWindowsFormsEditorService)
        If edSvc IsNot Nothing Then
            ' Display an Shape Type Editor Control and retrieve the value.
            Using editor As New ShapeTypeEditorControl(CType(value, ShapeType))
                edSvc.DropDownControl(editor)
                ' Return the value in the appropriate data format.
                If value.[GetType]() Is GetType(ShapeType) Then
                    Dim result As ShapeType = editor.Shape
                    Return result
                End If
            End Using
        End If
        Return value
    End Function

    ' Draws a representation of the property's value.
    <System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name:="FullTrust")>
    Public Overrides Sub PaintValue(e As System.Drawing.Design.PaintValueEventArgs)
        If e Is Nothing Then Return
        Using bm As New Bitmap(e.Bounds.Width + 4, e.Bounds.Height + 4, e.Graphics)
            Using g As Graphics = Graphics.FromImage(bm)
                Dim shape As ShapeType = CType(e.Value, ShapeType)
                Using path As New GraphicsPath()
                    ShapeControl.updateOutline(path, shape, e.Bounds.Width - 5, e.Bounds.Height - 5)
                    g.FillPath(Brushes.Yellow, path)
                    g.DrawPath(Pens.Red, path)
                    e.Graphics.DrawImage(bm, 3, 3, New Rectangle(New Point(0, 0), New Size(e.Bounds.Width, e.Bounds.Height)), GraphicsUnit.Pixel)
                End Using
            End Using
        End Using

    End Sub

    ' Indicates whether the UITypeEditor supports painting a 
    ' representation of a property's value.
    <System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name := "FullTrust")> _
	Public Overrides Function GetPaintValueSupported(context As System.ComponentModel.ITypeDescriptorContext) As Boolean
		Return True
	End Function
End Class




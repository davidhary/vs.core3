Imports System.Drawing.Design
Imports System.Windows.Forms.Design
''' <summary> A color editor control. </summary>
''' <license>
''' (c) 2016 Yang Kok Wah. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="5/14/2016" by="David" revision=""> 
''' http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </history>
Friend Class ColorEditorControl
    Inherits System.Windows.Forms.UserControl

#Region " CONSTRUCTOR "

    Public Sub New(initial_color As Color)
        Me.color = initial_color
        Me.old_color = initial_color

        InitializeComponent()
    End Sub

#End Region

#Region " DESIGNER "

    Private trackBarAlpha As System.Windows.Forms.TrackBar
    Private trackBarRed As System.Windows.Forms.TrackBar
    Private trackBarGreen As System.Windows.Forms.TrackBar
    Private trackBarBlue As System.Windows.Forms.TrackBar
    Private label1 As System.Windows.Forms.Label
    Private label2 As System.Windows.Forms.Label
    Private label3 As System.Windows.Forms.Label
    Private label4 As System.Windows.Forms.Label

    Public color As Color, old_color As Color
    Private labelColor As System.Windows.Forms.Label
    Private panelColor As System.Windows.Forms.Panel

    Private Sub InitializeComponent()
        Me.trackBarAlpha = New System.Windows.Forms.TrackBar()
        Me.trackBarRed = New System.Windows.Forms.TrackBar()
        Me.trackBarGreen = New System.Windows.Forms.TrackBar()
        Me.trackBarBlue = New System.Windows.Forms.TrackBar()
        Me.label1 = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        Me.label3 = New System.Windows.Forms.Label()
        Me.label4 = New System.Windows.Forms.Label()
        Me.panelColor = New System.Windows.Forms.Panel()
        Me.labelColor = New System.Windows.Forms.Label()
        DirectCast(Me.trackBarAlpha, System.ComponentModel.ISupportInitialize).BeginInit()
        DirectCast(Me.trackBarRed, System.ComponentModel.ISupportInitialize).BeginInit()
        DirectCast(Me.trackBarGreen, System.ComponentModel.ISupportInitialize).BeginInit()
        DirectCast(Me.trackBarBlue, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelColor.SuspendLayout()
        Me.SuspendLayout()
        ' 
        ' trackBarAlpha
        ' 
        Me.trackBarAlpha.Location = New System.Drawing.Point(40, 3)
        Me.trackBarAlpha.Maximum = 255
        Me.trackBarAlpha.Name = "trackBarAlpha"
        Me.trackBarAlpha.Size = New System.Drawing.Size(94, 45)
        Me.trackBarAlpha.TabIndex = 0
        Me.trackBarAlpha.TickFrequency = 20
        AddHandler Me.trackBarAlpha.ValueChanged, New System.EventHandler(AddressOf Me.trackBar_ValueChanged)
        ' 
        ' trackBarRed
        ' 
        Me.trackBarRed.Location = New System.Drawing.Point(40, 33)
        Me.trackBarRed.Maximum = 255
        Me.trackBarRed.Name = "trackBarRed"
        Me.trackBarRed.Size = New System.Drawing.Size(94, 45)
        Me.trackBarRed.TabIndex = 1
        Me.trackBarRed.TickFrequency = 20
        AddHandler Me.trackBarRed.ValueChanged, New System.EventHandler(AddressOf Me.trackBar_ValueChanged)
        ' 
        ' trackBarGreen
        ' 
        Me.trackBarGreen.Location = New System.Drawing.Point(40, 65)
        Me.trackBarGreen.Maximum = 255
        Me.trackBarGreen.Name = "trackBarGreen"
        Me.trackBarGreen.Size = New System.Drawing.Size(94, 45)
        Me.trackBarGreen.TabIndex = 2
        Me.trackBarGreen.TickFrequency = 20
        AddHandler Me.trackBarGreen.ValueChanged, New System.EventHandler(AddressOf Me.trackBar_ValueChanged)
        ' 
        ' trackBarBlue
        ' 
        Me.trackBarBlue.Location = New System.Drawing.Point(40, 97)
        Me.trackBarBlue.Maximum = 255
        Me.trackBarBlue.Name = "trackBarBlue"
        Me.trackBarBlue.Size = New System.Drawing.Size(94, 45)
        Me.trackBarBlue.TabIndex = 3
        Me.trackBarBlue.TickFrequency = 20
        AddHandler Me.trackBarBlue.ValueChanged, New System.EventHandler(AddressOf Me.trackBar_ValueChanged)
        ' 
        ' label1
        ' 
        Me.label1.Location = New System.Drawing.Point(8, 6)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(40, 24)
        Me.label1.TabIndex = 5
        Me.label1.Text = "Alpha"
        ' 
        ' label2
        ' 
        Me.label2.Location = New System.Drawing.Point(8, 38)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(48, 24)
        Me.label2.TabIndex = 6
        Me.label2.Text = "Red"
        ' 
        ' label3
        ' 
        Me.label3.Location = New System.Drawing.Point(8, 70)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(48, 24)
        Me.label3.TabIndex = 7
        Me.label3.Text = "Green"
        ' 
        ' label4
        ' 
        Me.label4.Location = New System.Drawing.Point(8, 104)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(48, 24)
        Me.label4.TabIndex = 8
        Me.label4.Text = "Blue"
        ' 
        ' panelColor
        ' 
        Me.panelColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panelColor.Controls.Add(Me.labelColor)
        Me.panelColor.Location = New System.Drawing.Point(136, 11)
        Me.panelColor.Name = "panelColor"
        Me.panelColor.Size = New System.Drawing.Size(31, 106)
        Me.panelColor.TabIndex = 9
        ' 
        ' labelColor
        ' 
        Me.labelColor.Location = New System.Drawing.Point(-10, -3)
        Me.labelColor.Name = "labelColor"
        Me.labelColor.Size = New System.Drawing.Size(56, 160)
        Me.labelColor.TabIndex = 10
        ' 
        ' ColorEditorControl
        ' 
        Me.BackColor = System.Drawing.Color.LightGray
        Me.Controls.Add(Me.trackBarBlue)
        Me.Controls.Add(Me.trackBarGreen)
        Me.Controls.Add(Me.trackBarRed)
        Me.Controls.Add(Me.panelColor)
        Me.Controls.Add(Me.trackBarAlpha)
        Me.Controls.Add(Me.label4)
        Me.Controls.Add(Me.label3)
        Me.Controls.Add(Me.label2)
        Me.Controls.Add(Me.label1)
        Me.Name = "ColorEditorControl"
        Me.Size = New System.Drawing.Size(171, 135)
        AddHandler Me.KeyPress, New System.Windows.Forms.KeyPressEventHandler(AddressOf Me.ColorEditorControl_KeyPress)
        AddHandler Me.Load, New System.EventHandler(AddressOf Me.ColorEditorControl_Load)
        DirectCast(Me.trackBarAlpha, System.ComponentModel.ISupportInitialize).EndInit()
        DirectCast(Me.trackBarRed, System.ComponentModel.ISupportInitialize).EndInit()
        DirectCast(Me.trackBarGreen, System.ComponentModel.ISupportInitialize).EndInit()
        DirectCast(Me.trackBarBlue, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelColor.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region " EVENT HANDLERS "

    Private Sub ColorEditorControl_Load(sender As Object, e As System.EventArgs)
        Dim argb As Integer = Me.color.ToArgb()
        ' the colors are store in the argb value as 4 byte : AARRGGBB
        Dim alpha As Byte = CByte(argb >> 24)
        Dim red As Byte = CByte(argb >> 16)
        Dim green As Byte = CByte(argb >> 8)
        Dim blue As Byte = CByte(argb)
        trackBarAlpha.Value = alpha
        trackBarRed.Value = red
        trackBarGreen.Value = green
        trackBarBlue.Value = blue

        'Foreground Label on the Color Panel
        labelColor.BackColor = Color.FromArgb(alpha, red, green, blue)

        'Create the Background Image for Color Panel 
        'The Color Panel is to allow the user to check on Alpha transparency
        Dim bm As New Bitmap(Me.panelColor.Width, Me.panelColor.Height)
        Me.panelColor.BackgroundImage = bm
        Dim g As Graphics = Graphics.FromImage(Me.panelColor.BackgroundImage)
        g.FillRectangle(Brushes.White, 0, 0, Me.panelColor.Width, Me.panelColor.Height)

        'For formating the string
        Using sf As New StringFormat()
            sf.Alignment = StringAlignment.Center
            sf.LineAlignment = StringAlignment.Center

            'For rotating the string 
            'If you want the text to be rotated uncomment the 5 lines below and comment off the last line

            Using font As New Font("Arial", 16, FontStyle.Bold)
                'Matrix m=new Matrix ();
                'm.Rotate(90);
                'm.Translate(this.panelColor.Width ,0 ,MatrixOrder.Append);
                'g.Transform=m;
                'g.DrawString("TEST",new Font("Arial",16,FontStyle.Bold),Brushes.Black,new Rectangle(0,0,panelColor.Height ,panelColor.Width  ),sf);
                g.DrawString("TEST", font, Brushes.Black, New Rectangle(0, 0, panelColor.Width, panelColor.Height), sf)
            End Using
        End Using
    End Sub

    Private Sub UpdateColor(a As Integer, r As Integer, g As Integer, b As Integer)
        Me.color = Color.FromArgb(a, r, g, b)
        labelColor.BackColor = Me.color
    End Sub

    Private Sub TrackBar_ValueChanged(sender As Object, e As System.EventArgs)
        UpdateColor(trackBarAlpha.Value, trackBarRed.Value, trackBarGreen.Value, trackBarBlue.Value)
    End Sub

    Private Sub ColorEditorControl_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs)
        'If the user hit the escape key we restore back the old color
        If e.KeyChar.Equals(Keys.Escape) Then
            Me.color = Me.old_color
        End If
    End Sub
#End Region

End Class

''' <summary> Editor for color. </summary>
''' <license>
''' (c) 2016 Yang Kok Wah. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="5/14/2016" by="David" revision=""> 
''' http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </history>
Public Class ColorEditor
    Inherits System.Drawing.Design.UITypeEditor

    Public Sub New()
    End Sub

    ' Indicates whether the UITypeEditor provides a form-based (modal) dialog, 
    ' drop down dialog, or no UI outside of the properties window.
    <System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name:="FullTrust")>
    Public Overrides Function GetEditStyle(context As System.ComponentModel.ITypeDescriptorContext) As System.Drawing.Design.UITypeEditorEditStyle
        Return UITypeEditorEditStyle.DropDown
    End Function

    ' Displays the UI for value selection.
    <System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name:="FullTrust")>
    Public Overrides Function EditValue(context As System.ComponentModel.ITypeDescriptorContext, provider As System.IServiceProvider, value As Object) As Object
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        If provider Is Nothing Then Throw New ArgumentNullException(NameOf(provider))
        If value.[GetType]() IsNot GetType(Color) Then
            Return value
        End If

        ' Uses the IWindowsFormsEditorService to display a 
        ' drop-down UI in the Properties window.
        Dim edSvc As IWindowsFormsEditorService = DirectCast(provider.GetService(GetType(IWindowsFormsEditorService)), IWindowsFormsEditorService)
        If edSvc IsNot Nothing Then
            Using editor As New ColorEditorControl(CType(value, Color))
                edSvc.DropDownControl(editor)
                ' Return the value in the appropriate data format.
                If value.[GetType]() Is GetType(Color) Then
                    Dim result As Color = editor.color
                    Return result
                End If
            End Using
        End If
        Return value
    End Function

    ' Draws a representation of the property's value.
    <System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name:="FullTrust")>
    Public Overrides Sub PaintValue(e As System.Drawing.Design.PaintValueEventArgs)
        If e Is Nothing Then Return
        Using br As New SolidBrush(CType(e.Value, Color))
            e.Graphics.FillRectangle(br, 1, 1, e.Bounds.Width, e.Bounds.Height)
        End Using
    End Sub

    ' Indicates whether the UITypeEditor supports painting a 
    ' representation of a property's value.
    <System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name:="FullTrust")>
    Public Overrides Function GetPaintValueSupported(context As System.ComponentModel.ITypeDescriptorContext) As Boolean
        Return True
    End Function
End Class





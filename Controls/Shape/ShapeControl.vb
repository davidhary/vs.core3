Imports System.ComponentModel
Imports System.Drawing.Drawing2D
''' <summary> A shape control. </summary>
''' <license>
''' (c) 2016 Yang Kok Wah. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="5/14/2016" by="David" revision=""> 
''' http://www.codeproject.com/Articles/10558/Shape-Control-for-NET. </history>
Public Class ShapeControl
    Inherits System.Windows.Forms.Control

#Region " CONSTRUCTOR "

    Public Sub New()
        MyBase.New()
        ' This call is required by the Windows.Forms Form Designer.
        InitializeComponent()
        Me.DoubleBuffered = True
        'Using of Double Buffer allow for smooth rendering 
        'minimizing flickering
        Me.SetStyle(ControlStyles.SupportsTransparentBackColor Or ControlStyles.DoubleBuffer Or ControlStyles.AllPaintingInWmPaint Or ControlStyles.UserPaint, True)

        'set the default back color and font
        Me.BackColor = Color.FromArgb(0, 255, 255, 255)
        Me.Font = New Font("Arial", 12, FontStyle.Bold)
    End Sub


    Protected Overrides Sub Dispose(disposing As Boolean)
        If disposing Then
            If Me._outline IsNot Nothing Then
                Me._outline.Dispose()
                Me._outline = Nothing
            End If
            If components IsNot Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region


#Region "Component Designer generated code"

    Private Sub InitializeComponent()
        '  Me.DoubleBuffered = True
        Me.components = New System.ComponentModel.Container()
        Me.timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        ' 
        ' timer1
        ' 
        Me.timer1.Interval = 200
        AddHandler Me.timer1.Tick, New System.EventHandler(AddressOf Me.timer1_Tick)
        ' 
        ' timer2
        ' 
        Me.timer2.Interval = 200
        AddHandler Me.timer2.Tick, New System.EventHandler(AddressOf Me.timer2_Tick)
        ' 
        ' CustomControl1
        ' 
        AddHandler Me.TextChanged, New System.EventHandler(AddressOf Me.ShapeControl_TextChanged)
        Me.ResumeLayout(False)

    End Sub

#End Region

    ' Private _Custompath As New GraphicsPath()
    Private components As IContainer
    Private _Istextset As Boolean = False
    Private _Shape As ShapeType = ShapeType.Rectangle
    Private _Borderstyle As DashStyle = DashStyle.Solid
    Private _Bordercolor As Color = Color.FromArgb(255, 255, 0, 0)
    Private _Borderwidth As Integer = 3
    Private _Outline As New GraphicsPath()
    Private _Usegradient As Boolean = False
    Private _Blink As Boolean = False
    Private _Vibrate As Boolean = False
    Private _Voffseted As Boolean = False
    Private _Centercolor As Color = Color.FromArgb(100, 255, 0, 0)
    Private _Surroundcolor As Color = Color.FromArgb(100, 0, 255, 255)
    Private timer1 As Timer
    Private timer2 As Timer
    Private _Tag2 As String = ""
    Private _Bm As Bitmap

    <Category("Shape"), Description("Additional user-defined data")>
    Public Property Tag2() As String
        Get
            Return _tag2
        End Get
        Set
            _tag2 = Value
        End Set
    End Property

    <Category("Shape"), Description("Causes the control to blink")>
    Public Property Blink() As Boolean
        Get
            Return _blink
        End Get
        Set

            _blink = Value
            timer1.Enabled = _blink
            If Not _blink Then
                Me.Visible = True
            End If
        End Set
    End Property

    <Category("Shape"), Description("Causes the control to vibrate")>
    Public Property Vibrate() As Boolean
        Get
            Return _vibrate
        End Get
        Set

            _vibrate = Value
            timer2.Enabled = _vibrate
            If Not _vibrate Then
                If _voffseted Then
                    Me.Top += 5
                    _voffseted = False
                End If
            End If
        End Set
    End Property

    <Category("Shape"), Description("Background Image to define outline")>
    Public Property ShapeImage() As Image
        Get
            Return _bm
        End Get
        Set
            If Value IsNot Nothing Then
                _bm = DirectCast(Value.Clone(), Bitmap)
                Width = 150
                Height = 150
                OnResize(Nothing)
            Else
                If _bm IsNot Nothing Then
                    _bm = Nothing
                End If

                OnResize(Nothing)
            End If
        End Set
    End Property

    <Category("Shape"), Description("Text to display")>
    Public Overrides Property Text() As String
        Get
            Return MyBase.Text
        End Get
        Set

            MyBase.Text = Value

            'When Visual Studio first create a new control, text=name
            'we do not want any default text, thus we override it with blank
            If (Not _istextset) AndAlso (MyBase.Text.Equals(MyBase.Name)) Then
                MyBase.Text = ""
            End If
            _istextset = True
        End Set
    End Property

    'Override the BackColor Property To be associated with our custom editor
    <Category("Shape"), Description("Back Color")>
    <BrowsableAttribute(True)>
    <EditorAttribute(GetType(ColorEditor), GetType(System.Drawing.Design.UITypeEditor))>
    Public Overrides Property BackColor() As Color
        Get
            Return MyBase.BackColor
        End Get
        Set
            MyBase.BackColor = Value

            Me.Refresh()
        End Set
    End Property


    <Category("Shape"), Description("Using Gradient to fill Shape")>
    Public Property UseGradient() As Boolean
        Get
            Return _usegradient
        End Get
        Set
            _usegradient = Value
            Me.Refresh()
        End Set
    End Property

    'For Gradient Rendering, this is the color at the center of the shape
    <Category("Shape"), Description("Color at center")>
    <BrowsableAttribute(True)>
    <EditorAttribute(GetType(ColorEditor), GetType(System.Drawing.Design.UITypeEditor))>
    Public Property CenterColor() As Color
        Get
            Return _centercolor
        End Get
        Set
            _centercolor = Value
            Me.Refresh()
        End Set
    End Property

    'For Gradient Rendering, this is the color at the edges of the shape
    <Category("Shape"), Description("Color at the edges of the Shape")>
    <BrowsableAttribute(True)>
    <EditorAttribute(GetType(ColorEditor), GetType(System.Drawing.Design.UITypeEditor))>
    Public Property SurroundColor() As Color
        Get
            Return _surroundcolor
        End Get
        Set
            _surroundcolor = Value
            Me.Refresh()
        End Set
    End Property


    <Category("Shape"), Description("Border Width")>
    Public Property BorderWidth() As Integer
        Get
            Return _borderwidth
        End Get
        Set
            _borderwidth = Value
            If _borderwidth < 0 Then
                _borderwidth = 0
            End If

            Me.Refresh()
        End Set
    End Property

    <Category("Shape"), Description("Border Color")>
    <BrowsableAttribute(True)>
    <EditorAttribute(GetType(ColorEditor), GetType(System.Drawing.Design.UITypeEditor))>
    Public Property BorderColor() As Color
        Get
            Return _bordercolor
        End Get
        Set
            _bordercolor = Value
            Me.Refresh()
        End Set
    End Property

    <Category("Shape"), Description("Border Style")>
    Public Property BorderStyle() As DashStyle
        Get
            Return _borderstyle
        End Get
        Set
            _borderstyle = Value
            Me.Refresh()
        End Set
    End Property

    <Category("Shape"), Description("Select Shape")>
    <BrowsableAttribute(True)>
    <EditorAttribute(GetType(ShapeTypeEditor), GetType(System.Drawing.Design.UITypeEditor))>
    Public Property Shape() As ShapeType
        Get
            Return _shape
        End Get
        Set
            _shape = Value
            OnResize(Nothing)
        End Set
    End Property

    'This function creates the path for each shape
    'It is also being used by the ShapeTypeEditor to create the various shapes
    'for the Shape property editor UI
    Friend Shared Sub UpdateOutline(ByRef outline As GraphicsPath, shape As ShapeType, width As Integer, height As Integer)
        Select Case shape
            Case ShapeType.CustomPie
                outline.AddPie(0, 0, width, height, 180, 270)
                Exit Select
            Case ShapeType.CustomPolygon
                outline.AddPolygon(New Point() {New Point(0, 0), New Point(width \ 2, height \ 4), New Point(width, 0), New Point((width * 3) \ 4, height \ 2), New Point(width, height), New Point(width \ 2, (height * 3) \ 4),
                    New Point(0, height), New Point(width \ 4, height \ 2)})
                Exit Select
            Case ShapeType.Diamond
                outline.AddPolygon(New Point() {New Point(0, height \ 2), New Point(width \ 2, 0), New Point(width, height \ 2), New Point(width \ 2, height)})
                Exit Select

            Case ShapeType.Rectangle
                outline.AddRectangle(New Rectangle(0, 0, width, height))
                Exit Select

            Case ShapeType.Ellipse
                outline.AddEllipse(0, 0, width, height)
                Exit Select

            Case ShapeType.TriangleUp
                outline.AddPolygon(New Point() {New Point(0, height), New Point(width, height), New Point(width \ 2, 0)})
                Exit Select

            Case ShapeType.TriangleDown
                outline.AddPolygon(New Point() {New Point(0, 0), New Point(width, 0), New Point(width \ 2, height)})
                Exit Select

            Case ShapeType.TriangleLeft
                outline.AddPolygon(New Point() {New Point(width, 0), New Point(0, height \ 2), New Point(width, height)})
                Exit Select

            Case ShapeType.TriangleRight
                outline.AddPolygon(New Point() {New Point(0, 0), New Point(width, height \ 2), New Point(0, height)})
                Exit Select

            Case ShapeType.RoundedRectangle
                outline.AddArc(0, 0, width \ 4, width \ 4, 180, 90)
                outline.AddLine(width \ 8, 0, width - width \ 8, 0)
                outline.AddArc(width - width \ 4, 0, width \ 4, width \ 4, 270, 90)
                outline.AddLine(width, width \ 8, width, height - width \ 8)
                outline.AddArc(width - width \ 4, height - width \ 4, width \ 4, width \ 4, 0, 90)
                outline.AddLine(width - width \ 8, height, width \ 8, height)
                outline.AddArc(0, height - width \ 4, width \ 4, width \ 4, 90, 90)
                outline.AddLine(0, height - width \ 8, 0, width \ 8)
                Exit Select

            Case ShapeType.BalloonSW
                outline.AddArc(0, 0, width \ 4, width \ 4, 180, 90)
                outline.AddLine(width \ 8, 0, width - width \ 8, 0)
                outline.AddArc(width - width \ 4, 0, width \ 4, width \ 4, 270, 90)
                outline.AddLine(width, width \ 8, width, (height * 0.75F) - width \ 8)
                outline.AddArc(width - width \ 4, (height * 0.75F) - width \ 4, width \ 4, width \ 4, 0, 90)
                outline.AddLine(width - width \ 8, (height * 0.75F), width \ 8 + (width \ 4), (height * 0.75F))
                outline.AddLine(width \ 8 + (width \ 4), height * 0.75F, width \ 8 + (width \ 8), height)
                outline.AddLine(width \ 8 + (width \ 8), height, width \ 8 + (width \ 8), (height * 0.75F))
                outline.AddLine(width \ 8 + (width \ 8), (height * 0.75F), width \ 8, (height * 0.75F))
                outline.AddArc(0, (height * 0.75F) - width \ 4, width \ 4, width \ 4, 90, 90)
                outline.AddLine(0, (height * 0.75F) - width \ 8, 0, width \ 8)
                Exit Select

            Case ShapeType.BalloonSE
                outline.AddArc(0, 0, width \ 4, width \ 4, 180, 90)
                outline.AddLine(width \ 8, 0, width - width \ 8, 0)
                outline.AddArc(width - width \ 4, 0, width \ 4, width \ 4, 270, 90)
                outline.AddLine(width, width \ 8, width, (height * 0.75F) - width \ 8)
                outline.AddArc(width - width \ 4, (height * 0.75F) - width \ 4, width \ 4, width \ 4, 0, 90)
                outline.AddLine(width - width \ 8, (height * 0.75F), width - (width \ 4), (height * 0.75F))
                outline.AddLine(width - (width \ 4), height * 0.75F, width - (width \ 4), height)
                outline.AddLine(width - (width \ 4), height, width - (3 * width \ 8), (height * 0.75F))
                outline.AddLine(width - (3 * width \ 8), (height * 0.75F), width \ 8, (height * 0.75F))
                outline.AddArc(0, (height * 0.75F) - width \ 4, width \ 4, width \ 4, 90, 90)
                outline.AddLine(0, (height * 0.75F) - width \ 8, 0, width \ 8)
                Exit Select

            Case ShapeType.BalloonNW
                outline.AddArc(width - width \ 4, (height) - width \ 4, width \ 4, width \ 4, 0, 90)
                outline.AddLine(width - width \ 8, (height), width - (width \ 4), (height))
                outline.AddArc(0, (height) - width \ 4, width \ 4, width \ 4, 90, 90)
                outline.AddLine(0, (height) - width \ 8, 0, height * 0.25F + width \ 8)
                outline.AddArc(0, height * 0.25F, width \ 4, width \ 4, 180, 90)
                outline.AddLine(width \ 8, height * 0.25F, width \ 4, height * 0.25F)
                outline.AddLine(width \ 4, height * 0.25F, width \ 4, 0)
                outline.AddLine(width \ 4, 0, 3 * width \ 8, height * 0.25F)
                outline.AddLine(3 * width \ 8, height * 0.25F, width - width \ 8, height * 0.25F)
                outline.AddArc(width - width \ 4, height * 0.25F, width \ 4, width \ 4, 270, 90)
                outline.AddLine(width, width \ 8 + height * 0.25F, width, (height) - width \ 8)
                Exit Select

            Case ShapeType.BalloonNE
                outline.AddArc(width - width \ 4, (height) - width \ 4, width \ 4, width \ 4, 0, 90)
                outline.AddLine(width - width \ 8, (height), width - (width \ 4), (height))
                outline.AddArc(0, (height) - width \ 4, width \ 4, width \ 4, 90, 90)
                outline.AddLine(0, (height) - width \ 8, 0, height * 0.25F + width \ 8)
                outline.AddArc(0, height * 0.25F, width \ 4, width \ 4, 180, 90)
                outline.AddLine(width \ 8, height * 0.25F, 5 * width \ 8, height * 0.25F)
                outline.AddLine(5 * width \ 8, height * 0.25F, 3 * width \ 4, 0)
                outline.AddLine(3 * width \ 4, 0, 3 * width \ 4, height * 0.25F)
                outline.AddLine(3 * width \ 4, height * 0.25F, width - width \ 8, height * 0.25F)
                outline.AddArc(width - width \ 4, height * 0.25F, width \ 4, width \ 4, 270, 90)
                outline.AddLine(width, width \ 8 + height * 0.25F, width, (height) - width \ 8)
                Exit Select
            Case Else

                Exit Select
        End Select
    End Sub

    Protected Overrides Sub OnResize(e As EventArgs)
        If (Me.Width < 0) OrElse (Me.Height <= 0) Then
            Return
        End If

        If _bm Is Nothing Then

            _outline = New GraphicsPath()

            updateOutline(_outline, _shape, Me.Width, Me.Height)
        Else
            Using bm As Bitmap = DirectCast(_bm.Clone(), Bitmap)
                Using bm2 As New Bitmap(Width, Height)
                    System.Diagnostics.Debug.WriteLine(bm2.Width & "," & bm2.Height)
                    Graphics.FromImage(bm2).DrawImage(bm, New RectangleF(0, 0, bm2.Width, bm2.Height),
                                                      New RectangleF(0, 0, bm.Width, bm.Height), GraphicsUnit.Pixel)
                    Dim trace As New OutlineTrace()
                    Dim s As String = trace.TraceOutlineN(bm2, 0, bm2.Height \ 2, bm2.Width \ 2,
                                                          Color.Black, Color.White, True, 1)
                    Dim p As Point() = OutlineTrace.StringOutline2Polygon(s)
                    _outline = New GraphicsPath()
                    _outline.AddPolygon(p)
                End Using
            End Using
        End If
        If _outline IsNot Nothing Then
            Me.Region = New Region(_outline)
        End If

        Me.Refresh()
        MyBase.OnResize(e)
    End Sub

    Protected Overrides Sub OnPaint(e As PaintEventArgs)
        If e Is Nothing Then Return
        'Rendering with Gradient
        If _usegradient Then
            Using br As New PathGradientBrush(Me._outline)
                br.CenterColor = Me._centercolor
                br.SurroundColors = New Color() {Me._surroundcolor}
                e.Graphics.FillPath(br, Me._outline)
            End Using
        End If

        'Rendering with Border
        If _borderwidth > 0 Then
            Using p As New Pen(_bordercolor, _borderwidth * 2)
                p.DashStyle = _borderstyle
                e.Graphics.SmoothingMode = SmoothingMode.HighQuality
                e.Graphics.DrawPath(p, Me._outline)
            End Using
        End If

        'Rendering the text to be at the center of the shape
        Using sf As New StringFormat()
            sf.Alignment = StringAlignment.Center
            sf.LineAlignment = StringAlignment.Center
            Select Case _shape
                Case ShapeType.BalloonNE, ShapeType.BalloonNW
                    Using br As New SolidBrush(Me.ForeColor)
                        e.Graphics.DrawString(Me.Text, Me.Font, br, New RectangleF(0, Me.Height * 0.25F, Me.Width, Me.Height * 0.75F), sf)
                    End Using
                Case ShapeType.BalloonSE, ShapeType.BalloonSW
                    Using br As New SolidBrush(Me.ForeColor)
                        e.Graphics.DrawString(Me.Text, Me.Font, br, New RectangleF(0, 0, Me.Width, Me.Height * 0.75F), sf)
                    End Using
                Case Else
                    Using br As New SolidBrush(Me.ForeColor)
                        e.Graphics.DrawString(Me.Text, Me.Font, br, New Rectangle(0, 0, Me.Width, Me.Height), sf)
                    End Using
            End Select
        End Using
        ' Calling the base class OnPaint
        MyBase.OnPaint(e)
    End Sub


    Private Sub ShapeControl_TextChanged(sender As Object, e As System.EventArgs)
        Me.Refresh()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs)
        Me.Visible = Not Me.Visible
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs)
        If Not _vibrate Then
            Return
        End If
        _voffseted = Not _voffseted
        Me.Top = If(_voffseted, Me.Top - 5, Me.Top + 5)


    End Sub
End Class


'All the defined shape type
Public Enum ShapeType
    Rectangle
    RoundedRectangle
    Diamond
    Ellipse
    TriangleUp
    TriangleDown
    TriangleLeft
    TriangleRight
    ''' <summary> An enum constant representing the balloon NW option. </summary>
    BalloonNE
    BalloonNW
    BalloonSW
    BalloonSE
    CustomPolygon
    CustomPie
End Enum

﻿Imports System.ComponentModel
''' <summary> Selector combo box. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="3/6/2015" by="David" revision=""> Created. </history>
<Description("Selector Combo Box"), System.Drawing.ToolboxBitmap(GetType(SelectorComboBox))>
<System.Runtime.InteropServices.ComVisible(False)>
Public Class SelectorComboBox
    Inherits SelectorControlBase

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        Me.SelectorButton = Me.Button
        Me.SelectorTextBox = Me.ComboBox
        MyBase.OnDirtyChanged()
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Controls.SelectorComboBox and
    ''' optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " EXPOSED PROPERTIES "

    ''' <summary> Gets or sets the read only property. </summary>
    ''' <value> The read only. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Indicates whether the check box is read only.")>
    Public Overrides Property [ReadOnly]() As Boolean
        Get
            Return Me.ComboBox.ReadOnly
        End Get
        Set(value As Boolean)
            MyBase.ReadOnly = value
            Me.ComboBox.ReadOnly = value
            Me.Button.Visible = Not value
        End Set
    End Property

    ''' <summary> Gets or sets the enabled. </summary>
    ''' <remarks>
    ''' Had some issues. Setting enabled on the control toggles the combo box. But if the combo
    ''' enabled is set, the link breaks.
    ''' </remarks>
    ''' <value> The enabled. </value>
    Public Overloads Property Enabled As Boolean
        Get
            Return MyBase.Enabled
        End Get
        Set(value As Boolean)
            If MyBase.Enabled <> value Then
                MyBase.Enabled = value
            End If
            If Me.ComboBox.Enabled <> value Then
                Me.ComboBox.Enabled = value
            End If
        End Set
    End Property

#End Region

#Region " CAPTION HANDLERS "

    ''' <summary> Gets the selected item. </summary>
    ''' <value> The selected item. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SelectedItem As Object

    ''' <summary> Gets the selected value. </summary>
    ''' <value> The selected value. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SelectedValue As Object

    ''' <summary> Gets a value indicating whether a value was selected. </summary>
    ''' <value> <c>true</c> if the selector has a selected value; otherwise <c>false</c> </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property HasSelectedValue As Boolean
        Get
            Return Me.SelectedValue IsNot Nothing
        End Get
    End Property

#End Region

#Region " SELECT VALUE "

    ''' <summary> Select value. </summary>
    ''' <param name="value"> The value. </param>
    Public Overloads Sub SelectValue(ByVal value As String)
        Me.Text = value
        Me.SelectValue()
    End Sub

    ''' <summary> Searches for the first match for the given string. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> An Integer. </returns>
    Public Function FindIndex(ByVal value As String) As Integer
        Return Me.ComboBox.FindStringExact(value)
    End Function

    ''' <summary> Displays an existing value described by value. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub DisplayExistingValue(ByVal value As String)
        If Me.ComboBox.FindString(value) >= 0 Then
            Me.Text = value
        Else
            Me.Text = Me.Watermark
        End If
    End Sub

#End Region

#Region " EVENT HANDLERS "

    ''' <summary> Combo box data source changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ComboBox_DataSourceChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox.DataSourceChanged
        Me.OnContentChanged()
    End Sub

    ''' <summary> Combo box selected value changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox.SelectedValueChanged
        Me.OnDirtyChanged()
    End Sub

#End Region

End Class

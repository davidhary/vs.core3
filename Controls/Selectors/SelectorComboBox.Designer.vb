﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SelectorComboBox

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ComboBox = New isr.Core.Controls.ComboBox()
        Me.Button = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'ComboBox
        '
        Me.ComboBox.Dock = System.Windows.Forms.DockStyle.Top
        Me.ComboBox.FormattingEnabled = True
        Me.ComboBox.Location = New System.Drawing.Point(0, 0)
        Me.ComboBox.Name = "ComboBox"
        Me.ComboBox.ReadOnlyBackColor = System.Drawing.SystemColors.Control
        Me.ComboBox.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText
        Me.ComboBox.ReadWriteBackColor = System.Drawing.SystemColors.Window
        Me.ComboBox.ReadWriteForeColor = System.Drawing.SystemColors.ControlText
        Me.ComboBox.Size = New System.Drawing.Size(66, 25)
        Me.ComboBox.TabIndex = 0
        '
        'Button
        '
        Me.Button.Dock = System.Windows.Forms.DockStyle.Right
        Me.Button.Image = Global.isr.Core.Controls.My.Resources.Resources.dialog_ok_4
        Me.Button.Location = New System.Drawing.Point(66, 0)
        Me.Button.MaximumSize = New System.Drawing.Size(25, 25)
        Me.Button.Name = "Button"
        Me.Button.Size = New System.Drawing.Size(25, 25)
        Me.Button.TabIndex = 1
        Me.Button.UseVisualStyleBackColor = True
        '
        'SelectorComboBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.ComboBox)
        Me.Controls.Add(Me.Button)
        Me.Name = "SelectorComboBox"
        Me.Size = New System.Drawing.Size(91, 25)
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents ComboBox As isr.Core.Controls.ComboBox
    Public WithEvents Button As System.Windows.Forms.Button

End Class

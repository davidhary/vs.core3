Imports System.ComponentModel
Imports isr.Core.Controls.BindingExtensions
Imports isr.Core.Pith
Imports isr.Core.Pith.BindingExtensions
Imports isr.Core.Pith.ErrorProviderExtensions
Imports isr.Core.Pith.ExceptionExtensions
''' <summary> A control for selecting and connecting to string resource. </summary>
''' <license> (c) 2006 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/20/2006" by="David" revision="1.0.2242.x"> created. </history>
''' <history date="12/11/2018" by="David" revision="3.4.6819.x"> created from VI resource selector connector. </history>
<Description("Resource Selector and Connector Base Control")>
<System.Drawing.ToolboxBitmap(GetType(ResourceConnector), "ResourceConnector"), ToolboxItem(True)>
Public Class ResourceConnector
    Inherits ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
        Me._ClearButton.Visible = True
        Me._ToggleConnectionButton.Visible = True
        Me._EnumerateButton.Visible = True
        Me._ToggleConnectionButton.Enabled = False
        Me._ClearButton.Enabled = False
        Me._EnumerateButton.Enabled = True
        AddHandler Me._ResourceNamesComboBox.ComboBox.SelectedValueChanged, AddressOf HandleResourceNamesComboBoxValueChanged
    End Sub

    ''' <summary> Creates a new ResourceConnector. </summary>
    ''' <returns> A ResourceConnector. </returns>
    Public Shared Function Create() As ResourceConnector
        Dim connector As ResourceConnector = Nothing
        Try
            connector = New ResourceConnector
            Return connector
        Catch
            connector.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                          <c>False</c> to release only unmanaged resources when called from the
    '''                          runtime finalize. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                Me.AssignViewModel(Nothing)
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " BINDING MANAGEMENT "

    ''' <summary> Handles the binding complete event. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                       <see cref="Control"/> </param>
    ''' <param name="e">      Binding complete event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub HandleBindingCompleteEvent(ByVal sender As Binding, ByVal e As BindingCompleteEventArgs)
        Dim activity As String = String.Empty
        Dim binding As Binding = TryCast(sender, Binding)
        If binding Is Nothing OrElse e Is Nothing Then Return
        Try
            activity = "setting cancel state"
            e.Cancel = e.BindingCompleteState <> BindingCompleteState.Success
            activity = $"binding: {e.Binding.BindingMemberInfo.BindingField}:{e.BindingCompleteContext}:{e.Binding.BindableComponent}"
            If e.BindingCompleteState = BindingCompleteState.DataError Then
                activity = $"data error; {activity}"
                Me.PublishWarning($"{activity};. {e.ErrorText}")
            ElseIf e.BindingCompleteState = BindingCompleteState.Exception Then
                If Not String.IsNullOrWhiteSpace(e.ErrorText) Then
                    activity = $"{activity}; {e.ErrorText}"
                End If
                Me.PublishException(activity, e.Exception)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Handles the binding complete event. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                       <see cref="Control"/> </param>
    ''' <param name="e">      Binding complete event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub HandleBindingCompleteEvent(ByVal sender As Object, ByVal e As BindingCompleteEventArgs)
        Dim activity As String = String.Empty
        Dim binding As Binding = TryCast(sender, Binding)
        If binding Is Nothing OrElse e Is Nothing Then Return
        Try
            If e.BindingCompleteState <> BindingCompleteState.Success Then
                Me.HandleBindingCompleteEvent(binding, e)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Adds or removes binding from a <see cref="IBindableComponent">bindable componenet</see>
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="component"> The bindable control. </param>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="binding">   The binding. </param>
    ''' <returns> A Binding. </returns>
    Public Function AddRemoveBinding(ByVal component As IBindableComponent, ByVal add As Boolean, ByVal binding As Binding) As Binding
        component.AddRemoveBinding(add, binding, AddressOf Me.HandleBindingCompleteEvent)
        Return binding
    End Function

    ''' <summary> Adds binding to a <see cref="IBindableComponent">bindable componenet</see> </summary>
    ''' <param name="component">    The bindable control. </param>
    ''' <param name="add">          True to add; otherwise, remove. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    ''' <param name="dataSource">   The data source. </param>
    ''' <param name="dataMember">   The data member. </param>
    ''' <returns> A Binding. </returns>
    Public Function AddRemoveBinding(ByVal component As IBindableComponent, ByVal add As Boolean, ByVal propertyName As String,
                                     ByVal dataSource As Object, ByVal dataMember As String) As Binding
        Return Me.AddRemoveBinding(component, add, New Binding(propertyName, dataSource, dataMember, True))
    End Function

    ''' <summary> Adds binding to a <see cref="IBindableComponent">bindable componenet</see> </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="component">    The bindable control. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    ''' <param name="dataSource">   The data source. </param>
    ''' <param name="dataMember">   The data member. </param>
    ''' <returns> A Binding. </returns>
    Public Function AddBinding(ByVal component As IBindableComponent, ByVal propertyName As String, ByVal dataSource As Object, ByVal dataMember As String) As Binding
        Return Me.AddRemoveBinding(component, True, New Binding(propertyName, dataSource, dataMember, True))
    End Function

    ''' <summary>
    ''' removes binding from a <see cref="IBindableComponent">bindable componenet</see>
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="component">    The bindable component. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    ''' <param name="dataSource">   The data source. </param>
    ''' <param name="dataMember">   The data member. </param>
    ''' <returns> A Binding. </returns>
    Public Function RemoveBinding(ByVal component As IBindableComponent, ByVal propertyName As String, ByVal dataSource As Object, ByVal dataMember As String) As Binding
        Return Me.AddRemoveBinding(component, False, New Binding(propertyName, dataSource, dataMember, True))
    End Function

#End Region

#Region " VIEW MODEL "

    ''' <summary> Gets or sets the view model. </summary>
    ''' <value> The view model. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ViewModel As ResourceConnectorViewModel

    ''' <summary> Assign view model. </summary>
    ''' <param name="viewModel"> The view model. </param>
    Public Sub AssignViewModel(ByVal viewModel As ResourceConnectorViewModel)
        If Me.ViewModel IsNot Nothing Then
            Me.BindClearButton(False, viewModel)
            Me.BindConnectButton(False, viewModel)
            Me.BindFindButton(False, viewModel)
            Me.BindResourceNamesCombo(False, viewModel)
            Me._ViewModel = Nothing
        End If
        If viewModel IsNot Nothing Then
            Me._ViewModel = viewModel
            viewModel.ConnectedImage = My.Resources.Connected_22x22
            viewModel.DisconnectedImage = My.Resources.Disconnected_22x22
            viewModel.SearchImage = My.Resources.Find_22x22
            viewModel.ClearImage = My.Resources.Clear_22x22
            Me.BindClearButton(True, viewModel)
            Me.BindConnectButton(True, viewModel)
            Me.BindFindButton(True, viewModel)
            Me.BindResourceNamesCombo(True, viewModel)
        End If
    End Sub

    ''' <summary> Binds the Clear button. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="viewModel"> The view model. </param>
    Private Sub BindClearButton(ByVal add As Boolean, ByVal viewModel As ResourceConnectorViewModel)
        With Me.AddRemoveBinding(Me._ClearButton, add, NameOf(ToolStripButton.Enabled), viewModel, NameOf(ResourceConnectorViewModel.IsConnected))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
        With Me.AddRemoveBinding(Me._ClearButton, add, NameOf(ToolStripButton.Visible), viewModel, NameOf(ResourceConnectorViewModel.Clearable))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
        With Me.AddRemoveBinding(Me._ClearButton, add, NameOf(ToolStripButton.ToolTipText), viewModel, NameOf(ResourceConnectorViewModel.ClearToolTip))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
    End Sub

    Private Sub BindConnectButton(ByVal add As Boolean, ByVal viewModel As ResourceConnectorViewModel)
        With Me.AddRemoveBinding(Me._ToggleConnectionButton, add, NameOf(ToolStripButton.Enabled), viewModel, NameOf(ResourceConnectorViewModel.ConnectEnabled))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
        With Me.AddRemoveBinding(Me._ToggleConnectionButton, add, NameOf(ToolStripButton.Visible), viewModel, NameOf(ResourceConnectorViewModel.Connectable))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
        With Me.AddRemoveBinding(Me._ToggleConnectionButton, add, NameOf(ToolStripButton.ToolTipText), viewModel, NameOf(ResourceConnectorViewModel.ConnectToolTip))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
        With Me.AddRemoveBinding(Me._ToggleConnectionButton, add, NameOf(ToolStripButton.Image), viewModel, NameOf(ResourceConnectorViewModel.ConnectImage))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
    End Sub

    Private Sub BindFindButton(ByVal add As Boolean, ByVal viewModel As ResourceConnectorViewModel)
        With Me.AddRemoveBinding(Me._EnumerateButton, add, NameOf(ToolStripButton.Enabled), viewModel, NameOf(ResourceConnectorViewModel.IsDisconnected))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
        With Me.AddRemoveBinding(Me._EnumerateButton, add, NameOf(ToolStripButton.Visible), viewModel, NameOf(ResourceConnectorViewModel.Searchable))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
        With Me.AddRemoveBinding(Me._EnumerateButton, add, NameOf(ToolStripButton.ToolTipText), viewModel, NameOf(ResourceConnectorViewModel.SearchToolTip))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
    End Sub

    Private Sub BindResourceNamesCombo(ByVal add As Boolean, ByVal viewModel As ResourceConnectorViewModel)
        With Me.AddRemoveBinding(Me._ResourceNamesComboBox.ComboBox, add, NameOf(ComboBox.Enabled), viewModel, NameOf(ResourceConnectorViewModel.IsDisconnected))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
        If add Then
            Me._ResourceNamesComboBox.ComboBox.DataSource = viewModel.ResourceNames ' viewModel.ResourceNameBindingSource
        Else
            Me._ResourceNamesComboBox.ComboBox.DataSource = Nothing
        End If
        Me.AddRemoveBinding(Me._ResourceNamesComboBox.ComboBox, add, NameOf(ComboBox.Text), viewModel, NameOf(ResourceConnectorViewModel.CandidateResourceName))
    End Sub

    ''' <summary> Handles the view model property changed event. </summary>
    ''' <param name="sender">    The sender. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")>
    <CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
    Private Sub HandlePropertyChange(ByVal sender As ResourceConnectorViewModel, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(ResourceConnectorViewModel.IsConnected)
                ' turning off visibility is required -- otherwise, control overflows and both search and toggle disappear 
                Me._ToggleConnectionButton.Visible = False
                Me._ToggleConnectionButton.Image = sender.ConnectImage
                Me._ToggleConnectionButton.Visible = sender.Connectable
                Me._ToggleConnectionButton.Enabled = sender.Connectable
                Me._ToolStrip.Invalidate()
        End Select
    End Sub

    ''' <summary> View Model property changed handler </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub ViewModelPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {GetType(ResourceConnectorViewModel)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.ViewModelPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChange(TryCast(sender, ResourceConnectorViewModel), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " BROWSABLE PROPERTIES "

    ''' <summary> Gets or sets the value indicating if the clear button is visible and can be enabled.
    ''' An item can be cleared only if it is connected. </summary>
    ''' <value> The clearable. </value>
    <Category("Appearance"), Description("Shows or hides the Clear button"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(True)>
    Public Property Clearable() As Boolean
        Get
            Return Me._ClearButton.Visible
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Clearable.Equals(value) Then
                Me._ClearButton.Visible = value
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the value indicating if the connect button is visible and can be
    ''' enabled. An item can be connected only if it is selected. </summary>
    ''' <value> The connectable. </value>
    <Category("Appearance"), Description("Shows or hides the Connect button"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
    RefreshProperties(RefreshProperties.All), DefaultValue(True)>
    Public Property Connectable() As Boolean
        Get
            Return Me._ToggleConnectionButton.Visible
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Connectable.Equals(value) Then
                Me._ToggleConnectionButton.Visible = value
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the condition determining if the control can be searchable. The elements
    ''' can be searched only if not connected. </summary>
    ''' <value> The searchable. </value>
    <Category("Appearance"), Description("Shows or hides the Search (Find) button"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(True)>
    Public Property Searchable() As Boolean
        Get
            Return Me._EnumerateButton.Visible
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Searchable.Equals(value) Then
                Me._EnumerateButton.Visible = value
            End If
        End Set
    End Property

#End Region

#Region " CONNECTION MANAGEMENT "

    ''' <summary> Executes the toggle connection action. </summary>
    ''' <param name="sender"> Source of the event. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="OK")>
    Private Sub OnToggleConnection(ByVal sender As ToolStripItem)
        If sender Is Nothing Then Return
        Dim activity As String = ""
        Try
            Me.Cursor = Cursors.WaitCursor
            Me._ErrorProvider.Clear()
            activity = $"Toggling connection '{Me.ViewModel.CandidateResourceName}'"
            Me.ViewModel.ToggleConnection()
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.Message)
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Toggle connection button click. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                       <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _ToggleConnectionButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ToggleConnectionButton.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.OnToggleConnection(TryCast(sender, ToolStripItem))
    End Sub

#End Region

#Region " RESOURCE SELECTION "

    ''' <summary> Attempts to validate the specified resource name </summary>
    ''' <param name="resourceName"> The value. </param>
    ''' <param name="e">     Cancel details event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="OK")>
    Private Function TryValidateResourceName(ByVal resourceName As String, ByVal e As isr.Core.Pith.ActionEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If String.IsNullOrWhiteSpace(resourceName) Then resourceName = ""
        Dim activity As String = ""
        Try
            Me._ErrorProvider.Clear()
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            activity = $"validating {resourceName}" : Me.PublishInfo($"{activity};. ")
            If Me.ViewModel.TryValidateResource(resourceName, e) Then
                If e.HasOutcomeEvent AndAlso e.HasDetails Then
                    activity = Me.Publish(e.OutcomeEvent, $"{activity};. reported {e.Details}")
                End If
            Else
                Me._ErrorProvider.Annunciate(Me._ResourceNamesComboBox, e.Details)
                activity = Me.PublishWarning($"Failed {activity};. {e.Details}")
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(Me._ResourceNamesComboBox, ex.Message)
            Dim message As New TraceMessage(TraceEventType.Error, My.MyLibrary.TraceEventId, $"Exception {activity};. {ex.ToFullBlownString}")
            activity = Me.Talker?.Publish(message)
            e.RegisterError(activity)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
        Return Not e.Failed
    End Function

    ''' <summary> Event handler called by the validated and selected index changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HandleResourceNamesComboBoxValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim control As ToolStripSpringComboBox = _ResourceNamesComboBox ' TryCast(sender, ToolStripSpringComboBox)
        If Not (control Is Nothing OrElse String.IsNullOrWhiteSpace(control.Text)) Then
            Me.TryValidateResourceName(control.Text, New isr.Core.Pith.ActionEventArgs)
        End If
    End Sub

#End Region

#Region " ENUMERATE RESOURCES HANDLER "

    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="OK")>
    Private Sub _EnumerateButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _EnumerateButton.Click
        Dim activity As String = String.Empty
        Try
            Me._ErrorProvider.Clear()
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            activity = "Enumerating resources" : Me.PublishInfo($"{activity};. ")
            Me.ViewModel.EnumerateResources()
        Catch ex As Exception
            Dim c As ToolStripItem = TryCast(sender, ToolStripItem)
            If c IsNot Nothing Then
                Me._ErrorProvider.Annunciate(c, ex.Message)
            End If
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

#End Region

#Region " CLEAR "

    ''' <summary> Requests a clear </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="OK")>
    Private Sub _ClearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ClearButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._ErrorProvider.Clear()
            activity = $"checking the clear status"
            If Me.ViewModel.IsConnected Then
                activity = $"clearing {Me.ViewModel.ValidatedResourceName}"
                Me.ViewModel.RequestClear()
            Else
                Me.PublishWarning($"resource '{Me.ViewModel.CandidateResourceName}' not validated or validation failed")
            End If
        Catch ex As Exception
            Dim c As ToolStripItem = TryCast(sender, ToolStripItem)
            If c IsNot Nothing Then
                Me._ErrorProvider.Annunciate(c, ex.Message)
            End If
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Resource names combo box double click. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                       <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _ResourceNamesComboBox_DoubleClick(sender As Object, e As EventArgs) Handles _ResourceNamesComboBox.DoubleClick
        Static connected As Boolean
        Me._ToggleConnectionButton.Visible = False
        Me._ToggleConnectionButton.Image = If(connected, My.Resources.Connected_22x22, My.Resources.Disconnected_22x22)
        Me._ToggleConnectionButton.Visible = True
        connected = False
        Me._ToolStrip.Refresh()
    End Sub

#End Region

#Region " UNIT TESTS INTERNALS "

    ''' <summary> Gets the number of internal resource names. </summary>
    ''' <value> The number of internal resource names. </value>
    Friend ReadOnly Property InternalResourceNamesCount As Integer
        Get
            If Me._ResourceNamesComboBox.ComboBox Is Nothing Then
                Return 0
            Else
                Return Me._ResourceNamesComboBox.Items.Count
            End If
        End Get
    End Property

    ''' <summary> Gets the name of the internal selected resource. </summary>
    ''' <value> The name of the internal selected resource. </value>
    Friend ReadOnly Property InternalSelectedResourceName As String
        Get
            If Me._ResourceNamesComboBox.ComboBox Is Nothing Then
                Return String.Empty
            Else
                Return Me._ResourceNamesComboBox.Text
            End If
        End Get
    End Property

#End Region

End Class

Imports System.ComponentModel
Imports isr.Core.Pith
Imports isr.Core.Pith.ErrorProviderExtensions
Imports isr.Core.Pith.ExceptionExtensions
''' <summary> A control for selecting and opening a resource defined by a resource name </summary>
''' <license> (c) 2006 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/20/2006" by="David" revision="1.0.2242.x"> created. </history>
''' <history date="12/11/2018" by="David" revision="3.4.6819.x"> created from VI resource selector connector. </history>
<Description("Resource Selector and Opener Base Control")>
<System.Drawing.ToolboxBitmap(GetType(SelectorOpener), "SelectorOpener"), ToolboxItem(True)>
Public Class SelectorOpener
    Inherits isr.Core.Pith.TalkerControlBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
        Me._ClearButton.Visible = True
        Me._ToggleOpenButton.Visible = True
        Me._EnumerateButton.Visible = True
        Me._ToggleOpenButton.Enabled = False
        Me._ClearButton.Enabled = False
        Me._EnumerateButton.Enabled = True
        AddHandler Me._ResourceNamesComboBox.ComboBox.SelectedValueChanged, AddressOf HandleResourceNamesComboBoxValueChanged
    End Sub

    ''' <summary> Creates a new <see cref="SelectorOpener"/> </summary>
    ''' <returns> A <see cref="SelectorOpener"/>. </returns>
    Public Shared Function Create() As SelectorOpener
        Dim selectorOpener As SelectorOpener = Nothing
        Try
            selectorOpener = New SelectorOpener
            Return selectorOpener
        Catch
            selectorOpener.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                          <c>False</c> to release only unmanaged resources when called from the
    '''                          runtime finalize. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                Me.AssignSelectorViewModel(Nothing)
                Me.AssignOpenerViewModel(Nothing)
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " BROWSABLE PROPERTIES "

    ''' <summary> Gets or sets the value indicating if the clear button is visible and can be enabled.
    ''' An item can be cleared only if it is open </summary>
    ''' <value> The clearable. </value>
    <Category("Appearance"), Description("Shows or hides the Clear button"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(True)>
    Public Property Clearable() As Boolean
        Get
            Return Me._ClearButton.Visible
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Clearable.Equals(value) Then
                Me._ClearButton.Visible = value
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the value indicating if the open button is visible and can be
    ''' enabled. An item can be opened only if it is validated </summary>
    ''' <value> The openable state </value>
    <Category("Appearance"), Description("Shows or hides the open button"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
    RefreshProperties(RefreshProperties.All), DefaultValue(True)>
    Public Property Openable() As Boolean
        Get
            Return Me._ToggleOpenButton.Visible
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Openable.Equals(value) Then
                Me._ToggleOpenButton.Visible = value
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the condition determining if the control can be searchable. The elements
    ''' can be searched only if not open </summary>
    ''' <value> The searchable. </value>
    <Category("Appearance"), Description("Shows or hides the Search (Find) button"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(True)>
    Public Property Searchable() As Boolean
        Get
            Return Me._EnumerateButton.Visible
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Searchable.Equals(value) Then
                Me._EnumerateButton.Visible = value
            End If
        End Set
    End Property

#End Region

#Region " SELECTOR VIEW MODEL "

    ''' <summary> Gets or sets the selector view model. </summary>
    ''' <value> The selector view model. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SelectorViewModel As SelectorViewModel

    ''' <summary> Assign the selector view model. </summary>
    ''' <param name="viewModel"> The view model. </param>
    Public Sub AssignSelectorViewModel(ByVal viewModel As SelectorViewModel)
        If Me.SelectorViewModel IsNot Nothing Then
            Me.BindEnumerateButton(False, Me.SelectorViewModel)
            Me.BindResourceNamesCombo(False, Me.SelectorViewModel)
            Me._SelectorViewModel = Nothing
        End If
        If viewModel IsNot Nothing Then
            Me._SelectorViewModel = viewModel
            Me.SelectorViewModel.SearchImage = My.Resources.Find_22x22
            Me.BindEnumerateButton(True, Me.SelectorViewModel)
            Me.BindResourceNamesCombo(True, Me.SelectorViewModel)
        End If
    End Sub

    ''' <summary> Bind enumerate button. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="viewModel"> The view model. </param>
    Private Sub BindEnumerateButton(ByVal add As Boolean, ByVal viewModel As SelectorViewModel)
        With Me.AddRemoveBinding(Me._EnumerateButton, add, NameOf(ToolStripButton.Visible), viewModel, NameOf(isr.Core.Pith.SelectorViewModel.Searchable))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
        With Me.AddRemoveBinding(Me._EnumerateButton, add, NameOf(ToolStripButton.Enabled), viewModel, NameOf(isr.Core.Pith.SelectorViewModel.SearchEnabled))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
        With Me.AddRemoveBinding(Me._EnumerateButton, add, NameOf(ToolStripButton.ToolTipText), viewModel, NameOf(isr.Core.Pith.SelectorViewModel.SearchToolTip))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
        With Me.AddRemoveBinding(Me._EnumerateButton, add, NameOf(ToolStripButton.Image), viewModel, NameOf(isr.Core.Pith.SelectorViewModel.SearchImage))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
    End Sub

    ''' <summary> Bind resource names combo. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="viewModel"> The view model. </param>
    Private Sub BindResourceNamesCombo(ByVal add As Boolean, ByVal viewModel As SelectorViewModel)
        With Me.AddRemoveBinding(Me._ResourceNamesComboBox.ComboBox, add, NameOf(ComboBox.Enabled), viewModel, NameOf(isr.Core.Pith.SelectorViewModel.SearchEnabled))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
        If add Then
            Me._ResourceNamesComboBox.ComboBox.DataSource = viewModel.ResourceNames
        Else
            Me._ResourceNamesComboBox.ComboBox.DataSource = Nothing
        End If
        Me.AddRemoveBinding(Me._ResourceNamesComboBox.ComboBox, add, NameOf(ComboBox.Text), viewModel, NameOf(isr.Core.Pith.SelectorViewModel.CandidateResourceName))
    End Sub

    ''' <summary> Attempts to validate the specified resource name </summary>
    ''' <param name="resourceName"> The value. </param>
    ''' <param name="e">     Cancel details event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="OK")>
    Private Function TryValidateResourceName(ByVal resourceName As String, ByVal e As isr.Core.Pith.ActionEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If String.IsNullOrWhiteSpace(resourceName) Then resourceName = ""
        Dim activity As String = String.Empty
        Dim sender As ToolStripSpringComboBox = Me._ResourceNamesComboBox
        Try
            Me._ErrorProvider.Clear()
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            activity = $"validating {resourceName}" : Me.PublishInfo($"{activity};. ")
            If Me.SelectorViewModel.TryValidateResource(resourceName, e) Then
                If e.HasOutcomeEvent AndAlso e.HasDetails Then
                    activity = Me.Publish(e.OutcomeEvent, $"{activity};. reported {e.Details}")
                End If
                ' this enables opening 
                If Me.OpenerViewModel IsNot Nothing Then Me.OpenerViewModel.ValidatedResourceName = Me.SelectorViewModel.ValidatedResourceName
            Else
                Me._ErrorProvider.Annunciate(sender, e.Details)
                activity = Me.PublishWarning($"Failed {activity};. {e.Details}") : Me.PublishWarning($"{activity};. ")
                ' this disables opening 
                If Me.OpenerViewModel IsNot Nothing Then Me.OpenerViewModel.ValidatedResourceName = String.Empty
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.Message)
            Dim message As New TraceMessage(TraceEventType.Error, My.MyLibrary.TraceEventId, $"Exception {activity};. {ex.ToFullBlownString}")
            activity = Me.Talker?.Publish(message)
            e.RegisterError(activity)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
        Return Not e.Failed
    End Function

    ''' <summary> Gets the number of selected value changes. </summary>
    ''' <value> The number of selected value changes. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SelectedValueChangeCount As Integer

    ''' <summary> Event handler called by the validated and selected index changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HandleResourceNamesComboBoxValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim control As ToolStripSpringComboBox = Me._ResourceNamesComboBox ' TryCast(sender, ToolStripSpringComboBox)
        If Not (control Is Nothing OrElse String.IsNullOrWhiteSpace(control.Text)) Then
            Me.SelectedValueChangeCount += 1
            Me.TryValidateResourceName(control.Text, New isr.Core.Pith.ActionEventArgs)
        End If
    End Sub

    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="OK")>
    Private Sub _EnumerateButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _EnumerateButton.Click
        Dim activity As String = String.Empty
        Try
            Me._ErrorProvider.Clear()
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            activity = "Enumerating resources" : Me.PublishInfo($"{activity};. ")
            Me.SelectorViewModel.EnumerateResources()
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.Message)
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

#End Region

#Region " OPENER VIEW MODEL "

    ''' <summary> Gets or sets the opener view model. </summary>
    ''' <value> The opener view model. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property OpenerViewModel As OpenerViewModel

    ''' <summary> Assigns the opener view model. </summary>
    ''' <param name="viewModel "> The view model. </param>
    Public Sub AssignOpenerViewModel(ByVal viewModel As OpenerViewModel)
        If Me.OpenerViewModel IsNot Nothing Then
            Me.BindClearButton(False, Me.OpenerViewModel)
            Me.BindOpenButton(False, Me.OpenerViewModel)
            Me._OpenerViewModel = Nothing
        End If
        If viewModel IsNot Nothing Then
            Me._OpenerViewModel = viewModel
            Me.OpenerViewModel.OpenedImage = My.Resources.Connected_22x22
            Me.OpenerViewModel.ClosedImage = My.Resources.Disconnected_22x22
            Me.OpenerViewModel.ClearImage = My.Resources.Clear_22x22
            Me.BindClearButton(True, Me.OpenerViewModel)
            Me.BindOpenButton(True, Me.OpenerViewModel)
        End If
    End Sub

#If False Then

    Public Sub AssignOpenerViewModelHandlers(ByVal viewModel As OpenerViewModel)
        If Me.OpenerViewModel IsNot Nothing Then
            RemoveHandler Me.OpenerViewModel.PropertyChanged, AddressOf OpenerViewModelPropertyChanged
        End If
        If viewModel IsNot Nothing Then
            AddHandler Me.OpenerViewModel.PropertyChanged, AddressOf OpenerViewModelPropertyChanged
        End If
    End Sub

    ''' <summary> Handles the subsystem property change. </summary>
    ''' <param name="viewModel">    The view model. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChange(ByVal viewModel As OpenerViewModel, ByVal propertyName As String)
        If viewModel Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(isr.Core.Pith.OpenerViewModel.OpenEnabled)
                Me._ToggleOpenButton.Enabled = viewModel.OpenEnabled
        End Select
    End Sub

    ''' <summary> Status subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overloads Sub OpenerViewModelPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(isr.Core.Pith.OpenerViewModel)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChange(TryCast(sender, isr.Core.Pith.OpenerViewModel), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub
#End If

    ''' <summary> Binds the Clear button. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="viewModel"> The view model. </param>
    Private Sub BindClearButton(ByVal add As Boolean, ByVal viewModel As OpenerViewModel)
        With Me.AddRemoveBinding(Me._ClearButton, add, NameOf(ToolStripButton.Enabled), viewModel, NameOf(isr.Core.Pith.OpenerViewModel.IsOpen))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
        With Me.AddRemoveBinding(Me._ClearButton, add, NameOf(ToolStripButton.Visible), viewModel, NameOf(isr.Core.Pith.OpenerViewModel.Clearable))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
        With Me.AddRemoveBinding(Me._ClearButton, add, NameOf(ToolStripButton.ToolTipText), viewModel, NameOf(isr.Core.Pith.OpenerViewModel.ClearToolTip))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
    End Sub

    ''' <summary> Bind open button. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="viewModel"> The view model. </param>
    Private Sub BindOpenButton(ByVal add As Boolean, ByVal viewModel As OpenerViewModel)
        With Me.AddRemoveBinding(Me._ToggleOpenButton, add, NameOf(ToolStripButton.Visible), viewModel, NameOf(isr.Core.Pith.OpenerViewModel.Openable))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
        With Me.AddRemoveBinding(Me._ToggleOpenButton, add, NameOf(ToolStripButton.Enabled), viewModel, NameOf(isr.Core.Pith.OpenerViewModel.OpenEnabled))
            .DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged
            .ControlUpdateMode = ControlUpdateMode.OnPropertyChanged
        End With
        With Me.AddRemoveBinding(Me._ToggleOpenButton, add, NameOf(ToolStripButton.ToolTipText), viewModel, NameOf(isr.Core.Pith.OpenerViewModel.OpenToolTip))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
        With Me.AddRemoveBinding(Me._ToggleOpenButton, add, NameOf(ToolStripButton.Image), viewModel, NameOf(isr.Core.Pith.OpenerViewModel.OpenImage))
            .DataSourceUpdateMode = DataSourceUpdateMode.Never
        End With
    End Sub

    ''' <summary> Executes the toggle open  action. </summary>
    ''' <param name="sender"> Source of the event. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="OK")>
    Private Sub ToggleOpen(ByVal sender As ToolStripItem)
        If sender Is Nothing Then Return
        Dim activity As String = ""
        Try
            Me.Cursor = Cursors.WaitCursor
            Me._ErrorProvider.Clear()
            Dim e As New isr.Core.Pith.ActionEventArgs
            If Me.OpenerViewModel.IsOpen Then
                activity = $"closing {Me.OpenerViewModel.ResourceNameCaption}" : Me.PublishInfo($"{activity};. ")
                Me.OpenerViewModel.TryClose(e)
            Else
                activity = $"opening {Me.OpenerViewModel.ResourceTitleCaption}" : Me.PublishInfo($"{activity};. ")
                Me.OpenerViewModel.TryOpen(e)
            End If
            If e.Failed Then
                Me._ErrorProvider.Annunciate(sender, Me.Publish(e))
            Else
                ' this ensures that the control refreshes.
                Me._OverflowLabel.Text = "."
                Windows.Forms.Application.DoEvents()
                Me._OverflowLabel.Text = String.Empty
                Windows.Forms.Application.DoEvents()
                Me.PublishInfo($"Done {activity};. ")
            End If
            ' this disables search when the resource is open
            If Me.SelectorViewModel IsNot Nothing Then Me.SelectorViewModel.IsOpen = Me.OpenerViewModel.IsOpen
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.Message)
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Toggle open button click. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                       <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _ToggleOpenButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ToggleOpenButton.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ToggleOpen(TryCast(sender, ToolStripItem))
    End Sub

    ''' <summary> Requests a clear </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="OK")>
    Private Sub _ClearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ClearButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._ErrorProvider.Clear()
            activity = $"checking {Me.OpenerViewModel.ResourceNameCaption} open status"
            If Me.OpenerViewModel.IsOpen Then
                Dim args As New isr.Core.Pith.ActionEventArgs
                activity = $"clearing {Me.OpenerViewModel.ResourceNameCaption}"
                If Not Me.OpenerViewModel.TryClearActiveState(args) Then
                    Me._ErrorProvider.Annunciate(sender, Me.Publish(args))
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, Me.PublishWarning($"failed {activity}; not open"))
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.Message)
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    Private Sub _ToolStrip_DoubleClick(sender As Object, e As EventArgs) Handles _ToolStrip.DoubleClick, _ResourceNamesComboBox.DoubleClick
        ' this ensures that the control refreshes.
        Me._OverflowLabel.Text = "."
        Windows.Forms.Application.DoEvents()
        Me._OverflowLabel.Text = String.Empty
        Windows.Forms.Application.DoEvents()
    End Sub

#End Region

#Region " UNIT TESTS INTERNALS "

    ''' <summary> Gets the number of internal resource names. </summary>
    ''' <value> The number of internal resource names. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Friend ReadOnly Property InternalResourceNamesCount As Integer
        Get
            If Me._ResourceNamesComboBox.ComboBox Is Nothing Then
                Return 0
            Else
                Return Me._ResourceNamesComboBox.Items.Count
            End If
        End Get
    End Property

    ''' <summary> Gets the name of the internal selected resource. </summary>
    ''' <value> The name of the internal selected resource. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Friend ReadOnly Property InternalSelectedResourceName As String
        Get
            If Me._ResourceNamesComboBox.ComboBox Is Nothing Then
                Return String.Empty
            Else
                Return Me._ResourceNamesComboBox.Text
            End If
        End Get
    End Property

#End Region

End Class

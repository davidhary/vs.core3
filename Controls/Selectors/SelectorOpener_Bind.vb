﻿Imports isr.Core.Pith.BindingExtensions

Partial Public Class SelectorOpener

#Region " BINDING MANAGEMENT "

    ''' <summary> Handles the binding complete event. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                       <see cref="Control"/> </param>
    ''' <param name="e">      Binding complete event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub HandleBindingCompleteEvent(ByVal sender As Binding, ByVal e As BindingCompleteEventArgs)
        Dim activity As String = String.Empty
        Dim binding As Binding = TryCast(sender, Binding)
        If binding Is Nothing OrElse e Is Nothing Then Return
        Try
            activity = "setting cancel state"
            e.Cancel = e.BindingCompleteState <> BindingCompleteState.Success
            activity = $"binding: {e.Binding.BindingMemberInfo.BindingField}:{e.BindingCompleteContext}:{e.Binding.BindableComponent}"
            If e.BindingCompleteState = BindingCompleteState.DataError Then
                activity = $"data error; {activity}"
                Me.PublishWarning($"{activity};. {e.ErrorText}")
            ElseIf e.BindingCompleteState = BindingCompleteState.Exception Then
                If Not String.IsNullOrWhiteSpace(e.ErrorText) Then
                    activity = $"{activity}; {e.ErrorText}"
                End If
                Me.PublishException(activity, e.Exception)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Handles the binding complete event. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                       <see cref="Control"/> </param>
    ''' <param name="e">      Binding complete event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub HandleBindingCompleteEvent(ByVal sender As Object, ByVal e As BindingCompleteEventArgs)
        Dim activity As String = String.Empty
        Dim binding As Binding = TryCast(sender, Binding)
        If binding Is Nothing OrElse e Is Nothing Then Return
        Try
            If e.BindingCompleteState <> BindingCompleteState.Success Then
                Me.HandleBindingCompleteEvent(binding, e)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Adds or removes binding from a <see cref="IBindableComponent">bindable componenet</see>
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="component"> The bindable control. </param>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="binding">   The binding. </param>
    ''' <returns> A Binding. </returns>
    Public Function AddRemoveBinding(ByVal component As IBindableComponent, ByVal add As Boolean, ByVal binding As Binding) As Binding
        component.AddRemoveBinding(add, binding, AddressOf Me.HandleBindingCompleteEvent)
        Return binding
    End Function

    ''' <summary> Adds binding to a <see cref="IBindableComponent">bindable componenet</see> </summary>
    ''' <param name="component">    The bindable control. </param>
    ''' <param name="add">          True to add; otherwise, remove. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    ''' <param name="dataSource">   The data source. </param>
    ''' <param name="dataMember">   The data member. </param>
    ''' <returns> A Binding. </returns>
    Public Function AddRemoveBinding(ByVal component As IBindableComponent, ByVal add As Boolean, ByVal propertyName As String,
                                     ByVal dataSource As Object, ByVal dataMember As String) As Binding
        ' must set formatting enabled to true to enable the binding complete event.
        Return Me.AddRemoveBinding(component, add, New Binding(propertyName, dataSource, dataMember, True))
    End Function

    ''' <summary> Adds binding to a <see cref="IBindableComponent">bindable componenet</see> </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="component">    The bindable control. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    ''' <param name="dataSource">   The data source. </param>
    ''' <param name="dataMember">   The data member. </param>
    ''' <returns> A Binding. </returns>
    Public Function AddBinding(ByVal component As IBindableComponent, ByVal propertyName As String, ByVal dataSource As Object, ByVal dataMember As String) As Binding
        ' must set formatting enabled to true to enable the binding complete event.
        Return Me.AddRemoveBinding(component, True, New Binding(propertyName, dataSource, dataMember, True))
    End Function

    ''' <summary>
    ''' removes binding from a <see cref="IBindableComponent">bindable componenet</see>
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="component">    The bindable component. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    ''' <param name="dataSource">   The data source. </param>
    ''' <param name="dataMember">   The data member. </param>
    ''' <returns> A Binding. </returns>
    Public Function RemoveBinding(ByVal component As IBindableComponent, ByVal propertyName As String, ByVal dataSource As Object, ByVal dataMember As String) As Binding
        ' must set formatting enabled to true to enable the binding complete event.
        Return Me.AddRemoveBinding(component, False, New Binding(propertyName, dataSource, dataMember, True))
    End Function

#End Region

End Class

Imports System.ComponentModel
Imports isr.Core.Pith.CompactExtensions
Imports isr.Core.Pith.EventHandlerExtensions
Imports isr.Core.Pith.ExceptionExtensions
Imports isr.Core.Pith.FileInfoExtensions
''' <summary> A simple text box and browse button file selector. </summary>
''' <license> (c) 2007 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/14/2007" by="David" revision="1.2.3405.x">  Add default event and
''' description and move bitmap setting from the designer file. </history>
''' <history date="03/14/2007" by="David" revision="1.00.2619.x"> created. </history>
<Description("File Selector"), DefaultEvent("SelectedChanged")>
Public Class FileSelector
    Inherits Pith.PropertyNotifyControlBase

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
        Me._textBoxContents = ""
        Me._filePath = ""
        Me.DefaultExtension = ".TXT"
        Me.DialogFilter = "Text files (*.txt; *.lst)|*.txt;*.lst|Batch files (*.bat)|*.bat|All Files (*.*)|*.*"
        Me.DialogTitle = "SELECT A TEXT FILE"
        Me._extensionName = ""
        Me._fileName = ""
        Me._fileTitle = ""
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Pith.PropertyNotifyControlBase and
    ''' optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveSelectedChangedEventHandler(Me.SelectedChangedEvent)
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FILE NAME PROPERTIES "

    Private _DefaultExtension As String

    ''' <summary> Gets or sets the default extension. </summary>
    ''' <value> The default extension. </value>
    <Category("Appearance"), Description("Default extension"), Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(".TXT")>
    Public Property DefaultExtension() As String
        Get
            Return Me._defaultExtension
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = ""
            If Not String.Equals(value, Me.DefaultExtension, StringComparison.OrdinalIgnoreCase) Then
                Me._DefaultExtension = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    Private _DialogFilter As String

    ''' <summary> Gets or sets the filename filter string, which determines the choices of files that
    ''' appear in the file type selection drop down list. </summary>
    ''' <value> The dialog filter. </value>
    <Category("Appearance"), Description("Filename filter string determining the choices of files that appear in the file type selection drop down list."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue("Text files (*.txt; *.lst)|*.txt;*.lst|Batch files (*.bat)|*.bat|All Files (*.*)|*.*")>
    Public Property DialogFilter() As String
        Get
            Return Me._dialogFilter
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = ""
            If Not String.Equals(value, Me.DialogFilter, StringComparison.OrdinalIgnoreCase) Then
                Me._DialogFilter = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    Private _DialogTitle As String

    ''' <summary> Gets or sets the dialog title. </summary>
    ''' <value> The dialog title. </value>
    <Category("Appearance"), Description("Dialog caption"),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue("SELECT A TEXT FILE")>
    Public Property DialogTitle() As String
        Get
            Return Me._dialogTitle
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = ""
            If Not String.Equals(value, Me.DialogTitle, StringComparison.OrdinalIgnoreCase) Then
                Me._DialogTitle = value
                Me.SafePostPropertyChanged()
            End If
        End Set
    End Property

    Private _ExtensionName As String

    ''' <summary> Gets the file extension. </summary>
    ''' <value> The name of the extension. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ExtensionName() As String
        Get
            Return Me._extensionName
        End Get
    End Property

    Private _FileName As String

    ''' <summary> Gets the file name. </summary>
    ''' <value> The name of the file. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property FileName() As String
        Get
            Return Me._fileName
        End Get
    End Property

    Private _TextBoxContents As String
    Private _FilePath As String

    ''' <summary> Gets or sets a new file path. </summary>
    ''' <value> The full pathname of the file. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property FilePath() As String
        Get
            Return Me._filePath
        End Get
        Set(ByVal Value As String)
            If String.IsNullOrWhiteSpace(Value) Then Value = ""
            Value = Value.Trim
            If Me._filePath Is Nothing Then Me._filePath = ""
            If Not (Me._filePath.Equals(Value, comparisonType:=StringComparison.OrdinalIgnoreCase) AndAlso
                    Me._textBoxContents.Equals(Value.Compact(Me._FilePathTextBox), comparisonType:=StringComparison.OrdinalIgnoreCase)) Then
                If String.IsNullOrWhiteSpace(Value) Then
                    Me._textBoxContents = ""
                    Me._FilePathTextBox.Text = Me._textBoxContents
                    Me._fileName = Me._textBoxContents
                    Me._extensionName = ""
                    Me._fileTitle = ""
                    Me._toolTip.SetToolTip(Me._FilePathTextBox, "Enter or browse for a file name")
                Else
                    If System.IO.File.Exists(Value) Then
                        Dim fileInfo As System.IO.FileInfo = My.Computer.FileSystem.GetFileInfo(Value)
                        Value = fileInfo.FullName
                        Me._textBoxContents = Value.Compact(Me._FilePathTextBox)
                        Me._FilePathTextBox.Text = Me._textBoxContents
                        Me._toolTip.SetToolTip(Me._FilePathTextBox, Value)
                        Me._fileName = fileInfo.Name
                        Me._extensionName = fileInfo.Extension
                        Me._fileTitle = fileInfo.Title
                    Else
                        Me._textBoxContents = "<file not found: " & Value & ">"
                        Me._FilePathTextBox.Text = Me._textBoxContents
                        Me._toolTip.SetToolTip(Me._FilePathTextBox, "Enter or browse for a file name")
                        Me._fileName = ""
                        Me._extensionName = ""
                        Me._fileTitle = ""
                    End If
                End If
                Me.OnSelectedChanged()
            End If
            Me._filePath = Value
            Me._FilePathTextBox.Refresh()
            Me.SafePostPropertyChanged()
        End Set
    End Property

    Private _FileTitle As String

    ''' <summary> Gets the file title. </summary>
    ''' <value> The file title. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property FileTitle() As String
        Get
            Return Me._fileTitle
        End Get
    End Property

#End Region

#Region " REFRESH ; SELECT FILE "

    ''' <summary> Refreshes the control. </summary>
    Public Overrides Sub Refresh()
        MyBase.Refresh()
    End Sub

    ''' <summary> Selects a file. </summary>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function SelectFile() As Boolean

        Using dialog As New OpenFileDialog()

            ' Use the common dialog class
            dialog.RestoreDirectory = True
            dialog.Multiselect = False
            dialog.CheckFileExists = True
            dialog.CheckPathExists = True
            dialog.ReadOnlyChecked = False
            dialog.DefaultExt = Me._defaultExtension
            dialog.Title = Me._dialogTitle
            dialog.FilterIndex = 0
            If String.IsNullOrWhiteSpace(Me._filePath) Then
                dialog.FileName = My.Computer.FileSystem.CurrentDirectory
            Else
                dialog.FileName = Me.FilePath
            End If
            dialog.Filter = Me._dialogFilter

            ' Open the Open dialog
            If dialog.ShowDialog(Me) = DialogResult.OK Then

                ' if file selected,
                Me.FilePath = dialog.FileName

                ' return true for pass
                Return True

            Else

                Return False

            End If

        End Using

    End Function

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Event handler. Called by browseButton for click events. Browses for a file name. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub BrowseButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _browseButton.Click

        ' Select a file
        SelectFile()

    End Sub

    ''' <summary> Event handler. Called by filePathTextBox for validating events. Enters a file name. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub FilePathTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _FilePathTextBox.Validating

        Me.FilePath = Me._FilePathTextBox.Text

    End Sub

    ''' <summary> Event handler. Called by UserControl for resize events. Resized the frame and sets
    ''' the width of the text boxes. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub UserControl_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize

        Height = Math.Max(Me._browseButton.Height, Me._FilePathTextBox.Height) + 2

        Me._browseButton.Top = (Height - Me._browseButton.Height) \ 2
        Me._browseButton.Left = Width - Me._browseButton.Width - 1
        Me._browseButton.Refresh()

        Me._FilePathTextBox.Top = (Height - Me._FilePathTextBox.Height) \ 2
        Me._FilePathTextBox.Width = Me._browseButton.Left - 1
        Me._FilePathTextBox.Refresh()

        Me.Refresh()

    End Sub

#End Region

#Region " EVENTS "

    ''' <summary> Occurs when a new file was selected. </summary>
    ''' <param name="e">Specifies the <see cref="EventArgs">event arguments</see>. </param>
    Public Event SelectedChanged As EventHandler(Of EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveSelectedChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.SelectedChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the selected changed event. </summary>
    Public Sub OnSelectedChanged()
        Dim evt As EventHandler(Of EventArgs) = Me.SelectedChangedEvent
        evt?.Invoke(Me, EventArgs.Empty)
        System.Windows.Forms.Application.DoEvents()
    End Sub

#End Region

End Class

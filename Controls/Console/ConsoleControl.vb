﻿Imports isr.Core.Pith.EventHandlerExtensions
Imports isr.Core.Pith.ExceptionExtensions
''' <summary> A console control. </summary>
''' <remarks>
''' This controls displays provides a 80x25 character display and entry console. The console
''' cannot be resized at this time nor can the text scroll.
''' </remarks>
''' <license>
''' (c) 2012 icemanind. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="7/9/2016" by="David">
''' > http://www.codeproject.com/Articles/1053951/Console-Control.
''' </history>
Public Class ConsoleControl
    Inherits UserControl

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me._RestoreDefaults()
    End Sub

    ''' <summary> Restore defaults. </summary>
    Private Sub _RestoreDefaults()
        Me._RenderFontName = "Courier New"
        Me._ControlScreenSize = New Size(80, 25)
        Me._CharacterSize = New Size(8, 15)
        Me._ControlSize = New Size(Me.ControlScreenSize.Width * Me.CharacterSize.Width + 6,
                                   Me.ControlScreenSize.Height * Me.CharacterSize.Height + 2)
        Me._ScreenSize = Me.ControlScreenSize.Width * Me.ControlScreenSize.Height
        Me._OnResize()

        Me._CursorX = 0
        Me._CursorY = 0
        Me._IsCursorOn = False
        Me.CursorType = CursorType.Underline

        Me.ConsoleBackgroundColor = Color.Black
        Me.ConsoleForegroundColor = Color.LightGray
        Me.CurrentForegroundColor = Color.LightGray
        Me.CurrentBackgroundColor = Color.Black

        Try
            Me._CursorFlashTimer = New Timer()
        Catch
            Me._CursorFlashTimer?.Dispose()
            Throw
        End Try
        Me._CursorFlashTimer.Enabled = False
        Me._CursorFlashTimer.Interval = 500

        Me._TextBlockArray = New TextBlock(Me.ScreenSize - 1) {} ' 80 x 25

        For i As Integer = 0 To ScreenSize - 1
            Me.TextBlockArray(i).BackgroundColor = ConsoleBackgroundColor
            Me.TextBlockArray(i).ForegroundColor = ConsoleForegroundColor
            Me.TextBlockArray(i).Character = ControlChars.NullChar
        Next i

        Me._KeysBuffer = New List(Of Char)()
        Me._CommandBuffer = New List(Of String)()
        Me._CommandBufferIndex = 0

        Me.AllowInput = True
        Me.EchoInput = True
        Me.ShowCursor = True

    End Sub

    ''' <summary> Gets options for controlling the create. </summary>
    ''' <value>
    ''' A <see cref="T:System.Windows.Forms.CreateParams" />
    '''  that contains the required creation parameters when the handle to the control is created.
    ''' </value>
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ExStyle = cp.ExStyle Or &H2000000
            Return cp
        End Get
    End Property

    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    If Me._CursorFlashTimer IsNot Nothing Then Me._CursorFlashTimer.Dispose() : Me._CursorFlashTimer = Nothing
                    If Me._renderFont IsNot Nothing Then Me._renderFont.Dispose()
                    Me.RemoveEventHandler(Me.LineEnteredEvent)
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " EVENTS "

    ''' <summary>
    ''' This event is raised whenever text is entered into the console control
    ''' </summary>
    Public Event LineEntered As EventHandler(Of LineEnteredEventArgs)

    ''' <summary> Removes event handler. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As EventHandler(Of LineEnteredEventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.LineEntered, CType(d, EventHandler(Of LineEnteredEventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

#End Region

#Region " APPERANCE "

    ''' <summary> Name of the render font. </summary>
    ''' <remarks> to_do: allow changing the font and building a new screen and character sizes. </remarks>
    Private ReadOnly Property RenderFontName As String = "Courier New"

    ''' <summary> Fixed Control Size. </summary>
    Private ReadOnly Property ControlSize As Size

    ''' <summary> Gets or sets the size of the control screen. </summary>
    ''' <value> The size of the control screen. </value>
    Private ReadOnly Property ControlScreenSize As Size

    ''' <summary> Gets or sets the size of the character. </summary>
    ''' <value> The size of the character. </value>
    Private ReadOnly Property CharacterSize As Size

    ''' <summary> Size of the screen. </summary>
    ''' <remarks> to_do: allow changing the screen size </remarks>
    Private ReadOnly Property ScreenSize As Integer

    Private _ShowCursor As Boolean
    ''' <summary>
    ''' Gets or Sets a value indicating whether the console should show the cursor
    ''' </summary>
    Public Property ShowCursor() As Boolean
        Get
            Return _ShowCursor
        End Get
        Set(value As Boolean)
            Me._ShowCursor = value
            Me._CursorFlashTimer.Enabled = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets a value indicating whether the console should allow keyboard input
    ''' </summary>
    Public Property AllowInput() As Boolean

    ''' <summary>
    ''' Gets or Sets a value indicating whether the console should echo any input it receives.
    ''' </summary>
    Public Property EchoInput() As Boolean

    Private _CursorType As CursorType
    ''' <summary>
    ''' Gets or Sets the current cursor type.
    ''' </summary>
    Public Property CursorType() As CursorType
        Get
            Return Me._cursorType
        End Get
        Set(ByVal value As CursorType)
            Me._cursorType = value
            Invalidate()
        End Set
    End Property

    ''' <summary> Gets or sets the current background color. </summary>
    ''' <value> The color of the current background. </value>
    Public Property CurrentBackgroundColor() As Color

    Private _ConsoleBackgroundColor As Color
    ''' <summary>
    ''' Gets or Sets the background color of the console.
    ''' Default is Black.
    ''' </summary>
    Public Property ConsoleBackgroundColor() As Color
        Get
            Return Me._consoleBackgroundColor
        End Get
        Set(ByVal value As Color)
            Me._consoleBackgroundColor = value
            Me.BackColor = value
            Invalidate()
        End Set
    End Property

    ''' <summary> Gets or sets the current foreground color. </summary>
    ''' <value> The color of the current foreground. </value>
    Public Property CurrentForegroundColor() As Color

    Private _ConsoleForegroundColor As Color
    ''' <summary>
    ''' Gets or Sets the foreground color of the console.
    ''' Default is light gray.
    ''' </summary>
    Public Property ConsoleForegroundColor() As Color
        Get
            Return Me._consoleForegroundColor
        End Get
        Set(ByVal value As Color)
            Me._consoleForegroundColor = value
            Me.ForeColor = value
            Invalidate()
        End Set
    End Property

#End Region

#Region " RENDER "

    Private _KeysBuffer As List(Of Char)
    Private _CommandBuffer As List(Of String)
    Private _CommandBufferIndex As Integer
    Private _IsCursorOn As Boolean

    ''' <summary> Process the command key. </summary>
    ''' <param name="msg">     [in,out] A <see cref="T:System.Windows.Forms.Message" />
    '''                        , passed by reference, that represents the window message to process. </param>
    ''' <param name="keyData"> One of the <see cref="T:System.Windows.Forms.Keys" />
    '''                         values that represents the key to process. </param>
    ''' <returns> true if the character was processed by the control; otherwise, false. </returns>
    Protected Overrides Function ProcessCmdKey(ByRef msg As Message, ByVal keyData As Keys) As Boolean
        If keyData = Keys.Up Then
            If Me._commandBufferIndex <= 0 OrElse Not Me.AllowInput Then
                Return True
            End If

            Dim len As Integer = Me._keysBuffer.Count
            For i As Integer = 0 To len - 1
                Me.ConsoleControlKeyPress(Me, New KeyPressEventArgs(ChrW(8)))
            Next i
            Me._keysBuffer.Clear()
            For Each c As Char In Me._commandBuffer(Me._commandBufferIndex - 1)
                Me._keysBuffer.Add(c)
                If EchoInput Then
                    Write(c)
                End If
            Next c
            Me._commandBufferIndex -= 1
            Return True
        End If
        If keyData = Keys.Down Then
            If (Me._commandBufferIndex + 1) >= Me._commandBuffer.Count OrElse Not AllowInput Then
                Return True
            End If

            Dim len As Integer = Me._keysBuffer.Count
            For i As Integer = 0 To len - 1
                Me.ConsoleControlKeyPress(Me, New KeyPressEventArgs(ChrW(8)))
            Next i
            _keysBuffer.Clear()

            For Each c As Char In Me._commandBuffer(Me._commandBufferIndex + 1)
                Me._keysBuffer.Add(c)
                If Me.EchoInput Then
                    Me.Write(c)
                End If
            Next c
            Me._commandBufferIndex += 1
            Return True
        End If
        Return MyBase.ProcessCmdKey(msg, keyData)
    End Function

    Private _CursorX As Integer
    Private _CursorY As Integer
    Private ReadOnly Property TextBlockArray() As TextBlock()
    ''' <summary> Console control key press. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Key press event information. </param>
    Private Sub ConsoleControlKeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles Me.KeyPress
        If Not AllowInput Then
            Return
        End If

        If AscW(e.KeyChar) = 8 Then
            If Me._keysBuffer.Count = 0 Then
                Return
            End If
            If Me.EchoInput Then
                Me._TextBlockArray(GetIndex()).Character = ControlChars.NullChar
                Me._cursorX -= 1
                If Me._cursorX < 0 Then
                    Me._cursorY -= 1
                    Me._cursorX = 79
                    If Me._cursorY < 0 Then
                        Me._cursorY += 1
                        Me._cursorX = 0
                    End If
                End If
                Me._TextBlockArray(GetIndex()).Character = ControlChars.NullChar
                Invalidate()
            End If
            Me._keysBuffer.RemoveAt(Me._keysBuffer.Count - 1)
            Return
        End If
        Me._keysBuffer.Add(e.KeyChar)
        If Me.EchoInput Then
            Me.Write(e.KeyChar)
            If e.KeyChar = ControlChars.Cr Then
                Me.Write(ControlChars.Lf)
            End If
        End If

        If e.KeyChar = ControlChars.Cr Then
            If Environment.NewLine.Length = 2 Then
                Me._keysBuffer.Add(ControlChars.Lf)
            End If
            Dim s As String = Me._keysBuffer.Aggregate("", Function(current, c) current + c)
            Me._keysBuffer.Clear()

            Me._commandBuffer.Add(s.Trim(ControlChars.Cr, ControlChars.Lf))
            Me._commandBufferIndex = Me._commandBuffer.Count
            Dim evt As EventHandler(Of LineEnteredEventArgs) = Me.LineEnteredEvent
            If evt IsNot Nothing Then evt.Invoke(Me, New LineEnteredEventArgs(s))
        End If
        Invalidate()
    End Sub

    Private WithEvents _CursorFlashTimer As Timer
    ''' <summary> Cursor flash timer tick. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _CursorFlashTimer_Tick(sender As Object, e As EventArgs) Handles _CursorFlashTimer.Tick
        If Not Me.ShowCursor Then Return
        Me._isCursorOn = Not Me._isCursorOn
        Dim c As Char
        Select Case CursorType
            Case CursorType.Block
                c = ChrW(&H2588)
            Case CursorType.Invisible
                c = " "c
            Case Else
                c = "_"c
        End Select
        Me._TextBlockArray(GetIndex()).Character = If(Me._isCursorOn, c, ControlChars.NullChar)
        Invalidate()
    End Sub

    Private ReadOnly _renderFont As New Font(RenderFontName, 10, FontStyle.Regular)
    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.Paint" />
    '''  event.
    ''' </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" />
    '''                   that contains the event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e Is Nothing Then Return
        Dim x As Integer = 0
        Dim y As Integer = 0
        Dim charWidth As Integer = Me.CharacterSize.Width
        Dim charHeight As Integer = Me.CharacterSize.Height

        Using bitmap As Bitmap = New Bitmap(Width, Height)
            Using g As Graphics = Graphics.FromImage(bitmap)
                For i As Integer = 0 To ScreenSize - 1
                    Dim fc As Color = If(Me.TextBlockArray(i).Character = ControlChars.NullChar,
                                            Me.ConsoleForegroundColor, Me.TextBlockArray(i).ForegroundColor)
                    Dim bc As Color = If(Me.TextBlockArray(i).Character = ControlChars.NullChar,
                                            Me.ConsoleBackgroundColor, Me.TextBlockArray(i).BackgroundColor)

                    Using bgBrush As Brush = New SolidBrush(bc)
                        g.FillRectangle(bgBrush, New Rectangle(x + 2, y + 1, charWidth, charHeight))
                        Using fgBrush As Brush = New SolidBrush(fc)
                            g.DrawString(If(Me.TextBlockArray(i).Character = ControlChars.NullChar, " ", Me.TextBlockArray(i).Character.ToString()),
                                            Me._renderFont, fgBrush, New Drawing.PointF(x, y))
                        End Using
                    End Using
                    x += charWidth
                    If x > 79 * charWidth Then
                        y += charHeight
                        x = 0
                    End If
                Next i
                e.Graphics.DrawImage(bitmap, e.ClipRectangle, e.ClipRectangle, GraphicsUnit.Pixel)
            End Using
        End Using
        MyBase.OnPaint(e)
    End Sub

    Private Sub _OnResize()
        Me.Width = Me.ControlSize.Width
        Me.Height = Me.ControlSize.Height
    End Sub
    ''' <summary> Raises the resize event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" />
    '''                   that contains the event data. </param>
    Protected Overrides Sub OnResize(ByVal e As EventArgs)
        MyBase.OnResize(e)
        Me._onResize()
    End Sub

#End Region

#Region " WRITING "

    ''' <summary>
    ''' Sets the position of the cursor
    ''' </summary>
    ''' <param name="row">The row of the cursor position</param>
    ''' <param name="column">The column of the cursor position</param>
    Public Sub SetCursorPosition(ByVal row As Integer, ByVal column As Integer)
        If ShowCursor Then
            Me.TextBlockArray(GetIndex()).Character = ControlChars.NullChar
        End If
        Me._cursorX = column
        Me._cursorY = row

        Invalidate()
    End Sub

    ''' <summary>
    ''' Sets the position of the cursor
    ''' </summary>
    ''' <param name="location">The location of the cursor position</param>
    Public Sub SetCursorPosition(ByVal location As Location)
        If location IsNot Nothing Then Me.SetCursorPosition(location.Row, location.Column)
    End Sub

    ''' <summary>
    ''' Gets the position of the cursor
    ''' </summary>
    ''' <returns>The location of the cursor</returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")>
    Public Function GetCursorPosition() As Location
        Return New Location With {.Column = _cursorX, .Row = _cursorY}
    End Function

    ''' <summary>
    ''' Writes a newline (carriage return) to the console.
    ''' </summary>
    Public Sub Write()
        Me.Write(Environment.NewLine)
    End Sub

    ''' <summary>
    ''' Writes a character to the console using the current foreground color and background color.
    ''' </summary>
    ''' <param name="value"> The character to write to the console. </param>
    Public Sub Write(ByVal value As Char)
        Me.Write(value, Me.CurrentForegroundColor, Me.CurrentBackgroundColor)
    End Sub

    ''' <summary>
    ''' Writes a character to the console using the specified foreground color and background color.
    ''' </summary>
    ''' <param name="value">     The character to write to the console. </param>
    ''' <param name="foreColor"> The foreground color. </param>
    ''' <param name="backColor"> The background color. </param>
    Public Sub Write(ByVal value As Char, ByVal foreColor As Color, ByVal backColor As Color)
        If AscW(value) = 7 Then
            Console.Beep(1000, 500)
            Return
        End If

        If AscW(value) = 13 Then
            Me.SetCursorPosition(Me.GetCursorPosition().Row, 0)
            Return
        End If
        If AscW(value) = 10 Then
            If Environment.NewLine.Length = 1 Then
                Me.SetCursorPosition(Me.GetCursorPosition().Row, 0)
            End If
            Me._cursorY += 1
            If Me._cursorY > 24 Then
                Me.ScrollUp()
                Me._cursorY = 24
            End If
            Return
        End If
        Me.TextBlockArray(GetIndex()).Character = value
        Me.TextBlockArray(GetIndex()).BackgroundColor = backColor
        Me.TextBlockArray(GetIndex()).ForegroundColor = foreColor
        Me.MoveCursorPosition()
        Invalidate()
    End Sub

    ''' <summary>
    ''' Writes a string to the console using the current
    ''' foreground color and background color
    ''' </summary>
    ''' <param name="text">The string to write to the console</param>
    Public Sub Write(ByVal text As String)
        Me.Write(text, CurrentForegroundColor, CurrentBackgroundColor)
    End Sub

    ''' <summary>
    ''' Writes a string to the console using the specified
    ''' foreground color and background color
    ''' </summary>
    ''' <param name="text">The string to write to the console</param>
    ''' <param name="foreColor">The foreground color</param>
    ''' <param name="backColor">The background color</param>
    Public Sub Write(ByVal text As String, ByVal foreColor As Color, ByVal backColor As Color)
        If String.IsNullOrWhiteSpace(text) Then Return
        For Each c As Char In text
            Me.Write(c, foreColor, backColor)
        Next c
        Invalidate()
    End Sub

    Private Sub MoveCursorPosition()
        Me._cursorX += 1
        If Me._cursorX > 79 Then
            Me._cursorX = 0
            Me._cursorY += 1
        End If
        If Me._cursorY > 24 Then
            ScrollUp()
            Me._cursorY = 24
        End If
    End Sub

    ''' <summary> Gets an index. </summary>
    ''' <param name="row"> The row of the cursor position. </param>
    ''' <param name="col"> The col. </param>
    ''' <returns> The index. </returns>
    Private Shared Function GetIndex(ByVal row As Integer, ByVal col As Integer) As Integer
        Return 80 * row + col
    End Function

    ''' <summary> Gets an index. </summary>
    ''' <returns> The index. </returns>
    Private Function GetIndex() As Integer
        Return ConsoleControl.GetIndex(Me._cursorY, Me._cursorX)
    End Function

    ''' <summary>
    ''' Scrolls the console screen window up the given.
    ''' number of lines.
    ''' </summary>
    ''' <param name="lines">The number of lines to scroll up</param>
    Public Sub ScrollUp(ByVal lines As Integer)
        Do While lines > 0
            For i As Integer = 0 To ScreenSize - 80 - 1
                Me._TextBlockArray(i) = Me._TextBlockArray(i + 80)
            Next i
            For i As Integer = ScreenSize - 80 To ScreenSize - 1
                Me._TextBlockArray(i).Character = ControlChars.NullChar
                Me._TextBlockArray(i).BackgroundColor = ConsoleBackgroundColor
                Me._TextBlockArray(i).ForegroundColor = ConsoleForegroundColor
            Next i
            lines -= 1
        Loop
        Invalidate()
    End Sub

    ''' <summary>
    ''' Scrolls the console screen window up one line.
    ''' </summary>
    Public Sub ScrollUp()
        ScrollUp(1)
    End Sub

    ''' <summary>
    ''' Clears the console screen
    ''' </summary>
    Public Sub Clear()
        For i As Integer = 0 To ScreenSize - 1
            Me._TextBlockArray(i).BackgroundColor = Me.ConsoleBackgroundColor
            Me._TextBlockArray(i).ForegroundColor = Me.ConsoleForegroundColor
            Me._TextBlockArray(i).Character = ControlChars.NullChar
        Next i
        Me._cursorX = 0
        Me._cursorY = 0
        Invalidate()
    End Sub

    ''' <summary>
    ''' Sets the background color at the specified location
    ''' </summary>
    ''' <param name="color">The background color to set</param>
    ''' <param name="row">The row at which to set the background color</param>
    ''' <param name="column">The column at which to set the background color</param>
    Public Sub SetBackgroundColorAt(ByVal color As Color, ByVal row As Integer, ByVal column As Integer)
        Me._TextBlockArray(GetIndex(row, column)).BackgroundColor = color
        Invalidate()
    End Sub

    ''' <summary>
    ''' Sets the foreground color at the specified location
    ''' </summary>
    ''' <param name="color">The foreground color to set</param>
    ''' <param name="row">The row at which to set the foreground color</param>
    ''' <param name="column">The column at which to set the foreground color</param>
    Public Sub SetForegroundColorAt(ByVal color As Color, ByVal row As Integer, ByVal column As Integer)
        Me._TextBlockArray(GetIndex(row, column)).ForegroundColor = color
        Invalidate()
    End Sub

    ''' <summary>
    ''' Sets the character at the specified location
    ''' </summary>
    ''' <param name="character">The character to set</param>
    ''' <param name="row">The row at which to place the character</param>
    ''' <param name="column">The column at which to place the character</param>
    Public Sub SetCharacterAt(ByVal character As Char, ByVal row As Integer, ByVal column As Integer)
        Me._TextBlockArray(GetIndex(row, column)).Character = character
        Invalidate()
    End Sub

#End Region

End Class

''' <summary> Values that represent cursor types. </summary>
Public Enum CursorType
    Invisible = 0
    Block = 1
    Underline = 2
End Enum

''' <summary> A location. </summary>
''' <license>
''' (c) 2012 icemanind. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="7/9/2016" by="David" revision=""> Created. </history>
Public Class Location
    Public Property Row() As Integer
    Public Property Column() As Integer
End Class

''' <summary> A text block. </summary>
''' <license>
''' (c) 2012 icemanind. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
Friend Structure TextBlock
    Public Property BackgroundColor() As Color
    Public Property ForegroundColor() As Color
    Public Property Character() As Char
End Structure

''' <summary> Additional information for line entered events. </summary>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="7/9/2016" by="David" revision=""> Created. </history>
Public Class LineEnteredEventArgs
    Inherits System.EventArgs

    ''' <summary> Constructor. </summary>
    ''' <param name="line"> The line. </param>
    Public Sub New(ByVal line As String)
        MyBase.New
        Me._Line = line
    End Sub

    ''' <summary> Gets the empty. </summary>
    ''' <value> The empty. </value>
    Public Shared Shadows ReadOnly Property Empty As LineEnteredEventArgs
        Get
            Return New LineEnteredEventArgs("")
        End Get
    End Property

    ''' <summary> Gets or sets the line. </summary>
    ''' <value> The line. </value>
    Public ReadOnly Property Line As String

End Class
﻿Imports System.Windows.Forms.Design
''' <summary> A bindable tool strip menu item. </summary>
''' <license>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="9/27/2018" by="David" revision="">
''' http://forums.devx.com/showthread.php?153607-Making-ToolStripStatusLabel-data-bindable. </history>
<ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.All)>
Public Class ToolStripMenuItem
    Inherits Windows.Forms.ToolStripMenuItem
    Implements IBindableComponent

#Region " BINDABLE "

    Private _context As BindingContext = Nothing

    ''' <summary>
    ''' Gets or sets the collection of currency managers for the
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </summary>
    ''' <value>
    ''' The collection of <see cref="T:System.Windows.Forms.BindingManagerBase" /> objects for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </value>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")>
    Public Property BindingContext() As BindingContext Implements IBindableComponent.BindingContext
        Get
            If Nothing Is Me._context Then Me._context = New BindingContext()
            Return Me._context
        End Get
        Set(ByVal value As BindingContext)
            Me._context = value
        End Set
    End Property

    Private _bindings As ControlBindingsCollection

    ''' <summary>
    ''' Gets the collection of data-binding objects for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </summary>
    ''' <value>
    ''' The <see cref="T:System.Windows.Forms.ControlBindingsCollection" /> for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </value>
    Public ReadOnly Property DataBindings() As ControlBindingsCollection Implements IBindableComponent.DataBindings
        Get
            If Me._bindings Is Nothing Then _bindings = New ControlBindingsCollection(Me)
            Return Me._bindings
        End Get
    End Property
#End Region

End Class


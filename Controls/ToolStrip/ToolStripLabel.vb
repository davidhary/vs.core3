﻿Imports System.ComponentModel

''' <summary> A tool strip spring Label. </summary>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="3/22/2017" by="David" revision=""> Created. </history>
Public Class ToolStripSpringLabel
    Inherits System.Windows.Forms.ToolStripLabel
    Implements ISpringable, IBindableComponent

#Region " BINDABLE "

    Private _context As BindingContext = Nothing

    ''' <summary>
    ''' Gets or sets the collection of currency managers for the
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </summary>
    ''' <value>
    ''' The collection of <see cref="T:System.Windows.Forms.BindingManagerBase" /> objects for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </value>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")>
    Public Property BindingContext() As BindingContext Implements IBindableComponent.BindingContext
        Get
            If Nothing Is Me._context Then Me._context = New BindingContext()
            Return Me._context
        End Get
        Set(ByVal value As BindingContext)
            Me._context = value
        End Set
    End Property

    Private _bindings As ControlBindingsCollection

    ''' <summary>
    ''' Gets the collection of data-binding objects for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </summary>
    ''' <value>
    ''' The <see cref="T:System.Windows.Forms.ControlBindingsCollection" /> for this
    ''' <see cref="T:System.Windows.Forms.IBindableComponent" />.
    ''' </value>
    Public ReadOnly Property DataBindings() As ControlBindingsCollection Implements IBindableComponent.DataBindings
        Get
            If Me._bindings Is Nothing Then _bindings = New ControlBindingsCollection(Me)
            Return Me._bindings
        End Get
    End Property

#End Region

#Region " I SPRINGABLE "

    ''' <summary> Gets or sets the sentinel indicating if the tool strip item can spring. </summary>
    ''' <value> The sentinel indicating if the tool strip item can spring. </value>
    Public ReadOnly Property CanSpring As Boolean Implements ISpringable.CanSpring
        Get
            Return Me.AutoSize AndAlso Me.Spring
        End Get
    End Property

    ''' <summary> Gets or sets the spring. </summary>
    ''' <value> <c>true</c> if the control stretches to fill the remaining space in the owner control. </value>
    <DefaultValue(False), Description("Spring"), Category("Appearance")>
    Public Property Spring As Boolean Implements ISpringable.Spring

#End Region

#Region " SPRING IMPLEMENTATION "

    Public Overrides Function GetPreferredSize(ByVal constrainingSize As Size) As Size

        ' Use the default size if the tool strip item is on the overflow menu,
        ' is on a vertical ToolStrip, or cannot spring.
        If Me.IsOnOverflow OrElse Me.Owner.Orientation = Orientation.Vertical OrElse Not Me.CanSpring Then
            Return Me.DefaultSize
        End If

        ' Declare a variable to store the total available width as 
        ' it is calculated, starting with the display width of the 
        ' owning ToolStrip.
        Dim width As Int32 = Owner.DisplayRectangle.Width

        ' Subtract the width of the overflow button if it is displayed. 
        If Owner.OverflowButton.Visible Then
            width = width - Owner.OverflowButton.Width -
                Owner.OverflowButton.Margin.Horizontal()
        End If

        ' Declare a variable to maintain a count of Spring items
        ' currently displayed in the owning ToolStrip. 
        Dim springItemCount As Int32 = 0

        For Each item As ToolStripItem In Owner.Items

            ' Ignore items on the overflow menu.
            If item.IsOnOverflow Then Continue For

            If TryCast(item, ISpringable)?.CanSpring Then
                ' For Spring items, increment the count and 
                ' subtract the margin width from the total available width.
                springItemCount += 1
                width -= item.Margin.Horizontal
            Else
                ' For all other items, subtract the full width from the total
                ' available width.
                width = width - item.Width - item.Margin.Horizontal
            End If

        Next

        ' If there are multiple spring items in the owning
        ' ToolStrip, divide the total available width between them. 
        If springItemCount > 1 Then width = width \ springItemCount

        ' If the available width is less than the default width, use the
        ' default width, forcing one or more items onto the overflow menu.
        If width < DefaultSize.Width Then width = DefaultSize.Width

        ' Retrieve the preferred size from the base class, but change the
        ' width to the calculated width. 
        Dim preferredSize As Size = MyBase.GetPreferredSize(constrainingSize)
        preferredSize.Width = width
        Return preferredSize

    End Function
#End Region


End Class

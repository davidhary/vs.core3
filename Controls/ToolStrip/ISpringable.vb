﻿''' <summary> Interface for springable. </summary>
''' <license>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
Public Interface ISpringable

    ''' <summary> Gets or sets the sentinel indicating if the tool strip item can spring. </summary>
    ''' <value> The sentinel indicating if the tool strip item can spring. </value>
    ReadOnly Property CanSpring As Boolean

    ''' <summary> Gets or sets the spring. </summary>
    ''' <value> <c>true</c> if the control stretches to fill the remaining space in the owner control. </value>
    Property Spring As Boolean

End Interface

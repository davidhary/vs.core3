Imports System.Drawing.Drawing2D
''' <summary> A tab style chrome provider. </summary>
''' <license>
''' (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="09/26/2015" by="David" revision=""> Created.
''' http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </history>
<System.ComponentModel.ToolboxItem(False)>
Public Class TabStyleChromeProvider
    Inherits TabStyleProvider

    Public Sub New(tabControl As CustomTabControl)
        MyBase.New(tabControl, 16, 16, True, Color.White, New Point(7, 5))
    End Sub

    Public Overrides Sub AddTabBorder(path As System.Drawing.Drawing2D.GraphicsPath, tabBounds As System.Drawing.Rectangle)

        If path Is Nothing Then Throw New ArgumentNullException(NameOf(path))
        Dim spread As Integer
        Dim eigth As Integer
        Dim sixth As Integer
        Dim quarter As Integer

        If Me.TabControl.Alignment <= TabAlignment.Bottom Then
            spread = CInt(Math.Truncate(Math.Floor(CDec(tabBounds.Height) * 2 / 3)))
            eigth = CInt(Math.Truncate(Math.Floor(CDec(tabBounds.Height) * 1 / 8)))
            sixth = CInt(Math.Truncate(Math.Floor(CDec(tabBounds.Height) * 1 / 6)))
            quarter = CInt(Math.Truncate(Math.Floor(CDec(tabBounds.Height) * 1 / 4)))
        Else
            spread = CInt(Math.Truncate(Math.Floor(CDec(tabBounds.Width) * 2 / 3)))
            eigth = CInt(Math.Truncate(Math.Floor(CDec(tabBounds.Width) * 1 / 8)))
            sixth = CInt(Math.Truncate(Math.Floor(CDec(tabBounds.Width) * 1 / 6)))
            quarter = CInt(Math.Truncate(Math.Floor(CDec(tabBounds.Width) * 1 / 4)))
        End If

        Select Case Me.TabControl.Alignment
            Case TabAlignment.Top

                path.AddCurve(New Point() {New Point(tabBounds.X, tabBounds.Bottom), New Point(tabBounds.X + sixth, tabBounds.Bottom - eigth), New Point(tabBounds.X + spread - quarter, tabBounds.Y + eigth), New Point(tabBounds.X + spread, tabBounds.Y)})
                path.AddLine(tabBounds.X + spread, tabBounds.Y, tabBounds.Right - spread, tabBounds.Y)
                path.AddCurve(New Point() {New Point(tabBounds.Right - spread, tabBounds.Y), New Point(tabBounds.Right - spread + quarter, tabBounds.Y + eigth), New Point(tabBounds.Right - sixth, tabBounds.Bottom - eigth), New Point(tabBounds.Right, tabBounds.Bottom)})
                Exit Select
            Case TabAlignment.Bottom
                path.AddCurve(New Point() {New Point(tabBounds.Right, tabBounds.Y), New Point(tabBounds.Right - sixth, tabBounds.Y + eigth), New Point(tabBounds.Right - spread + quarter, tabBounds.Bottom - eigth), New Point(tabBounds.Right - spread, tabBounds.Bottom)})
                path.AddLine(tabBounds.Right - spread, tabBounds.Bottom, tabBounds.X + spread, tabBounds.Bottom)
                path.AddCurve(New Point() {New Point(tabBounds.X + spread, tabBounds.Bottom), New Point(tabBounds.X + spread - quarter, tabBounds.Bottom - eigth), New Point(tabBounds.X + sixth, tabBounds.Y + eigth), New Point(tabBounds.X, tabBounds.Y)})
                Exit Select
            Case TabAlignment.Left
                path.AddCurve(New Point() {New Point(tabBounds.Right, tabBounds.Bottom), New Point(tabBounds.Right - eigth, tabBounds.Bottom - sixth), New Point(tabBounds.X + eigth, tabBounds.Bottom - spread + quarter), New Point(tabBounds.X, tabBounds.Bottom - spread)})
                path.AddLine(tabBounds.X, tabBounds.Bottom - spread, tabBounds.X, tabBounds.Y + spread)
                path.AddCurve(New Point() {New Point(tabBounds.X, tabBounds.Y + spread), New Point(tabBounds.X + eigth, tabBounds.Y + spread - quarter), New Point(tabBounds.Right - eigth, tabBounds.Y + sixth), New Point(tabBounds.Right, tabBounds.Y)})

                Exit Select
            Case TabAlignment.Right
                path.AddCurve(New Point() {New Point(tabBounds.X, tabBounds.Y), New Point(tabBounds.X + eigth, tabBounds.Y + sixth), New Point(tabBounds.Right - eigth, tabBounds.Y + spread - quarter), New Point(tabBounds.Right, tabBounds.Y + spread)})
                path.AddLine(tabBounds.Right, tabBounds.Y + spread, tabBounds.Right, tabBounds.Bottom - spread)
                path.AddCurve(New Point() {New Point(tabBounds.Right, tabBounds.Bottom - spread), New Point(tabBounds.Right - eigth, tabBounds.Bottom - spread + quarter), New Point(tabBounds.X + eigth, tabBounds.Bottom - sixth), New Point(tabBounds.X, tabBounds.Bottom)})
                Exit Select
        End Select
    End Sub

    Protected Overrides Sub DrawTabCloser(index As Integer, graphics As Graphics)
        If graphics Is Nothing Then Throw New ArgumentNullException(NameOf(graphics))
        If Me.ShowTabCloser Then
            Dim closerRect As Rectangle = Me.TabControl.GetTabCloserRect(index)
            graphics.SmoothingMode = SmoothingMode.AntiAlias
            If closerRect.Contains(Me.TabControl.MousePosition) Then
                Using closerPath As GraphicsPath = GetCloserButtonPath(closerRect)
                    Using closerBrush As New SolidBrush(Color.FromArgb(193, 53, 53))
                        graphics.FillPath(closerBrush, closerPath)
                    End Using
                End Using
                Using closerPath As GraphicsPath = GetCloserPath(closerRect)
                    Using closerPen As New Pen(Me.CloserColorActive)
                        graphics.DrawPath(closerPen, closerPath)
                    End Using
                End Using
            Else
                Using closerPath As GraphicsPath = GetCloserPath(closerRect)
                    Using closerPen As New Pen(Me.CloserColor)
                        graphics.DrawPath(closerPen, closerPath)
                    End Using
                End Using
            End If
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Private Shared Function GetCloserButtonPath(closerRect As Rectangle) As GraphicsPath
        Dim closerPath As New GraphicsPath()
        closerPath.AddEllipse(New Rectangle(closerRect.X - 2, closerRect.Y - 2, closerRect.Width + 4, closerRect.Height + 4))
        closerPath.CloseFigure()
        Return closerPath
    End Function
End Class

Imports System.Drawing.Drawing2D
''' <summary> A tab style vs 2010 provider. </summary>
''' <license>
''' (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="09/26/2015" by="David" revision=""> Created.
''' http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </history>
<System.ComponentModel.ToolboxItem(False)>
Public Class TabStyleVS2010Provider
    Inherits TabStyleRoundedProvider

    Public Sub New(tabControl As CustomTabControl)
        MyBase.New(tabControl,
                    Color.Transparent, Color.FromArgb(155, 167, 183),
                   Color.White, Color.WhiteSmoke,
                   New Point(6, 5), 3,
               True, Color.Black, Color.FromArgb(117, 99, 61))
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Protected Overrides Function GetTabBackgroundBrush(index As Integer) As Brush
        Dim fillBrush As LinearGradientBrush = Nothing

        '	Capture the colors dependent on selection state of the tab
        Dim dark As Color = Color.Transparent
        Dim light As Color = Color.Transparent

        If Me.TabControl.SelectedIndex = index Then
            dark = Color.FromArgb(229, 195, 101)
            light = SystemColors.Window
        ElseIf Not Me.TabControl.TabPages(index).Enabled Then
            light = dark
        ElseIf Me.HotTrack AndAlso index = Me.TabControl.ActiveIndex Then
            '	Enable hot tracking
            dark = Color.FromArgb(108, 116, 118)
            light = dark
        End If

        '	Get the correctly aligned gradient
        Dim tabBounds As Rectangle = Me.GetTabRect(index)
        tabBounds.Inflate(3, 3)
        tabBounds.X -= 1
        tabBounds.Y -= 1
        Select Case Me.TabControl.Alignment
            Case TabAlignment.Top
                fillBrush = New LinearGradientBrush(tabBounds, light, dark, LinearGradientMode.Vertical)
            Case TabAlignment.Bottom
                fillBrush = New LinearGradientBrush(tabBounds, dark, light, LinearGradientMode.Vertical)
            Case TabAlignment.Left
                fillBrush = New LinearGradientBrush(tabBounds, light, dark, LinearGradientMode.Horizontal)
            Case TabAlignment.Right
                fillBrush = New LinearGradientBrush(tabBounds, dark, light, LinearGradientMode.Horizontal)
        End Select

        '	Add the blend
        fillBrush.Blend = GetBackgroundBlend()

        Return fillBrush
    End Function

    Private Overloads Shared Function GetBackgroundBlend() As Blend
        Dim relativeIntensities As Single() = New Single() {0F, 0.5F, 1.0F, 1.0F}
        Dim relativePositions As Single() = New Single() {0F, 0.5F, 0.51F, 1.0F}
        Dim blend As New Blend() With {.Factors = relativeIntensities, .Positions = relativePositions}
        Return blend
    End Function

    Public Overrides Function GetPageBackgroundBrush(index As Integer) As Brush

        '	Capture the colors dependent on selection state of the tab
        Dim light As Color = Color.Transparent

        If Me.TabControl.SelectedIndex = index Then
            light = Color.FromArgb(229, 195, 101)
        ElseIf Not Me.TabControl.TabPages(index).Enabled Then
            light = Color.Transparent
        ElseIf Me.HotTrack AndAlso index = Me.TabControl.ActiveIndex Then
            '	Enable hot tracking
            light = Color.Transparent
        End If

        Return New SolidBrush(light)
    End Function

    Protected Overrides Sub DrawTabCloser(index As Integer, graphics As Graphics)
        If graphics Is Nothing Then Throw New ArgumentNullException(NameOf(graphics))
        If Me.ShowTabCloser AndAlso graphics IsNot Nothing Then
            Dim closerRect As Rectangle = Me.TabControl.GetTabCloserRect(index)
            graphics.SmoothingMode = SmoothingMode.AntiAlias
            If closerRect.Contains(Me.TabControl.MousePosition) Then
                Using closerPath As GraphicsPath = GetCloserButtonPath(closerRect)
                    graphics.FillPath(Brushes.White, closerPath)
                    Using closerPen As New Pen(Color.FromArgb(229, 195, 101))
                        graphics.DrawPath(closerPen, closerPath)
                    End Using
                End Using
                Using closerPath As GraphicsPath = GetCloserPath(closerRect)
                    Using closerPen As New Pen(Me.CloserColorActive)
                        closerPen.Width = 2
                        graphics.DrawPath(closerPen, closerPath)
                    End Using
                End Using
            Else
                If index = Me.TabControl.SelectedIndex Then
                    Using closerPath As GraphicsPath = GetCloserPath(closerRect)
                        Using closerPen As New Pen(Me.CloserColor)
                            closerPen.Width = 2
                            graphics.DrawPath(closerPen, closerPath)
                        End Using
                    End Using
                ElseIf index = Me.TabControl.ActiveIndex Then
                    Using closerPath As GraphicsPath = GetCloserPath(closerRect)
                        Using closerPen As New Pen(Color.FromArgb(155, 167, 183))
                            closerPen.Width = 2
                            graphics.DrawPath(closerPen, closerPath)
                        End Using
                    End Using
                End If

            End If
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Private Shared Function GetCloserButtonPath(closerRect As Rectangle) As GraphicsPath
        Dim closerPath As New GraphicsPath()
        closerPath.AddLine(closerRect.X - 1, closerRect.Y - 2, closerRect.Right + 1, closerRect.Y - 2)
        closerPath.AddLine(closerRect.Right + 2, closerRect.Y - 1, closerRect.Right + 2, closerRect.Bottom + 1)
        closerPath.AddLine(closerRect.Right + 1, closerRect.Bottom + 2, closerRect.X - 1, closerRect.Bottom + 2)
        closerPath.AddLine(closerRect.X - 2, closerRect.Bottom + 1, closerRect.X - 2, closerRect.Y - 1)
        closerPath.CloseFigure()
        Return closerPath
    End Function

End Class

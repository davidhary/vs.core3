Imports System.ComponentModel
''' <summary> A flashing LED Control. </summary>
''' <license> (c) 2007 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/04/2007" by="HORIA TUDOSIE" revision="1.0.2803.x"> http://www.CodeProject.com/cs/MISCCTRL/FlashLED.asp. </history>
''' <history date="09/05/2007" by="David" revision="1.0.2804.x">         Added Flush Color property. </history>
<System.ComponentModel.Description("Flashing LED Control")>
Public Class FlashLed

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        SetStyle(ControlStyles.DoubleBuffer, True)
        SetStyle(ControlStyles.UserPaint, True)
        SetStyle(ControlStyles.ResizeRedraw, True)

        ' These too lines are required to make the control transparent.
        SetStyle(ControlStyles.SupportsTransparentBackColor, True)
        Me._FlashColor = Color.Red
        '_flash = False
        Me._FlashColorsArray = New Color(4) {Color.Red, Color.Empty, Color.Yellow, Color.Empty, Color.Blue}
        Me._FlashColors = "Red,,Yellow,,Blue"
        Me._FlashIntervalsArray = New Integer(5) {500, 250, 500, 250, 500, 250}
        Me._FlashIntervals = "500,250,500,250,500,250"
        Me._SparklePenWidth = 2
        Me.Active = True
        Me.BackColor = Color.Transparent
        Me.OffColor = SystemColors.Control
        Me.OnColor = Color.Red

        ' Set the Width and Height to an odd number for the convenience of having a pixel in the center 
        ' of the control. 17 is a good size for a perfect circle for a circular LED.
        'Width = 17
        'Height = 17
        Try
            Me._tickTimer = New Timer()
        Catch
            Me._tickTimer?.Dispose()
            Throw
        End Try
        Me._tickTimer.Enabled = False
        AddHandler Me._tickTimer.Tick, AddressOf Me._Tick

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Pith.PropertyNotifyControlBase and
    ''' optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
                If _tickTimer IsNot Nothing Then _tickTimer.Dispose() : _tickTimer = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> true to active. </summary>
    Private _Active As Boolean

    ''' <summary> Turns on the <see cref="OnColor">on color</see> when True or the
    ''' <see cref="OffColor">off color</see> when False. </summary>
    ''' <value> The active. </value>
    <Category("Behavior"), DefaultValue(True)>
    Public Property Active() As Boolean
        Get
            Return Me._Active
        End Get
        Set(ByVal value As Boolean)
            Me._Active = value
            Invalidate()
        End Set
    End Property

    ''' <summary> true to flash. </summary>
    Private _Flash As Boolean

    ''' <summary> Gets or sets the Flash condition.  When True, the Led flashes. </summary>
    ''' <value> The flash. </value>
    <Category("Behavior"), DefaultValue(False)>
    Public Property Flash() As Boolean
        Get
            Return Me._Flash
        End Get
        Set(ByVal value As Boolean)
            Me._Flash = value ' AndAlso (Me._flashIntervalsArray.Length > 0)
            tickIndex = 0
            _tickTimer.Interval = Me._FlashIntervalsArray(tickIndex)
            _tickTimer.Enabled = Me._Flash
            Me.Active = Me.Active
        End Set
    End Property

    ''' <summary> The flash color. </summary>
    Private _FlashColor As Color

    ''' <summary> Gets or sets the Flash color. </summary>
    ''' <value> The color of the flash. </value>
    <Category("Appearance")>
    Public Property FlashColor() As Color
        Get
            Return Me._FlashColor
        End Get
        Set(ByVal value As Color)
            Me._FlashColor = value
            Invalidate()
        End Set
    End Property

    ''' <summary>
    ''' Holds the array of flash colors in case a set of colors is used.
    ''' </summary>
    Private _FlashColorsArray As Color()

    ''' <summary> List of colors of the flashes. </summary>
    Private _FlashColors As String

    ''' <summary> Gets or sets the set of flash colors. </summary>
    ''' <remarks> Accepts a delimited string that is converted to a color array for setting the flash
    ''' color. Any reasonable delimiter will break the input string, and error items will default to
    ''' Color.Empty for colors that will switch the LED Off. </remarks>
    ''' <value> A list of colors of the flashes. </value>
    <Category("Appearance"), DefaultValue("Red,,Yellow,,Blue")>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Property FlashColors() As String
        Get
            Return Me._FlashColors
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then
                Me._FlashColors = ""
            Else
                Me._FlashColors = value
            End If
            If String.IsNullOrWhiteSpace(Me._FlashColors) Then
                Me._FlashColorsArray = Nothing
            Else
                Dim fc As String() = Me._FlashColors.Split(New Char() {","c, "/"c, "|"c, " "c, Environment.NewLine.ToCharArray()(0), Environment.NewLine.ToCharArray()(1)})
                Me._FlashColorsArray = New Color(fc.Length - 1) {}
                For i As Integer = 0 To fc.Length - 1
                    Try
                        If (fc(i) <> "") Then
                            Me._FlashColorsArray(i) = Color.FromName(fc(i))
                        Else
                            Me._FlashColorsArray(i) = Color.Empty
                        End If
                    Catch
                        Me._FlashColorsArray(i) = Color.Empty
                    End Try
                Next i
            End If
        End Set
    End Property

    ''' <summary>
    ''' Holds the array of flash intervals.
    ''' </summary>
    Private _FlashIntervalsArray As Integer()

    ''' <summary> The flash intervals. </summary>
    Private _FlashIntervals As String

    ''' <summary> Gets or sets the set of flash intervals. </summary>
    ''' <remarks> Accepts a delimited string that is converted to a interval array for setting the
    ''' flash intervals. Any reasonable delimiter will break the input string, and error items will
    ''' default to 25 ms for the interval. </remarks>
    ''' <value> The flash intervals. </value>
    <Category("Appearance"), DefaultValue("500,250,500,250,500,250")>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Property FlashIntervals() As String
        Get
            Return Me._FlashIntervals
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then
                Me._FlashIntervals = ""
            Else
                Me._FlashIntervals = value
            End If
            If String.IsNullOrWhiteSpace(Me._FlashIntervals) Then
                Me._FlashIntervalsArray = Nothing
            Else
                Dim fi As String() = Me._FlashIntervals.Split(New Char() {","c, "/"c, "|"c, " "c, Environment.NewLine.ToCharArray()(0), Environment.NewLine.ToCharArray()(1)})
                Me._FlashIntervalsArray = New Integer(fi.Length - 1) {}
                For i As Integer = 0 To fi.Length - 1
                    Try
                        Me._FlashIntervalsArray(i) = Integer.Parse(fi(i), Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture)
                    Catch
                        Me._FlashIntervalsArray(i) = 25
                    End Try
                Next i
            End If
        End Set
    End Property

    ''' <summary> The off color. </summary>
    Private _OffColor As Color

    ''' <summary> Gets or sets the OFF color. </summary>
    ''' <value> The color of the off. </value>
    <Category("Appearance")>
    Public Property OffColor() As Color
        Get
            Return Me._OffColor
        End Get
        Set(ByVal value As Color)
            Me._OffColor = value
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The on color. </summary>
    Private _OnColor As Color

    ''' <summary> Gets or sets the ON color. </summary>
    ''' <value> The color of the on. </value>
    <Category("Appearance")>
    Public Property OnColor() As Color
        Get
            Return Me._OnColor
        End Get
        Set(ByVal value As Color)
            Me._OnColor = value
            Me.Invalidate()
        End Set
    End Property

#End Region

#Region " HELPER COLOR FUNCTIONS "

    ''' <summary> Blends the <para>firstColor</para> over the <para>secondColor</para> with a weighted
    ''' blend of <para>firstWeight</para> and <para>secondWeight</para>. </summary>
    ''' <remarks> The function works by splitting the two colors in R, G and B components, applying the
    ''' ratio, and returning the recomposed color. This function blends the LED color in the margin
    ''' of the bubble, and adds the sparkle that will give the bubble's volume. When the LED is On,
    ''' the margin is enlightened with White, while - when Off is darkened with Black. The same for
    ''' the sparkle, but with different ratios. </remarks>
    ''' <param name="firstColor">   The first color. </param>
    ''' <param name="secondColor">  The second color. </param>
    ''' <param name="firstWeight">  The first weight. </param>
    ''' <param name="secondWeight"> The second weight. </param>
    ''' <returns> null if it fails, else a Color. </returns>
    Public Shared Function FadeColor(ByVal firstColor As Color, ByVal secondColor As Color, ByVal firstWeight As Integer, ByVal secondWeight As Integer) As Color
        Dim r As Integer = CInt((firstWeight * firstColor.R + secondWeight * secondColor.R) / (firstWeight + secondWeight))
        Dim g As Integer = CInt((firstWeight * firstColor.G + secondWeight * secondColor.G) / (firstWeight + secondWeight))
        Dim b As Integer = CInt((firstWeight * firstColor.B + secondWeight * secondColor.B) / (firstWeight + secondWeight))
        Return Color.FromArgb(r, g, b)
    End Function

    ''' <summary> Blends two colors with equal weights. </summary>
    ''' <param name="firstColor">  The first color. </param>
    ''' <param name="secondColor"> The second color. </param>
    ''' <returns> A Color. </returns>
    Public Shared Function FadeColor(ByVal firstColor As Color, ByVal secondColor As Color) As Color
        Return FadeColor(firstColor, secondColor, 1, 1)
    End Function

    ''' <summary> Width of the sparkle pen. </summary>
    Private _SparklePenWidth As Single

    ''' <summary> Draws the LED. </summary>
    ''' <param name="graphics"> The graphics. </param>
    ''' <param name="color">    The color. </param>
    ''' <param name="sparkle">  true to sparkle. </param>
    Private Sub DrawLed(ByVal graphics As Graphics, ByVal color As Color, ByVal sparkle As Boolean)
        Dim halfPenWidth As Single = Me._SparklePenWidth / 2
        Dim imageWidth As Integer = Width - Me.Margin.Left
        Dim imageHeight As Integer = Height - Me.Margin.Top
        If sparkle Then
            Using b As New SolidBrush(color)
                graphics.FillEllipse(b, 1, 1, imageWidth, imageHeight)
            End Using
            Using p As New Pen(FadeColor(color, Color.White, 1, 2), Me._SparklePenWidth)
                graphics.DrawArc(p, halfPenWidth + 2, halfPenWidth + 2, Width - 6 - halfPenWidth, Height - 6 - halfPenWidth, -90.0F, -90.0F)
            End Using
            Using p As New Pen(FadeColor(color, Color.White), 1)
                graphics.DrawEllipse(p, 1, 1, imageWidth, imageHeight)
            End Using
        Else
            Using b As New SolidBrush(color)
                graphics.FillEllipse(b, 1, 1, imageWidth, imageHeight)
            End Using
            Using p As New Pen(FadeColor(color, Color.Black, 2, 1), Me._SparklePenWidth)
                graphics.DrawArc(p, 3, 3, Width - 7, Height - 7, 0.0F, 90.0F)
            End Using
            Using p As New Pen(FadeColor(color, Color.Black), 1)
                graphics.DrawEllipse(p, 1, 1, imageWidth, imageHeight)
            End Using
        End If
    End Sub

#End Region

#Region " EVENT HANDLERS "

    ''' <summary>
    ''' Event for adding external features.
    ''' </summary>
    Public Shadows Event Paint As PaintEventHandler

    ''' <summary> Draws the control. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    ''' event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e Is Nothing Then Return
        If PaintEvent IsNot Nothing Then
            PaintEvent(Me, e)
        Else
            MyBase.OnPaint(e)
            '        e.Graphics.Clear(BackColor);
            If Enabled Then
                If Me._Flash Then
                    Me.DrawLed(e.Graphics, Me._FlashColor, True)
                    'e.Graphics.FillEllipse(New SolidBrush(Me.FlashColor), 1, 1, Width - 3, Height - 3)
                    'e.Graphics.DrawArc(New Pen(FadeColor(FlashColor, Color.White, 1, 2), 2), 3, 3, Width - 7, Height - 7, -90.0F, -90.0F)
                    'e.Graphics.DrawEllipse(New Pen(FadeColor(FlashColor, Color.White), 1), 1, 1, Width - 3, Height - 3)
                Else
                    ' if non flash mode, set the on or off color.
                    If Active Then
                        Me.DrawLed(e.Graphics, Me._OnColor, True)
                        'e.Graphics.FillEllipse(New SolidBrush(Me.OnColor), 1, 1, Width - 3, Height - 3)
                        'e.Graphics.DrawArc(New Pen(FadeColor(OnColor, Color.White, 1, 2), 2), 3, 3, Width - 7, Height - 7, -90.0F, -90.0F)
                        'e.Graphics.DrawEllipse(New Pen(FadeColor(OnColor, Color.White), 1), 1, 1, Width - 3, Height - 3)
                    Else
                        Me.DrawLed(e.Graphics, Me._OffColor, False)
                        'e.Graphics.FillEllipse(New SolidBrush(OffColor), 1, 1, Width - 3, Height - 3)
                        'e.Graphics.DrawArc(New Pen(FadeColor(OffColor, Color.Black, 2, 1), 2), 3, 3, Width - 7, Height - 7, 0.0F, 90.0F)
                        'e.Graphics.DrawEllipse(New Pen(FadeColor(OffColor, Color.Black), 1), 1, 1, Width - 3, Height - 3)
                    End If
                End If
            Else
                Using p As New Pen(System.Drawing.SystemColors.ControlDark, 1)
                    e.Graphics.DrawEllipse(p, 1, 1, Width - 3, Height - 3)
                End Using
            End If
        End If
    End Sub

    ''' <summary> Zero-based index of the tick. </summary>
    Private tickIndex As Integer

    ''' <summary> Handles the change of colors in flash mode. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        tickIndex += 1
        If tickIndex >= Me._FlashIntervalsArray.Length Then
            tickIndex = 0
        End If
        _tickTimer.Interval = Me._FlashIntervalsArray(tickIndex)
        If Me._flashColorsArray Is Nothing Then
            ' if no color array defined or array color index out of bounds,
            ' use the on/off colors to flash
            If Me._flashColor.Equals(Me._onColor) Then
                Me.FlashColor = Me._offColor
            Else
                Me.FlashColor = Me._onColor
            End If
        ElseIf (Me._flashColorsArray.Length <= tickIndex) OrElse (Me._flashColorsArray(tickIndex).Equals(Color.Empty)) Then
            Me.FlashColor = Me._offColor
        Else
            Me.FlashColor = Me._flashColorsArray(tickIndex)
        End If
    End Sub

#End Region

End Class

Imports isr.Core.Pith.EventHandlerExtensions
Imports isr.Core.Pith.ExceptionExtensions
''' <summary> Defines a simple LED display with a caption. </summary>
''' <license> (c) 2006 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/05/2006" by="David" revision="2.0.2439.x"> created. </history>
<System.ComponentModel.Description("Squared Led Control")>
Public Class SquareLed
    Inherits Pith.PropertyNotifyControlBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Pith.PropertyNotifyControlBase and
    ''' optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveClickEvent(Me.ClickEvent)
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " APPEARANCE "

    ''' <summary> The background color. </summary>
    Private _BackgroundColor As Integer

    ''' <summary> Gets or sets the color of the background. </summary>
    ''' <value> The color of the background. </value>
    <System.ComponentModel.Browsable(True),
      System.ComponentModel.Category("Appearance"),
      System.ComponentModel.Description("Background Color.")>
    Public Property BackgroundColor() As Integer
        Get
            Return Me._backgroundColor
        End Get
        Set(ByVal Value As Integer)
            If Value <> Me.BackgroundColor Then
                Me._backgroundColor = Value
                Me.BackColor = System.Drawing.ColorTranslator.FromOle(Value)
            End If
        End Set
    End Property

    ''' <summary> The caption. </summary>
    Private _Caption As String

    ''' <summary> Gets or sets the caption. </summary>
    ''' <value> The caption. </value>
    <System.ComponentModel.Browsable(True),
      System.ComponentModel.Category("Appearance"),
      System.ComponentModel.Description("Caption.")>
    Public Property Caption() As String
        Get
            Return Me._caption
        End Get
        Set(ByVal Value As String)
            If Value <> Me.Caption Then
                Me._caption = Value
                Me.captionLabel.Text = Caption
            End If
        End Set
    End Property

    ''' <summary> The indicator color. </summary>
    Private _IndicatorColor As Integer

    ''' <summary> Gets or sets the color of the indicator. </summary>
    ''' <value> The color of the indicator. </value>
    <System.ComponentModel.Browsable(True),
      System.ComponentModel.Category("Appearance"),
      System.ComponentModel.Description("Indicator color.")>
    Public Property IndicatorColor() As Integer
        Get
            Return Me._indicatorColor
        End Get
        Set(ByVal Value As Integer)
            If Value <> Me.IndicatorColor Then
                Me._indicatorColor = Value
                Me.indicatorLabel.BackColor = System.Drawing.ColorTranslator.FromOle(Me._indicatorColor)
            End If
        End Set
    End Property

    ''' <summary> The state. </summary>
    Private _State As IndicatorState

    ''' <summary> Gets or sets the is on. </summary>
    ''' <value> The is on. </value>
    <System.ComponentModel.Browsable(True),
      System.ComponentModel.Category("Appearance"),
      System.ComponentModel.Description("Is On.")>
    Public Property IsOn() As Boolean
        Get
            Return Me._state = IndicatorState.On
        End Get
        Set(ByVal Value As Boolean)
            If Value <> Me.IsOn Then
                If Value Then
                    Me.State = IndicatorState.On
                Else
                    Me.State = IndicatorState.Off
                End If
            End If
        End Set
    End Property

    ''' <summary> The no color. </summary>
    Private _NoColor As Integer

    ''' <summary> Gets or sets the color of the no. </summary>
    ''' <value> The color of the no. </value>
    <System.ComponentModel.Browsable(True),
      System.ComponentModel.Category("Appearance"),
      System.ComponentModel.Description("No Color.")>
    Public Property NoColor() As Integer
        Get
            Return Me._noColor
        End Get
        Set(ByVal Value As Integer)
            Me._noColor = Value
        End Set
    End Property

    ''' <summary> The off color. </summary>
    Private _OffColor As Integer

    ''' <summary> Gets or sets the color of the off. </summary>
    ''' <value> The color of the off. </value>
    <System.ComponentModel.Browsable(True),
      System.ComponentModel.Category("Appearance"),
      System.ComponentModel.Description("Off Color.")>
    Public Property OffColor() As Integer
        Get
            Return Me._offColor
        End Get
        Set(ByVal Value As Integer)
            Me._offColor = Value
        End Set
    End Property

    ''' <summary> The on color. </summary>
    Private _OnColor As Integer

    ''' <summary> Gets or sets the color of the on. </summary>
    ''' <value> The color of the on. </value>
    <System.ComponentModel.Browsable(True),
      System.ComponentModel.Category("Appearance"),
      System.ComponentModel.Description("On Color.")>
    Public Property OnColor() As Integer
        Get
            Return Me._onColor
        End Get
        Set(ByVal Value As Integer)
            If Value <> Me.OnColor Then
                Me._onColor = Value
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the indicator color based on the state. </summary>
    ''' <value> The state. </value>
    <System.ComponentModel.Browsable(True),
      System.ComponentModel.Category("Appearance"),
      System.ComponentModel.Description("Current State.")>
    Public Property State() As IndicatorState
        Get
            Return Me._state
        End Get
        Set(ByVal Value As IndicatorState)
            If Value <> Me.State Then
                Me._state = Value
                If Me.Visible Then
                    Select Case True
                        Case Value = IndicatorState.None
                            Me.IndicatorColor = Me.NoColor
                        Case Value = IndicatorState.On
                            Me.IndicatorColor = Me.OnColor
                        Case Value = IndicatorState.Off
                            Me.IndicatorColor = Me.OffColor
                    End Select
                End If
            End If

        End Set
    End Property

#End Region

#Region " EVENTS HANDLERS "

    ''' <summary>Occurs when the user clicks the indicator.
    ''' </summary>
    Public Shadows Event Click As EventHandler(Of EventArgs)

    ''' <summary> Removes click event. </summary>
    ''' <param name="value"> The value. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveClickEvent(ByVal value As EventHandler(Of System.EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.Click, CType(d, EventHandler(Of System.EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the click event. </summary>
    ''' <param name="eventSender"> The source of the event. </param>
    ''' <param name="e">           The <see cref="System.EventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub IndicatorLabel_Click(ByVal eventSender As System.Object, ByVal e As System.EventArgs) Handles indicatorLabel.Click

        Dim evt As EventHandler(Of EventArgs) = Me.ClickEvent
        evt?.Invoke(Me, e)

    End Sub

    ''' <summary> Position the controls. </summary>
    ''' <param name="eventSender"> The source of the event. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub SquareLed_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize

        captionLabel.SetBounds((Width - captionLabel.Width) \ 2, 0, Width, captionLabel.Height, System.Windows.Forms.BoundsSpecified.All)
        indicatorLabel.SetBounds((Width - indicatorLabel.Width) \ 2, Height - indicatorLabel.Height, 0, 0, System.Windows.Forms.BoundsSpecified.X Or System.Windows.Forms.BoundsSpecified.Y)

    End Sub

#End Region

End Class

''' <summary> Enumerates the indicator states. </summary>
Public Enum IndicatorState
    ''' <summary> . </summary>
    <System.ComponentModel.Description("None")> None
    ''' <summary> . </summary>
    <System.ComponentModel.Description("On")> [On]
    ''' <summary> . </summary>
    <System.ComponentModel.Description("Off")> [Off]
End Enum


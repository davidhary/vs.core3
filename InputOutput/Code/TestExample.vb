﻿Imports System.Runtime.InteropServices

Namespace KnownFolderPaths

	''' <summary>
	''' Main class of the program containing the application entry point.
	''' </summary>
	Friend Class Program
        Public Shared Sub Main()
            ' Go through each KnownFolder enum entry
            For Each knownFolder As KnownFolder In System.Enum.GetValues(GetType(KnownFolder))
                ' Write down the current and default path
                Console.WriteLine(knownFolder.ToString())
                Try
                    Console.Write("Current Path: ")
                    Console.WriteLine(KnownFolders.GetPath(knownFolder))
                    Console.Write("Default Path: ")
                    Console.WriteLine(KnownFolders.GetDefaultPath(knownFolder))
                Catch ex As ExternalException
                    ' While uncommon with personal folders, other KnownFolders don't exist on every
                    ' system, and trying to query those returns an error which we catch here
                    Console.WriteLine("<Exception> " & ex.ErrorCode)
                End Try
                Console.WriteLine()
            Next knownFolder
            Console.ReadLine()
        End Sub
    End Class
End Namespace

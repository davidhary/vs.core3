﻿Imports isr.Core.Pith
Imports isr.Core.Pith.ExceptionExtensions
Namespace My

    Partial Public NotInheritable Class MyLibrary

        Private Sub New()
            MyBase.New
        End Sub

        ' ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.Pith.My.ProjectTraceEventId.InputOutput

        Public Const AssemblyTitle As String = "Core Input Output Library"
        Public Const AssemblyDescription As String = "Core Input Output Library"
        Public Const AssemblyProduct As String = "Core.InputOutput.2018"

    End Class

End Namespace

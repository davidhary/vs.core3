﻿Imports Microsoft.SqlServer.MessageBox
Imports System.Windows.Forms
Imports System.Runtime.CompilerServices
Namespace ExceptionMessageBoxExtensions
    ''' <summary> Extends the <see cref="ExceptionMessageBox">Exception message dialog</see>. </summary>
    ''' <license> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    ''' SOFTWARE.</para> </license>
    ''' <history date="02/14/2014" by="David" revision="2.0.5158.x"> Exception Message. </history>
    Public Module Methods

        ''' <summary> Synchronously Invokes the exception display on the apartment thread to permit using
        ''' the clipboard. </summary>
        ''' <remarks> This method ensures that the user can copy the messaged displayed using the
        ''' <see cref="ExceptionMessageBox">Exception Message Box</see>. The copy uses the Clipboard COM
        ''' object using OLE, which can only run on a Single Apartment Thread (STA). If, otherwise, the
        ''' Exception Message Box is opened from the cross thread, the following error occurs:<code>
        ''' 'This message cannot be copied to the clipboard. (mscorlib). Current thread must be set to
        ''' single thread apartment (STA) mode before OLE calls can be made. Ensure that your Main
        ''' function has STAThreadAttribute marked on it. (System.Windows.Forms)
        ''' at System.Windows.Forms.Clipboard.SetDataObject(Object data, Boolean copy, Int32 retryTimes,
        ''' Int32 retryDelay)
        ''' at System.Windows.Forms.Clipboard.SetDataObject(Object data, Boolean copy)
        ''' at Microsoft.SqlServer.MessageBox.ExceptionMessageBoxForm.CopyToClipboard()
        ''' </code>
        ''' A more elegant solution would be to run the Copy To Clipboard method on the apartment thread. 
        ''' <example> 
        ''' Example 1: Simple Message
        '''           <code>
        ''' Dim box As ExceptionMessageBox = New ExceptionMessageBox(message)
        ''' box.InvokeShow(owner)
        '''       </code></example>
        ''' <example> 
        ''' Example 2: Exception, buttons, symbol and default button.
        '''           <code>
        ''' Dim box As ExceptionMessageBox = New ExceptionMessageBox(exception, ExceptionMessageBoxButtons.AbortRetryIgnore,
        '''                                                          ExceptionMessageBoxSymbol.Asterisk, 
        '''                                                          ExceptionMessageBoxDefaultButton.Button1)
        ''' box.InvokeShow(owner)
        '''       </code></example>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="box">   The <see cref="ExceptionMessageBox">exception message box</see>. </param>
        ''' <param name="owner"> The owner. </param>
        ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
        <Extension()>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Function ShowDialog(ByVal box As ExceptionMessageBox, ByVal owner As IWin32Window) As DialogResult
            If box Is Nothing Then
                Throw New ArgumentNullException(NameOf(box))
            End If
            Dim r As DialogResult = DialogResult.Abort
            Dim oThread As Threading.Thread
            ' this was added from the custom calls in the legacy code.
            If owner Is Nothing Then
                Using newOwner As TextBox = New TextBox
                    Return Methods.ShowDialog(box, newOwner)
                End Using
            End If
            If owner Is Nothing OrElse TypeOf owner Is Form Then
                oThread = New Threading.Thread(New Threading.ParameterizedThreadStart(Sub() r = box.Show(Nothing)))
            Else
                oThread = New Threading.Thread(New Threading.ParameterizedThreadStart(Sub() r = box.Show(owner)))
            End If
            oThread.SetApartmentState(Threading.ApartmentState.STA)
            Try
                oThread.Start()
                oThread.Join()
                Do While oThread.IsAlive
                    Threading.Thread.Sleep(1000)
                Loop
            Catch ex As Exception
                Dim errorMessage As System.Text.StringBuilder = New System.Text.StringBuilder()
                If box Is Nothing OrElse box.Message Is Nothing Then
                    errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                              "Exception occurred attempting to display an application exception")
                Else
                    errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                              "Exception occurred attempting to display the following application exception: {0}{1}", Environment.NewLine,
                                              box.Message)
                End If
                errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                          "{0}The following error occurred:{0}{1}", Environment.NewLine, ex.ToString)
                errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                          "{0}{0}Click Abort to exit application. Otherwise, the application will continue.", Environment.NewLine)
                r = MessageBox.Show(errorMessage.ToString(), "Application Error",
                                                  MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1,
                                                  MessageBoxOptions.DefaultDesktopOnly)

            End Try
            Return r
        End Function

        ''' <summary> Displays the message box. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <remarks>
        ''' <example> 
        ''' Example 1: Simple Message
        '''           <code>
        ''' Dim box As ExceptionMessageBox = New ExceptionMessageBox(exception)
        ''' box.InvokeShow(owner, New String(){"A","B"},ExceptionMessageBoxSymbol.Asterisk,
        '''                                             ExceptionMessageBoxDefaultButton.Button1)
        '''       </code></example>
        '''          </remarks>
        ''' <param name="box">   The <see cref="ExceptionMessageBox">exception message box</see>. </param>
        ''' <param name="owner"> The owner. </param>
        ''' <param name="buttonText">    The buttons text. </param>
        ''' <param name="symbol">         The symbol. </param>
        ''' <param name="defaultButton">  The default button. </param>
        ''' <returns> <see cref="ExceptionMessageBoxDialogResult">custom dialog result</see> if okay,
        ''' otherwise,
        ''' <see cref="ExceptionMessageBoxDialogResult.None"></see> </returns>
        <Extension()>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Function ShowDialog(ByVal box As ExceptionMessageBox, ByVal owner As IWin32Window,
                                   ByVal buttonText As String(), ByVal symbol As ExceptionMessageBoxSymbol,
                                   ByVal defaultButton As ExceptionMessageBoxDefaultButton) As ExceptionMessageBoxDialogResult

            If box Is Nothing Then
                Throw New ArgumentNullException(NameOf(box))
            ElseIf buttonText Is Nothing Then
                Throw New ArgumentNullException(NameOf(buttonText))
            ElseIf buttonText.Count = 0 OrElse buttonText.Count > 5 Then
                Throw New ArgumentOutOfRangeException("buttonText", "Must have between 1 and 5 values")
            End If

            ' Set the names of the custom buttons.
            Select Case buttonText.Count
                Case 1
                    box.SetButtonText(buttonText(0))
                Case 2
                    box.SetButtonText(buttonText(0), buttonText(1))
                Case 3
                    box.SetButtonText(buttonText(0), buttonText(1), buttonText(2))
                Case 4
                    box.SetButtonText(buttonText(0), buttonText(1), buttonText(2), buttonText(3))
                Case 5
                    box.SetButtonText(buttonText(0), buttonText(1), buttonText(2), buttonText(4))
            End Select
            box.DefaultButton = defaultButton
            box.Symbol = symbol
            box.Buttons = ExceptionMessageBoxButtons.Custom
            box.ShowDialog(owner)
            Return box.CustomDialogResult

        End Function

        ''' <summary> Displays the message box. </summary>
        ''' <param name="box">           The <see cref="ExceptionMessageBox">exception message box</see>. </param>
        ''' <param name="owner">         The owner. </param>
        ''' <param name="buttonText">    The buttons text. </param>
        ''' <param name="symbol">        The symbol. </param>
        ''' <param name="defaultButton"> The default button. </param>
        ''' <param name="dialogResults"> The dialog results corresponding to the
        ''' <paramref name="buttonText">buttons</paramref>. </param>
        ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
        ''' <example> Example 1: Simple Message
        ''' <code>
        ''' Dim box As ExceptionMessageBox = New ExceptionMessageBox(exception)
        ''' box.InvokeShow(owner, New String(){"A","B"},ExceptionMessageBoxSymbol.Asterisk,
        '''                                   ExceptionMessageBoxDefaultButton.Button1)
        ''' </code></example>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required
        ''' arguments are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are
        ''' outside the required range. </exception>
        <Extension()>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Function ShowDialog(ByVal box As ExceptionMessageBox, ByVal owner As IWin32Window,
                                   ByVal buttonText As String(), ByVal symbol As ExceptionMessageBoxSymbol,
                                   ByVal defaultButton As ExceptionMessageBoxDefaultButton,
                                   ByVal dialogResults As DialogResult()) As DialogResult

            If box Is Nothing Then
                Throw New ArgumentNullException(NameOf(box))
            ElseIf buttonText Is Nothing Then
                Throw New ArgumentNullException(NameOf(buttonText))
            ElseIf buttonText.Count = 0 OrElse buttonText.Count > 5 Then
                Throw New ArgumentOutOfRangeException("buttonText", "Must have between 1 and 5 values")
            ElseIf dialogResults Is Nothing Then
                Throw New ArgumentNullException(NameOf(dialogResults))
            ElseIf dialogResults.Count = 0 OrElse dialogResults.Count > 5 Then
                Throw New ArgumentOutOfRangeException("dialogResults", "Must have between 1 and 5 values")
            ElseIf dialogResults.Count <> buttonText.Count Then
                Throw New ArgumentOutOfRangeException("dialogResults", "Must have the same count as button text")
            End If

            ' Set the names of the custom buttons.
            Select Case buttonText.Count
                Case 1
                    box.SetButtonText(buttonText(0))
                Case 2
                    box.SetButtonText(buttonText(0), buttonText(1))
                Case 3
                    box.SetButtonText(buttonText(0), buttonText(1), buttonText(2))
                Case 4
                    box.SetButtonText(buttonText(0), buttonText(1), buttonText(2), buttonText(3))
                Case 5
                    box.SetButtonText(buttonText(0), buttonText(1), buttonText(2), buttonText(4))
            End Select
            box.DefaultButton = defaultButton
            box.Symbol = symbol
            box.Buttons = ExceptionMessageBoxButtons.Custom
            box.ShowDialog(owner)
            Select Case box.CustomDialogResult
                Case ExceptionMessageBoxDialogResult.Button1
                    Return dialogResults(0)
                Case ExceptionMessageBoxDialogResult.Button2
                    Return dialogResults(1)
                Case ExceptionMessageBoxDialogResult.Button3
                    Return dialogResults(2)
                Case ExceptionMessageBoxDialogResult.Button4
                    Return dialogResults(3)
                Case ExceptionMessageBoxDialogResult.Button5
                    Return dialogResults(4)
                Case Else
                    Return MessageBox.Show("Failed displaying the message box. Click Abort to exit application. Otherwise, the application will continue.",
                                                         "Application Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1,
                                                         MessageBoxOptions.DefaultDesktopOnly)
            End Select

        End Function

        ''' <summary> Displays the message box. </summary>
        ''' <param name="box">   The <see cref="ExceptionMessageBox">exception message box</see>. </param>
        ''' <param name="owner"> The owner. </param>
        ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
        <Extension()>
        Public Function ShowDialogAbortIgnore(ByVal box As ExceptionMessageBox, ByVal owner As IWin32Window) As DialogResult
            Return box.ShowDialog(owner, New String() {"Abort", "Ignore"}, ExceptionMessageBoxSymbol.Stop,
                               ExceptionMessageBoxDefaultButton.Button1, New DialogResult() {DialogResult.Abort, DialogResult.Ignore})
        End Function

    End Module


End Namespace

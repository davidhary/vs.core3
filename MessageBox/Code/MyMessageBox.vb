﻿Imports Microsoft.SqlServer.MessageBox
Imports System.Windows.Forms
''' <summary> Extends the <see cref="ExceptionMessageBox">Exception message dialog</see>. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="02/14/2014" by="David" revision="2.0.5158.x"> Exception Message. </history>
Public Class MyMessageBox

#Region " CONSTRUCTION "

    Private _ExceptionMessageBox As MyExceptionMessageBox

    ''' <summary> Constructor. </summary>
    ''' <param name="exception"> The exception. </param>
    Public Sub New(ByVal exception As Exception)
        MyBase.New()
        Me._ExceptionMessageBox = New MyExceptionMessageBox(exception)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    Public Sub New(ByVal text As String, ByVal caption As String)
        MyBase.New()
        Me._ExceptionMessageBox = New MyExceptionMessageBox(text, caption)
    End Sub

#End Region

#Region " SHARED "

    ''' <summary> Synchronously Invokes the exception display on the apartment thread to permit using
    ''' the clipboard. </summary>
    ''' <param name="owner">     The owner. </param>
    ''' <param name="exception"> The exception. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialog(ByVal owner As IWin32Window, ByVal exception As Exception) As DialogResult
        Dim box As New MyExceptionMessageBox(exception)
        Return box.ShowDialog(owner)
    End Function

    ''' <summary> Synchronously Invokes the exception display on the apartment thread to permit using
    ''' the clipboard. </summary>
    ''' <param name="owner">   The owner. </param>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialog(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String) As DialogResult
        Dim box As New MyExceptionMessageBox(text, caption)
        Return box.ShowDialog(owner)
    End Function

    ''' <summary> Synchronously Invokes the exception display on the apartment thread to permit using
    ''' the clipboard. </summary>
    ''' <param name="owner">         The owner. </param>
    ''' <param name="exception">     The exception. </param>
    ''' <param name="icon">          The icon. </param>
    ''' <param name="dialogResults"> The dialog results. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialog(ByVal owner As IWin32Window, ByVal exception As Exception, ByVal icon As MessageBoxIcon,
                                      ByVal dialogResults() As DialogResult) As DialogResult
        Dim box As New MyExceptionMessageBox(exception)
        Return box.ShowDialog(owner, MyExceptionMessageBox.ToSymbol(icon), dialogResults)
    End Function

    ''' <summary> Synchronously Invokes the exception display on the apartment thread to permit using
    ''' the clipboard. </summary>
    ''' <param name="owner">         The owner. </param>
    ''' <param name="text">          The exception. </param>
    ''' <param name="caption">       The caption. </param>
    ''' <param name="icon">          The icon. </param>
    ''' <param name="dialogResults"> The dialog results. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialog(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String,
                                      ByVal icon As MessageBoxIcon, ByVal dialogResults() As DialogResult) As DialogResult
        Dim box As New MyExceptionMessageBox(text, caption)
        Return box.ShowDialog(owner, MyExceptionMessageBox.ToSymbol(icon), dialogResults)
    End Function


    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialogAbortIgnore(ByVal owner As IWin32Window, ByVal exception As Exception, ByVal icon As MessageBoxIcon) As DialogResult
        Return MyMessageBox.ShowDialog(owner, exception, icon, New DialogResult() {DialogResult.Abort, DialogResult.Ignore})
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialogAbortIgnore(ByVal owner As IWin32Window, ByVal exception As Exception) As DialogResult
        Return MyMessageBox.ShowDialog(owner, exception, MessageBoxIcon.Stop, New DialogResult() {DialogResult.Abort, DialogResult.Ignore})
    End Function


    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner">   The owner. </param>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialogAbortIgnore(ByVal owner As IWin32Window, ByVal text As String,
                                                 ByVal caption As String, ByVal icon As MessageBoxIcon) As DialogResult
        Return MyMessageBox.ShowDialog(owner, text, caption, icon, New DialogResult() {DialogResult.Abort, DialogResult.Ignore})
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> Either <see cref="DialogResult.Ignore">ignore</see> or <see cref="DialogResult.OK">Okay</see>. </returns>
    Public Shared Function ShowDialogIgnoreExit(ByVal owner As IWin32Window, ByVal exception As Exception, ByVal icon As MessageBoxIcon) As DialogResult
        Dim box As New MyMessageBox(exception)
        Return box.ShowDialogIgnoreExit(owner, icon)
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> Either <see cref="DialogResult.Ignore">ignore</see> or <see cref="DialogResult.OK">Okay</see>. </returns>
    Public Shared Function ShowDialogIgnoreExit(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String, ByVal icon As MessageBoxIcon) As DialogResult
        Dim box As New MyMessageBox(text, caption)
        Return box.ShowDialogIgnoreExit(owner, icon)
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> <see cref="DialogResult.OK">Okay</see>. </returns>
    Public Shared Function ShowDialogExit(ByVal owner As IWin32Window, ByVal exception As Exception, ByVal icon As MessageBoxIcon) As DialogResult
        Dim box As New MyMessageBox(exception)
        Return box.ShowDialogExit(owner, icon)
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> <see cref="DialogResult.OK">Okay</see>. </returns>
    Public Shared Function ShowDialogExit(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String, ByVal icon As MessageBoxIcon) As DialogResult
        Dim box As New MyMessageBox(text, caption)
        Return box.ShowDialogExit(owner, icon)
    End Function

    ''' <summary> Shows the dialog okay. </summary>
    ''' <param name="owner">   The owner. </param>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogOkay(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String, ByVal icon As MessageBoxIcon) As DialogResult
        Dim box As New MyMessageBox(text, caption)
        Return box.ShowDialogExit(owner, icon)
    End Function

    ''' <summary> Shows the dialog okay. </summary>
    ''' <param name="owner">   The owner. </param>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogOkay(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String) As DialogResult
        Return MyMessageBox.ShowDialogOkay(owner, text, caption, MessageBoxIcon.Information)
    End Function

    ''' <summary> Shows the dialog okay cancel. </summary>
    ''' <param name="owner">   The owner. </param>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogOkayCancel(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String,
                                                ByVal icon As MessageBoxIcon) As DialogResult
        Return MyMessageBox.ShowDialog(owner, text, caption, icon, New DialogResult() {DialogResult.OK, DialogResult.Cancel})
    End Function

    ''' <summary> Shows the dialog okay cancel. </summary>
    ''' <param name="owner">   The owner. </param>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogOkayCancel(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String) As DialogResult
        Return MyMessageBox.ShowDialogOkayCancel(owner, text, caption, MessageBoxIcon.Information)
    End Function


#End Region

#Region " SHOW DIALOG "

    ''' <summary> Shows the exception display. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    ''' <example> Example 1: Simple Message <code>
    ''' Dim box As MyMessageBox = New MyMessageBox(message)
    ''' me.ShowDialog(owner)      </code></example>
    ''' <example> Example 2: Message box with exception message. <code>
    ''' Dim box As MyMessageBox = New MyMessageBox(exception)
    ''' me.ShowDialog(owner)      </code></example>
    '''
    ''' ### <exception cref="ArgumentNullException"> Thrown when one or more required arguments are
    ''' null. </exception>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function ShowDialog(ByVal owner As IWin32Window) As DialogResult

        Dim r As DialogResult = DialogResult.Abort
        Try
            If owner Is Nothing OrElse TypeOf owner Is Form Then
                r = Me._ExceptionMessageBox.Show(Nothing)
            Else
                r = Me._ExceptionMessageBox.Show(owner)
            End If
        Catch ex As Exception

            Dim builder As System.Text.StringBuilder = New System.Text.StringBuilder()
            If Me._ExceptionMessageBox.Message Is Nothing Then
                builder.AppendLine("Exception occurred attempting to display an application exception")
            Else
                builder.AppendLine($"{ex.Message} occurred attempting to display the following application exception:")
                builder.AppendLine(Me._ExceptionMessageBox.Message.ToFullBlownString)
            End If
            builder.AppendLine($"The following error occurred:")
            builder.AppendLine(ex.ToFullBlownString)
            builder.AppendLine()
            builder.AppendLine($"Click Abort to exit application. Otherwise, the application will continue.")
            r = MessageBox.Show(builder.ToString(), "Application Error",
                                              MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1,
                                              MessageBoxOptions.DefaultDesktopOnly)
        End Try
        Return r
    End Function

#End Region

#Region " SHOW DIALOG - PREDEFINED BUTTONS "

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    Public Function ShowDialogAbortIgnore(ByVal owner As IWin32Window, ByVal icon As MessageBoxIcon) As DialogResult
        Return Me._ExceptionMessageBox.ShowDialog(owner, MyExceptionMessageBox.ToSymbol(icon),
                                                  New DialogResult() {DialogResult.Abort, DialogResult.Ignore})
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> Either <see cref="DialogResult.Ignore">ignore</see> or <see cref="DialogResult.OK">Okay</see>. </returns>
    Public Function ShowDialogIgnoreExit(ByVal owner As IWin32Window, ByVal icon As MessageBoxIcon) As DialogResult
        Return Me._ExceptionMessageBox.ShowDialog(owner, New String() {"Ignore", "Exit"}, MyExceptionMessageBox.ToSymbol(icon),
                                                  ExceptionMessageBoxDefaultButton.Button1, New DialogResult() {DialogResult.Ignore, DialogResult.OK})
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> <see cref="DialogResult.OK">Okay</see>. </returns>
    Public Function ShowDialogExit(ByVal owner As IWin32Window, ByVal icon As MessageBoxIcon) As DialogResult
        Return Me._ExceptionMessageBox.ShowDialog(owner, New String() {"Exit"}, MyExceptionMessageBox.ToSymbol(icon),
                                                  ExceptionMessageBoxDefaultButton.Button1, New DialogResult() {DialogResult.OK})
    End Function

#End Region

End Class

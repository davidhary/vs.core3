﻿Imports System.Text

''' <summary> Includes extensions for <see cref="Exception">Exceptions</see>. </summary>
''' <license> (c) 2017 Marco Bertschi.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/30/2017" by="David" revision="3.1.6298.x">
''' https://www.codeproject.com/script/Membership/View.aspx?mid=8888914
''' https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc
''' </history>
Friend Module Methods

    ''' <summary> Converts a value to a full blown string. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a String. </returns>
    <System.Runtime.CompilerServices.Extension>
    Public Function ToFullBlownString(ByVal value As System.Exception) As String
        Return Methods.ToFullBlownString(value, Integer.MaxValue)
    End Function

    ''' <summary> Converts this object to a full blown string. </summary>
    ''' <param name="value"> The value. </param>
    ''' <param name="level"> The level. </param>
    ''' <returns> The given data converted to a String. </returns>
    <System.Runtime.CompilerServices.Extension>
    Public Function ToFullBlownString(ByVal value As System.Exception, ByVal level As Integer) As String
        Dim builder As New StringBuilder()
        Dim counter As Integer = 1
        Do While value IsNot Nothing AndAlso counter <= level
            builder.AppendLine($"{counter}-> Level: {counter}")
            builder.AppendLine($"{counter}-> Message: {value.Message}")
            builder.AppendLine($"{counter}-> Source: {value.Source}")
            builder.AppendLine($"{counter}-> Target Site: {value.TargetSite}")
            builder.AppendLine($"{counter}-> Stack Trace: {value.StackTrace}")
            If value.Data IsNot Nothing Then
                For Each keyValuePair As System.Collections.DictionaryEntry In value.Data
                    builder.AppendLine($"{counter}-> {keyValuePair.Key}: {keyValuePair.Value}")
                Next
            End If
            value = value.InnerException
            counter += 1
        Loop
        Return builder.ToString()
    End Function

End Module

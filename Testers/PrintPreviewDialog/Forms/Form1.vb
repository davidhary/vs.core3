﻿''' <summary> Form 1. </summary>
''' <license> (c) 2009 Bernardo Castilho. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="8/5/2009" by="Bernardo Castilho" revision=""> 
''' http://www.codeproject.com/Articles/38758/An-Enhanced-PrintPreviewDialog. </history>
Public Class Form1

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="ShowDialog")> <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="PrintDocument")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="CoolPrintPreviewDialog")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="PrintPreviewDialog")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="ShowDialog")>
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me._CoolCodeLabel.Text = String.Format("Using dialog As CoolPrintPreviewDialog = New CoolPrintPreviewDialog{0}{1}dialog.Document = Me._PrintDocument{0}{1}dialog.ShowDialog(Me){0}End Using",
                                               Environment.NewLine, New String(" "c, 4))
        Me._StandardCodeLabel.Text = String.Format("Using dialog As PrintPreviewDialog = New PrintPreviewDialog{0}{1}dialog.Document = Me._PrintDocument{0}{1}dialog.ShowDialog(Me){0}End Using",
                                                   Environment.NewLine, New String(" "c, 4))

    End Sub


    ' fields
    Private _Font As Font = New Font("Segoe UI", 14.0!)
    Private _Page As Integer = 0
    Private _Start As DateTime

    ' show standard print preview
    Private Sub _OpenStandardPreviewDialogButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenStandardPreviewDialogButton.Click
        Using dialog As PrintPreviewDialog = New PrintPreviewDialog
            dialog.Document = Me._PrintDocument
            dialog.ShowDialog(Me)
        End Using
    End Sub

    ' show cool print preview
    Private Sub _OpenCoolPreviewDialogButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenCoolPreviewDialogButton.Click
        Using dialog As isr.Core.Controls.CoolPrintPreviewDialog = New isr.Core.Controls.CoolPrintPreviewDialog
            dialog.Document = Me._PrintDocument
            dialog.ShowDialog(Me)
        End Using
    End Sub

    ' render document
    Private Sub PrintDocument1_BeginPrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles _PrintDocument.BeginPrint
        Me._start = DateTime.Now
        Me._page = 0
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="ms")>
    Private Sub PrintDocument1_EndPrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles _PrintDocument.EndPrint
        Console.WriteLine("Document rendered in {0} ms", DateTime.Now.Subtract(Me._Start).TotalMilliseconds)
    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles _PrintDocument.PrintPage
        Dim rc As Rectangle = e.MarginBounds
        rc.Height = (Me._font.Height + 10)
        Dim i As Integer = 0
        Do While True
            Dim text As String = String.Format("line {0} on page {1}", (i + 1), (Me._page + 1))
            e.Graphics.DrawString([text], Me._font, Brushes.Black, rc)
            rc.Y = (rc.Y + rc.Height)
            If (rc.Bottom > e.MarginBounds.Bottom) Then
                Me._page += 1
                If Me._LongDocumentCheckBox.Checked Then
                    e.HasMorePages = Me._page < 3000
                Else
                    e.HasMorePages = Me._page < 30
                End If
                Return
            End If
            i += 1
        Loop
    End Sub
End Class

﻿Imports System.Threading

Partial Public Class DemoForm
    Inherits Form

    Public Sub New()
        InitializeComponent()
        SetPropertiesForStylesTabSwitches()
        SetPropertiesForPropertiesTabSwitches()
        SetPropertiesForCustomizationsTabSwitches()
    End Sub

    Public Sub SetPropertiesForStylesTabSwitches()
        'Set the properties for the ToggleSwitches on the "Styles" tab

        MetroStyleToggleSwitch.Style = ToggleSwitchStyle.Metro 'Default
        MetroStyleToggleSwitch.Size = New Size(75, 23)

        IOS5StyleToggleSwitch.Style = ToggleSwitchStyle.Ios5
        IOS5StyleToggleSwitch.Size = New Size(98, 42)
        IOS5StyleToggleSwitch.OnText = "ON"
        IOS5StyleToggleSwitch.OnFont = New Font(DemoTabControl.Font.FontFamily, 12, FontStyle.Bold)
        IOS5StyleToggleSwitch.OnForeColor = Color.White
        IOS5StyleToggleSwitch.OffText = "OFF"
        IOS5StyleToggleSwitch.OffFont = New Font(DemoTabControl.Font.FontFamily, 12, FontStyle.Bold)
        IOS5StyleToggleSwitch.OffForeColor = Color.FromArgb(141, 123, 141)

        AndroidStyleToggleSwitch.Style = ToggleSwitchStyle.Android
        AndroidStyleToggleSwitch.Size = New Size(78, 23)
        AndroidStyleToggleSwitch.OnText = "ON"
        AndroidStyleToggleSwitch.OnFont = New Font(DemoTabControl.Font.FontFamily, 8, FontStyle.Bold)
        AndroidStyleToggleSwitch.OnForeColor = Color.White
        AndroidStyleToggleSwitch.OffText = "OFF"
        AndroidStyleToggleSwitch.OffFont = New Font(DemoTabControl.Font.FontFamily, 8, FontStyle.Bold)
        AndroidStyleToggleSwitch.OffForeColor = Color.FromArgb(141, 123, 141)

        BrushedMetalStyleToggleSwitch.Style = ToggleSwitchStyle.BrushedMetal
        BrushedMetalStyleToggleSwitch.Size = New Size(93, 30)
        BrushedMetalStyleToggleSwitch.OnText = "ON"
        BrushedMetalStyleToggleSwitch.OnFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        BrushedMetalStyleToggleSwitch.OnForeColor = Color.White
        BrushedMetalStyleToggleSwitch.OffText = "OFF"
        BrushedMetalStyleToggleSwitch.OffFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        BrushedMetalStyleToggleSwitch.OffForeColor = Color.White

        IphoneStyleToggleSwitch.Style = ToggleSwitchStyle.IPhone
        IphoneStyleToggleSwitch.Size = New Size(93, 30)
        IphoneStyleToggleSwitch.OnText = "ON"
        IphoneStyleToggleSwitch.OnFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        IphoneStyleToggleSwitch.OnForeColor = Color.White
        IphoneStyleToggleSwitch.OffText = "OFF"
        IphoneStyleToggleSwitch.OffFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        IphoneStyleToggleSwitch.OffForeColor = Color.FromArgb(92, 92, 92)

        ModernStyleToggleSwitch.Style = ToggleSwitchStyle.Modern
        ModernStyleToggleSwitch.Size = New Size(85, 32)
        ModernStyleToggleSwitch.OnText = "ON"
        ModernStyleToggleSwitch.OnFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        ModernStyleToggleSwitch.OnForeColor = Color.White
        ModernStyleToggleSwitch.OffText = "OFF"
        ModernStyleToggleSwitch.OffFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        ModernStyleToggleSwitch.OffForeColor = Color.FromArgb(153, 153, 153)

        CarbonStyleToggleSwitch.Style = ToggleSwitchStyle.Carbon
        CarbonStyleToggleSwitch.Size = New Size(93, 30)
        CarbonStyleToggleSwitch.OnText = "On"
        CarbonStyleToggleSwitch.OnFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        CarbonStyleToggleSwitch.OnForeColor = Color.White
        CarbonStyleToggleSwitch.OffText = "Off"
        CarbonStyleToggleSwitch.OffFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        CarbonStyleToggleSwitch.OffForeColor = Color.White

        OSXStyleToggleSwitch.Style = ToggleSwitchStyle.OS10
        OSXStyleToggleSwitch.Size = New Size(93, 25)

        FancyStyleToggleSwitch.Style = ToggleSwitchStyle.Fancy
        FancyStyleToggleSwitch.Size = New Size(100, 30)
        FancyStyleToggleSwitch.OnText = "ON"
        FancyStyleToggleSwitch.OnFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        FancyStyleToggleSwitch.OnForeColor = Color.White
        FancyStyleToggleSwitch.OffText = "OFF"
        FancyStyleToggleSwitch.OffFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        FancyStyleToggleSwitch.OffForeColor = Color.White
        FancyStyleToggleSwitch.ButtonImage = My.Resources.handle
    End Sub

    Public Sub SetPropertiesForPropertiesTabSwitches()
        'Set the properties for the ToggleSwitches on the "(Semi)-Important Properties" tab

        'AllowUserChange example:

        AllowUserChangeToggleSwitch1.AllowUserChange = False
        AllowUserChangeToggleSwitch2.AllowUserChange = True

        'Animation example:

        NoAnimationToggleSwitch.Style = ToggleSwitchStyle.Carbon 'Only to provide an interesting look
        NoAnimationToggleSwitch.Size = New Size(93, 30) 'Only to provide an interesting look
        NoAnimationToggleSwitch.OnText = "On" 'Only to provide an interesting look
        NoAnimationToggleSwitch.OnFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        NoAnimationToggleSwitch.OnForeColor = Color.White 'Only to provide an interesting look
        NoAnimationToggleSwitch.OffText = "Off" 'Only to provide an interesting look
        NoAnimationToggleSwitch.OffFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        NoAnimationToggleSwitch.OffForeColor = Color.White 'Only to provide an interesting look
        NoAnimationToggleSwitch.UseAnimation = False

        FastAnimationToggleSwitch.Style = ToggleSwitchStyle.Carbon 'Only to provide an interesting look
        FastAnimationToggleSwitch.Size = New Size(93, 30) 'Only to provide an interesting look
        FastAnimationToggleSwitch.OnText = "On" 'Only to provide an interesting look
        FastAnimationToggleSwitch.OnFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        FastAnimationToggleSwitch.OnForeColor = Color.White 'Only to provide an interesting look
        FastAnimationToggleSwitch.OffText = "Off" 'Only to provide an interesting look
        FastAnimationToggleSwitch.OffFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        FastAnimationToggleSwitch.OffForeColor = Color.White 'Only to provide an interesting look
        FastAnimationToggleSwitch.UseAnimation = True 'Default
        FastAnimationToggleSwitch.AnimationInterval = 1 'Default
        FastAnimationToggleSwitch.AnimationStep = 10 'Default

        SlowAnimationToggleSwitch.Style = ToggleSwitchStyle.Carbon 'Only to provide an interesting look
        SlowAnimationToggleSwitch.Size = New Size(93, 30) 'Only to provide an interesting look
        SlowAnimationToggleSwitch.OnText = "On" 'Only to provide an interesting look
        SlowAnimationToggleSwitch.OnFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        SlowAnimationToggleSwitch.OnForeColor = Color.White 'Only to provide an interesting look
        SlowAnimationToggleSwitch.OffText = "Off" 'Only to provide an interesting look
        SlowAnimationToggleSwitch.OffFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        SlowAnimationToggleSwitch.OffForeColor = Color.White 'Only to provide an interesting look
        SlowAnimationToggleSwitch.UseAnimation = True 'Default
        SlowAnimationToggleSwitch.AnimationInterval = 10
        SlowAnimationToggleSwitch.AnimationStep = 1

        'GrayWhenDisabled example:

        GrayWhenDisabledToggleSwitch1.Style = ToggleSwitchStyle.Fancy 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch1.Size = New Size(100, 30) 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch1.OnText = "ON" 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch1.OnFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch1.OnForeColor = Color.White 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch1.OffText = "OFF" 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch1.OffFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch1.OffForeColor = Color.White 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch1.ButtonImage = My.Resources.arrowright 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch1.GrayWhenDisabled = False

        GrayWhenDisabledToggleSwitch2.Style = ToggleSwitchStyle.Fancy 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch2.Size = New Size(100, 30) 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch2.OnText = "ON" 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch2.OnFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch2.OnForeColor = Color.White 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch2.OffText = "OFF" 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch2.OffFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch2.OffForeColor = Color.White 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch2.ButtonImage = My.Resources.arrowright 'Only to provide an interesting look
        GrayWhenDisabledToggleSwitch2.GrayWhenDisabled = True 'Default

        'ThresholdPercentage example:

        ThresholdPercentageToggleSwitch.Style = ToggleSwitchStyle.Ios5 'Only to provide an interesting look
        ThresholdPercentageToggleSwitch.Size = New Size(98, 42) 'Only to provide an interesting look
        ThresholdPercentageToggleSwitch.OnText = "ON" 'Only to provide an interesting look
        ThresholdPercentageToggleSwitch.OnFont = New Font(DemoTabControl.Font.FontFamily, 12, FontStyle.Bold) 'Only to provide an interesting look
        ThresholdPercentageToggleSwitch.OnForeColor = Color.White 'Only to provide an interesting look
        ThresholdPercentageToggleSwitch.OffText = "OFF" 'Only to provide an interesting look
        ThresholdPercentageToggleSwitch.OffFont = New Font(DemoTabControl.Font.FontFamily, 12, FontStyle.Bold) 'Only to provide an interesting look
        ThresholdPercentageToggleSwitch.OffForeColor = Color.FromArgb(141, 123, 141) 'Only to provide an interesting look
        ThresholdPercentageToggleSwitch.ThresholdPercentage = 50 'Default

        'ToggleOnButtonClick & ToggleOnSideClick example:

        ToggleOnClickToggleSwitch.Style = ToggleSwitchStyle.BrushedMetal 'Only to provide an interesting look
        ToggleOnClickToggleSwitch.Size = New Size(93, 30) 'Only to provide an interesting look
        ToggleOnClickToggleSwitch.OnText = "ON" 'Only to provide an interesting look
        ToggleOnClickToggleSwitch.OnFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        ToggleOnClickToggleSwitch.OnForeColor = Color.White 'Only to provide an interesting look
        ToggleOnClickToggleSwitch.OffText = "OFF" 'Only to provide an interesting look
        ToggleOnClickToggleSwitch.OffFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold) 'Only to provide an interesting look
        ToggleOnClickToggleSwitch.OffForeColor = Color.White 'Only to provide an interesting look
        ToggleOnClickToggleSwitch.ToggleOnButtonClick = True 'Default
        ToggleOnClickToggleSwitch.ToggleOnSideClick = True 'Default
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Sub SetPropertiesForCustomizationsTabSwitches()
        'Set the properties for the ToggleSwitches on the "Special Customizations" tab

        'Color customization example, Metro Style ToggleSwitch:

        NormalMetroToggleSwitch.Style = ToggleSwitchStyle.Metro 'Default
        NormalMetroToggleSwitch.Size = New Size(75, 23)

        Dim customizedMetroRenderer As New ToggleSwitchMetroRenderer() With {
            .LeftSideColor = Color.Red,
            .LeftSideColorHovered = Color.FromArgb(210, 0, 0),
            .LeftSideColorPressed = Color.FromArgb(190, 0, 0),
            .RightSideColor = Color.Yellow,
            .RightSideColorHovered = Color.FromArgb(245, 245, 0),
            .RightSideColorPressed = Color.FromArgb(235, 235, 0)}

        CustomizedMetroToggleSwitch.Style = ToggleSwitchStyle.Metro 'Default
        CustomizedMetroToggleSwitch.Size = New Size(75, 23)
        CustomizedMetroToggleSwitch.SetRenderer(customizedMetroRenderer)

        'Color customization example, IOS5 Style ToggleSwitch:

        NormalIOS5ToggleSwitch.Style = ToggleSwitchStyle.Ios5
        NormalIOS5ToggleSwitch.Size = New Size(98, 42)
        NormalIOS5ToggleSwitch.OnText = "ON"
        NormalIOS5ToggleSwitch.OnFont = New Font(DemoTabControl.Font.FontFamily, 12, FontStyle.Bold)
        NormalIOS5ToggleSwitch.OnForeColor = Color.White
        NormalIOS5ToggleSwitch.OffText = "OFF"
        NormalIOS5ToggleSwitch.OffFont = New Font(DemoTabControl.Font.FontFamily, 12, FontStyle.Bold)
        NormalIOS5ToggleSwitch.OffForeColor = Color.FromArgb(141, 123, 141)

        'Maybe not the prettiest color scheme in the world - It's just for demonstration :-)
        Dim customizedIos5Renderer As New ToggleSwitchIos5Renderer() With {
            .LeftSideUpperColor1 = Color.FromArgb(128, 0, 64),
            .LeftSideUpperColor2 = Color.FromArgb(180, 0, 90),
            .LeftSideLowerColor1 = Color.FromArgb(250, 0, 125),
            .LeftSideLowerColor2 = Color.FromArgb(255, 120, 190),
            .RightSideUpperColor1 = Color.FromArgb(0, 64, 128),
            .RightSideUpperColor2 = Color.FromArgb(0, 90, 180),
            .RightSideLowerColor1 = Color.FromArgb(0, 125, 250),
            .RightSideLowerColor2 = Color.FromArgb(120, 190, 255),
            .ButtonNormalOuterBorderColor = Color.Green,
            .ButtonNormalInnerBorderColor = Color.Green,
            .ButtonNormalSurfaceColor1 = Color.Red,
            .ButtonNormalSurfaceColor2 = Color.Red,
            .ButtonHoverOuterBorderColor = Color.Green,
            .ButtonHoverInnerBorderColor = Color.Green,
            .ButtonHoverSurfaceColor1 = Color.Red,
            .ButtonHoverSurfaceColor2 = Color.Red,
            .ButtonPressedOuterBorderColor = Color.Green,
            .ButtonPressedInnerBorderColor = Color.Green,
            .ButtonPressedSurfaceColor1 = Color.Red,
            .ButtonPressedSurfaceColor2 = Color.Red}

        CustomizedIOS5ToggleSwitch.Style = ToggleSwitchStyle.Ios5
        CustomizedIOS5ToggleSwitch.Size = New Size(98, 42)
        CustomizedIOS5ToggleSwitch.OnText = "ON"
        CustomizedIOS5ToggleSwitch.OnFont = New Font(DemoTabControl.Font.FontFamily, 12, FontStyle.Bold)
        CustomizedIOS5ToggleSwitch.OnForeColor = Color.White
        CustomizedIOS5ToggleSwitch.OffText = "OFF"
        CustomizedIOS5ToggleSwitch.OffFont = New Font(DemoTabControl.Font.FontFamily, 12, FontStyle.Bold)
        CustomizedIOS5ToggleSwitch.OffForeColor = Color.White 'OBS: Need to change this for text visibility
        CustomizedIOS5ToggleSwitch.SetRenderer(customizedIos5Renderer)

        'Image customization example, Fancy Style ToggleSwitch:

        NormalFancyToggleSwitch.Style = ToggleSwitchStyle.Fancy
        NormalFancyToggleSwitch.Size = New Size(100, 30)
        NormalFancyToggleSwitch.OnText = "ON"
        NormalFancyToggleSwitch.OnFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        NormalFancyToggleSwitch.OnForeColor = Color.White
        NormalFancyToggleSwitch.OffText = "OFF"
        NormalFancyToggleSwitch.OffFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        NormalFancyToggleSwitch.OffForeColor = Color.White

        CustomizedFancyToggleSwitch.Style = ToggleSwitchStyle.Fancy
        CustomizedFancyToggleSwitch.Size = New Size(100, 30)
        CustomizedFancyToggleSwitch.OffButtonImage = My.Resources.Arrowright
        CustomizedFancyToggleSwitch.OffSideImage = My.Resources.Cross
        CustomizedFancyToggleSwitch.OnButtonImage = My.Resources.Arrowleft
        CustomizedFancyToggleSwitch.OnSideImage = My.Resources.Check

        'Advanced behavior example, Fancy Style ToggleSwitch:

        Dim tempColor As Color

        Dim customizedFancyRenderer As New ToggleSwitchFancyRenderer()
        tempColor = customizedFancyRenderer.LeftSideBackColor1
        customizedFancyRenderer.LeftSideBackColor1 = customizedFancyRenderer.RightSideBackColor1
        customizedFancyRenderer.RightSideBackColor1 = tempColor
        tempColor = customizedFancyRenderer.LeftSideBackColor2
        customizedFancyRenderer.LeftSideBackColor2 = customizedFancyRenderer.RightSideBackColor2
        customizedFancyRenderer.RightSideBackColor2 = tempColor

        AdvancedBehaviorFancyToggleSwitch.Style = ToggleSwitchStyle.Fancy
        AdvancedBehaviorFancyToggleSwitch.Size = New Size(150, 30)
        AdvancedBehaviorFancyToggleSwitch.OnText = "Restart"
        AdvancedBehaviorFancyToggleSwitch.OnFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        AdvancedBehaviorFancyToggleSwitch.OnForeColor = Color.White
        AdvancedBehaviorFancyToggleSwitch.OffText = "Online"
        AdvancedBehaviorFancyToggleSwitch.OffFont = New Font(DemoTabControl.Font.FontFamily, 10, FontStyle.Bold)
        AdvancedBehaviorFancyToggleSwitch.OffForeColor = Color.White
        AdvancedBehaviorFancyToggleSwitch.OffButtonImage = My.Resources.Arrowright
        AdvancedBehaviorFancyToggleSwitch.UseAnimation = False
        AdvancedBehaviorFancyToggleSwitch.SetRenderer(customizedFancyRenderer)
        AddHandler AdvancedBehaviorFancyToggleSwitch.CheckedChanged, AddressOf AdvancedBehaviorFancyToggleSwitch_CheckedChanged

        AnimatedGifPictureBox.Parent = AdvancedBehaviorFancyToggleSwitch 'Necessary to get the ToggleSwitch button to show through the picture box' transparent background
    End Sub

    Private Sub AllowUserChangeCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AllowUserChangeCheckBox.CheckedChanged
        AllowUserChangeToggleSwitch1.Checked = AllowUserChangeCheckBox.Checked
        AllowUserChangeToggleSwitch2.Checked = AllowUserChangeCheckBox.Checked
    End Sub

    Private Sub GrayWhenDisabledCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GrayWhenDisabledCheckBox.CheckedChanged
        GrayWhenDisabledToggleSwitch1.Enabled = GrayWhenDisabledCheckBox.Checked
        GrayWhenDisabledToggleSwitch2.Enabled = GrayWhenDisabledCheckBox.Checked
    End Sub

    Private Sub ThresholdPercentageTrackBar_Scroll(ByVal sender As Object, ByVal e As System.EventArgs) Handles ThresholdPercentageTrackBar.Scroll
        label15.Text = String.Format("Value = {0} (Default = 50)", ThresholdPercentageTrackBar.Value)
        ThresholdPercentageToggleSwitch.ThresholdPercentage = ThresholdPercentageTrackBar.Value
    End Sub

    Private Sub ToggleOnButtonClickCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToggleOnButtonClickCheckBox.CheckedChanged
        ToggleOnClickToggleSwitch.ToggleOnButtonClick = ToggleOnButtonClickCheckBox.Checked
    End Sub

    Private Sub ToggleOnSideClickCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToggleOnSideClickCheckBox.CheckedChanged
        ToggleOnClickToggleSwitch.ToggleOnSideClick = ToggleOnSideClickCheckBox.Checked
    End Sub

    Private Sub AdvancedBehaviorFancyToggleSwitch_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If AdvancedBehaviorFancyToggleSwitch.Checked Then
            AdvancedBehaviorFancyToggleSwitch.AllowUserChange = False
            AdvancedBehaviorFancyToggleSwitch.OnText = "Restarting..."

            PositionAniGifPictureBox()
            AnimatedGifPictureBox.Visible = True

            If Not SimulateRestartBackgroundWorker.IsBusy Then
                SimulateRestartBackgroundWorker.RunWorkerAsync()
            End If
        Else
            AdvancedBehaviorFancyToggleSwitch.AllowUserChange = True
            AdvancedBehaviorFancyToggleSwitch.OnText = "Restart"
        End If
    End Sub

    Private Sub PositionAniGifPictureBox()
        'Position anigif picturebox

        Dim buttonRectangle As Rectangle = AdvancedBehaviorFancyToggleSwitch.ButtonRectangle

        AnimatedGifPictureBox.Height = buttonRectangle.Height - 2
        AnimatedGifPictureBox.Width = AnimatedGifPictureBox.Height
        AnimatedGifPictureBox.Left = buttonRectangle.X + ((buttonRectangle.Width - AnimatedGifPictureBox.Width) \ 2)
        AnimatedGifPictureBox.Top = buttonRectangle.Y + ((buttonRectangle.Height - AnimatedGifPictureBox.Height) \ 2)
    End Sub

    Private Sub SimulateRestartBackgroundWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles SimulateRestartBackgroundWorker.DoWork
        'Simulate restart delay
        Thread.Sleep(1500)
    End Sub

    Private Sub SimulateRestartBackgroundWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles SimulateRestartBackgroundWorker.RunWorkerCompleted
        AnimatedGifPictureBox.Visible = False
        AdvancedBehaviorFancyToggleSwitch.Checked = False
    End Sub
End Class

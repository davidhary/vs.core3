Imports System.Windows.Forms
Partial Public Class Form1
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me.imageButton1 = New isr.Core.Controls.ImageButton()
        Me.imageButton3 = New isr.Core.Controls.ImageButton()
        Me.imageButton2 = New isr.Core.Controls.ImageButton()
        Me.label1 = New System.Windows.Forms.Label()
        Me.imageButton4 = New isr.Core.Controls.ImageButton()
        Me.imageButton5 = New isr.Core.Controls.ImageButton()
        Me.imageButton6 = New isr.Core.Controls.ImageButton()
        Me.imageButton7 = New isr.Core.Controls.ImageButton()
        Me.label2 = New System.Windows.Forms.Label()
        Me.TraceMessageToolStrip1 = New isr.Core.Pith.TraceMessageToolStrip()
        CType(Me.imageButton1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imageButton3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imageButton2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imageButton4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imageButton5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imageButton6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imageButton7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'imageButton1
        '
        Me.imageButton1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.imageButton1.DownImage = Global.ImageButtonDemo.My.Resources.Resources.ExampleButtonDown
        Me.imageButton1.HoverImage = Global.ImageButtonDemo.My.Resources.Resources.ExampleButtonHover
        Me.imageButton1.Location = New System.Drawing.Point(52, 47)
        Me.imageButton1.Name = "imageButton1"
        Me.imageButton1.NormalImage = Global.ImageButtonDemo.My.Resources.Resources.ExampleButton
        Me.imageButton1.Size = New System.Drawing.Size(100, 50)
        Me.imageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.imageButton1.TabIndex = 0
        Me.imageButton1.TabStop = False
        '
        'imageButton3
        '
        Me.imageButton3.DialogResult = System.Windows.Forms.DialogResult.None
        Me.imageButton3.DownImage = Global.ImageButtonDemo.My.Resources.Resources.CUncheckedDown
        Me.imageButton3.HoverImage = Global.ImageButtonDemo.My.Resources.Resources.CUncheckedHover
        Me.imageButton3.Location = New System.Drawing.Point(90, 118)
        Me.imageButton3.Name = "imageButton3"
        Me.imageButton3.NormalImage = Global.ImageButtonDemo.My.Resources.Resources.CUncheckedNormal
        Me.imageButton3.Size = New System.Drawing.Size(20, 20)
        Me.imageButton3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.imageButton3.TabIndex = 2
        Me.imageButton3.TabStop = False
        '
        'imageButton2
        '
        Me.imageButton2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.imageButton2.DownImage = Global.ImageButtonDemo.My.Resources.Resources.ExampleButtonDownA
        Me.imageButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.imageButton2.HoverImage = Global.ImageButtonDemo.My.Resources.Resources.ExampleButtonHoverA
        Me.imageButton2.Location = New System.Drawing.Point(158, 47)
        Me.imageButton2.Name = "imageButton2"
        Me.imageButton2.NormalImage = Global.ImageButtonDemo.My.Resources.Resources.ExampleButtonA
        Me.imageButton2.Size = New System.Drawing.Size(100, 50)
        Me.imageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.imageButton2.TabIndex = 1
        Me.imageButton2.TabStop = False
        Me.imageButton2.Text = "Example B"
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.BackColor = System.Drawing.Color.Transparent
        Me.label1.Location = New System.Drawing.Point(108, 122)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(95, 13)
        Me.label1.TabIndex = 3
        Me.label1.Text = "Disable click alerts"
        '
        'imageButton4
        '
        Me.imageButton4.DialogResult = System.Windows.Forms.DialogResult.None
        Me.imageButton4.DownImage = Global.ImageButtonDemo.My.Resources.Resources.ExampleButtonDownA
        Me.imageButton4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.imageButton4.HoverImage = Nothing
        Me.imageButton4.Location = New System.Drawing.Point(264, 47)
        Me.imageButton4.Name = "imageButton4"
        Me.imageButton4.NormalImage = Global.ImageButtonDemo.My.Resources.Resources.ExampleButtonA
        Me.imageButton4.Size = New System.Drawing.Size(100, 50)
        Me.imageButton4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.imageButton4.TabIndex = 4
        Me.imageButton4.TabStop = False
        Me.imageButton4.Text = "Example C"
        '
        'imageButton5
        '
        Me.imageButton5.DialogResult = System.Windows.Forms.DialogResult.None
        Me.imageButton5.DownImage = Nothing
        Me.imageButton5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.imageButton5.HoverImage = Global.ImageButtonDemo.My.Resources.Resources.ExampleButtonHoverA
        Me.imageButton5.Location = New System.Drawing.Point(370, 47)
        Me.imageButton5.Name = "imageButton5"
        Me.imageButton5.NormalImage = Global.ImageButtonDemo.My.Resources.Resources.ExampleButtonA
        Me.imageButton5.Size = New System.Drawing.Size(100, 50)
        Me.imageButton5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.imageButton5.TabIndex = 5
        Me.imageButton5.TabStop = False
        Me.imageButton5.Text = "Example D"
        '
        'imageButton6
        '
        Me.imageButton6.DialogResult = System.Windows.Forms.DialogResult.None
        Me.imageButton6.DownImage = Nothing
        Me.imageButton6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.imageButton6.HoverImage = Nothing
        Me.imageButton6.Location = New System.Drawing.Point(264, 103)
        Me.imageButton6.Name = "imageButton6"
        Me.imageButton6.NormalImage = Global.ImageButtonDemo.My.Resources.Resources.ExampleButtonA
        Me.imageButton6.Size = New System.Drawing.Size(100, 50)
        Me.imageButton6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.imageButton6.TabIndex = 6
        Me.imageButton6.TabStop = False
        Me.imageButton6.Text = "Example E"
        '
        'imageButton7
        '
        Me.imageButton7.DialogResult = System.Windows.Forms.DialogResult.None
        Me.imageButton7.DownImage = Global.ImageButtonDemo.My.Resources.Resources.ExampleButtonDownA
        Me.imageButton7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.imageButton7.HoverImage = Global.ImageButtonDemo.My.Resources.Resources.ExampleButtonHoverA
        Me.imageButton7.Location = New System.Drawing.Point(370, 103)
        Me.imageButton7.Name = "imageButton7"
        Me.imageButton7.NormalImage = Nothing
        Me.imageButton7.Size = New System.Drawing.Size(100, 50)
        Me.imageButton7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.imageButton7.TabIndex = 7
        Me.imageButton7.TabStop = False
        Me.imageButton7.Text = "Example F"
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(57, 96)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(83, 13)
        Me.label2.TabIndex = 8
        Me.label2.Text = "Default button ^"
        '
        'TraceMessageToolStrip1
        '
        Me.TraceMessageToolStrip1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TraceMessageToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TraceMessageToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.TraceMessageToolStrip1.Name = "TraceMessageToolStrip1"
        Me.TraceMessageToolStrip1.Size = New System.Drawing.Size(556, 29)
        Me.TraceMessageToolStrip1.TabIndex = 9
        Me.TraceMessageToolStrip1.Text = "TraceMessageToolStrip1"
        '
        'Form1
        '
        Me.AcceptButton = Me.imageButton1
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(556, 192)
        Me.Controls.Add(Me.TraceMessageToolStrip1)
        Me.Controls.Add(Me.imageButton7)
        Me.Controls.Add(Me.imageButton6)
        Me.Controls.Add(Me.imageButton5)
        Me.Controls.Add(Me.imageButton4)
        Me.Controls.Add(Me.imageButton3)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.imageButton2)
        Me.Controls.Add(Me.imageButton1)
        Me.Controls.Add(Me.label2)
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.ShowIcon = False
        Me.Text = "Image Button Demo"
        CType(Me.imageButton1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imageButton3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imageButton2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imageButton4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imageButton5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imageButton6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imageButton7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private WithEvents ImageButton1 As isr.Core.Controls.ImageButton
    Private WithEvents ImageButton2 As isr.Core.Controls.ImageButton
    Private WithEvents ImageButton3 As isr.Core.Controls.ImageButton
    Private label1 As System.Windows.Forms.Label
    Private WithEvents ImageButton4 As isr.Core.Controls.ImageButton
    Private WithEvents ImageButton5 As isr.Core.Controls.ImageButton
    Private WithEvents ImageButton6 As isr.Core.Controls.ImageButton
    Private WithEvents ImageButton7 As isr.Core.Controls.ImageButton
    Private label2 As System.Windows.Forms.Label
    Private WithEvents TraceMessageToolStrip1 As isr.Core.Pith.TraceMessageToolStrip
End Class


﻿Public Class DropDownTextBoxForm

    Dim _Height As Integer
    Private Sub TextBox1_MouseClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TextBox1.MouseClick
        Dim textBox As TextBox = TryCast(sender, TextBox)
        If textBox IsNot Nothing Then
            With textBox
                If _height = 0 Then
                    _height = .Height
                End If
                .Multiline = True
                .Width = 300
                .Height = 200
                .Refresh()
            End With
        End If
    End Sub

    Private Sub TextBox1_MouseDoubleClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TextBox1.MouseDoubleClick
        Dim textBox As TextBox = TryCast(sender, TextBox)
        If textBox IsNot Nothing Then
            With textBox
                .Multiline = False
                .Width = 300
                .Height = 20
                .Refresh()
            End With
        End If
    End Sub

End Class
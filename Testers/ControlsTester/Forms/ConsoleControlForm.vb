﻿Imports System.Drawing
Imports System.Runtime.InteropServices

''' <summary> The application's main form. </summary>
''' <license>
''' (c) 2012 icemanind. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="7/9/2016" by="David" revision=""> http://www.codeproject.com/Articles/1053951/Console-Control </history>
Partial Public Class ConsoleControlForm
    Inherits Form

#Region " CONSTRUCTOR "

    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2234:PassSystemUriObjectsInsteadOfStrings")>
    Public Sub New()
        InitializeComponent()

        Dim codeBase As String = System.Reflection.Assembly.GetExecutingAssembly().CodeBase
        Dim uri As New UriBuilder(codeBase)
        _CurrentDirectory = System.IO.Path.GetDirectoryName(System.Uri.UnescapeDataString(uri.Path))
    End Sub

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    Private NotInheritable Class SafeNativeMethods
        Private Sub New()
            MyBase.New()
        End Sub
        <DllImport("user32.dll", CharSet:=CharSet.Auto, CallingConvention:=CallingConvention.Winapi)>
        Friend Shared Function GetFocus() As IntPtr
        End Function
    End Class

#End Region

    Private _CurrentDirectory As String

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        _LeftconsoleControl.SetCursorPosition(0, 0)
        _LeftconsoleControl.Write("Hello World!")
        _LeftconsoleControl.SetCursorPosition(2, 15)

        _LeftconsoleControl.Write("Look at this message!", Color.Yellow, Color.Blue)

        _CursorColumnTextBox.Text = _LeftconsoleControl.GetCursorPosition().Column.ToString()
        _CursorRowTextBox.Text = _LeftconsoleControl.GetCursorPosition().Row.ToString()

        _MessageTextBox.Text = "Hello World!"

        _BackgroundColorPanel.BackColor = _LeftconsoleControl.CurrentBackgroundColor
        _ForegroundColorPanel.BackColor = _LeftconsoleControl.CurrentForegroundColor

        _LeftconsoleControl.AllowInput = False

        _RightConsoleControl.SetCursorPosition(0, 0)
        _RightConsoleControl.Write("Welcome to Iceman Shell!")
        _RightConsoleControl.SetCursorPosition(1, 0)
        _RightConsoleControl.Write(String.Format("The current date and time is {0} {1}", Date.Now.ToShortDateString(), Date.Now.ToShortTimeString()))
        _RightConsoleControl.Write()
        _RightConsoleControl.Write("Use the command 'help' if you need help!")
        _RightConsoleControl.SetCursorPosition(_RightConsoleControl.GetCursorPosition().Row + 2, 0)
        ShowPrompt()

        AddHandler _RightConsoleControl.LineEntered, AddressOf ConsoleControl2LineEntered

        ActiveControl = _RightConsoleControl
    End Sub

    Private Sub ShowPrompt()
        _RightConsoleControl.Write()
        _RightConsoleControl.Write("# ")
    End Sub

    Private Sub ConsoleControl2LineEntered(ByVal sender As Object, ByVal e As isr.Core.Controls.LineEnteredEventArgs)
        If e Is Nothing Then Return
        Dim line As String = e.Line
        line = line.Trim()
        Dim ndx As Integer = line.IndexOf(" "c)
        Dim cmd As String = If(ndx < 0, line, line.Substring(0, ndx))
        Dim parameters As String = If(ndx < 0, "", line.Remove(0, ndx + 1))

        Select Case cmd.ToLower()
            Case ""
                ShowPrompt()
            Case "help"
                ShowHelp()
                ShowPrompt()
            Case "exit"
                Application.Exit()
            Case "ls"
                DoLsCommand(parameters)
            Case "echo"
                DoEchoCommand(parameters)
                ShowPrompt()
            Case "date"
                DoDateCommand()
                ShowPrompt()
            Case "pwd"
                DoPwdCommand()
                ShowPrompt()
            Case "cd"
                DoCdCommand(parameters)
                ShowPrompt()
            Case Else
                _RightConsoleControl.Write("> Unknown Command!")
                _RightConsoleControl.Write()
                ShowPrompt()
        End Select
    End Sub

    Private Sub BtnClearConsole_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _ClearConsoleButton.Click
        _LeftconsoleControl.Clear()
        _CursorColumnTextBox.Text = _LeftconsoleControl.GetCursorPosition().Column.ToString()
        _CursorRowTextBox.Text = _LeftconsoleControl.GetCursorPosition().Row.ToString()
    End Sub

    Private Sub BtnMoveCursor_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _MoveCursorButton.Click
        Dim row As Integer = Integer.Parse(_CursorRowTextBox.Text)
        Dim column As Integer = Integer.Parse(_CursorColumnTextBox.Text)

        If row < 0 OrElse row > 24 Then
            MessageBox.Show("Cursor Row Must be between 0 and 24!")
            Return
        End If

        If column < 0 OrElse column > 79 Then
            MessageBox.Show("Cursor Column Must be between 0 and 79!")
            Return
        End If

        _LeftconsoleControl.SetCursorPosition(row, column)
    End Sub

    Private Sub RbUnderline_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _UnderlineRadioButton.CheckedChanged
        ChangeCursorType()
    End Sub

    Private Sub RbBlock_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _BlockRadioButton.CheckedChanged
        ChangeCursorType()
    End Sub

    Private Sub RbNone_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _NoneRadioButton.CheckedChanged
        ChangeCursorType()
    End Sub

    Private Sub ChangeCursorType()
        If _UnderlineRadioButton.Checked Then
            _LeftconsoleControl.CursorType = isr.Core.Controls.CursorType.Underline
        End If
        If _BlockRadioButton.Checked Then
            _LeftconsoleControl.CursorType = isr.Core.Controls.CursorType.Block
        End If
        If _NoneRadioButton.Checked Then
            _LeftconsoleControl.CursorType = isr.Core.Controls.CursorType.Invisible
        End If
    End Sub

    Private Sub BtnWriteMessage_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _WriteMessageButton.Click
        _LeftconsoleControl.Write(_MessageTextBox.Text)
    End Sub

    Private Sub PnlBackgroundColor_DoubleClick(ByVal sender As Object, ByVal e As EventArgs) Handles _BackgroundColorPanel.DoubleClick
        _ColorDialog.Color = _LeftconsoleControl.CurrentBackgroundColor
        Dim dr As DialogResult = _ColorDialog.ShowDialog()

        If dr = System.Windows.Forms.DialogResult.OK Then
            _BackgroundColorPanel.BackColor = _ColorDialog.Color
            _LeftconsoleControl.CurrentBackgroundColor = _ColorDialog.Color
        End If
    End Sub

    Private Sub PnlForegroundColor_DoubleClick(ByVal sender As Object, ByVal e As EventArgs) Handles _ForegroundColorPanel.DoubleClick
        _ColorDialog.Color = _LeftconsoleControl.CurrentForegroundColor
        Dim dr As DialogResult = _ColorDialog.ShowDialog()

        If dr = System.Windows.Forms.DialogResult.OK Then
            _ForegroundColorPanel.BackColor = _ColorDialog.Color
            _LeftconsoleControl.CurrentForegroundColor = _ColorDialog.Color
        End If
    End Sub

    Private Function FindFocusedControl() As Control
        Dim focusedControl As Control = Nothing
        Dim focusedHandle As IntPtr = SafeNativeMethods.GetFocus()
        If focusedHandle <> IntPtr.Zero Then
            focusedControl = FromHandle(focusedHandle)
        End If
        Return focusedControl
    End Function

    Private Sub Form1_Paint(ByVal sender As Object, ByVal e As PaintEventArgs) Handles MyBase.Paint
        Const offsetX As Integer = 2

        Dim b As New Bitmap(ClientSize.Width, ClientSize.Height)
        Dim g As Graphics = Graphics.FromImage(b)

        Dim c1 As Control = FindFocusedControl()

        If c1 IsNot Nothing AndAlso c1.Name.StartsWith("consoleControl") Then
            For i As Integer = 1 To 5
                Dim p As New Pen(Color.FromArgb(Math.Min(25 * (i * 2), 255), 50, 80, 255))
                g.DrawRectangle(p, c1.Location.X - i, c1.Location.Y - i, c1.Size.Width + i + i, c1.Size.Height + i + i)
            Next i
        End If

        g.DrawLine(New Pen(Color.FromArgb(200, 200, 200)), 660 + offsetX, 3, 660 + offsetX, ClientSize.Height - 5)
        g.DrawLine(New Pen(Color.FromArgb(180, 180, 180)), 661 + offsetX, 3, 661 + offsetX, ClientSize.Height - 5)
        g.DrawLine(New Pen(Color.FromArgb(128, 128, 128)), 662 + offsetX, 3, 662 + offsetX, ClientSize.Height - 5)
        g.DrawLine(New Pen(Color.FromArgb(108, 108, 108)), 663 + offsetX, 3, 663 + offsetX, ClientSize.Height - 5)

        e.Graphics.DrawImage(b, New Point(0, 0))
        g.Dispose()
        b.Dispose()
    End Sub

    Private Sub ConsoleControl1_Enter(ByVal sender As Object, ByVal e As EventArgs) Handles _LeftconsoleControl.Enter
        Refresh()
    End Sub

    Private Sub ConsoleControl1_Leave(ByVal sender As Object, ByVal e As EventArgs) Handles _LeftconsoleControl.Leave
        Refresh()
    End Sub

    Private Sub ConsoleControl2_Enter(ByVal sender As Object, ByVal e As EventArgs) Handles _RightConsoleControl.Enter
        Refresh()
    End Sub

    Private Sub ConsoleControl2_Leave(ByVal sender As Object, ByVal e As EventArgs) Handles _RightConsoleControl.Leave
        Refresh()
    End Sub

    Private Sub ShowHelp()
        _RightConsoleControl.Write("IcemanShell is just a 'toy' shell to show off Icemanind's Console Control.")
        _RightConsoleControl.Write()
        _RightConsoleControl.Write("Here is a list of commands:")
        _RightConsoleControl.Write()
        _RightConsoleControl.Write("(Note that the commands are NOT case sensitive)")
        _RightConsoleControl.Write()
        _RightConsoleControl.Write()
        _RightConsoleControl.Write("----------------------------------------------------------------------")
        _RightConsoleControl.Write()
        _RightConsoleControl.Write("HELP                Shows this help message")
        _RightConsoleControl.Write()
        _RightConsoleControl.Write("EXIT                Quits the application")
        _RightConsoleControl.Write()
        _RightConsoleControl.Write("LS <path>           Shows the contents of the specified directory.")
        _RightConsoleControl.Write()
        _RightConsoleControl.Write("ECHO [string]       Echo's the [string] to the console.")
        _RightConsoleControl.Write()
        _RightConsoleControl.Write("DATE                Shows the current date.")
        _RightConsoleControl.Write()
        _RightConsoleControl.Write("PWD                 Shows the current working directory.")
        _RightConsoleControl.Write()
        _RightConsoleControl.Write("CD <path>           Change the current working directory to <path>.")
        _RightConsoleControl.Write()
    End Sub

    Private Sub DoLsCommand(ByVal parameters As String)
        Dim path As String
        Dim bgColor As Color = _RightConsoleControl.CurrentBackgroundColor
        Dim fgColor As Color = _RightConsoleControl.CurrentForegroundColor

        If String.IsNullOrEmpty(parameters) Then
            path = _currentDirectory
        Else
            path = parameters.Trim(""""c)
        End If

        Dim files() As String = System.IO.Directory.GetFiles(path)
        Dim directories() As String = System.IO.Directory.GetDirectories(path)

        _RightConsoleControl.CurrentForegroundColor = Color.CadetBlue
        For Each directory As String In directories.OrderBy(Function(z) z)
            Dim dirName As String = "[" & System.IO.Path.GetFileName(directory) & "]"
            _RightConsoleControl.Write(String.Format("{0,-40}", If(dirName.Length > 40, Environment.NewLine & dirName & Environment.NewLine, dirName)))
        Next directory

        For Each file As String In files.OrderBy(Function(z) z)
            Dim fileName As String = If(System.IO.Path.GetFileName(file), "")
            Dim extension As String = If(System.IO.Path.GetExtension(file), "")
            Select Case extension.ToLower()
                Case ".exe"
                    _RightConsoleControl.CurrentForegroundColor = Color.FromArgb(100, 255, 100)
                Case ".dll"
                    _RightConsoleControl.CurrentForegroundColor = Color.FromArgb(10, 165, 10)
                Case Else
                    _RightConsoleControl.CurrentForegroundColor = Color.LightGray
            End Select
            _RightConsoleControl.Write(String.Format("{0,-40}", If(fileName.Length > 40, Environment.NewLine & fileName & Environment.NewLine, fileName)))
        Next file

        _RightConsoleControl.CurrentBackgroundColor = bgColor
        _RightConsoleControl.CurrentForegroundColor = fgColor

        ShowPrompt()
    End Sub

    Private Sub DoEchoCommand(ByVal parameters As String)
        _RightConsoleControl.Write(parameters)
        _RightConsoleControl.Write()
    End Sub

    Private Sub DoDateCommand()
        _RightConsoleControl.Write(String.Format("{0}", Date.Now.ToString("ddd MMM d HH:mm:ss K yyyy")))
        _RightConsoleControl.Write()
    End Sub

    Private Sub DoPwdCommand()
        _RightConsoleControl.Write(_currentDirectory)
        _RightConsoleControl.Write()
    End Sub

    Private Sub DoCdCommand(ByVal parameters As String)
        parameters = parameters.Trim(""""c)

        If parameters.Length >= 2 AndAlso parameters.Chars(1) = ":"c Then
            If System.IO.Directory.Exists(parameters) Then
                _currentDirectory = parameters
            Else
                _RightConsoleControl.Write("Invalid Directory ==> " & parameters)
                _RightConsoleControl.Write()
                Return
            End If
        ElseIf parameters.Length > 0 AndAlso parameters.Chars(0) = "\"c Then
            If System.IO.Directory.Exists(System.IO.Path.GetPathRoot(_currentDirectory) & parameters.Remove(0, 1)) Then
                _currentDirectory = System.IO.Path.GetPathRoot(_currentDirectory) & parameters.Remove(0, 1)
            Else
                _RightConsoleControl.Write("Invalid Directory ==> " & System.IO.Path.GetPathRoot(_currentDirectory) & parameters.Remove(0, 1))
                _RightConsoleControl.Write()
                Return
            End If
        ElseIf parameters.Length > 0 Then
            If System.IO.Directory.Exists(_currentDirectory & "\" & parameters) Then
                _currentDirectory = _currentDirectory & "\" & parameters
            Else
                _RightConsoleControl.Write("Invalid Directory ==> " & _currentDirectory & parameters)
                _RightConsoleControl.Write()
                Return
            End If
        End If

        _currentDirectory = _currentDirectory.Replace("\\", "\")
        _RightConsoleControl.Write(String.Format("Current Directory is now: {0}", _currentDirectory))
        _RightConsoleControl.Write()
    End Sub
End Class

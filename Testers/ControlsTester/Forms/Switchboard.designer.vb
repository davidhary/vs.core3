<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class Switchboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Switchboard))
        Me._CancelButton = New System.Windows.Forms.Button()
        Me._ExitButton = New System.Windows.Forms.Button()
        Me._TestButton = New System.Windows.Forms.Button()
        Me._ActionsComboBox = New System.Windows.Forms.ComboBox()
        Me._OpenButton = New System.Windows.Forms.Button()
        Me._ControlsPanel = New System.Windows.Forms.Panel()
        Me._MessagesTextBox = New System.Windows.Forms.TextBox()
        Me._LogOnButton = New System.Windows.Forms.Button()
        Me._ControlsPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        '_CancelButton
        '
        Me._CancelButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._CancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._CancelButton.Location = New System.Drawing.Point(308, 38)
        Me._CancelButton.Name = "_CancelButton"
        Me._CancelButton.Size = New System.Drawing.Size(60, 23)
        Me._CancelButton.TabIndex = 7
        Me._CancelButton.Text = "&Cancel"
        '
        '_ExitButton
        '
        Me._ExitButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ExitButton.DialogResult = System.Windows.Forms.DialogResult.OK
        Me._ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._ExitButton.Location = New System.Drawing.Point(374, 38)
        Me._ExitButton.Name = "_ExitButton"
        Me._ExitButton.Size = New System.Drawing.Size(60, 23)
        Me._ExitButton.TabIndex = 5
        Me._ExitButton.Text = "E&xit"
        '
        '_TestButton
        '
        Me._TestButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._TestButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._TestButton.Location = New System.Drawing.Point(10, 38)
        Me._TestButton.Name = "_TestButton"
        Me._TestButton.Size = New System.Drawing.Size(60, 23)
        Me._TestButton.TabIndex = 8
        Me._TestButton.Text = "&Test"
        '
        '_ActionsComboBox
        '
        Me._ActionsComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ActionsComboBox.BackColor = System.Drawing.SystemColors.Window
        Me._ActionsComboBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._ActionsComboBox.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ActionsComboBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._ActionsComboBox.Location = New System.Drawing.Point(9, 3)
        Me._ActionsComboBox.Name = "_ActionsComboBox"
        Me._ActionsComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ActionsComboBox.Size = New System.Drawing.Size(361, 27)
        Me._ActionsComboBox.TabIndex = 11
        Me._ActionsComboBox.Text = "Select option from the list"
        '
        '_OpenButton
        '
        Me._OpenButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._OpenButton.Location = New System.Drawing.Point(376, 4)
        Me._OpenButton.Name = "_OpenButton"
        Me._OpenButton.Size = New System.Drawing.Size(58, 24)
        Me._OpenButton.TabIndex = 12
        Me._OpenButton.Text = "&Open..."
        '
        '_ControlsPanel
        '
        Me._ControlsPanel.Controls.Add(Me._ActionsComboBox)
        Me._ControlsPanel.Controls.Add(Me._CancelButton)
        Me._ControlsPanel.Controls.Add(Me._ExitButton)
        Me._ControlsPanel.Controls.Add(Me._OpenButton)
        Me._ControlsPanel.Controls.Add(Me._LogOnButton)
        Me._ControlsPanel.Controls.Add(Me._TestButton)
        Me._ControlsPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._ControlsPanel.Location = New System.Drawing.Point(0, 199)
        Me._ControlsPanel.Name = "_ControlsPanel"
        Me._ControlsPanel.Size = New System.Drawing.Size(442, 66)
        Me._ControlsPanel.TabIndex = 15
        '
        '_MessagesTextBox
        '
        Me._MessagesTextBox.BackColor = System.Drawing.SystemColors.Info
        Me._MessagesTextBox.CausesValidation = False
        Me._MessagesTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._MessagesTextBox.Location = New System.Drawing.Point(0, 0)
        Me._MessagesTextBox.Multiline = True
        Me._MessagesTextBox.Name = "_MessagesTextBox"
        Me._MessagesTextBox.ReadOnly = True
        Me._MessagesTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._MessagesTextBox.Size = New System.Drawing.Size(442, 199)
        Me._MessagesTextBox.TabIndex = 16
        '
        '_LogOnButton
        '
        Me._LogOnButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._LogOnButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._LogOnButton.Location = New System.Drawing.Point(87, 38)
        Me._LogOnButton.Name = "_LogOnButton"
        Me._LogOnButton.Size = New System.Drawing.Size(60, 23)
        Me._LogOnButton.TabIndex = 8
        Me._LogOnButton.Text = "&Test"
        '
        'Switchboard
        '
        Me.AcceptButton = Me._ExitButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me._CancelButton
        Me.ClientSize = New System.Drawing.Size(442, 265)
        Me.Controls.Add(Me._MessagesTextBox)
        Me.Controls.Add(Me._ControlsPanel)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Switchboard"
        Me.Text = "Form1"
        Me._ControlsPanel.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _CancelButton As System.Windows.Forms.Button
    Private WithEvents _ExitButton As System.Windows.Forms.Button
    Private WithEvents _TestButton As System.Windows.Forms.Button
    Private WithEvents _ActionsComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _OpenButton As System.Windows.Forms.Button
    Private WithEvents _ControlsPanel As System.Windows.Forms.Panel
    Private WithEvents _MessagesTextBox As System.Windows.Forms.TextBox
    Private WithEvents _LogOnButton As System.Windows.Forms.Button

End Class

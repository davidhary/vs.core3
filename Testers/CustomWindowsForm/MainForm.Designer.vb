﻿Imports isr.Core.Controls
Partial Public Class MainForm
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me._TopPanel = New System.Windows.Forms.Panel()
        Me._WindowTextLabel = New System.Windows.Forms.Label()
        Me._MinButton = New ZopeButton()
        Me._CloseButton = New ZopeButton()
        Me._RightPanel = New System.Windows.Forms.Panel()
        Me._LeftPanel = New System.Windows.Forms.Panel()
        Me._BottomPanel = New System.Windows.Forms.Panel()
        Me._ExitButton = New ZopeButton()
        Me._OpenDashboardShapedButton = New ShapedButton()
        Me._OpenDarkFormShapedButton = New ShapedButton()
        Me._OpenBlueFormShapedButton = New ShapedButton()
        Me._TopPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        '_TopPanel
        '
        Me._TopPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._TopPanel.Controls.Add(Me._WindowTextLabel)
        Me._TopPanel.Controls.Add(Me._MinButton)
        Me._TopPanel.Controls.Add(Me._CloseButton)
        Me._TopPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._TopPanel.Location = New System.Drawing.Point(0, 0)
        Me._TopPanel.Name = "_TopPanel"
        Me._TopPanel.Size = New System.Drawing.Size(355, 52)
        Me._TopPanel.TabIndex = 0
        '
        '_WindowTextLabel
        '
        Me._WindowTextLabel.AutoSize = True
        Me._WindowTextLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._WindowTextLabel.ForeColor = System.Drawing.Color.White
        Me._WindowTextLabel.Location = New System.Drawing.Point(21, 14)
        Me._WindowTextLabel.Name = "_WindowTextLabel"
        Me._WindowTextLabel.Size = New System.Drawing.Size(80, 20)
        Me._WindowTextLabel.TabIndex = 4
        Me._WindowTextLabel.Text = "Main Form"
        '
        '_MinButton
        '
        Me._MinButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._MinButton.DisplayText = "_"
        Me._MinButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._MinButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MinButton.ForeColor = System.Drawing.Color.White
        Me._MinButton.Location = New System.Drawing.Point(289, 3)
        Me._MinButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(160, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(200, Byte), Integer))
        Me._MinButton.MouseColorsEnabled = True
        Me._MinButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(40, Byte), Integer))
        Me._MinButton.Name = "_MinButton"
        Me._MinButton.Size = New System.Drawing.Size(31, 24)
        Me._MinButton.TabIndex = 3
        Me._MinButton.Text = "_"
        Me._MinButton.TextLocationLeft = 6
        Me._MinButton.TextLocationTop = -20
        Me._MinButton.UseVisualStyleBackColor = True
        '
        '_CloseButton
        '
        Me._CloseButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._CloseButton.DisplayText = "X"
        Me._CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._CloseButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._CloseButton.ForeColor = System.Drawing.Color.White
        Me._CloseButton.Location = New System.Drawing.Point(320, 3)
        Me._CloseButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(150, Byte), Integer), CType(CType(220, Byte), Integer))
        Me._CloseButton.MouseColorsEnabled = True
        Me._CloseButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(40, Byte), Integer))
        Me._CloseButton.Name = "_CloseButton"
        Me._CloseButton.Size = New System.Drawing.Size(31, 24)
        Me._CloseButton.TabIndex = 2
        Me._CloseButton.Text = "X"
        Me._CloseButton.TextLocationLeft = 6
        Me._CloseButton.TextLocationTop = -1
        Me._CloseButton.UseVisualStyleBackColor = True
        '
        '_RightPanel
        '
        Me._RightPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._RightPanel.Dock = System.Windows.Forms.DockStyle.Right
        Me._RightPanel.Location = New System.Drawing.Point(343, 52)
        Me._RightPanel.Name = "_RightPanel"
        Me._RightPanel.Size = New System.Drawing.Size(12, 375)
        Me._RightPanel.TabIndex = 1
        '
        '_LeftPanel
        '
        Me._LeftPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._LeftPanel.Dock = System.Windows.Forms.DockStyle.Left
        Me._LeftPanel.Location = New System.Drawing.Point(0, 52)
        Me._LeftPanel.Name = "_LeftPanel"
        Me._LeftPanel.Size = New System.Drawing.Size(12, 375)
        Me._LeftPanel.TabIndex = 2
        '
        '_BottomPanel
        '
        Me._BottomPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._BottomPanel.Location = New System.Drawing.Point(12, 415)
        Me._BottomPanel.Name = "_BottomPanel"
        Me._BottomPanel.Size = New System.Drawing.Size(331, 12)
        Me._BottomPanel.TabIndex = 3
        '
        '_ExitButton
        '
        Me._ExitButton.BusyBackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._ExitButton.DisplayText = "Exit"
        Me._ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._ExitButton.Font = New System.Drawing.Font("Microsoft YaHei UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ExitButton.ForeColor = System.Drawing.Color.White
        Me._ExitButton.Location = New System.Drawing.Point(227, 367)
        Me._ExitButton.MouseClickColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me._ExitButton.MouseColorsEnabled = True
        Me._ExitButton.MouseHoverColor = System.Drawing.Color.FromArgb(CType(CType(180, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(40, Byte), Integer))
        Me._ExitButton.Name = "_ExitButton"
        Me._ExitButton.Size = New System.Drawing.Size(93, 29)
        Me._ExitButton.TabIndex = 7
        Me._ExitButton.Text = "Exit"
        Me._ExitButton.TextLocationLeft = 26
        Me._ExitButton.TextLocationTop = 3
        Me._ExitButton.UseVisualStyleBackColor = True
        '
        '_OpenDashboardShapedButton
        '
        Me._OpenDashboardShapedButton.BackColor = System.Drawing.Color.Transparent
        Me._OpenDashboardShapedButton.BorderColor = System.Drawing.Color.Transparent
        Me._OpenDashboardShapedButton.BorderWidth = 2
        Me._OpenDashboardShapedButton.ButtonShape = ButtonShape.RoundRect
        Me._OpenDashboardShapedButton.ButtonText = "Dashboard UI Form"
        Me._OpenDashboardShapedButton.EndColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(40, Byte), Integer))
        Me._OpenDashboardShapedButton.EndOpacity = 250
        Me._OpenDashboardShapedButton.FlatAppearance.BorderSize = 0
        Me._OpenDashboardShapedButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me._OpenDashboardShapedButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me._OpenDashboardShapedButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._OpenDashboardShapedButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._OpenDashboardShapedButton.ForeColor = System.Drawing.Color.White
        Me._OpenDashboardShapedButton.GradientAngle = 90
        Me._OpenDashboardShapedButton.Location = New System.Drawing.Point(25, 269)
        Me._OpenDashboardShapedButton.MouseClickStartColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(40, Byte), Integer))
        Me._OpenDashboardShapedButton.MouseClickEndColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._OpenDashboardShapedButton.MouseHoverStartColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(180, Byte), Integer))
        Me._OpenDashboardShapedButton.MouseHoverEndColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._OpenDashboardShapedButton.Name = "_OpenDashboardShapedButton"
        Me._OpenDashboardShapedButton.ShowButtonText = True
        Me._OpenDashboardShapedButton.Size = New System.Drawing.Size(312, 75)
        Me._OpenDashboardShapedButton.StartColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(120, Byte), Integer))
        Me._OpenDashboardShapedButton.StartOpacity = 250
        Me._OpenDashboardShapedButton.TabIndex = 6
        Me._OpenDashboardShapedButton.Text = "Dashboard UI Form"
        Me._OpenDashboardShapedButton.TextLocation = New Point(90, 25)
        Me._OpenDashboardShapedButton.UseVisualStyleBackColor = False
        '
        '_OpenDarkFormShapedButton
        '
        Me._OpenDarkFormShapedButton.BackColor = System.Drawing.Color.Transparent
        Me._OpenDarkFormShapedButton.BorderColor = System.Drawing.Color.Transparent
        Me._OpenDarkFormShapedButton.BorderWidth = 2
        Me._OpenDarkFormShapedButton.ButtonShape = ButtonShape.RoundRect
        Me._OpenDarkFormShapedButton.ButtonText = "Dark Custom Form"
        Me._OpenDarkFormShapedButton.EndColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer))
        Me._OpenDarkFormShapedButton.EndOpacity = 250
        Me._OpenDarkFormShapedButton.FlatAppearance.BorderSize = 0
        Me._OpenDarkFormShapedButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me._OpenDarkFormShapedButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me._OpenDarkFormShapedButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._OpenDarkFormShapedButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._OpenDarkFormShapedButton.ForeColor = System.Drawing.Color.White
        Me._OpenDarkFormShapedButton.GradientAngle = 90
        Me._OpenDarkFormShapedButton.Location = New System.Drawing.Point(25, 172)
        Me._OpenDarkFormShapedButton.MouseClickStartColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(40, Byte), Integer))
        Me._OpenDarkFormShapedButton.MouseClickEndColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._OpenDarkFormShapedButton.MouseHoverStartColor = System.Drawing.Color.FromArgb(CType(CType(150, Byte), Integer), CType(CType(150, Byte), Integer), CType(CType(150, Byte), Integer))
        Me._OpenDarkFormShapedButton.MouseHoverEndColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
        Me._OpenDarkFormShapedButton.Name = "_OpenDarkFormShapedButton"
        Me._OpenDarkFormShapedButton.ShowButtonText = True
        Me._OpenDarkFormShapedButton.Size = New System.Drawing.Size(312, 75)
        Me._OpenDarkFormShapedButton.StartColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(100, Byte), Integer))
        Me._OpenDarkFormShapedButton.StartOpacity = 250
        Me._OpenDarkFormShapedButton.TabIndex = 5
        Me._OpenDarkFormShapedButton.Text = "shapedButton2"
        Me._OpenDarkFormShapedButton.TextLocation = New Point(90, 25)
        Me._OpenDarkFormShapedButton.UseVisualStyleBackColor = False
        '
        '_OpenBlueFormShapedButton
        '
        Me._OpenBlueFormShapedButton.BackColor = System.Drawing.Color.Transparent
        Me._OpenBlueFormShapedButton.BorderColor = System.Drawing.Color.Transparent
        Me._OpenBlueFormShapedButton.BorderWidth = 2
        Me._OpenBlueFormShapedButton.ButtonShape = ButtonShape.RoundRect
        Me._OpenBlueFormShapedButton.ButtonText = "Simple Blue Form"
        Me._OpenBlueFormShapedButton.EndColor = System.Drawing.Color.MidnightBlue
        Me._OpenBlueFormShapedButton.EndOpacity = 250
        Me._OpenBlueFormShapedButton.FlatAppearance.BorderSize = 0
        Me._OpenBlueFormShapedButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me._OpenBlueFormShapedButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me._OpenBlueFormShapedButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me._OpenBlueFormShapedButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._OpenBlueFormShapedButton.ForeColor = System.Drawing.Color.White
        Me._OpenBlueFormShapedButton.GradientAngle = 90
        Me._OpenBlueFormShapedButton.Location = New System.Drawing.Point(25, 77)
        Me._OpenBlueFormShapedButton.MouseClickStartColor = System.Drawing.Color.FromArgb(CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(40, Byte), Integer))
        Me._OpenBlueFormShapedButton.MouseClickEndColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._OpenBlueFormShapedButton.MouseHoverStartColor = System.Drawing.Color.Turquoise
        Me._OpenBlueFormShapedButton.MouseHoverEndColor = System.Drawing.Color.DarkSlateGray
        Me._OpenBlueFormShapedButton.Name = "_OpenBlueFormShapedButton"
        Me._OpenBlueFormShapedButton.ShowButtontext = True
        Me._OpenBlueFormShapedButton.Size = New System.Drawing.Size(312, 75)
        Me._OpenBlueFormShapedButton.StartColor = System.Drawing.Color.DodgerBlue
        Me._OpenBlueFormShapedButton.StartOpacity = 250
        Me._OpenBlueFormShapedButton.TabIndex = 4
        Me._OpenBlueFormShapedButton.Text = "Simple Blue Form"
        Me._OpenBlueFormShapedButton.TextLocation = New Point(90, 26)
        Me._OpenBlueFormShapedButton.UseVisualStyleBackColor = False
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(355, 427)
        Me.Controls.Add(Me._ExitButton)
        Me.Controls.Add(Me._OpenDashboardShapedButton)
        Me.Controls.Add(Me._OpenDarkFormShapedButton)
        Me.Controls.Add(Me._OpenBlueFormShapedButton)
        Me.Controls.Add(Me._BottomPanel)
        Me.Controls.Add(Me._LeftPanel)
        Me.Controls.Add(Me._RightPanel)
        Me.Controls.Add(Me._TopPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Main Form"
        Me._TopPanel.ResumeLayout(False)
        Me._TopPanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private WithEvents _TopPanel As System.Windows.Forms.Panel
    Private WithEvents _MinButton As ZopeButton
    Private WithEvents _CloseButton As ZopeButton
    Private WithEvents _WindowTextLabel As System.Windows.Forms.Label
    Private _RightPanel As System.Windows.Forms.Panel
    Private _LeftPanel As System.Windows.Forms.Panel
    Private _BottomPanel As System.Windows.Forms.Panel
    Private WithEvents _OpenBlueFormShapedButton As ShapedButton
    Private WithEvents _OpenDarkFormShapedButton As ShapedButton
    Private WithEvents _OpenDashboardShapedButton As ShapedButton
    Private WithEvents _ExitButton As ZopeButton
End Class

﻿Imports System.ComponentModel
Imports System.Text
Imports System.Threading.Tasks
Imports isr.Core.Controls

Partial Public Class BlueForm
    Inherits Form

    Public Sub New()
        InitializeComponent()
    End Sub


    Private isTopPanelDragged As Boolean = False
    Private isLeftPanelDragged As Boolean = False
    Private isRightPanelDragged As Boolean = False
    Private isBottomPanelDragged As Boolean = False
    Private isTopBorderPanelDragged As Boolean = False
    Private isWindowMaximized As Boolean = False
    Private offset As Point
    Private _NormalWindowSize As Size
    Private _NormalWindowLocation As Point = Point.Empty


    Private Sub TopBorderPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            isTopBorderPanelDragged = True
        Else
            isTopBorderPanelDragged = False
        End If
    End Sub

    Private Sub TopBorderPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderPanel.MouseMove
        If e.Y < Me.Location.Y Then
            If isTopBorderPanelDragged Then
                If Me.Height < 50 Then
                    Me.Height = 50
                    isTopBorderPanelDragged = False
                Else
                    Me.Location = New Point(Me.Location.X, Me.Location.Y + e.Y)
                    Me.Height = Me.Height - e.Y
                End If
            End If
        End If
    End Sub

    Private Sub TopBorderPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderPanel.MouseUp
        isTopBorderPanelDragged = False
    End Sub




    Private Sub TopPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            isTopPanelDragged = True
            Dim pointStartPosition As Point = Me.PointToScreen(New Point(e.X, e.Y))
            offset = New Point() With {
                .X = Me.Location.X - pointStartPosition.X,
                .Y = Me.Location.Y - pointStartPosition.Y}
        Else
            isTopPanelDragged = False
        End If
        If e.Clicks = 2 Then
            isTopPanelDragged = False
            _MaxButton_Click(sender, e)
        End If
    End Sub

    Private Sub TopPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseMove
        If isTopPanelDragged Then
            Dim newPoint As Point = _TopPanel.PointToScreen(New Point(e.X, e.Y))
            newPoint.Offset(offset)
            Me.Location = newPoint

            If Me.Location.X > 2 OrElse Me.Location.Y > 2 Then
                If Me.WindowState = FormWindowState.Maximized Then
                    Me.Location = _normalWindowLocation
                    Me.Size = _normalWindowSize
                    _ToolTip.SetToolTip(_MaxButton, "Maximize")
                    _MaxButton.CustomFormState = CustomFormState.Normal
                    isWindowMaximized = False
                End If
            End If
        End If
    End Sub

    Private Sub TopPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseUp
        isTopPanelDragged = False
        If Me.Location.Y <= 5 Then
            If Not isWindowMaximized Then
                _normalWindowSize = Me.Size
                _normalWindowLocation = Me.Location

                Dim rect As Rectangle = Screen.PrimaryScreen.WorkingArea
                Me.Location = New Point(0, 0)
                Me.Size = New System.Drawing.Size(rect.Width, rect.Height)
                _ToolTip.SetToolTip(_MaxButton, "Restore Down")
                _MaxButton.CustomFormState = CustomFormState.Maximize
                isWindowMaximized = True
            End If
        End If
    End Sub



    Private Sub LeftPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderPanel.MouseDown
        If Me.Location.X <= 0 OrElse e.X < 0 Then
            isLeftPanelDragged = False
            Me.Location = New Point(10, Me.Location.Y)
        Else
            If e.Button = MouseButtons.Left Then
                isLeftPanelDragged = True
            Else
                isLeftPanelDragged = False
            End If
        End If
    End Sub

    Private Sub LeftPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderPanel.MouseMove
        If e.X < Me.Location.X Then
            If isLeftPanelDragged Then
                If Me.Width < 100 Then
                    Me.Width = 100
                    isLeftPanelDragged = False
                Else
                    Me.Location = New Point(Me.Location.X + e.X, Me.Location.Y)
                    Me.Width = Me.Width - e.X
                End If
            End If
        End If
    End Sub

    Private Sub LeftPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderPanel.MouseUp
        isLeftPanelDragged = False
    End Sub



    Private Sub RightPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            isRightPanelDragged = True
        Else
            isRightPanelDragged = False
        End If
    End Sub

    Private Sub RightPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderPanel.MouseMove
        If isRightPanelDragged Then
            If Me.Width < 100 Then
                Me.Width = 100
                isRightPanelDragged = False
            Else
                Me.Width = Me.Width + e.X
            End If
        End If
    End Sub

    Private Sub RightPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderPanel.MouseUp
        isRightPanelDragged = False
    End Sub



    Private Sub BottomPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            isBottomPanelDragged = True
        Else
            isBottomPanelDragged = False
        End If
    End Sub

    Private Sub BottomPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderPanel.MouseMove
        If isBottomPanelDragged Then
            If Me.Height < 50 Then
                Me.Height = 50
                isBottomPanelDragged = False
            Else
                Me.Height = Me.Height + e.Y
            End If
        End If
    End Sub

    Private Sub BottomPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderPanel.MouseUp
        isBottomPanelDragged = False
    End Sub




    Private Sub _CloseButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CloseButton.Click
        Me.Close()
    End Sub

    Private Sub _MaxButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MaxButton.Click
        If isWindowMaximized Then
            Me.Location = _normalWindowLocation
            Me.Size = _normalWindowSize
            _ToolTip.SetToolTip(_MaxButton, "Maximize")
            _MaxButton.CustomFormState = CustomFormState.Normal
            isWindowMaximized = False
        Else
            _normalWindowSize = Me.Size
            _normalWindowLocation = Me.Location

            Dim rect As Rectangle = Screen.PrimaryScreen.WorkingArea
            Me.Location = New Point(0, 0)
            Me.Size = New System.Drawing.Size(rect.Width, rect.Height)
            _ToolTip.SetToolTip(_MaxButton, "Restore Down")
            _MaxButton.CustomFormState = CustomFormState.Maximize
            isWindowMaximized = True
        End If
    End Sub

    Private Sub _MinButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MinButton.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub
End Class

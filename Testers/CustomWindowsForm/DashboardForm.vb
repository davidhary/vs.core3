﻿Imports System.ComponentModel
Imports System.Text
Imports System.Threading.Tasks
Imports isr.Core.Controls
Partial Public Class DashboardForm
    Inherits Form

    Public Sub New()
        InitializeComponent()
    End Sub

    Private offset As Point
    Private isTopPanelDragged As Boolean = False


    Private Sub Dashboard_Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dashboard_button_Click(sender, e)
    End Sub

    Private Sub TopPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            isTopPanelDragged = True
            Dim pointStartPosition As Point = Me.PointToScreen(New Point(e.X, e.Y))
            offset = New Point() With {
                .X = Me.Location.X - (pointStartPosition.X + _LeftTopPanel.Size.Width),
                .Y = Me.Location.Y - pointStartPosition.Y}
        Else
            isTopPanelDragged = False
        End If
    End Sub

    Private Sub TopPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseMove
        If isTopPanelDragged Then
            Dim newPoint As Point = _TopPanel.PointToScreen(New Point(e.X, e.Y))
            newPoint.Offset(offset)
            Me.Location = newPoint
        End If
    End Sub

    Private Sub TopPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseUp
        isTopPanelDragged = False
    End Sub

    Private Sub _CloseButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CloseButton.Click
        Me.Close()
    End Sub

    Private Sub _MinButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MinButton.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub DashboardLabel_mouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _DashboardLabel.MouseDown
        TopPanel_MouseDown(sender, e)
    End Sub

    Private Sub DashboardLabel_mouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _DashboardLabel.MouseMove
        TopPanel_MouseMove(sender, e)
    End Sub

    Private Sub DashboardLabel_mouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _DashboardLabel.MouseUp
        TopPanel_MouseUp(sender, e)
    End Sub








    Private Sub Dashboard_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _DashboardButton.Click
        _DashboardButton.BusyBackColor = Color.FromArgb(20, 20, 20)
        _DashboardButton.MouseColorsEnabled = False
        _StatsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _PagesButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _LayoutButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _ThemeButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _SettingsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _StatsButton.MouseColorsEnabled = True
        _PagesButton.MouseColorsEnabled = True
        _LayoutButton.MouseColorsEnabled = True
        _ThemeButton.MouseColorsEnabled = True
        _SettingsButton.MouseColorsEnabled = True
    End Sub

    Private Sub Stats_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _StatsButton.Click
        _StatsButton.BusyBackColor = Color.FromArgb(20, 20, 20)
        _StatsButton.MouseColorsEnabled = False
        _DashboardButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _PagesButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _LayoutButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _ThemeButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _SettingsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _DashboardButton.MouseColorsEnabled = True
        _PagesButton.MouseColorsEnabled = True
        _LayoutButton.MouseColorsEnabled = True
        _ThemeButton.MouseColorsEnabled = True
        _SettingsButton.MouseColorsEnabled = True
    End Sub

    Private Sub Pages_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _PagesButton.Click
        _PagesButton.BusyBackColor = Color.FromArgb(20, 20, 20)
        _PagesButton.MouseColorsEnabled = False
        _DashboardButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _StatsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _LayoutButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _ThemeButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _SettingsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _DashboardButton.MouseColorsEnabled = True
        _StatsButton.MouseColorsEnabled = True
        _LayoutButton.MouseColorsEnabled = True
        _ThemeButton.MouseColorsEnabled = True
        _SettingsButton.MouseColorsEnabled = True
    End Sub

    Private Sub Layout_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _LayoutButton.Click
        _LayoutButton.BusyBackColor = Color.FromArgb(20, 20, 20)
        _LayoutButton.MouseColorsEnabled = False
        _DashboardButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _StatsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _PagesButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _ThemeButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _SettingsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _DashboardButton.MouseColorsEnabled = True
        _StatsButton.MouseColorsEnabled = True
        _PagesButton.MouseColorsEnabled = True
        _ThemeButton.MouseColorsEnabled = True
        _SettingsButton.MouseColorsEnabled = True
    End Sub

    Private Sub Theme_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ThemeButton.Click
        _ThemeButton.BusyBackColor = Color.FromArgb(20, 20, 20)
        _ThemeButton.MouseColorsEnabled = False
        _DashboardButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _StatsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _PagesButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _LayoutButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _SettingsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _DashboardButton.MouseColorsEnabled = True
        _StatsButton.MouseColorsEnabled = True
        _PagesButton.MouseColorsEnabled = True
        _LayoutButton.MouseColorsEnabled = True
        _SettingsButton.MouseColorsEnabled = True
    End Sub

    Private Sub Settings_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SettingsButton.Click
        _SettingsButton.BusyBackColor = Color.FromArgb(20, 20, 20)
        _SettingsButton.MouseColorsEnabled = False
        _DashboardButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _StatsButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _PagesButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _LayoutButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _ThemeButton.BusyBackColor = Color.FromArgb(30, 30, 40)
        _DashboardButton.MouseColorsEnabled = True
        _StatsButton.MouseColorsEnabled = True
        _PagesButton.MouseColorsEnabled = True
        _LayoutButton.MouseColorsEnabled = True
        _ThemeButton.MouseColorsEnabled = True
    End Sub



End Class

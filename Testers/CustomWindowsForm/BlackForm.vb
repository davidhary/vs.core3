﻿Imports System.ComponentModel
Imports System.Text
Imports System.Threading.Tasks
Imports isr.Core.Controls

Partial Public Class BlackForm
    Inherits Form

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub BlackForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub


    Private isTopPanelDragged As Boolean = False
    Private isLeftPanelDragged As Boolean = False
    Private isRightPanelDragged As Boolean = False
    Private isBottomPanelDragged As Boolean = False
    Private isTopBorderPanelDragged As Boolean = False

    Private isRightBottomPanelDragged As Boolean = False
    Private isLeftBottomPanelDragged As Boolean = False
    Private isRightTopPanelDragged As Boolean = False
    Private isLeftTopPanelDragged As Boolean = False

    Private isWindowMaximized As Boolean = False
    Private offset As Point
    Private _NormalWindowSize As Size
    Private _NormalWindowLocation As Point = Point.Empty



    Private Sub TopBorderPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            isTopBorderPanelDragged = True
        Else
            isTopBorderPanelDragged = False
        End If
    End Sub


    Private Sub TopBorderPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderPanel.MouseMove
        If e.Y < Me.Location.Y Then
            If isTopBorderPanelDragged Then
                If Me.Height < 50 Then
                    Me.Height = 50
                    isTopBorderPanelDragged = False
                Else
                    Me.Location = New Point(Me.Location.X, Me.Location.Y + e.Y)
                    Me.Height = Me.Height - e.Y
                End If
            End If
        End If
    End Sub


    Private Sub TopBorderPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderPanel.MouseUp
        isTopBorderPanelDragged = False
    End Sub



    Private Sub TopPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            isTopPanelDragged = True
            Dim pointStartPosition As Point = Me.PointToScreen(New Point(e.X, e.Y))
            offset = New Point() With {
                .X = Me.Location.X - pointStartPosition.X,
                .Y = Me.Location.Y - pointStartPosition.Y}
        Else
            isTopPanelDragged = False
        End If
        If e.Clicks = 2 Then
            isTopPanelDragged = False
            _MaxButton_Click(sender, e)
        End If
    End Sub

    Private Sub TopPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseMove
        If isTopPanelDragged Then
            Dim newPoint As Point = _TopPanel.PointToScreen(New Point(e.X, e.Y))
            newPoint.Offset(offset)
            Me.Location = newPoint

            If Me.Location.X > 2 OrElse Me.Location.Y > 2 Then
                If Me.WindowState = FormWindowState.Maximized Then
                    Me.Location = _normalWindowLocation
                    Me.Size = _normalWindowSize
                    _ToolTip.SetToolTip(_MaxButton, "Maximize")
                    _MaxButton.CustomFormState = CustomFormState.Normal
                    isWindowMaximized = False
                End If
            End If
        End If
    End Sub


    Private Sub TopPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseUp
        isTopPanelDragged = False
        If Me.Location.Y <= 5 Then
            If Not isWindowMaximized Then
                _normalWindowSize = Me.Size
                _normalWindowLocation = Me.Location

                Dim rect As Rectangle = Screen.PrimaryScreen.WorkingArea
                Me.Location = New Point(0, 0)
                Me.Size = New System.Drawing.Size(rect.Width, rect.Height)
                _ToolTip.SetToolTip(_MaxButton, "Restore Down")
                _MaxButton.CustomFormState = CustomFormState.Maximize
                isWindowMaximized = True
            End If
        End If
    End Sub



    Private Sub LeftPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderPanel.MouseDown
        If Me.Location.X <= 0 OrElse e.X < 0 Then
            isLeftPanelDragged = False
            Me.Location = New Point(10, Me.Location.Y)
        Else
            If e.Button = MouseButtons.Left Then
                isLeftPanelDragged = True
            Else
                isLeftPanelDragged = False
            End If
        End If
    End Sub

    Private Sub LeftPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderPanel.MouseMove
        If e.X < Me.Location.X Then
            If isLeftPanelDragged Then
                If Me.Width < 100 Then
                    Me.Width = 100
                    isLeftPanelDragged = False
                Else
                    Me.Location = New Point(Me.Location.X + e.X, Me.Location.Y)
                    Me.Width = Me.Width - e.X
                End If
            End If
        End If
    End Sub

    Private Sub LeftPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderPanel.MouseUp
        isLeftPanelDragged = False
    End Sub



    Private Sub RightPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            isRightPanelDragged = True
        Else
            isRightPanelDragged = False
        End If
    End Sub

    Private Sub RightPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderPanel.MouseMove
        If isRightPanelDragged Then
            If Me.Width < 100 Then
                Me.Width = 100
                isRightPanelDragged = False
            Else
                Me.Width = Me.Width + e.X
            End If
        End If
    End Sub

    Private Sub RightPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderPanel.MouseUp
        isRightPanelDragged = False
    End Sub



    Private Sub BottomPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            isBottomPanelDragged = True
        Else
            isBottomPanelDragged = False
        End If
    End Sub

    Private Sub BottomPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderPanel.MouseMove
        If isBottomPanelDragged Then
            If Me.Height < 50 Then
                Me.Height = 50
                isBottomPanelDragged = False
            Else
                Me.Height = Me.Height + e.Y
            End If
        End If
    End Sub

    Private Sub BottomPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderPanel.MouseUp
        isBottomPanelDragged = False
    End Sub


    Private Sub _MinButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MinButton.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub _MaxButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MaxButton.Click
        If isWindowMaximized Then
            Me.Location = _NormalWindowLocation
            Me.Size = _NormalWindowSize
            _ToolTip.SetToolTip(_MaxButton, "Maximize")
            _MaxButton.CustomFormState = CustomFormState.Normal
            isWindowMaximized = False
        Else
            _NormalWindowSize = Me.Size
            _NormalWindowLocation = Me.Location

            Dim rect As Rectangle = Screen.PrimaryScreen.WorkingArea
            Me.Location = New Point(0, 0)
            Me.Size = New System.Drawing.Size(rect.Width, rect.Height)
            _ToolTip.SetToolTip(_MaxButton, "Restore Down")
            _MaxButton.CustomFormState = CustomFormState.Maximize
            isWindowMaximized = True
        End If
    End Sub

    Private Sub _CloseButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CloseButton.Click
        Me.Close()
    End Sub




    Private Sub RightBottomPanel_1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderRightPanel.MouseDown
        isRightBottomPanelDragged = True
    End Sub

    Private Sub RightBottomPanel_1_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderRightPanel.MouseMove
        If isRightBottomPanelDragged Then
            If Me.Width < 100 OrElse Me.Height < 50 Then
                Me.Width = 100
                Me.Height = 50
                isRightBottomPanelDragged = False
            Else
                Me.Width = Me.Width + e.X
                Me.Height = Me.Height + e.Y
            End If
        End If
    End Sub


    Private Sub RightBottomPanel_1_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderRightPanel.MouseUp
        isRightBottomPanelDragged = False
    End Sub

    Private Sub RightBottomPanel_2_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderBottomPanel.MouseDown
        RightBottomPanel_1_MouseDown(sender, e)
    End Sub

    Private Sub RightBottomPanel_2_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderBottomPanel.MouseMove
        RightBottomPanel_1_MouseMove(sender, e)
    End Sub

    Private Sub RightBottomPanel_2_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderBottomPanel.MouseUp
        RightBottomPanel_1_MouseUp(sender, e)
    End Sub



    Private Sub LeftBottomPanel_1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderLeftPanel.MouseDown
        isLeftBottomPanelDragged = True
    End Sub

    Private Sub LeftBottomPanel_1_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderLeftPanel.MouseMove
        If e.X < Me.Location.X Then
            If isLeftBottomPanelDragged OrElse Me.Height < 50 Then
                If Me.Width < 100 Then
                    Me.Width = 100
                    Me.Height = 50
                    isLeftBottomPanelDragged = False
                Else
                    Me.Location = New Point(Me.Location.X + e.X, Me.Location.Y)
                    Me.Width = Me.Width - e.X
                    Me.Height = Me.Height + e.Y
                End If
            End If
        End If
    End Sub

    Private Sub LeftBottomPanel_1_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _BottomBorderLeftPanel.MouseUp
        isLeftBottomPanelDragged = False
    End Sub

    Private Sub LeftBottomPanel_2_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderBottomPanel.MouseDown
        LeftBottomPanel_1_MouseDown(sender, e)
    End Sub

    Private Sub LeftBottomPanel_2_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderBottomPanel.MouseMove
        LeftBottomPanel_1_MouseMove(sender, e)
    End Sub

    Private Sub LeftBottomPanel_2_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderBottomPanel.MouseUp
        LeftBottomPanel_1_MouseUp(sender, e)
    End Sub




    Private Sub RightTopPanel_1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderTopPanel.MouseDown
        isRightTopPanelDragged = True
    End Sub

    Private Sub RightTopPanel_1_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderTopPanel.MouseMove
        If e.Y < Me.Location.Y OrElse e.X < Me.Location.X Then
            If isRightTopPanelDragged Then
                If Me.Height < 50 OrElse Me.Width < 100 Then
                    Me.Height = 50
                    Me.Width = 100
                    isRightTopPanelDragged = False
                Else
                    Me.Location = New Point(Me.Location.X, Me.Location.Y + e.Y)
                    Me.Height = Me.Height - e.Y
                    Me.Width = Me.Width + e.X
                End If
            End If
        End If
    End Sub

    Private Sub RightTopPanel_1_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _RightBorderTopPanel.MouseUp
        isRightTopPanelDragged = False
    End Sub

    Private Sub RightTopPanel_2_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderRightPanel.MouseDown
        RightTopPanel_1_MouseDown(sender, e)
    End Sub

    Private Sub RightTopPanel_2_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderRightPanel.MouseMove
        RightTopPanel_1_MouseMove(sender, e)
    End Sub

    Private Sub RightTopPanel_2_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderRightPanel.MouseUp
        RightTopPanel_1_MouseUp(sender, e)
    End Sub





    Private Sub LeftTopPanel_1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderLeftPanel.MouseDown
        isLeftTopPanelDragged = True
    End Sub

    Private Sub LeftTopPanel_1_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderLeftPanel.MouseMove
        If e.X < Me.Location.X OrElse e.Y < Me.Location.Y Then
            If isLeftTopPanelDragged Then
                If Me.Width < 100 OrElse Me.Height < 50 Then
                    Me.Width = 100
                    Me.Height = 100
                    isLeftTopPanelDragged = False
                Else
                    Me.Location = New Point(Me.Location.X + e.X, Me.Location.Y)
                    Me.Width = Me.Width - e.X
                    Me.Location = New Point(Me.Location.X, Me.Location.Y + e.Y)
                    Me.Height = Me.Height - e.Y
                End If
            End If
        End If

    End Sub

    Private Sub LeftTopPanel_1_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopBorderLeftPanel.MouseUp
        isLeftTopPanelDragged = False
    End Sub

    Private Sub LeftTopPanel_2_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderTopPanel.MouseDown
        LeftTopPanel_1_MouseDown(sender, e)
    End Sub

    Private Sub LeftTopPanel_2_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderTopPanel.MouseMove
        LeftTopPanel_1_MouseMove(sender, e)
    End Sub

    Private Sub LeftTopPanel_2_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _LeftBorderTopPanel.MouseUp
        LeftTopPanel_1_MouseUp(sender, e)
    End Sub






    Private Sub File_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FileButton.Click
        _FileButton.BusyBackColor = Color.Black
        _FileButton.MouseColorsEnabled = False
        _EditButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _ViewButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _RunButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _HelpButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _EditButton.MouseColorsEnabled = True
        _ViewButton.MouseColorsEnabled = True
        _RunButton.MouseColorsEnabled = True
        _HelpButton.MouseColorsEnabled = True
    End Sub

    Private Sub Edit_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _EditButton.Click
        _EditButton.BusyBackColor = Color.Black
        _EditButton.MouseColorsEnabled = False
        _FileButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _ViewButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _RunButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _HelpButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _FileButton.MouseColorsEnabled = True
        _ViewButton.MouseColorsEnabled = True
        _RunButton.MouseColorsEnabled = True
        _HelpButton.MouseColorsEnabled = True
    End Sub

    Private Sub View_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ViewButton.Click
        _ViewButton.BusyBackColor = Color.Black
        _ViewButton.MouseColorsEnabled = False
        _FileButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _EditButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _RunButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _HelpButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _FileButton.MouseColorsEnabled = True
        _EditButton.MouseColorsEnabled = True
        _RunButton.MouseColorsEnabled = True
        _HelpButton.MouseColorsEnabled = True
    End Sub

    Private Sub Run_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _RunButton.Click
        _RunButton.BusyBackColor = Color.Black
        _RunButton.MouseColorsEnabled = False
        _FileButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _EditButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _ViewButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _HelpButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _FileButton.MouseColorsEnabled = True
        _EditButton.MouseColorsEnabled = True
        _ViewButton.MouseColorsEnabled = True
        _HelpButton.MouseColorsEnabled = True
    End Sub

    Private Sub Help_button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _HelpButton.Click
        _HelpButton.BusyBackColor = Color.Black
        _HelpButton.MouseColorsEnabled = False
        _FileButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _EditButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _ViewButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _RunButton.BusyBackColor = Color.FromArgb(60, 60, 60)
        _FileButton.MouseColorsEnabled = True
        _EditButton.MouseColorsEnabled = True
        _ViewButton.MouseColorsEnabled = True
        _RunButton.MouseColorsEnabled = True
    End Sub






    Private Sub WindowTextLabel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _WindowTextLabel.MouseDown
        TopPanel_MouseDown(sender, e)
    End Sub

    Private Sub WindowTextLabel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _WindowTextLabel.MouseMove
        TopPanel_MouseMove(sender, e)
    End Sub

    Private Sub WindowTextLabel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _WindowTextLabel.MouseUp
        TopPanel_MouseUp(sender, e)
    End Sub




End Class


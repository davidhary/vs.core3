﻿Imports System.ComponentModel
Imports System.Text
Imports System.Threading.Tasks
Imports isr.Core.Controls
Partial Public Class MainForm
    Inherits Form

    Public Sub New()
        InitializeComponent()
    End Sub

    Private offset As Point
    Private isTopPanelDragged As Boolean = False


    Private Sub TopPanel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            isTopPanelDragged = True
            Dim pointStartPosition As Point = Me.PointToScreen(New Point(e.X, e.Y))
            offset = New Point() With {
                .X = Me.Location.X - pointStartPosition.X,
                .Y = Me.Location.Y - pointStartPosition.Y}
        Else
            isTopPanelDragged = False
        End If
    End Sub

    Private Sub TopPanel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseMove
        If isTopPanelDragged Then
            Dim newPoint As Point = _TopPanel.PointToScreen(New Point(e.X, e.Y))
            newPoint.Offset(offset)
            Me.Location = newPoint
        End If
    End Sub

    Private Sub TopPanel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _TopPanel.MouseUp
        isTopPanelDragged = False
    End Sub

    Private Sub WindowTextLabel_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _WindowTextLabel.MouseDown
        TopPanel_MouseDown(sender, e)
    End Sub

    Private Sub WindowTextLabel_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _WindowTextLabel.MouseMove
        TopPanel_MouseMove(sender, e)
    End Sub

    Private Sub WindowTextLabel_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _WindowTextLabel.MouseUp
        TopPanel_MouseUp(sender, e)
    End Sub

    Private Sub _CloseButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CloseButton.Click
        Me.Close()
    End Sub

    Private Sub _MinButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MinButton.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub


    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Private Sub ShapedButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _OpenBlueFormShapedButton.Click
        TryCast(New BlueForm(), BlueForm)?.Show()
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Private Sub ShapedButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _OpenDarkFormShapedButton.Click
        TryCast(New BlackForm(), BlackForm)?.Show()
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Private Sub ShapedButton3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _OpenDashboardShapedButton.Click
        TryCast(New DashboardForm(), DashboardForm)?.Show()
    End Sub

    Private Sub ButtonZ1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ExitButton.Click
        Me.Close()
    End Sub


End Class


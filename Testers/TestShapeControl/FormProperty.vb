Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms
Imports isr.Core.Controls

Public Partial Class FormProperty
	Inherits Form

    Private _Caller As ShapeControl = Nothing

    Public Property Caller() As ShapeControl
        Get
            Return _caller
        End Get
        Set
            _caller = Value
            Dim s As String = _caller.Tag2
            Dim info As String() = s.Split(":"c)
            textBoxIP.Text = info(0)
            textBoxNotes.Text = info(1)
            Me.Text = _caller.Text & " properties"
        End Set
    End Property

    Public Sub New()
		InitializeComponent()
	End Sub

	Private Sub FormProperty_FormClosing(sender As Object, e As FormClosingEventArgs)
		'should validate first
		_caller.Tag2 = textBoxIP.Text & ":" & textBoxNotes.Text
	End Sub

	Private Sub FormProperty_Activated(sender As Object, e As EventArgs)
		Me.Location = _caller.Location
	End Sub

	Private Sub TextBoxIP_Enter(sender As Object, e As EventArgs)
		textBoxIP.SelectionStart = textBoxIP.Text.Length
	End Sub
End Class

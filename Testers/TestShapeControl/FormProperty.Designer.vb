Partial Class FormProperty
	''' <summary>
	''' Required designer variable.
	''' </summary>
	Private components As System.ComponentModel.IContainer = Nothing

	''' <summary>
	''' Clean up any resources being used.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(disposing As Boolean)
		If disposing AndAlso (components IsNot Nothing) Then
			components.Dispose()
		End If
		MyBase.Dispose(disposing)
	End Sub

	#Region "Windows Form Designer generated code"

	''' <summary>
	''' Required method for Designer support - do not modify
	''' the contents of this method with the code editor.
	''' </summary>
	Private Sub InitializeComponent()
		Me.label1 = New System.Windows.Forms.Label()
		Me.label2 = New System.Windows.Forms.Label()
		Me.textBoxNotes = New System.Windows.Forms.TextBox()
		Me.textBoxIP = New System.Windows.Forms.TextBox()
		Me.SuspendLayout()
		' 
		' label1
		' 
		Me.label1.AutoSize = True
		Me.label1.Location = New System.Drawing.Point(32, 39)
		Me.label1.Name = "label1"
		Me.label1.Size = New System.Drawing.Size(17, 13)
		Me.label1.TabIndex = 0
		Me.label1.Text = "IP"
		' 
		' label2
		' 
		Me.label2.AutoSize = True
		Me.label2.Location = New System.Drawing.Point(32, 81)
		Me.label2.Name = "label2"
		Me.label2.Size = New System.Drawing.Size(35, 13)
		Me.label2.TabIndex = 1
		Me.label2.Text = "Notes"
		' 
		' textBoxNotes
		' 
		Me.textBoxNotes.Location = New System.Drawing.Point(73, 81)
		Me.textBoxNotes.MaxLength = 100
		Me.textBoxNotes.Name = "textBoxNotes"
		Me.textBoxNotes.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
		Me.textBoxNotes.Size = New System.Drawing.Size(188, 20)
		Me.textBoxNotes.TabIndex = 1
		' 
		' textBoxIP
		' 
		Me.textBoxIP.Location = New System.Drawing.Point(73, 39)
		Me.textBoxIP.MaxLength = 15
		Me.textBoxIP.Name = "textBoxIP"
		Me.textBoxIP.Size = New System.Drawing.Size(188, 20)
		Me.textBoxIP.TabIndex = 0
		AddHandler Me.textBoxIP.Enter, New System.EventHandler(AddressOf Me.textBoxIP_Enter)
		' 
		' FormProperty
		' 
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6F, 13F)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(296, 161)
		Me.Controls.Add(Me.textBoxIP)
		Me.Controls.Add(Me.textBoxNotes)
		Me.Controls.Add(Me.label2)
		Me.Controls.Add(Me.label1)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "FormProperty"
		Me.Text = "Cam Properties"
		AddHandler Me.Activated, New System.EventHandler(AddressOf Me.FormProperty_Activated)
		AddHandler Me.FormClosing, New System.Windows.Forms.FormClosingEventHandler(AddressOf Me.FormProperty_FormClosing)
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

	#End Region

	Private label1 As System.Windows.Forms.Label
	Private label2 As System.Windows.Forms.Label
	Private textBoxNotes As System.Windows.Forms.TextBox
	Private textBoxIP As System.Windows.Forms.TextBox
End Class

Imports System.Drawing
Imports System.Windows.Forms
Imports System.Reflection
Imports isr.Core.Controls

''' <summary>
''' Summary description for Form1.
''' </summary>
Public Class Form1
	Inherits System.Windows.Forms.Form
    Private WithEvents ShapeControl1 As ShapeControl
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.Container = Nothing
	Private panel1 As System.Windows.Forms.Panel
	Private bm As Bitmap
	Private sx As Integer, sy As Integer
	Private tempregion As Region
    Private WithEvents ShapeControl3 As ShapeControl
    Private WithEvents ShapeControl4 As ShapeControl
    Private shapeControl5 As ShapeControl
    Private shapeControl7 As ShapeControl
    Private shapeControl6 As ShapeControl
    Private customControl11 As ShapeControl
    Private shapeControl2 As ShapeControl


    Public Sub New()

        '
        ' Required for Windows Form Designer support
        '
        InitializeComponent()

        '
        ' TODO: Add any constructor code after InitializeComponent call
        '
        bm = New Bitmap(Me.panel1.Width, Me.panel1.Height)
        Dim g As Graphics = System.Drawing.Graphics.FromImage(bm)
        g.FillRectangle(Brushes.LightBlue, New Rectangle(0, 0, Me.panel1.Width, Me.panel1.Height))
        Dim sf As New StringFormat() With {.Alignment = StringAlignment.Center, .LineAlignment = StringAlignment.Center}
        g.DrawString("THIS IS THE TEST BACKGROUND", New Font("Arial", 18, FontStyle.Bold),
                     Brushes.Brown, New Rectangle(New Point(0, 0), New Size(Me.panel1.Width, Me.panel1.Height)), sf)
        Me.panel1.BackgroundImage = bm
    End Sub

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
                If bm IsNot Nothing Then bm.Dispose() : bm = Nothing
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"
    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.panel1 = New System.Windows.Forms.Panel
        Me.shapeControl5 = New ShapeControl
        Me.customControl11 = New ShapeControl
        Me.shapeControl7 = New ShapeControl
        Me.shapeControl4 = New ShapeControl
        Me.shapeControl3 = New ShapeControl
        Me.shapeControl2 = New ShapeControl
        Me.shapeControl1 = New ShapeControl
        Me.shapeControl6 = New ShapeControl
        Me.panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'panel1
        '
        Me.panel1.Controls.Add(Me.shapeControl1)
        Me.panel1.Location = New System.Drawing.Point(8, 16)
        Me.panel1.Name = "panel1"
        Me.panel1.Size = New System.Drawing.Size(387, 320)
        Me.panel1.TabIndex = 2
        '
        'shapeControl5
        '
        Me.shapeControl5.BackColor = System.Drawing.Color.FromArgb(CType(CType(198, Byte), Integer), CType(CType(227, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(72, Byte), Integer))
        Me.shapeControl5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.shapeControl5.Blink = False
        Me.shapeControl5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.shapeControl5.BorderStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.shapeControl5.BorderWidth = 3
        Me.shapeControl5.CenterColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.shapeControl5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.shapeControl5.Location = New System.Drawing.Point(419, 64)
        Me.shapeControl5.Name = "shapeControl5"
        Me.shapeControl5.Shape = ShapeType.BalloonSE
        Me.shapeControl5.ShapeImage = Nothing
        Me.shapeControl5.Size = New System.Drawing.Size(144, 106)
        Me.shapeControl5.SurroundColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.shapeControl5.TabIndex = 6
        Me.shapeControl5.Tag2 = ""
        Me.shapeControl5.Text = "Hello, I am Tommy"
        Me.shapeControl5.UseGradient = False
        Me.shapeControl5.Vibrate = False
        Me.shapeControl5.Visible = False
        '
        'customControl11
        '
        Me.customControl11.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.customControl11.Blink = False
        Me.customControl11.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.customControl11.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
        Me.customControl11.BorderWidth = 0
        Me.customControl11.CenterColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.customControl11.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.customControl11.Location = New System.Drawing.Point(555, 137)
        Me.customControl11.Name = "customControl11"
        Me.customControl11.Shape = ShapeType.Ellipse
        Me.customControl11.ShapeImage = Nothing
        Me.customControl11.Size = New System.Drawing.Size(93, 92)
        Me.customControl11.SurroundColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(11, Byte), Integer))
        Me.customControl11.TabIndex = 9
        Me.customControl11.Tag2 = ""
        Me.customControl11.UseGradient = True
        Me.customControl11.Vibrate = False
        '
        'shapeControl7
        '
        Me.shapeControl7.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.shapeControl7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.shapeControl7.Blink = False
        Me.shapeControl7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.shapeControl7.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
        Me.shapeControl7.BorderWidth = 1
        Me.shapeControl7.CenterColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.shapeControl7.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.shapeControl7.Location = New System.Drawing.Point(585, 100)
        Me.shapeControl7.Name = "shapeControl7"
        Me.shapeControl7.Shape = ShapeType.CustomPolygon
        Me.shapeControl7.ShapeImage = Nothing
        Me.shapeControl7.Size = New System.Drawing.Size(20, 22)
        Me.shapeControl7.SurroundColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.shapeControl7.TabIndex = 8
        Me.shapeControl7.Tag2 = ""
        Me.shapeControl7.UseGradient = False
        Me.shapeControl7.Vibrate = False
        '
        'shapeControl4
        '
        Me.shapeControl4.BackColor = System.Drawing.Color.FromArgb(CType(CType(124, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.shapeControl4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.shapeControl4.Blink = False
        Me.shapeControl4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(131, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(4, Byte), Integer))
        Me.shapeControl4.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
        Me.shapeControl4.BorderWidth = 3
        Me.shapeControl4.CenterColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.shapeControl4.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.shapeControl4.Location = New System.Drawing.Point(551, 255)
        Me.shapeControl4.Name = "shapeControl4"
        Me.shapeControl4.Shape = ShapeType.RoundedRectangle
        Me.shapeControl4.ShapeImage = Nothing
        Me.shapeControl4.Size = New System.Drawing.Size(97, 35)
        Me.shapeControl4.SurroundColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.shapeControl4.TabIndex = 5
        Me.shapeControl4.Tag2 = ""
        Me.shapeControl4.Text = "Click Me!"
        Me.shapeControl4.UseGradient = False
        Me.shapeControl4.Vibrate = False
        '
        'shapeControl3
        '
        Me.shapeControl3.BackColor = System.Drawing.Color.FromArgb(CType(CType(124, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(159, Byte), Integer), CType(CType(83, Byte), Integer))
        Me.shapeControl3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.shapeControl3.Blink = False
        Me.shapeControl3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(131, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(4, Byte), Integer))
        Me.shapeControl3.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
        Me.shapeControl3.BorderWidth = 3
        Me.shapeControl3.CenterColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.shapeControl3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.shapeControl3.Location = New System.Drawing.Point(551, 306)
        Me.shapeControl3.Name = "shapeControl3"
        Me.shapeControl3.Shape = ShapeType.RoundedRectangle
        Me.shapeControl3.ShapeImage = Nothing
        Me.shapeControl3.Size = New System.Drawing.Size(97, 35)
        Me.shapeControl3.SurroundColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.shapeControl3.TabIndex = 4
        Me.shapeControl3.Tag2 = ""
        Me.shapeControl3.Text = "Close"
        Me.shapeControl3.UseGradient = False
        Me.shapeControl3.Vibrate = False
        '
        'shapeControl2
        '
        Me.shapeControl2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.shapeControl2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.shapeControl2.Blink = False
        Me.shapeControl2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(118, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(4, Byte), Integer), CType(CType(9, Byte), Integer))
        Me.shapeControl2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
        Me.shapeControl2.BorderWidth = 0
        Me.shapeControl2.CenterColor = System.Drawing.Color.FromArgb(CType(CType(90, Byte), Integer), CType(CType(163, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.shapeControl2.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.shapeControl2.Location = New System.Drawing.Point(519, 90)
        Me.shapeControl2.Name = "shapeControl2"
        Me.shapeControl2.Shape = ShapeType.TriangleUp
        Me.shapeControl2.ShapeImage = Nothing
        Me.shapeControl2.Size = New System.Drawing.Size(152, 32)
        Me.shapeControl2.SurroundColor = System.Drawing.Color.FromArgb(CType(CType(122, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.shapeControl2.TabIndex = 3
        Me.shapeControl2.Tag2 = ""
        Me.shapeControl2.UseGradient = True
        Me.shapeControl2.Vibrate = False
        '
        'shapeControl1
        '
        Me.shapeControl1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.shapeControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.shapeControl1.Blink = False
        Me.shapeControl1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(98, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.shapeControl1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
        Me.shapeControl1.BorderWidth = 0
        Me.shapeControl1.CenterColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.shapeControl1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.shapeControl1.ForeColor = System.Drawing.Color.Yellow
        Me.shapeControl1.Location = New System.Drawing.Point(55, 39)
        Me.shapeControl1.Name = "shapeControl1"
        Me.shapeControl1.Shape = ShapeType.Diamond
        Me.shapeControl1.ShapeImage = Nothing
        Me.shapeControl1.Size = New System.Drawing.Size(131, 143)
        Me.shapeControl1.SurroundColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.shapeControl1.TabIndex = 0
        Me.shapeControl1.Tag2 = ""
        Me.shapeControl1.Text = "Transparency Test Drag Me Around"
        Me.shapeControl1.UseGradient = True
        Me.shapeControl1.Vibrate = False
        '
        'shapeControl6
        '
        Me.shapeControl6.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(53, Byte), Integer))
        Me.shapeControl6.Blink = False
        Me.shapeControl6.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.shapeControl6.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
        Me.shapeControl6.BorderWidth = 0
        Me.shapeControl6.CenterColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(202, Byte), Integer), CType(CType(91, Byte), Integer), CType(CType(171, Byte), Integer))
        Me.shapeControl6.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.shapeControl6.Location = New System.Drawing.Point(682, 64)
        Me.shapeControl6.Name = "shapeControl6"
        Me.shapeControl6.Shape = ShapeType.Ellipse
        Me.shapeControl6.ShapeImage = CType(resources.GetObject("shapeControl6.ShapeImage"), System.Drawing.Image)
        Me.shapeControl6.Size = New System.Drawing.Size(101, 82)
        Me.shapeControl6.SurroundColor = System.Drawing.Color.FromArgb(CType(CType(53, Byte), Integer), CType(CType(198, Byte), Integer), CType(CType(74, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.shapeControl6.TabIndex = 9
        Me.shapeControl6.Tag2 = ""
        Me.shapeControl6.UseGradient = True
        Me.shapeControl6.Vibrate = False
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(810, 372)
        Me.ControlBox = False
        Me.Controls.Add(Me.shapeControl5)
        Me.Controls.Add(Me.customControl11)
        Me.Controls.Add(Me.shapeControl6)
        Me.Controls.Add(Me.shapeControl7)
        Me.Controls.Add(Me.shapeControl4)
        Me.Controls.Add(Me.shapeControl3)
        Me.Controls.Add(Me.shapeControl2)
        Me.Controls.Add(Me.panel1)
        Me.DoubleBuffered = True
        Me.Name = "Form1"
        Me.Text = "Test Shape Control"
        Me.panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region

    ''' <summary>
    ''' The main entry point for the application.
    ''' </summary>
    <STAThread>
    Private Shared Sub Main()
        Application.Run(New Form1())
    End Sub

    Private Sub ShapeControl1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles shapeControl1.MouseDown
        If e.Button.Equals(MouseButtons.Left) Then
            sx = e.X
            'temp region = ((ShapeControl)sender).Region.Clone();
            '((ShapeControl)sender).Region = null;
            sy = e.Y
        End If
    End Sub

    Private Sub ShapeControl1_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles shapeControl1.MouseMove
        If e.Button.Equals(MouseButtons.Left) Then
            DirectCast(sender, Control).Left = DirectCast(sender, Control).Left + (e.X - sx)
            DirectCast(sender, Control).Top = DirectCast(sender, Control).Top + (e.Y - sy)
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.DoubleBuffered = True
        GetType(Panel).InvokeMember("DoubleBuffered", BindingFlags.NonPublic Or BindingFlags.Instance Or BindingFlags.SetProperty, Nothing, panel1, New Object() {True})
    End Sub

    Private Sub ShapeControl4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles shapeControl4.Click
        shapeControl5.Visible = True
        Me.Refresh()
        System.Threading.Thread.Sleep(1000)
        shapeControl5.Visible = False
        Me.Refresh()

    End Sub

    Private Sub ShapeControl3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles shapeControl3.Click
        Me.Close()
    End Sub

End Class

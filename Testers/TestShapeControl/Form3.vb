Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms
Imports System.Reflection
Imports System.Linq
Imports System.IO
Imports isr.Core.Controls

Public Partial Class Form3
	Inherits Form
    Private ctrllist As New List(Of ShapeControl)()
    Private sx As Integer, sy As Integer

    Private static_i As Integer = 0
    Private ctrlKey As Boolean = False
    Private altKey As Boolean = False
    Private plusKey As Boolean = False
    Private minusKey As Boolean = False
    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub BtnAddCam_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddCam.Click
        AddCam("")
    End Sub

    Private Function GetNextCamIndex() As Integer
        If ctrllist.Count = 0 Then
            Return 1
        End If
        Dim tempvar As Object
        tempvar = ctrllist.OrderBy(Function(x) x.Name).ToList()
        ctrllist = DirectCast(tempvar, List(Of ShapeControl))
        ctrllist = ctrllist.OrderBy(Function(x) x.Name.Length).ToList()
        Dim templist As List(Of ShapeControl) = ctrllist.ToList()
        Dim count As Integer = templist.Count
        Dim retval As Integer = count + 1

        'find missing index
        For i As Integer = 0 To count - 1
            Dim ctrlname As String = templist(i).Name
            Dim ctrlindex As String = ctrlname.Substring(3)
            If (i + 1) <> Integer.Parse(ctrlindex) Then
                retval = (i + 1)
                Exit For
            End If
        Next


        Return retval
    End Function



    Private Sub AddCam(caminfo As String)


        Dim bNew As Boolean = (caminfo = "")
        Dim name As String = ""
        Dim tag As String = "", tag2 As String = ""
        Dim x As Integer = 0, y As Integer = 0, w As Integer = 0, h As Integer = 0, c As Integer = 0

        If caminfo <> "" Then
            Dim info As String() = caminfo.Split("|"c)
            For i As Integer = 0 To info.Length - 1
                Dim details As String() = info(i).Split("="c)
                Select Case details(0)
                    Case "name"
                        name = details(1)
                        Exit Select
                    Case "x"
                        x = Integer.Parse(details(1))
                        Exit Select
                    Case "y"
                        y = Integer.Parse(details(1))
                        Exit Select
                    Case "w"
                        w = Integer.Parse(details(1))
                        Exit Select
                    Case "h"
                        h = Integer.Parse(details(1))
                        Exit Select
                    Case "c"
                        c = Integer.Parse(details(1))
                        Exit Select
                    Case "tag"
                        tag = details(1)
                        Exit Select
                    Case "tag2"
                        tag2 = details(1)
                        Exit Select

                End Select
            Next
        End If
        ' ctrllist.Add(ctrl1);
        Dim ctrl1 As New ShapeControl() With {
            .BackColor = If(bNew, System.Drawing.Color.FromArgb(CInt(CByte(126)), Color.Red), Color.FromArgb(c)),
            .Blink = False,
            .BorderColor = System.Drawing.Color.FromArgb(CInt(CByte(0)), CInt(CByte(255)), CInt(CByte(255)), CInt(CByte(255))),
            .BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid,
            .BorderWidth = 3,
            .Font = New System.Drawing.Font("Arial", 8.0F, System.Drawing.FontStyle.Bold),
            .Name = If(bNew, "cam" & GetNextCamIndex(), name),
            .Shape = ShapeType.Ellipse,
            .ShapeImage = Global.TestShapeControl.My.Resources.Camshape,
            .Size = If(bNew, New System.Drawing.Size(40, 40), New System.Drawing.Size(w, h)),
            .TabIndex = 0,
            .UseGradient = False,
            .Vibrate = False,
            .Visible = True}
        '  ctrl1.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        'ctrllist.Count;
        ' ctrl1.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));

        AddHandler ctrl1.MouseDown, New MouseEventHandler(AddressOf ctrl1_MouseDown)
        AddHandler ctrl1.MouseMove, New MouseEventHandler(AddressOf ctrl1_MouseMove)

        AddHandler ctrl1.MouseDoubleClick, New MouseEventHandler(AddressOf ctrl1_MouseDoubleClick)
        AddHandler ctrl1.MouseHover, New EventHandler(AddressOf ctrl1_MouseHover)
        ctrllist.Add(ctrl1)
        Dim ypos As Integer = (50 * ctrllist.Count) Mod panel1.Height
        Dim xpos As Integer = ((50 * ctrllist.Count) \ panel1.Height) * 50
        ctrl1.Location = If(bNew, New System.Drawing.Point(50 + xpos, ypos - 40), New System.Drawing.Point(50, 50))
        Me.panel1.Controls.Add(ctrl1)
        ctrl1.Text = "cam"
        ctrl1.Text = If(bNew, DirectCast(ctrl1.Name.ToString().Clone(), String), name)
        ctrl1.BringToFront()
        ctrl1.Tag2 = If(bNew, "127.0.0.1:New cam", tag2)
        'set the color
        If bNew Then
            ctrl1_MouseDoubleClick(ctrl1, New MouseEventArgs(MouseButtons.Left, 2, 0, 0, 0))
        End If


        Dim dy As Single = CSng(ctrl1.Top + ctrl1.Height \ 2) - CSng(panel1.Height) / 2
        Dim dx As Single = CSng(ctrl1.Left + ctrl1.Width \ 2) - CSng(panel1.Width) / 2

        ctrl1.Tag = If(bNew, (dx & "," & dy & "," & getNumPixelforImageDisplayed()), tag)

    End Sub


    Private Sub Ctrl1_MouseHover(sender As Object, e As EventArgs)
        toolTip1.Show(DirectCast(sender, ShapeControl).Tag2 & ",(" & DirectCast(sender, ShapeControl).Left & "," & DirectCast(sender, ShapeControl).Top & ")", DirectCast(sender, ShapeControl), 2000)
    End Sub

    Private Sub Ctrl1_MouseDoubleClick(sender As Object, e As MouseEventArgs)
        If e.Clicks < 2 Then
            Return
        End If

        If e.Button.Equals(MouseButtons.Left) Then
            If plusKey AndAlso Not minusKey Then
                If DirectCast(sender, ShapeControl).Width < 80 Then

                    DirectCast(sender, ShapeControl).Size = New Size(DirectCast(sender, ShapeControl).Width + 5, DirectCast(sender, ShapeControl).Height + 5)
                End If
                plusKey = False
                Return
            End If

            If minusKey AndAlso Not plusKey Then
                If DirectCast(sender, ShapeControl).Width > 20 Then

                    DirectCast(sender, ShapeControl).Size = New Size(DirectCast(sender, ShapeControl).Width - 5, DirectCast(sender, ShapeControl).Height - 5)
                End If
                minusKey = False
                Return
            End If
            If ctrlKey AndAlso Not altKey Then
                Dim dr As DialogResult = MessageBox.Show(Me, "Delete cam?", "Delete", MessageBoxButtons.OKCancel)
                If dr = DialogResult.OK Then
                    ctrllist.Remove(DirectCast(sender, ShapeControl))
                    panel1.Controls.Remove(DirectCast(sender, ShapeControl))
                End If
                ctrlKey = False

                Return
            End If

            If altKey AndAlso Not ctrlKey Then
                DirectCast(sender, ShapeControl).Vibrate = Not DirectCast(sender, ShapeControl).Vibrate
                altKey = False
                Return
            End If

            If static_i >= 6 Then
                static_i = 0
            End If
            Select Case static_i
                Case 0
                    DirectCast(sender, ShapeControl).BackColor = System.Drawing.Color.FromArgb(126, Color.Red)
                    Exit Select
                Case 1
                    DirectCast(sender, ShapeControl).BackColor = System.Drawing.Color.FromArgb(126, Color.Blue)

                    Exit Select

                Case 2
                    DirectCast(sender, ShapeControl).BackColor = System.Drawing.Color.FromArgb(126, Color.Green)

                    Exit Select

                Case 3
                    DirectCast(sender, ShapeControl).BackColor = System.Drawing.Color.FromArgb(126, Color.Wheat)
                    Exit Select
                Case 4
                    DirectCast(sender, ShapeControl).BackColor = System.Drawing.Color.FromArgb(126, Color.GreenYellow)

                    Exit Select

                Case 5
                    DirectCast(sender, ShapeControl).BackColor = System.Drawing.Color.FromArgb(126, Color.Cyan)

                    Exit Select

            End Select
            static_i += 1
        End If


    End Sub

    Private Sub Ctrl1_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs)
        If e.Button.Equals(MouseButtons.Left) Then
            sx = e.X

            sy = e.Y
        End If
        If e.Button.Equals(MouseButtons.Right) Then
            Dim frm As New FormProperty() With {.Caller = DirectCast(sender, ShapeControl)}
            frm.ShowDialog()
        End If
    End Sub

    Private Sub Ctrl1_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs)

        If e.Button.Equals(MouseButtons.Left) Then
            DirectCast(sender, Control).Left = DirectCast(sender, Control).Left + (e.X - sx)
            DirectCast(sender, Control).Top = DirectCast(sender, Control).Top + (e.Y - sy)
            Dim dy As Single = CSng(DirectCast(sender, Control).Top) + DirectCast(sender, Control).Height \ 2 - CSng(Panel1.Height) / 2
            Dim dx As Single = CSng(DirectCast(sender, Control).Left) + DirectCast(sender, Control).Width \ 2 - CSng(panel1.Width) / 2
            DirectCast(sender, Control).Tag = dx & "," & dy & "," & getNumPixelforImageDisplayed()
        End If
    End Sub

    Private Sub Form3_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles MyBase.KeyDown
        ctrlKey = e.Control
        altKey = e.Alt
        If e.KeyCode = Keys.OemMinus Then
            minusKey = True
        End If
        If e.KeyCode = Keys.Oemplus Then
            plusKey = True
        End If
    End Sub

    Private Sub Form3_KeyUp(ByVal sender As Object, ByVal e As KeyEventArgs) Handles MyBase.KeyUp
        ctrlKey = False
        altKey = False
        minusKey = False
        plusKey = False
    End Sub

    Private Sub Panel1_MouseDoubleClick(ByVal sender As Object, ByVal e As MouseEventArgs) Handles panel1.MouseDoubleClick
        openFileDialog1.Filter = "Image files (*.bmp;*.jpg;*.gif)|*.bmp;*.jpg;*.gif|All files (*.*)|*.*"
        Dim dr As DialogResult = openFileDialog1.ShowDialog()
        If dr = DialogResult.OK Then
            Try
                Dim tempImage As New Bitmap(openFileDialog1.FileName)
                panel1.BackgroundImage = New Bitmap(tempImage)


            Catch
            End Try
        End If

    End Sub

    Private Function GetNumPixelforImageDisplayed() As Integer
        If panel1.BackgroundImage Is Nothing Then Exit Function
        Dim panelratio As Single = CSng(panel1.Width) / panel1.Height
        Dim imgratio As Single = CSng(panel1.BackgroundImage.Width) / CSng(panel1.BackgroundImage.Height)
        Dim dispwidth As Single, dispheight As Single
        If panelratio > imgratio Then
            'height limiting
            dispheight = panel1.Height
            dispwidth = imgratio * dispheight
        Else
            dispwidth = panel1.Width
            dispheight = dispwidth / imgratio
        End If

        ' System.Diagnostics.Debug.Print(imgratio +"," + dispwidth + "," + dispheight);

        Return CInt(Math.Truncate(dispwidth * dispheight))
    End Function

    'new 
    Private Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button2.Click
        label1.Text = "NewMap_" & Guid.NewGuid().ToString() & ".map"
        panel1.BackgroundImage = New Bitmap(panel1.Width, panel1.Height)
        Graphics.FromImage(panel1.BackgroundImage).FillRectangle(Brushes.White, New Rectangle(0, 0, panel1.Width, panel1.Height))
        Graphics.FromImage(panel1.BackgroundImage).DrawString("Dbl Click here to insert floor plan..", New Font(FontFamily.GenericSansSerif, 12), Brushes.Black, 50, 50)
        ctrllist.Clear()
        panel1.Controls.Clear()

    End Sub

    'save
    Private Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles button1.Click
        Using writer As StreamWriter = File.CreateText(label1.Text)
            ctrllist = ctrllist.OrderBy(Function(x) x.Name).ToList()
            ctrllist = ctrllist.OrderBy(Function(x) x.Name.Length).ToList()
            Dim templist As List(Of ShapeControl) = ctrllist.ToList()
            writer.WriteLine("CAM_COUNT=" & templist.Count)
            For i As Integer = 0 To ctrllist.Count - 1

                writer.WriteLine("name=" & templist(i).Name & "|" & "x=" & templist(i).Left & "|" & "y=" & templist(i).Top & "|" & "w=" & templist(i).Width & "|" & "h=" & templist(i).Height & "|" & "c=" & templist(i).BackColor.ToArgb() & "|" & "tag=" & templist(i).Tag.ToString() & "|" & "tag2=" & templist(i).Tag2.ToString())
            Next
        End Using
        If panel1.BackgroundImage IsNot Nothing Then
            panel1.BackgroundImage.Save(label1.Text & ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg)
        End If

        MessageBox.Show(label1.Text & " is saved")
    End Sub

    Private Sub Form3_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        Me.DoubleBuffered = True

        'invoke double buffer 
        GetType(Panel).InvokeMember("DoubleBuffered", BindingFlags.NonPublic Or BindingFlags.Instance Or BindingFlags.SetProperty, Nothing, panel1, New Object() {True})

        label2.Text = "On Cam> Right Click:Set Properties, Dbl_Click:Change Color, Ctl+Dbl_Click:Del, Alt+Dbl_Click:Vibrate, Minus+Dbl_Click:Smaller, Plus+Dbl_Click:Larger"
        button2_Click(Nothing, Nothing)
    End Sub

    Private Sub BtnImportMap_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnImportMap.Click
        openFileDialog1.Filter = "Map files (*.map)|*.map"
        Dim dr As DialogResult = openFileDialog1.ShowDialog()
        If dr = DialogResult.OK Then

            button2_Click(Nothing, Nothing)
            label1.Text = openFileDialog1.FileName

            Try
                Using reader As StreamReader = File.OpenText(label1.Text)
                    Dim s As String = reader.ReadLine()
                    Dim info As String() = s.Split("="c)
                    For i As Integer = 0 To Integer.Parse(info(1)) - 1
                        s = reader.ReadLine()
                        AddCam(s)



                    Next
                End Using


                If File.Exists(openFileDialog1.FileName & ".jpg") Then
                    Using tempImage As New Bitmap(openFileDialog1.FileName & ".jpg")
                        panel1.BackgroundImage = New Bitmap(tempImage)
                    End Using

                End If


                'resize


                updateCamPosAfterResize()

            Catch
            End Try
        End If

    End Sub


	Private Sub UpdateCamPosAfterResize()
		Dim newarea As Integer = getNumPixelforImageDisplayed()


		For i As Integer = 0 To ctrllist.Count - 1
            Dim info As String() = ctrllist(i).Tag.ToString().Split(","c)
			Dim dx As Single = Single.Parse(info(0))
			Dim dy As Single = Single.Parse(info(1))
			Dim area As Integer = Integer.Parse(info(2))
			'square root of area ratio = linear ratio
			Dim ratio As Single = CSng(Math.Sqrt(CSng(newarea) / area))
			'get the new offset using the calculated linear ratio
			Dim newdx As Single = ratio * dx
			Dim newdy As Single = ratio * dy


			'update the new pos for the cam 
			ctrllist(i).Left = CInt(Math.Truncate(panel1.Width \ 2 + newdx - ctrllist(i).Width \ 2))

			ctrllist(i).Top = CInt(Math.Truncate(panel1.Height \ 2 + newdy - ctrllist(i).Height \ 2))
		Next



	End Sub

    Private Sub Form3_Resize(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Resize

        Me.panel1.Visible = False

        panel1.Height = Me.ClientSize.Height - (3 * panel1.Top \ 2)
        panel1.Width = Me.ClientSize.Width - 2 * panel1.Left
        label2.Top = panel1.Top + panel1.Height + 10
        updateCamPosAfterResize()

        Me.panel1.Visible = True

    End Sub



End Class

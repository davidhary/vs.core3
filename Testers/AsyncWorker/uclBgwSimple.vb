﻿Imports System.Drawing
Imports System.Windows.Forms
Imports System.ComponentModel

Partial Public Class uclBgwSimple : Inherits UserControl

   Private Structure Data2Thread
      Public Time As DateTime
      Public Pt As Point
   End Structure
   Private Structure Data2Gui
      Public Text As String
      Public Pt As Point
   End Structure

   Private Sub Ucl_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseDown
      If BackgroundWorker1.IsBusy Then
         MessageBox.Show("backgroundWorker1.IsBusy")
         Return
      End If
      Dim d As New Data2Thread() With {.Time = DateTime.Now, .Pt = e.Location}
      BackgroundWorker1.RunWorkerAsync(d)
   End Sub

   Private Sub BackgroundWorker1_DoWork( 
         ByVal sender As Object, ByVal e As DoWorkEventArgs) 
         Handles BackgroundWorker1.DoWork
      Dim d As Data2Thread = CType(e.Argument, Data2Thread)
      System.Threading.Thread.Sleep(1000)
      Dim d2g As New Data2Gui() With { 
       .Pt = d.Pt, 
       .Text = String.Format("Position {0} / {1}" & vbLf & "clicked at {2:T}", d.Pt.X, d.Pt.Y, d.Time) 
      }
      e.Result = d2g
   End Sub

   Private Sub BackgroundWorker1_RunWorkerCompleted( 
         ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) 
         Handles BackgroundWorker1.RunWorkerCompleted
      Dim d2g As Data2Gui = CType(e.Result, Data2Gui)
      label1.Text = d2g.Text
      label1.Location = d2g.Pt - label1.Size
   End Sub

End Class

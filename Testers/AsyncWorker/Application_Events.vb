﻿Imports Microsoft.VisualBasic.ApplicationServices
Imports WinFormApp = System.Windows.Forms.Application

Namespace My

   Partial Friend Class MyApplication

      Private Sub App_Startup(ByVal sender As Object, ByVal e As StartupEventArgs) Handles Me.Startup
         AddHandler AppDomain.CurrentDomain.UnhandledException, AddressOf SideThread_Exception
         AddHandler WinFormApp.ThreadException, AddressOf MainThread_Exception
      End Sub

      Private Shared Sub MainThread_Exception(ByVal sender As Object, ByVal e As System.Threading.ThreadExceptionEventArgs)
         MessageBox.Show("Log unhandled main-thread-Exception here" & vbLf & e.Exception.ToString())
         WinFormApp.Exit()
      End Sub

      Private Shared Sub SideThread_Exception(ByVal sender As Object, ByVal e As System.UnhandledExceptionEventArgs)
         MessageBox.Show("Log unhandled side-thread-Exception here" & vbLf + e.ExceptionObject.ToString())
         WinFormApp.Exit()
      End Sub

   End Class

End Namespace


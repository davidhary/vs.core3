Imports isr.Core.Pith

Public Class SimpleForm
    Private itemsView As BindingListView(Of FeedItem)

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        LoadFeed()
    End Sub

    Private feed As Feed

    Private Sub LoadFeed()

        ' Get the BBC news RSS feed
        feed = New Feed("http://newsrss.bbc.co.uk/rss/newsonline_uk_edition/front_page/rss.xml")
        feed.Update()

        ' Create a view of the items
        itemsView = New BindingListView(Of FeedItem)(feed.Items)

        ' Make the grid display this view
        itemsGrid.DataSource = itemsView
    End Sub

    Private Sub FilterTextBox_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles filterTextBox.TextChanged
        itemsView.ApplyFilter(AddressOf TitleFilter)
    End Sub

    Private Function TitleFilter(ByVal item As FeedItem) As Boolean
        Return item.Title.ToLower().Contains(filterTextBox.Text.ToLower())
    End Function

    ''' <summary>
    ''' Handles the DataError event of the _dataGridView control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="DataGridViewDataErrorEventArgs"/> instance containing the event data.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _DataGridView_DataError(sender As Object, e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles itemsGrid.DataError
        Try
            ' prevent error reporting when adding a new row or editing a cell
            Dim grid As DataGridView = TryCast(sender, DataGridView)
            If grid IsNot Nothing Then
                If grid.CurrentRow IsNot Nothing AndAlso grid.CurrentRow.IsNewRow Then Return
                If grid.IsCurrentCellInEditMode Then Return
                If grid.IsCurrentRowDirty Then Return
                Me.EnunciateErrorDataGrid(grid, "Exception occurred displaying data")
            End If
        Catch
        End Try

    End Sub

    ''' <summary>
    ''' Enunciates the data grid error.
    ''' </summary>
    ''' <param name="value">The value.</param>
    Private Sub EnunciateErrorDataGrid(sender As Control, ByVal value As String)
        If sender IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(value) Then
            Me._ErrorProvider.SetIconPadding(sender, -15)
            Me._ErrorProvider.SetError(sender, value)
        End If
    End Sub

End Class
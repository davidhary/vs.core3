﻿Imports System
Imports System.Threading.Tasks

Namespace DependentTaskRunner.Examples
    Public NotInheritable Class AsyncAwaitExample

        Private Sub New()
            MyBase.New
        End Sub

        Public Shared Sub Run()
            Dim taskRunner As TaskRunner(Of MyTask) = New TaskRunner(Of MyTask)(Function(t) t.Dependencies, Function(t) Task.Run(t.Perform()))
            Dim taskA As New MyTask With {.Perform = AddressOf RunA}
            Dim taskB As New MyTask With {.Perform = AddressOf RunB}
            Dim taskC As New MyTask With {.Dependencies = {taskA, taskB}, .Perform = AddressOf RunC}
            taskRunner.PerformTasks({taskA, taskB, taskC})
        End Sub

        Private Shared Async Function RunA() As Task(Of Boolean)
            Console.WriteLine(Date.Now & " A Started")
            Await Task.Delay(2000)
            Console.WriteLine(Date.Now & " A Finished")
            Return True
        End Function

        Private Shared Async Function RunB() As Task(Of Boolean)
            Console.WriteLine(Date.Now & " B Started")
            Await Task.Delay(3000)
            Console.WriteLine(Date.Now & " B Finished")
            Return True
        End Function

        Private Shared Async Function RunC() As Task(Of Boolean)
            Console.WriteLine(Date.Now & " C Started")
            Await Task.Delay(4000)
            Console.WriteLine(Date.Now & " C Finished")
            Return True
        End Function

        Friend Class MyTask
            Public Property Dependencies() As MyTask()
            Public Property Perform() As Func(Of Task(Of Boolean))
        End Class
    End Class
End Namespace

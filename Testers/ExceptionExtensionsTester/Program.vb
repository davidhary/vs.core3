﻿Imports System.Text
Imports isr.Core.Pith.ExceptionExtensions
Friend Class Program
    Shared Sub Main(ByVal args() As String)

        Try
#If False Then
#ElseIf False Then
            DependentTaskRunner.Examples.BasicExample.Run()
#ElseIf True Then
            DependentTaskRunner.Examples.AsyncAwaitExample.Run()
#ElseIf False Then
            Dim exception As New Exception("This is an exception. Please see inner exception for details", GetInnerException())
            Throw exception
#Else
#End If
        Catch e As Exception
            Console.WriteLine(e.ToFullBlownString())
            Console.WriteLine(vbLf & vbLf & vbLf & "and now with level = 1:")
            Console.WriteLine(e.ToFullBlownString(1))
        End Try
        Console.WriteLine("Press any key to exit")
        Console.ReadKey()
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes")>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Shared Function GetInnerException() As Exception
        Try
            Throw New Exception("This is the inner exception. If this was production code, you'd be presented with details here.")
        Catch e As Exception
            Return e
        End Try
    End Function
End Class

﻿Imports System
Imports System.Threading
Imports System.Threading.Tasks

Namespace DependentTaskRunner.Examples
	Public NotInheritable Class BasicExample

		Private Sub New()
		End Sub

		Public Shared Sub Run()
            Dim taskRunner As New TaskRunner(Of MyTask)(Function(t) t.Dependencies, Function(t) Task.Run(t.Perform()))

            Dim taskA As New MyTask With {.Perform = AddressOf RunA}
            Dim taskB As New MyTask With {.Perform = AddressOf RunB}
            Dim taskC As New MyTask With {.Dependencies = {taskA, taskB}, .Perform = AddressOf RunC}

            taskRunner.PerformTasks( { taskA, taskB, taskC })
		End Sub

		Private Shared Function RunA() As Boolean
			Console.WriteLine(Date.Now & " A Started")
			Thread.Sleep(2000)
			Console.WriteLine(Date.Now & " A Finished")
			Return True
		End Function

		Private Shared Function RunB() As Boolean
			Console.WriteLine(Date.Now & " B Started")
			Thread.Sleep(3000)
			Console.WriteLine(Date.Now & " B Finished")
			Return True
		End Function

		Private Shared Function RunC() As Boolean
			Console.WriteLine(Date.Now & " C Started")
			Thread.Sleep(4000)
			Console.WriteLine(Date.Now & " C Finished")
			Return True
		End Function

		Friend Class MyTask
			Public Property Dependencies() As MyTask()
            Public Property Perform() As Func(Of Boolean)
        End Class
	End Class
End Namespace
